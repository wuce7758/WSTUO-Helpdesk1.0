
package com.wstuo.common.util;

import java.util.UUID;
/**
 * 
 * @author will
 *
 */
public class UUIDGenerator
{

    public UUIDGenerator()
    {
    }

    public static String getUUID()
    {
        String s = UUID.randomUUID().toString();
        return (new StringBuilder(String.valueOf(s.substring(0, 8)))).append(s.substring(9, 13)).append(s.substring(14, 18)).append(s.substring(19, 23)).append(s.substring(24)).toString();
    }

    public static String[] getUUID(int number)
    {
    	String ss[] = null;
        if(number > 0){
        	ss  = new String[number];
        	for(int i = 0; i < number; i++)
        		ss[i] = getUUID();
        }
        return ss;
    }

}
