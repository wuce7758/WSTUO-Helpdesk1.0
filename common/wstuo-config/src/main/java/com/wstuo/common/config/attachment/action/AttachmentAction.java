package com.wstuo.common.config.attachment.action;

import java.io.InputStream;

import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.xwork2.ActionSupport;
import com.wstuo.common.config.attachment.dto.AttachmentDTO;
import com.wstuo.common.config.attachment.dto.AttachmentDownloadDTO;
import com.wstuo.common.config.attachment.entity.Attachment;
import com.wstuo.common.config.attachment.service.IAttachmentService;
import com.wstuo.common.dto.PageDTO;


/**
 * 附件Action
 * @author QXY
 * date 2011-02-11
 */
@SuppressWarnings("serial")
public class AttachmentAction extends ActionSupport{
	@Autowired
    private IAttachmentService attachmentService;
    private Long aid;
    private Long downloadAttachmentId; //the attachment Id that was required to download.
    private InputStream downloadStream; //the input stream of the download file.
    private String downloadFileName; //the file name that was required to download.
    private String downloadContentType="application/zip"; //the content type of the download file.
    private PageDTO pageDTO;
    private int page=1;
	private int rows=10; 
	private String sidx;
	private String sord;
	private AttachmentDTO dto;
	private String filePath;
	private InputStream imageStream;
	private String imagePath;
	private Boolean result;
	

	public Boolean getResult() {
		return result;
	}

	public void setResult(Boolean result) {
		this.result = result;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public InputStream getImageStream() {
		return imageStream;
	}

	public void setImageStream(InputStream imageStream) {
		this.imageStream = imageStream;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public PageDTO getPageDTO() {
		return pageDTO;
	}

	public void setPageDTO(PageDTO pageDTO) {
		this.pageDTO = pageDTO;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getRows() {
		return rows;
	}

	public void setRows(int rows) {
		this.rows = rows;
	}

	public String getSidx() {
		return sidx;
	}

	public void setSidx(String sidx) {
		this.sidx = sidx;
	}

	public String getSord() {
		return sord;
	}

	public void setSord(String sord) {
		this.sord = sord;
	}

	public AttachmentDTO getDto() {
		return dto;
	}

	public void setDto(AttachmentDTO dto) {
		this.dto = dto;
	}

	public String getDownloadContentType() {
		return downloadContentType;
	}

    public InputStream getDownloadStream() {
		return downloadStream;
	}

	public String getDownloadFileName() {
		return downloadFileName;
	}

	public Long getDownloadAttachmentId() {
		return downloadAttachmentId;
	}

	public void setDownloadAttachmentId(Long downloadAttachmentId) {
		this.downloadAttachmentId = downloadAttachmentId;
	}

	public Long getAid() {

        return aid;
    }

    public void setAid(Long aid) {

        this.aid = aid;
    }
    /**
     * 获取附件列表信息
     */
    public String findattachmentPager(){
		int start = (page - 1) * rows;
		pageDTO=attachmentService.findByPager(dto, start, rows, sidx, sord);  
		pageDTO.setRows(rows);
		pageDTO.setPage(page);
		return "pageDTO";
	}

    /**
     * 删除附件
     * @return SUCCESS
     */
    public String deleteAttachment() {
        attachmentService.delete(aid);
        return SUCCESS;
    }
    /**
     * 删除附件
     * @return SUCCESS
     */
    public String deleteAttachmentReturn() {
    	result = attachmentService.deleteReturn(aid);
    	return "result";
    }
    
   
    /**
     * 图片查找返回流
     * @return String
     */
    public String findFileName(){
    	imageStream=attachmentService.getImageStream(dto.getAid());
    	return "imageStream";
    }
    /**
     * 下载附件
     */
    public String download() {
    	Attachment attachment = attachmentService.findAttachmentById(downloadAttachmentId);
    	
    	AttachmentDownloadDTO addto = attachmentService.downloadAttachment(attachment);
    	
		downloadStream = addto.getDownloadStream();
		downloadFileName = addto.getDownloadFileName();
		downloadContentType = addto.getDownloadContentType();
    	return "downloadSuccess";
    }
    
    
    /**
     * 删除文件.
     */
    public String delete(){
    	attachmentService.deleteAttachment(downloadAttachmentId);
    	return SUCCESS;
    }
    /**
     * 判断文件是否存在
     */
    public String findExist(){
    	result = attachmentService.isFileExit(imagePath);
    	return "result";
    }

}