package com.wstuo.common.config.attachment.dao;

import com.wstuo.common.config.attachment.dto.AttachmentDTO;
import com.wstuo.common.config.attachment.entity.Attachment;
import com.wstuo.common.dao.IEntityDAO;
import com.wstuo.common.dto.PageDTO;

/**
 * 附件DAO类接口
 * @author QXY
 * 修改时间20011-02-11
 */
public interface IAttachmentDAO extends IEntityDAO<Attachment>{

	
	/**
	 * 保存附件
	 * @param att
	 */
	void save (Attachment att);
	
	/**
	 * 删除附件
	 * @param att
	 */
	void delete(Attachment att);
	
	/**
	 * 批量删除
	 * @param ids
	 */
	void delete(Long [] ids);
	/**
	 * 分页查询
	 * @param dto
	 * @param start
	 * @param limit
	 * @param sidx
	 * @param sord
	 * @return PageDTO
	 */
	PageDTO findattachmentPager(AttachmentDTO dto,int start, int limit,String sidx , String sord);
	
	
}
