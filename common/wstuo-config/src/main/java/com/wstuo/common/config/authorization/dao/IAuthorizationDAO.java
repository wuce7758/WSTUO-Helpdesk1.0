package com.wstuo.common.config.authorization.dao;


import com.wstuo.common.config.authorization.dto.AuthorizationDTO;
import com.wstuo.common.config.authorization.entity.Authorization;
import com.wstuo.common.dao.IEntityDAO;
import com.wstuo.common.dto.PageDTO;

public interface IAuthorizationDAO extends IEntityDAO<Authorization>{

	/**
	 * 任务分页查询
	 * @param authorizationDTO
	 * @param start
	 * @param limit
	 * @return PageDTO
	 */
	PageDTO findPagerAuthorization(AuthorizationDTO authorizationDTO,
			int start, int limit,String sidx,String sord);
	
}
