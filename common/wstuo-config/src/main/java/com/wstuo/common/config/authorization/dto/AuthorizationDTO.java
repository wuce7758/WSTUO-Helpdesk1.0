package com.wstuo.common.config.authorization.dto;

import java.util.Date;

import com.wstuo.common.dto.BaseDTO;
/**
 * 授权管理DTO类
 * @author Will
 *
 */
public class AuthorizationDTO extends BaseDTO {
	private Long authId;
	private String authName;
	private String authType;
	private String authPawd;
	protected Date createTime=new Date();
	protected String creator;

	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public String getCreator() {
		return creator;
	}
	public void setCreator(String creator) {
		this.creator = creator;
	}
	public Long getAuthId() {
		return authId;
	}
	public void setAuthId(Long authId) {
		this.authId = authId;
	}
	public String getAuthName() {
		return authName;
	}
	public void setAuthName(String authName) {
		this.authName = authName;
	}
	public String getAuthType() {
		return authType;
	}
	public void setAuthType(String authType) {
		this.authType = authType;
	}
	public String getAuthPawd() {
		return authPawd;
	}
	public void setAuthPawd(String authPawd) {
		this.authPawd = authPawd;
	}
}
