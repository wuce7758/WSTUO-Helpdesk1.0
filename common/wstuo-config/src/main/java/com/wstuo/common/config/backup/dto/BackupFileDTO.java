package com.wstuo.common.config.backup.dto;

import com.wstuo.common.dto.BaseDTO;

/**
 * 备份信息DTO类
 * @author Will
 *
 */
public class BackupFileDTO extends BaseDTO{
	
	
	private String fileName;
	private String crateDate;
	private String fileSize;
	private String mqTitle;//MQ执行的主题
    private String mqContent;//MQ执行的内容
    private String mqCreator;//MQ创建者
    
	
	public String getMqTitle() {
		return mqTitle;
	}
	public void setMqTitle(String mqTitle) {
		this.mqTitle = mqTitle;
	}
	public String getMqContent() {
		return mqContent;
	}
	public void setMqContent(String mqContent) {
		this.mqContent = mqContent;
	}
	public String getMqCreator() {
		return mqCreator;
	}
	public void setMqCreator(String mqCreator) {
		this.mqCreator = mqCreator;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getCrateDate() {
		return crateDate;
	}
	public void setCrateDate(String crateDate) {
		this.crateDate = crateDate;
	}
	public String getFileSize() {
		return fileSize;
	}
	public void setFileSize(String fileSize) {
		this.fileSize = fileSize;
	}
	
	
	

}
