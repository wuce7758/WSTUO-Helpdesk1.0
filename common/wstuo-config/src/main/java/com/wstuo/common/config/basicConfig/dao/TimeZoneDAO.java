package com.wstuo.common.config.basicConfig.dao;

import com.wstuo.common.config.basicConfig.entity.TimeZone;
import com.wstuo.common.dao.BaseDAOImplHibernate;

/**
 * 时区DAO Class
 * @author QXY
 *
 */
public class TimeZoneDAO  extends BaseDAOImplHibernate<TimeZone> implements ITimeZoneDAO{

	
}
