package com.wstuo.common.config.cab.service;

import com.wstuo.common.config.cab.dto.CABDTO;
import com.wstuo.common.config.cab.dto.CABQueryDTO;
import com.wstuo.common.dto.PageDTO;

/**
 * CAB Service 接口
 * @author WSTUO
 *
 */
public interface ICABService {
	
	/**
	 * CAB 分页查询
	 * @param cabQueryDto
	 * @param start
	 * @param limit
	 * @return PageDTO
	 */
	public PageDTO findPagerCAB(CABQueryDTO cabQueryDto, int start, int limit);
	
	/**
	 * 保存CAB
	 * @param cabDto
	 */
	public void saveCAB(CABDTO cabDto);
	
	/**
	 * 编辑CAB
	 * @param cabDto
	 */
	public void editCAB(CABDTO cabDto);
	
	/**
	 * 删除CAB
	 * @param cabId
	 * @param cabIds
	 */
	public void deleteCAB(final Long cabId,final Long[] cabIds);
	/**
	 * 根据CABID获取成员列表
	 * @param cabId
	 * @param page
	 * @param limt
	 * @return PageDTO
	 */
	PageDTO findPagerCABMember(Long cabId,int page,int limt);
	
}
