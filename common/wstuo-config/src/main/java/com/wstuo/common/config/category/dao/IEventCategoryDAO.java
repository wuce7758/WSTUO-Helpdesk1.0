package com.wstuo.common.config.category.dao;


import com.wstuo.common.config.category.dto.EventCategoryDTO;
import com.wstuo.common.config.category.entity.EventCategory;
import com.wstuo.common.dao.IEntityDAO;
import com.wstuo.common.dto.PageDTO;

import java.util.List;

/**
 * Event Category DAO Interface
 * @version 0.1
 * @author Jet
 *  date 2010-9-10
 */
public interface IEventCategoryDAO
    extends IEntityDAO<EventCategory>
{
    /**
     * Query data
     */
    List<EventCategory> findTreeViews( String CategoryName );
    
    /**
     * 查询常用分类树
     * @param CategoryName
     * @return EventCategory
     */
    EventCategory findEventCategoryTree(String CategoryName);
    /**
     * 查询常用分类树
     * @param parentEventId
     * @return EventCategory
     */
    EventCategory findEventCategoryTreeByParentEventId(Long parentEventId);
    
    /**
     * 根据父节点查询所有子节点。
     * @param parentId
     * @return EventCategory
     */
    EventCategory findAllChildren(Long parentId);
    
    /**
     * 根据传入的编号数据查询服务目录
     * @param qdto
     * @return PageDTO
     */
    PageDTO findPagerByIds( EventCategoryDTO qdto );
    
    /**
     * 根据父节点查询子节点
     * @param parentNo
     * @return List<EventCategory>
     */
    List<EventCategory> findByParent( Long parentNo );
    /**
     * 数节点名称的查询
     * @param Event
     * @return EventCategory
     */
    EventCategory findEventTree(String Event);
    
    /**
     * 判断分类是否存在
     * @param ec
     * @return
     */
    boolean isCategoryExisted(EventCategoryDTO ec);

    /**
     * 根据Path路径查询子分类
     * @param path 分类路径
     * @return 返回当前路径下的所有分类
     */
    List<EventCategory> findSubCategoryByPath(String path);
    
}
