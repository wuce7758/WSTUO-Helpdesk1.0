package com.wstuo.common.config.category.dto;

import java.util.ArrayList;
import java.util.List;

import com.wstuo.common.dto.BaseDTO;

@SuppressWarnings("serial")
public class PullDownTreeViewDTO extends BaseDTO {
	
	private Long id;
	
	private String text;
	
	private String state;
	
	private List<PullDownTreeViewDTO> children = new ArrayList<PullDownTreeViewDTO>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public List<PullDownTreeViewDTO> getChildren() {
		return children;
	}

	public void setChildren(List<PullDownTreeViewDTO> children) {
		this.children = children;
	}
	

}
