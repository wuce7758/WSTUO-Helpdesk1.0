package com.wstuo.common.config.customfilter.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.wstuo.common.config.customfilter.dao.IExpressionDAO;
import com.wstuo.common.config.customfilter.dto.CustomExpressionDTO;
import com.wstuo.common.config.customfilter.entity.CustomExpression;

public class ExpressionService implements IExpressionService{
	@Autowired 
	private IExpressionDAO expressionDAO;
	
	/**
	 * 添加表达式
	 * @param dto
	 */
	@Transactional
	public void save(CustomExpressionDTO dto){
		CustomExpression entity=new CustomExpression();
		CustomExpressionDTO.dto2entity(dto, entity);
		expressionDAO.save(entity);
		dto.setExpId(entity.getExpId());
		
	}
	
	@Transactional
	public void batchSave(List<CustomExpressionDTO> list){
		if(list!=null){
			for (CustomExpressionDTO customExpressionDTO : list) {
				save(customExpressionDTO);
			}
		}
		
	}
	
}
