package com.wstuo.common.config.customfilter.service;

import org.springframework.transaction.annotation.Transactional;

import com.wstuo.common.config.customfilter.dto.KeyTransferDTO;
import com.wstuo.common.dto.PageDTO;

public interface ILinksEventQueryServices {

	@Transactional
	public abstract PageDTO finddynamic(KeyTransferDTO ktd,
			Long[] companyNos, int start, int rows, String sidx, String sord);

}