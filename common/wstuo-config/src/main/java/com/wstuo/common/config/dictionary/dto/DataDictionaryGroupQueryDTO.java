package com.wstuo.common.config.dictionary.dto;

import com.wstuo.common.dto.BaseDTO;
/**
 * DTO of dataDictionary group query
 * @author QXY
 *
 */
public class DataDictionaryGroupQueryDTO extends BaseDTO{

	
	/**
     * DataDictionaryGroup no*/
    private Long groupNo;
    /**
     * DataDictionaryGroup name*/
    private String groupName;
    private String groupCode;
    private String groupType;
	/**
     * Page start*/
    private Integer start;

    /**
     * Page limit*/
    private Integer limit;
    
    private String sidx;
    private String sord;
    
    
    
    
    public String getGroupType() {
		return groupType;
	}


	public void setGroupType(String groupType) {
		this.groupType = groupType;
	}


	public String getSidx() {
		return sidx;
	}


	public void setSidx(String sidx) {
		this.sidx = sidx;
	}


	public String getSord() {
		return sord;
	}


	public void setSord(String sord) {
		this.sord = sord;
	}


	public DataDictionaryGroupQueryDTO(){
    	super();
    }
    
    
    public Long getGroupNo() {
		return groupNo;
	}

	public void setGroupNo(Long groupNo) {
		this.groupNo = groupNo;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public Integer getStart() {
		return start;
	}


	public void setStart(Integer start) {
		this.start = start;
	}


	public Integer getLimit() {
		return limit;
	}


	public void setLimit(Integer limit) {
		this.limit = limit;
	}


	public String getGroupCode() {
		return groupCode;
	}


	public void setGroupCode(String groupCode) {
		this.groupCode = groupCode;
	}

	
}
