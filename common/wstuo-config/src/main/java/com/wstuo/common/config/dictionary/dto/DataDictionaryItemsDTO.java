package com.wstuo.common.config.dictionary.dto;

import com.wstuo.common.dto.BaseDTO;


/**
 * DTO of datadictionary.
 * @author QXY
 * */
@SuppressWarnings( "serial" )
public class DataDictionaryItemsDTO
    extends BaseDTO
{
    /**
     * DataDictionaryItems no*/
    private Long dcode;

    /**
     * DataDictionaryItems name*/
    private String dname;

    /**
     * DataDictionaryItems flag*/
    private String dflag;

    /**
     * DataDictionaryItems remark*/
    private String remark;

    /**
     * DataDictionaryItems description*/
    private String description;

    /**
     * DataDictionaryItems color*/
    private String color;

    /**
     * DataDictionaryItems icon*/
    private String icon;

    /**
     * DataDictionaryGroup no*/
    private Long groupNo;

    /**
     * DataDictionaryGroup name*/
    private String groupName;
    
 	private Byte dataFlag;

  	private String groupCode;
  	
  	private String dno;
    
   

	public Long getDcode() {
		return dcode;
	}



	public void setDcode(Long dcode) {
		this.dcode = dcode;
	}



	public String getDname() {
		return dname;
	}



	public void setDname(String dname) {
		this.dname = dname;
	}



	public String getDflag() {
		return dflag;
	}



	public void setDflag(String dflag) {
		this.dflag = dflag;
	}



	public String getRemark() {
		return remark;
	}



	public void setRemark(String remark) {
		this.remark = remark;
	}



	public String getDescription() {
		return description;
	}



	public void setDescription(String description) {
		this.description = description;
	}



	public String getColor() {
		return color;
	}



	public void setColor(String color) {
		this.color = color;
	}



	public String getIcon() {
		return icon;
	}



	public void setIcon(String icon) {
		this.icon = icon;
	}



	public Long getGroupNo() {
		return groupNo;
	}



	public void setGroupNo(Long groupNo) {
		this.groupNo = groupNo;
	}



	public String getGroupName() {
		return groupName;
	}



	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}



	public Byte getDataFlag() {
		return dataFlag;
	}



	public void setDataFlag(Byte dataFlag) {
		this.dataFlag = dataFlag;
	}



	public String getGroupCode() {
		return groupCode;
	}



	public void setGroupCode(String groupCode) {
		this.groupCode = groupCode;
	}
	
	public String getDno() {
		return dno;
	}



	public void setDno(String dno) {
		this.dno = dno;
	}

	public DataDictionaryItemsDTO(){
	}
	public DataDictionaryItemsDTO(Long dcode, String dname, String dflag,
			String remark, String description, String color, String icon,
			Long groupNo, String groupName, String groupCode) {
		super();
		this.dcode = dcode;
		this.dname = dname;
		this.dflag = dflag;
		this.remark = remark;
		this.description = description;
		this.color = color;
		this.icon = icon;
		this.groupNo = groupNo;
		this.groupName = groupName;
		this.groupCode = groupCode;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((color == null) ? 0 : color.hashCode());
		result = prime * result
				+ ((dataFlag == null) ? 0 : dataFlag.hashCode());
		result = prime * result + ((dcode == null) ? 0 : dcode.hashCode());
		result = prime * result
				+ ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((dflag == null) ? 0 : dflag.hashCode());
		result = prime * result + ((dname == null) ? 0 : dname.hashCode());
		result = prime * result + ((dno == null) ? 0 : dno.hashCode());
		result = prime * result
				+ ((groupCode == null) ? 0 : groupCode.hashCode());
		result = prime * result
				+ ((groupName == null) ? 0 : groupName.hashCode());
		result = prime * result + ((groupNo == null) ? 0 : groupNo.hashCode());
		result = prime * result + ((icon == null) ? 0 : icon.hashCode());
		result = prime * result + ((remark == null) ? 0 : remark.hashCode());
		return result;
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DataDictionaryItemsDTO other = (DataDictionaryItemsDTO) obj;
		if (color == null) {
			if (other.color != null)
				return false;
		} else if (!color.equals(other.color))
			return false;
		if (dataFlag == null) {
			if (other.dataFlag != null)
				return false;
		} else if (!dataFlag.equals(other.dataFlag))
			return false;
		if (dcode == null) {
			if (other.dcode != null)
				return false;
		} else if (!dcode.equals(other.dcode))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (dflag == null) {
			if (other.dflag != null)
				return false;
		} else if (!dflag.equals(other.dflag))
			return false;
		if (dname == null) {
			if (other.dname != null)
				return false;
		} else if (!dname.equals(other.dname))
			return false;
		if (dno == null) {
			if (other.dno != null)
				return false;
		} else if (!dno.equals(other.dno))
			return false;
		if (groupCode == null) {
			if (other.groupCode != null)
				return false;
		} else if (!groupCode.equals(other.groupCode))
			return false;
		if (groupName == null) {
			if (other.groupName != null)
				return false;
		} else if (!groupName.equals(other.groupName))
			return false;
		if (groupNo == null) {
			if (other.groupNo != null)
				return false;
		} else if (!groupNo.equals(other.groupNo))
			return false;
		if (icon == null) {
			if (other.icon != null)
				return false;
		} else if (!icon.equals(other.icon))
			return false;
		if (remark == null) {
			if (other.remark != null)
				return false;
		} else if (!remark.equals(other.remark))
			return false;
		return true;
	}
}
