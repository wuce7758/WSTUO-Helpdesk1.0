package com.wstuo.common.config.dictionary.dto;

import java.io.File;

import com.wstuo.common.dto.AbstractValueObject;

/**
 * the Query DTO of DataDictionary PageDTO.
 * 
 * @author QXY
 * */
@SuppressWarnings("serial")
public class DataDictionaryQueryDTO extends AbstractValueObject {
	/**
	 * DataDictionaryItems name
	 */
	private String dname;

	/**
	 * DataDictionaryItems description
	 */
	private String description;

	/**
	 * Page start
	 */
	private Integer start;

	/**
	 * Page limit
	 */
	private Integer limit;
	private Long groupNo;
	private String groupCode;

	public String getGroupCode() {
		return groupCode;
	}

	public void setGroupCode(String groupCode) {
		this.groupCode = groupCode;
	}

	private File importFile;

	public File getImportFile() {
		return importFile;
	}

	public void setImportFile(File importFile) {
		this.importFile = importFile;
	}

	/**
	 * get&set Method
	 */
	public String getDname() {
		return dname;
	}

	public void setDname(String dname) {
		this.dname = dname;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getStart() {
		return start;
	}

	public void setStart(Integer start) {
		this.start = start;
	}

	public Integer getLimit() {
		return limit;
	}

	public void setLimit(Integer limit) {
		this.limit = limit;
	}

	public Long getGroupNo() {
		return groupNo;
	}

	public void setGroupNo(Long groupNo) {
		this.groupNo = groupNo;
	}
}
