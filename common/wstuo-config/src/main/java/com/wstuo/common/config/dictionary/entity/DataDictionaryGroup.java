package com.wstuo.common.config.dictionary.entity;

import com.wstuo.common.entity.BaseEntity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * entity of datadictionarygroup
 * @author QXY
 * */
@SuppressWarnings( {"rawtypes",
    "serial"
} )
@Entity
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class DataDictionaryGroup
    extends BaseEntity
{
    /**
     * DataDictionaryGroup no*/
	@Id
    @GeneratedValue( strategy = GenerationType.AUTO )
    private Long groupNo;
    /**
     * DataDictionaryGroup name*/
	@Column( unique = true )
    private String groupName;
    @Column( unique = true )
    private String groupCode;
    
    private String groupType;
    /**
     * DataDictionaryItems*/
	@OneToMany( cascade = CascadeType.REMOVE )
	@JoinColumn( name = "groupNo" )
    private List<DataDictionaryItems> datadicItems = new ArrayList<DataDictionaryItems>();
   
	private Long treeNo=0l; //树结构id
	
	
	
	public Long getTreeNo() {
		return treeNo;
	}

	public void setTreeNo(Long treeNo) {
		this.treeNo = treeNo;
	}

	public String getGroupType() {
		return groupType;
	}

	public void setGroupType(String groupType) {
		this.groupType = groupType;
	}

	public String getGroupCode() {
		return groupCode;
	}

	public void setGroupCode(String groupCode) {
		this.groupCode = groupCode;
	}

    /**
     * get&set Method*/
    public List<DataDictionaryItems> getDatadicItems(  )
    {
        return datadicItems;
    }

    public void setDatadicItems( List<DataDictionaryItems> datadicItems )
    {
        this.datadicItems = datadicItems;
    }

    public Long getGroupNo(  )
    {
        return groupNo;
    }

    public void setGroupNo( Long groupNo )
    {
        this.groupNo = groupNo;
    }

    public String getGroupName(  )
    {
        return groupName;
    }

    public void setGroupName( String groupName )
    {
        this.groupName = groupName;
    }
}
