package com.wstuo.common.config.historyData.service;

import com.wstuo.common.config.historyData.dto.HistoryDataDTO;
import com.wstuo.common.config.historyData.dto.HistoryDataQueryDTO;
import com.wstuo.common.dto.PageDTO;

/**
 * 历史数据Serivce接口
 * @author Will 
 *
 */
public interface IHistoryDataService {
	
	/**
	 * 分页查询历史数据.
	 * @param qdto
	 * @return PageDTO
	 */
	PageDTO findHistoryDataPager(HistoryDataQueryDTO qdto);
	
	/**
	 * 保存历史数据.
	 * @param dto
	 */
	void saveHistoryData(HistoryDataDTO dto);

	/**
	 * 删除历史数据.
	 * @param ids
	 */
	void deleteHistoryData(Long [] ids);
	
	/**
	 * 根据编号取得JSON数据.
	 * @param entityId 编号
	 * @param entityClass 实体名称
	 * @return Object
	 */
	Object restore(Long entityId,String entityClass);

}
