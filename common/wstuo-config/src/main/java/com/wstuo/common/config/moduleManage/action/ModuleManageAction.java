package com.wstuo.common.config.moduleManage.action;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.xwork2.ActionSupport;
import com.wstuo.common.config.moduleManage.dto.ModuleManageDTO;
import com.wstuo.common.config.moduleManage.entity.ModuleManage;
import com.wstuo.common.config.moduleManage.service.IModuleManageService;
import com.wstuo.common.dto.PageDTO;
/**
 * 热拔插Action类
 * @author Wstuo
 *
 */
@SuppressWarnings("serial")
public class ModuleManageAction extends ActionSupport {
	@Autowired
	private IModuleManageService moduleManageService;
	private PageDTO pageDto;
	private ModuleManageDTO dto = new ModuleManageDTO();
	private int page = 1;
	private int rows = 10;
	private String sidx;
	private String sord;
	private Long[] ids;
	private  List<ModuleManage> list;
	
	public List<ModuleManage> getList() {
		return list;
	}

	public void setList(List<ModuleManage> list) {
		this.list = list;
	}

	public Long[] getIds() {
		return ids;
	}

	public void setIds(Long[] ids) {
		this.ids = ids;
	}

	public ModuleManageDTO getDto() {
		return dto;
	}

	public void setDto(ModuleManageDTO dto) {
		this.dto = dto;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getRows() {
		return rows;
	}

	public void setRows(int rows) {
		this.rows = rows;
	}

	public String getSidx() {
		return sidx;
	}

	public void setSidx(String sidx) {
		this.sidx = sidx;
	}

	public String getSord() {
		return sord;
	}

	public void setSord(String sord) {
		this.sord = sord;
	}

	public PageDTO getPageDto() {
		return pageDto;
	}

	public void setPageDto(PageDTO pageDto) {
		this.pageDto = pageDto;
	}


	/**
	 * 分页查询模块
	 * 
	 * @return String
	 */
	public String findPager() {
		int start = (page - 1) * rows;
		dto.setLimit(rows);
		dto.setStart(start);
		pageDto = moduleManageService.findModuleManagePager(dto, sord,
				sidx);
		pageDto.setPage(page);
		pageDto.setRows(rows);
		return SUCCESS;
	}

	
	/**
	 * 启用/禁用模块
	 * 
	 * @return String
	 */
	public String moduleDisable() {
		moduleManageService.moduleDisable(dto.getModuleId());
		return SUCCESS;
	}
	/**
	 * save
	 * 
	 * @return String
	 */
	public String saveModule() {
		moduleManageService.saveModule(dto);
		return SUCCESS;
	}
	/**
	 * 修改模块
	 * 
	 * @return String
	 */
	public String editModule() {
		moduleManageService.editModule(dto);
		return SUCCESS;
	}
	public String findAll() {
		list=moduleManageService.findAll();
		return "findAll";
	}
}
