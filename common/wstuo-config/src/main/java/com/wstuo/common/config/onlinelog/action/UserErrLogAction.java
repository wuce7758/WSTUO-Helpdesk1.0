package com.wstuo.common.config.onlinelog.action;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.wstuo.common.config.onlinelog.dto.UserErrLogDTO;
import com.wstuo.common.config.onlinelog.service.IUserErrLogService;
import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.security.action.SupplierAction;
import com.wstuo.common.security.utils.AppConfigUtils;
import com.wstuo.common.util.TimeUtils;
/**
 * 用户错误日志Action
 * @author Will
 *
 */
@SuppressWarnings("serial")
public class UserErrLogAction extends SupplierAction {
	@Autowired
	private IUserErrLogService userErrLogService;
	private UserErrLogDTO userErrLogDTO=new UserErrLogDTO();
	private PageDTO pageDTO=new PageDTO();
	private InputStream exportStream;
	private String fileName="";
	private String[] fileNames;
	private String sidx;
    private String sord;
	private int page = 1;
    private int rows = 10;
    private String startTime;
    private String endTime;
    
	private static final Logger LOGGER = Logger.getLogger(UserErrLogAction.class );    

	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	public String getSidx() {
		return sidx;
	}
	public void setSidx(String sidx) {
		this.sidx = sidx;
	}
	public String getSord() {
		return sord;
	}
	public void setSord(String sord) {
		this.sord = sord;
	}
	public UserErrLogDTO getUserErrLogDTO() {
		return userErrLogDTO;
	}
	public void setUserErrLogDTO(UserErrLogDTO userErrLogDTO) {
		this.userErrLogDTO = userErrLogDTO;
	}
	public PageDTO getPageDTO() {
		return pageDTO;
	}
	public void setPageDTO(PageDTO pageDTO) {
		this.pageDTO = pageDTO;
	}

	public InputStream getExportStream() {
		return exportStream;
	}
	public void setExportStream(InputStream exportStream) {
		this.exportStream = exportStream;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public int getRows() {
		return rows;
	}
	public void setRows(int rows) {
		this.rows = rows;
	}
	
	public String[] getFileNames() {
		return fileNames;
	}
	public void setFileNames(String[] fileNames) {
		this.fileNames = fileNames;
	}
	/**
	 * 查询用户错误日志列表
	 * @return String
	 */
	public String findUserErrLogPager(){
		int start = ( page - 1 ) * rows;
		pageDTO=userErrLogService.findUserErrLogPager(userErrLogDTO, start, rows,sidx,sord);
		return SUCCESS;
	}
	/**
	 * 保存用户错误日志
	 * @return String
	 */
	public String saveUserErrLog(){
		userErrLogService.saveUserErrLog(userErrLogDTO);
		return SUCCESS;
	}
	
	/**
	 * 导出用户错误日志列表
	 * @return String
	 */
	public String exportUserErrLog(){
	    String ERROR_LOG_PATH=AppConfigUtils.getInstance().getErrorLogPath();
        String fileName=ERROR_LOG_PATH+"/errorLog"+TimeUtils.format(new Date(), "yyyy_MM_dd_HH_mm_ss")+".csv";
        userErrLogService.exportUserErrLog(userErrLogDTO, 0, 10000,fileName);
        return SUCCESS;
	}
	/**
 	 * 取得错误日志文件列表.
 	 * @return String
 	 */
 	public String showAllErrLogFiles(){
 	   String ERRLOGPATH=AppConfigUtils.getInstance().getErrLogPath();  //错误日志
 	   pageDTO = userErrLogService.showAllErrLogFiles(page, rows, ERRLOGPATH, fileName, startTime, endTime);
 	   return SUCCESS;
 	}
 	/**
	 * 删除文件
	 */
	public String deleteErrorLog(){
	    String ERRLOGPATH=AppConfigUtils.getInstance().getErrLogPath();  //错误日志
		userErrLogService.deleteErrorLog(fileNames, ERRLOGPATH);
		return SUCCESS;
	}
	
	/**
	 * 下载错误日志文件.
	 */
	public String download(){
		try {
		    String ERRLOGPATH=AppConfigUtils.getInstance().getErrLogPath();  //错误日志
			exportStream = new FileInputStream(new File(ERRLOGPATH+"/"+fileName));
		} catch (FileNotFoundException e) {
			LOGGER.error("User ErrLog download", e);
		}
		return "exportFileSuccessful";
		
	}

}
