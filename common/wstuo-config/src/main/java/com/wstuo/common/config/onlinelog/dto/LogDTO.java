package com.wstuo.common.config.onlinelog.dto;

import com.wstuo.common.dto.AnnotationPropertyDTO;

@AnnotationPropertyDTO(id="id")
public class LogDTO {
	private int id;
	private String fileName;
	private String time;
	private String fileSize;
	private String url;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getFileSize() {
		return fileSize;
	}
	public void setFileSize(String fileSize) {
		this.fileSize = fileSize;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
}
