package com.wstuo.common.config.onlinelog.dto;

/***
 * UserOptLogDTO class.
 * 
 * @author spring date 2010-10-11
 */
public class UserErrLogDTO extends UserOptLogDTO {
	private String errMsg;
	private String errCause;
	public String getErrMsg() {
		return errMsg;
	}
	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}
	public String getErrCause() {
		return errCause;
	}
	public void setErrCause(String errCause) {
		this.errCause = errCause;
	}
}
