package com.wstuo.common.config.refreshmq;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.Map;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;
import javax.jms.Session;

import org.apache.activemq.command.ActiveMQObjectMessage;
import org.apache.log4j.Logger;
import org.springframework.jms.support.converter.MessageConversionException;
import org.springframework.jms.support.converter.MessageConverter;

import com.wstuo.common.config.backup.dto.BackupFileDTO;


/**
 * 发送消息
 * @author WSTUO
 *
 */
public class RefreshMessageConverter implements MessageConverter {
	private static final Logger LOGGER = Logger.getLogger(RefreshMessageConverter.class );
	@SuppressWarnings({ "rawtypes" })
	public Object fromMessage(Message msg) throws JMSException,
			MessageConversionException {
		Object obj = null ;
		if (msg instanceof ObjectMessage) {
			Map map = (HashMap) ((ObjectMessage) msg)
					.getObjectProperty("Map");
			ByteArrayInputStream bis = null;
			ObjectInputStream ois = null;
			try {
				// Order,Order,Product must implements Seralizable
				bis = new ByteArrayInputStream(
						(byte[]) map.get("User"));
				ois = new ObjectInputStream(bis);
				obj = ois.readObject();
			} catch (IOException e) {
				LOGGER.error("Refresh Message Converter IOException", e);
			} catch (ClassNotFoundException e) {
				LOGGER.error("Refresh Message Converter ClassNotFoundException", e);
			}finally {
				try {
					if(bis!=null){
						bis.close();
					}
					if(ois!=null){
						ois.close();
					}
				} catch (IOException e) {
					LOGGER.error(e);
				}
			}
			return obj;
		} else {
			throw new JMSException("Msg:[" + msg + "] is not Map");
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
    public Message toMessage(Object obj, Session session) throws JMSException,
			MessageConversionException {
		System.err.println("RefreshMessageConverter");
		// check Type
		if(obj instanceof BackupFileDTO) {
			ActiveMQObjectMessage objMsg = (ActiveMQObjectMessage) session
					.createObjectMessage();
			Map map = new HashMap();
			ByteArrayOutputStream bos  = null;
			ObjectOutputStream oos= null;
			try {
				// Order,Order,Product must implements Seralizable
				bos = new ByteArrayOutputStream();
				oos = new ObjectOutputStream(bos);
				oos.writeObject(obj);
				
				map.put("User", bos.toByteArray());
				objMsg.setObjectProperty("Map", map);
			} catch (IOException e) {
				LOGGER.error("toMessage IOException", e);
			} finally {
				try {
					if(oos!=null){
						oos.close();
					}
					if(bos!=null){
						bos.close();
					}
				} catch (IOException e) {
					LOGGER.error(e);
				}
				
			}
			return objMsg;
		} else {
			throw new JMSException("Object:[" + obj + "] is not User");
		}
	}

}
