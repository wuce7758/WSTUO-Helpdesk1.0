package com.wstuo.common.config.systeminfo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.wstuo.common.config.systeminfo.dao.ISystemInfoDAO;
import com.wstuo.common.config.systeminfo.entity.SystemInfo;

public class SystemInfoService implements ISystemInfoService {

	@Autowired
	private ISystemInfoDAO systemInfoDAO;
	@Transactional
	public void initSystemInfoData(String languageVersion) {
		// TODO Auto-generated method stub
		SystemInfo systemInfo = new SystemInfo();
		systemInfo.setSystemDataVer(languageVersion);
		systemInfoDAO.save(systemInfo);
	}

}
