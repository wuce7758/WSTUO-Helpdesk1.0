package com.wstuo.common.config.template.dto;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 请求模板DTO
 * @author WSTUO
 *
 */

@SuppressWarnings("serial")
public class TemplateRequestDTO {
	private Long companyNo;
	private String companyName;
	private String etitle;
    private String edesc;
    private Long ecategoryNo;
    private String ecategoryName;
    private Long requestCategoryNo;
    private String requestCategoryName;
    private Long imodeNo;
    private Long priorityNo;
    private Long levelNo;
    private Long effectRangeNo;
    private Long seriousnessNo;
    private Long createdByNo;
    private String createdByName;
    private String creator;
    private Long ciId;
    private String ciname;
    private String cino;
    private String ciCategoryName;
    private String ciStatus;
    private String attachmentStr;
    private Date overdueTime;
	private Map<String,String> attrVals =new HashMap<String, String>();
	private String effect;
	private String onLinePlan;
	private String returnPlan;
	private String checklist;
	private String assigneGroupName;
	private Long technicianNo;
	private String technicianName;
	private Date planStartTime;
	private Date planEndTime;
	private Long orgNo;
	private String model;
	private String serialNumber;
	private String barcode;
	private Long statusId;
	private Long brandId;
	private Long providerId;
	private Long locId;
	private Date buyDate;
	private Date arrivalDate;
	private Date warningDate;
	private String poNo;
	private Double assetsOriginalValue;
	private Boolean financeCorrespond = false;
	private String department;
	private String workNumber;
	private String project;
	private String sourceUnits;
	private Integer lifeCycle;
	private Integer warranty;
	private Date wasteTime;
	private Date borrowedTime;
	private Date expectedRecoverTime;
	private Date recoverTime;
	private String usePermissions;
	private String originalUser;
	private String userName;
	private String owner;
	private String CDI;
	private String title;
	private String knowledgeStatus;
	private Long serviceDirectoryItemNo;
	private String keyWords;
	private String content;
	private Long categoryNo;
	private String categoryName;
	private String subServiceName;
	private List<Long> knowledServiceNo;
	private Map<Long,String> serviceNos=new HashMap<Long, String>();
	//服务目录项树
    private Map<Long,String> knowledgeServiceLi=new HashMap<Long, String>();
	private String ciName;//请求管理配置的名称
	
	private List<Long> relatedCiNos;//关联配置项编号
	private Map<Long,Long> scores; //服务分值
	
	private String softSetingParam;
	private String softConfigureAm;
	private String softRemark1;
	private String softRemark2;
	private String softRemark3;
	private Integer depreciationIsZeroYears;
	private Long systemPlatformId;
	private Long formId = 0L;//表单Id
	
	private Long locationNos;
    private String locationName;//地点
    
	public Long getLocationNos() {
		return locationNos;
	}
	public void setLocationNos(Long locationNos) {
		this.locationNos = locationNos;
	}
	public String getLocationName() {
		return locationName;
	}
	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}
	public Long getFormId() {
		return formId;
	}
	public void setFormId(Long formId) {
		this.formId = formId;
	}
	public Integer getDepreciationIsZeroYears() {
		return depreciationIsZeroYears;
	}
	public void setDepreciationIsZeroYears(Integer depreciationIsZeroYears) {
		this.depreciationIsZeroYears = depreciationIsZeroYears;
	}
	public String getSoftSetingParam() {
		return softSetingParam;
	}
	public void setSoftSetingParam(String softSetingParam) {
		this.softSetingParam = softSetingParam;
	}
	public String getSoftConfigureAm() {
		return softConfigureAm;
	}
	public void setSoftConfigureAm(String softConfigureAm) {
		this.softConfigureAm = softConfigureAm;
	}
	public String getSoftRemark1() {
		return softRemark1;
	}
	public void setSoftRemark1(String softRemark1) {
		this.softRemark1 = softRemark1;
	}
	public String getSoftRemark2() {
		return softRemark2;
	}
	public void setSoftRemark2(String softRemark2) {
		this.softRemark2 = softRemark2;
	}
	public String getSoftRemark3() {
		return softRemark3;
	}
	public void setSoftRemark3(String softRemark3) {
		this.softRemark3 = softRemark3;
	}
	public Map<Long, Long> getScores() {
		return scores;
	}
	public void setScores(Map<Long, Long> scores) {
		this.scores = scores;
	}
	public List<Long> getRelatedCiNos() {
		return relatedCiNos;
	}
	public void setRelatedCiNos(List<Long> relatedCiNos) {
		this.relatedCiNos = relatedCiNos;
	}
	public String getCiName() {
		return ciName;
	}
	public void setCiName(String ciName) {
		this.ciName = ciName;
	}
	public Map<Long, String> getServiceNos() {
		return serviceNos;
	}
	public void setServiceNos(Map<Long, String> serviceNos) {
		this.serviceNos = serviceNos;
	}
	public Long getRequestCategoryNo() {
		return requestCategoryNo;
	}
	public void setRequestCategoryNo(Long requestCategoryNo) {
		this.requestCategoryNo = requestCategoryNo;
	}
	public List<Long> getKnowledServiceNo() {
		return knowledServiceNo;
	}
	public void setKnowledServiceNo(List<Long> knowledServiceNo) {
		this.knowledServiceNo = knowledServiceNo;
	}
	public Map<Long, String> getKnowledgeServiceLi() {
		return knowledgeServiceLi;
	}
	public void setKnowledgeServiceLi(Map<Long, String> knowledgeServiceLi) {
		this.knowledgeServiceLi = knowledgeServiceLi;
	}
	public String getSubServiceName() {
		return subServiceName;
	}
	public void setSubServiceName(String subServiceName) {
		this.subServiceName = subServiceName;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public String getRequestCategoryName() {
		return requestCategoryName;
	}
	public void setRequestCategoryName(String requestCategoryName) {
		this.requestCategoryName = requestCategoryName;
	}
	public Long getCategoryNo() {
		return categoryNo;
	}
	public void setCategoryNo(Long categoryNo) {
		this.categoryNo = categoryNo;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getKnowledgeStatus() {
		return knowledgeStatus;
	}
	public void setKnowledgeStatus(String knowledgeStatus) {
		this.knowledgeStatus = knowledgeStatus;
	}
	public Long getServiceDirectoryItemNo() {
		return serviceDirectoryItemNo;
	}
	public void setServiceDirectoryItemNo(Long serviceDirectoryItemNo) {
		this.serviceDirectoryItemNo = serviceDirectoryItemNo;
	}
	public String getKeyWords() {
		return keyWords;
	}
	public void setKeyWords(String keyWords) {
		this.keyWords = keyWords;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public String getSerialNumber() {
		return serialNumber;
	}
	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}
	public String getBarcode() {
		return barcode;
	}
	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}
	public Long getStatusId() {
		return statusId;
	}
	public void setStatusId(Long statusId) {
		this.statusId = statusId;
	}
	public Long getBrandId() {
		return brandId;
	}
	public void setBrandId(Long brandId) {
		this.brandId = brandId;
	}
	public Long getProviderId() {
		return providerId;
	}
	public void setProviderId(Long providerId) {
		this.providerId = providerId;
	}
	public Long getLocId() {
		return locId;
	}
	public void setLocId(Long locId) {
		this.locId = locId;
	}
	public Date getBuyDate() {
		return buyDate;
	}
	public void setBuyDate(Date buyDate) {
		this.buyDate = buyDate;
	}
	public Date getArrivalDate() {
		return arrivalDate;
	}
	public void setArrivalDate(Date arrivalDate) {
		this.arrivalDate = arrivalDate;
	}
	public Date getWarningDate() {
		return warningDate;
	}
	public void setWarningDate(Date warningDate) {
		this.warningDate = warningDate;
	}
	public String getPoNo() {
		return poNo;
	}
	public void setPoNo(String poNo) {
		this.poNo = poNo;
	}
	public Double getAssetsOriginalValue() {
		return assetsOriginalValue;
	}
	public void setAssetsOriginalValue(Double assetsOriginalValue) {
		this.assetsOriginalValue = assetsOriginalValue;
	}
	public Boolean getFinanceCorrespond() {
		return financeCorrespond;
	}
	public void setFinanceCorrespond(Boolean financeCorrespond) {
		this.financeCorrespond = financeCorrespond;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public String getWorkNumber() {
		return workNumber;
	}
	public void setWorkNumber(String workNumber) {
		this.workNumber = workNumber;
	}
	public String getProject() {
		return project;
	}
	public void setProject(String project) {
		this.project = project;
	}
	public String getSourceUnits() {
		return sourceUnits;
	}
	public void setSourceUnits(String sourceUnits) {
		this.sourceUnits = sourceUnits;
	}
	public Integer getLifeCycle() {
		return lifeCycle;
	}
	public void setLifeCycle(Integer lifeCycle) {
		this.lifeCycle = lifeCycle;
	}
	public Integer getWarranty() {
		return warranty;
	}
	public void setWarranty(Integer warranty) {
		this.warranty = warranty;
	}
	public Date getWasteTime() {
		return wasteTime;
	}
	public void setWasteTime(Date wasteTime) {
		this.wasteTime = wasteTime;
	}
	public Date getBorrowedTime() {
		return borrowedTime;
	}
	public void setBorrowedTime(Date borrowedTime) {
		this.borrowedTime = borrowedTime;
	}
	public Date getExpectedRecoverTime() {
		return expectedRecoverTime;
	}
	public void setExpectedRecoverTime(Date expectedRecoverTime) {
		this.expectedRecoverTime = expectedRecoverTime;
	}
	public Date getRecoverTime() {
		return recoverTime;
	}
	public void setRecoverTime(Date recoverTime) {
		this.recoverTime = recoverTime;
	}
	public String getUsePermissions() {
		return usePermissions;
	}
	public void setUsePermissions(String usePermissions) {
		this.usePermissions = usePermissions;
	}
	public String getOriginalUser() {
		return originalUser;
	}
	public void setOriginalUser(String originalUser) {
		this.originalUser = originalUser;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getOwner() {
		return owner;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}
	public String getCDI() {
		return CDI;
	}
	public void setCDI(String cDI) {
		CDI = cDI;
	}
	public String getEffect() {
		return effect;
	}
	public void setEffect(String effect) {
		this.effect = effect;
	}
	public String getOnLinePlan() {
		return onLinePlan;
	}
	public void setOnLinePlan(String onLinePlan) {
		this.onLinePlan = onLinePlan;
	}
	public String getReturnPlan() {
		return returnPlan;
	}
	public void setReturnPlan(String returnPlan) {
		this.returnPlan = returnPlan;
	}
	public String getChecklist() {
		return checklist;
	}
	public void setChecklist(String checklist) {
		this.checklist = checklist;
	}
	public String getAssigneGroupName() {
		return assigneGroupName;
	}
	public void setAssigneGroupName(String assigneGroupName) {
		this.assigneGroupName = assigneGroupName;
	}
	public Long getTechnicianNo() {
		return technicianNo;
	}
	public void setTechnicianNo(Long technicianNo) {
		this.technicianNo = technicianNo;
	}
	public String getTechnicianName() {
		return technicianName;
	}
	public void setTechnicianName(String technicianName) {
		this.technicianName = technicianName;
	}
	public Date getPlanStartTime() {
		return planStartTime;
	}
	public void setPlanStartTime(Date planStartTime) {
		this.planStartTime = planStartTime;
	}
	public Date getPlanEndTime() {
		return planEndTime;
	}
	public void setPlanEndTime(Date planEndTime) {
		this.planEndTime = planEndTime;
	}
	public Long getOrgNo() {
		return orgNo;
	}
	public void setOrgNo(Long orgNo) {
		this.orgNo = orgNo;
	}
	public String getCino() {
		return cino;
	}
	public void setCino(String cino) {
		this.cino = cino;
	}
	public String getCiCategoryName() {
		return ciCategoryName;
	}
	public void setCiCategoryName(String ciCategoryName) {
		this.ciCategoryName = ciCategoryName;
	}
	public String getCiStatus() {
		return ciStatus;
	}
	public void setCiStatus(String ciStatus) {
		this.ciStatus = ciStatus;
	}
	public Date getOverdueTime() {
		return overdueTime;
	}
	public void setOverdueTime(Date overdueTime) {
		this.overdueTime = overdueTime;
	}
	public String getEtitle() {
		return etitle;
	}
	public void setEtitle(String etitle) {
		this.etitle = etitle;
	}
	public String getEdesc() {
		return edesc;
	}
	public void setEdesc(String edesc) {
		this.edesc = edesc;
	}
	public Long getEcategoryNo() {
		return ecategoryNo;
	}
	public void setEcategoryNo(Long ecategoryNo) {
		this.ecategoryNo = ecategoryNo;
	}
	public String getEcategoryName() {
		return ecategoryName;
	}
	public void setEcategoryName(String ecategoryName) {
		this.ecategoryName = ecategoryName;
	}
	public Long getImodeNo() {
		return imodeNo;
	}
	public void setImodeNo(Long imodeNo) {
		this.imodeNo = imodeNo;
	}
	public Long getPriorityNo() {
		return priorityNo;
	}
	public void setPriorityNo(Long priorityNo) {
		this.priorityNo = priorityNo;
	}
	public Long getLevelNo() {
		return levelNo;
	}
	public void setLevelNo(Long levelNo) {
		this.levelNo = levelNo;
	}
	public Long getEffectRangeNo() {
		return effectRangeNo;
	}
	public void setEffectRangeNo(Long effectRangeNo) {
		this.effectRangeNo = effectRangeNo;
	}
	public Long getSeriousnessNo() {
		return seriousnessNo;
	}
	public void setSeriousnessNo(Long seriousnessNo) {
		this.seriousnessNo = seriousnessNo;
	}
	public Long getCreatedByNo() {
		return createdByNo;
	}
	public void setCreatedByNo(Long createdByNo) {
		this.createdByNo = createdByNo;
	}

	public String getCreatedByName() {
		return createdByName;
	}
	public void setCreatedByName(String createdByName) {
		this.createdByName = createdByName;
	}
	public String getCiname() {
		return ciname;
	}
	public void setCiname(String ciname) {
		this.ciname = ciname;
	}
	public Long getCiId() {
		return ciId;
	}
	public void setCiId(Long ciId) {
		this.ciId = ciId;
	}

	public String getAttachmentStr() {
		return attachmentStr;
	}
	public void setAttachmentStr(String attachmentStr) {
		this.attachmentStr = attachmentStr;
	}
	public Map<String, String> getAttrVals() {
		return attrVals;
	}
	public void setAttrVals(Map<String, String> attrVals) {
		this.attrVals = attrVals;
	}
	public Long getCompanyNo() {
		return companyNo;
	}
	public void setCompanyNo(Long companyNo) {
		this.companyNo = companyNo;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getCreator() {
		return creator;
	}
	public void setCreator(String creator) {
		this.creator = creator;
	}
	public Long getSystemPlatformId() {
		return systemPlatformId;
	}
	public void setSystemPlatformId(Long systemPlatformId) {
		this.systemPlatformId = systemPlatformId;
	}
	
}
