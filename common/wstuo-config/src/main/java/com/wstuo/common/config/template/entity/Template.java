package com.wstuo.common.config.template.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;

import com.wstuo.common.entity.BaseEntity;

/**
 * 内容模板实体类
 * @author WSTUO
 *
 */
@SuppressWarnings({ "serial", "rawtypes" })
@Entity
public class Template extends BaseEntity{
	//规则ID
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	private Long templateId;//Id
	@Column(nullable=false)
	private String templateName;//模板名称
	@Column(nullable=false)
	private String templateType;//模板类型
	@Lob
	private String templateJson;//模板详细信息
	private Long formId;//表单Id
	private String isShowBorder;
	private Boolean isNewForm;
	private Long serviceDirId;//服务目录id
	private Long categoryNo;//配置项分类
	
	
	
	public Long getCategoryNo() {
		return categoryNo;
	}
	public void setCategoryNo(Long categoryNo) {
		this.categoryNo = categoryNo;
	}
	public Long getServiceDirId() {
		return serviceDirId;
	}
	public void setServiceDirId(Long serviceDirId) {
		this.serviceDirId = serviceDirId;
	}
	public Boolean getIsNewForm() {
		return isNewForm;
	}
	public void setIsNewForm(Boolean isNewForm) {
		this.isNewForm = isNewForm;
	}
	public String getIsShowBorder() {
		return isShowBorder;
	}
	public void setIsShowBorder(String isShowBorder) {
		this.isShowBorder = isShowBorder;
	}
	public Long getFormId() {
		return formId;
	}
	public void setFormId(Long formId) {
		this.formId = formId;
	}
	public String getTemplateName() {
		return templateName;
	}
	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}
	public Long getTemplateId() {
		return templateId;
	}
	public void setTemplateId(Long templateId) {
		this.templateId = templateId;
	}
	public String getTemplateType() {
		return templateType;
	}
	public void setTemplateType(String templateType) {
		this.templateType = templateType;
	}
	public String getTemplateJson() {
		return templateJson;
	}
	public void setTemplateJson(String templateJson) {
		this.templateJson = templateJson;
	}
}
