package com.wstuo.common.config.updatelevel.dto;

import com.wstuo.common.dto.BaseDTO;



/**
 * DTO of UpdateLevel Query
 * @author QXY
 *
 */
@SuppressWarnings("serial")
public class UpdateLevelQueryDTO extends BaseDTO{

	private String ulName;
	private Long approvalNo=0L;
    private Integer start;
    private Integer limit;
	private String sord;
	private String sidx;
    
    
	public String getSord() {
		return sord;
	}
	public void setSord(String sord) {
		this.sord = sord;
	}
	public String getSidx() {
		return sidx;
	}
	public void setSidx(String sidx) {
		this.sidx = sidx;
	}
	public String getUlName() {
		return ulName;
	}
	public void setUlName(String ulName) {
		this.ulName = ulName;
	}
	public Long getApprovalNo() {
		return approvalNo;
	}
	public void setApprovalNo(Long approvalNo) {
		this.approvalNo = approvalNo;
	}
	public Integer getStart() {
		return start;
	}
	public void setStart(Integer start) {
		this.start = start;
	}
	public Integer getLimit() {
		return limit;
	}
	public void setLimit(Integer limit) {
		this.limit = limit;
	}

	
}
