package com.wstuo.common.config.userCustom.action;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.xwork2.ActionSupport;
import com.wstuo.common.config.userCustom.dto.UserCustomDTO;
import com.wstuo.common.config.userCustom.dto.UserCustomQueryDTO;
import com.wstuo.common.config.userCustom.service.IUserCustomService;
import com.wstuo.common.dto.PageDTO;

/**
 * 自定义Action
 * @author WSTUO
 *
 */
@SuppressWarnings("serial")
public class UserCustomAction extends ActionSupport {
	@Autowired
	private IUserCustomService userCustomService;
	private PageDTO pageDTO;
	private UserCustomDTO  userCustomDTO;
	private UserCustomQueryDTO queryDTO;
	private Long[] ids;
	private List<UserCustomDTO> lists=new ArrayList<UserCustomDTO>();
	private String sidx;
	private String sord;
	private int page = 1;
    private int rows = 10;
	public UserCustomDTO getUserCustomDTO() {
		return userCustomDTO;
	}
	public void setUserCustomDTO(UserCustomDTO userCustomDTO) {
		this.userCustomDTO = userCustomDTO;
	}
	public UserCustomQueryDTO getQueryDTO() {
		return queryDTO;
	}
	public void setQueryDTO(UserCustomQueryDTO queryDTO) {
		this.queryDTO = queryDTO;
	}
	public Long[] getIds() {
		return ids;
	}
	public void setIds(Long[] ids) {
		this.ids = ids;
	}
	public List<UserCustomDTO> getLists() {
		return lists;
	}
	public void setLists(List<UserCustomDTO> lists) {
		this.lists = lists;
	}
	
	public PageDTO getPageDTO() {
		return pageDTO;
	}
	public void setPageDTO(PageDTO pageDTO) {
		this.pageDTO = pageDTO;
	}
	public String getSidx() {
		return sidx;
	}
	public void setSidx(String sidx) {
		this.sidx = sidx;
	}
	public String getSord() {
		return sord;
	}
	public void setSord(String sord) {
		this.sord = sord;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public int getRows() {
		return rows;
	}
	public void setRows(int rows) {
		this.rows = rows;
	}
	/**
	 * 用户自定义查询
	 * @return String
	 */
	public String findUserCustom(){
		lists=userCustomService.findUserCustom(queryDTO);
		return SUCCESS;
	}
	
	/**
	 * 查询用户自定义
	 * @return String
	 */
	public String findPagerUserCustom(){
		int start = ( page - 1 ) * rows;
		queryDTO.setStart(start);
		queryDTO.setLimit(rows);
		queryDTO.setSidx(sidx);
		queryDTO.setSord(sord);
		pageDTO=userCustomService.findPagerUserCustom(queryDTO);
		pageDTO.setPage( page );
		pageDTO.setRows( rows );
		return "pageDTO";
	}
	
	/**
	 * 保存自定义
	 * @return String
	 */
	public String saveUserCustom(){
		userCustomService.saveUserCustom(userCustomDTO);
		return SUCCESS;
	}
	/**
	 * 编辑自定义
	 * @return String
	 */
	public String editUserCustom(){
		userCustomService.editUserCustom(userCustomDTO);
		return SUCCESS;
	}
	/**
	 * 删除自定义
	 * @return String
	 */
	public String deleteUserCustom(){
		userCustomService.deleteUserCustom(ids);
		return SUCCESS;
	}
	
	/**
	 * 更新自定义视图
	 * @return String
	 */
	public String updateUserCustomView(){
		userCustomService.updateCustomView(userCustomDTO);
		return SUCCESS;
	}
	
	/**
	 * 更新并保存门户面板自定义结果
	 * @return String
	 */
	public String dashboardCustomSet(){
		userCustomService.dashboardCustomSet(userCustomDTO);
		return SUCCESS;
	}
	/**
	 * 查找我的自定义
	 * @return String
	 */
	public String findUserCustomByLoginNameAndType(){
		userCustomDTO=userCustomService.findUserCustomByLoginNameAndType(queryDTO);
		return "userCustomDTO";
	}
	
	/**
	 * 加载门户
	 * @return String
	 */
	public String portalDashboard(){
		userCustomDTO=userCustomService.findUserCustomByLoginNameAndType(queryDTO);
		return "portalPage";
	}
	
	/**
	 * 加载门户预览
	 * @return String
	 */
	public String portalDashboardPreview(){
		userCustomDTO=userCustomService.findUserCustomByLoginNameAndType(queryDTO);
		return "portalDashboardPage";
	}
	
	/**
	 * 门户面板布局
	 * @return String
	 */
	public String dashboardLayoutSet(){
		userCustomService.dashboardLayoutSet(userCustomDTO);
		return SUCCESS;
	}
	
}
