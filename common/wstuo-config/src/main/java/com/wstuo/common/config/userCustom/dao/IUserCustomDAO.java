package com.wstuo.common.config.userCustom.dao;

import java.util.List;

import com.wstuo.common.config.userCustom.dto.UserCustomQueryDTO;
import com.wstuo.common.config.userCustom.entity.UserCustom;
import com.wstuo.common.dao.IEntityDAO;
import com.wstuo.common.dto.PageDTO;
/**
 * 用户自定义DAO接口类
 * @author WSTUO
 *
 */
public interface IUserCustomDAO extends IEntityDAO<UserCustom> {
	List<UserCustom> findUserCustom(final UserCustomQueryDTO queryDTO);
	
	/**
	 * 分页查询
	 * @param queryDTO
	 * @return PageDTO
	 */
	PageDTO findPagerUserCustom(final UserCustomQueryDTO queryDTO);
}
