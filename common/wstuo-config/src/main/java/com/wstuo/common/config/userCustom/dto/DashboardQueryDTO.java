package com.wstuo.common.config.userCustom.dto;

/**
 * 面板查询DTO类
 * @author WSTUO
 *
 */
public class DashboardQueryDTO {
	private Long dashboardId;
	private String dashboardName;//面板名称
	private String dashboardDataLoadUrl;//面板加载数据URL
	private String dashboardDivId;//面板DIV ID
	private Long sortNo;//面板排序NO
	private Long defaultShow;//1表示默认显示
	private String sord;
	private String sidx;
	private Integer start;
	private Integer limit;
	public Long getDashboardId() {
		return dashboardId;
	}
	public void setDashboardId(Long dashboardId) {
		this.dashboardId = dashboardId;
	}
	public String getDashboardName() {
		return dashboardName;
	}
	public void setDashboardName(String dashboardName) {
		this.dashboardName = dashboardName;
	}
	public String getDashboardDataLoadUrl() {
		return dashboardDataLoadUrl;
	}
	public void setDashboardDataLoadUrl(String dashboardDataLoadUrl) {
		this.dashboardDataLoadUrl = dashboardDataLoadUrl;
	}
	public String getDashboardDivId() {
		return dashboardDivId;
	}
	public void setDashboardDivId(String dashboardDivId) {
		this.dashboardDivId = dashboardDivId;
	}
	public Long getSortNo() {
		return sortNo;
	}
	public void setSortNo(Long sortNo) {
		this.sortNo = sortNo;
	}
	public Long getDefaultShow() {
		return defaultShow;
	}
	public void setDefaultShow(Long defaultShow) {
		this.defaultShow = defaultShow;
	}
	public String getSord() {
		return sord;
	}
	public void setSord(String sord) {
		this.sord = sord;
	}
	public String getSidx() {
		return sidx;
	}
	public void setSidx(String sidx) {
		this.sidx = sidx;
	}
	public Integer getStart() {
		return start;
	}
	public void setStart(Integer start) {
		this.start = start;
	}
	public Integer getLimit() {
		return limit;
	}
	public void setLimit(Integer limit) {
		this.limit = limit;
	}
	
	
}
