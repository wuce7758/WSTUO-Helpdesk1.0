package com.wstuo.common.config.userCustom.dto;


import com.wstuo.common.dto.BaseDTO;

/**
 * 用户自定义DTO
 * @author WSTUO
 *
 */
@SuppressWarnings("serial")
public class UserCustomQueryDTO extends BaseDTO{
	private Long userCustomId;//ID
	private String loginName;//登录名
	private Long customType;//自定义类型.1:门户模块,2:自定义列
	private String eventType;//事件类型
	private String customResult;//自定义结果
	private String layoutType;//布局类型
	private String sord;
	private String sidx;
	private Integer start = 0;
	private Integer limit = 0;
	public Long getUserCustomId() {
		return userCustomId;
	}
	public void setUserCustomId(Long userCustomId) {
		this.userCustomId = userCustomId;
	}
	public String getLoginName() {
		return loginName;
	}
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	public Long getCustomType() {
		return customType;
	}
	public void setCustomType(Long customType) {
		this.customType = customType;
	}
	public String getEventType() {
		return eventType;
	}
	public void setEventType(String eventType) {
		this.eventType = eventType;
	}
	public String getCustomResult() {
		return customResult;
	}
	public void setCustomResult(String customResult) {
		this.customResult = customResult;
	}
	public String getSord() {
		return sord;
	}
	public void setSord(String sord) {
		this.sord = sord;
	}
	public String getSidx() {
		return sidx;
	}
	public void setSidx(String sidx) {
		this.sidx = sidx;
	}
	public Integer getStart() {
		return start;
	}
	public void setStart(Integer start) {
		this.start = start;
	}
	public Integer getLimit() {
		return limit;
	}
	public void setLimit(Integer limit) {
		this.limit = limit;
	}
	public String getLayoutType() {
		return layoutType;
	}
	public void setLayoutType(String layoutType) {
		this.layoutType = layoutType;
	}

	
}
