package com.wstuo.common.config.userCustom.service;


import java.util.List;

import com.wstuo.common.config.userCustom.dto.UserCustomDTO;
import com.wstuo.common.config.userCustom.dto.UserCustomQueryDTO;
import com.wstuo.common.dto.PageDTO;

/**
 * 用户自定义Service方法
 * @author WSTUO
 *
 */
public interface IUserCustomService {
	/**
	 * 用户自定义查询
	 * @param queryDTO
	 * @return List<UserCustomDTO>
	 */
	List<UserCustomDTO> findUserCustom(final UserCustomQueryDTO queryDTO);
	
	/**
	 * 分页查询
	 * @param queryDTO
	 * @return PageDTO
	 */
	PageDTO findPagerUserCustom(final UserCustomQueryDTO queryDTO);
	
	/**
	 * 保存用户自定义
	 * @param dto
	 */
	void saveUserCustom(UserCustomDTO dto);
	
	/**
	 * 编辑用户自定义
	 * @param dto
	 */
	void editUserCustom(UserCustomDTO dto);
	/**
	 *  更新自定义视图
	 * @param dto
	 */
	void updateCustomView(UserCustomDTO dto);
	/**
	 * 删除自定义
	 * @param ids
	 */
	void deleteUserCustom(Long[] ids);
	
	/**
	 * 更新并保存门户面板自定义结果
	 * @param dto
	 */
	void dashboardCustomSet(UserCustomDTO dto);
	
	/**
	 * 获取当前用户的自定义面板结果
	 * @param queryDTO
	 * @return UserCustomDTO
	 */
	UserCustomDTO findUserCustomByLoginNameAndType(final UserCustomQueryDTO queryDTO);
	
	/**
	 * 门户面板布局设置
	 * @param dto
	 */
	void dashboardLayoutSet(UserCustomDTO dto);
	
}
