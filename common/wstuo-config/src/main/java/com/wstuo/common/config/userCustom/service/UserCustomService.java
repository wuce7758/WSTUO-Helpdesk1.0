package com.wstuo.common.config.userCustom.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.wstuo.common.config.userCustom.comparator.ComparatorDashboardDTO;
import com.wstuo.common.config.userCustom.dao.IDashboardDAO;
import com.wstuo.common.config.userCustom.dao.IUserCustomDAO;
import com.wstuo.common.config.userCustom.dto.DashboardDTO;
import com.wstuo.common.config.userCustom.dto.UserCustomDTO;
import com.wstuo.common.config.userCustom.dto.UserCustomQueryDTO;
import com.wstuo.common.config.userCustom.entity.Dashboard;
import com.wstuo.common.config.userCustom.entity.UserCustom;
import com.wstuo.common.dto.PageDTO;
/**
 * 用户自定义Service方法
 * @author WSTUO
 *
 */
public class UserCustomService implements IUserCustomService {
	@Autowired
	private IUserCustomDAO userCustomDAO;
	@Autowired
	private IDashboardDAO dashboardDAO;
	@Autowired IDashboardService dashboardService;
	/**
	 * 全部
	 * @param queryDTO
	 * @return  List<UserCustomDTO>
	 */
	@Transactional
	public List<UserCustomDTO> findUserCustom(final UserCustomQueryDTO queryDTO){
		List<UserCustom> lists=userCustomDAO.findUserCustom(queryDTO);
		List<UserCustomDTO> list=new ArrayList<UserCustomDTO>(lists.size());
		
		for(UserCustom entity:lists){
			UserCustomDTO userCustomDTO=new UserCustomDTO();
			UserCustomDTO.entity2dto(entity, userCustomDTO);
			list.add(userCustomDTO);
		}
		return list;
	}
	/**
	 * 分页查询
	 * @param queryDTO
	 * @return PageDTO
	 */
	@Transactional
	public PageDTO findPagerUserCustom(final UserCustomQueryDTO queryDTO){
		PageDTO p=userCustomDAO.findPagerUserCustom(queryDTO);
		List<UserCustom> lists=(List<UserCustom>)p.getData();
		List<UserCustomDTO> list=new ArrayList<UserCustomDTO>(lists.size());
		
		for(UserCustom entity:lists){
			UserCustomDTO userCustomDTO=new UserCustomDTO();
			UserCustomDTO.entity2dto(entity, userCustomDTO);
			list.add(userCustomDTO);
		}
		p.setData(list);
		return p;
	}
	
	/**
	 * 保存用户自定义
	 * @param dto
	 */
	@Transactional
	public void saveUserCustom(UserCustomDTO dto){
		UserCustom entity=new UserCustom();
		UserCustomDTO.dto2entity(dto, entity);
		userCustomDAO.save(entity);
		dto.setUserCustomId(entity.getUserCustomId());
	}
	
	/**
	 * 编辑用户自定义
	 * @param dto
	 */
	@Transactional
	public void editUserCustom(UserCustomDTO dto){
		UserCustom entity=userCustomDAO.findById(dto.getUserCustomId());
		entity.setCustomResult(dto.getCustomResult());
		userCustomDAO.merge(entity);
	}
	
	/**
	 * 删除自定义
	 * @param ids
	 */
	@Transactional
	public void deleteUserCustom(Long[] ids){
		userCustomDAO.deleteByIds(ids);
	}
	
	/**
	 * 获取当前用户的自定义面板结果(门户)
	 * @param queryDTO
	 * @return UserCustomDTO
	 */
	@SuppressWarnings("unchecked")
	@Transactional
	public UserCustomDTO findUserCustomByLoginNameAndType(final UserCustomQueryDTO queryDTO){
		
		List<UserCustom> list=userCustomDAO.findUserCustom(queryDTO);
		UserCustom entity=new UserCustom();
		UserCustomDTO userCustomDTO=new UserCustomDTO();
		List<DashboardDTO> dashboardDTOs=new ArrayList<DashboardDTO>();
		if(list!=null && list.size()>0){
			entity=list.get(0);
			UserCustomDTO.entity2dto(entity, userCustomDTO);
			List<Dashboard> dashboards=entity.getDashboard();
			if(dashboards!=null && dashboards.size()>0){
				for(Dashboard d:dashboards){
					DashboardDTO dashboardDTO=new DashboardDTO();
					DashboardDTO.entity2dto(d, dashboardDTO);
					dashboardDTOs.add(dashboardDTO);
				}
				//对列表进行排序
		    	Collections.sort(dashboardDTOs,new ComparatorDashboardDTO());
			}
			userCustomDTO.setDashboards(dashboardDTOs);
		}else{
			for(DashboardDTO dto:dashboardService.findDashboardAll()){
				if(dto.getDefaultShow()==1){
					dashboardDTOs.add(dto);
				}
			}
			userCustomDTO.setDashboards(dashboardDTOs);
		}
		return userCustomDTO;
		
	}
	
	/**
	 * 门户面板布局设置，根据用户名和类型
	 * @param dto
	 */
	@Transactional
	public void dashboardLayoutSet(UserCustomDTO dto){
		UserCustomQueryDTO queryDTO=new UserCustomQueryDTO();
		queryDTO.setLoginName(dto.getLoginName());
		queryDTO.setCustomType(dto.getCustomType());
		queryDTO.setCustomResult(dto.getCustomResult());
		List<UserCustom> list=userCustomDAO.findUserCustom(queryDTO);
		UserCustom entity=new UserCustom();
		if(list!=null && list.size()>0){
			entity=list.get(0);
		}else{
			UserCustomDTO.dto2entity(dto, entity);
		}
		entity.setLayoutType(dto.getLayoutType());
		userCustomDAO.merge(entity);
	}
	
	/**
	 *   更新用户自定义视图
	 *   @param dto
	 */
	@Transactional
	public void updateCustomView(UserCustomDTO dto){
		UserCustomQueryDTO queryDTO=new UserCustomQueryDTO();
		queryDTO.setLoginName(dto.getLoginName());
		queryDTO.setCustomType(dto.getCustomType());
		List<UserCustom> list=userCustomDAO.findUserCustom(queryDTO);
		UserCustom entity=new UserCustom();
		if(list!=null && list.size()>0){
			entity = list.get(0);
			entity.setLayoutType(dto.getLayoutType());
			entity.setViewIdStr(dto.getViewIdStr());
			entity.setViewRowsStr(dto.getViewRowsStr());
			userCustomDAO.merge(entity);
		}else{
			UserCustomDTO.dto2entity(dto, entity);
			//保存一条
			userCustomDAO.save(entity);
		}		
	}
	
	
	/**
	 * 更新并保存门户面板自定义结果，根据用户名和类型
	 * @param dto
	 */
	@Transactional
	public void dashboardCustomSet(UserCustomDTO dto){
		UserCustomQueryDTO queryDTO=new UserCustomQueryDTO();
		queryDTO.setLoginName(dto.getLoginName());
		queryDTO.setCustomType(dto.getCustomType());
		queryDTO.setEventType(dto.getEventType());
		List<UserCustom> list=userCustomDAO.findUserCustom(queryDTO);
		List<Dashboard> dashboards=new ArrayList<Dashboard>();
		if(dto.getDashboardIds()!=null && dto.getDashboardIds().length>0){
			for(Long id:dto.getDashboardIds()){
				Dashboard dashboard=dashboardDAO.findById(id);
				dashboards.add(dashboard);
			}
		}
		UserCustom entity=new UserCustom();
		if(list!=null && list.size()>0){
			entity=list.get(0);
		}else{
			UserCustomDTO.dto2entity(dto, entity);
		}
		entity.setDashboard(dashboards);
		entity.setCustomResult(dto.getCustomResult());
		userCustomDAO.merge(entity);
	}
}
