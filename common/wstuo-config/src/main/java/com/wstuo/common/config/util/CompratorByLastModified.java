package com.wstuo.common.config.util;

import java.io.File;
import java.util.Comparator;

/**
 * 根据最后修改排序
 * @author QXY
 *
 */
public class CompratorByLastModified implements Comparator<File>{
	public int compare(File f1, File f2) { 
		int result = 0;
		long diff = f1.lastModified()-f2.lastModified();
		if (diff > 0)
			result = -1;// 倒序正序控制
		else if (diff == 0)
			result = 0;
		else
			result = 1;// 倒序正序控制
		return result;
	}  

}
