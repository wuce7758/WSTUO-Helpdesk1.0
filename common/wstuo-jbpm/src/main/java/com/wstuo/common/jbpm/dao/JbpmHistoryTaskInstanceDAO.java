package com.wstuo.common.jbpm.dao;

import org.hibernate.Session;
import org.jbpm.api.ProcessEngine;
import org.jbpm.api.cmd.Command;
import org.jbpm.api.cmd.Environment;
import org.jbpm.pvm.internal.history.model.HistoryTaskInstanceImpl;
import org.springframework.beans.factory.annotation.Autowired;

import com.wstuo.common.dao.BaseDAOImplHibernate;

public class JbpmHistoryTaskInstanceDAO  extends BaseDAOImplHibernate<HistoryTaskInstanceImpl>  implements IJbpmHistoryTaskInstanceDAO {
	@Autowired
	protected ProcessEngine processEngine;
	/**
	 * 根据TaskId获取历史任务 
	 * @param taskId
	 */
	public HistoryTaskInstanceImpl getHistoryTaskInstanceByTaskId(final String taskId){  
	  return processEngine.execute(new Command<HistoryTaskInstanceImpl>(){  
	    private static final long serialVersionUID = 1L;  
	    @Override  
	    public HistoryTaskInstanceImpl execute(Environment environment)  
	        throws Exception {  
	      Session session = environment.get(Session.class);  
	      StringBuilder hql = new StringBuilder();  
	      hql.append("select hti from ").append(HistoryTaskInstanceImpl.class.getName());  
	      hql.append(" as hti ");  
	      hql.append("where hti.historyTask.dbid = :taskDbid");  
	      return (HistoryTaskInstanceImpl) session.createQuery(hql.toString())  
	               .setLong("taskDbid", Long.valueOf(taskId)).uniqueResult();  
	    }  
	  });  
	}  
	

}
