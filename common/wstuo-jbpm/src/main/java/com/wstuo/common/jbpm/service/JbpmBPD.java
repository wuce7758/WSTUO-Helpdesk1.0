package com.wstuo.common.jbpm.service;

import java.io.File;
import java.io.InputStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.wstuo.common.bpm.api.IBPD;
import com.wstuo.common.bpm.dto.ProcessDefinitionDTO;
import com.wstuo.common.bpm.service.IFlowPropertyService;
import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.jbpm.IJbpmFacade;

/**
 * business process definition implement class
 * @author wstuo
 *
 */
public class JbpmBPD implements IBPD {

	@Autowired
	private IJbpmFacade jbpmFacade;
	@Autowired
	private IFlowPropertyService flowPropertyService;
	@Transactional
	public String deployProcessDefinitionFile(File file) {
		// TODO Auto-generated method stub
		return jbpmFacade.deployJpdl(file);
	}
	@Transactional
	public String deployProcessDefinitionFile(InputStream fis) {
		// TODO Auto-generated method stub
		return jbpmFacade.deployJpdl(fis);
	}
	@Transactional
	public String deployProcessDefinitionXmlFile(File jpdlfile) {
		// TODO Auto-generated method stub
		return jbpmFacade.deployJpdlXml(jpdlfile);
	}
	@Transactional
	public void unDeployProcessDefinition(String deploymentId) {
		// TODO Auto-generated method stub
		jbpmFacade.undeployJpdl(deploymentId);
		flowPropertyService.deleteFlowProperty(deploymentId);
	}
	
	public String findProcessDefinitionIdByDeploymentId(String deploymentId) {
		// TODO Auto-generated method stub
		return jbpmFacade.findProcessDefinitionIdByDeploymentId(deploymentId);
	}
	@Transactional
	public PageDTO findPageProcessDefinitions(ProcessDefinitionDTO qdto, int start, int limit,
			String sord, String sidx) {
		// TODO Auto-generated method stub
		return jbpmFacade.findLatestProcessDefinitions(qdto, start, limit,sord,sidx);
	}

}
