package com.wstuo.itsm.knowledge.dto;

import com.wstuo.common.config.onlinelog.dto.ADTO;
import com.wstuo.common.dto.BaseDTO;
/**
 * 知识库信息DTO类
 * @author Eileen
 */
@SuppressWarnings("serial")
@ADTO(id="kid")
public class KnowledgeCountResultDTO extends BaseDTO{

    private Integer shareCount;//共享知识
    private Integer myCount;//我发布的知识
    private Integer myapCount;//我的待审知识
    private Integer allapCount;//所有待审知识
    private Integer myapfCount;//我未审核通过的知识
    private Integer allapfCount;//所有未审核通过的知识
	public Integer getShareCount() {
		return shareCount;
	}
	public void setShareCount(Integer shareCount) {
		this.shareCount = shareCount;
	}
	public Integer getMyCount() {
		return myCount;
	}
	public void setMyCount(Integer myCount) {
		this.myCount = myCount;
	}
	public Integer getMyapCount() {
		return myapCount;
	}
	public void setMyapCount(Integer myapCount) {
		this.myapCount = myapCount;
	}
	public Integer getAllapCount() {
		return allapCount;
	}
	public void setAllapCount(Integer allapCount) {
		this.allapCount = allapCount;
	}
	public Integer getMyapfCount() {
		return myapfCount;
	}
	public void setMyapfCount(Integer myapfCount) {
		this.myapfCount = myapfCount;
	}
	public Integer getAllapfCount() {
		return allapfCount;
	}
	public void setAllapfCount(Integer allapfCount) {
		this.allapfCount = allapfCount;
	}
}