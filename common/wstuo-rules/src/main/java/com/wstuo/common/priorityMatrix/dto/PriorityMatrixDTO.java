package com.wstuo.common.priorityMatrix.dto;

/**
 * 优先级矩阵DTO类
 * @author WSTUO
 *
 */
public class PriorityMatrixDTO {
	private Long urgencyId ;//紧急度
	private Long affectId;//影响范围
	private Long priorityId;//优先级
	private Long priorityMatrixId;
	public Long getUrgencyId() {
		return urgencyId;
	}
	public void setUrgencyId(Long urgencyId) {
		this.urgencyId = urgencyId;
	}
	public Long getAffectId() {
		return affectId;
	}
	public void setAffectId(Long affectId) {
		this.affectId = affectId;
	}
	public Long getPriorityId() {
		return priorityId;
	}
	public void setPriorityId(Long priorityId) {
		this.priorityId = priorityId;
	}
	public Long getPriorityMatrixId() {
		return priorityMatrixId;
	}
	public void setPriorityMatrixId(Long priorityMatrixId) {
		this.priorityMatrixId = priorityMatrixId;
	}
	
}
