package com.wstuo.common.priorityMatrix.service;


import com.wstuo.common.priorityMatrix.dto.PriorityMatrixDTO;
import com.wstuo.common.priorityMatrix.dto.PriorityMatrixDataShowDTO;

/**
 * 优先级矩阵Service接口类
 * @author WSTUO
 *
 */
public interface IPriorityMatrixService {
	/**
	 * 保存优先级矩阵
	 * @param dto PriorityMatrixDTO
	 */
	void savePriorityMatrix(PriorityMatrixDTO dto);
	/**
	 * 获取优先级矩阵显示数据
	 * @return 返回优先级矩阵前台显示数据 PriorityMatrixDataShowDTO 
	 */
	PriorityMatrixDataShowDTO findPriorityMatrixDataShow();
}
