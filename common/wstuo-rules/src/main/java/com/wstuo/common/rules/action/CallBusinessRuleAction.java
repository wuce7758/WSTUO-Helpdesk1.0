package com.wstuo.common.rules.action;

import com.opensymphony.xwork2.ActionSupport;

import com.wstuo.common.rules.dto.RuleDTO;
import com.wstuo.common.rules.dto.RulePackageDTO;
import com.wstuo.common.rules.dto.RuleQueryDTO;
import com.wstuo.common.rules.service.IRulePackageService;
import com.wstuo.common.rules.service.IRuleService;
import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.exception.ApplicationException;

import org.apache.log4j.Logger;

import org.springframework.beans.factory.annotation.Autowired;

import java.io.File;
import java.io.InputStream;

/**
 * 请求业务规则Action类
 * 
 * @author QXY
 * 
 */
@SuppressWarnings("serial")
public class CallBusinessRuleAction extends ActionSupport {
	public static final Logger LOGGER = Logger.getLogger(CallBusinessRuleAction.class);
	private RulePackageDTO rulePackageDTO = new RulePackageDTO();
	private RuleDTO ruleDTO = new RuleDTO();
	private RuleQueryDTO ruleQueryDTO = new RuleQueryDTO();
	@Autowired
	private IRuleService ruleService;
	@Autowired
	private IRulePackageService rulePackageService;
	private int page = 1;
	private int rows = 10;
	private PageDTO pageDTO;
	private Long rulePackageNo;
	private Long[] ruleNos;
	private Boolean result;
	private String sidx;
	private String sord;
	private String effect;
	private File importFile;
	private InputStream exportFile;
	private String fileName;
	private String ruleName;
	private String packageName;
	private String flagName;

	public String getRuleName() {
		return ruleName;
	}

	public void setRuleName(String ruleName) {
		this.ruleName = ruleName;
	}

	public String getFlagName() {
		return flagName;
	}

	public void setFlagName(String flagName) {
		this.flagName = flagName;
	}

	public String getPackageName() {
		return packageName;
	}

	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getEffect() {
		return effect;
	}

	public void setEffect(String effect) {
		this.effect = effect;
	}

	public File getImportFile() {
		return importFile;
	}

	public void setImportFile(File importFile) {
		this.importFile = importFile;
	}

	public InputStream getExportFile() {
		return exportFile;
	}

	public void setExportFile(InputStream exportFile) {
		this.exportFile = exportFile;
	}

	public String getSidx() {
		return sidx;
	}

	public void setSidx(String sidx) {
		this.sidx = sidx;
	}

	public String getSord() {
		return sord;
	}

	public void setSord(String sord) {
		this.sord = sord;
	}
	public Long[] getRuleNos() {
		return ruleNos;
	}

	public void setRuleNos(Long[] ruleNos) {
		this.ruleNos = ruleNos;
	}

	public Long getRulePackageNo() {
		return rulePackageNo;
	}

	public void setRulePackageNo(Long rulePackageNo) {
		this.rulePackageNo = rulePackageNo;
	}

	public RulePackageDTO getRulePackageDTO() {
		return rulePackageDTO;
	}

	public void setRulePackageDTO(RulePackageDTO rulePackageDTO) {
		this.rulePackageDTO = rulePackageDTO;
	}

	public RuleDTO getRuleDTO() {
		return ruleDTO;
	}

	public void setRuleDTO(RuleDTO ruleDTO) {
		this.ruleDTO = ruleDTO;
	}

	public RuleQueryDTO getRuleQueryDTO() {
		return ruleQueryDTO;
	}

	public void setRuleQueryDTO(RuleQueryDTO ruleQueryDTO) {
		this.ruleQueryDTO = ruleQueryDTO;
	}

	public IRuleService getRuleService() {
		return ruleService;
	}

	public void setRuleService(IRuleService ruleService) {
		this.ruleService = ruleService;
	}

	public IRulePackageService getRulePackageService() {
		return rulePackageService;
	}

	public void setRulePackageService(IRulePackageService rulePackageService) {
		this.rulePackageService = rulePackageService;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getRows() {
		return rows;
	}

	public void setRows(int rows) {
		this.rows = rows;
	}

	public PageDTO getPageDTO() {
		return pageDTO;
	}

	public void setPageDTO(PageDTO pageDTO) {
		this.pageDTO = pageDTO;
	}

	public Boolean getResult() {
		return result;
	}

	public void setResult(Boolean result) {
		this.result = result;
	}

	/**
	 * find method
	 */
	public String find() {
		ruleQueryDTO.setRulePackageNo(rulePackageNo);
		ruleQueryDTO.setPackageName(packageName);
		ruleQueryDTO.setFlagName(flagName);
		int start = (page - 1) * rows;
		pageDTO = ruleService.findRuleByPager(ruleQueryDTO, start, rows, sidx, sord);
		pageDTO.setPage(page);
		pageDTO.setRows(rows);
		return SUCCESS;
	}

	/**
	 * del method
	 */
	public String delete() {

		try {
			result = ruleService.removeRules(ruleNos);
		} catch (Exception ex) {
			throw new ApplicationException("ERROR_DATA_CAN_NOT_DELETE", ex);
		}

		return "result";
	}

	/**
	 * findRuleEntity method
	 */
	public String findRules() {
		find();
		return "pageDTO";
	}

	/**
	 * 导入数据.
	 */
	public String importRules() {

		rulePackageNo = ruleService.importRules(importFile.getAbsolutePath(), rulePackageNo);
		if (rulePackageNo != null && rulePackageNo > 0) {
			effect = "success";
		} else {
			effect = "fall";
		}

		return SUCCESS;
	}

	/**
	 * 导出数据.
	 */
	public String exportRules() {
		fileName = fileName + ".drl";
		exportFile = ruleService.exportRules(rulePackageNo);
		return "exportFile";
	}

	public String existRuleName() {
		result = ruleService.existRuleName(ruleName, rulePackageNo);
		return "result";
	}
}
