package com.wstuo.common.rules.drool;

public interface IRuleResourcePath {
	
	public String getRuleResourcePath();
}
