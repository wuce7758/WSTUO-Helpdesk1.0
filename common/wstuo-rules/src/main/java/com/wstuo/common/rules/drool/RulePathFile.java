package com.wstuo.common.rules.drool;

public enum RulePathFile {
	/**
	 * 请求规则资源文件路径
	 */
	URL_SETREQUESTRULESOURCE("drools/drl/request"),
	/**
	 * 变更规则资源文件路径
	 */
	URL_SETCHANGERULESOURCE("drools/drl/change"),
	/**
	 * 邮件转请求规则资源文件路径
	 */
	URL_SETREQUESTMAILRULESOURCE("drools/drl/requestMail"),
	/**
	 * 请求流程规则资源文件路径
	 */
	URL_SETREQUESTWORKFLOWRULESOURCE("drools/drl/requestWorkFlow"),
	/**
	 * 变更流程规则资源文件路径
	 */
	URL_SETCHANGEWORKFLOWRULESOURCE("drools/drl/changeWorkFlow"),
	/**
	 * sla规则资源文件路径
	 */
	URL_SETSLARULESOURCE("drools/drl/sla");

	// 定义私有变量
	private String nCode;

	// 构造函数，枚举类型只能为私有
	private RulePathFile(String _nCode) {
		this.nCode = _nCode;
	}

	public String getValue() {
		return nCode;
	}
}
