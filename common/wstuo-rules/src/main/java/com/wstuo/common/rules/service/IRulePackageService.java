package com.wstuo.common.rules.service;

import com.wstuo.common.rules.dto.RulePackageDTO;
import com.wstuo.common.dto.PageDTO;

import java.util.List;

/**
 * interface of RulePackageService
 * @author <a href="mailto:376890523@qq.com">jeff</a>
 * @version 0.1  2010-9-28
 * */
public interface IRulePackageService{

    /**
     * save rulePackage
     * @param dto
     */
    void saveRulePackage( RulePackageDTO dto );

    /**
     * remove rulePackage
     * @param no
     */
    void removeRulePackage( Long no );

    /**
     * remove rulePackages
     * @param nos
     */
    void removeRulePackages( Long[] nos );

    /**
     * merge rulePackage
     * @param dto
     * @return RulePackageDTO
     */
    RulePackageDTO mergeRulePackage( RulePackageDTO dto );

    /**
     * merge rulePackages
     * @param dtos
     */
    void mergeAllRulePackage( List<RulePackageDTO> dtos );


    /**
     * find all rulePackage
     * @return List<RulePackageDTO>
     */
    List<RulePackageDTO> findRulePackages();
    
    /**
     * 打印所有规则文件.
     */
    void printAllRuleFiles();
    

    /**
     * find rulePackage by id
     * @param no
     * @return RulePackageDTO
     */
    RulePackageDTO findRulePackageById( Long no );
    /**
     * 分页查询规则包
     * @param packageDTO
     * @param sidx
     * @param sord
     * @param start
     * @param rows
     * @return PageDTO
     */
    PageDTO findRulePackageByPage(RulePackageDTO packageDTO,String sidx,String sord,int start,int rows);
    /**
     * 保存
     * @param packageDTO
     */
    void rulePackageSave(final RulePackageDTO packageDTO);
    /**
     * 编辑
     * @param packageDTO
     */
    void editRulePackage(final RulePackageDTO packageDTO);
    /**
     * 删除
     * @param ids
     */
    void deleteRulePackage(final Long[] ids);
    /**
     * 判断
     * @param addpackageName
     * @return boolean
     */
    Boolean findRulePackageBoolean(final String addpackageName);
}
