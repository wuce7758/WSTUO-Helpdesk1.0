package com.wstuo.common.rules.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.wstuo.common.rules.dao.IRuleConstraintDAO;
import com.wstuo.common.rules.dao.IRulePatternDAO;
import com.wstuo.common.rules.dto.RuleConstraintDTO;
import com.wstuo.common.rules.entity.RuleConstraint;
import com.wstuo.common.rules.entity.RulePattern;

/**
 * the service class of RuleConstraintService
 * 
 * @author <a href="mailto:376890523@qq.com">jeff</a>
 * @version 0.1 2010-9-30
 * */
public class RuleConstraintService implements IRuleConstraintService {
	@Autowired
	private IRulePatternDAO rulePatternDAO;
	@Autowired
	private IRuleConstraintDAO ruleConstraintDAO;

	/**
	 * entity2dto Method
	 * 
	 * @param entity
	 *            ,dto
	 */
	private void entity2dto(RuleConstraint entity, RuleConstraintDTO dto) {
		if (entity.getPattern() != null) {
			Long rulePatternNo = entity.getPattern().getRulePatternNo();
			dto.setRulePatternNo(rulePatternNo);
		}
	}

	/**
	 * --------------save,remove,update Method--------------
	 * */

	/**
	 * save constraint
	 * 
	 * @param dto
	 */
	@Transactional
	public void saveRuleConstraint(RuleConstraintDTO dto) {
		RuleConstraint entity = new RuleConstraint();
		RuleConstraintDTO.dto2entity(dto, entity);
		if (null != dto.getRulePatternNo()) {
			RulePattern rulePattern = rulePatternDAO.findById(dto.getRulePatternNo());
			entity.setPattern(rulePattern);
		}
		ruleConstraintDAO.save(entity);
		dto.setConNo(entity.getConNo());
	}

	/**
	 * remove constraint
	 * 
	 * @param no
	 */
	@Transactional
	public void removeRuleConstraint(Long no) {
		ruleConstraintDAO.delete(ruleConstraintDAO.findById(no));
	}

	/**
	 * remove constraints
	 * 
	 * @param nos
	 */
	@Transactional
	public void removeRuleConstraints(Long[] nos) {
		ruleConstraintDAO.deleteByIds(nos);
	}

	/**
	 * merge constraint
	 * 
	 * @param dto
	 * @return RuleConstraintDTO
	 */
	@Transactional
	public RuleConstraintDTO mergeRuleConstraint(RuleConstraintDTO dto) {
		RuleConstraint entity = new RuleConstraint();
		RuleConstraintDTO.dto2entity(dto, entity);
		if (dto.getRulePatternNo() != null) {
			RulePattern rulePattern = rulePatternDAO.findById(dto.getRulePatternNo());
			entity.setPattern(rulePattern);
		}
		entity = ruleConstraintDAO.merge(entity);
		entity2dto(entity, dto);
		RuleConstraintDTO.entity2dto(entity, dto);
		return dto;
	}

	/**
	 * merge constraints
	 * 
	 * @param dtos
	 */
	@Transactional
	public void mergeAllRuleConstraint(List<RuleConstraintDTO> dtos) {
		List<RuleConstraint> entities = new ArrayList<RuleConstraint>();
		for (RuleConstraintDTO dto : dtos) {
			RuleConstraint entity = new RuleConstraint();
			RuleConstraintDTO.dto2entity(dto, entity);
			if (dto.getRulePatternNo() != null) {
				RulePattern rulePattern = rulePatternDAO.findById(dto.getRulePatternNo());
				entity.setPattern(rulePattern);
			}
			entities.add(entity);
		}
		ruleConstraintDAO.mergeAll(entities);
	}

	/**
	 * find all constraint
	 * 
	 * @return List<RuleConstraintDTO>
	 */
	@Transactional
	public List<RuleConstraintDTO> findRuleConstraints() {
		List<RuleConstraint> entities = ruleConstraintDAO.findAll();
		List<RuleConstraintDTO> dtos = new ArrayList<RuleConstraintDTO>(entities.size());
		for (RuleConstraint entity : entities) {
			RuleConstraintDTO dto = new RuleConstraintDTO();
			entity2dto(entity, dto);
			RuleConstraintDTO.entity2dto(entity, dto);
			dtos.add(dto);
		}

		return dtos;
	}

	/**
	 * find constraint by id
	 * 
	 * @param no
	 * @return RuleConstraintDTO
	 */
	public RuleConstraintDTO findRuleConstraintById(Long no) {
		RuleConstraint entity = ruleConstraintDAO.findById(no);
		RuleConstraintDTO dto = new RuleConstraintDTO();
		entity2dto(entity, dto);
		RuleConstraintDTO.entity2dto(entity, dto);
		return dto;
	}

	/**
	 * find constraint by PatternNo
	 * 
	 * @param patternNo
	 * @return List<RuleConstraintDTO>
	 */
	public List<RuleConstraintDTO> findRuleConstraintByPatternNo(Long patternNo) {
		RulePattern pattern = rulePatternDAO.findById(patternNo);
		List<RuleConstraint> entities = pattern.getConstraints();
		List<RuleConstraintDTO> dtos = new ArrayList<RuleConstraintDTO>(entities.size());
		for (RuleConstraint entity : entities) {
			RuleConstraintDTO dto = new RuleConstraintDTO();
			entity2dto(entity, dto);
			RuleConstraintDTO.entity2dto(entity, dto);
			dtos.add(dto);
		}
		return dtos;
	}
}
