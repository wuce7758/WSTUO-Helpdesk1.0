package com.wstuo.common.rules.service;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.wstuo.common.rules.dao.IRulePackageDAO;
import com.wstuo.common.rules.drool.RulePathType;
import com.wstuo.common.rules.dto.RulePackageDTO;
import com.wstuo.common.rules.entity.RulePackage;
import com.wstuo.common.rules.parser.ProcreationDrlFiles;
import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.security.utils.AppConfigUtils;

/**
 * the service class of RulePackageService
 * 
 * @author <a href="mailto:376890523@qq.com">jeff</a>
 * @version 0.1 2010-9-28
 * */
public class RulePackageService implements IRulePackageService {
	@Autowired
	private IRulePackageDAO rulePackageDAO;
	private ProcreationDrlFiles procreationDrlFiles = new ProcreationDrlFiles();

	private static final String RULE_PATH =  AppConfigUtils.getInstance().getDroolsFilePath();

	/**
	 * --------------save,remove,update Method--------------
	 * */

	/**
	 * save rulePackage
	 * 
	 * @param dto
	 */
	@Transactional
	public void saveRulePackage(RulePackageDTO dto) {
		RulePackage entity = new RulePackage();
		RulePackageDTO.dto2entity(dto, entity);
		rulePackageDAO.save(entity);
		dto.setRulePackageNo(entity.getRulePackageNo());
	}

	/**
	 * remove rulePackage
	 * 
	 * @param no
	 */
	@Transactional
	public void removeRulePackage(Long no) {
		rulePackageDAO.delete(rulePackageDAO.findById(no));
	}

	/**
	 * remove rulePackages
	 * 
	 * @param nos
	 */
	@Transactional
	public void removeRulePackages(Long[] nos) {
		rulePackageDAO.deleteByIds(nos);
	}

	/**
	 * merge rulePackage
	 * 
	 * @param dto
	 */
	@Transactional
	public RulePackageDTO mergeRulePackage(RulePackageDTO dto) {
		RulePackage entity = new RulePackage();
		RulePackageDTO.dto2entity(dto, entity);
		entity.getRules().addAll(rulePackageDAO.findById(entity.getRulePackageNo()).getRules());
		entity = rulePackageDAO.merge(entity);
		RulePackageDTO.entity2dto(entity, dto);
		return dto;
	}

	/**
	 * merge rulePackages
	 * 
	 * @param dtos
	 */
	@Transactional
	public void mergeAllRulePackage(List<RulePackageDTO> dtos) {
		List<RulePackage> entities = new ArrayList<RulePackage>();
		for (RulePackageDTO dto : dtos) {
			RulePackage entity = new RulePackage();
			RulePackageDTO.dto2entity(dto, entity);
			entities.add(entity);
		}
		rulePackageDAO.mergeAll(entities);
	}

	/**
	 * find all rulePackage
	 * 
	 * @return List<RulePackageDTO>
	 */
	public List<RulePackageDTO> findRulePackages() {
		List<RulePackage> entities = rulePackageDAO.findRules();
		List<RulePackageDTO> dtos = new ArrayList<RulePackageDTO>();
		for (RulePackage entity : entities) {
			RulePackageDTO dto = new RulePackageDTO();
			RulePackageDTO.entity2dto(entity, dto);
			dtos.add(dto);
		}

		return dtos;
	}

	/**
     * 
     */
	public RulePackageDTO findRulePackageById(Long no) {
		RulePackage entity = rulePackageDAO.findById(no);
		RulePackageDTO dto = new RulePackageDTO();
		RulePackageDTO.entity2dto(entity, dto);
		return dto;
	}

	/**
	 * 打印所有规则文件.
	 */
	@Transactional
	public void printAllRuleFiles() {
		//查看文件夹是否存在，不存在则创建
    	createAllRuleFolder();
		List<RulePackage> packages = rulePackageDAO.findAll();
		for (RulePackage pkg : packages) {
			procreationDrlFiles.printDrlFiles(pkg);
		}
	}
	
	/**
	 * 查看文件夹是否存在，不存在则创建
	 */
	private void createAllRuleFolder(){
		List<String> paths = new ArrayList<String>();
		paths.add(RulePathType.Request.getValue());
		paths.add(RulePathType.RequestMail.getValue());
		paths.add(RulePathType.Change.getValue());
		paths.add(RulePathType.RequestWorkFlow.getValue());
		paths.add(RulePathType.Sla.getValue());
		for (String path : paths) {
			File folder = new File( RULE_PATH +"/"+path );
	         //如果文件夹不存在就创建文件夹
	         if (!folder.exists()) {
	         	folder.mkdirs();
	         }
		}
	}

	/**
	 * 分页查询规则包。
	 * 
	 * @param packageDTO
	 * @param sidx
	 * @param sord
	 * @param start
	 * @param rows
	 * @return PageDTO
	 */
	@SuppressWarnings("unchecked")
	@Transactional
	public PageDTO findRulePackageByPage(RulePackageDTO packageDTO, String sidx, String sord, int start, int rows) {
		PageDTO p = rulePackageDAO.findPager(packageDTO, sidx, sord, start, rows);
		List<RulePackage> listEntity = p.getData();
		List<RulePackageDTO> listDto = new ArrayList<RulePackageDTO>();
		for (RulePackage entity : listEntity) {
			RulePackageDTO dto = new RulePackageDTO();
			RulePackageDTO.entity2dto(entity, dto);
			listDto.add(dto);
		}
		p.setData(listDto);
		return p;
	}

	@Transactional
	public void rulePackageSave(final RulePackageDTO packageDTO) {
		RulePackage entity = new RulePackage();
		entity.setPackageName("com.drools.drl." + packageDTO.getPackageName());// 规则包名称
		if(packageDTO!=null && packageDTO.getModule()!=null && packageDTO.getModule().equals("change")){
			entity.setImports("com.wstuo.itsm.change.dto.ChangeDTO;");
			entity.setFlagName("changeProce");
		}else{
			entity.setImports("com.wstuo.itsm.request.dto.RequestDTO;");
			entity.setFlagName("requestProce");
		}
		entity.setLocation(new Date().getTime() + ".drl");
		entity.setModule(packageDTO.getModule());
		entity.setRulePackageRemarks(packageDTO.getRulePackageRemarks());// 规则包备注
		rulePackageDAO.save(entity);
		packageDTO.setRulePackageNo(entity.getRulePackageNo());
	}

	@Transactional
	public void editRulePackage(final RulePackageDTO packageDTO) {
		RulePackage entity = rulePackageDAO.findById(packageDTO.getRulePackageNo());
		entity.setRulePackageRemarks(packageDTO.getRulePackageRemarks());
		rulePackageDAO.merge(entity);
	}

	@Transactional
	public void deleteRulePackage(final Long[] ids) {
		List<RulePackage> RulePackages = rulePackageDAO.findByIds(ids);
		for (RulePackage entity : RulePackages) {
			if (entity != null && entity.getLocation() != null) {
				procreationDrlFiles.removeFile(entity.getLocation());
			}
		}
		rulePackageDAO.deleteByIds(ids);
	}

	@Transactional
	public Boolean findRulePackageBoolean(final String addpackageName) {
		boolean result = true;
		String packageName = "com.drools.drl." + addpackageName;
		List<RulePackage> li = rulePackageDAO.findBy("packageName", packageName);
		if (li != null && li.size() > 0) {
			result = false;
		}
		return result;
	}
}
