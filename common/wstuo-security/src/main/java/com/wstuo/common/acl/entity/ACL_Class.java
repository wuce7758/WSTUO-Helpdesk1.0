package com.wstuo.common.acl.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * ACL CLASS ENTITY CLASS
 * @author Administrator
 *
 */
//@Entity(name="acl_class")
public class ACL_Class {
	@Id
    @GeneratedValue( strategy = GenerationType.AUTO )
	private Long id;
	@Column(name="class")
	private String aclclass;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getAclclass() {
		return aclclass;
	}
	public void setAclclass(String aclclass) {
		this.aclclass = aclclass;
	}
	
}
