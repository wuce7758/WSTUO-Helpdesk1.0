package com.wstuo.common.activation.service;


public interface IActivationService {

	/**
	 * 监听内容
	 */
	public abstract void init();
	/**
	 * 取access值
	 * @return
	 */
	public abstract Boolean getAccess();

}