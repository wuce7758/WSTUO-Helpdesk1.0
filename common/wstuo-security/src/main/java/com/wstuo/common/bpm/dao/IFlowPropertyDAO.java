package com.wstuo.common.bpm.dao;

import com.wstuo.common.bpm.entity.FlowProperty;
import com.wstuo.common.dao.IEntityDAO;

/**
 * Flow Property DAO Interface Class
 * @author wstuo
 *
 */
public interface IFlowPropertyDAO extends IEntityDAO<FlowProperty>{
	
}
