package com.wstuo.common.bpm.dto;

import java.util.List;

import com.wstuo.common.bpm.dto.FlowTaskDTO;

/**
 * 流程节点详细DTO
 * @author wstuo
 *
 */
public class ProcessDetailDTO {
	private String activityName;
	private List<FlowTaskDTO> flowTaskDTO;
	public String getActivityName() {
		return activityName;
	}
	public void setActivityName(String activityName) {
		this.activityName = activityName;
	}
	public List<FlowTaskDTO> getFlowTaskDTO() {
		return flowTaskDTO;
	}
	public void setFlowTaskDTO(List<FlowTaskDTO> flowTaskDTO) {
		this.flowTaskDTO = flowTaskDTO;
	}
}
