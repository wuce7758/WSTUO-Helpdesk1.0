package com.wstuo.common.bpm.dto;

import com.wstuo.common.dto.BaseDTO;
/**
 * 流程使用DTO 
 * @author wstuo
 *
 */
@SuppressWarnings("serial")
public class ProcessUseDTO extends BaseDTO {
	private Long processUseNo;
	private String useName;
	private String requestProcessKey;
	private String changeProcessKey;
	private String problemProcessKey;
	private String knowledgeProcessKey;
	private String configureItemProcessKey;
	private String releaseProcessKey;
	
	private String requestProcessDefinitionId;//请求流程定义ID
	private String changeProcessDefinitionId;//问题流程定义ID
	private String problemProcessDefinitionId;//变更流程定义ID
	private String knowledgeProcessDefinitionId;//知识库流程定义ID
	private String configureItemProcessDefinitionId;//配置项流程定义ID
	private String releaseProcessDefinitionId;//发布流程定义ID
	
	public Long getProcessUseNo() {
		return processUseNo;
	}
	public void setProcessUseNo(Long processUseNo) {
		this.processUseNo = processUseNo;
	}
	public String getUseName() {
		return useName;
	}
	public void setUseName(String useName) {
		this.useName = useName;
	}
	public String getRequestProcessKey() {
		return requestProcessKey;
	}
	public void setRequestProcessKey(String requestProcessKey) {
		this.requestProcessKey = requestProcessKey;
	}
	public String getChangeProcessKey() {
		return changeProcessKey;
	}
	public void setChangeProcessKey(String changeProcessKey) {
		this.changeProcessKey = changeProcessKey;
	}
	public String getProblemProcessKey() {
		return problemProcessKey;
	}
	public void setProblemProcessKey(String problemProcessKey) {
		this.problemProcessKey = problemProcessKey;
	}
	public String getKnowledgeProcessKey() {
		return knowledgeProcessKey;
	}
	public void setKnowledgeProcessKey(String knowledgeProcessKey) {
		this.knowledgeProcessKey = knowledgeProcessKey;
	}
	public String getConfigureItemProcessKey() {
		return configureItemProcessKey;
	}
	public void setConfigureItemProcessKey(String configureItemProcessKey) {
		this.configureItemProcessKey = configureItemProcessKey;
	}
	public String getRequestProcessDefinitionId() {
		return requestProcessDefinitionId;
	}
	public void setRequestProcessDefinitionId(String requestProcessDefinitionId) {
		this.requestProcessDefinitionId = requestProcessDefinitionId;
	}
	public String getChangeProcessDefinitionId() {
		return changeProcessDefinitionId;
	}
	public void setChangeProcessDefinitionId(String changeProcessDefinitionId) {
		this.changeProcessDefinitionId = changeProcessDefinitionId;
	}
	public String getProblemProcessDefinitionId() {
		return problemProcessDefinitionId;
	}
	public void setProblemProcessDefinitionId(String problemProcessDefinitionId) {
		this.problemProcessDefinitionId = problemProcessDefinitionId;
	}
	public String getKnowledgeProcessDefinitionId() {
		return knowledgeProcessDefinitionId;
	}
	public void setKnowledgeProcessDefinitionId(String knowledgeProcessDefinitionId) {
		this.knowledgeProcessDefinitionId = knowledgeProcessDefinitionId;
	}
	public String getConfigureItemProcessDefinitionId() {
		return configureItemProcessDefinitionId;
	}
	public void setConfigureItemProcessDefinitionId(
			String configureItemProcessDefinitionId) {
		this.configureItemProcessDefinitionId = configureItemProcessDefinitionId;
	}
	public String getReleaseProcessDefinitionId() {
		return releaseProcessDefinitionId;
	}
	public void setReleaseProcessDefinitionId(String releaseProcessDefinitionId) {
		this.releaseProcessDefinitionId = releaseProcessDefinitionId;
	}
	public String getReleaseProcessKey() {
		return releaseProcessKey;
	}
	public void setReleaseProcessKey(String releaseProcessKey) {
		this.releaseProcessKey = releaseProcessKey;
	}

	
}
