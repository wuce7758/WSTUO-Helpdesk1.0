package com.wstuo.common.bpm.dto;

import com.wstuo.common.dto.BaseDTO;

import java.util.Date;
import java.util.Map;

/**
 * 任务信息DTO
 * @author wstuo
 */
@SuppressWarnings("serial")
public class TaskDTO extends BaseDTO{
    public static final String APP_VARIABLE = "dto";
    private String remark;
    protected String name;
    protected String description;
    protected String assignee;
    protected String formResourceName;
    protected Date createTime;
    protected Date duedate;
    protected Integer progress;
    protected int priority;
    protected String executionId;
    protected String activityName;
    protected String id;
    private Map<String, Object> variables;

    public String getId(  )
    {
        return id;
    }

    public void setId( String id )
    {
        this.id = id;
    }

    public String getName(  )
    {
        return name;
    }

    public void setName( String name )
    {
        this.name = name;
    }

    public String getDescription(  )
    {
        return description;
    }

    public void setDescription( String description )
    {
        this.description = description;
    }

    public String getAssignee(  )
    {
        return assignee;
    }

    public void setAssignee( String assignee )
    {
        this.assignee = assignee;
    }

    public String getFormResourceName(  )
    {
        return formResourceName;
    }

    public void setFormResourceName( String formResourceName )
    {
        this.formResourceName = formResourceName;
    }

    public Date getCreateTime(  )
    {
        return createTime;
    }

    public void setCreateTime( Date createTime )
    {
        this.createTime = createTime;
    }

    public Date getDuedate(  )
    {
        return duedate;
    }

    public void setDuedate( Date duedate )
    {
        this.duedate = duedate;
    }

    public Integer getProgress(  )
    {
        return progress;
    }

    public void setProgress( Integer progress )
    {
        this.progress = progress;
    }

    public int getPriority(  )
    {
        return priority;
    }

    public void setPriority( int priority )
    {
        this.priority = priority;
    }

    public String getExecutionId(  )
    {
        return executionId;
    }

    public void setExecutionId( String executionId )
    {
        this.executionId = executionId;
    }

    public String getActivityName(  )
    {
        return activityName;
    }

    public void setActivityName( String activityName )
    {
        this.activityName = activityName;
    }

    public Long getExecutionDbid(  )
    {
        return executionDbid;
    }

    public void setExecutionDbid( Long executionDbid )
    {
        this.executionDbid = executionDbid;
    }

    public Long getSuperTaskDbid(  )
    {
        return superTaskDbid;
    }

    public void setSuperTaskDbid( Long superTaskDbid )
    {
        this.superTaskDbid = superTaskDbid;
    }

    protected Long executionDbid;
    protected Long superTaskDbid;

    public String getRemark(  )
    {
        return remark;
    }

    public void setRemark( String remark )
    {
        this.remark = remark;
    }

    public Map<String, Object> getVariables(  )
    {
        return variables;
    }

    public void setVariables( Map<String, Object> variables )
    {
        this.variables = variables;
    }
}
