package com.wstuo.common.bpm.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Flow Transition Entity Class
 * @author wstuo
 *
 */
@Entity
public class FlowTransition {
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String transitionName;//过渡名
	private String destination;//目的地
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getTransitionName() {
		return transitionName;
	}
	public void setTransitionName(String transitionName) {
		this.transitionName = transitionName;
	}
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	
}
