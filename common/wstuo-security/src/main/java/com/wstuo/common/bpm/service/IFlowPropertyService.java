package com.wstuo.common.bpm.service;

import com.wstuo.common.bpm.dto.FlowActivityDTO;
import com.wstuo.common.bpm.dto.FlowPropertyDTO;


/**
 * Flow Property Service Interface Class
 * @author wstuo
 *
 */
public interface IFlowPropertyService {

	/**
	 * Save Flow Property
	 * @param flowPropertyDTO
	 */
	void saveFlowProperty(FlowPropertyDTO flowPropertyDTO);
	
	
	/**
	 * Find Flow Property Details By DeploymentID
	 * @param processDefinitionId
	 * @return FlowPropertyDTO
	 */
	FlowPropertyDTO findFlowPropertyByProcessDefinitionId(String processDefinitionId);
	
	String findFlowPropertyByDeploymentId(String deploymentId);
	
	/**
	 * Delete Flow Property
	 * @param deploymentId
	 */
	void deleteFlowProperty(String deploymentId);
	
	/**
	 * Update Flow Activity
	 * @param dto FlowActivityDTO
	 */
	void updateFlowActivity(FlowActivityDTO dto);
	/**
	 * Find Flow Activity By Id
	 * @param id
	 * @return FlowActivityDTO
	 */
	FlowActivityDTO findFlowActivityById(Long id);
	
	/**
	 * 查询流程动作
	 * @param processDefinitionId 流程定义ID
	 * @param activityType 活动类型
	 * @param activityName 活动名称
	 * @return FlowActivityDTO
	 */
	FlowActivityDTO findFlowActivity(String processDefinitionId,String[] activityType,String activityName);
	/**
	 * 是否存在该任务
	 * @param taskId String type
	 * @return boolean
	 */
	boolean isExistTaskId(String taskId);
}
