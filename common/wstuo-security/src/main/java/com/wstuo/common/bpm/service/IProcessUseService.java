package com.wstuo.common.bpm.service;

import com.wstuo.common.bpm.dto.ProcessUseDTO;


/**
 * Process Use Service Interface Class(默认流程设置)
 * @author wstuo
 *
 */
public interface IProcessUseService {
	/**
	 * processUse Save
	 * @param processUseDTO
	 */
	void processUseSave(ProcessUseDTO processUseDTO);
	/**
	 * find By Id ProcessUse
	 * @param id
	 * @return ProcessUseDTO
	 */
	ProcessUseDTO findById(Long id);
	
	/**
	 * 获取流程选用 
	 * @param useName
	 * @return ProcessUseDTO
	 */
	ProcessUseDTO findProcessUseByUseName(String useName);
	
	/**
	 * 流程选用 修改
	 * @param processUseDTO
	 */
	void editProcessUse(ProcessUseDTO processUseDTO);
}
