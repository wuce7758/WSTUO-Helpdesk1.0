package com.wstuo.common.ldapmq;

import javax.jms.Destination;

import org.springframework.jms.core.JmsTemplate;

import com.wstuo.common.security.dto.UserDTO;


/**
 * 发送消息
 * @author will
 *
 */
public class LdapAddUserProducer{
	private JmsTemplate template;

	private Destination destination;


	public void setTemplate(JmsTemplate template) {
	   this.template = template;
	}

	public void setDestination(Destination destination) {
	   this.destination = destination;
	}

	public void send(UserDTO order) {
		template.convertAndSend(this.destination, order);
	} 
}