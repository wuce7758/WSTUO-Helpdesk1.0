package com.wstuo.common.security.dao;

import com.wstuo.common.dao.IEntityDAO;
import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.security.dto.OperationQueryDTO;
import com.wstuo.common.security.entity.Operation;

/**
 * 操作DAO接口.
 * @author WILL
 */
public interface IOperationDAO
    extends IEntityDAO<Operation>
{
    /**
     * 分页查找功能操作.
     * @param  qdto 查询DTO OperationQueryDTO 
     * @return 分页数据 PageDTO
     */
    PageDTO findPager( OperationQueryDTO qdto );

    /**
     * 验证是否存重复的操作.
     * @param qdto 查询DTO OperationQueryDTO
     * @return boolean
     */
    Boolean findExist( OperationQueryDTO qdto );
    
}
