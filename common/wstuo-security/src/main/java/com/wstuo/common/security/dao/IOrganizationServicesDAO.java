package com.wstuo.common.security.dao;

import com.wstuo.common.dao.IEntityDAO;
import com.wstuo.common.security.entity.OrganizationServices;

import java.util.List;

/**
 * 服务机构DAO接口.
 * @author will
 */
public interface IOrganizationServicesDAO
    extends IEntityDAO<OrganizationServices>
{
    /**
       * 根据公司编号查询服务机构信息.
       * @param companyNo 公司编号 Long companyNo
       * @return 服务机构列表 List<OrganizationServices>
       */
    List<OrganizationServices> findByCompany( Long companyNo );
}
