package com.wstuo.common.security.dto;

import com.wstuo.common.dto.BaseDTO;

/**
 * class CompanyDTO.
 * 
 * @author will
 */
@SuppressWarnings("serial")
public class CompanyDTO extends BaseDTO {
	private Long orgNo;
	private String orgName;
	private String address;
	private String email;
	private String officePhone;
	private String officeFax;
	public String logo;
	public String homePage;
	private Boolean setup =false;// 设置标签
	// 机构负责�?
	private Long personInChargeNo;
	private String personInChargeName;

	public Long getOrgNo() {
		return orgNo;
	}

	public void setOrgNo(Long orgNo) {
		this.orgNo = orgNo;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getOfficePhone() {
		return officePhone;
	}

	public void setOfficePhone(String officePhone) {
		this.officePhone = officePhone;
	}

	public String getOfficeFax() {
		return officeFax;
	}

	public void setOfficeFax(String officeFax) {
		this.officeFax = officeFax;
	}

	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	public String getHomePage() {
		return homePage;
	}

	public void setHomePage(String homePage) {
		this.homePage = homePage;
	}

	public Boolean getSetup() {
		return setup;
	}

	public void setSetup(Boolean setup) {
		this.setup = setup;
	}

	public Long getPersonInChargeNo() {
		return personInChargeNo;
	}

	public void setPersonInChargeNo(Long personInChargeNo) {
		this.personInChargeNo = personInChargeNo;
	}

	public String getPersonInChargeName() {
		return personInChargeName;
	}

	public void setPersonInChargeName(String personInChargeName) {
		this.personInChargeName = personInChargeName;
	}

}
