package com.wstuo.common.security.dto;

import com.wstuo.common.dto.BaseDTO;

/**
 * class LDAPCheckDTO
 * @author will
 */
@SuppressWarnings("serial")
public class LDAPCheckDTO extends BaseDTO{
    private String ldapName;
    private String ldapURL;
    private String prot;
    private String adminName;
    private String adminPassword;
    private String searchFilter;
    private String searchBase;
    private String ldapType;

    public LDAPCheckDTO(  )
    {
        super(  );
    }

    public String getLdapName(  )
    {
        return ldapName;
    }

    public void setLdapName( String ldapName )
    {
        this.ldapName = ldapName;
    }

    public String getLdapURL(  )
    {
        return ldapURL;
    }

    public void setLdapURL( String ldapURL )
    {
        this.ldapURL = ldapURL;
    }

    public String getProt(  )
    {
        return prot;
    }

    public void setProt( String prot )
    {
        this.prot = prot;
    }

    public String getAdminName(  )
    {
        return adminName;
    }

    public void setAdminName( String adminName )
    {
        this.adminName = adminName;
    }

    public String getAdminPassword(  )
    {
        return adminPassword;
    }

    public void setAdminPassword( String adminPassword )
    {
        this.adminPassword = adminPassword;
    }

    public String getSearchFilter(  )
    {
        return searchFilter;
    }

    public void setSearchFilter( String searchFilter )
    {
        this.searchFilter = searchFilter;
    }

    public String getSearchBase(  )
    {
        return searchBase;
    }

    public void setSearchBase( String searchBase )
    {
        this.searchBase = searchBase;
    }

    public String getLdapType(  )
    {
        return ldapType;
    }

    public void setLdapType( String ldapType )
    {
        this.ldapType = ldapType;
    }
}
