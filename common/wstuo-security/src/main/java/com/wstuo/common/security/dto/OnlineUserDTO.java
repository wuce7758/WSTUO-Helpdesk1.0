package com.wstuo.common.security.dto;

public class OnlineUserDTO {
    
    /*
     * 用户名
     */
    private String userName;
    
    /*
     * 是否终端用户
     */
    private Boolean isTerminal;
    
    public OnlineUserDTO(){
        super();
    }
    
    public OnlineUserDTO(String userName, boolean isTerminal) {
        this.userName = userName;
        this.isTerminal = isTerminal;
    }
    
    public String getUserName() {
        return userName;
    }
    public void setUserName(String userName) {
        this.userName = userName;
    }
    public Boolean getIsTerminal() {
        return isTerminal;
    }
    public void setIsTerminal(Boolean isTerminal) {
        this.isTerminal = isTerminal;
    }
    
    
    
}
