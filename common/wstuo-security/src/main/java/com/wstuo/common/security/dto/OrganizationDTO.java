package com.wstuo.common.security.dto;

import com.wstuo.common.dto.BaseDTO;
import com.wstuo.common.security.entity.Role;

import java.util.Arrays;
import java.util.Set;

/**
 * this class is OrganizationDTO.
 * @author will
 */
@SuppressWarnings("serial")
public class OrganizationDTO extends BaseDTO {
	private Long orgNo;
	private String orgName;
	private String address;
	private String email;
	private String officePhone;
	private String officeFax;
	// 公用信息
	private Long parentOrgNo;
	// 公司信息
	private String homePage;
	private String logo;
	// 机构负责人
	private Long personInChargeNo;
	private String personInChargeName;
	// 角色
	private Long[] orgRole;
	private Set<Role> orgRoles;
	private Byte dataFlag;
	private Boolean setup = false;// 设置标签
	private String orgType;
	private String remark;// 备注

	public Long getOrgNo() {
		return orgNo;
	}

	public void setOrgNo(Long orgNo) {
		this.orgNo = orgNo;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getOfficePhone() {
		return officePhone;
	}

	public void setOfficePhone(String officePhone) {
		this.officePhone = officePhone;
	}

	public String getOfficeFax() {
		return officeFax;
	}

	public void setOfficeFax(String officeFax) {
		this.officeFax = officeFax;
	}

	public Long getParentOrgNo() {
		return parentOrgNo;
	}

	public void setParentOrgNo(Long parentOrgNo) {
		this.parentOrgNo = parentOrgNo;
	}

	public String getHomePage() {
		return homePage;
	}

	public void setHomePage(String homePage) {
		this.homePage = homePage;
	}

	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	public Long getPersonInChargeNo() {
		return personInChargeNo;
	}

	public void setPersonInChargeNo(Long personInChargeNo) {
		this.personInChargeNo = personInChargeNo;
	}

	public String getPersonInChargeName() {
		return personInChargeName;
	}

	public void setPersonInChargeName(String personInChargeName) {
		this.personInChargeName = personInChargeName;
	}

	public Long[] getOrgRole() {
		return orgRole;
	}

	public void setOrgRole(Long[] orgRole) {
		this.orgRole = orgRole;
	}

	public Set<Role> getOrgRoles() {
		return orgRoles;
	}

	public void setOrgRoles(Set<Role> orgRoles) {
		this.orgRoles = orgRoles;
	}

	public Byte getDataFlag() {
		return dataFlag;
	}

	public void setDataFlag(Byte dataFlag) {
		this.dataFlag = dataFlag;
	}

	public Boolean getSetup() {
		return setup;
	}

	public void setSetup(Boolean setup) {
		this.setup = setup;
	}

	public String getOrgType() {
		return orgType;
	}

	public void setOrgType(String orgType) {
		this.orgType = orgType;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public OrganizationDTO(){
		super();
	}
	
	public OrganizationDTO(Long orgNo, String orgName, String address,
			String email, String officePhone, String officeFax,
			Long parentOrgNo, String homePage, String logo) {
		super();
		this.orgNo = orgNo;
		this.orgName = orgName;
		this.address = address;
		this.email = email;
		this.officePhone = officePhone;
		this.officeFax = officeFax;
		this.parentOrgNo = parentOrgNo;
		this.homePage = homePage;
		this.logo = logo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		result = prime * result
				+ ((dataFlag == null) ? 0 : dataFlag.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result
				+ ((homePage == null) ? 0 : homePage.hashCode());
		result = prime * result + ((logo == null) ? 0 : logo.hashCode());
		result = prime * result
				+ ((officeFax == null) ? 0 : officeFax.hashCode());
		result = prime * result
				+ ((officePhone == null) ? 0 : officePhone.hashCode());
		result = prime * result + ((orgName == null) ? 0 : orgName.hashCode());
		result = prime * result + ((orgNo == null) ? 0 : orgNo.hashCode());
		result = prime * result + Arrays.hashCode(orgRole);
		result = prime * result
				+ ((orgRoles == null) ? 0 : orgRoles.hashCode());
		result = prime * result + ((orgType == null) ? 0 : orgType.hashCode());
		result = prime * result
				+ ((parentOrgNo == null) ? 0 : parentOrgNo.hashCode());
		result = prime
				* result
				+ ((personInChargeName == null) ? 0 : personInChargeName
						.hashCode());
		result = prime
				* result
				+ ((personInChargeNo == null) ? 0 : personInChargeNo.hashCode());
		result = prime * result + ((remark == null) ? 0 : remark.hashCode());
		result = prime * result + ((setup == null) ? 0 : setup.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OrganizationDTO other = (OrganizationDTO) obj;
		if (address == null) {
			if (other.address != null)
				return false;
		} else if (!address.equals(other.address))
			return false;
		if (dataFlag == null) {
			if (other.dataFlag != null)
				return false;
		} else if (!dataFlag.equals(other.dataFlag))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (homePage == null) {
			if (other.homePage != null)
				return false;
		} else if (!homePage.equals(other.homePage))
			return false;
		if (logo == null) {
			if (other.logo != null)
				return false;
		} else if (!logo.equals(other.logo))
			return false;
		if (officeFax == null) {
			if (other.officeFax != null)
				return false;
		} else if (!officeFax.equals(other.officeFax))
			return false;
		if (officePhone == null) {
			if (other.officePhone != null)
				return false;
		} else if (!officePhone.equals(other.officePhone))
			return false;
		if (orgName == null) {
			if (other.orgName != null)
				return false;
		} else if (!orgName.equals(other.orgName))
			return false;
		if (orgNo == null) {
			if (other.orgNo != null)
				return false;
		} else if (!orgNo.equals(other.orgNo))
			return false;
		if (!Arrays.equals(orgRole, other.orgRole))
			return false;
		if (orgRoles == null) {
			if (other.orgRoles != null)
				return false;
		} else if (!orgRoles.equals(other.orgRoles))
			return false;
		if (orgType == null) {
			if (other.orgType != null)
				return false;
		} else if (!orgType.equals(other.orgType))
			return false;
		if (parentOrgNo == null) {
			if (other.parentOrgNo != null)
				return false;
		} else if (!parentOrgNo.equals(other.parentOrgNo))
			return false;
		if (personInChargeName == null) {
			if (other.personInChargeName != null)
				return false;
		} else if (!personInChargeName.equals(other.personInChargeName))
			return false;
		if (personInChargeNo == null) {
			if (other.personInChargeNo != null)
				return false;
		} else if (!personInChargeNo.equals(other.personInChargeNo))
			return false;
		if (remark == null) {
			if (other.remark != null)
				return false;
		} else if (!remark.equals(other.remark))
			return false;
		if (setup == null) {
			if (other.setup != null)
				return false;
		} else if (!setup.equals(other.setup))
			return false;
		return true;
	}


}
