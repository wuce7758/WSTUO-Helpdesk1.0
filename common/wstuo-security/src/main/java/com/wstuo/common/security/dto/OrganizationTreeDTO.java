package com.wstuo.common.security.dto;

import com.wstuo.common.dto.BaseDTO;
import com.wstuo.common.exception.ApplicationException;
import com.wstuo.common.security.entity.Company;
import com.wstuo.common.security.entity.Organization;
import com.wstuo.common.security.entity.Role;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * a class OrganizationTreeDTO.
 * @author will
 */
@SuppressWarnings( "serial" )
public class OrganizationTreeDTO
    extends BaseDTO
{
    private String data;
    private String state;
    private Set<Role> orgRoles;
    private Map<String, String> attr = new HashMap<String, String>(  );
    private List<OrganizationTreeDTO> children = new ArrayList<OrganizationTreeDTO>(  );

    public OrganizationTreeDTO(  )
    {
        super(  );
    }

    public String getData(  )
    {
        return data;
    }

    public void setData( String data )
    {
        this.data = data;
    }

    public String getState(  )
    {
        return state;
    }

    public void setState( String state )
    {
        this.state = state;
    }

    public Map<String, String> getAttr(  )
    {
        return attr;
    }

    public void setAttr( Map<String, String> attributes )
    {
        this.attr = attributes;
    }

    public List<OrganizationTreeDTO> getChildren(  )
    {
        return children;
    }

    public void setChildren( List<OrganizationTreeDTO> children )
    {
        this.children = children;
    }

    public Set<Role> getOrgRoles(  )
    {
        return orgRoles;
    }

    public void setOrgRoles( Set<Role> orgRoles )
    {
        this.orgRoles = orgRoles;
    }

    /**
    * OrganizationInner to OrganizationTreeDTO.
    * @param organization
    * @param dto
    */
    public static void org2dto( Organization organization, OrganizationTreeDTO dto )
    {
        try
        {
            if ( organization.getOrgRoles(  ) != null )
            {
                Set<Role> roles = new HashSet<Role>(  );

                for ( Role role : organization.getOrgRoles(  ) )
                {
                    roles.add( role );
                }

                dto.setOrgRoles( roles );
            }

            dto.setData( organization.getOrgName(  ) );
            dto.getAttr(  ).put( "orgNo", "" + organization.getOrgNo(  ) );
            dto.getAttr(  ).put( "orgName",
                                 organization.getOrgName(  ) );
            dto.getAttr(  ).put( "address",
                                 organization.getAddress(  ) );
            dto.getAttr(  ).put( "email",
                                 organization.getEmail(  ) );
            dto.getAttr(  ).put( "officeFax",
                                 organization.getOfficeFax(  ) );
            dto.getAttr(  ).put( "officePhone",
                                 organization.getOfficePhone(  ) );
            

            //递归查询
            if ( ! organization.getSuborgs(  ).isEmpty(  ) )
            {

                for ( Organization ins : organization.getSuborgs(  ) )
                {
                    OrganizationTreeDTO.org2dto( ins, dto );
                }
            }
        } catch ( Exception ex )
        {
            throw new ApplicationException( "Exception caused while converting Entity into DTO: " + ex.getMessage(  ),ex);
        }
    }

    /**
     *
     * @param company
     * @param dto
     */
    public static void company2dto( Company company, OrganizationTreeDTO dto )
    {

            dto.setData( company.getOrgName(  ) );
            dto.getAttr(  ).put( "orgNo", "" + company.getOrgNo(  ) );
            dto.getAttr(  ).put( "orgName",
                                 company.getOrgName(  ) );
            dto.getAttr(  ).put( "address",
                                 company.getAddress(  ) );
            dto.getAttr(  ).put( "email",
                                 company.getEmail(  ) );
            dto.getAttr(  ).put( "officeFax",
                                 company.getOfficeFax(  ) );
            dto.getAttr(  ).put( "officePhone",
                                 company.getOfficePhone(  ) );
            dto.getAttr(  ).put( "companyNo", company.getOrgNo(  ) + "" );
            dto.getAttr(  ).put( "homePage",
                                 company.getHomePage(  ) );
            dto.getAttr(  ).put( "logo",
                                 company.getLogo(  ) );

    }

    /**
     *
     */
}
