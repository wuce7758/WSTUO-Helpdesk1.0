package com.wstuo.common.security.dto;

import com.wstuo.common.dto.BaseDTO;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * Class UserDTO
 * 
 */
@SuppressWarnings("serial")
public class UserDTO extends BaseDTO implements UserDetails {
	private Long userId;
	private String loginName;
	private String username;
	private String firstName;
	private String lastName;
	private String password;
	private String email;
	private String moblie;
	private String phone;
	private String officePhone;
	private String description;
	private String remark;
	private String job;
	private Long[] roleIds;
	private Boolean userState = false;
	private Float userCost;
	private Boolean enabled = false;
	private Collection<GrantedAuthority> authorities;
	private String fax; // 传真
	private String msn;
	private String officeAddress; // 办公地址
	private Long orgNo;
	private String orgName;
	private Byte dataFlag;
	private String roles;
	private String pinyin;// 姓名拼音
	private Date birthday;// 生日
	private String position;// 职务
	private String icCard;// 证件号
	private Boolean sex = false;// 性别
	private List<RoleDTO> userRoles;// 角色
	private String fullName;// 真实姓名
	private String remoteHost;// 小灵呼语音卡远程主机
	private String remotePort;// 小灵呼语音卡远程端口
	private Long[] technicalGroupIds;
	private Boolean holidayStatus = false;// 休假状态
	private Long[] belongsGroupIds;// 所属组ID
	private String belongsGroup;// 所属组
	private String randomId;
	private String roleCode;
	String lcallCodeOper;// 小灵呼语音卡工号
	String domain;// 用户所在的域名
	String distinguishedName;// 用户所在域的详细位置
	String extension;// 分机号
	
	private String endUserStyle;//终端用户打开页面的方式
	
	
	public String getEndUserStyle() {
		return endUserStyle;
	}
	public void setEndUserStyle(String endUserStyle) {
		this.endUserStyle = endUserStyle;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public String getLoginName() {
		return loginName;
	}
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getMoblie() {
		return moblie;
	}
	public void setMoblie(String moblie) {
		this.moblie = moblie;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getOfficePhone() {
		return officePhone;
	}
	public void setOfficePhone(String officePhone) {
		this.officePhone = officePhone;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getJob() {
		return job;
	}
	public void setJob(String job) {
		this.job = job;
	}
	public Long[] getRoleIds() {
		return roleIds;
	}
	public void setRoleIds(Long[] roleIds) {
		this.roleIds = roleIds;
	}
	public Boolean getUserState() {
		return userState;
	}
	public void setUserState(Boolean userState) {
		this.userState = userState;
	}
	public Float getUserCost() {
		return userCost;
	}
	public void setUserCost(Float userCost) {
		this.userCost = userCost;
	}
	public Boolean getEnabled() {
		return enabled;
	}
	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}
	public Collection<GrantedAuthority> getAuthorities() {
		return authorities;
	}
	public void setAuthorities(Collection<GrantedAuthority> authorities) {
		this.authorities = authorities;
	}
	public String getFax() {
		return fax;
	}
	public void setFax(String fax) {
		this.fax = fax;
	}
	public String getMsn() {
		return msn;
	}
	public void setMsn(String msn) {
		this.msn = msn;
	}
	public String getOfficeAddress() {
		return officeAddress;
	}
	public void setOfficeAddress(String officeAddress) {
		this.officeAddress = officeAddress;
	}
	public Long getOrgNo() {
		return orgNo;
	}
	public void setOrgNo(Long orgNo) {
		this.orgNo = orgNo;
	}
	public String getOrgName() {
		return orgName;
	}
	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}
	public Byte getDataFlag() {
		return dataFlag;
	}
	public void setDataFlag(Byte dataFlag) {
		this.dataFlag = dataFlag;
	}
	public String getRoles() {
		return roles;
	}
	public void setRoles(String roles) {
		this.roles = roles;
	}
	public String getPinyin() {
		return pinyin;
	}
	public void setPinyin(String pinyin) {
		this.pinyin = pinyin;
	}
	public Date getBirthday() {
		return birthday;
	}
	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	public String getIcCard() {
		return icCard;
	}
	public void setIcCard(String icCard) {
		this.icCard = icCard;
	}
	public Boolean getSex() {
		return sex;
	}
	public void setSex(Boolean sex) {
		this.sex = sex;
	}
	public List<RoleDTO> getUserRoles() {
		return userRoles;
	}
	public void setUserRoles(List<RoleDTO> userRoles) {
		this.userRoles = userRoles;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getRemoteHost() {
		return remoteHost;
	}
	public void setRemoteHost(String remoteHost) {
		this.remoteHost = remoteHost;
	}
	public String getRemotePort() {
		return remotePort;
	}
	public void setRemotePort(String remotePort) {
		this.remotePort = remotePort;
	}
	public Long[] getTechnicalGroupIds() {
		return technicalGroupIds;
	}
	public void setTechnicalGroupIds(Long[] technicalGroupIds) {
		this.technicalGroupIds = technicalGroupIds;
	}
	public Boolean getHolidayStatus() {
		return holidayStatus;
	}
	public void setHolidayStatus(Boolean holidayStatus) {
		this.holidayStatus = holidayStatus;
	}
	public Long[] getBelongsGroupIds() {
		return belongsGroupIds;
	}
	public void setBelongsGroupIds(Long[] belongsGroupIds) {
		this.belongsGroupIds = belongsGroupIds;
	}
	public String getBelongsGroup() {
		return belongsGroup;
	}
	public void setBelongsGroup(String belongsGroup) {
		this.belongsGroup = belongsGroup;
	}
	public String getRandomId() {
		return randomId;
	}
	public void setRandomId(String randomId) {
		this.randomId = randomId;
	}
	public String getRoleCode() {
		return roleCode;
	}
	public void setRoleCode(String roleCode) {
		this.roleCode = roleCode;
	}
	public String getLcallCodeOper() {
		return lcallCodeOper;
	}
	public void setLcallCodeOper(String lcallCodeOper) {
		this.lcallCodeOper = lcallCodeOper;
	}
	public String getDomain() {
		return domain;
	}
	public void setDomain(String domain) {
		this.domain = domain;
	}
	public String getDistinguishedName() {
		return distinguishedName;
	}
	public void setDistinguishedName(String distinguishedName) {
		this.distinguishedName = distinguishedName;
	}
	public String getExtension() {
		return extension;
	}
	public void setExtension(String extension) {
		this.extension = extension;
	}
	public boolean isAccountNonExpired() {
		// TODO Auto-generated method stub
		return false;
	}
	public boolean isAccountNonLocked() {
		// TODO Auto-generated method stub
		return false;
	}
	public boolean isCredentialsNonExpired() {
		// TODO Auto-generated method stub
		return false;
	}
	public boolean isEnabled() {
		return enabled!=null?enabled:false;
	}
	
	public UserDTO(){
		super();
	}
	public UserDTO(Long userId, String loginName, String username, String firstName, String lastName, String password, String email, String moblie, String phone, String officePhone, String description, String remark, String job, Long[] roleIds, Boolean userState, Float userCost, Boolean enabled, Collection<GrantedAuthority> authorities, String fax, String msn, String officeAddress, Long orgNo, String orgName) {
		super();
		this.userId = userId;
		this.loginName = loginName;
		this.username = username;
		this.firstName = firstName;
		this.lastName = lastName;
		this.password = password;
		this.email = email;
		this.moblie = moblie;
		this.phone = phone;
		this.officePhone = officePhone;
		this.description = description;
		this.remark = remark;
		this.job = job;
		this.roleIds = roleIds;
		this.userState = userState;
		this.userCost = userCost;
		this.enabled = enabled;
		this.authorities = authorities;
		this.fax = fax;
		this.msn = msn;
		this.officeAddress = officeAddress;
		this.orgNo = orgNo;
		this.orgName = orgName;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((authorities == null) ? 0 : authorities.hashCode());
		result = prime * result
				+ ((belongsGroup == null) ? 0 : belongsGroup.hashCode());
		result = prime * result + Arrays.hashCode(belongsGroupIds);
		result = prime * result
				+ ((birthday == null) ? 0 : birthday.hashCode());
		result = prime * result
				+ ((dataFlag == null) ? 0 : dataFlag.hashCode());
		result = prime * result
				+ ((description == null) ? 0 : description.hashCode());
		result = prime
				* result
				+ ((distinguishedName == null) ? 0 : distinguishedName
						.hashCode());
		result = prime * result + ((domain == null) ? 0 : domain.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((enabled == null) ? 0 : enabled.hashCode());
		result = prime * result
				+ ((extension == null) ? 0 : extension.hashCode());
		result = prime * result + ((fax == null) ? 0 : fax.hashCode());
		result = prime * result
				+ ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result
				+ ((fullName == null) ? 0 : fullName.hashCode());
		result = prime * result
				+ ((holidayStatus == null) ? 0 : holidayStatus.hashCode());
		result = prime * result + ((icCard == null) ? 0 : icCard.hashCode());
		result = prime * result + ((job == null) ? 0 : job.hashCode());
		result = prime * result
				+ ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result
				+ ((lcallCodeOper == null) ? 0 : lcallCodeOper.hashCode());
		result = prime * result
				+ ((loginName == null) ? 0 : loginName.hashCode());
		result = prime * result + ((moblie == null) ? 0 : moblie.hashCode());
		result = prime * result + ((msn == null) ? 0 : msn.hashCode());
		result = prime * result
				+ ((officeAddress == null) ? 0 : officeAddress.hashCode());
		result = prime * result
				+ ((officePhone == null) ? 0 : officePhone.hashCode());
		result = prime * result + ((orgName == null) ? 0 : orgName.hashCode());
		result = prime * result + ((orgNo == null) ? 0 : orgNo.hashCode());
		result = prime * result
				+ ((password == null) ? 0 : password.hashCode());
		result = prime * result + ((phone == null) ? 0 : phone.hashCode());
		result = prime * result + ((pinyin == null) ? 0 : pinyin.hashCode());
		result = prime * result
				+ ((position == null) ? 0 : position.hashCode());
		result = prime * result
				+ ((randomId == null) ? 0 : randomId.hashCode());
		result = prime * result + ((remark == null) ? 0 : remark.hashCode());
		result = prime * result
				+ ((remoteHost == null) ? 0 : remoteHost.hashCode());
		result = prime * result
				+ ((remotePort == null) ? 0 : remotePort.hashCode());
		result = prime * result
				+ ((roleCode == null) ? 0 : roleCode.hashCode());
		result = prime * result + Arrays.hashCode(roleIds);
		result = prime * result + ((roles == null) ? 0 : roles.hashCode());
		result = prime * result + ((sex == null) ? 0 : sex.hashCode());
		result = prime * result + Arrays.hashCode(technicalGroupIds);
		result = prime * result
				+ ((userCost == null) ? 0 : userCost.hashCode());
		result = prime * result + ((userId == null) ? 0 : userId.hashCode());
		result = prime * result
				+ ((userRoles == null) ? 0 : userRoles.hashCode());
		result = prime * result
				+ ((userState == null) ? 0 : userState.hashCode());
		result = prime * result
				+ ((username == null) ? 0 : username.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserDTO other = (UserDTO) obj;
		if (authorities == null) {
			if (other.authorities != null)
				return false;
		} else if (!authorities.equals(other.authorities))
			return false;
		if (belongsGroup == null) {
			if (other.belongsGroup != null)
				return false;
		} else if (!belongsGroup.equals(other.belongsGroup))
			return false;
		if (!Arrays.equals(belongsGroupIds, other.belongsGroupIds))
			return false;
		if (birthday == null) {
			if (other.birthday != null)
				return false;
		} else if (!birthday.equals(other.birthday))
			return false;
		if (dataFlag == null) {
			if (other.dataFlag != null)
				return false;
		} else if (!dataFlag.equals(other.dataFlag))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (distinguishedName == null) {
			if (other.distinguishedName != null)
				return false;
		} else if (!distinguishedName.equals(other.distinguishedName))
			return false;
		if (domain == null) {
			if (other.domain != null)
				return false;
		} else if (!domain.equals(other.domain))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (enabled == null) {
			if (other.enabled != null)
				return false;
		} else if (!enabled.equals(other.enabled))
			return false;
		if (extension == null) {
			if (other.extension != null)
				return false;
		} else if (!extension.equals(other.extension))
			return false;
		if (fax == null) {
			if (other.fax != null)
				return false;
		} else if (!fax.equals(other.fax))
			return false;
		if (firstName == null) {
			if (other.firstName != null)
				return false;
		} else if (!firstName.equals(other.firstName))
			return false;
		if (fullName == null) {
			if (other.fullName != null)
				return false;
		} else if (!fullName.equals(other.fullName))
			return false;
		if (holidayStatus == null) {
			if (other.holidayStatus != null)
				return false;
		} else if (!holidayStatus.equals(other.holidayStatus))
			return false;
		if (icCard == null) {
			if (other.icCard != null)
				return false;
		} else if (!icCard.equals(other.icCard))
			return false;
		if (job == null) {
			if (other.job != null)
				return false;
		} else if (!job.equals(other.job))
			return false;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		if (lcallCodeOper == null) {
			if (other.lcallCodeOper != null)
				return false;
		} else if (!lcallCodeOper.equals(other.lcallCodeOper))
			return false;
		if (loginName == null) {
			if (other.loginName != null)
				return false;
		} else if (!loginName.equals(other.loginName))
			return false;
		if (moblie == null) {
			if (other.moblie != null)
				return false;
		} else if (!moblie.equals(other.moblie))
			return false;
		if (msn == null) {
			if (other.msn != null)
				return false;
		} else if (!msn.equals(other.msn))
			return false;
		if (officeAddress == null) {
			if (other.officeAddress != null)
				return false;
		} else if (!officeAddress.equals(other.officeAddress))
			return false;
		if (officePhone == null) {
			if (other.officePhone != null)
				return false;
		} else if (!officePhone.equals(other.officePhone))
			return false;
		if (orgName == null) {
			if (other.orgName != null)
				return false;
		} else if (!orgName.equals(other.orgName))
			return false;
		if (orgNo == null) {
			if (other.orgNo != null)
				return false;
		} else if (!orgNo.equals(other.orgNo))
			return false;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;
		if (phone == null) {
			if (other.phone != null)
				return false;
		} else if (!phone.equals(other.phone))
			return false;
		if (pinyin == null) {
			if (other.pinyin != null)
				return false;
		} else if (!pinyin.equals(other.pinyin))
			return false;
		if (position == null) {
			if (other.position != null)
				return false;
		} else if (!position.equals(other.position))
			return false;
		if (randomId == null) {
			if (other.randomId != null)
				return false;
		} else if (!randomId.equals(other.randomId))
			return false;
		if (remark == null) {
			if (other.remark != null)
				return false;
		} else if (!remark.equals(other.remark))
			return false;
		if (remoteHost == null) {
			if (other.remoteHost != null)
				return false;
		} else if (!remoteHost.equals(other.remoteHost))
			return false;
		if (remotePort == null) {
			if (other.remotePort != null)
				return false;
		} else if (!remotePort.equals(other.remotePort))
			return false;
		if (roleCode == null) {
			if (other.roleCode != null)
				return false;
		} else if (!roleCode.equals(other.roleCode))
			return false;
		if (!Arrays.equals(roleIds, other.roleIds))
			return false;
		if (roles == null) {
			if (other.roles != null)
				return false;
		} else if (!roles.equals(other.roles))
			return false;
		if (sex == null) {
			if (other.sex != null)
				return false;
		} else if (!sex.equals(other.sex))
			return false;
		if (!Arrays.equals(technicalGroupIds, other.technicalGroupIds))
			return false;
		if (userCost == null) {
			if (other.userCost != null)
				return false;
		} else if (!userCost.equals(other.userCost))
			return false;
		if (userId == null) {
			if (other.userId != null)
				return false;
		} else if (!userId.equals(other.userId))
			return false;
		if (userRoles == null) {
			if (other.userRoles != null)
				return false;
		} else if (!userRoles.equals(other.userRoles))
			return false;
		if (userState == null) {
			if (other.userState != null)
				return false;
		} else if (!userState.equals(other.userState))
			return false;
		if (username == null) {
			if (other.username != null)
				return false;
		} else if (!username.equals(other.username))
			return false;
		return true;
	}

}
