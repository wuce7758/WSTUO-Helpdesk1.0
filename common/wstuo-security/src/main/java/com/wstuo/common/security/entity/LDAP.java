package com.wstuo.common.security.entity;

import com.wstuo.common.entity.BaseEntity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * LDAP Entity class
 */
@SuppressWarnings({ "rawtypes", "serial" })
@Entity
public class LDAP extends BaseEntity{
	@Id
    @GeneratedValue( strategy = GenerationType.AUTO )
    private long ldapId;
    private String ldapName;//名称
    private String ldapURL;//路径
    private String prot;//端口
    private String adminName;//管理员
    private String adminPassword;//密码
    private String searchFilter;//基本识别名称(BaseDN) 
    private String searchBase;//搜索过滤器 
    private String ldapType;//类型
    private String ifImport;//是否已导入
    private String connStatus;//连接状态
    private String ifAllowAutoImport;//是否允许自动导入
	private Integer updateSum=0;//更新记录
    public long getLdapId(  )
    {
        return ldapId;
    }

    public void setLdapId( long ldapId )
    {
        this.ldapId = ldapId;
    }

    public String getLdapName(  )
    {
        return ldapName;
    }

    public void setLdapName( String ldapName )
    {
        this.ldapName = ldapName;
    }

    public String getLdapURL(  )
    {
        return ldapURL;
    }

    public void setLdapURL( String ldapURL )
    {
        this.ldapURL = ldapURL;
    }

    public String getProt(  )
    {
        return prot;
    }

    public void setProt( String prot )
    {
        this.prot = prot;
    }

    public String getAdminName(  )
    {
        return adminName;
    }

    public void setAdminName( String adminName )
    {
        this.adminName = adminName;
    }

    public String getAdminPassword(  )
    {
        return adminPassword;
    }

    public void setAdminPassword( String adminPassword )
    {
        this.adminPassword = adminPassword;
    }

    public String getSearchFilter(  )
    {
        return searchFilter;
    }

    public void setSearchFilter( String searchFilter )
    {
        this.searchFilter = searchFilter;
    }

    public String getSearchBase(  )
    {
        return searchBase;
    }

    public void setSearchBase( String searchBase )
    {
        this.searchBase = searchBase;
    }

    public String getLdapType(  )
    {
        return ldapType;
    }

    public void setLdapType( String ldapType )
    {
        this.ldapType = ldapType;
    }

	public String getIfImport() {
		return ifImport;
	}

	public void setIfImport(String ifImport) {
		this.ifImport = ifImport;
	}

	public String getConnStatus() {
		return connStatus;
	}

	public void setConnStatus(String connStatus) {
		this.connStatus = connStatus;
	}

	public String getIfAllowAutoImport() {
		return ifAllowAutoImport;
	}

	public void setIfAllowAutoImport(String ifAllowAutoImport) {
		this.ifAllowAutoImport = ifAllowAutoImport;
	}
	
	public Integer getUpdateSum() {
		return updateSum;
	}

	public void setUpdateSum(Integer updateSum) {
		this.updateSum = updateSum;
	}
    
}
