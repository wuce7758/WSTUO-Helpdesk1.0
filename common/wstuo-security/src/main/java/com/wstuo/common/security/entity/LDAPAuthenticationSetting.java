package com.wstuo.common.security.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/***
 * 验证设置实体类
 */
@Entity
@Table(name="ldapauthenticationsetting")
public class LDAPAuthenticationSetting {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long ldapId;
	private String ldapType;//LDAP服务器类型
    private String ldapServer;//LDAP服务器地址  
    private String prot;//LDAP端口号
    private String adminName;//用户名
    private String adminPassword;//密码
    private String searchFilter;//搜索条件
    private String searchBase;//搜索DN
    private String authenticationMode;//验证方式(userName:用户名验证，userNamePassword:用户名密码验证)
    private Boolean enableAuthentication = false;//LDAP验证是否启动
    private String commonUserName;//公共帐号
    private String commonPassword;//公共帐号的密码
    
    private String kerberosType;//Kerberos服务器类型
    private String kerberosServer;//kerberos服务器地址  
    private String kerberosName;//kerberos用户名
    private String kerberosPassword;//kerberos密码
    private String kerberosBase;//kerberos域名称
    private Boolean kerberosEnableAuthentication = false;//Kerberos验证是否启动
	public Long getLdapId() {
		return ldapId;
	}
	public void setLdapId(Long ldapId) {
		this.ldapId = ldapId;
	}
	public String getLdapServer() {
		return ldapServer;
	}
	public void setLdapServer(String ldapServer) {
		this.ldapServer = ldapServer;
	}
	public String getProt() {
		return prot;
	}
	public void setProt(String prot) {
		this.prot = prot;
	}
	public String getAdminName() {
		return adminName;
	}
	public void setAdminName(String adminName) {
		this.adminName = adminName;
	}
	public String getAdminPassword() {
		return adminPassword;
	}
	public void setAdminPassword(String adminPassword) {
		this.adminPassword = adminPassword;
	}
	public String getSearchFilter() {
		return searchFilter;
	}
	public void setSearchFilter(String searchFilter) {
		this.searchFilter = searchFilter;
	}
	public String getSearchBase() {
		return searchBase;
	}
	public void setSearchBase(String searchBase) {
		this.searchBase = searchBase;
	}
	public String getLdapType() {
		return ldapType;
	}
	public void setLdapType(String ldapType) {
		this.ldapType = ldapType;
	}
	public String getAuthenticationMode() {
		return authenticationMode;
	}
	public void setAuthenticationMode(String authenticationMode) {
		this.authenticationMode = authenticationMode;
	}
	public String getCommonUserName() {
		return commonUserName;
	}
	public void setCommonUserName(String commonUserName) {
		this.commonUserName = commonUserName;
	}
	public String getCommonPassword() {
		return commonPassword;
	}
	public void setCommonPassword(String commonPassword) {
		this.commonPassword = commonPassword;
	}
	public String getKerberosServer() {
		return kerberosServer;
	}
	public void setKerberosServer(String kerberosServer) {
		this.kerberosServer = kerberosServer;
	}
	public String getKerberosName() {
		return kerberosName;
	}
	public void setKerberosName(String kerberosName) {
		this.kerberosName = kerberosName;
	}
	public String getKerberosPassword() {
		return kerberosPassword;
	}
	public void setKerberosPassword(String kerberosPassword) {
		this.kerberosPassword = kerberosPassword;
	}
	public String getKerberosBase() {
		return kerberosBase;
	}
	public void setKerberosBase(String kerberosBase) {
		this.kerberosBase = kerberosBase;
	}
	public String getKerberosType() {
		return kerberosType;
	}
	public void setKerberosType(String kerberosType) {
		this.kerberosType = kerberosType;
	}
	public Boolean getEnableAuthentication() {
		return enableAuthentication;
	}
	public void setEnableAuthentication(Boolean enableAuthentication) {
		this.enableAuthentication = enableAuthentication;
	}
	public Boolean getKerberosEnableAuthentication() {
		return kerberosEnableAuthentication;
	}
	public void setKerberosEnableAuthentication(Boolean kerberosEnableAuthentication) {
		this.kerberosEnableAuthentication = kerberosEnableAuthentication;
	}
	
}
