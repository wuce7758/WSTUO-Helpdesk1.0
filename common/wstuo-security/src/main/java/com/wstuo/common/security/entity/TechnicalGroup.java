package com.wstuo.common.security.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

/**
 * Technical Group Entity Class
 */
@Entity
public class TechnicalGroup {
	@Id
    @GeneratedValue( strategy = GenerationType.AUTO )
	private Long technicalGroupId;
	private String technicalGroupName;
	private String description;
	@JoinColumn(name="tgId_roleId")
	@OneToMany(fetch = FetchType.LAZY)
	private List<Role> roles;
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public List<Role> getRoles() {
		return roles;
	}
	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}
	public Long getTechnicalGroupId() {
		return technicalGroupId;
	}
	public void setTechnicalGroupId(Long technicalGroupId) {
		this.technicalGroupId = technicalGroupId;
	}
	public String getTechnicalGroupName() {
		return technicalGroupName;
	}
	public void setTechnicalGroupName(String technicalGroupName) {
		this.technicalGroupName = technicalGroupName;
	}
}