package com.wstuo.common.security.enums;


/**
*
* Enum class UserState
* @author wstuo
*
*/
public enum UserState
{	disabled,
    available;
}
