package com.wstuo.common.security.service;

import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.security.dto.OrganizationDTO;
import com.wstuo.common.security.dto.OrganizationQueryDTO;
import com.wstuo.common.security.dto.OrganizationTreeDTO;
import com.wstuo.common.security.entity.Organization;
import com.wstuo.common.security.entity.Role;
import com.wstuo.common.security.entity.User;

import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 机构业务类接口
 */
public interface IOrganizationService
{
    /**
     * 分页查询机构信息.
     * @param organizationQueryDTO 分页查询数据DTO：OrganizationQueryDTO
     * @return 分页数据：PageDTO
     */
    PageDTO findPagerOrganization( OrganizationQueryDTO organizationQueryDTO );

    /**
     * 根据ID数组分页查找机构.
     * @param organizationQueryDTO 分页查询DTO：OrganizationQueryDTO
     * @return 分页数据：PageDTO
     */
    PageDTO findPagerOrganizationByIds( OrganizationQueryDTO organizationQueryDTO );

    /**
     * 查找服务机构.
     * @return 机构DTO集合：List<OrganizationDTO>
     */
    List<OrganizationDTO> findAllOrganization();

    /**
     * 根据编号查找机构信息.
     * @param id 机构编号：Long orgNo
     * @return 机构数据：OrganizationDTO
     */
    OrganizationDTO findOrganizationById( Long id );

    /**
     * 查找服务机构（树形）.
     * @param companyNo
     * @return 树形集合：List<OrganizationTreeDTO>
     */
    List<OrganizationTreeDTO> findOrgTreeDtos(Long companyNo);
    
    /**
     * 查找登陆用户负责的服务机构（树形）.
     * @param companyNo
     * @return 树形集合：List<OrganizationTreeDTO>
     */
    List<OrganizationTreeDTO> findOrgTreeDtosByUser(Long companyNo);
    
    
    /**
     * 根据父机构ID查子机构树结构数据
     * @param parentOrgNo
     * @return List<OrganizationTreeDTO>
     */
    List<OrganizationTreeDTO> findSubOrgTreeDtos(Long parentOrgNo);

    /**
     * 查找服务机构树
     * @return 树形集合：List<OrganizationTreeDTO>
     */
    List<OrganizationTreeDTO> findServicesOrgTreeDtos(  );

    /**
     * 查找选择机构树
     * @return 树形集合：List<OrganizationTreeDTO>
     */
    List<OrganizationTreeDTO> findOrganizationSelectTree(  );

    /**
     * 删除服务机构.
     * @param orgNo 机构编号：Long orgNo
     */
    void removeServicesOrg( Long orgNo );

    /**
     * 批量删除机构.
     * @param orgNos 机构编号数组：Long[] orgNos
     */
    void deleteOrgs( Long[] orgNos );

    /**
     * 修改机构.
     * @param dto 机构DTO：OrganizationDTO dto
     */
    void updateOrganization( OrganizationDTO dto );

    /**
     * 修改机构父节构
     * @param dto 机构DTO：OrganizationDTO
     */
    void moveOrg( OrganizationDTO dto );

    /**
     * 复制机构.
     * @param dto 机构DTO：OrganizationDTO
     */
    Long copyOrg( OrganizationDTO dto );

    /**
     * 查找内部机构树
     * @return 树形集合：List<OrganizationTreeDTO>
     */
    List<OrganizationTreeDTO> findInnerOrgTreeDtos(  );

    /**
     * 根据父机构ID获取子机构
     * @param orgNo
     * @param flag
     * @return List<OrganizationDTO>
     */
    
    List<OrganizationDTO> findOrganizationsByType(Long orgNo,String flag);
    

    /**
     * 根据机构ID获取对应的角色
     * @param orgNo
     * @return Set<Role>
     */
    Set<Role> findRolesById( Long orgNo );

    /**
     * 导出机构信息.
     * @param qdto
     * @return InputStream
     */
    InputStream exportOrganization(OrganizationQueryDTO qdto);
    
    /**
     * 导入机构信息.
     * @param importFile
     * @return import result
     */
    String importOrganization(String importFile);
    
    /**
     * 获取外包服务商公司
     * @return Organization
     */
    OrganizationDTO findServiceProvidersCompany();
    
    /**
     * 根据数组id,查找所有机构
     * @param orgNo
     * @param flag
     * @return List<OrganizationDTO>
     */
    List<OrganizationDTO> findOrganizationsByTypeArray(Long[] orgNo,String[] flag);
    
    /**
     * 反转查找机构，也就是反方向把其父机构一起查找出来
     * @param orgNos
     * @return Long[]
     */
    Long[] reverFindOrg(Long[] orgNos);
    
    /**
	 * 根据用户获取机构负责人
	 * @param userId 用户ID
	 * @param level 取当前、取上一个、取上上个机构....的负责人(取当前用户机构负责人为0，上一个为1，依次类推)
	 * @return 返回机构负责人
	 */
	User findOrganizationOwnerByUser(Long userId , Long level);
    
    /**
     * 根据用户名查询机构
     * @param loginName
     * @return Map
     */
    @SuppressWarnings("rawtypes")
    Map findOrgGroupHeadsByname(String loginName);
    
    /**
     * 遍历机构负责人
     * @param orgNo
     * @param map
     * @param n 层次
     * @return Map
     */
    @SuppressWarnings("rawtypes")
    Map findOrgByChild(Long orgNo,Map map,int n);
	/**
     * 根据机构名称查询
     * @param orgName
     * @return Organization
     */
	Organization findOrgByName(String orgName);
	/**
     * 根据机构名称和父节点名称查询
     * @param orgName
     * @return Organization
     */
	Organization findOrgByOrgNameOrparentOrgName(String orgNames,String parentOrgName);
	/**
	 * 根据编号获得角色列表
	 */
	List<Long> findRolesByOrgNo(Long orgNo);
	
	/**
     * 判断分类是否存在
     * @param ec
     */
    boolean isCategoryExisted(OrganizationDTO dto);
    
    /**
     * 判断编辑时分类是否存在
     * @param dto
     * @return
     */
    boolean isCategoryExistdOnEdit(OrganizationDTO dto);
    /**
     * 根据上传图片名称获取LOGO图片流
     * @param companyNo
     * @return InputStream
     */
    InputStream getImageUrlforimageStream(String actionType);
}
