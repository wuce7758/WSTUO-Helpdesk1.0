package com.wstuo.common.security.validatecode;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.wstuo.common.exception.ApplicationException;
import com.wstuo.common.security.enums.SessionName;

/**
 * 验证码生成过滤器
 * @author wstuo
 * 
 * 1. 页面加上验证码的html (注册,登录...)
 * <tr>
 * 	<td align="right" style="text-align: right;">验证码<span style="color: red;">*</span>:</td>
 *	<td><input id="validateCode" name="j_validate" style="width:100px"/>&nbsp;<img id="validateCode_img" src="security/captcha.jpg"  style="vertical-align: middle;border:solid 2px black;cursor:pointer;"/></td>
 * </tr>
 *
 * 2. web.xml中加上验证码过滤器及映射
 *	<!-- Captcha filter -->
 *	<filter>
 *	   <filter-name>CaptchaFilter</filter-name>
 *	   <filter-class>com.wstuo.common.security.validatecode.CaptchaFilter</filter-class>
 *	</filter>
 * <filter-mapping>
 *	   <filter-name>CaptchaFilter</filter-name>
 *	   <url-pattern>/wstuo_login</url-pattern>
 *	</filter-mapping>
 *	<!-- Captcha img URL -->
 *	<filter-mapping>
 *		<filter-name>CaptchaFilter</filter-name>
 *		<url-pattern>/security/captcha.jpg</url-pattern>
 *	</filter-mapping>
 */

public class CaptchaFilter extends HttpServlet implements Filter {
	/**
	 * 判断用户输入的验证码是否正确
	 */
	private static final long serialVersionUID = -5838154525730151323L;

	public void init(FilterConfig config) throws ServletException {
	}

	public void destroy() {
	}

	public void doFilter(ServletRequest servletRequest,
			ServletResponse servletResponse, FilterChain filterChain)
			throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest) servletRequest;
		HttpServletResponse response = (HttpServletResponse) servletResponse;
		HttpSession session = request.getSession();
		String servletPath = request.getServletPath();
		//获取验证码
		if(servletPath.matches("/security/captcha.jpg")) {
			response.setContentType("image/jpeg");
			//禁止图像缓存。
			response.setHeader("Pragma", "no-cache");
			response.setHeader("Cache-Control", "no-cache");
			response.setDateHeader("Expires", 0);
			//参数：宽、高、字符数、干扰量
			ValidateCode vCode = new ValidateCode(64,22,4,200);
			session.setAttribute(SessionName.CAPTCHA.getName(), vCode.getCode());
			vCode.write(response.getOutputStream());
			return;
		}
		
		//校验验证码
		String code = request.getParameter("j_validate");
		String loginModel=request.getParameter("j_loginModel");
		String realCode = (String) session.getAttribute("captcha");
		
		
		if(loginModel!=null && loginModel.equals("cookie")){
			
			filterChain.doFilter(request, response);
			return;
		}
		
		if(code==null || realCode==null || !code.equals(realCode)) {
			
			session.setAttribute(SessionName.CAPTCHA.getName(), new Double(Math.random()));
			throw new ApplicationException("validate_error_verificationCode");
			
		} else {
			session.setAttribute(SessionName.CAPTCHA.getName(), new Double(Math.random()));
			filterChain.doFilter(request, response);
		}
	}
}