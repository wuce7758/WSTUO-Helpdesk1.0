package com.wstuo.common.security.validatecode;

import java.io.*;
import java.net.URL;
import java.awt.Font;

/**
 * ttf字体文件
 * @author dsna
 *
 */
public class ImgFontByte {
	//验证码字体
	public static Font CAPTCHA_FONT = null;

	/**
	 * 加载ttf字体流
	 * @return InputStream
	 */
	public Font getFont(int fontHeight) {
		try{
			if(CAPTCHA_FONT ==null) {
				URL fileURL = this.getClass().getResource("/conf/captchaFont.ttf");
				File file = new File(fileURL.getPath());
				Font baseFont = Font.createFont(Font.TRUETYPE_FONT, new FileInputStream(file));
				CAPTCHA_FONT = baseFont.deriveFont(Font.PLAIN, fontHeight);
			}
		} catch(Exception e) {
			CAPTCHA_FONT = new Font("Arial",Font.PLAIN, fontHeight);
		}
		return CAPTCHA_FONT;
	}
}