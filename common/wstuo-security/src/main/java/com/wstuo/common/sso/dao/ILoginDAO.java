package com.wstuo.common.sso.dao;


public interface ILoginDAO
{
    public int login( String account );
}
