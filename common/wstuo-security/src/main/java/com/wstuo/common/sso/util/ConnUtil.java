package com.wstuo.common.sso.util;

import java.io.IOException;
import java.util.Properties;

public class ConnUtil {
	private Properties pro;
	private static final ConnUtil cu = new ConnUtil();

	private ConnUtil() {
		init();
	}

	public static ConnUtil getInstance() {
		return cu;
	}

	private void init() {
		this.pro = new Properties();
		try {
			this.pro.load(getClass().getResourceAsStream(
					"/addressCofig.properties"));
		} catch (IOException e) {
			System.out.println("连接失败");
			e.printStackTrace();
		}
	}

	public String getIp() {
		return this.pro.getProperty("ip");
	}
	
	public String getPort() {
		return this.pro.getProperty("port");
	}
	
}
