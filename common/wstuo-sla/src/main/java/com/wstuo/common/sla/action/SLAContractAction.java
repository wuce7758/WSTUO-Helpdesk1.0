package com.wstuo.common.sla.action;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.xwork2.ActionSupport;
import com.wstuo.common.config.category.dto.EventCategoryDTO;
import com.wstuo.common.config.category.service.IEventCategoryService;
import com.wstuo.common.config.dictionary.dto.DataDictionaryItemsDTO;
import com.wstuo.common.config.dictionary.service.IDataDictionaryItemsService;
import com.wstuo.common.security.dto.OrganizationDTO;
import com.wstuo.common.security.dto.OrganizationQueryDTO;
import com.wstuo.common.security.service.IOrganizationService;
import com.wstuo.common.sla.dto.SLAContractDTO;
import com.wstuo.common.sla.dto.SLAContractQueryDTO;
import com.wstuo.common.sla.service.ISLAContractService;
import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.exception.ApplicationException;

/**
 * SLA Action类
 * 
 * @author QXY
 */
@SuppressWarnings("serial")
public class SLAContractAction extends ActionSupport {

	/**
	 * SLAContractAction错误日志
	 */
	public static final Logger LOGGER = Logger
			.getLogger(SLAContractAction.class);
	/**
	 * Sla协议DTO
	 */
	private SLAContractDTO slaContractDTO = new SLAContractDTO();
	/**
	 * Sla协议查询DTO
	 */
	private SLAContractQueryDTO qdto = new SLAContractQueryDTO();
	/**
	 * 数据字典DTO
	 */
	private List<DataDictionaryItemsDTO> dataDicItemsDTOList = new ArrayList<DataDictionaryItemsDTO>();
	/**
	 * 机构DTO
	 */
	private List<OrganizationDTO> orgDTOList = new ArrayList<OrganizationDTO>();
	
	private List<SLAContractDTO> slaContractDTOList = new ArrayList<SLAContractDTO>();
	/**
	 *SLA协议业务类接口
	 */
	@Autowired
	private ISLAContractService slaContractService;
	/**
	 * 数据字典项业务类接口.
	 */
	@Autowired
	private IDataDictionaryItemsService dataDictionaryItemsService;
	/**
	 * 机构业务类接口
	 */
	@Autowired
	private IOrganizationService organizationService;
	/**
	 * 分类业务接口
	 */
	@Autowired
	private IEventCategoryService eventCategoryService;
	/**
	 * 页数
	 */
	private int page = 1;
	/**
	 * 记录数
	 */
	private int rows = 10;
	/**
	 * 分页DTO
	 */
	private PageDTO pageDTO;
	/**
	 * sla协议No数组
	 */
	private Long[] contractNos;
	/**
	 * sla协议No
	 */
	private Long contractNo;
	/**
	 * 被服务机构对应字符串
	 */
	private String byServicesNosStr;
	/**
	 * 返回结果
	 */
	private boolean result;
	/**
	 * 被服务机构字符串
	 */
	private String servicesNosStr;
	/**
	 * 结果
	 */
	private String effect;
	/**
	 * 导入文件
	 */
	private File importFile;
	/**
	 * 导出文件
	 */
	private InputStream exportFile;
	/**
	 * 分类DTO
	 */
	private EventCategoryDTO eventCategoryDTO = new EventCategoryDTO();
	/**
	 * 排序的属性
	 */
	private String sidx;
	/**
	 * 排序规则
	 */
	private String sord;
	/**
	 * 机构查询DTO
	 */
	private OrganizationQueryDTO organizationQueryDTO = new OrganizationQueryDTO();

	public String getSidx() {
		return sidx;
	}

	public void setSidx(String sidx) {
		this.sidx = sidx;
	}

	public String getSord() {
		return sord;
	}

	public void setSord(String sord) {
		this.sord = sord;
	}

	public String getServicesNosStr() {
		return servicesNosStr;
	}

	public void setServicesNosStr(String servicesNosStr) {
		this.servicesNosStr = servicesNosStr;
	}

	public EventCategoryDTO getEventCategoryDTO() {
		return eventCategoryDTO;
	}

	public void setEventCategoryDTO(EventCategoryDTO eventCategoryDTO) {
		this.eventCategoryDTO = eventCategoryDTO;
	}

	public String getEffect() {
		return effect;
	}

	public void setEffect(String effect) {
		this.effect = effect;
	}

	public File getImportFile() {
		return importFile;
	}

	public void setImportFile(File importFile) {
		this.importFile = importFile;
	}

	public InputStream getExportFile() {
		return exportFile;
	}

	public void setExportFile(InputStream exportFile) {
		this.exportFile = exportFile;
	}

	public String getByServicesNosStr() {
		return byServicesNosStr;
	}

	public void setByServicesNosStr(String byServicesNosStr) {
		this.byServicesNosStr = byServicesNosStr;
	}

	

	public List<DataDictionaryItemsDTO> getDataDicItemsDTOList() {

		return dataDicItemsDTOList;
	}

	public void setDataDicItemsDTOList(
			List<DataDictionaryItemsDTO> dataDicItemsDTOList) {

		this.dataDicItemsDTOList = dataDicItemsDTOList;
	}

	public List<OrganizationDTO> getOrgDTOList() {

		return orgDTOList;
	}

	public void setOrgDTOList(List<OrganizationDTO> orgDTOList) {

		this.orgDTOList = orgDTOList;
	}

	public Long getContractNo() {

		return contractNo;
	}

	public void setContractNo(Long contractNo) {

		this.contractNo = contractNo;
	}

	public Long[] getContractNos() {

		return contractNos;
	}

	public void setContractNos(Long[] contractNos) {

		this.contractNos = contractNos;
	}

	public SLAContractDTO getSlaContractDTO() {

		return slaContractDTO;
	}

	public void setSlaContractDTO(SLAContractDTO slaContractDTO) {

		this.slaContractDTO = slaContractDTO;
	}

	public SLAContractQueryDTO getQdto() {

		return qdto;
	}

	public void setQdto(SLAContractQueryDTO qdto) {

		this.qdto = qdto;
	}

	public int getPage() {

		return page;
	}

	public void setPage(int page) {

		this.page = page;
	}

	public int getRows() {

		return rows;
	}

	public void setRows(int rows) {

		this.rows = rows;
	}

	public PageDTO getPageDTO() {

		return pageDTO;
	}

	public void setPageDTO(PageDTO pageDTO) {

		this.pageDTO = pageDTO;
	}

	public boolean isResult() {
		return result;
	}

	public void setResult(boolean result) {
		this.result = result;
	}

	public List<SLAContractDTO> getSlaContractDTOList() {
		return slaContractDTOList;
	}

	public void setSlaContractDTOList(List<SLAContractDTO> slaContractDTOList) {
		this.slaContractDTOList = slaContractDTOList;
	}

	/**
	 * 分页查找SLA列表.
	 * 
	 * @return String sla分页DTO
	 */
	public String find() {
		int start = (page - 1) * rows;

		pageDTO = slaContractService.findSLAContractByPager(qdto, start, rows,
				sidx, sord);

		pageDTO.setPage(page);
		pageDTO.setRows(rows);

		return SUCCESS;
	}
	
	/**
	 * 查询所有sla协议
	 * @return
	 */
	public String findAll(){
		slaContractDTOList = slaContractService.findAllSLAContract();
		return "showSLAContractList";
	}

	/**
	 * 根据ID查找SLA
	 * 
	 * @return String sla协议DTO
	 */
	public String findSLAContract() {

		slaContractDTO = slaContractService.findById(contractNo);

		return "showSLAContract";
	}

	/**
	 * 查找SLA详细信息
	 * 
	 * @return String sla协议DTO
	 */
	public String findSLAContractShowRules() {

		slaContractDTO = slaContractService.findById(contractNo);
		return "showSLAContractAndRules";
	}

	

	/**
	 * 保存SLA信息
	 * 
	 * @return String
	 */
	public String save() {
		slaContractDTO.setByServicesNos(slaContractDTO.getByServicesNosFormByServicesNosStr(byServicesNosStr));
		slaContractDTO.setServiceDirNos(slaContractDTO.getServicesDirNosFormservicesNosStr(servicesNosStr));
		try {
			slaContractService.saveSLAContract(slaContractDTO);
		} catch (Exception e) {
			if (e.toString().indexOf("ERROR_WORKING_DAYS_NOT_SET") != -1)
				throw new ApplicationException("ERROR_WORKING_DAYS_NOT_SET");
			if (e.toString().indexOf("ERROR_SERVICE_PROVIDER_HAS_SLA") != -1)
				throw new ApplicationException("ERROR_SERVICE_PROVIDER_HAS_SLA");
		}
		return SUCCESS;
	}

	/**
	 * 根据服务机构查找SLA
	 * 
	 * @return String sla分页DTO
	 */
	public String findByOrgServices() {
		if (contractNo != null && contractNo != 0) {

			Long[] bySerivcesNos = slaContractService
					.findByServicesNosByContractNo(contractNo);

			if (bySerivcesNos!=null&&bySerivcesNos.length > 0) {

				organizationQueryDTO.setOrgNos(bySerivcesNos);

				int start = (page - 1) * rows;

				organizationQueryDTO.setStart(start);
				organizationQueryDTO.setLimit(rows);
				pageDTO = organizationService
						.findPagerOrganizationByIds(organizationQueryDTO);
				pageDTO.setPage(page);
				pageDTO.setRows(rows);
			}else
				pageDTO=new PageDTO();

		}

		return SUCCESS;

	}

	/**
	 *根据服务机构No查询分类DTO
	 * @return 分类DTO
	 */
	public String findByServiceDir() {
		if (contractNo != null && contractNo != 0) {
			Long[] serivcesNos = slaContractService
					.findByServicesDirNosByContractNo(contractNo);
			if (serivcesNos.length > 0) {

				eventCategoryDTO.setEventIds(serivcesNos);

				int start = (page - 1) * rows;

				eventCategoryDTO.setStart(start);
				eventCategoryDTO.setLimit(rows);
				pageDTO = eventCategoryService.findByServiceNos(eventCategoryDTO);
				pageDTO.setPage(page);
				pageDTO.setRows(rows);

			}else
				pageDTO=new PageDTO();
		}
		return SUCCESS;
	}

	/**
	 * 保存SLA
	 * 
	 * @return String 无
	 */
	public String merge() {
		slaContractDTO.setByServicesNos(slaContractDTO.getByServicesNosFormByServicesNosStr(byServicesNosStr));
		slaContractDTO.setServiceDirNos(slaContractDTO.getServicesDirNosFormservicesNosStr(servicesNosStr));
		try {
			slaContractService.mergeSLAContract(slaContractDTO);
		} catch (Exception e) {
			throw new ApplicationException("ERROR_WORKING_DAYS_NOT_SET");
		}

		return SUCCESS;
	}

	/**
	 * 删除SLA
	 * 
	 * @return result 是否成功
	 */
	public String delete() {

		try {

			result = slaContractService.removeSLAContracts(contractNos);
		} catch (Exception ex) {
			if(ex.getMessage().indexOf("ERROR_DATA_CAN_NOT_DELETE")!=-1){
				throw new ApplicationException("ERROR_DATA_CAN_NOT_DELETE", ex);
			}
		}
		return "result";
	}

	/**
	 * 查询sla协议详情
	 * 
	 * @return String sla协议DTO，数据字典DTO，机构DTO
	 */
	public String findSlaSortAndOrg() {

		if (slaContractDTO.getContractNo() != 0) {

			slaContractDTO = slaContractService.findById(slaContractDTO
					.getContractNo());
		}

		dataDicItemsDTOList = dataDictionaryItemsService
				.findDataDictionaryItemByGroupNo(1L);
		orgDTOList = organizationService.findAllOrganization();
		OrganizationDTO odto = new OrganizationDTO();
		odto.setOrgName("请选择");
		DataDictionaryItemsDTO datadto = new DataDictionaryItemsDTO();
		datadto.setDname("请选择");
		orgDTOList.add(0, odto);
		dataDicItemsDTOList.add(0, datadto);

		return "showSlaSortAndOrg";
	}

	/**
	 * 根据ID查询sla协议
	 * 
	 * @return String sla协议DTO
	 */
	public String findSlaById() {

		slaContractDTO = slaContractService.findById(contractNo);
		return "singleSLAContract";
	}

	/**
	 * 查询sla协议详情
	 * 
	 * @return String sla协议DTO，数据字典DTO，机构DTO
	 */
	public String findSlaSortAndOrgShowContract() {

		if (slaContractDTO.getContractNo() != 0) {

			slaContractDTO = slaContractService.findById(slaContractDTO
					.getContractNo());
		}

		dataDicItemsDTOList = dataDictionaryItemsService
				.findDataDictionaryItemByGroupNo(1L);
		orgDTOList = organizationService.findAllOrganization();
		OrganizationDTO odto = new OrganizationDTO();
		odto.setOrgName("请选择");
		DataDictionaryItemsDTO datadto = new DataDictionaryItemsDTO();
		datadto.setDname("请选择");
		orgDTOList.add(0, odto);
		dataDicItemsDTOList.add(0, datadto);
		return "showSlaSortAndOrgAndContract";
	}

	/**
	 * 返回sla列表页面
	 * 
	 * @return String 无
	 */
	public String back() {

		return "showBack";
	}

	/**
	 * 导入SLA.
	 * 
	 * @return 导入结果
	 */
	public String importSLA() {
		effect = slaContractService.importSLA(importFile.getAbsolutePath());
		return "effect";
	}

	/**
	 * 导出SLA.
	 * @return 导出文件
	 */
	public String exportSLA() {
		exportFile = slaContractService.exportSLA(qdto);
		return "exportFile";
	}

}