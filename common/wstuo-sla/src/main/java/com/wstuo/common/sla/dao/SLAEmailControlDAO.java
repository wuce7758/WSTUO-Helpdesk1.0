package com.wstuo.common.sla.dao;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;

import com.wstuo.common.sla.entity.SLAEmailControl;
import com.wstuo.common.dao.BaseDAOImplHibernate;
import com.wstuo.common.util.StringUtils;

/**
 * sla协议发送邮件业务类
 * @author QXY
 *
 */
public class SLAEmailControlDAO extends BaseDAOImplHibernate<SLAEmailControl>  implements ISLAEmailControlDAO{
	/**
	 * 查询根据sla协议发送的邮件
	 */
	public List<SLAEmailControl> findSLAEmailControl(SLAEmailControl slaEmailControl) {
		final DetachedCriteria dc =DetachedCriteria.forClass(SLAEmailControl.class);
		if(slaEmailControl!=null){
			if(slaEmailControl.getEno()!=null){
				dc.add(Restrictions.eq("eno", slaEmailControl.getEno()));
			}
			if(StringUtils.hasText(slaEmailControl.getRuleType())){
				dc.add(Restrictions.eq("ruleType", slaEmailControl.getRuleType()));
			}
			if(StringUtils.hasText(slaEmailControl.getReferType())){
				dc.add(Restrictions.eq("referType",slaEmailControl.getReferType()));
			}
			if(slaEmailControl.getRuleNo()!=null){
				dc.add(Restrictions.eq("ruleNo", slaEmailControl.getRuleNo()));
			}
			if(slaEmailControl.getUpgradeTime()!=null){
				dc.add(Restrictions.eq("upgradeTime", slaEmailControl.getUpgradeTime()));
			}
			if(slaEmailControl.getUpdateLevelUserId()!=null){
				dc.add(Restrictions.eq("updateLevelUserId", slaEmailControl.getUpdateLevelUserId()));
			}
		}
		return super.getHibernateTemplate().findByCriteria(dc);

	}

	
}
