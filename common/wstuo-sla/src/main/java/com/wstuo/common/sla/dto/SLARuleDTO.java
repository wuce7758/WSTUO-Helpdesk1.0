package com.wstuo.common.sla.dto;

import java.util.Date;

import com.wstuo.common.rules.dto.RuleDTO;

/**
 * SLA Rule DTO
 * 
 * @author QXY
 * 
 */
public class SLARuleDTO extends RuleDTO {

	private Long contractNo;
	private String contractName;
	private Integer respondTime;
	private Integer finishTime;
	private String showRespondTime;
	private String showFinishTime;
	private Long salience;
	private Date beginTime;
	private Date endTime;

	public Long getSalience() {
		return salience;
	}

	public void setSalience(Long salience) {
		this.salience = salience;
	}

	public Date getBeginTime() {
		return beginTime;
	}

	public void setBeginTime(Date beginTime) {
		this.beginTime = beginTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public String getShowRespondTime() {
		return showRespondTime;
	}

	public void setShowRespondTime(String showRespondTime) {
		this.showRespondTime = showRespondTime;
	}

	public String getShowFinishTime() {
		return showFinishTime;
	}

	public void setShowFinishTime(String showFinishTime) {
		this.showFinishTime = showFinishTime;
	}

	public Long getContractNo() {

		return contractNo;
	}

	public void setContractNo(Long contractNo) {

		this.contractNo = contractNo;
	}

	public String getContractName() {

		return contractName;
	}

	public void setContractName(String contractName) {

		this.contractName = contractName;
	}

	public Integer getRespondTime() {
		return respondTime;
	}

	public void setRespondTime(Integer respondTime) {
		this.respondTime = respondTime;
	}

	public Integer getFinishTime() {
		return finishTime;
	}

	public void setFinishTime(Integer finishTime) {
		this.finishTime = finishTime;
	}

}