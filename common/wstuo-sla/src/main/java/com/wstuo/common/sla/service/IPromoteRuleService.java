package com.wstuo.common.sla.service;


import com.wstuo.common.sla.dto.PromoteRuleDTO;
import com.wstuo.common.sla.dto.PromoteRuleQueryDTO;
import com.wstuo.common.sla.entity.PromoteRule;
import com.wstuo.common.dto.PageDTO;

/**
 * 自动升级规则业务类接口.
 * @author QXY
 *
 */
public interface IPromoteRuleService {
	

	/**
	 * 分页查找自动升级列表.
	 * @param qdto 查询DTO
	 * @param start 开始数据行
	 * @param limit 每页数据条数
	 * @return PageDTO 分页数据
	 */
	PageDTO findPromoteRuleByContractNo(PromoteRuleQueryDTO qdto, int start,int limit,String sidx,String sord);
	
	/**
	 * 根据ID查找自动升级信息.
	 * @param no 自动升级编号
	 * @return PromoteRule 自动升级数据
	 */
	PromoteRule findPromoteRuleById(Long no);

	/**
	 * 修改自动升级信息.
	 * @param  promoteRule 数据DTO
	 * @return PromoteRule
	 */
	PromoteRule mergePromoteRuleEntity(PromoteRuleDTO promoteRule);

	/**
	 * 根据ID删除自动升级.
	 * @param no 自动升级ID
	 */
	void removePromoteRule(Long no);

	/**
	 * 批量删除自动升级信息.
	 * @param nos 自动升级ID数组
	 */
	void removePromoteRules(Long[] nos);

	/**
	 * 保存自动升级.
	 * @param promoteRule
	 */
	void savePromoteRuleEntity(PromoteRuleDTO promoteRule);
	
	/**
	 * 根据ID查找自动升级信息DTO.
	 * @param no 自动升级编号
	 * @return PromoteRuleDTO 自动升级数据
	 */
    PromoteRuleDTO findPromoteRuleDTOById(Long no);
	
}
