package com.wstuo.common.activemq.service;

import java.util.List;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;

import org.springframework.jms.support.converter.MessageConversionException;

import com.wstuo.common.activemq.dto.ActiveMQInfoDTO;
import com.wstuo.common.activemq.dto.ActiveMQMessageInfoDTO;
import com.wstuo.common.activemq.dto.QueueConfigureDTO;

/**
 * 消息队列Service Interface
 * @author WSTUO
 *
 */
public interface IActiveMQService {
	
	/**
	 * 获取全部队列
	 * @return List<ActiveMQInfoDTO>
	 */
	List<ActiveMQInfoDTO> showQueues();
	/**
	 * 获取队列中的详细信息
	 * @param queueName 队列名称
	 * @return List<ActiveMQMessageInfoDTO>
	 */
	List<ActiveMQMessageInfoDTO> showQueueinfo(String queueName);
	/**
	 * 删除消息
	 * @param messageId
	 * @param queueName
	 * @return boolean
	 */
	boolean deleteMessage(String messageId,String queueName);
	/**
	 * 查询队列执行状态
	 * @param queueName
	 * @return boolean
	 */
	boolean findQueueStatus(String queueName);
	/**
	 * 根据ID查询配置信息
	 * @return QueueConfigureDTO
	 */
	QueueConfigureDTO findQueueConfigure();
	/**
	 * 保存队列参数配置信息
	 * @param dto QueueConfigureDTO
	 */
	void saveQueueConfigure(QueueConfigureDTO dto);
	/**
	 * MQ接收消息转换方法
	 */
	Object fromMessage(Message msg) throws JMSException,MessageConversionException;
	
	Message toMessage(Object obj, Session session) throws JMSException,MessageConversionException;
}
