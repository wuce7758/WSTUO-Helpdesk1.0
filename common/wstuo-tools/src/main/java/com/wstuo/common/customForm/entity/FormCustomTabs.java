package com.wstuo.common.customForm.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;

@Entity
public class FormCustomTabs {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long tabId;	
	private String tabName;	
	private String tabAttrNos;		
	@Lob
	private String tabAttrsConent;
	@Lob
	private String tabAttrsDecode;
	@Lob
	private String tabAttrsConentDecode;
	
	public String getTabAttrsConentDecode() {
		return tabAttrsConentDecode;
	}

	public void setTabAttrsConentDecode(String tabAttrsConentDecode) {
		this.tabAttrsConentDecode = tabAttrsConentDecode;
	}

	public String getTabAttrsDecode() {
		return tabAttrsDecode;
	}

	public void setTabAttrsDecode(String tabAttrsDecode) {
		this.tabAttrsDecode = tabAttrsDecode;
	}

	public String getTabAttrsConent() {
		return tabAttrsConent;
	}

	public void setTabAttrsConent(String tabAttrsConent) {
		this.tabAttrsConent = tabAttrsConent;
	}

	public Long getTabId() {
		return tabId;
	}

	public void setTabId(Long tabId) {
		this.tabId = tabId;
	}

	public String getTabName() {
		return tabName;
	}

	public void setTabName(String tabName) {
		this.tabName = tabName;
	}

	public String getTabAttrNos() {
		return tabAttrNos;
	}

	public void setTabAttrNos(String tabAttrNos) {
		this.tabAttrNos = tabAttrNos;
	}
	
	
}
