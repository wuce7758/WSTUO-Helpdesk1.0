package com.wstuo.common.customForm.service;

import java.util.List;

import com.wstuo.common.customForm.dto.FormCustomDTO;
import com.wstuo.common.customForm.dto.FormCustomQueryDTO;
import com.wstuo.common.customForm.dto.SimpleFileDTO;
import com.wstuo.common.dto.PageDTO;

/**
 * 自定义表单Service层
 * @author Haley
 *
 */
public interface IFormCustomService {

	/**
	 * 添加一个自定义表单
	 * @param dto
	 */
	Long saveFormCustom(FormCustomDTO dto);
	
	/**
	 * 删除一个自定义表单
	 * @param dto
	 */
	void deleteFormCustom(Long[] formCustomIds);
	
	/**
	 * 根据id 查找表单
	 * @param formCustomId
	 * @return
	 */
	FormCustomQueryDTO findFormCustomById(Long formCustomId);
	
	/**
	 * 修改一个自定义表单
	 * @param dto
	 */
	void updateFormCustom(FormCustomDTO dto);
	
	/**
	 * 查询默认的表单
	 * @return
	 */
	FormCustomDTO findIsDefault(String type);
	
	PageDTO findFormCustomPager(FormCustomQueryDTO queryDto, String sord,
			String sidx);
	/**
	 * 根据服务目录Id查询自定义表单
	 * @param queryDto
	 * @return
	 */
	FormCustomDTO findFormCustomByServiceDirId(FormCustomQueryDTO queryDto);
	
	/**
	 *  查询所有的自定义表单模板
	 * @param isTemplate
	 * @return
	 */
	List<FormCustomQueryDTO> findSimilarFormCustom(FormCustomQueryDTO queryDto);
	
	/**
	 * 查询所有自定义表单
	 * @return
	 */
	List<FormCustomQueryDTO> findAllFormCustom();
	
	/**
	 * 查询所有的请求表单
	 * @return
	 */
	List<FormCustomQueryDTO> findAllRequestFormCustom();
	
	/**
	 * 根据配置项的分类ID查询自定义表单
	 * @return
	 */
	FormCustomDTO findFormCustomByCiCategoryNo(Long ciCategoryNo);
	
	/**
	 * 是否存在 相同表單名稱
	 * @param FormCustomQueryDTO
	 * @return Boolean
	 */
	Boolean isFormCustomNameExisted(FormCustomQueryDTO dto);
	/**
	 * 是否存在 相同表單名稱
	 * @param FormCustomQueryDTO
	 * @return Boolean
	 */
	Boolean isFormCustomNameExistedOnEdit(FormCustomQueryDTO dto);
	
	SimpleFileDTO generateTemplateWithCategory(Long categoryNo);
}
