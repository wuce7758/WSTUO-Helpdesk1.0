package com.wstuo.common.customForm.service;

import org.springframework.transaction.annotation.Transactional;

import com.wstuo.common.customForm.dto.FormCustomTabsDTO;

public interface IFormCustomTabsService {
	
	/**
	 * 保存表单选项卡
	 * @param formCustomTabsDTO
	 * @return 
	 */
	Long saveFormCustomTabs(FormCustomTabsDTO formCustomTabsDTO);
	
	
	/**
	 * 查询表单选项卡
	 * @param tabsId
	 * @return
	 */
	FormCustomTabsDTO findFormCustomTabsByTabsId(Long tabsId);
	
	
	/**
	 * 保存表单选项卡
	 * @param formCustomTabsDTO
	 * @return 
	 */
	Long updateFormCustomTabs(FormCustomTabsDTO formCustomTabsDTO);

}
