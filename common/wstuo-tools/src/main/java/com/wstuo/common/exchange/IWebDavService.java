package com.wstuo.common.exchange;

import java.util.List;

import com.wstuo.common.tools.dto.EmailDTO;
import com.wstuo.common.tools.dto.EmailMessageDTO;
import com.wstuo.common.tools.dto.EmailServerDTO;

/**
 * Web Dav Service Interface
 * @author WSTUO
 *
 */
public interface IWebDavService {
	 /**
     * 连接测试
     * @return boolean
     */
    boolean emailConnTest(EmailServerDTO emailServerDTO);
    
    /**
     * 获取邮箱信息
     * @return List<EmailMessageDTO>
     */
    List<EmailMessageDTO> getInboxMail();
    
    /**
     * 邮件发送
     * @param dto
     * @return boolean
     */
    boolean sendEmail(EmailDTO dto);
}
