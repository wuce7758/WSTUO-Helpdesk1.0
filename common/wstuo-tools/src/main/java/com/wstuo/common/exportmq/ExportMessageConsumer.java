package com.wstuo.common.exportmq;

import org.hibernate.internal.SessionFactoryImpl;
import org.springframework.context.ApplicationContext;

import com.wstuo.common.activemq.consumer.IMessageConsumer;
import com.wstuo.common.tools.dto.ExportQueryDTO;
import com.wstuo.common.util.AppliactionBaseListener;

/**
 * 消息消费类
 * @author will
 *
 */

public class ExportMessageConsumer {
	private SessionFactoryImpl sessionFactory;
	
	public SessionFactoryImpl getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactoryImpl sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	/**
	 * 请求导出消费信息方法
	 * @param dto
	 */
	public void exportConsumer(ExportQueryDTO dto) {
/*		TransactionSynchronizationManager.unbindResourceIfPossible(sessionFactory);
		TenantIdResolver tenantIdResolver = (TenantIdResolver)sessionFactory.getCurrentTenantIdentifierResolver();
		if(tenantIdResolver!=null){
			tenantIdResolver.setTenantId(dto.getTenantId());
		}*/
		ApplicationContext ctx=AppliactionBaseListener.ctx;
		//判断是否存在
		if(ctx != null && dto != null
				&& ctx.containsLocalBean(dto.getType()+"MessageConsumer")){
			IMessageConsumer consumer=(IMessageConsumer) ctx.getBean(dto.getType()+"MessageConsumer");
			consumer.messageConsumer(dto);
		}
	}
	
}
