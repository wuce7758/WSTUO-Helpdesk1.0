package com.wstuo.common.noticeRule.dao;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import com.wstuo.common.noticeRule.dto.NoticeRuleDTO;
import com.wstuo.common.noticeRule.entity.NoticeRule;
import com.wstuo.common.dao.BaseDAOImplHibernate;
import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.util.DaoUtils;
import com.wstuo.common.util.StringUtils;
/**
 * 通知规则DAO类
 * @author QXY
 *
 */
public class NoticeRuleDAO extends BaseDAOImplHibernate<NoticeRule> implements INoticeRuleDAO {
	/**
	 * 分页查询通知规则
	 * @param noticeRuleDTO
	 * @param start
	 * @param limit
	 * @param sord
	 * @param sidx
	 * @return PageDTO
	 */
	public PageDTO findPager(NoticeRuleDTO noticeRuleDTO, int start, int limit,String sord, String sidx){
		
		 DetachedCriteria dc = DetachedCriteria.forClass( NoticeRule.class );
		 if(noticeRuleDTO!=null){
			 
			 if (StringUtils.hasText( noticeRuleDTO.getNoticeRuleName()) ){
				dc.add(Restrictions.like("noticeRuleName", noticeRuleDTO.getNoticeRuleName(),MatchMode.ANYWHERE));
			 }
			 if (noticeRuleDTO.getNoticeRuleNo()!=null ){
				dc.add(Restrictions.like("noticeRuleNo", noticeRuleDTO.getNoticeRuleNo(),MatchMode.ANYWHERE));
			 }
			 if (StringUtils.hasText( noticeRuleDTO.getNoticeRuleType() ) ){
				dc.add(Restrictions.like("noticeRuleType", noticeRuleDTO.getNoticeRuleType(),MatchMode.ANYWHERE));
			 }
			 if(noticeRuleDTO.getSmsNotice()!=null && noticeRuleDTO.getSmsNotice()){
				 dc.add(Restrictions.eq("smsNotice", noticeRuleDTO.getSmsNotice()));
			 }
			 if(noticeRuleDTO.getImNotice()!=null && noticeRuleDTO.getImNotice()){
				 dc.add(Restrictions.eq("imNotice", noticeRuleDTO.getImNotice()));
			 }
			 if(noticeRuleDTO.getMailNotice()!=null && noticeRuleDTO.getMailNotice()){
				 dc.add(Restrictions.eq("mailNotice", noticeRuleDTO.getMailNotice()));
			 }
			 if(noticeRuleDTO.getPushNotice()!=null && noticeRuleDTO.getPushNotice()){
				 dc.add(Restrictions.eq("pushNotice", noticeRuleDTO.getPushNotice()));
			 }
			 if(noticeRuleDTO.getUseStatus()!= null ){
				 dc.add(Restrictions.eq("useStatus", noticeRuleDTO.getUseStatus()));
			 }
			 if(StringUtils.hasText(noticeRuleDTO.getModule())){
				dc.add(Restrictions.like("module", noticeRuleDTO.getModule(),MatchMode.ANYWHERE));
			 }
		 }
		//排序
         dc = DaoUtils.orderBy(sidx, sord, dc);
		 return super.findPageByCriteria(dc, start, limit);
	};
}
