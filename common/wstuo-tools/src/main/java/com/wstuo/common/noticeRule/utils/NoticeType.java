package com.wstuo.common.noticeRule.utils;

public enum NoticeType {
	    EmailNotice("email"),
	    IMNotice("im"),
		SMSNotice("sms"),
	    PUSHNotice("push");
	
		// 定义私有变量
		private String nCode;

		// 构造函数，枚举类型只能为私有
		private NoticeType(String _nCode) {
			this.nCode = _nCode;
		}

		public String getValue() {
			return nCode;
		}

}
