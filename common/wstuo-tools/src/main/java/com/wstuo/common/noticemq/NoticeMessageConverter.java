package com.wstuo.common.noticemq;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.support.converter.MessageConversionException;
import org.springframework.jms.support.converter.MessageConverter;

import com.wstuo.common.activemq.service.IActiveMQService;
import com.wstuo.common.tools.dto.NoticeSendDTO;

/**
 * Email Message Converter
 * @author will
 *
 */
public class NoticeMessageConverter implements MessageConverter {
	@Autowired
	private IActiveMQService activeMQService;
	public NoticeSendDTO fromMessage(Message msg) throws JMSException,
			MessageConversionException {
		return (NoticeSendDTO) activeMQService.fromMessage(msg);
	}

	/**
	 * 发送信息
	 */
    public Message toMessage(Object obj, Session session) throws JMSException,MessageConversionException {
    	return activeMQService.toMessage(obj,session);
	}

}
