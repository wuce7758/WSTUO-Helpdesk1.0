package com.wstuo.common.scheduled.dto;

import java.util.Date;

/**
 * 行程搜索DTO
 * @author WSTUO
 */
public class ScheduleQueryDTO {
	private Long companyNo;
	private Long ecategoryNo;
	private String requestCode;
	private Long priorityNo;
	private Long statusNo;
	private Long technicianNo;
	private String scheduleType;
	private Date startTime;
	private Date endTime;
	private Integer start;
    private Integer limit;
    private String module;
	public Long getCompanyNo() {
		return companyNo;
	}
	public void setCompanyNo(Long companyNo) {
		this.companyNo = companyNo;
	}
	public Long getEcategoryNo() {
		return ecategoryNo;
	}
	public void setEcategoryNo(Long ecategoryNo) {
		this.ecategoryNo = ecategoryNo;
	}
	public String getRequestCode() {
		return requestCode;
	}
	public void setRequestCode(String requestCode) {
		this.requestCode = requestCode;
	}
	public Long getPriorityNo() {
		return priorityNo;
	}
	public void setPriorityNo(Long priorityNo) {
		this.priorityNo = priorityNo;
	}
	public Long getStatusNo() {
		return statusNo;
	}
	public void setStatusNo(Long statusNo) {
		this.statusNo = statusNo;
	}
	public Long getTechnicianNo() {
		return technicianNo;
	}
	public void setTechnicianNo(Long technicianNo) {
		this.technicianNo = technicianNo;
	}
	public String getScheduleType() {
		return scheduleType;
	}
	public void setScheduleType(String scheduleType) {
		this.scheduleType = scheduleType;
	}
	public Date getStartTime() {
		return startTime;
	}
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
	public Date getEndTime() {
		return endTime;
	}
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	public Integer getStart() {
		return start;
	}
	public void setStart(Integer start) {
		this.start = start;
	}
	public Integer getLimit() {
		return limit;
	}
	public void setLimit(Integer limit) {
		this.limit = limit;
	}
	public String getModule() {
		return module;
	}
	public void setModule(String module) {
		this.module = module;
	}
	
}
