package com.wstuo.common.tools.action;


import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.xwork2.ActionSupport;
import com.wstuo.common.tools.entity.PagesSet;
import com.wstuo.common.tools.service.IPagesSetSerice;

/**
 * 页面设置Action
 * @author Candy
 *
 */
public class PagesSetAction extends ActionSupport{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Autowired
	private IPagesSetSerice pagesSetSerice;
	private PagesSet entity;

	public PagesSet getEntity() {
		return entity;
	}

	public void setEntity(PagesSet entity) {
		this.entity = entity;
	}
	
	/**
	 * 更新页面设置
	 */
	public void updatePagesSet(){
		
		pagesSetSerice.updatePagesSet(entity);
	}
	/**
	 * 显示页面设置
	 * @return PagesSet
	 */
	public String showPagesSet(){
		
		entity=pagesSetSerice.showPagesSet();
		return "entity";
	}
}
