package com.wstuo.common.tools.dao;

import java.util.Date;
import java.util.List;

import com.wstuo.common.tools.dto.CostDTO;
import com.wstuo.common.tools.entity.Cost;
import com.wstuo.common.dao.IEntityDAO;
import com.wstuo.common.dto.PageDTO;

/**
 * cost DAO interface class 
 * @author WSTUO
 *
 */
public interface ICostDAO extends IEntityDAO<Cost>{
	
	/**
	 * 成本分页查询
	 * @param dto
	 * @param start
	 * @param limit
	 * @param sidx
	 * @param sord
	 * @return PageDTO
	 */
    PageDTO findPagerCost( CostDTO dto, int start, int limit, String sidx, String sord  );
    /**
	 * 行程进展及成本
	 * @param queryDto
	 * @param enos
	 * @param sidx
	 * @param sord
	 * @return List<Cost>
	 */
	List<Cost> findScheduleCost(CostDTO queryDto,List<Long> enos,String sidx, String sord);
	
	/**
	 * 时间段冲突
	 * @param dto
	 * @return Cost
	 */
	Cost costPeriodConflict(CostDTO dto);
	
	
	List<Cost> findScheduleCost( Long scheduleTaskId,Date startTime,Date endTime, String taskCycle);
	
	/**
	 * 根据任务ID删除进展及成本
	 * @param scheduleTaskIds
	 */
	void deleteCostBySchedule(final Long[] scheduleTaskIds);
}
