package com.wstuo.common.tools.dao;

import com.wstuo.common.tools.entity.SMSAccount;
import com.wstuo.common.dao.IEntityDAO;
/**
 * 短信DAO接口类
 * @author QXY
 * date 2010-10-11
 */
public interface ISMSAccountDAO
    extends IEntityDAO<SMSAccount>
{
	/**
	 * 获取短信账号信息
	 * @return 短信账号Entity
	 */
	SMSAccount getSmsAccount();
}
