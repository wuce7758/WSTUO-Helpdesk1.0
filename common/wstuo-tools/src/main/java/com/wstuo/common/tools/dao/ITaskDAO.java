package com.wstuo.common.tools.dao;

import java.util.List;

import com.wstuo.common.tools.dto.TaskDTO;
import com.wstuo.common.tools.dto.TaskQueryDTO;
import com.wstuo.common.tools.entity.Task;
import com.wstuo.common.dao.IEntityDAO;
import com.wstuo.common.dto.PageDTO;

/**
 * 任务DAO接口类
 * @author QXY
 *
 */
public interface ITaskDAO extends IEntityDAO<Task>{
	
	List<Long> findDeleteTask( Long[] ids );
	List<Long> findDeleteTaskCycle( Long[] ids );
	
	/**
	 * 任务分页查询
	 * @param taskQueryDto
	 * @param start
	 * @param limit
	 * @return PageDTO
	 */
	PageDTO findPagerTaskDAO(TaskQueryDTO taskQueryDto,
			int start, int limit,String sidx,String sord);
	/**
	 * 查询我的全部任务
	 * @param qdto TaskQueryDTO
	 * @return List<Task>
	 */
	List<Task> findMyAllTask(TaskQueryDTO qdto);
	/**
	 * 查询指定时间段任务
	 * @param taskDTO
	 * @return boolean
	 */
	boolean findSameTimeTask(TaskDTO taskDTO);
	List<Task> findTimeRangeTask(TaskDTO taskDTO);
	
	void endTask(Long[] ids);
}
