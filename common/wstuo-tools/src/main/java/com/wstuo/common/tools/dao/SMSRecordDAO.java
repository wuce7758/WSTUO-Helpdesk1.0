package com.wstuo.common.tools.dao;

import java.util.Calendar;
import java.util.GregorianCalendar;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import com.wstuo.common.tools.dto.SMSRecordDTO;
import com.wstuo.common.tools.entity.SMSRecord;
import com.wstuo.common.dao.BaseDAOImplHibernate;
import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.util.DaoUtils;
import com.wstuo.common.util.StringUtils;

/**
 * 短信发送记录DAO
 * @author QXY
 *
 */
public class SMSRecordDAO extends BaseDAOImplHibernate<SMSRecord> implements ISMSRecordDAO{


	
	/**
	 * 分页查询短信记录.
	 * @param dto
	 * @param start
	 * @param limit
	 * @param sidx
	 * @param sord
	 * @return PageDTO
	 */
    public PageDTO findPager(SMSRecordDTO dto, int start, int limit, String sidx, String sord){
    	
        DetachedCriteria dc = DetachedCriteria.forClass(SMSRecord.class);

        if (dto!= null){

            if(StringUtils.hasText(dto.getContent())){//短信内容
                dc.add(Restrictions.like("content",dto.getContent(),MatchMode.ANYWHERE));
            }
            
            if(StringUtils.hasText(dto.getSender())){//发送人
                dc.add(Restrictions.like("sender",dto.getSender(),MatchMode.ANYWHERE));
            }
            
            if(StringUtils.hasText(dto.getMobile())){//收信人
                dc.add(Restrictions.like("mobile",dto.getMobile(),MatchMode.ANYWHERE));
            }

            if (dto.getSendTime_start() != null && dto.getSendTime_end() != null) {
				Calendar endTimeCl = new GregorianCalendar();
				endTimeCl.setTime(dto.getSendTime_end());
				endTimeCl.set(Calendar.DATE, endTimeCl.get(Calendar.DATE) + 1);

				dc.add(Restrictions.and(
						Restrictions.le("sendTime", endTimeCl.getTime()),
						Restrictions.ge("sendTime", dto.getSendTime_start())));
			}

            
        }

        //排序
        dc = DaoUtils.orderBy(sidx, sord, dc);
        return super.findPageByCriteria(dc, start, limit);
    }
	
}
