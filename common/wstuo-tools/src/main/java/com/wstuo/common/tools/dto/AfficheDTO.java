package com.wstuo.common.tools.dto;

import com.wstuo.common.dto.BaseDTO;

import java.util.Date;

/**
 * 公告信息属性DTO
 * 
 * @author brain date 2010/9/10
 * **/
@SuppressWarnings("serial")
public class AfficheDTO extends BaseDTO {
	/*
	 * Notice Information Code
	 * 
	 * *
	 */
	private Long affId;

	/*
	 * Notice Title *
	 */
	private String affTitle;

	/*
	 * Notice launch time *
	 */
	private Date affStart;

	/*
	 * Notice the end of time *
	 */
	private Date affEnd;

	/*
	 * Who created *
	 */
	private String affCreator;

	/*
	 * Created *
	 */
	private Date affCreaTime;

	/*
	 * Notice Content *
	 */
	private String affContents;

	/*
	 * Visible state *
	 */
	private Integer visibleState;

	/*
	 * Notice by email *
	 */
	private Boolean noticeByEmal = false;

	/*
	 * 所属客户 *
	 */
	private Long companyNo;

	public AfficheDTO() {
		super();
	}

	public Long getAffId() {
		return affId;
	}

	public void setAffId(Long affId) {
		this.affId = affId;
	}

	public String getAffTitle() {
		return affTitle;
	}

	public void setAffTitle(String affTitle) {
		this.affTitle = affTitle;
	}

	public Date getAffStart() {
		return affStart;
	}

	public void setAffStart(Date affStart) {
		this.affStart = affStart;
	}

	public Date getAffEnd() {
		return affEnd;
	}

	public void setAffEnd(Date affEnd) {
		this.affEnd = affEnd;
	}

	public String getAffCreator() {
		return affCreator;
	}

	public void setAffCreator(String affCreator) {
		this.affCreator = affCreator;
	}

	public Date getAffCreaTime() {
		return affCreaTime;
	}

	public void setAffCreaTime(Date d) {
		this.affCreaTime = d;
	}

	public String getAffContents() {
		return affContents;
	}

	public void setAffContents(String affContents) {
		this.affContents = affContents;
	}

	public Long getCompanyNo() {
		return companyNo;
	}

	public void setCompanyNo(Long companyNo) {
		this.companyNo = companyNo;
	}

	public Integer getVisibleState() {
		return visibleState;
	}

	public void setVisibleState(Integer visibleState) {
		this.visibleState = visibleState;
	}

	public Boolean getNoticeByEmal() {
		return noticeByEmal;
	}

	public void setNoticeByEmal(Boolean noticeByEmal) {
		this.noticeByEmal = noticeByEmal;
	}

}
