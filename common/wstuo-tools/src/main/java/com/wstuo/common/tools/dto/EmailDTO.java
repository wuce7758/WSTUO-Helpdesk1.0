package com.wstuo.common.tools.dto;

import java.util.List;

/**
 * 邮件信息DTO
 * 
 * @author brain date 2010/10/5 remark Reference is someone else's mail server
 * **/
public class EmailDTO {
	private Integer mailId; // 邮件编号
	private List<String> to; // 接收地址
	private String receiveAddress;
	private String from; // 发送地址
	private String sendDate; // 发送日期
	private Object state; // 发送状状
	private String[] toUser; // 接收用户
	private String fromUser; // 发送人
	private String subject; // 邮件主题
	private String description; // 邮件描述
	private String bcc; // 密件抄送
	private String cc; // 抄送
	private String mailPath; // 邮件存储路径（用来保存邮件在本地路径)
	private String[] affachmentPath; // 附件的路径
	private String picPath; // 如片路径
	private String content; // 邮件内容
	private String[] affachment; // 附件
	private String fromEmail = "";
	/** 发件人姓名 */
	private String personal;

	private String moduleCode;// 关联的模块编号

	@Override
	public String toString() {
		return "EmailDTO [to=" + to + "]";
	}

	public EmailDTO() {
		super();
	}

	public String getModuleCode() {
		return moduleCode;
	}

	public void setModuleCode(String moduleCode) {
		this.moduleCode = moduleCode;
	}

	public String[] getAffachment() {
		return affachment;
	}

	public void setAffachment(String[] affachment) {
		this.affachment = affachment;
	}

	public Integer getMailId() {
		return mailId;
	}

	public void setMailId(Integer mailId) {
		this.mailId = mailId;
	}

	public List<String> getTo() {
		return to;
	}

	public void setTo(List<String> to) {
		this.to = to;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getSendDate() {
		return sendDate;
	}

	public void setSendDate(String sendDate) {
		this.sendDate = sendDate;
	}

	public Object getState() {
		return state;
	}

	public void setState(Object state) {
		this.state = state;
	}

	public String[] getToUser() {
		return toUser;
	}

	public void setToUser(String[] toUser) {
		this.toUser = toUser;
	}

	public String getFromUser() {
		return fromUser;
	}

	public void setFromUser(String fromUser) {
		this.fromUser = fromUser;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getBcc() {
		return bcc;
	}

	public void setBcc(String bcc) {
		this.bcc = bcc;
	}

	public String getCc() {
		return cc;
	}

	public void setCc(String cc) {
		this.cc = cc;
	}

	public String getMailPath() {
		return mailPath;
	}

	public void setMailPath(String mailPath) {
		this.mailPath = mailPath;
	}

	public String[] getAffachmentPath() {
		return affachmentPath;
	}

	public void setAffachmentPath(String[] affachmentPath) {
		this.affachmentPath = affachmentPath;
	}

	public String getPicPath() {
		return picPath;
	}

	public void setPicPath(String picPath) {
		this.picPath = picPath;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getFromEmail() {
		return fromEmail.replace(">", "}").replace("<", "{");
	}

	public void setFromEmail(String fromEmail) {
		this.fromEmail = fromEmail;
	}

	public String getReceiveAddress() {
		return receiveAddress;
	}

	public void setReceiveAddress(String receiveAddress) {
		this.receiveAddress = receiveAddress;
	}

	public String getPersonal() {
		return personal;
	}

	public void setPersonal(String personal) {
		this.personal = personal;
	}

}
