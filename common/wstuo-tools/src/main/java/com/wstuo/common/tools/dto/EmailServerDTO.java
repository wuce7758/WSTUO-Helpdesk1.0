package com.wstuo.common.tools.dto;

import com.wstuo.common.dto.BaseDTO;
import com.wstuo.common.util.StringUtils;

/**
 * 服务邮箱地址DTO
 * 
 * @author QXY
 * 
 */
@SuppressWarnings("serial")
public class EmailServerDTO extends BaseDTO {
	/**
	 * ServicesEmail number
	 */
	private Long emailServerId;

	/**
	 * server address
	 */
	private String smtpServerAddress;

	/**
	 * server port
	 */
	private String smtpServerPort;

	/**
	 * server address
	 */
	private String pop3ServerAddress;

	/**
	 * server port
	 */
	private String pop3ServerPort;

	/**
	 * User name
	 */
	private String userName;

	/**
	 * Password
	 */
	private String password;

	/**
	 * attestation;
	 */
	private Boolean attestation = false;

	private String emailType;// 邮件类型(exchange)
    private String emailVersion;//邮件版本

	private String exchangeHostName;// 服务器

	private String exchangeUserName;// 用户名

	private String exchangePassword;// 密码

	private String domain;// 域

	private String specifyConnectionUrl;// 指定连接的URL

	private Boolean useHttps = true;// 使用HTTPS链接

	private String exchangeEmailAccount;// 邮件账号

	/** 发件人姓名 */
	private String personal;

	/** 发件人邮件地址 */
	private String personalEmailAddress;

	public Long getEmailServerId() {
		return emailServerId;
	}

	public void setEmailServerId(Long emailServerId) {
		this.emailServerId = emailServerId;
	}

	public String getSmtpServerAddress() {
		return smtpServerAddress;
	}

	public void setSmtpServerAddress(String smtpServerAddress) {
		this.smtpServerAddress = smtpServerAddress;
	}

	public String getSmtpServerPort() {
		return smtpServerPort;
	}

	public void setSmtpServerPort(String smtpServerPort) {
		this.smtpServerPort = smtpServerPort;
	}

	public String getPop3ServerAddress() {
		return pop3ServerAddress;
	}

	public void setPop3ServerAddress(String pop3ServerAddress) {
		this.pop3ServerAddress = pop3ServerAddress;
	}

	public String getPop3ServerPort() {
		return pop3ServerPort;
	}

	public void setPop3ServerPort(String pop3ServerPort) {
		this.pop3ServerPort = pop3ServerPort;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmailType() {
		return emailType;
	}

	public void setEmailType(String emailType) {
		this.emailType = emailType;
	}

	public String getExchangeHostName() {
		return exchangeHostName;
	}

	public void setExchangeHostName(String exchangeHostName) {
		this.exchangeHostName = exchangeHostName;
	}

	public String getExchangeUserName() {
		return exchangeUserName;
	}

	public void setExchangeUserName(String exchangeUserName) {
		this.exchangeUserName = exchangeUserName;
	}

	public String getExchangePassword() {
		return exchangePassword;
	}

	public void setExchangePassword(String exchangePassword) {
		this.exchangePassword = exchangePassword;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public String getSpecifyConnectionUrl() {
		return specifyConnectionUrl;
	}

	public void setSpecifyConnectionUrl(String specifyConnectionUrl) {
		this.specifyConnectionUrl = specifyConnectionUrl;
	}

	public String getExchangeEmailAccount() {
		return exchangeEmailAccount;
	}

	public void setExchangeEmailAccount(String exchangeEmailAccount) {
		this.exchangeEmailAccount = exchangeEmailAccount;
	}

	public String getPersonal() {
		return personal;
	}

	public void setPersonal(String personal) {
		this.personal = personal;
	}

	public String getPersonalEmailAddress() {
		return personalEmailAddress;
	}

	public void setPersonalEmailAddress(String personalEmailAddress) {
		this.personalEmailAddress = personalEmailAddress;
	}

	public Boolean getAttestation() {
		return attestation;
	}

	public void setAttestation(Boolean attestation) {
		this.attestation = attestation;
	}

	public Boolean getUseHttps() {
		return useHttps;
	}

	public void setUseHttps(Boolean useHttps) {
		this.useHttps = useHttps;
	}

	public String getEmailVersion() {
		return emailVersion;
	}

	public void setEmailVersion(String emailVersion) {
		this.emailVersion = emailVersion;
	}

	public static final int EMAIL_TYPE_NONE = 0; // 没有配置
	public static final int EMAIL_TYPE_RECEIVE_EXCHANGE = 1; // Exchange接收   
	public static final int EMAIL_TYPE_EXCHANGE = 2; // Exchange 
	public static final int EMAIL_TYPE_NORMAL = 3;  //普通邮件配置
	public static final int EMAIL_TYPE_RECEIVE_NORMAL = 4; //普通邮件配置（接收）
	/**
	 * 0：没有配置
	 * 1：Exchange接收   
	 * 2：Exchange  <br><hr>
	 * emailType<br>
	 * 	普通邮件配置（且只配置了一个） ：normal；<br>
	 * 	普通邮件配置接收类型（配置了两个） ：receive；<br>
	 *  Exchange邮件配置（且只配置了一个） ：exchange；<br>
	 *  Exchange邮件配置接收类型（配置了两个） ：Receive_exchange；<br>
	 */
	public int getServerEmailType() {
		int result = EMAIL_TYPE_NONE;
		String type = this.getEmailType();
		if ( StringUtils.hasText( type ) ){
			if ( type.equals("Receive_exchange") ) {
				result = EMAIL_TYPE_RECEIVE_EXCHANGE ;
			}else if ( type.equals("exchange") ) {
				result = EMAIL_TYPE_EXCHANGE;
			}else if ( type.equals("normal") ) {
				result = EMAIL_TYPE_NORMAL;
			}else if ( type.equals("Receive") ) {
				result = EMAIL_TYPE_RECEIVE_NORMAL;
			}
		}
		return result;
	}

	@Override
	public String toString() {
		return "EmailServerDTO [emailServerId=" + emailServerId
				+ ", smtpServerAddress=" + smtpServerAddress
				+ ", smtpServerPort=" + smtpServerPort + ", pop3ServerAddress="
				+ pop3ServerAddress + ", pop3ServerPort=" + pop3ServerPort
				+ ", userName=" + userName + ", password=" + password
				+ ", attestation=" + attestation + ", emailType=" + emailType
				+ ", emailVersion=" + emailVersion + ", exchangeHostName="
				+ exchangeHostName + ", exchangeUserName=" + exchangeUserName
				+ ", exchangePassword=" + exchangePassword + ", domain="
				+ domain + ", specifyConnectionUrl=" + specifyConnectionUrl
				+ ", useHttps=" + useHttps + ", exchangeEmailAccount="
				+ exchangeEmailAccount + ", personal=" + personal
				+ ", personalEmailAddress=" + personalEmailAddress + "]";
	}
}
