package com.wstuo.common.tools.dto;

import java.util.Set;
/**
 * 消息消费DTO
 * @author QXY
 *
 */
public class MessageConsumerDTO {
	//标题
	private String subject;
	//内容
	private String content;
	//模块
	private String module;
	//发送地址
	private Set<String> to;
	//关联的eno
	private Long relateEno;

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getModule() {
		return module;
	}

	public void setModule(String module) {
		this.module = module;
	}

	public Set<String> getTo() {
		return to;
	}

	public void setTo(Set<String> to) {
		this.to = to;
	}

	public Long getRelateEno() {
		return relateEno;
	}

	public void setRelateEno(Long relateEno) {
		this.relateEno = relateEno;
	}

	
}
