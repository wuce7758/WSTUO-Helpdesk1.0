package com.wstuo.common.tools.dto;

import java.util.Date;

/**
 * 页面配置DTO
 * @author Candy
 *
 */
public class PagesSetDTO {
	private long id;
	private String paName;
	private String imgname;
	private Date createDate;
	
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getPaName() {
		return paName;
	}
	public void setPaName(String paName) {
		this.paName = paName;
	}
	public String getImgname() {
		return imgname;
	}
	public void setImgname(String imgname) {
		this.imgname = imgname;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
}
