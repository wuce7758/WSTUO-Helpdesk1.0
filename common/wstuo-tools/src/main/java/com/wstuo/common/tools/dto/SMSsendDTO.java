package com.wstuo.common.tools.dto;

import com.wstuo.common.dto.BaseDTO;

/**
 * 发送短信MQ信息类
 * @author will
 *
 */
@SuppressWarnings("serial")
public class SMSsendDTO extends BaseDTO{

	private String mobile;
	private String content;
	private boolean isSystem;
	private boolean saveHistory;
	//tenantId值
	private String tenantId;
	
	
	public String getTenantId() {
		return tenantId;
	}
	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public boolean isSystem() {
		return isSystem;
	}
	public void setSystem(boolean isSystem) {
		this.isSystem = isSystem;
	}
	public boolean isSaveHistory() {
		return saveHistory;
	}
	public void setSaveHistory(boolean saveHistory) {
		this.saveHistory = saveHistory;
	}
	
}
