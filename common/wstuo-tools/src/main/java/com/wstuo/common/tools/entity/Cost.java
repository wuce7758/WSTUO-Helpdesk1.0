package com.wstuo.common.tools.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import com.wstuo.common.security.entity.User;

/**
 * 技术员成本
 * @author WSTUO
 *
 */
@SuppressWarnings("serial")
@Entity
public class Cost extends Progress{
	
	@ManyToOne
	private User users;
	@Column(nullable=true)
	private Date opertionTime;
	@Column(nullable=true)
	private Double perHourFees;
	@Column(nullable=true)
	private Long useTime;
	@Column(nullable=true)
	private Double skillFees;
	@Column(nullable=true)
	private Double othersFees;
	@Column(nullable=true)
	private Double totalFees;
	@Column(nullable=true)
	/**物料成本*/
	private Double materialCost;
	@ManyToOne //任务
	private Task scheduleTask;
	private Date scheduleTaskTime;
	
	public User getUsers() {
		return users;
	}
	public void setUsers(User users) {
		this.users = users;
	}
	public Date getOpertionTime() {
		return opertionTime;
	}
	public void setOpertionTime(Date opertionTime) {
		this.opertionTime = opertionTime;
	}
	public Double getPerHourFees() {
		return perHourFees;
	}
	public void setPerHourFees(Double perHourFees) {
		this.perHourFees = perHourFees;
	}
	public Long getUseTime() {
		return useTime;
	}
	public void setUseTime(Long useTime) {
		this.useTime = useTime;
	}
	public Double getSkillFees() {
		return skillFees;
	}
	public void setSkillFees(Double skillFees) {
		this.skillFees = skillFees;
	}
	public Double getOthersFees() {
		return othersFees;
	}
	public void setOthersFees(Double othersFees) {
		this.othersFees = othersFees;
	}
	public Double getTotalFees() {
		return totalFees;
	}
	public void setTotalFees(Double totalFees) {
		this.totalFees = totalFees;
	}
	public Double getMaterialCost() {
		return materialCost;
	}
	public void setMaterialCost(Double materialCost) {
		this.materialCost = materialCost;
	}
	
	public Task getScheduleTask() {
		return scheduleTask;
	}
	public void setScheduleTask(Task scheduleTask) {
		this.scheduleTask = scheduleTask;
	}
	public Date getScheduleTaskTime() {
		return scheduleTaskTime;
	}
	public void setScheduleTaskTime(Date scheduleTaskTime) {
		this.scheduleTaskTime = scheduleTaskTime;
	}
	@Override
	public String toString() {
		
		return "Cost [users=" + users + ", opertionTime=" + opertionTime
				+ ", perHourFees=" + perHourFees + ", useTime=" + useTime
				+ ", skillFees=" + skillFees + ", othersFees=" + othersFees
				+ ", totalFees=" + totalFees + ", materialCost=" + materialCost
				+ "]\n" + super.toString();
	}
}
