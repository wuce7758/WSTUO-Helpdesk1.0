package com.wstuo.common.tools.entity;
import javax.persistence.Cacheable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.wstuo.common.config.attachment.entity.Attachment;

/**
 * Event Attachment Entity class
 * @author WSTUO
 *
 */
@SuppressWarnings("serial")
@Entity
@Table(name="T_EventAttachment")
@Cacheable
public class EventAttachment {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long eventAttchmentId;
	private Long eno;
	private String eventType;
	@ManyToOne
	private Attachment attachment = new Attachment();
	public Long getEno() {
		return eno;
	}
	public void setEno(Long eno) {
		this.eno = eno;
	}
	public String getEventType() {
		return eventType;
	}
	public void setEventType(String eventType) {
		this.eventType = eventType;
	}
	public Long getEventAttchmentId() {
		return eventAttchmentId;
	}
	public void setEventAttchmentId(Long eventAttchmentId) {
		this.eventAttchmentId = eventAttchmentId;
	}
	public Attachment getAttachment() {
		return attachment;
	}
	public void setAttachment(Attachment attachment) {
		this.attachment = attachment;
	}
	
}
