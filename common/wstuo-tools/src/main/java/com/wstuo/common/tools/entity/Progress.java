package com.wstuo.common.tools.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;

import com.wstuo.common.entity.BaseEntity;

/**
 * 进展
 * @author WSTUO
 *
 */
@SuppressWarnings({ "rawtypes", "serial" })
@Entity
@Inheritance( strategy = InheritanceType.JOINED )
public class Progress extends BaseEntity{
	@Id
	@GeneratedValue( strategy = GenerationType.AUTO )
	private Long progressId;
	private String stage;
	private Long status;
	@ManyToOne
	private EventTask eventTask;
	private Long eno;
	private String eventType;
	@Column(nullable=true)
	private Date startTime;//执行开始时间
	private Date endTime;//执行结束时间
	private Long startToEedTime;//开始到结束时间(分钟)
	private Long actualTime;//实际处理时间(分钟)
	private String type;//进展类型(挂起、工程师填写) HANG,MaterialCost物料成本
	
	public Long getProgressId() {
		return progressId;
	}
	public void setProgressId(Long progressId) {
		this.progressId = progressId;
	}
	public String getStage() {
		return stage;
	}
	public void setStage(String stage) {
		this.stage = stage;
	}
	public Long getStatus() {
		return status;
	}
	public void setStatus(Long status) {
		this.status = status;
	}
	public EventTask getEventTask() {
		return eventTask;
	}
	public void setEventTask(EventTask eventTask) {
		this.eventTask = eventTask;
	}
	public Long getEno() {
		return eno;
	}
	public void setEno(Long eno) {
		this.eno = eno;
	}
	public String getEventType() {
		return eventType;
	}
	public void setEventType(String eventType) {
		this.eventType = eventType;
	}
	public Date getStartTime() {
		return startTime;
	}
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
	public Date getEndTime() {
		return endTime;
	}
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	public Long getStartToEedTime() {
		return startToEedTime;
	}
	public void setStartToEedTime(Long startToEedTime) {
		this.startToEedTime = startToEedTime;
	}
	public Long getActualTime() {
		return actualTime;
	}
	public void setActualTime(Long actualTime) {
		this.actualTime = actualTime;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	@Override
	public String toString() {
		return "Progress [progressId=" + progressId + ", stage=" + stage
				+ ", status=" + status + ", eventTask=" + eventTask + ", eno="
				+ eno + ", eventType=" + eventType + ", startTime=" + startTime
				+ ", endTime=" + endTime + ", startToEedTime=" + startToEedTime
				+ ", actualTime=" + actualTime + ", type=" + type + "]";
	}
	
}
