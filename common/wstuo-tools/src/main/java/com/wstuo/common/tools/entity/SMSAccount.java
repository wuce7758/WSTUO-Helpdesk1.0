package com.wstuo.common.tools.entity;

import com.wstuo.common.entity.BaseEntity;

import javax.persistence.Cacheable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * 短信连接信息实体类
 * @author QXY
 * date 2010-10-11
 */
@SuppressWarnings({ "serial", "rawtypes" })
@Entity
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class SMSAccount extends BaseEntity
{
	@Id
    @GeneratedValue( strategy = GenerationType.AUTO )
	private Long said;
	private String orgId;
	private String userName;
	private String pwd;
	private Integer smsLength;
	private Integer moblieCount;
	private String smsInstance;
	
	public Long getSaid() {
		return said;
	}
	public void setSaid(Long said) {
		this.said = said;
	}
	public String getOrgId() {
		return orgId;
	}
	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPwd() {
		return pwd;
	}
	public void setPwd(String pwd) {
		this.pwd = pwd;
	}
	public Integer getSmsLength() {
		return smsLength;
	}
	public void setSmsLength(Integer smsLength) {
		this.smsLength = smsLength;
	}
	public Integer getMoblieCount() {
		return moblieCount;
	}
	public void setMoblieCount(Integer moblieCount) {
		this.moblieCount = moblieCount;
	}
	public String getSmsInstance() {
		return smsInstance;
	}
	public void setSmsInstance(String smsInstance) {
		this.smsInstance = smsInstance;
	}
}
