package com.wstuo.common.tools.service;

import java.io.File;

import com.wstuo.common.tools.dto.FileUploadDTO;

public interface IFileUpload {
	
	/**
	 * 文件上传
	 * @param myFile
	 * @param fileName
	 * @param imageFileName
	 * @param imgSize
	 * @param myFileName
	 * @return
	 */
	public FileUploadDTO fileUpload(File myFile,String fileName,String imageFileName,String imgSize,String myFileName);
}
