package com.wstuo.itsm.config.autoComplete.action;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.xwork2.ActionSupport;
import com.wstuo.itsm.config.autoComplete.dto.AutoCompleteParamDTO;
import com.wstuo.itsm.config.autoComplete.service.IAutoCompleteService;
import com.wstuo.common.dto.AutoCompleteDTO;

/**
 *  自动补全 Action层
 * @author mark
 *
 */
@SuppressWarnings("serial")
public class AutoCompleteAction extends ActionSupport {
	
	@Autowired
	private IAutoCompleteService autoCompleteService;
	/**
	 * 自动补全参数DTO
	 */
	private AutoCompleteParamDTO paramDto;
	/**
	 * 自动补全查询DTO
	 */
	private List<AutoCompleteDTO> list;

	public AutoCompleteParamDTO getParamDto() {
		return paramDto;
	}
	public void setParamDto(AutoCompleteParamDTO paramDto) {
		this.paramDto = paramDto;
	}
	public List<AutoCompleteDTO> getList() {
		return list;
	}
	public void setList(List<AutoCompleteDTO> list) {
		this.list = list;
	}
	/**
	 * 自动补全查询
	 * @return String
	 */
	public String autoComplete(){
		list=autoCompleteService.autoComplete(paramDto);
		return "autoCompleteList";
	}
}
