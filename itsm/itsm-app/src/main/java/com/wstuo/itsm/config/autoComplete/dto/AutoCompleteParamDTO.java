package com.wstuo.itsm.config.autoComplete.dto;
/**
 * 自动补全参数DTo
 * @author QXY
 *
 */
public class AutoCompleteParamDTO {

	private String entityName;	// 补全的实体类名
	private String propertyName; // 补全的字段
	private String queryValue; 	//字段的值
	private String returnLabel;	// 补全下拉显示的内容
	private String returnValue; 	//补全文本框显示的内容
	private String returnValueType; 	//字段的类型（Long or String）
	private String term;//条件

	
	public String getTerm() {
		return term;
	}
	public void setTerm(String term) {
		this.term = term;
	}
	public String getEntityName() {
		return entityName;
	}
	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}
	public String getPropertyName() {
		return propertyName;
	}
	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}
	public String getQueryValue() {
		return queryValue;
	}
	public void setQueryValue(String queryValue) {
		this.queryValue = queryValue;
	}
	public String getReturnLabel() {
		return returnLabel;
	}
	public void setReturnLabel(String returnLabel) {
		this.returnLabel = returnLabel;
	}
	public String getReturnValue() {
		return returnValue;
	}
	public void setReturnValue(String returnValue) {
		this.returnValue = returnValue;
	}
	public String getReturnValueType() {
		return returnValueType;
	}
	public void setReturnValueType(String returnValueType) {
		this.returnValueType = returnValueType;
	}
	 
	 
}
