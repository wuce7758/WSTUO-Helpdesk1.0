package com.wstuo.itsm.config.visit.dao;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.util.StringUtils;

import com.wstuo.itsm.config.visit.dto.VisitDTO;
import com.wstuo.itsm.config.visit.entity.Visit;
import com.wstuo.common.dao.BaseDAOImplHibernate;
import com.wstuo.common.dto.PageDTO;

/**
 * 请求回访DAO类 
 * @author QXY
 *
 */
public class VisitDAO extends BaseDAOImplHibernate<Visit> implements IVisitDAO {
	
	/**
	 * 分页查询请求回访事项
	 * @param start
	 * @param limit
	 * @param sord
	 * @param sidx
	 * @return PageDTO
	 */
	public PageDTO findPager(VisitDTO visitDTO, int start, int limit,String sord, String sidx,boolean useStatus){
		final DetachedCriteria dc = DetachedCriteria.forClass( Visit.class );
		if(useStatus){
			dc.add(Restrictions.eq("useStatus", true));
		}
		if(visitDTO!=null){
			if(StringUtils.hasText(visitDTO.getVisitName())){
				dc.add(Restrictions.like("visitName", visitDTO.getVisitName() , MatchMode.ANYWHERE));
			}
			
			if(StringUtils.hasText(visitDTO.getVisitItemType())){
				dc.add(Restrictions.eq("visitItemType", visitDTO.getVisitItemType()));
			}
		}
		
        //排序
        if(StringUtils.hasText(sord)&&StringUtils.hasText(sidx)){
            if(sord.equals("desc"))
            	dc.addOrder(Order.desc(sidx));
            else
            	dc.addOrder(Order.asc(sidx));
        }else{
        
        	dc.addOrder(Order.desc("visitOrder"));
        }
		
		return super.findPageByCriteria(dc, start, limit);
	};
	
}
