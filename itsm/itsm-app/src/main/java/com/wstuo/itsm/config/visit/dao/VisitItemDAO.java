package com.wstuo.itsm.config.visit.dao;


import com.wstuo.itsm.config.visit.entity.VisitItem;
import com.wstuo.common.dao.BaseDAOImplHibernate;

/**
 * 请求回访事项DAO类
 * @author QXY
 *
 */
public class VisitItemDAO extends BaseDAOImplHibernate<VisitItem> implements IVisitItemDAO {
	
}
