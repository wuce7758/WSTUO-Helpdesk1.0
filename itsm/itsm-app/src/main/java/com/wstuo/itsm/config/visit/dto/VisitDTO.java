package com.wstuo.itsm.config.visit.dto;

import com.wstuo.common.dto.BaseDTO;

/**
 * 请求回访DTO
 * 
 * @author QXY
 * 
 */
@SuppressWarnings("serial")
public class VisitDTO extends BaseDTO {
	private Long visitNo;
	private String visitName;
	private String visitItemType;
	private Boolean useStatus = false;
	private Long visitOrder;
	private Byte dataFlag;

	public Long getVisitNo() {
		return visitNo;
	}

	public void setVisitNo(Long visitNo) {
		this.visitNo = visitNo;
	}

	public String getVisitName() {
		return visitName;
	}

	public void setVisitName(String visitName) {
		this.visitName = visitName;
	}

	public String getVisitItemType() {
		return visitItemType;
	}

	public void setVisitItemType(String visitItemType) {
		this.visitItemType = visitItemType;
	}
	public Boolean getUseStatus() {
		return useStatus;
	}

	public void setUseStatus(Boolean useStatus) {
		this.useStatus = useStatus;
	}

	public Long getVisitOrder() {
		return visitOrder;
	}

	public void setVisitOrder(Long visitOrder) {
		this.visitOrder = visitOrder;
	}

	public Byte getDataFlag() {
		return dataFlag;
	}

	public void setDataFlag(Byte dataFlag) {
		this.dataFlag = dataFlag;
	}

	public VisitDTO() {

	}

	public VisitDTO(Long visitNo, String visitName, String visitItemType,
			boolean useStatus, Long visitOrder) {
		super();
		this.visitNo = visitNo;
		this.visitName = visitName;
		this.visitItemType = visitItemType;
		this.useStatus = useStatus;
		this.visitOrder = visitOrder;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((dataFlag == null) ? 0 : dataFlag.hashCode());
		result = prime * result
				+ ((useStatus == null) ? 0 : useStatus.hashCode());
		result = prime * result
				+ ((visitItemType == null) ? 0 : visitItemType.hashCode());
		result = prime * result
				+ ((visitName == null) ? 0 : visitName.hashCode());
		result = prime * result + ((visitNo == null) ? 0 : visitNo.hashCode());
		result = prime * result
				+ ((visitOrder == null) ? 0 : visitOrder.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VisitDTO other = (VisitDTO) obj;
		if (dataFlag == null) {
			if (other.dataFlag != null)
				return false;
		} else if (!dataFlag.equals(other.dataFlag))
			return false;
		if (useStatus == null) {
			if (other.useStatus != null)
				return false;
		} else if (!useStatus.equals(other.useStatus))
			return false;
		if (visitItemType == null) {
			if (other.visitItemType != null)
				return false;
		} else if (!visitItemType.equals(other.visitItemType))
			return false;
		if (visitName == null) {
			if (other.visitName != null)
				return false;
		} else if (!visitName.equals(other.visitName))
			return false;
		if (visitNo == null) {
			if (other.visitNo != null)
				return false;
		} else if (!visitNo.equals(other.visitNo))
			return false;
		if (visitOrder == null) {
			if (other.visitOrder != null)
				return false;
		} else if (!visitOrder.equals(other.visitOrder))
			return false;
		return true;
	}

}
