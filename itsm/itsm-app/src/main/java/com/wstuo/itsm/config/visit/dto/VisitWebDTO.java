package com.wstuo.itsm.config.visit.dto;

import java.util.List;
import com.wstuo.itsm.config.visit.entity.VisitItem;
import com.wstuo.common.dto.BaseDTO;

/**
 * 请求回访事项web显示DTO
 * @author Administrator
 *
 */
public class VisitWebDTO extends BaseDTO {
	private Long visitNo;
	private String visitName;
	private String visitItemType;
	private Boolean useStatus = false;
	private Long visitOrder;
	private List<VisitItem> visitItems;
	
	public Long getVisitNo() {
		return visitNo;
	}
	public void setVisitNo(Long visitNo) {
		this.visitNo = visitNo;
	}
	public String getVisitName() {
		return visitName;
	}
	public void setVisitName(String visitName) {
		this.visitName = visitName;
	}
	public String getVisitItemType() {
		return visitItemType;
	}
	public void setVisitItemType(String visitItemType) {
		this.visitItemType = visitItemType;
	}
	public Boolean getUseStatus() {
		return useStatus;
	}
	public void setUseStatus(Boolean useStatus) {
		this.useStatus = useStatus;
	}
	public Long getVisitOrder() {
		return visitOrder;
	}
	public void setVisitOrder(Long visitOrder) {
		this.visitOrder = visitOrder;
	}
	public List<VisitItem> getVisitItems() {
		return visitItems;
	}
	public void setVisitItems(List<VisitItem> visitItems) {
		this.visitItems = visitItems;
	}

	
}
