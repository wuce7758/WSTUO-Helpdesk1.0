package com.wstuo.itsm.config.visit.service;

import java.util.ArrayList;
import java.util.Collections;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.wstuo.itsm.config.visit.dao.IVisitDAO;
import com.wstuo.itsm.config.visit.dto.VisitDTO;
import com.wstuo.itsm.config.visit.dto.VisitWebDTO;
import com.wstuo.itsm.config.visit.entity.Visit;
import com.wstuo.itsm.config.visit.entity.VisitItem;
import com.wstuo.common.dto.PageDTO;
/**
 * 请求回访服务层
 * @author QXY
 *
 */
public class VisitService implements IVisitService {
	
	@Autowired
	private IVisitDAO visitDAO;
	
	/**
	 * 分页查询回访项
	 * @param start
	 * @param limit
	 * @param sord
	 * @param sidx
	 * @return PageDTO
	 */
	@SuppressWarnings("unchecked")
	@Transactional
	public PageDTO findPagerVisit(VisitDTO visitDTO, int start, int limit,String sord, String sidx,boolean useStatus){
		PageDTO page=visitDAO.findPager(visitDTO, start, limit, sord, sidx,useStatus);
		List<Visit> visList =(List<Visit>)page.getData();
		List<VisitDTO> dtos=new ArrayList<VisitDTO>(visList.size());
		for(Visit vs:visList){
			VisitDTO visDto= new VisitDTO();
			VisitDTO.entity2dto(vs, visDto);
			dtos.add(visDto);
		}
		page.setData(dtos);
		
		return page;
		
	}
	/**
	 * 回访项保存
	 * @param visitDto
	 */
	@Transactional
	public void visitSave(VisitDTO visitDto){
		Visit visit = new Visit();
		VisitDTO.dto2entity(visitDto, visit);
		visitDAO.save(visit);
		
		visitDto.setVisitNo(visit.getVisitNo());
	}
	/**
	 * 回访项更新
	 * @param visitDto
	 */
	@Transactional
	public void visitUpdate(VisitDTO visitDto){
		Visit visit = visitDAO.findById(visitDto.getVisitNo());
		visit.setVisitItemType(visitDto.getVisitItemType());
		visit.setVisitName(visitDto.getVisitName());
		visit.setUseStatus(visitDto.getUseStatus());
		visit.setVisitOrder(visitDto.getVisitOrder());
		
		visitDAO.update(visit);
	}
	/**
	 * 回访项删除
	 * @param visitNo
	 */
	@Transactional
	public void visitDelete(Long[] visitNo){
		visitDAO.deleteByIds(visitNo);
	}
	
	/**
	 * 查询所有回访项
	 * @param start
	 * @param limit
	 * @param sord
	 * @param sidx
	 * @return PageDTO
	 */
	@Transactional
	public PageDTO findAllVisit(VisitDTO visitDTO,int start, int limit,String sord, String sidx,boolean useStatus){
		
		PageDTO page=visitDAO.findPager(visitDTO,start, limit, sord, sidx,useStatus);
		List<Visit> visList =(List<Visit>)page.getData();
		List<VisitWebDTO> dtos=new ArrayList<VisitWebDTO>(visList.size());
		for(Visit vs:visList){
			VisitWebDTO visDto= new VisitWebDTO();
			VisitWebDTO.entity2dto(vs, visDto);
			List<VisitItem> vistItems=new ArrayList<VisitItem>();
			//add mars
			if(vs.getVisitItems()!=null){
				for(VisitItem vi:vs.getVisitItems()){
					vistItems.add(vi);
				}
			}
			
			ComparatorVisitItem cvi = new ComparatorVisitItem();
	        Collections.sort(vistItems, cvi);
			visDto.setVisitItems(vistItems);
			
			dtos.add(visDto);
		}
		page.setData(dtos);
		
		return page;
		
	}
	
	/**
	 * 根据ID查找回访项
	 * @param visitNo
	 */
	@Transactional
	public VisitDTO findById(Long visitNo){
		VisitDTO dto=new VisitDTO();
		Visit entity = visitDAO.findById(visitNo);
		VisitDTO.entity2dto(entity, dto);
		
		return dto;
	
	}
	
}
