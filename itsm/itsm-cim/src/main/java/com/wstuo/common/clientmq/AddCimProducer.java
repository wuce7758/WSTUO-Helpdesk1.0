package com.wstuo.common.clientmq;

import javax.jms.Destination;

import org.springframework.jms.core.JmsTemplate;

import com.wstuo.itsm.cim.dto.CIDTO;


/**
 * 发送消息
 * @author wing
 *
 */
public class AddCimProducer{
	private JmsTemplate template;

	private Destination destination;


	public void setTemplate(JmsTemplate template) {
	   this.template = template;
	}

	public void setDestination(Destination destination) {
	   this.destination = destination;
	}

	public void send(CIDTO order) {
		template.convertAndSend(this.destination, order);
	} 
}