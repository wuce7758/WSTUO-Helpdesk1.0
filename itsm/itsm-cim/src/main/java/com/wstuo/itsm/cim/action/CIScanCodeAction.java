package com.wstuo.itsm.cim.action;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import com.opensymphony.xwork2.ActionSupport;
import com.wstuo.common.config.server.dto.ServerUrlDTO;
import com.wstuo.common.config.server.service.IServerUrlService;
import com.wstuo.common.exception.ApplicationException;
import com.wstuo.common.security.utils.AppConfigUtils;
import com.wstuo.itsm.cim.QRCodeUtil.QRCode;
import com.wstuo.itsm.cim.dao.ICIDAO;
import com.wstuo.itsm.cim.dto.CIDetailDTO;
import com.wstuo.itsm.cim.dto.CIScanDTO;
import com.wstuo.itsm.cim.dto.HardwareDTO;
import com.wstuo.itsm.cim.service.ICIService;
/**
 * 配置项扫描码的管理
 * @author QXY
 *
 */
@SuppressWarnings("serial")
public class CIScanCodeAction extends ActionSupport{
	
	@Autowired
    private ICIService ciService;
	@Autowired
	private ICIDAO ciDAO;
	@Autowired
	private IServerUrlService serverUrlService;
	@Autowired
	private QRCode qrCode;
	
	private CIDetailDTO ciDetailDTO;
	private HardwareDTO hardwareDTOs;
	
	private InputStream downloadStream; //the input stream of the download file.
	private String downloadFileName; //the file name that was required to download.
	private CIScanDTO ciScanDTO =new CIScanDTO();
	private InputStream imageStream;
	private boolean boo=true;
	
	public boolean isBoo() {
		return boo;
	}

	public void setBoo(boolean boo) {
		this.boo = boo;
	}

	public InputStream getImageStream() {
		return imageStream;
	}

	public void setImageStream(InputStream imageStream) {
		this.imageStream = imageStream;
	}

	public CIScanDTO getCiScanDTO() {
		return ciScanDTO;
	}

	public void setCiScanDTO(CIScanDTO ciScanDTO) {
		this.ciScanDTO = ciScanDTO;
	}

	public ICIDAO getCiDAO() {
		return ciDAO;
	}

	public void setCiDAO(ICIDAO ciDAO) {
		this.ciDAO = ciDAO;
	}
	/**
	 * the input stream of the download file.
	 * @return String
	 */
	public InputStream getDownloadStream() {
		return downloadStream;
	}

	public void setDownloadStream(InputStream downloadStream) {
		this.downloadStream = downloadStream;
	}
	/**
	 * the file name that was required to download.
	 * @return String
	 */
	public String getDownloadFileName() {
		return downloadFileName;
	}

	public void setDownloadFileName(String downloadFileName) {
		this.downloadFileName = downloadFileName;
	}
	/**
	 * 下载内容类型
	 * @return String
	 */
	public String getDownloadContentType() {
		return downloadContentType;
	}

	public void setDownloadContentType(String downloadContentType) {
		this.downloadContentType = downloadContentType;
	}
	private String downloadContentType="application/zip"; //the content type of the download file.
	
	public ICIService getCiService() {
		return ciService;
	}

	public void setCiService(ICIService ciService) {
		this.ciService = ciService;
	}

	public CIDetailDTO getCiDetailDTO() {
		return ciDetailDTO;
	}

	public void setCiDetailDTO(CIDetailDTO ciDetailDTO) {
		this.ciDetailDTO = ciDetailDTO;
	}
	public HardwareDTO getHardwareDTOs() {
		return hardwareDTOs;
	}

	public void setHardwareDTOs(HardwareDTO hardwareDTOs) {
		this.hardwareDTOs = hardwareDTOs;
	}
	
	
	/**
	 * 更具配置项编号查询 cino,并返回二维码配置项信息
	 * @return String
	 * @throws UnsupportedEncodingException
	 */
	public String SaveScan() throws UnsupportedEncodingException{
	
		ciScanDTO= ciService.findConfigureItemByCINO(ciDetailDTO.getCino());
		ServerUrlDTO sdto= serverUrlService.findServiceUrl();
		boo=qrCode.encode(sdto,ciScanDTO);
		
		return "ciScanDTO";
	}
	
	
	
	/**
	 * 查看二维码的图片
	 * @return String
	 */
	public String findImage(){
		
		imageStream=qrCode.encodeImage(ciScanDTO.getCiId());
	
		return "imageStream";
	}
	
	
	/**
	 * 下载二维码的图片
	 * @return String
	 */
    public String download()  {
    	
	  	String fileUrl="";
	
	  	fileUrl = "img_"+ciDetailDTO.getCiId()+".png";
		
    	String fileName = AppConfigUtils.getInstance().getQRCodePath()+"/"+ fileUrl;
    	
    	File file = new File(fileName);
    	if (!file.exists()) {
    		
    		return null;
    		//throw new ApplicationException("ERROE_FILENOTFIND\n");
    	}
    	try {
			downloadStream = new FileInputStream(file);	
		} catch (FileNotFoundException e) {
			throw new ApplicationException("ERROR_ATTACHMENT_FAIL_READ\n",e);
		}
		
		downloadFileName = fileUrl;
		
		String fileExt=fileUrl.substring(fileUrl.lastIndexOf(".")+1,fileUrl.length()).toLowerCase();
		downloadContentType=switchContentType(fileExt);
    	return "downloadSuccess";
    }
    /**
     * 根据文件后缀获取ContentType
     * @param fileExt
     * @return String
     */
    private String switchContentType(String fileExt){
    	Map<String,String> typeMaps=new HashMap<String,String>();
    	typeMaps.put("png", "image/PNG");
    	//typeMaps.put("jpg", "image/JPEG");
    	typeMaps.put("doc", "application/msword");
    	typeMaps.put("xls", "application/vnd.ms-excel");
    	typeMaps.put("ppt", "application/mspowerpoint");
    	typeMaps.put("docx", "application/vnd.openxmlformats-officedocument.wordprocessingml.document");
    	typeMaps.put("pptx", "application/vnd.openxmlformats-officedocument.presentationml.presentation");
    	typeMaps.put("xlsx", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
    	if(typeMaps.get(fileExt)==null){
    		return "application/"+fileExt;
    	}
    	return typeMaps.get(fileExt);
    }
    
    
    /**
     * 更具二维码查看配置项详细信息
     * */
   /* public String showScanCIInfo(){
    	return "ciScanDTO";
    }*/
    
	/**
	 * 更具配置项编号查询 cino,并返回二维码配置项信息
	 * @return String
	 * */
	public String findConfigureItemByCINO(){ 
		ciScanDTO= ciService.findConfigureItemByCIID(Long.parseLong(ciScanDTO.getCino()));
		//ciService.findConfigureItemByCINO(ciScanDTO.getCino());因为手机APP扫描返回的其实ciId，但是调用了根据
		return "ciScanDTO";
	}
	/**
	 * 空方法
	 * @return String
	 */
	public String findConfigureItemByCIID(){
		
		return "ciScanDTO";
	}
	    
}	
