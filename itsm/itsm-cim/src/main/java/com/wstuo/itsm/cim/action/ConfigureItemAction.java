package com.wstuo.itsm.cim.action;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.xwork2.ActionSupport;
import com.wstuo.common.config.attachment.entity.Attachment;
import com.wstuo.common.config.category.dto.CategoryDTO;
import com.wstuo.common.config.category.service.ICICategoryService;
import com.wstuo.common.config.customfilter.dto.KeyTransferDTO;
import com.wstuo.common.config.dictionary.dto.DataDictionaryItemsDTO;
import com.wstuo.common.config.dictionary.service.IDataDictionaryItemsService;
import com.wstuo.common.config.template.dto.TemplateDTO;
import com.wstuo.common.config.template.service.ITemplateService;
import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.exception.ApplicationException;
import com.wstuo.itsm.cim.dto.BehaviorModificationDTO;
import com.wstuo.itsm.cim.dto.CIDTO;
import com.wstuo.itsm.cim.dto.CIDetailDTO;
import com.wstuo.itsm.cim.dto.CIGridDTO;
import com.wstuo.itsm.cim.dto.CIHistoryUpdateDTO;
import com.wstuo.itsm.cim.dto.CIQueryDTO;
import com.wstuo.itsm.cim.dto.CIUtilDTO;
import com.wstuo.itsm.cim.dto.CiHardwareDTO;
import com.wstuo.itsm.cim.dto.CiSoftwareDTO;
import com.wstuo.itsm.cim.dto.HardwareDTO;
import com.wstuo.itsm.cim.dto.SimpleFileDTO;
import com.wstuo.itsm.cim.dto.StaLocBraSupDTO;
import com.wstuo.itsm.cim.service.ICIService;
import com.wstuo.itsm.cim.service.ICIprefuseService;
import com.wstuo.itsm.cim.service.IImportCIService;
import com.wstuo.itsm.itsop.itsopuser.dto.CustomerDataCountDTO;


/**
 * 配置项列表功能管理
 * @author will
 * @version 0.1
 */
@SuppressWarnings("serial")
public class ConfigureItemAction extends ActionSupport {
	private static final Logger LOGGER = Logger.getLogger(ConfigureItemAction.class);
	private static final String PROVIDER_NO="supplier";
	private static final String STATUS_NO="useStatus";
	private static final String LOC_NO="location";
	private static final String BRAND_NO="brand";

	@Autowired
    private ICIService ciService;
	@Autowired
    private IImportCIService importCIService;
    @Autowired
    private IDataDictionaryItemsService dataDictionaryItemsService;
    @Autowired
	private ICICategoryService ciCategoryService;
    
    private List<DataDictionaryItemsDTO> dictionaryItemsStatusList;
    private List<DataDictionaryItemsDTO> dictionaryItemsBrandList;
    private List<DataDictionaryItemsDTO> dictionaryItemsLocList;
    private List<DataDictionaryItemsDTO> dictionaryItemsProviderList;
    private List<DataDictionaryItemsDTO> dictionaryItemsSystemPlatformList;
    private List<CIHistoryUpdateDTO> historyUpdatelist;
    private List<BehaviorModificationDTO> behaviorModificationlist;
    private CIHistoryUpdateDTO historyUpdateDTO =new CIHistoryUpdateDTO();
    private DataDictionaryItemsDTO dictionaryItemsDto;

	private PageDTO configureItemsPagination;
    private CIDTO ciDto;
    private CIQueryDTO ciQueryDTO;
    private CIDetailDTO ciDetailDTO;
    private StaLocBraSupDTO staLocBraSupDto = new StaLocBraSupDTO();
    private Long[] ids;
    private int page = 1; 
    private int rows = 10;
    private String sidx;
    private String sord;
	private boolean existCI;
    private long ciEditId;
    private String categoryType;
    private Long categoryNo;
    private String categoryName;
    private List<Attachment> attach;
    private CiHardwareDTO hardwareDTO;
    private CiSoftwareDTO softwareDTO;
    private Long aid;
    private String fileName="";
    private InputStream exportStream;
    private String effect="";
    private File importFile;
    private Long eavNo;
    private List<CustomerDataCountDTO> countsDTO=new ArrayList<CustomerDataCountDTO>();
    private Integer countTotal;
    private HardwareDTO hardwareDTOs = new HardwareDTO();
    private boolean result;
    private String loginName; 
    private String WMIJson;//配置Json
    private Long historyUpdateId;
    private TemplateDTO templateDTO;
    @Autowired
    private ITemplateService templateService;
    private String attachmentStr;
	private Long ciId;
	private String eventType;
	private String creator;
    private String hardwareIP;
    private String findCino;
    private String upUpdate;//更新返回值
    private Map<String,Map<Long,String>> listMap = new HashMap<String ,Map<Long,String>>();
    private List<CIGridDTO> ciGridList;
    private boolean isAll = false;
    private KeyTransferDTO ktd = new KeyTransferDTO();//报表数据传递
    @Autowired
    private ICIprefuseService ciprefuseService;
    private String jason;
    
    private BehaviorModificationDTO behaviorModificationDTO=new BehaviorModificationDTO();
    private Long beId;
    
	public String getJason() {
		return jason;
	}

	public void setJason(String jason) {
		this.jason = jason;
	}

	public BehaviorModificationDTO getBehaviorModificationDTO() {
		return behaviorModificationDTO;
	}

	public void setBehaviorModificationDTO(
			BehaviorModificationDTO behaviorModificationDTO) {
		this.behaviorModificationDTO = behaviorModificationDTO;
	}

	public Long getBeId() {
		return beId;
	}

	public void setBeId(Long beId) {
		this.beId = beId;
	}

	public List<BehaviorModificationDTO> getBehaviorModificationlist() {
		return behaviorModificationlist;
	}

	public void setBehaviorModificationlist(
			List<BehaviorModificationDTO> behaviorModificationlist) {
		this.behaviorModificationlist = behaviorModificationlist;
	}
	public KeyTransferDTO getKtd() {
		return ktd;
	}
	public void setKtd(KeyTransferDTO ktd) {
		this.ktd = ktd;
	}
	public List<CIGridDTO> getCiGridList() {
		return ciGridList;
	}
	public void setCiGridList(List<CIGridDTO> ciGridList) {
		this.ciGridList = ciGridList;
	}
	/**
	 * 更新返回值
	 * @return String
	 */
	public String getUpUpdate() {
		return upUpdate;
	}
	public void setUpUpdate(String upUpdate) {
		this.upUpdate = upUpdate;
	}
	public DataDictionaryItemsDTO getDictionaryItemsDto() {
		return dictionaryItemsDto;
	}
	public void setDictionaryItemsDto(DataDictionaryItemsDTO dictionaryItemsDto) {
		this.dictionaryItemsDto = dictionaryItemsDto;
	}
	public Map<String, Map<Long, String>> getListMap() {
		return listMap;
	}
	public void setListMap(Map<String, Map<Long, String>> listMap) {
		this.listMap = listMap;
	}
	/**
	 * 附件路径
	 * @return String
	 */
	public String getAttachmentStr() {
		return attachmentStr;
	}
	public void setAttachmentStr(String attachmentStr) {
		this.attachmentStr = attachmentStr;
	}
	/**
	 * 配置项编号
	 * @return String
	 */
	public Long getCiId() {
		return ciId;
	}
	public void setCiId(Long ciId) {
		this.ciId = ciId;
	}
	/**
	 * 类型
	 * @return String
	 */
	public String getEventType() {
		return eventType;
	}
	public void setEventType(String eventType) {
		this.eventType = eventType;
	}
	/**
	 * 创造者
	 * @return String
	 */
	public String getCreator() {
		return creator;
	}
	public void setCreator(String creator) {
		this.creator = creator;
	}
	public TemplateDTO getTemplateDTO() {
		return templateDTO;
	}
	public void setTemplateDTO(TemplateDTO templateDTO) {
		this.templateDTO = templateDTO;
	}
	public Long getHistoryUpdateId() {
		return historyUpdateId;
	}
	public void setHistoryUpdateId(Long historyUpdateId) {
		this.historyUpdateId = historyUpdateId;
	}
	public String getWMIJson() {
		return WMIJson;
	}
	public void setWMIJson(String wMIJson) {
		WMIJson = wMIJson;
	}
	public String getSidx() {
		return sidx;
	}
	public void setSidx(String sidx) {
		this.sidx = sidx;
	}

	public CIDetailDTO getCiDetailDTO() {
		return ciDetailDTO;
	}
	public void setCiDetailDTO(CIDetailDTO ciDetailDTO) {
		this.ciDetailDTO = ciDetailDTO;
	}
	public String getSord() {
		return sord;
	}

	public void setSord(String sord) {
		this.sord = sord;
	}
	/**
	 * 附件名称
	 * @return String
	 */
	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	/**
	 * 附件流
	 * @return String
	 */
	public InputStream getExportStream() {
		return exportStream;
	}

	public void setExportStream(InputStream exportStream) {
		this.exportStream = exportStream;
	}
	/**
	 * 影响
	 * @return String
	 */
	public String getEffect() {
		return effect;
	}

	public void setEffect(String effect) {
		this.effect = effect;
	}
   public List<CIHistoryUpdateDTO> getHistoryUpdatelist() {
		return historyUpdatelist;
	}
	public void setHistoryUpdatelist(List<CIHistoryUpdateDTO> historyUpdatelist) {
		this.historyUpdatelist = historyUpdatelist;
	}
	/**
	 * 分类
	 * @return String
	 */
	public String getCategoryType() {
		return categoryType;
	}

	public void setCategoryType(String categoryType) {
		this.categoryType = categoryType;
	}

	public ICIService getCiService() {

        return ciService;
    }

    public void setCiService(ICIService ciService) {

        this.ciService = ciService;
    }

    public IDataDictionaryItemsService getDataDictionaryItemsService() {

        return dataDictionaryItemsService;
    }
    
    public boolean isAll() {
		return isAll;
	}
	public void setAll(boolean isAll) {
		this.isAll = isAll;
	}
	public void setDataDictionaryItemsService(
        IDataDictionaryItemsService dataDictionaryItemsService) {

        this.dataDictionaryItemsService = dataDictionaryItemsService;
    }
    /**
     * 修改的配置项编号
     * @return String
     */
    public long getCiEditId() {

        return ciEditId;
    }

    public void setCiEditId(long ciEditId) {

        this.ciEditId = ciEditId;
    }

    public boolean isExistCI() {

        return existCI;
    }

    public void setExistCI(boolean existCI) {

        this.existCI = existCI;
    }

    public List<DataDictionaryItemsDTO> getDictionaryItemsStatusList() {

        return dictionaryItemsStatusList;
    }

    public void setDictionaryItemsStatusList(
        List<DataDictionaryItemsDTO> dictionaryItemsStatusList) {

        this.dictionaryItemsStatusList = dictionaryItemsStatusList;
    }

    public List<DataDictionaryItemsDTO> getDictionaryItemsBrandList() {

        return dictionaryItemsBrandList;
    }
	public CIHistoryUpdateDTO getHistoryUpdateDTO() {
		return historyUpdateDTO;
	}
	public void setHistoryUpdateDTO(CIHistoryUpdateDTO historyUpdateDTO) {
		this.historyUpdateDTO = historyUpdateDTO;
	}
    public void setDictionaryItemsBrandList(
        List<DataDictionaryItemsDTO> dictionaryItemsBrandList) {

        this.dictionaryItemsBrandList = dictionaryItemsBrandList;
    }

    public List<DataDictionaryItemsDTO> getDictionaryItemsLocList() {

        return dictionaryItemsLocList;
    }

    public void setDictionaryItemsLocList(
        List<DataDictionaryItemsDTO> dictionaryItemsLocList) {

        this.dictionaryItemsLocList = dictionaryItemsLocList;
    }

    public List<DataDictionaryItemsDTO> getDictionaryItemsProviderList() {

        return dictionaryItemsProviderList;
    }

    public void setDictionaryItemsProviderList(
        List<DataDictionaryItemsDTO> dictionaryItemsProviderList) {

        this.dictionaryItemsProviderList = dictionaryItemsProviderList;
    }

    public CIDTO getCiDto() {

        return ciDto;
    }

    public void setCiDto(CIDTO cIDto) {

        this.ciDto = cIDto;
    }



    public Long[] getIds() {

        return ids;
    }

    public void setIds(Long[] ids) {

        this.ids = ids;
    }

    public int getPage() {

        return page;
    }

    public void setPage(int page) {

        this.page = page;
    }

    public int getRows() {

        return rows;
    }

    public void setRows(int rows) {

        this.rows = rows;
    }

    public PageDTO getConfigureItemsPagination() {

        return configureItemsPagination;
    }

    public void setConfigureItemsPagination(
        PageDTO configureItemsPagination) {

        this.configureItemsPagination = configureItemsPagination;
    }

    public StaLocBraSupDTO getStaLocBraSupDto() {

        return staLocBraSupDto;
    }

    public void setStaLocBraSupDto(StaLocBraSupDTO staLocBraSupDto) {

        this.staLocBraSupDto = staLocBraSupDto;
    }

    public CIQueryDTO getCiQueryDTO() {

        return ciQueryDTO;
    }

    public void setCiQueryDTO(CIQueryDTO ciQueryDTO) {

        this.ciQueryDTO = ciQueryDTO;
    }
	/**
	 * 分类编号
	 * @return String
	 */
	public Long getCategoryNo() {
		return categoryNo;
	}

	public void setCategoryNo(Long categoryNo) {
		this.categoryNo = categoryNo;
	}
	/**
	 * 分类名称
	 * @return String
	 */
	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public List<Attachment> getAttach() {
		return attach;
	}

	public void setAttach(List<Attachment> attach) {
		this.attach = attach;
	}


	public CiHardwareDTO getHardwareDTO() {
		return hardwareDTO;
	}

	public void setHardwareDTO(CiHardwareDTO hardwareDTO) {
		this.hardwareDTO = hardwareDTO;
	}

	public CiSoftwareDTO getSoftwareDTO() {
		return softwareDTO;
	}

	public void setSoftwareDTO(CiSoftwareDTO softwareDTO) {
		this.softwareDTO = softwareDTO;
	}

	public Long getAid() {
		return aid;
	}

	public void setAid(Long aid) {
		this.aid = aid;
	}
	/**
	 * Eav编号
	 * @return String
	 */
    public Long getEavNo() {
		return eavNo;
	}

	public void setEavNo(Long eavNo) {
		this.eavNo = eavNo;
	}
	/**
	 * 导入的附件流
	 * @return String
	 */
	public File getImportFile() {
		return importFile;
	}

	public void setImportFile(File importFile) {
		this.importFile = importFile;
	}

	public List<CustomerDataCountDTO> getCountsDTO() {
		return countsDTO;
	}

	public void setCountsDTO(List<CustomerDataCountDTO> countsDTO) {
		this.countsDTO = countsDTO;
	}
	/**
	 * 统计总数
	 * @return String
	 */
	public Integer getCountTotal() {
		return countTotal;
	}
	public void setCountTotal(Integer countTotal) {
		this.countTotal = countTotal;
	}
	public HardwareDTO getHardwareDTOs() {
		return hardwareDTOs;
	}
	public void setHardwareDTOs(HardwareDTO hardwareDTOs) {
		this.hardwareDTOs = hardwareDTOs;
	}
	/**
	 * 返回的判断值
	 * @return String
	 */
	public boolean isResult() {
		return result;
	}
	public void setResult(boolean result) {
		this.result = result;
	}
	/**
	 * 登录帐号
	 * @return String
	 */
	public String getLoginName() {
		return loginName;
	}
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	public List<DataDictionaryItemsDTO> getDictionaryItemsSystemPlatformList() {
		return dictionaryItemsSystemPlatformList;
	}
	public void setDictionaryItemsSystemPlatformList(
			List<DataDictionaryItemsDTO> dictionaryItemsSystemPlatformList) {
		this.dictionaryItemsSystemPlatformList = dictionaryItemsSystemPlatformList;
	}
	
	public String getHardwareIP() {
		return hardwareIP;
	}
	public void setHardwareIP(String hardwareIP) {
		this.hardwareIP = hardwareIP;
	}
	
	public String getFindCino() {
		return findCino;
	}
	public void setFindCino(String findCino) {
		this.findCino = findCino;
	}
	/**
     * 配置项分页查询
     * @return String SUCCESS
     */
    public String cItemsFind() {
        int start = (page - 1) * rows;
        configureItemsPagination = ciService.findPagerConfigureItem(ciQueryDTO,start,rows,sidx,sord);
        configureItemsPagination.setRows(rows);
        configureItemsPagination.setPage(page);
        return SUCCESS;
    }
    
    /**
	 * 自定义分页查询配置项列表.
	 * @return String
	 */
    public String findCItemsByCustomFilter() {
        int start = (page - 1) * rows;
        configureItemsPagination = ciService.findConfigureItemPagerByCustomtFilter(ciQueryDTO, start, rows,sidx,sord);
        configureItemsPagination.setRows(rows);
        configureItemsPagination.setPage(page);
        return SUCCESS;
    }
    
    /**
     * 查询配置项历史更新记录
     * 	
     * */
    public String findCiHistoryUpdate(){
    	historyUpdatelist= ciService.findCiHistoryUpdate(ciEditId);
    	return "historyUpdatelist";
    }
    
    /**
     * 修改行为
     * 	
     * */
    public String findBehaviorModification(){
    	behaviorModificationlist= ciService.findBehaviorModification(ciDto);
    	return "behaviorModificationlist";
    }
    /**
     * 查询配置项历史更新记录ID
     * */
    public String findCiHistoryUpdateID(){
    	historyUpdateDTO= ciService.findCiHistoryUpdateID(historyUpdateId);
    	return "historyUpdateDTO";
    }
    
    
    /**
     * 添加配置项
     * @return String SUCCESS
     */
    public String cItemSave() {
        ciService.addConfigureItem(ciDto,hardwareDTOs);
        ciId = ciDto.getCiId();
        return "ciId";
        
    }
    /**
     * 删除配置项
     * @return String SUCCESS
     */
    public String cItemsDel() {
    	try {
    		existCI = ciService.deleteConfigureItem(ids);
		} catch (Exception e) {
			throw new ApplicationException("ERROR_DATA_CAN_NOT_DELETE",e);
		}
        
        return SUCCESS;
    }
    /**
     * 判断配置项ID是否存在
     * @return String SUCCESS
     */
    public String existCIById() {
        existCI = ciService.isConfigureItemExist(ciEditId);
        return "existCI";
    }
    /**
     * 判断配置项编号是否存在
     * @return String
     */
    public String existCIByCiNo() {
        existCI = ciService.isConfigureItemExist(ciQueryDTO.getCino());
        return "existCI";
    }
    
    
    /**
     * 更新配置项信息
     * @return String SUCCESS
     */
    public String cItemUpdate() {
        ciService.updateConfigureItem(ciDto,hardwareDTOs);
        return SUCCESS;
    }

    /**
     * 获取配置项基础数据
     * @return String staLocBraSupDto
     */
    public String findStaLocBraSupList() {
        dictionaryItemsProviderList = dataDictionaryItemsService
            .findDictionaryItemByGroupCode(PROVIDER_NO);
        dictionaryItemsStatusList = dataDictionaryItemsService
            .findDictionaryItemByGroupCode(STATUS_NO);
        dictionaryItemsLocList = dataDictionaryItemsService
            .findDictionaryItemByGroupCode(LOC_NO);
        dictionaryItemsBrandList = dataDictionaryItemsService
            .findDictionaryItemByGroupCode(BRAND_NO);
        dictionaryItemsSystemPlatformList = dataDictionaryItemsService.findDictionaryItemByGroupCode("systemPlatform");
        staLocBraSupDto.setDictionaryItemsProviderList(dictionaryItemsProviderList);
        staLocBraSupDto.setDictionaryItemsStatusList(dictionaryItemsStatusList);
        staLocBraSupDto.setDictionaryItemsLocList(dictionaryItemsLocList);
        staLocBraSupDto.setDictionaryItemsBrandList(dictionaryItemsBrandList);
        staLocBraSupDto.setDictionaryItemsSystemPlatformList(this.dictionaryItemsSystemPlatformList);
        return "staLocBraSupDto";
    }
    /**
     * 根据ID获取配置项
     * @return String showisItemsInfo
     */
    public String findByciId() {
    	ciDetailDTO=ciService.findConfigureItemById(ciEditId);
        return "showisItemsInfo";
    }
    /**
     * find ciDetailDTO
     * @return
     */
    public String findDetailDTOByCiId() {
    	ciDetailDTO=ciService.findConfigureItemById(ciEditId);
        return "ciDetailDTO";
    }
    
    
    
    /**
     * 根据cino查询
     * @return String
     */
    public String findByCieno(){
    	ciDetailDTO=ciService.findByCieno(findCino);
    	if(ciDetailDTO.getCino() != null && ciDetailDTO.getCino() != ""){
    		return "showisItemsInfo";
    	}else{
    		//如果为空返回一个值
    		LOGGER.error("error cino is null");
    		return null;
    	}
    }
    /**
     * 根据硬件ip查询
     * @return String
     */
    public String findByHardwareIp(){
    	ciDetailDTO= ciService.findByHardwareIp(hardwareIP);
    	if(ciDetailDTO.getCino() != null && ciDetailDTO.getCino() != ""){
   		return "showisItemsInfo";
    	}else{
   		//如果为空返回一个值
		LOGGER.error("error cino is null");
		return null;
    	}
    }
    /**
     * 配置添加页面
     * @return String showisItemsAdd
     */
    public String configureItemAdd() {
    	dictionaryItems();
        try {
        	if(categoryName != null){
            	categoryName=new String(categoryName.getBytes("iso-8859-1"),"utf-8");
        	}
		} catch (Exception e) {
			LOGGER.error(e);
		}
        return "showisItemsAdd";
    }
    
    /**
     * 获取配置项基础数据
     */
    public void dictionaryItems(){
    	//基础数据下拉列表	
   	 	dictionaryItemsProviderList = dataDictionaryItemsService
        .findDictionaryItemByGroupCode(PROVIDER_NO);
	     dictionaryItemsStatusList = dataDictionaryItemsService
	         .findDictionaryItemByGroupCode(STATUS_NO);
	     dictionaryItemsLocList = dataDictionaryItemsService
	         .findDictionaryItemByGroupCode(LOC_NO);
	     dictionaryItemsBrandList = dataDictionaryItemsService
	         .findDictionaryItemByGroupCode(BRAND_NO);
	     this.dictionaryItemsSystemPlatformList = this.dataDictionaryItemsService.findDictionaryItemByGroupCode("systemPlatform");
    }
    /**
     * 配置项编辑页面
     * @return String showisItemsEdit
     */
    public String configureItemEdit(){
    	
    	dictionaryItems();
    	//配置项详细信息
    	ciDetailDTO=ciService.findConfigureItemById(ciEditId);
	    //hardwareDTOs=ciService.findConfigureItenHardware(ciEditId);
    	return "showisItemsEdit";
    }
    /**
     * 获取配置项附件信息
     * @return String attach
     */
    public String getAttachmentByCiId(){
    	attach=ciService.findConfigureItemById(ciEditId).getAttachments();
    	return "attach";
    }
    /**
     * 获取配置项软件附件信息
     * @return String attach
     */
    public String getSoftAttachmentByCiId(){
    	attach=ciService.findConfigureItemById(ciEditId).getSoftAttachments();
    	return "attach";
    }
    /**
     * 删除附件
     * @return String
     */
    public String deleteAttachement(){
    	ciService.deleteAttachement(ciEditId, aid);
    	return SUCCESS;
    }
    /**
     * 删除软件附件
     * @return String
     */
    public String deleteSoftAttachement(){
    	ciService.deleteSoftAttachement(ciEditId, aid);
    	return SUCCESS;
    }
    /**
     * 保存电脑配置项信息
     */
   public String saveCiConfigInfo(){
	   ciService.saveCiConfigInfo(ciEditId, hardwareDTO, softwareDTO);
	   return SUCCESS;
   };
    /**
     * 保存CI配置信息
     */
   public String saveHardware(){
	   ciService.saveHardware(hardwareDTO);
	   return SUCCESS;
   };
    /**
     * 获取硬件配置信息
     * @return String
     */
   public String getCiHardware(){
	   hardwareDTO=ciService.getCiHardware(ciEditId);
	   return "hardwareDTO";
   };
    
    /**
     * 获取软件安装信息
     * @return String PageDTO
     */
   public String getCiSoftware(){
	   configureItemsPagination=ciService.getCiSoftware(ciEditId, page, rows);
	   configureItemsPagination.setPage(page);
	   configureItemsPagination.setRows(rows);
	   return SUCCESS;
   };
    
   /**
    * 导出数据.
    * @return String
    */
   public String exportConfigureItem() {
	   try{
		   if(ciQueryDTO!=null && ciQueryDTO.getFilterId()!=null && ciQueryDTO.getFilterId()!=-1){
			   ciService.exportConfigureItemsByFilter(ciQueryDTO,sidx,sord);//过滤器
		   }else{
			   ciService.exportConfigureItems(ciQueryDTO,sidx,sord);
		   } 
	   }catch(Exception ex){
		   if(ex.getMessage()!=null && ex.getMessage().indexOf("JMS")!=-1){
	       		throw new ApplicationException("label_expoort_error_jmsException");
	       	}
		   LOGGER.error(ex);
		}
	   return "exportFileSuccessful";
   }
 
   /**
    * 导出文件
    * @return String
    */
   public String exportData(){
	  
        exportStream=ciService.exportDownLoad(categoryName);
        if(exportStream==null) {
        	effect="ERROE_FILENOTFIND";
        	return "exportFileError";
		}
		return "exportFileSuccess";
   }
   /**
    * 导入数据
    * @return String String
    */
   public String importConfigureItem(){
	 try {
		   effect=ciService.importCI(importFile,ciDto);
	} catch (Exception e) {
		effect="ERROR_CSV_FILE_IO";
		LOGGER.error(e);
	}
   	
   	
   	return "importResult";
   }
   
   /**
    * 根据公司进行统计
    * @return String
    */
   public String configureItemDataCountByCompanyNo(){
	   	countsDTO=ciService.configureItemDataCountByCompanyNo(loginName);
	   	return "companyCountResult";
   }
   
   /**
    * 软件许可使用统计
    * @return String
    */
   public String softwareLicenseUseCount(){
	   countTotal=ciService.softwareLicenseUseCount(ciDto.getCiname());
	   return "countResult";
   }
   /**
    * 软件许可总数统计
    * @return String
    */
   public String softwareLicenseTotal(){
	   countTotal=ciService.softwareLicenseTotal(ciEditId);
	   return "countResult";
   }
   
    /**
     * 计算是否到期结果
     * @return String
     */
   public String calcExpired(){
	   effect=ciService.calcExpired(ciQueryDTO.getCiId());
	   return "importResult";
   }
   
   /**
    * 根据用户查询所关联的配置项
    * @return String
    */
   public String findPageConfigureItemByUser(){
	   int start = (page - 1) * rows;
	   configureItemsPagination = ciService.findPageConfigureItemByUser(ciQueryDTO,start,rows,sidx,sord);
       configureItemsPagination.setRows(rows);
       configureItemsPagination.setPage(page);
       return SUCCESS;
   }
   
   /**
    * 导入WMI文件
    * @return String
    */
   public String importWMIFile(){
	   effect=ciService.importWMIFile(importFile,ciDto);
	   return "importResult";
   }
   /**
    * 获取硬件
    * @return String
    */
   public String findConfigureItenHardware(){
	   hardwareDTOs=ciService.findConfigureItenHardware(ciQueryDTO.getCiId());
	   return "hardware";
   }
     
   /**
    * 添加关联的软件
    * @return String
    */
   public String addConfigureItemSoftware(){
	   
	   ciService.addConfigureItemSoftware(ciEditId, ids);
	   return SUCCESS;
   }
   
   /**
    * 删除关联的软件
    * @return String
    */ 
   public String deleteConfigureItemSoftware(){
	   ciService.deleteConfigureItemSoftware(ciEditId, ids);
	   return SUCCESS;
   }
   /**
    * 更新配置项
    * @return String
    */
   public String updateCi(){ 
	   ciService.scanToolUpdate(WMIJson); 
	   return null;
   }
   /**
    * 根据编号更新硬件信息
    * @return String
    */
   public String updateOneCi(){    
	   ciService.scanTooloneUpdate(loginName,WMIJson); 
	   return null;
   }
   /**
    * add configureItemTemplate
    * @return String
    */
   public String saveConfigureItemTemplate(){
	   
	   ciDto = ciService.convertCiTemplateValue(ciDto);
	   templateService.saveTemplate(ciDto, templateDTO);
	   return SUCCESS;
   }
   /**
    * add ci attachment
    * @return String
    */
   public String saveAttachment(){
	   ciService.saveCIAttachment(ciId, attachmentStr, eventType,aid);
	   return SUCCESS;
   }
   /**
    * 查询所有附件
    * @return String
    */
   public String findAllAttachment(){
	   attach=ciService.findConfigureItemById(ciId).getAttachments();
	   return "attach";
   }
   /**
    * 查询动态报表配置项列表
    * @return String
    */
   public String findConfigureItemsByCrosstabCell(){
   	int start = (page - 1) * rows; 
   	configureItemsPagination = ciService.findConfigureItemsByCrosstabCell(ktd,start,rows,sidx,sord);
   	configureItemsPagination.setRows(rows);
    configureItemsPagination.setPage(page);
   	return SUCCESS; 
   }
   /**
    * 根据ID获取配置项
    * @return String showisItemsInfo
    */
   public String findByciIdClent() {
        ciDetailDTO=ciService.findConfigureItemById(ciEditId);
	    
       return "showisItemsInfoClent";
   }
   
   public String findById(){
	   ciDetailDTO=ciService.findConfigureItemById(ciEditId);
	   //hardwareDTOs=ciService.findConfigureItenHardware(ciEditId);
	   
	   return "ciDetailDTO";
   }
   
   
   /**
    * 查询所有配置项
    * 已失效
    * @return String
    */
   public String aclUpUpdate(){
	   
	   ciService.aclUpUpdate();
	   
	   return SUCCESS;
   }
   /**
    * 根据ID查询配置项集合
    * @return String
    */
   public String findByIds(){
	   if(ids!=null && ids.length>0){
		   ciGridList=ciService.findByIds(ids);
	   }
	   return "ciGridList";
   }
   public String newPrefuseFindCINode(){
	   CIUtilDTO ciutil=new CIUtilDTO();
	   Set<Long> ids=new HashSet<Long>();
	   jason = ciprefuseService.newPrefuseFindCINode(ciId,ciutil,ids);
		return "jason";
	}
   	public String findCiPrefuseView(){
	   CIUtilDTO ciutil=new CIUtilDTO();
	   jason = ciprefuseService.findCiPrefuseView(ciId,ciutil);
		return "jason";
	}
   	public String findColor(){
 	   jason = ciprefuseService.findColor();
 		return "jason";
 	}
   /**
    * 根据修改行为ID查询
    * @return
    */
   public String findBehaviorModificationByid(){
	   behaviorModificationDTO=ciService.findBehaviorModificationByid(beId);
	   return "behaviorModificationDTO";
   }
   
   
   
   /**
	 * 生成导入模板
	 * @return
	 */
	public String createTemplateWithCategory(){
		try {
			SimpleFileDTO fileDTO = importCIService.generateTemplateWithCategory(categoryNo);
	   		if( fileDTO != null ){
	   			fileName = fileDTO.getFileName();
	   			exportStream = fileDTO.getInputStream();
	   		}
		} catch (Exception e) {
			LOGGER.error(e);//e.printStackTrace();
		}
  		return "exportSuccess";
  	}
}