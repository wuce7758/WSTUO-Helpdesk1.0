package com.wstuo.itsm.cim.dto;

import java.util.Date;
import com.wstuo.common.dto.BaseDTO;

/**
 * 配置项历史通知
 * @author WSTUO_QXY
 *
 */
@SuppressWarnings("serial")
public class CIHistoryNoticeQueryDTO extends BaseDTO {
	private Long id;
	private Long ciId;
	private Long type;
	private Date arrivalDate;
	private Date warningDate;
	private Integer lifeCycle;
	private Integer warranty;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getCiId() {
		return ciId;
	}
	public void setCiId(Long ciId) {
		this.ciId = ciId;
	}
	public Long getType() {
		return type;
	}
	public void setType(Long type) {
		this.type = type;
	}
	public Date getArrivalDate() {
		return arrivalDate;
	}
	public void setArrivalDate(Date arrivalDate) {
		this.arrivalDate = arrivalDate;
	}
	public Date getWarningDate() {
		return warningDate;
	}
	public void setWarningDate(Date warningDate) {
		this.warningDate = warningDate;
	}
	public Integer getLifeCycle() {
		return lifeCycle;
	}
	public void setLifeCycle(Integer lifeCycle) {
		this.lifeCycle = lifeCycle;
	}
	public Integer getWarranty() {
		return warranty;
	}
	public void setWarranty(Integer warranty) {
		this.warranty = warranty;
	}

}
