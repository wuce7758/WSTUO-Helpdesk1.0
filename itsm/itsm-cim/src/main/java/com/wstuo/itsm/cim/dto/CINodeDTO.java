package com.wstuo.itsm.cim.dto;
/**
 * 配置项节点信息DTO
 * @author QXY
 *
 */
public class CINodeDTO {
	private Long ciid;
	private String ciName;//CI名称
	private String cino;//CI编码
	private String categoryName;//CI分类
	private String ciStatus;//CI状态
	private String createTime;//创建时间
	private String lastUpdateTime;//最后更新时间
	public String remark;//备注
	private String categoryImg;//图片名称
	
	public Long getCiid() {
		return ciid;
	}
	public void setCiid(Long ciid) {
		this.ciid = ciid;
	}
	public String getCiName() {
		return ciName;
	}
	public void setCiName(String ciName) {
		this.ciName = ciName;
	}
	public String getCino() {
		return cino;
	}
	public void setCino(String cino) {
		this.cino = cino;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public String getCiStatus() {
		return ciStatus;
	}
	public void setCiStatus(String ciStatus) {
		this.ciStatus = ciStatus;
	}
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	public String getLastUpdateTime() {
		return lastUpdateTime;
	}
	public void setLastUpdateTime(String lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getCategoryImg() {
		return categoryImg;
	}
	public void setCategoryImg(String categoryImg) {
		this.categoryImg = categoryImg;
	}
}