package com.wstuo.itsm.cim.dto;

import java.util.Date;

import com.wstuo.common.dto.BaseDTO;
/**
 * 配置项扫描信息DTO
 * @author QXY
 *
 */
@SuppressWarnings("serial")
public class CIScanDTO extends BaseDTO{
	private Long ciId; // ci id
	private String cino; // ci num
	private String ciname; // ci name
	private String netWork_ip;// IP
	private String netWork_mac;// MAC
	private String operatingSystem_caption;// 系统名称
	private String CDI;// 配置项备注
	private String owner; // 拥有者
	private String userName; // 使用人

	private String buyDate; // 采购时间
	private Date warningDate; // 预警日期
	private String computerSystem_name;// 计算机名
	private String elapsedTime;// 过保时间
	private String serialNumber; // 序列号

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public Long getCiId() {
		return ciId;
	}

	public void setCiId(Long ciId) {
		this.ciId = ciId;
	}

	public String getElapsedTime() {
		if (elapsedTime == null)
			elapsedTime = "";

		return elapsedTime;
	}

	public void setElapsedTime(String elapsedTime) {
		this.elapsedTime = elapsedTime;
	}

	public String getComputerSystem_name() {
		if (computerSystem_name == null)
			computerSystem_name = "";
		return computerSystem_name;
	}

	public void setComputerSystem_name(String computerSystem_name) {
		this.computerSystem_name = computerSystem_name;
	}

	public String getOwner() {
		if (owner == null)
			owner = "";
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public String getBuyDate() {
		if (buyDate == null)
			buyDate = "";
		return buyDate;
	}

	public void setBuyDate(String buyDate) {
		this.buyDate = buyDate;
	}

	public Date getWarningDate() {

		return warningDate;
	}

	public void setWarningDate(Date warningDate) {
		this.warningDate = warningDate;
	}

	public String getCino() {
		if (cino == null)
			cino = "";
		return cino;
	}

	public void setCino(String cino) {
		this.cino = cino;
	}

	public String getCiname() {
		if (ciname == null)
			ciname = "";
		return ciname;
	}

	public void setCiname(String ciname) {
		this.ciname = ciname;
	}

	public String getNetWork_ip() {
		if (netWork_ip == null)
			netWork_ip = "";
		return netWork_ip;
	}

	public void setNetWork_ip(String netWork_ip) {
		this.netWork_ip = netWork_ip;
	}

	public String getNetWork_mac() {
		if (netWork_mac == null)
			netWork_mac = "";
		return netWork_mac;
	}

	public void setNetWork_mac(String netWork_mac) {
		this.netWork_mac = netWork_mac;
	}

	public String getOperatingSystem_caption() {
		if (operatingSystem_caption == null)
			operatingSystem_caption = "";
		return operatingSystem_caption;
	}

	public void setOperatingSystem_caption(String operatingSystem_caption) {
		this.operatingSystem_caption = operatingSystem_caption;
	}

	public String getCDI() {
		if (CDI == null)
			CDI = "";
		return CDI;
	}

	public void setCDI(String cDI) {
		CDI = cDI;
	}

}
