package com.wstuo.itsm.cim.dto;

import java.util.ArrayList;
import java.util.List;
/**
 * 配置项使用率信息DTO
 * @author QXY
 *
 */
public class CIUtilDTO {
	private List<CINodeDTO> cilist=new ArrayList<CINodeDTO>();
	private List<CIRelevanceEdgesDTO> releList=new ArrayList<CIRelevanceEdgesDTO>();
	
	public List<CIRelevanceEdgesDTO> getReleList() {
		return releList;
	}
	public void setReleList(List<CIRelevanceEdgesDTO> releList) {
		this.releList = releList;
	}
	public List<CINodeDTO> getCilist() {
		return cilist;
	}
	public void setCilist(List<CINodeDTO> cilist) {
		this.cilist = cilist;
	}

}
