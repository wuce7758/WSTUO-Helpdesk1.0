package com.wstuo.itsm.cim.dto;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.wstuo.common.dto.BaseDTO;

/**
 * 关联配置项树
 * @author WSTUO_QXY
 *
 */
@SuppressWarnings( "serial" )
public class CiRelevanceTreeViewDTO extends BaseDTO {
	private String data;
	private String state;
	private Map<String, String> attr = new HashMap<String, String>(  );
	private List<CiRelevanceTreeViewDTO> children = new ArrayList<CiRelevanceTreeViewDTO>(  );
	
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public Map<String, String> getAttr() {
		return attr;
	}
	public void setAttr(Map<String, String> attr) {
		this.attr = attr;
	}
	public List<CiRelevanceTreeViewDTO> getChildren() {
		return children;
	}
	public void setChildren(List<CiRelevanceTreeViewDTO> children) {
		this.children = children;
	}
	
}
