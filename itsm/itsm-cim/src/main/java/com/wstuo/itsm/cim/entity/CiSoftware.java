package com.wstuo.itsm.cim.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.wstuo.common.config.category.entity.EventCategory;
import com.wstuo.common.config.dictionary.entity.DataDictionaryItems;
import com.wstuo.common.entity.BaseEntity;

/**
 * 软件
 * @author will
 *
 */
@SuppressWarnings({ "serial", "rawtypes" })
@Entity
public class CiSoftware extends BaseEntity {
	
	@Id
    @GeneratedValue( strategy = GenerationType.AUTO )
	private Long softwareId;//软件ID
	@Column(nullable=true)
	private String softwareName;//软件名
	@Column(nullable=true)
	private String softwareVersion;//软件版本
	@ManyToOne(fetch=FetchType.LAZY)
	private DataDictionaryItems softwareType;//软件类型
	@ManyToOne(fetch=FetchType.LAZY)
    private EventCategory softwareCategory; //软件分类
	@ManyToOne(fetch=FetchType.LAZY)
    private DataDictionaryItems softwareProvider;//软件供应商
    @Column(nullable=true)
	private String installDate;//安装时间
    @Column(nullable=true)
	private String installState;//安装状态
    @Column(nullable=true)
   	private String caption;//标题
    @Column(nullable=true)
   	private String identifyingNumber;//标识编号
    @Column(nullable=true)
   	private String productID;// 产品ID
    @Column(nullable=true)
   	private String regOwner;//登记人
    @Column(nullable=true)
   	private String packageCache;//软件包缓存
    @Column(nullable=true)
   	private String packageCode;//软件包编码
    @Column(nullable=true)
   	private String assignmentType;//分配类型
    @Column(nullable=true)
   	private String localPackage;//本地包
    @Column(nullable=true)
   	private String installSource;//安装来源
    @Column(nullable=true)
   	private String installLocation;//安装位置
    @Column(nullable=true)
   	private String installDate2;//安装日期
    @Column(nullable=true)
   	private String vendor;// 供应商
    @Column(nullable=true)
   	private String packageName;//软件包名
    @Column(nullable=true)
   	private String softwareLanguage;//语言 
    @Column(nullable=true)
   	private String regCompany;//注册公司
    @Column(nullable=true)
   	private String skuNumber;//SKU编号
	
	public String getInstallState() {
		return installState;
	}
	public void setInstallState(String installState) {
		this.installState = installState;
	}
	public String getCaption() {
		return caption;
	}
	public void setCaption(String caption) {
		this.caption = caption;
	}
	public String getIdentifyingNumber() {
		return identifyingNumber;
	}
	public void setIdentifyingNumber(String identifyingNumber) {
		this.identifyingNumber = identifyingNumber;
	}
	public String getProductID() {
		return productID;
	}
	public void setProductID(String productID) {
		this.productID = productID;
	}
	public String getRegOwner() {
		return regOwner;
	}
	public void setRegOwner(String regOwner) {
		this.regOwner = regOwner;
	}
	public String getPackageCache() {
		return packageCache;
	}
	public void setPackageCache(String packageCache) {
		this.packageCache = packageCache;
	}
	public String getPackageCode() {
		return packageCode;
	}
	public void setPackageCode(String packageCode) {
		this.packageCode = packageCode;
	}
	public String getAssignmentType() {
		return assignmentType;
	}
	public void setAssignmentType(String assignmentType) {
		this.assignmentType = assignmentType;
	}
	public String getLocalPackage() {
		return localPackage;
	}
	public void setLocalPackage(String localPackage) {
		this.localPackage = localPackage;
	}
	public String getInstallSource() {
		return installSource;
	}
	public void setInstallSource(String installSource) {
		this.installSource = installSource;
	}
	public String getInstallLocation() {
		return installLocation;
	}
	public void setInstallLocation(String installLocation) {
		this.installLocation = installLocation;
	}
	public String getInstallDate2() {
		return installDate2;
	}
	public void setInstallDate2(String installDate2) {
		this.installDate2 = installDate2;
	}
	public String getVendor() {
		return vendor;
	}
	public void setVendor(String vendor) {
		this.vendor = vendor;
	}
	public String getPackageName() {
		return packageName;
	}
	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	public String getSoftwareLanguage() {
        return softwareLanguage;
    }
    public void setSoftwareLanguage(String softwareLanguage) {
        this.softwareLanguage = softwareLanguage;
    }
    public String getRegCompany() {
		return regCompany;
	}
	public void setRegCompany(String regCompany) {
		this.regCompany = regCompany;
	}
	public String getSkuNumber() {
		return skuNumber;
	}
	public void setSkuNumber(String skuNumber) {
		this.skuNumber = skuNumber;
	}
	public Long getSoftwareId() {
		return softwareId;
	}
	public void setSoftwareId(Long softwareId) {
		this.softwareId = softwareId;
	}
	
	public String getSoftwareName() {
		return softwareName;
	}
	public void setSoftwareName(String softwareName) {
		this.softwareName = softwareName;
	}
	
	
	public String getInstallDate() {
		return installDate;
	}
	public void setInstallDate(String installDate) {
		this.installDate = installDate;
	}
	
	
	public String getSoftwareVersion() {
		return softwareVersion;
	}
	public void setSoftwareVersion(String softwareVersion) {
		this.softwareVersion = softwareVersion;
	}
	
	
	public DataDictionaryItems getSoftwareType() {
		return softwareType;
	}
	public void setSoftwareType(DataDictionaryItems softwareType) {
		this.softwareType = softwareType;
	}
	
	
	public EventCategory getSoftwareCategory() {
		return softwareCategory;
	}
	public void setSoftwareCategory(EventCategory softwareCategory) {
		this.softwareCategory = softwareCategory;
	}
	
	
	public DataDictionaryItems getSoftwareProvider() {
		return softwareProvider;
	}
	public void setSoftwareProvider(DataDictionaryItems softwareProvider) {
		this.softwareProvider = softwareProvider;
	}
	
	
}
