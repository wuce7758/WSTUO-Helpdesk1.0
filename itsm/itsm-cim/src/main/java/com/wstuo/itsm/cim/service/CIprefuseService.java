package com.wstuo.itsm.cim.service;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.apache.struts2.json.JSONException;
import org.apache.struts2.json.JSONUtil;
import org.springframework.beans.factory.annotation.Autowired;

import com.wstuo.common.config.dictionary.dao.IDataDictionaryGroupDAO;
import com.wstuo.common.config.dictionary.entity.DataDictionaryGroup;
import com.wstuo.common.config.dictionary.entity.DataDictionaryItems;
import com.wstuo.common.config.server.dto.ServerUrlDTO;
import com.wstuo.common.config.server.service.IServerUrlService;
import com.wstuo.common.util.StringUtils;
import com.wstuo.itsm.cim.dao.ICIDAO;
import com.wstuo.itsm.cim.dao.ICIRelevanceDAO;
import com.wstuo.itsm.cim.dto.CIDictionaryColor;
import com.wstuo.itsm.cim.dto.CIDictionaryitems;
import com.wstuo.itsm.cim.dto.CINodeDTO;
import com.wstuo.itsm.cim.dto.CIRelevanceEdgesDTO;
import com.wstuo.itsm.cim.dto.CIUtilDTO;
import com.wstuo.itsm.cim.entity.CI;
import com.wstuo.itsm.cim.entity.CIRelevance;
/**
 * 配置项关联关系管理
 * @author QXY
 *
 */
public class CIprefuseService implements ICIprefuseService{
	private static final Logger LOGGER = Logger.getLogger(CIprefuseService.class);
	@Autowired
    private ICIDAO ciDAO;
	
	@Autowired
    private ICIRelevanceDAO relevanceDAO; 
	@Autowired
	private IServerUrlService serverUrlService;
	@Autowired
	private IDataDictionaryGroupDAO dataDictionaryGroupDAO;
	
	/**
	 * 查询配置项关联关系显示
	 */
	public String findCiPrefuseView(Long ciid,CIUtilDTO ciutil){
		oneCinodeDto(ciid,ciutil);
		String json=null;
		//关联配置项 节点
    	List<CIRelevance> ciReleList=relevanceDAO.findCiRelevanceList(ciid);
    	for(CIRelevance cr:ciReleList){
    		addCiNodeDTO(cr,ciutil);
    	}

    	//被关联配置项 节点
    	List<CIRelevance> nuCiReleList=relevanceDAO.findNuCiRelevanceList(ciid);
    	for(CIRelevance cr:nuCiReleList){
    		addCiNodeDTO(cr,ciutil);
    	}
    	Set<Long> st=new HashSet<Long>(); 
    	for(CINodeDTO i:ciutil.getCilist()){ 
    		st.add(i.getCiid());//不加重复的元素 
    	}
    	List<CINodeDTO> Nodelist=new ArrayList<CINodeDTO>();
    	for (CINodeDTO str:ciutil.getCilist()) {
    		if(st.contains(str.getCiid())){
        		Nodelist.add(str);
        		st.remove(str.getCiid());
    		}
    	}  
    	ciutil.setCilist(Nodelist);
    	ciutil.setReleList(addCIRelevanceEdgesDTO(ciutil,ciid));
    	 try {
			 json= JSONUtil.serialize(ciutil);
		} catch (JSONException e) {
			LOGGER.error(e);
		}
		return json;
	}
	/**
	 * 当前配置项信息
	 * @param ciid
	 * @param ciutil
	 */
	private void oneCinodeDto(Long ciid,CIUtilDTO ciutil){
		CINodeDTO ciNodeDTO=new CINodeDTO();
		CI ci = ciDAO.findById(ciid);
		entity2dto(ci, ciNodeDTO);
		ciutil.getCilist().add(ciNodeDTO);
	}
	/**
	 * 添加正向关联节点
	 * @param cr
	 * @param ciutil
	 */
	private void addCiNodeDTO(CIRelevance cr,CIUtilDTO ciutil){
		CINodeDTO ciNodeDTO=new CINodeDTO();
		CI ci = ciDAO.findById(cr.getCiRelevanceId().getCiId());
		entity2dto(ci, ciNodeDTO);
		ciutil.getCilist().add(ciNodeDTO);
		
		
		CINodeDTO ciNodeDTOs=new CINodeDTO();
		CI cis = ciDAO.findById(cr.getUnCiRelevanceId().getCiId());
		entity2dto(cis, ciNodeDTOs);
		ciutil.getCilist().add(ciNodeDTOs);
		
	}
	/**
	 * 关联关系线
	 * @param ciutil
	 * @param ciid
	 * @return String
	 */
	private List<CIRelevanceEdgesDTO> addCIRelevanceEdgesDTO(CIUtilDTO ciutil,long ciid){
		List<CIRelevance> li=relevanceDAO.findBy("ciRelevanceId.ciId",ciid);
		for(CIRelevance cr:li){
			CIRelevanceEdgesDTO cirele=new CIRelevanceEdgesDTO();
			cirele.setRelevanceId(cr.getRelevanceId());
			cirele.setCiRelevance(cr.getCiRelevanceId().getCiId());
			cirele.setUnCiRelevance(cr.getUnCiRelevanceId().getCiId());
			if(cr.getCiRelationType()!=null){
				cirele.setCiRelationTypeName(cr.getCiRelationType().getDname());//类型名称 
			}
			ciutil.getReleList().add(cirele);
		}
		List<CIRelevance> li2=relevanceDAO.findBy("unCiRelevanceId.ciId",ciid);
		for(CIRelevance cr:li2){
			CIRelevanceEdgesDTO cirele=new CIRelevanceEdgesDTO();
			cirele.setRelevanceId(cr.getRelevanceId());
			cirele.setCiRelevance(cr.getCiRelevanceId().getCiId());
			cirele.setUnCiRelevance(cr.getUnCiRelevanceId().getCiId());
			if(cr.getCiRelationType()!=null){
				cirele.setCiRelationTypeName(cr.getCiRelationType().getDname());//类型名称
			}
			ciutil.getReleList().add(cirele);
		}

		return ciutil.getReleList();
	}
	
	/**
	 * 查询配置项的节点 *(转换Jason字符)
	 * @param ciid
	 * @param ciutil
	 * @return String
	 */
	public String newPrefuseFindCINode(Long ciid,CIUtilDTO ciutil,Set<Long> ids){
		ids.add(ciid);
		String jason =findCINode(ciid, ciutil,ids);
		jason = jason.replace('{', '<').replace('}', '>').replace('"','_');
		return jason;
	}
	/**
	 * 查询系统链接
	 */
	public String findUrl(){
		String url = "";
		ServerUrlDTO serverUrlDTO = serverUrlService.findServiceUrl();
		if(StringUtils.hasText(serverUrlDTO.getUrlName())){
			url=serverUrlDTO.getUrlName();
		}
		return url;
	}
	/**
	 * 查询对应色码
	 */
	public String findColor(){
		String jason = "";
		CIDictionaryColor cc=new CIDictionaryColor();
//		DataDictionaryGroup group= dataDictionaryGroupDAO.findUniqueBy("groupCode","useStatus");
//		List<CIDictionaryitems> status = new ArrayList<CIDictionaryitems>();
//		if(group!=null && group.getDatadicItems().size()>0)
//		for(DataDictionaryItems dt:group.getDatadicItems()){
//			CIDictionaryitems cd=new CIDictionaryitems();
//			cd.setDcode(dt.getDcode());
//			cd.setDname(dt.getDname());
//			cd.setColor(dt.getColor());
//			status.add(cd);
//		}
//		cc.setStatus(status);
		DataDictionaryGroup groups= dataDictionaryGroupDAO.findUniqueBy("groupCode","ciRelationType");
		List<CIDictionaryitems> cirelationlist = new ArrayList<CIDictionaryitems>();
		if(groups!=null && groups.getDatadicItems().size()>0)
		for(DataDictionaryItems dt:groups.getDatadicItems()){
			CIDictionaryitems cd=new CIDictionaryitems();
			cd.setDcode(dt.getDcode());
			cd.setDname(dt.getDname());
			cd.setColor(dt.getColor());
			cirelationlist.add(cd);
		}
		cc.setCirelationlist(cirelationlist);
		try {
			jason= JSONUtil.serialize(cc);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		jason = jason.replace('{', '<').replace('}', '>').replace('"','_');
		return jason;
	}
	

	/**
	 * 查询配置项的节点
	 * */
	public String findCINode(Long ciid,CIUtilDTO ciutil,Set<Long> ids){
		CINodeDTO ciNodeDTO=new CINodeDTO();
		String json=null;
		CI ci = ciDAO.findById(ciid);
		entity2dto(ci, ciNodeDTO);
		
		ciutil.getCilist().add(ciNodeDTO);
		
		List<CIRelevance> li=relevanceDAO.findBy("ciRelevanceId.ciId",ci.getCiId());
		
		for(CIRelevance cr:li){
			if(!ids.contains(cr.getUnCiRelevanceId().getCiId())){
				CIRelevanceEdgesDTO cirele=new CIRelevanceEdgesDTO();
				cirele.setRelevanceId(cr.getRelevanceId());
				cirele.setCiRelevance(cr.getCiRelevanceId().getCiId());
				cirele.setUnCiRelevance(cr.getUnCiRelevanceId().getCiId());
				if(cr.getCiRelationType()!=null){
					cirele.setCiRelationTypeName(cr.getCiRelationType().getDname());//类型名称
				}
				ciutil.getReleList().add(cirele);
				ids.add(cr.getUnCiRelevanceId().getCiId());
				this.findCINode(cr.getUnCiRelevanceId().getCiId(),ciutil,ids);
			}
		}
		
		List<CIRelevance> li2=relevanceDAO.findBy("unCiRelevanceId.ciId",ci.getCiId());
		
		for(CIRelevance cr:li2){
			if(!ids.contains(cr.getCiRelevanceId().getCiId())){
				CIRelevanceEdgesDTO cirele=new CIRelevanceEdgesDTO();
				cirele.setRelevanceId(cr.getRelevanceId());
				cirele.setCiRelevance(cr.getCiRelevanceId().getCiId());
				cirele.setUnCiRelevance(cr.getUnCiRelevanceId().getCiId());
				if(cr.getCiRelationType()!=null){
					cirele.setCiRelationTypeName(cr.getCiRelationType().getDname());//类型名称
				}
				ciutil.getReleList().add(cirele);
				ids.add(cr.getCiRelevanceId().getCiId());
				this.findCINode(cr.getCiRelevanceId().getCiId(),ciutil,ids);
			}
		}
		
		 try {
			 json= JSONUtil.serialize(ciutil);
		} catch (JSONException e) {
			LOGGER.error(e);
		}
		 
		return json;
		
	}
	/**
	 * 转换
	 * @param ci
	 * @param ciNodeDTO
	 */
	private void entity2dto(CI ci,CINodeDTO ciNodeDTO){
		SimpleDateFormat format=new SimpleDateFormat("yyyy-MM-dd");
		ciNodeDTO.setCiid(ci.getCiId());
		ciNodeDTO.setCino(ci.getCino());
		ciNodeDTO.setCiName(ci.getCiname().replaceAll("_", "").replaceAll("<", "").replaceAll(">", ""));
		if(ci.getCategory()!=null){
			ciNodeDTO.setCategoryName(ci.getCategory().getCname());
		}
		if(ci.getStatus()!=null){
			ciNodeDTO.setCiStatus(ci.getStatus().getDname());
		}
		if(ci.getCreateTime()!=null){
			String createTime=format.format(ci.getCreateTime());
			if(StringUtils.hasText(createTime)){
				ciNodeDTO.setCreateTime(createTime);
			}
		}
		if(ci.getLastUpdateTime()!=null){
			String lastUpdateTime=format.format(ci.getLastUpdateTime());
			if(StringUtils.hasText(lastUpdateTime)){
				ciNodeDTO.setLastUpdateTime(lastUpdateTime);
			}
		}
		if(ci.getCDI()!=null){
			ciNodeDTO.setRemark(ci.getCDI());
		}	
		if(ci.getCategory()!=null && ci.getCategory().getIconPath()!=null){
			ciNodeDTO.setCategoryImg(ci.getCategory().getIconPath());
		}
			
	}
}
