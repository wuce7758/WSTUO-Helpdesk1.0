package com.wstuo.itsm.cim.service;

import com.wstuo.itsm.cim.dto.SimpleFileDTO;

public interface IImportCIService {

	SimpleFileDTO generateTemplateWithCategory(Long categoryNo);
}
