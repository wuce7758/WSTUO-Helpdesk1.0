package com.wstuo.itsm.itsop.itsopuser.entity;

import java.util.List;

import javax.persistence.Cacheable;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.wstuo.common.config.dictionary.entity.DataDictionaryItems;
import com.wstuo.common.security.entity.Company;
import com.wstuo.common.security.entity.User;
import com.wstuo.common.util.StringUtils;

/**
 * 外部客户的用户实体类
 * @author QXY
 *
 */
@SuppressWarnings("serial")
@Entity
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ITSOPUser extends Company{
	
	@ManyToMany(fetch=FetchType.LAZY)
	private List<User> technicians;//负责人，技术员
	@ManyToOne
	private DataDictionaryItems type;
	private String technicianNames;
	@Transient
	public String getOrgType() {
			return "itsop";
	}

	public List<User> getTechnicians() {
		return technicians;
	}

	public void setTechnicians(List<User> technicians) {
		this.technicians = technicians;
	}


	public DataDictionaryItems getType() {
		return type;
	}



	public void setType(DataDictionaryItems type) {
		this.type = type;
	}

	public String getTechnicianNames() {
		return technicianNames;
	}


	public void setTechnicianNames(String technicianNames) {
		this.technicianNames = technicianNames;
	}

	public String generateTcNames(){
		
		String techNames="";
		
		if(technicians!=null && technicians.size()>0){
			
			for(User u:technicians){
				if(StringUtils.hasText(u.getFullName())){
					techNames+=u.getFullName()+";";
				}
			}
		}
		
		return techNames;
	}
	
	

	
	
	

}
