package com.wstuo.common.createmq;

import org.apache.activemq.command.ActiveMQQueue;
import org.springframework.jms.core.JmsTemplate;
import com.wstuo.itsm.request.dto.RequestHttpDTO;

/**
 * 发送创建请求信息
 * @author will
 *
 */
public class RequestCreateMessageProducer {
	private JmsTemplate template;


	public void setTemplate(JmsTemplate template) {
	   this.template = template;
	}

	public void send(RequestHttpDTO httpDto) {
		ActiveMQQueue queue=new ActiveMQQueue("RequestCreateMessageQueue."+httpDto.getTenantId());
		template.convertAndSend(queue, httpDto);
	} 
}
