package com.wstuo.itsm.request.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.util.StringUtils;

import com.wstuo.common.tools.entity.ExportInfo;
import com.wstuo.common.dto.BaseDTO;
import com.wstuo.common.dto.FullTextQueryDTO;
import com.wstuo.common.util.MathUtils;

/**
 * 请求查询DTO类.
 * 
 * @author QXY
 * 
 */
@SuppressWarnings("serial")
public class RequestQueryDTO extends BaseDTO {
	private Long eno;
	private Long ecategoryNo;
	private String ecategoryName;
	private String etitle;
	private String edesc;
	private Long levelNo;
	private String levelName;
	private Long imodeNo;
	private String imodeName;
	private Long effectRangeNo;
	private String effectRangeName;
	private String effectRemark;
	private Long seriousnessNo;
	private String seriousnessName;
	private Long priorityNo;
	private String priorityName;
	private Long statusNo;
	private String statusName;
	private String address;
	private Date createdOn;
	private Long createdByNo;
	private String createdByName;
	private Long technicianNo;
	private String technicianName;
	private Long[] relatedConfigureItemNos;// 关联配置项编号
	private Integer start = 0;
	private Integer limit = 10;
	private String currentUser;
	private String countQueryType;

	// 当前用户
	private String userName;
	// 当前用户所以部门ID
	private Long orgNo;
	private String loginName;
	// 我负责的请求
	private String ownerName;
	// 关键字
	private String keyWord;
	private String requestCode;

	private String name;
	private Long[] categoryNos;
	private Date startTime;
	private Date endTime;
	private String assigneeName;// 指派人名字
	private Long assigneeGroupNo;
	private String companyName;// 客户名称
	private Long companyNo;// 客户No
	private Long[] companyNos;// 多个客户No
	private Boolean hang=false;// 挂起(20110831-QXY)
	private Long filterId;// 过滤器ID
	private Long slaStateNo;// SLA状态ID
	private String slaStateName;// SLA状态Name
	private Long requestorOrgNo;// 请求人所在组
	// 按全名查询
	private String createdByFullName;
	private String assigneeFullName;
	private String ownerFullName;
	private String mqTitle;// MQ执行的主题
	private String mqContent;// MQ执行的内容
	private String mqCreator;// MQ创建者
	private ExportInfo exportInfo;
	private String sord;
	private String sidx;

	private String lang; // i18n导出国际化参数
	private Long[] Groups;// 所属组
	private String requestorMoblie;// 请求人手机号码
	private String requestorPhone;// 请求人电话号码
	// 额外
	private String queryTag = "";
	private Long ciId;
	private String groupField;//分组统计字段
	private String label;
	private String tenantId;
	private Long locationId;
	private Long serviceDirIds;//服務目錄
	
    private Map<String,String> attrVals =new HashMap<String, String>();
	private Long[] ids;
	private String alias;
	
	
	public Long[] getIds() {
		return ids;
	}

	public void setIds(Long[] ids) {
		this.ids = ids;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public Map<String, String> getAttrVals() {
		return attrVals;
	}

	public void setAttrVals(Map<String, String> attrVals) {
		this.attrVals = attrVals;
	}

	public Long getServiceDirIds() {
		return serviceDirIds;
	}

	public void setServiceDirIds(Long serviceDirIds) {
		this.serviceDirIds = serviceDirIds;
	}

	public Long getLocationId() {
		return locationId;
	}

	public void setLocationId(Long locationId) {
		this.locationId = locationId;
	}

	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public Long getCiId() {
		return ciId;
	}

	public void setCiId(Long ciId) {
		this.ciId = ciId;
	}

	public Long[] getRelatedConfigureItemNos() {
		return relatedConfigureItemNos;
	}

	public void setRelatedConfigureItemNos(Long[] relatedConfigureItemNos) {
		this.relatedConfigureItemNos = relatedConfigureItemNos;
	}

	public Long[] getGroups() {
		return Groups;
	}

	public void setGroups(Long[] groups) {
		Groups = groups;
	}

	public String getLang() {
		return lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	public String getSord() {
		return sord;
	}

	public void setSord(String sord) {
		this.sord = sord;
	}

	public String getSidx() {
		return sidx;
	}

	public void setSidx(String sidx) {
		this.sidx = sidx;
	}

	public String getMqTitle() {
		return mqTitle;
	}

	public void setMqTitle(String mqTitle) {
		this.mqTitle = mqTitle;
	}

	public String getMqContent() {
		return mqContent;
	}

	public void setMqContent(String mqContent) {
		this.mqContent = mqContent;
	}

	public String getMqCreator() {
		return mqCreator;
	}

	public void setMqCreator(String mqCreator) {
		this.mqCreator = mqCreator;
	}

	public ExportInfo getExportInfo() {
		return exportInfo;
	}

	public void setExportInfo(ExportInfo exportInfo) {
		this.exportInfo = exportInfo;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRequestCode() {
		return requestCode;
	}

	public void setRequestCode(String requestCode) {
		this.requestCode = requestCode;
	}

	public String getQueryTag() {
		return queryTag;
	}

	public void setQueryTag(String queryTag) {
		this.queryTag = queryTag;
	}

	public String getKeyWord() {
		return keyWord;
	}

	public void setKeyWord(String keyWord) {
		this.keyWord = keyWord;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	private String waitAssign;

	public String getWaitAssign() {
		return waitAssign;
	}

	public void setWaitAssign(String waitAssign) {
		this.waitAssign = waitAssign;
	}

	public RequestQueryDTO() {
	}

	public Long getEno() {
		return eno;
	}

	public void setEno(Long eno) {
		this.eno = eno;
	}

	public Long getEcategoryNo() {
		return ecategoryNo;
	}

	public void setEcategoryNo(Long ecategoryNo) {
		this.ecategoryNo = ecategoryNo;
	}

	public String getEcategoryName() {
		return ecategoryName;
	}

	public void setEcategoryName(String ecategoryName) {
		this.ecategoryName = ecategoryName;
	}

	public String getEtitle() {
		return etitle;
	}

	public void setEtitle(String etitle) {
		this.etitle = etitle.replace("%", "\\%");
	}

	public String getEdesc() {
		return edesc;
	}

	public void setEdesc(String edesc) {
		this.edesc = edesc.replace("%", "\\%");
	}

	public Long getEffectRangeNo() {
		return effectRangeNo;
	}

	public void setEffectRangeNo(Long effectRangeNo) {
		this.effectRangeNo = effectRangeNo;
	}

	public String getEffectRangeName() {
		return effectRangeName;
	}

	public void setEffectRangeName(String effectRangeName) {
		this.effectRangeName = effectRangeName;
	}

	public String getEffectRemark() {
		return effectRemark;
	}

	public void setEffectRemark(String effectRemark) {
		this.effectRemark = effectRemark;
	}

	public Long getSeriousnessNo() {
		return seriousnessNo;
	}

	public void setSeriousnessNo(Long seriousnessNo) {
		this.seriousnessNo = seriousnessNo;
	}

	public String getSeriousnessName() {
		return seriousnessName;
	}

	public void setSeriousnessName(String seriousnessName) {
		this.seriousnessName = seriousnessName;
	}

	public Long getPriorityNo() {
		return priorityNo;
	}

	public void setPriorityNo(Long priorityNo) {
		this.priorityNo = priorityNo;
	}

	public String getPriorityName() {
		return priorityName;
	}

	public void setPriorityName(String priorityName) {
		this.priorityName = priorityName;
	}

	public Long getStatusNo() {
		return statusNo;
	}

	public void setStatusNo(Long statusNo) {
		this.statusNo = statusNo;
	}

	public String getStatusName() {
		return statusName;
	}

	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Long getCreatedByNo() {
		return createdByNo;
	}

	public void setCreatedByNo(Long createdByNo) {
		this.createdByNo = createdByNo;
	}

	public String getCreatedByName() {
		return createdByName;
	}

	public void setCreatedByName(String createdByName) {
		this.createdByName = createdByName;
	}

	public String[] getCreatedByNames() {
		return StringUtils.hasText(this.getCreatedByName()) ? this
				.getCreatedByName().split(";") : null;
	}

	public Long getTechnicianNo() {
		return technicianNo;
	}

	public void setTechnicianNo(Long technicianNo) {
		this.technicianNo = technicianNo;
	}

	public String getTechnicianName() {
		return technicianName;
	}

	public void setTechnicianName(String technicianName) {
		this.technicianName = technicianName;
	}

	public Integer getStart() {
		return start;
	}

	public void setStart(Integer start) {
		this.start = start;
	}

	public Integer getLimit() {
		return limit;
	}

	public void setLimit(Integer limit) {
		this.limit = limit;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Long getOrgNo() {
		return orgNo;
	}

	public void setOrgNo(Long orgNo) {
		this.orgNo = orgNo;
	}

	public String getOwnerName() {
		return ownerName;
	}

	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}

	public String[] getOwnerNames() {
		return StringUtils.hasText(this.ownerName) ? this.ownerName.split(";")
				: null;
	}


	public String getCurrentUser() {
		return currentUser;
	}

	public void setCurrentUser(String currentUser) {
		this.currentUser = currentUser;
	}

	public String getCountQueryType() {
		return countQueryType;
	}

	public void setCountQueryType(String countQueryType) {
		this.countQueryType = countQueryType;
	}

	public Long[] getCategoryNos() {
		return categoryNos;
	}

	public void setCategoryNos(Long[] categoryNos) {
		this.categoryNos = categoryNos;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public Long getCompanyNo() {
		return companyNo;
	}

	public void setCompanyNo(Long companyNo) {
		this.companyNo = companyNo;
	}

	public Long[] getCompanyNos() {
		return companyNos;
	}

	public void setCompanyNos(Long[] companyNos) {
		this.companyNos = companyNos;
	}
	public Boolean getHang() {
		return hang;
	}

	public void setHang(Boolean hang) {
		this.hang = hang;
	}

	public Long getLevelNo() {
		return levelNo;
	}

	public void setLevelNo(Long levelNo) {
		this.levelNo = levelNo;
	}

	public String getLevelName() {
		return levelName;
	}

	public void setLevelName(String levelName) {
		this.levelName = levelName;
	}

	public Long getImodeNo() {
		return imodeNo;
	}

	public void setImodeNo(Long imodeNo) {
		this.imodeNo = imodeNo;
	}

	public String getImodeName() {
		return imodeName;
	}

	public void setImodeName(String imodeName) {
		this.imodeName = imodeName;
	}

	public String getAssigneeName() {
		return assigneeName;
	}

	public void setAssigneeName(String assigneeName) {
		this.assigneeName = assigneeName;
	}

	public String[] getAssigneeNames() {
		return StringUtils.hasText(this.assigneeName) ? this.assigneeName
				.split(";") : null;
	}


	public Long getAssigneeGroupNo() {
		return assigneeGroupNo;
	}

	public void setAssigneeGroupNo(Long assigneeGroupNo) {
		this.assigneeGroupNo = assigneeGroupNo;
	}

	public Long getFilterId() {
		return filterId;
	}

	public void setFilterId(Long filterId) {
		this.filterId = filterId;
	}

	public Long getSlaStateNo() {
		return slaStateNo;
	}

	public void setSlaStateNo(Long slaStateNo) {
		this.slaStateNo = slaStateNo;
	}

	public String getSlaStateName() {
		return slaStateName;
	}

	public void setSlaStateName(String slaStateName) {
		this.slaStateName = slaStateName;
	}

	public Long getRequestorOrgNo() {
		return requestorOrgNo;
	}

	public void setRequestorOrgNo(Long requestorOrgNo) {
		this.requestorOrgNo = requestorOrgNo;
	}

	public String getAssigneeFullName() {
		return assigneeFullName;
	}

	public void setAssigneeFullName(String assigneeFullName) {
		this.assigneeFullName = assigneeFullName;
	}

	public String[] getAssigneeFullNames() {
		return StringUtils.hasText(this.assigneeFullName) ? this.assigneeFullName
				.split(";") : null;
	}


	public String getOwnerFullName() {
		return ownerFullName;
	}

	public void setOwnerFullName(String ownerFullName) {
		this.ownerFullName = ownerFullName;
	}

	public String[] getOwnerFullNames() {
		return StringUtils.hasText(this.ownerFullName) ? this.ownerFullName
				.split(";") : null;
	}

	public String getCreatedByFullName() {
		return createdByFullName;
	}

	public void setCreatedByFullName(String createdByFullName) {
		this.createdByFullName = createdByFullName;
	}

	public String[] getCreatedByFullNames() {
		return StringUtils.hasText(this.createdByFullName) ? this.createdByFullName
				.split(";") : null;
	}

	public String getRequestorMoblie() {
		return requestorMoblie;
	}

	public void setRequestorMoblie(String requestorMoblie) {
		this.requestorMoblie = requestorMoblie;
	}

	public String getRequestorPhone() {
		return requestorPhone;
	}

	public void setRequestorPhone(String requestorPhone) {
		this.requestorPhone = requestorPhone;
	}
	public String getGroupField() {
		return groupField;
	}

	public void setGroupField(String groupField) {
		this.groupField = groupField;
	}
	/**
	 * 判断是否满足创建请求人的条件
	 * 
	 * @param qdto
	 * @return
	 */
	public static Boolean isCreateAliasForCreateBy(RequestQueryDTO qdto) {
		return (StringUtils.hasText(qdto.getCountQueryType()) && ("myProposedRequest"
				.equals(qdto.getCountQueryType())
				|| "myGroupProposedRequest".equals(qdto.getCountQueryType()) || "myNotComprehensiveNotSubmitted"
					.equals(qdto.getCountQueryType())))
				|| StringUtils.hasText(qdto.getCreatedByName())
				|| StringUtils.hasText(qdto.getRequestorMoblie())
				|| StringUtils.hasText(qdto.getRequestorPhone())
				|| MathUtils.isPositive(qdto.getRequestorOrgNo());
	}

	/**
	 * 判断是否满足创建技术员的条件
	 * 
	 * @param qdto
	 * @return
	 */
	public static Boolean isCeateAliasForTechnician(RequestQueryDTO qdto) {
		Boolean result = false;
		if (null != qdto.getAssigneeNames()||null != qdto.getAssigneeFullNames()||StringUtils.hasText(qdto.getCountQueryType())
				|| StringUtils.hasText(qdto.getAssigneeName())) {
			result = StringUtils.hasText(qdto.getAssigneeName())
					|| (StringUtils.hasText(qdto.getCountQueryType())
							&& ("nohandle".equals(qdto.getCountQueryType())
									|| "handle"
											.equals(qdto.getCountQueryType())
									|| "complete".equals(qdto
											.getCountQueryType())
									|| "close".equals(qdto.getCountQueryType()) || "assigneeToMyRequest"
										.equals(qdto.getCountQueryType()))
							|| StringUtils.hasText(qdto.getAssigneeFullName()) || "actingToMyRequest"
							.equals(qdto.getCountQueryType())
							&& StringUtils.hasText(qdto.getCurrentUser()));
		}
		return result;
	}

	/**
	 * 判断是否满足创建负责人的条件
	 * 
	 * @param qdto
	 * @return
	 */
	public static Boolean isCreateAliasForOwner(RequestQueryDTO qdto) {
		return null != qdto.getOwnerNames()
				|| StringUtils.hasText(qdto.getOwnerFullName())
				|| "myOwnerRequest".equals(qdto.getCountQueryType())
				&& StringUtils.hasText(qdto.getCurrentUser());
	}

	/**
	 * 是否存在查询条件
	 * 
	 * @param qdto
	 * @param countQueryTypes
	 * @return
	 */
	public static Boolean isContainQueryType(RequestQueryDTO qdto) {
		List<String> countQueryTypes = new ArrayList<String>();
		countQueryTypes.add("nohandle");
		countQueryTypes.add("complete");
		countQueryTypes.add("close");
		countQueryTypes.add("myNotComprehensiveNotSubmitted");
		countQueryTypes.add("handle");
		return countQueryTypes.contains(qdto.getCountQueryType());
	}

	/**
	 * 是否满足查询登录名的条件
	 * @param qdto
	 * @return
	 */
	public static Boolean isMatchToSearchLoginNameCondition(RequestQueryDTO qdto) {
		return "nohandle".equals(qdto.getCountQueryType())
				|| "handle".equals(qdto.getCountQueryType())
				|| "complete".equals(qdto.getCountQueryType())
				|| "close".equals(qdto.getCountQueryType())
			/*	|| "myNotComprehensiveNotSubmitted".equals(qdto
						.getCountQueryType())*/;
	}

	
	
}
