package com.wstuo.itsm.request.service;

import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;
import org.jbpm.api.task.AssignmentHandler;

/**
 * 工作流任务指派类
 * 在流程图中有使用
 * @author QXY
 * 2.4GA版本开始废除
 */
@SuppressWarnings("serial")
@Deprecated
public class AssignTask implements AssignmentHandler {
	/**
	 * 请求任务指派
	 */
	public void assign(Assignable assignable, OpenExecution execution) throws Exception {
		
	}
}
