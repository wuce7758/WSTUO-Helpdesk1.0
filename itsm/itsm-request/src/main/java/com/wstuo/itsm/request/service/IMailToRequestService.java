package com.wstuo.itsm.request.service;

/**
 * 邮件转请求Service接口
 * @author QXY
 *
 */
public interface IMailToRequestService {

	/**
	 * 邮件扫描，自动转请求
	 */
	void convertMail();

}