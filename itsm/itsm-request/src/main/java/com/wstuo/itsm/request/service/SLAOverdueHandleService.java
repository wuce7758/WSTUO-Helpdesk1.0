package com.wstuo.itsm.request.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.wstuo.common.noticeRule.service.INoticeRuleService;
import com.wstuo.common.sla.dao.IPromoteRuleDAO;
import com.wstuo.common.sla.dao.ISLAEmailControlDAO;
import com.wstuo.common.sla.entity.PromoteRule;
import com.wstuo.common.sla.entity.SLAEmailControl;
import com.wstuo.itsm.request.entity.Request;
import com.wstuo.itsm.request.dao.IRequestDAO;
import com.wstuo.common.security.dao.IUserDAO;

/**
 * SLA逾期处理(升级及通知)Service
 * @author QXY
 */
public class SLAOverdueHandleService implements ISLAOverdueHandleService{
	@Autowired
	private IRequestDAO requestDAO;
	@Autowired
	private IUserDAO userDAO;
	@Autowired
	private ISLAEmailControlDAO slaEmailControlDAO;
	@Autowired
	private IRequestService requestService;
	@Autowired
    private IPromoteRuleDAO promoteRuleDAO;
	
	private static final String PROMOTERULETYPE_PROMOTE="promote";
	private static final String PROMOTERULETYPE_OVERDUE="overdue";
	private static final String REFERTYPE_RESPONSE="response";
	
	/**
	 * SLA逾期处理
	 */
	@Transactional
	public void slaOverdueHandle(){
		slaOverdueUpgrade();
		slaOverdueNoticeTechnician();
	}

	/**
	 * SLA逾期自动升级
	 */
	@Transactional
	private void slaOverdueUpgrade(){
		List<PromoteRule> promoteRule = promoteRuleDAO.findPromoteRuleByruleType(PROMOTERULETYPE_PROMOTE);
		for (PromoteRule pr : promoteRule) {
			if(pr!=null){
				List<Request> requests = getOverdueRequests(pr);
				for (Request request : requests) {
					request.setOwner(userDAO.findById(pr.getUpdateLevel().getUserId()));
					request.setUpgradeApplySign(IRequestService.REQUEST_UPGRADEAPPLY_SIGN);
					//升级通知
					SLAEmailControl slaEmailControl = setSLAEmailControl(request, pr);
					//逾期通知
					if(!isHaveNoticed(slaEmailControl)){
						requestService.requestProcessNotice(INoticeRuleService.REQUEST_UPGRADE_NOTICE, request, null, null, null, null,null,null);
						slaEmailControlDAO.save(slaEmailControl);
					}
				}
			}
			
		}
	}
	
	/**
	 * SLA逾期通知技术员
	 */
	@Transactional
	private void slaOverdueNoticeTechnician(){
		//得到所有SLA配置(存在逾期通知规则)
		List<PromoteRule> promoteRule = promoteRuleDAO.findPromoteRuleByruleType(PROMOTERULETYPE_OVERDUE);
		for (PromoteRule pr : promoteRule) {
			if(pr!=null){
				List<Request> requests = getOverdueRequests(pr);
				for (Request request : requests) {
					SLAEmailControl slaEmailControl = setSLAEmailControl(request, pr);
					//逾期通知
					if(!isHaveNoticed(slaEmailControl)){
						requestService.requestProcessNotice(INoticeRuleService.REQUEST_SLA_OVER_DUE_NOTICE, request, null, null, null, null,null,null);
						slaEmailControlDAO.save(slaEmailControl);
					}
				}
			}
		}
	}
	
	/**
	 * 获取过期的请求
	 * @return List<Request>
	 */
	@Transactional
	private List<Request> getOverdueRequests(PromoteRule pr){
		Long contractNo = pr.getSlaContract().getContractNo();
		String referType = pr.getReferType();
		Long nowDate = new Date().getTime();
		Long responseTime = null;
		Long completeTime = null;
		Long ruleTime=0l;
		if(pr.getRuleTime()!=null)
			ruleTime = pr.getRuleTime();
		
		if(pr.getBeforeOrAfter()){	//"逾期后升级"
			if(REFERTYPE_RESPONSE.equals(referType)){
				responseTime = nowDate-ruleTime*1000;
			}else{
				completeTime = nowDate-ruleTime*1000;
			}
		}else{
			if(REFERTYPE_RESPONSE.equals(referType)){
				responseTime = nowDate+ruleTime*1000;
			}else{
				completeTime = nowDate+ruleTime*1000;
			}
		}
		List<Request> requests = requestDAO.findEligibleRequests(contractNo, responseTime, completeTime);
		
		return requests;
	}
	/**
	 * is Have Noticed
	 * @param request
	 * @param pr
	 * @return boolean
	 */
	@Transactional
	private boolean isHaveNoticed(SLAEmailControl slaEmailControl){
		boolean bool = false;
		List<SLAEmailControl> list = slaEmailControlDAO.findSLAEmailControl(slaEmailControl);
		if(list.size()>0){
			bool = true;
		}
		return bool;
	}
	
	/**
	 * set SLAEmailControl
	 * @param request
	 * @param pr
	 * @return SLAEmailControl
	 */
	@Transactional
	private SLAEmailControl setSLAEmailControl(Request request,PromoteRule pr){
		SLAEmailControl slaEmailControl = new SLAEmailControl();
		slaEmailControl.setEno(request.getEno());
		slaEmailControl.setRuleNo(pr.getRuleNo());
		slaEmailControl.setRuleType(pr.getRuleType());
		slaEmailControl.setReferType(pr.getReferType());
		slaEmailControl.setUpgradeTime(pr.getRuleTime());
		if(pr.getUpdateLevel()!=null){
			slaEmailControl.setUpdateLevelUserId(pr.getUpdateLevel().getUserId());
		}
		return slaEmailControl;
	}
}
