package com.wstuo.itsm.updateRequest.action;

import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.xwork2.ActionSupport;
import com.wstuo.itsm.updateRequest.dto.UpdateRequestDTO;
import com.wstuo.itsm.updateRequest.service.IUpdateRequestService;
/**
 * 升级时间Action class
 * @author Will
 *
 */
@SuppressWarnings("serial")
public class UpdateRequestAction extends ActionSupport{

	@Autowired
	private IUpdateRequestService updateRequestService;
	private UpdateRequestDTO updateRequestDTO;
	
	public UpdateRequestDTO getUpdateRequestDTO() {
		return updateRequestDTO;
	}

	public void setUpdateRequestDTO(UpdateRequestDTO updateRequestDTO) {
		this.updateRequestDTO = updateRequestDTO;
	}

	/**
	 * 保存.
	 * @return String
	 */
	public String merge(){
		updateRequestService.merge(updateRequestDTO);
		return SUCCESS;
	}
	
	/**
	 * 查询
	 * @return String
	 */
	public String find(){
		updateRequestDTO=updateRequestService.find();
		return SUCCESS;
	}
	

	
}
