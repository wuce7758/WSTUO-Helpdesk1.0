package com.wstuo.itsm.updateRequest.dao;

import com.wstuo.itsm.updateRequest.entity.UpdateRequest;
import com.wstuo.common.dao.IEntityDAO;

/**
 * 升级时间DAO interface class
 * 
 * @author Will
 * 
 */
public interface IUpdateRequestDAO extends IEntityDAO<UpdateRequest> {

}
