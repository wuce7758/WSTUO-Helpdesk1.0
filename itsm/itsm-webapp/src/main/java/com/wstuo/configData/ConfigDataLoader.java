package com.wstuo.configData;

import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.xwork2.ActionSupport;
import com.wstuo.configData.service.IConfigDataLoaderService;
import com.wstuo.common.security.service.IFunctionService;

/**
 * 加载配置数据.
 * @author Van
 */
@SuppressWarnings("serial")
public class ConfigDataLoader extends ActionSupport{

	private String installType;
	private String languageVersion;
	private boolean insatllDomeData;
	@Autowired
	private IConfigDataLoaderService configDataLoaderService;
	@Autowired
	private IFunctionService functionService;//功能
	private String effect;
	
	public String getInstallType() {
		return installType;
	}
	public void setInstallType(String installType) {
		this.installType = installType;
	}
	public boolean isInsatllDomeData() {
		return insatllDomeData;
	}
	public void setInsatllDomeData(boolean insatllDomeData) {
		this.insatllDomeData = insatllDomeData;
	}
	public String getEffect() {
		return effect;
	}
	public void setEffect(String effect) {
		this.effect = effect;
	}
	public String getLanguageVersion() {
		return languageVersion;
	}
	public void setLanguageVersion(String languageVersion) {
		this.languageVersion = languageVersion;
	}

	/**
	 * 导入数据
	 */
	public String importConfigData(){
		if(functionService.findAllFunction().size()==0){
			effect=configDataLoaderService.importAllConfigData(languageVersion,insatllDomeData);
		}else{
			effect="configed";
		}
		return SUCCESS;
	}
	/**
	 * 准备配置文件
	 */
	public String preparingConfigure(){
		effect=configDataLoaderService.preparingConfigure();
		return SUCCESS;
	}
}
