package com.wstuo.configData.service;

import com.wstuo.common.security.dto.UserDTO;

/**
 * 基础配置数据加载Service接口
 * @author Ciel
 *
 */
public interface IConfigDataLoaderService {
	/**
	 * 加载全部数据
	 * @param language
	 * @return String
	 */
	String importAllConfigData(String language,Boolean installDemoData);
	/**
	 * 多租户模式：加载基础数据
	 * @param language
	 * @param userDTO
	 */
	String importAllConfigData(String language,UserDTO userDTO);
	/**
	 * 模块数据加载公共方法
	 * @param module
	 * @param isHaveProcess
	 * @param isHaveNotice
	 */
	void moduleInstallDataCommon(String Language,String module,Boolean isHaveProcess,Boolean isHaveNotice);
	/**
	 * 加载流程公共方法
	 * @param module
	 */
	void loadBpm(String module);
	/**
	 * 加载使用流程
	 * @param module
	 * @param processKey
	 * @param pid
	 */
	void processUserCommon(String module,String processKey,String pid);
	/**
	   * 准备配置文件
	   */
	 String preparingConfigure();
}
