package com.wstuo.itsm;

import org.hibernate.dialect.Oracle9iDialect;
import org.hibernate.sql.ANSIJoinFragment;
import org.hibernate.sql.JoinFragment;
/**
 * Oracle10g方言
 * @author Ciel
 *
 */
public class Oracle10gDialect extends Oracle9iDialect{
	
	 public Oracle10gDialect() {
         super();
     }
  
     public JoinFragment createOuterJoinFragment() {
         return new ANSIJoinFragment();
     }


}