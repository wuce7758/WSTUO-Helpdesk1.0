
Ext.ns('App');

App = {
    webforms: {},

    init: function() {
        Ext.QuickTips.init();
        this.propertyManager = new App.property.PropertyManager();
        var items=[
                   this.createNorth(),
                   this.createSouth(),
                   this.createWest(),
                   this.createEast(),
                   this.createCenter()
               ];
        if(Gef.TYPE && Gef.TYPE == 'edit' ){
        	items=[
                   this.createNorth(),
                   this.createSouth(),
                   this.createEast(),
                   this.createCenter()
               ];
        }
        var viewport = new Ext.Viewport({
            layout: 'border',
            items: items
        });
        //加载initEditor...
        //this.initEditor();

        setTimeout(function() {
            Ext.get('loading').remove();
            Ext.get('loading-mask').fadeOut({remove:true});
        }, 100);
    },

    initEditor: function(v_id) {
    
    	var xml = "";
    	//如果流程为null 则默认展示一个例子
	    if(!v_id || v_id == 'null' ){
	         xml = "<?xml version='1.0' encoding='UTF-8'?>"
	            + "<process xmlns='http://jbpm.org/4.4/jpdl' name='request_default' version='1'>"
	            + "<description>请求</description>"
	            + " <start g='113,110,48,48' name='开始'>"
	            + "  <transition g='-13,-5' to='提交请求' name='开始'/>"
	            + " </start>"
	            + " <end g='568,111,48,48' name='结束'/>"
	            + " <task g='389,112,90,50' name='请求处理'>"
	            + "  <transition to='结束' name='结束' g='-7,-3'/>"
	         + " </task>"
	         + " <state g='226,110,90,50' name='提交请求'>"
	         + "   <transition name='提交' g='-14,-7' to='请求处理'/>"
	         + "  </state>"
			            + "</process>";
	         this.initxml(xml);
	     }else{
	     	//DWR设置同步：
			//dwr.engine.setAsync(false);
			Ext.Ajax.request({
                method: 'post',
                url: Gef.GETXML_URL,
                success: function(response) {
                    try {
                    	xml=response.responseText.replace(/\\/g,'');
                    	App.initxml(xml.substr(1,xml.length-2));
                    } catch(e) {
                        alert(response.responseText.replace(/\\/g,'').substr(1,xml.length-2));
                    }
                },
                failure: function(response) {
                    Ext.Msg.alert('系统错误', response.responseText);
                },
                params: {
                	processDefinitionId: v_id
                }
            });
	     }
    },
    initxml:function(xml){
    	var editor = new Gef.jbs.ExtEditor();
        var input = new Gef.jbs.JBSEditorInput();
        //有数据
        if(xml){
        	input.readXml(xml);
        }
       

        var workbenchWindow = new Gef.ui.support.DefaultWorkbenchWindow();
        workbenchWindow.getActivePage().openEditor(editor, input);

        workbenchWindow.render();

        Gef.activeEditor = editor;
		//添加事件
		//alert("cheng:initSelectionListener");
        this.propertyManager.initSelectionListener(editor);
    },

    getProcessModel: function() {
        var viewer = Gef.activeEditor.getGraphicalViewer();
        var processEditPart = viewer.getContents();
        return processEditPart.model;
    },

    createNorth: function() {
        var p = null;
        if (Gef.MODE_DEMO === true) {
            p = new Ext.Panel({
                region: 'north'
            });
        } else {
            p = new Ext.Panel({
                region: 'north',
                //height: 60,
                html: '<a class="navbar-brand" id="pageh1" href="index.jsp"> <img alt="WSTUO" src="../images/logo2.png" class="hidden-xs"><span>流程设计器</span></a>'
				
            });
        }

        App.northPanel = p;
        return p;
    },

    createSouth: function() {
        var p = this.propertyManager.getBottom();
        return p;
    },

    createWest: function() {
        var p = new App.PalettePanel({
            collapsible: true
        });

        App.westPanel = p;
        return p;
    },

    createEast: function() {
        var p = this.propertyManager.getRight();
        return p;
    },

    createCenter: function() {
        var p = new App.CanvasPanel();

        App.centerPanel = p;
        return p;
    },

    getSelectionListener: function() {
        if (!this.selectionListener) {
            this.selectionListener = new Gef.jbs.ExtSelectionListener(null);
        }
        return this.selectionListener;
    }
};

/*
Gef.override(App.PalettePanel, {
    configItems: function() {
        this.html = 'sdfasfdfdsa';
    }
});
*/

Gef.PALETTE_TYPE = 'plain';
//Gef.PALETTE_TYPE = 'accordion';

Ext.onReady(App.init, App);

App.CanvasPanel = Ext.extend(Ext.Panel, {
    initComponent: function() {
        //this.on('bodyresize', function(p, w, h) {
        //    var b = p.body.getBox();
        //});
        this.region = 'center';
        this.autoScroll = true;
        if(Gef.TYPE && Gef.TYPE == 'edit' ){
        	this.tbar = new Ext.Toolbar([{
                text: '编辑流程任务属性'
            }]);
        }else{
	        this.tbar = new Ext.Toolbar([{
	            text: '新建',
	            iconCls: 'tb-new',
	            handler: function() {
	                Gef.activeEditor.reset();
	            }
	        }, {
	            text: '导入',
	            iconCls: 'tb-webform',
	            handler: function() {
	            	//获取现有的 xml 字符串
	                var xml = Gef.activeEditor.serial();
	                
	                if (!this.openWin) {
	                	
	                    this.openWin = new Ext.Window({
	                        title: 'xml',
	                        layout: 'fit',
	                        width: 500,
	                        height: 300,
	                        closeAction: 'hide',
	                        modal: true,
	                        items: [{
	                            id: '__gef_jbpm4_xml_import__',
	                            xtype: 'textarea'
	                        }],
	                        buttons: [{
	                        	xtype: "button",
	                            text: '导入',
	                            handler: function() {
	                            	//alert("cheng start 导入...");
	                                var xml = Ext.getDom('__gef_jbpm4_xml_import__').value;
	                                //alert("cheng xml---"+xml);
	                                Gef.activeEditor.resetAndOpen(xml);
	                                this.openWin.hide();
	                            },
	                            scope: this
	                        }, {
	                            text: '取消',
	                            handler: function() {
	                                this.openWin.hide();
	                            },
	                            scope: this
	                        }]
	                    });
	                    this.openWin.on('show', function() {
	                        Gef.activeEditor.disable();
	                    });
	                    this.openWin.on('hide', function() {
	                        Gef.activeEditor.enable();
	                    });
	                }
	                this.openWin.show(null, function() {
	                    Ext.getDom('__gef_jbpm4_xml_import__').value = xml;
	                });
	            }
	        }, {
	            text: '导出',
	            iconCls: 'tb-prop',
	            handler: function() {
	                var xml = Gef.activeEditor.serial();
	                if (!this.openWin) {
	                    this.openWin = new Ext.Window({
	                        title: 'xml',
	                        layout: 'fit',
	                        width: 500,
	                        height: 300,
	                        closeAction: 'hide',
	                        modal: true,
	                        items: [{
	                            id: '__gef_jbpm4_xml_export__',
	                            xtype: 'textarea'
	                        }],
	                        buttons: [{
	                            text: '关闭',
	                            handler: function() {
	                                this.openWin.hide();
	                            },
	                            scope: this
	                        }]
	                    });
	                    this.openWin.on('show', function() {
	                        Gef.activeEditor.disable();
	                    });
	                    this.openWin.on('hide', function() {
	                        Gef.activeEditor.enable();
	                    });
	                }
	                this.openWin.show(null, function() {
	                    Ext.getDom('__gef_jbpm4_xml_export__').value = xml;
	                });
	            }
	        }, {
	            text: '清空',
	            iconCls: 'tb-clear',
	            handler: function() {
	                Gef.activeEditor.clear();
	            }
	        }, {
	            text: '撤销',
	            iconCls: 'tb-undo',
	            handler: function() {
	                var viewer = Gef.activeEditor.getGraphicalViewer();
	                var browserListener = viewer.getBrowserListener();
	                var selectionManager = browserListener.getSelectionManager();
	                selectionManager.clearAll();
	                var commandStack = viewer.getEditDomain().getCommandStack();
	                commandStack.undo();
	            },
	            scope: this
	        }, {
	            text: '重做',
	            iconCls: 'tb-redo',
	            handler: function() {
	                var viewer = Gef.activeEditor.getGraphicalViewer();
	                var browserListener = viewer.getBrowserListener();
	                var selectionManager = browserListener.getSelectionManager();
	                selectionManager.clearAll();
	                var commandStack = viewer.getEditDomain().getCommandStack();
	                commandStack.redo();
	            },
	            scope: this
	        }, {
	            text: '布局',
	            iconCls: 'tb-activity',
	            handler: function() {
	
	                var viewer = Gef.activeEditor.getGraphicalViewer();
	                var browserListener = viewer.getBrowserListener();
	                var selectionManager = browserListener.getSelectionManager();
	                selectionManager.clearAll();
	
	                new Layout(Gef.activeEditor).doLayout();
	            },
	            scope: this
	        }, {
	            text: '删除',
	            iconCls: 'tb-delete',
	            handler: this.removeSelected,
	            scope: this
	        }, 
	        {
	            text: '发布部署',
	            iconCls: 'tb-save',
	            handler: function() {
	                var editor = Gef.activeEditor;
	
	                var isValid = new Validation(editor).validate();
	                if (!isValid) {
	                    return false;
	                }
					//后续需要添加判断该流程 是否存在。
	              	//如果存在则进行提示
	                var xml = editor.serial();
	                var model = editor.getGraphicalViewer().getContents().getModel();
	                var name = model.text;
	                
	                Ext.Msg.wait('正在发布流程'+name);
	                Ext.Ajax.request({
	                    method: 'post',
	                    url: Gef.DEPLOY_URL,
	                    success: function(response) {
	                        try {
		                       // if(response.responseText !="" && response.responseText.indexOf("成功") > 0){
	                        	 Ext.Msg.alert('信息', '操作成功',function(){
	                        	 //操作成功后关闭编辑窗口 并刷新 设计流程页面
	                        		 try{
	                        			 window.opener.queryData();
	                        		 }catch(e){}
	                        	 });
	                        	 
	                        	 setTimeout(function() {
                        			location.href="index.jsp?pid="+response.responseText.replace(/"/g,'')+"&type=edit";
                        		 }, 300);
	                        } catch(e) {
	                            Ext.Msg.alert('系统错误', response.responseText);
	                        }
	                    },
	                    failure: function(response) {
	                    	if(response.responseText.indexOf('already exists')>0){
	                    		Ext.Msg.alert('系统提示', "该流程已存在，请修改名称或版本！");
	                    	}else
	                    		Ext.Msg.alert('系统错误', response.responseText);
	                    },
	                    params: {
	                    	/*'nodeDTO.nodeTag': 'process',
	                        'nodeDTO.index': 0,//业务目录
	                        "nodeDTO.nodeAttribute['name']": model.procDefName,//流程名称
	*/                        /*procCode: model.procDefCode,//流程编码
	                        procVersion: model.procVerName,//版本号
	                        remark:model.descn,//备注*/
	                        rawXml: xml
	                    }
	                });
	            }
	        } 
	       ]);
        }
        App.CanvasPanel.superclass.initComponent.call(this);
    },

    afterRender: function() {
        App.CanvasPanel.superclass.afterRender.call(this);

        var width = 1500;
        var height = 1000;

        Ext.DomHelper.append(this.body, [{
            id: '__gef_jbs__',
            tag: 'div',
            style: 'width:' + (width + 10) + 'px;height:' + (height + 10) + 'px;',
            children: [{
                id: '__gef_jbs_center__',
                tag: 'div',
                style: 'width:' + width + 'px;height:' + height + 'px;float:left;'
            }, {
                id: '__gef_jbs_right__',
                tag: 'div',
                style: 'width:10px;height:' + height + 'px;float:left;background-color:#EEEEEE;cursor:pointer;'
            }, {
                id: '__gef_jbs_bottom__',
                tag: 'div',
                style: 'width:' + (width + 10) + 'px;height:10px;float:left;background-color:#EEEEEE;cursor:pointer;'
            }]
        }]);

        var rightEl = Ext.fly('__gef_jbs_right__');
       /* rightEl.on('mouseover', function(e) {
            var t = e.getTarget();
            t.style.backgroundColor = 'yellow';
            t.style.backgroundImage = 'url(images/arrow/arrow-right.png)';
        });*/
        rightEl.on('mouseout', function(e) {
            var t = e.getTarget();
            t.style.backgroundColor = '#EEEEEE';
            //alert("cheng mouseout");
            t.style.backgroundImage = '';
        });
        rightEl.on('click', function(e) {
            Ext.fly('__gef_jbs__').setWidth(Ext.fly('__gef_jbs__').getWidth() + 100);
            Ext.fly('__gef_jbs_center__').setWidth(Ext.fly('__gef_jbs_center__').getWidth() + 100);
            Ext.fly('__gef_jbs_bottom__').setWidth(Ext.fly('__gef_jbs_bottom__').getWidth() + 100);
			//alert("cheng click:");
            Gef.activeEditor.addWidth(100);
        });

        var bottomEl = Ext.fly('__gef_jbs_bottom__');
        bottomEl.on('mouseover', function(e) {
            var t = e.getTarget();
            t.style.backgroundColor = 'yellow';
            t.style.backgroundImage = 'url(images/arrow/arrow-bottom.png)';
        });
        bottomEl.on('mouseout', function(e) {
            var t = e.getTarget();
            t.style.backgroundColor = '#EEEEEE';
            t.style.backgroundImage = '';
        });
        rightEl.on('click', function(e) {
            Ext.fly('__gef_jbs__').setHeight(Ext.fly('__gef_jbs__').getHeight() + 100);
            Ext.fly('__gef_jbs_center__').setHeight(Ext.fly('__gef_jbs_center__').getHeight() + 100);
            Ext.fly('__gef_jbs_right__').setHeight(Ext.fly('__gef_jbs_right__').getHeight() + 100);

            Gef.activeEditor.addHeight(100);
        });

        this.body.on('contextmenu', this.onContextMenu, this);
    },

    onContextMenu: function(e) {
        if (!this.contextMenu) {
            this.contextMenu = new Ext.menu.Menu({
                items: [/*{
                    text: '详细配置',
                    iconCls: 'tb-prop',
                    handler: this.showWindow,
                    scope: this
                },*/ {
                    text: '删除',
                    iconCls: 'tb-remove',
                    handler: this.removeSelected,
                    scope: this
                }]
            });
        }
        e.preventDefault();
        this.contextMenu.showAt(e.getXY());
    },

    showWindow: function() {
        App.propertyManager.changePropertyStatus('max');
    },

    removeSelected: function() {
        var viewer = Gef.activeEditor.getGraphicalViewer();
        var browserListener = viewer.getBrowserListener();
        var selectionManager = browserListener.getSelectionManager();

        var edge = selectionManager.selectedConnection;
        var nodes = selectionManager.items;

        var request = {};

        if (edge != null) {
            request.role = {
                name: 'REMOVE_EDGE'
            };
            this.executeCommand(edge, request);
            selectionManager.removeSelectedConnection();
        } else if (nodes.length > 0) {
            request.role = {
                name: 'REMOVE_NODES',
                nodes: nodes
            };
            this.executeCommand(viewer.getContents(), request);
            selectionManager.clearAll();
        }
    },
	
    executeCommand: function(editPart, request) {
        var command = editPart.getCommand(request);
        if (command != null) {
            Gef.activeEditor.getGraphicalViewer().getEditDomain().getCommandStack().execute(command);
        }
    }
});

