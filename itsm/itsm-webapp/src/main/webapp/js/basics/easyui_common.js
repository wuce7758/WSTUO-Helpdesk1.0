function msgAlert(msg,type){
	$.noty.closeAll();
	noty({'text':msg,'layout':'center','type':type,'animateOpen': {'opacity': 'show'}});
	/*var content = msg;
	var confirm = i18n['Confirm'];
	switch(type){
		case "error":
			content="<div class=\"messager-icon messager-error\"></div>"+msg;
		break;
		case "info":
			content="<div class=\"messager-icon messager-info\"></div>"+msg;
		break;
		case "question":
			content="<div class=\"messager-icon messager-question\"></div>"+msg;
		break;
		case "warning":
			content="<div class=\"messager-icon messager-warning\"></div>"+msg;
		break;
	}
	
	$('#alertMSG').html(content);
	
	var buttons = {};
	 buttons[confirm] = function() {
		 $(this).dialog('close');
	 };  
	$('#alertMSG').dialog(
	        $.extend({}, jqueryDialog, 
                {
                    title:i18n['tip'],
                    width:320,
                    Open:true,
                    show: 'blind',
                    hide: 'explode',
                    modal: true,
                    close:function(){
                    },
                    buttons: buttons
                }
	        )
	    );
	
	setTimeout(function(){
        // 需要先判断按钮的文字是 确认才能获取焦点
        $('.ui-dialog-buttonpane .ui-button-text').each(function(){
            if (confirm == $(this).text()) {
                $(this).parent().focus();
            }
        });
    }, 300);*/
	
}
function msgConfirm(title,content,event,cancel){
	if(title==''){
		title = i18n['tip'];
	}
	$("#modal_title").text(title);
	$('#modal-body').html(content);
	$('#alert_modal').modal('show');
	$('#modal_confirm').unbind('click').click(function(){
		event();
	});
	$('#modal_cancel').unbind('click').click(function(){
		 if (typeof cancel === 'function') 
			 cancel();
	});
    $('#modal_confirm').focus();
        
/*    $(document).bind('keypress',function(event){  
        if(event.keyCode == '37') {//左
        	$('#modal_cancel').focus();
        }
        if(event.keyCode == '39') {
        	$('#modal_confirm').focus();
        }
    });*/
	
}

function msgShow(msg,type){
	switch(type){
		case "error":
		break;
		case "info":
			type="success";
		break;
		case "show":
			type="success";
		break;
		case "question":
			type="warning";
		break;
		case "warning":
		break;
	}
	$.noty.closeAll();
	noty({'text':msg,'layout':'center','type':type,'animateOpen': {'opacity': 'show'}});
	
}
function msgAssginShow(msg,next,top){
	$.noty.closeAll();
	var n = noty({
	      text: msg,
	      type: 'info',
	      dismissQueue: {'opacity': 'show'},
	      layout: 'bottomRight',
	      timeout: false,
	      buttons: [
			{addClass: 'btn btn-info btn-setting', text: '<i class="glyphicon glyphicon-backward" title="上一条"></i>', click: function($noty) {
			    top($noty.data('noty_options').id);
			  }
			},
	        {addClass: 'btn btn-info btn-setting', text: '<i class="glyphicon glyphicon-forward" title="下一条"></i>', click: function($noty) {
	        	next($noty.data('noty_options').id);
	          }
	        },
	        {addClass: 'btn btn-danger', text: '<i class="glyphicon glyphicon-remove" title="关闭"></i>', click: function($noty) {
	        	$noty.close();
	            stopBlinkNewMsg();
	          }
	        }
	      ]
	    });
	
}

function show(title,msg,type,time){
	
}

function showAndHidePanel(showId,hiddenId){
	 $(hiddenId).hide();
	 $(showId).show();
}

function loadTreeView(treeId,queryUrl,selectNodeMethod){
	$(treeId).jstree({
		json_data: {
			ajax: {
				url :queryUrl,
				data:function(n){
			    	  return {'parentOrgNo':n.attr ? n.attr("orgNo").replace("node_",""):0};//types is in action
				},
				cache: false}
		},
		plugins : ["themes", "json_data", "ui", "crrm", "dnd"]
	})
	.bind('select_node.jstree',selectNodeMethod);
}

//打开进程窗口
function startProcess(){
	$('#processWindow').dialog($.extend({},loading, {open:function(){
		$('#processWindow').parent().css('height',70);
		$('#processWindow').parent().css('width',250);
		$('.ui-dialog #processWindow').parent().find('.ui-dialog-titlebar').remove();
	}}));
}

//关闭
function endProcess(){
	if( !$('#processWindow').is(':hidden'))
		$('#processWindow').dialog('close');
}

//打开进程窗口
function startLoading(){
	$('#loading_window').dialog($.extend({},loading, {open:function(){
		$('#loading_window').parent().css('height',50);
		$('.ui-dialog #loading_window').parent().find('.ui-dialog-titlebar').remove();
	}}));
	
}

//关闭
function endLoading(){
	if( !$('#loading_window').is(':hidden'))
		$('#loading_window').dialog('close');
}

/**
 * 调用jquery.bgiframe处理IE6下select控件显示在最前面
 * 
$.extend($.fn.window.defaults,{
	onBeforeOpen:function(){
		$(".panel").parent().bgiframe(); 
		$(".panel-header").parent().bgiframe();
	}
});
*/
//设置容器高度
function setPanelHeight(panelId){
	
	var height=document.getElementById("regCenter").style.height;
	height=height.substring(0,height.length-2);
	document.getElementById(panelId).style.height=height-35+"px";
}

//刷新JSTREE
function refreshTree(treeId){
	
	$(treeId).bind('loaded.jstree', function(e,data){data.inst.open_all(-1);
		}).jstree('refresh',-1); 
}

/**
 * 获取可操作项HTML.
 * @param sec_edit
 * @param sec_delete
 * @returns HTMLStr
 */
function actionFormat(sec_edit,sec_delete){
	
	var HTMLStr='';
	if(sec_edit=='1'){
		HTMLStr+='<a title="'+i18n['common_edit']+'" href="javascript:[edit]"><i class="glyphicon glyphicon-edit"></i></a>';
	}
	if(sec_delete=='1'){
		HTMLStr+='<a title="'+i18n['label_tree_delete']+'" href="javascript:[delete]" style="margin-left:8px"><i class="glyphicon glyphicon-trash"></i></a>';
	}

	
	return HTMLStr;
}
/**
 * 日期格式:2011-09-19
 * @param str
 * @returns
 */
function strDateTime(str)
{
var r = str.match(/^(\d{1,4})(-|\/)(\d{1,2})\2(\d{1,2})$/);
if(r==null)return false;
var d= new Date(r[1], r[3]-1, r[4]);
return (d.getFullYear()==r[1]&&(d.getMonth()+1)==r[3]&&d.getDate()==r[4]);
}
/**
 * 日期格式:2011-09-19 15:14:11
 * @param str
 * @returns
 */
function strDateTimeLeng(str)
{
var reg = /^(\d{1,4})(-|\/)(\d{1,2})\2(\d{1,2}) (\d{1,2}):(\d{1,2}):(\d{1,2})$/;
var r = str.match(reg);
if(r==null)return false;
var d= new Date(r[1], r[3]-1,r[4],r[5],r[6],r[7]);
return (d.getFullYear()==r[1]&&(d.getMonth()+1)==r[3]&&d.getDate()==r[4]&&d.getHours()==r[5]&&d.getMinutes()==r[6]&&d.getSeconds()==r[7]);
}


function DateComparison(date1,date2){
	return Date.parse(date1.replace(/-/g,"/"))<=Date.parse(date2.replace(/-/g,"/"));
}
function NumberComparison(num1,num2){
   return num1<=num2;
} 
