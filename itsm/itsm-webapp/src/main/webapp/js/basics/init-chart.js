
/*	$.post("report!requestCatagoryReport.action",function(data){
		var attr=[];
		$.each(data,function(i,o){
			if(o[0]==null){
				o[0]="未分类";
			}
			attr.push({label:o[0], data:o[1]});
		});
		//console.log(attr);
		piechart("requestCatagorypiechart",attr);
	});*/
/*	$.post("report!requestStatusReport.action",function(data){
		var attr=[];
		$.each(data,function(i,o){
			if(o[0]==null){
				o[0]="未分类";
			}
			attr.push({label:o[0], data:o[1]});
		});
		//console.log(attr);
		piechart("requestStatusinfo",attr);
	});*/
//pie chart

function piechart(divid,attr){
	 if ($("#"+divid).length) {
         $.plot($("#"+divid), attr,
             {
                 series: {
                     pie: {
                         show: true,
                        
                     },
	                   label: {
	                	   formatter: function(label, slice) {
	   						return "<div style='font-size:16px;text-align:center;padding:2px;color:" + slice.color + ";'>" + label + "<br/>" + Math.round(slice.percent) + "%</div>";
	   					    }
	                   }
                 },
                 grid: {
                     hoverable: true,
                     clickable: true
                 },
                 legend: {
                     show: false
                 }
             });
         function pieHover(event, pos, obj) {
             if (!obj)
                 return;
             percent = parseFloat(obj.series.percent).toFixed(2);
             $("#hover").html('<span style="font-size=20px;font-weight: bold; color: ' + obj.series.color + '">' + obj.series.label + ' (' + percent + '%)</span>');
         }

         $("#"+divid).bind("plothover", pieHover);
     }
		
}


// we use an inline data source in the example, usually data would
// be fetched from a server
var data = [], totalPoints = 300;

function getRandomData() {
    if (data.length > 0)
        data = data.slice(1);

    // do a random walk
    while (data.length < totalPoints) {
        var prev = data.length > 0 ? data[data.length - 1] : 50;
        var y = prev + Math.random() * 10 - 5;
        if (y < 0)
            y = 0;
        if (y > 100)
            y = 100;
        data.push(y);
    }

    // zip the generated y values with the x values
    var res = [];
    for (var i = 0; i < data.length; ++i)
        res.push([i, data[i]])
    return res;
}

// setup control widget
var updateInterval = 30;
$("#updateInterval").val(updateInterval).change(function () {
    var v = $(this).val();
    if (v && !isNaN(+v)) {
        updateInterval = +v;
        if (updateInterval < 1)
            updateInterval = 1;
        if (updateInterval > 2000)
            updateInterval = 2000;
        $(this).val("" + updateInterval);
    }
});

//realtime chart
if ($("#realtimechart").length) {
    var options = {
        series: { shadowSize: 1 }, // drawing is faster without shadows
        yaxis: { min: 0, max: 100 },
        xaxis: { show: false }
    };
    var plot = $.plot($("#realtimechart"), [ getRandomData() ], options);

    function update() {
        plot.setData([ getRandomData() ]);
        // since the axes don't change, we don't need to call plot.setupGrid()
        plot.draw();

        setTimeout(update, updateInterval);
    }

    update();
}