
	 function getUploader(fileId,valueId,fileList,queueId,uploadCallbackMethod,dropZoneEnabled){
		// checkFlashPlayer();//检测Flash插件
		 if(dropZoneEnabled==undefined) dropZoneEnabled=true;
		 var fileExt='*.doc;*.docx;*.xls;*.rar;*.zip;*.txt;*.pdf;*.jpg;*.png;*.gif;*.bmp;*.swf;*.xlsx;*.gif;*.jpg;*.png;*.bmp;*.ppt;*.vsd;*.pptx';
		 $(fileId).fileinput({
		        uploadUrl: 'uploadFile!uploadAttr.action', // you must set a valid URL here else you will get an error
		        allowedFileExtensions :["csv","doc","docx","xls","rar","zip","txt","pdf","png","bmp","xlsx","gif","ppt","vsd","pptx","gif","jpg","rar","zip","swf"],//允许后缀名
		        overwriteInitial: false,
		        maxFileSize: 10000,
		        showPreview:true,
		        dropZoneEnabled:dropZoneEnabled,
		        maxFilesNum: 10,
		        language : 'zh',
		        slugCallback: function(filename) {
		            return filename.replace('(', '_').replace(']', '_');
		        }
			}).on('filecleared', function(event, data, previewId, index) {
				$(valueId).val('');
			}).on('fileerror', function(event, data, msg) {
	            msgAlert(msg);
			}).on("fileuploaded", function (event, data, previewId, index) {
				//console.log(data);console.log(event);console.log(index);
               var response=data.response;
               var fileSize=response.fileSize;
               var fileName=response.msg;
               var file = data.files[index];
               if(fileSize!=null && fileSize>0){
            	   //login1.png==20160617/1466157305488.png==23656
					$(valueId).val($(valueId).val()+file.name+"=="+fileName+"=="+fileSize+"-s-");
               }	
               if(dropZoneEnabled)
            	   $(".file-preview-frame .file-footer-buttons").html(
            		   '<button type="button" class="kv-file-remove btn btn-xs btn-default" title="删除文件" onclick="deleteFile(\''+fileName+'\',this,\''+valueId+'\')"><i class="glyphicon glyphicon-trash text-danger"></i></button>');
               //uploadSuccess(file.type,file.name,fileName,valueId,fileList,response.err);
               if (uploadCallbackMethod) {
					uploadCallbackMethod();
               }
	        });
		  /*
		  $(fileId).uploadify({
				'uploader':'../scripts/jquery/uploadify/uploadify.swf', 
				'script':'uploadFile!uploadAttr.action',   
				'cancelImg':'../images/icons/remove.gif',  
				'fileExt':fileExt,
				'fileDesc':'doc;docx;xls;rar;zip;txt;pdf;jpg;png;gif;bmp;swf;xlsx;vsd;',
				'removeCompleted':true,
				'queueID':queueId,   
				'width':113,
				'height':24,
	            'auto':false,
	            'multi':true,
 				'buttonImg':'../skin/default/images/'+i18n['image_upload_icon'],
 				'onSelect':function(e, queueId, fileObj){
 					if(fileExt.indexOf(fileObj.type.toLowerCase())==-1){
 						msgAlert(i18n['file_disAllow_upload'],'info');
 						return false;
 					}
 					
 					if(fileObj.size==0){
 						msgAlert(fileObj.name+" "+i18n['uploadEmpty'],'info');
 						return false;
 					}
 					if(fileObj.size>102400000){
 						msgAlert(i18n['msg_attachment_maxsize'],'info');
 						return false;
 					}
 					var valueStr=$(valueId).val();
 					if(valueStr.length>0){
 						if(valueStr.indexOf(fileObj.name)===0 || valueStr.indexOf("-s-"+fileObj.name)>0){
 							msgAlert(i18n.upload_Name_already_exists,'info');
 							setTimeout(function(){
 								$(fileId).uploadifyCancel(queueId);
 							},300);
 							return false;
						}
 					}
 						
 				},
 				'onAllComplete':function(){
 					if (uploadCallbackMethod) {
						uploadCallbackMethod();
					}
 					
 				},
				'onComplete':function(event, queueID, fileObj, response, data){
					
					response=eval('('+response+')');
					var fileSize=response.fileSize;
					var fileName=response.msg;
					if(fileExt.indexOf(fileObj.type.toLowerCase())!=-1 && fileSize!=null && fileSize>0){
						$(valueId).val($(valueId).val()+fileObj.name+"=="+fileName+"=="+response.fileSize+"-s-");
					}	
					
					var fileFix=fileObj.type.toLowerCase();
					
					uploadSuccess(fileFix,fileName,fileObj.name,valueId,fileList,response.err);
				}
			});		*/	
	 }
	 /**
	  * 上传成功加载附件名称 
	  * @param fileFix
	  * @param fileName20150429/1430276507627.png
	  * @param name logo1-2.png
	  * @param fileList
	  * @param valueId
	  * @author will
	  */
	 function uploadSuccess(fileFix,fileName,name,valueId,fileList,err){
			var attachIcon="../images/attachicons/unknown.gif";
			fileFix=fileFix.toLowerCase();
			if(fileFix==".rar"){
				attachIcon="../images/attachicons/rar.gif";
			}
			if(fileFix==".zip"){
				attachIcon="../images/attachicons/zip.gif";
			}
			if(fileFix==".gif"||fileFix==".jpg"||fileFix==".bmp"||fileFix.indexOf("image")>-1){
				attachIcon="../images/attachicons/image.gif";
			}
			if(fileFix==".doc"||fileFix==".docx"||fileFix==".xls"||fileFix==".ppt"||fileFix==".pptx"||fileFix==".xlsx"){
				attachIcon="../images/attachicons/msoffice.gif";
			}
			if(fileFix==".pdf"){
				attachIcon="../images/attachicons/pdf.gif";
			}
			if(fileFix==".swf"){
				attachIcon="../images/attachicons/flash.gif";
			}
			if(fileFix==".txt" ||fileFix.indexOf("text")>-1){
				attachIcon="../images/attachicons/text.gif";
			}
			var htmlCode="<div id='"+fileName.substring(fileName.lastIndexOf("/")+1,fileName.lastIndexOf("."))+
				"' ><img width='14px' height='14px' src='"+attachIcon+"' />&nbsp;&nbsp;"+name+
				"&nbsp;&nbsp;<a href=javascript:deleteFile('"+fileName+"','"+fileList+"','"+valueId+"')>"+i18n['deletes']+"</a><br/>";
			
			if(err==""){
				$(fileList).append(htmlCode);
			}
	 }
	 /**
	  * 上传拔插har包文件
	  * @param fileId
	  * @param valueId
	  * @param fileList
	  * @param queueId
	  * @param uploadCallbackMethod
	  */
	 function getUploaderHar(fileId,fileList,queueId,uploadCallbackMethod){
		// checkFlashPlayer();//检测Flash插件
		 
		 var fileExt='*.har;';
		 
		 $(fileId).uploadify({
				'uploader':'../scripts/jquery/uploadify/uploadify.swf',   
				'script':'module!uploadHarFile.action',
				'cancelImg':'../images/icons/remove.gif',
				'fileExt':fileExt,
				'fileDesc':'har;',
				'removeCompleted':true,
				'queueID':queueId,
				'width':158,
				'height':24,
	            'auto':false,
	            'multi':true,
 				'buttonImg':'../images/icons/'+i18n['image_upload_icon'],
 				'onSelect':function(e, queueId, fileObj){
 					if(fileExt.indexOf(fileObj.type.toLowerCase())==-1){
 						msgAlert(i18n['file_disAllow_upload'],'info');
 						return false;
 					}
 					if(fileObj.size==0){
 						msgAlert(fileObj.name+" "+i18n['uploadEmpty'],'info');
 						return false;
 					}
 				},
 				'onAllComplete':function(){
 					if (uploadCallbackMethod) {
						uploadCallbackMethod();
					}
 					
 				},
 				'onError':function(event,queueId,fileObj,errorObj){
 						if(errorObj.info=="500"){
 							msgAlert(i18n.ERRORINSTALL,"error");
 						}
 				},
				'onComplete':function(event, queueID, fileObj, response, data){
					response=eval('('+response+')');
					var fileSize=response.fileSize;
					var fileName=response.msg;
					fileName=fileName.substring(fileName.lastIndexOf("/")+1,fileName.lastIndexOf("."));
					var htmlCode="<div>&nbsp;&nbsp;"+fileObj.name+"&nbsp;&nbsp;安装成功<br/>";
						
					if(response.err==""){
						$(fileList).append(htmlCode);
						$('#moduleManageGrid').trigger('reloadGrid');
					}else if(response.err=="moduleHave"){
						msgAlert(i18n.moduleHave,"error");
					}else{
						msgAlert(response.err,"info");
					}
				}
			});			
	 }
	 
	 /**
	  * 删除上传后的文件
	  */
	 function deleteFile(fileName,event,valueId){
		 
		 
		 msgConfirm(i18n['msg_msg'],'<br/>'+i18n['msg_confirm_deleteAttachment'],function(){
			 fileName=fileName.substring(fileName.lastIndexOf("/")+1);
				//var divId=fileName.substring(0,fileName.lastIndexOf("."));
				$(event).closest('.file-preview-frame').remove();     //删除匹配元素
				var _url = "uploadFile!deleteFile.action?deleteFileName="+fileName;
				$.post(_url,function(){
					//删除值
					var fileValue=$(valueId).val();
					var newFileValue="";
					var valArr=fileValue.split("-s-");
					for(var i=0;i<valArr.length;i++){
						if(valArr[i].indexOf(fileName)==-1){
							newFileValue=newFileValue+valArr[i]+"-s-";
						}
					}
					newFileValue=newFileValue.replace("-s--s-","-s-");
					$(valueId).val(newFileValue);
				});
		 });
	 }
	 
	 
	 /**
	  * 检查Flash插件是否安装
	  */
	 function checkFlashPlayer(){
		 
		 
		 var hasFlash=0;//是否安装了flash
		 var flashVersion=0; //flash版本
	
		 if($.browser.msie){//IE
			 
				var swf = new ActiveXObject('ShockwaveFlash.ShockwaveFlash');
				if (swf) {
					hasFlash = 1;
					VSwf = swf.GetVariable("$version");
					flashVersion = parseInt(VSwf.split(" ")[1].split(",")[0]);
				}

		}else{//其它浏览器
			
			if (navigator.plugins && navigator.plugins.length > 0) {
				var swf = navigator.plugins["Shockwave Flash"];
				if (swf) {
					hasFlash = 1;
					var words = swf.description.split(" ");
					for ( var i = 0; i < words.length; ++i) {
						if (isNaN(parseInt(words[i])))
							continue;
						flashVersion = parseInt(words[i]);
					}
				}
			}
		}
		 
		if(hasFlash==0){
			$('.WSTUO-dialog').dialog('close');
			msgAlert("<a href='javascript:void(0)' onclick='window.open(\"http://get.adobe.com/cn/flashplayer/\")'>"+i18n['msg_flash_player_not_installed']+"</a>",'warning');
		}
	 }