$package("itsm.cim") 
$import("common.config.dictionary.dataDictionaryUtil");
$import("common.security.userUtil");
$import('itsm.itsop.selectCompany');
$import('itsm.cim.ciCategoryTree');
$import('common.config.attachment.chooseAttachment');
$import('common.knowledge.knowledgeTree');
$import('common.security.includes.includes');
$import('common.config.includes.includes');
$import('common.config.category.serviceCatalog');
/**  
 * @fileOverview 配置项添加
 * @author QXY 
 * @version 1.0  
 */  
 /**  
 * @author QXY  
 * @constructor configureItemAdd
 * @description 配置项添加
 * @date 2010-11-17
 * @since version 1.0 
 */
itsm.cim.configureItemAdd=function(){
	
	return{
		/**
		 * 过滤字符
		 */
		stringItem:function(str){
			 var pattern=new RegExp("[`~%!@#^=?~！@#￥……&——？*]");
			 //[]内输入你要过滤的字符，这里是我的 
			 var rs=pattern.test(str);
			
			 return rs;
		},
		/**
		 * @description 保存到数据库
		 */
		saveConfigureItem:function(){
			if($('#ciBasicInfoDiv form').form('validate')){
				if($('#ci_add_eavAttributet_form').form('validate')){
					var ci_add_cino=trim($('#ci_add_cino').val());
					var boo=itsm.cim.configureItemAdd.stringItem(ci_add_cino);
					if(boo==true){
						$('#ci_add_cino').focus();
						//$('#ci_add_cino').css("color","red");
						msgShow(i18n['label_string'],'show');
						return false;
					}
					itsm.cim.ciCategoryTree.getFormAttributesValue("#ci_add_eavAttributet_form");
					var _params = $('#configureItemAdd_panel form').serialize();
					startProcess();
					$.post('ci!existCIByCiNo.action','ciQueryDTO.cino='+ci_add_cino,function(data){
						if(data){
							msgAlert(i18n['err_ciNoExist'],'info');
							endProcess();
						}else{
							$.post('ci!cItemSave.action', _params, function(){
								endProcess();
								basics.tab.tabUtils.closeTab(i18n['ci_addConfigureItem']);	
								basics.tab.tabUtils.selectTab(i18n['ci_configureItemAdmin'],function(){
									$('#configureGrid').trigger('reloadGrid');
								});
								msgShow(i18n['saveSuccess'],'show');
								
								if ($('#itsmMainTab').tabs('exists',i18n.ci_configureItemAdmin)){
									itsm.cim.leftMenu.companyStat();
									basics.showChart.cimShowChart();
								}
							})
						}
					});
				}else{
					msgAlert(i18n.eavAttributet_notNull,'info');
					$('#addCim_tabs').tabs('select', ci_add_eavAttributet);
				}
			}
	
		},
		
		/**
		 * @description 配置项分类选择窗口打开
		 **/
		focusInit:function(){
			configureItemTree();
			windows('configureItemAddCategory',{width:400,height:400});
		},

		
		/**
		 * @description 选择请求者.
		 */
		selectCICreator:function(userName){
			common.security.userUtil.selectUser(userName,'','','fullName');

		},
		/**
		 * 返回列表
		 */
		returnConfigureItemList:function(){
			basics.tab.tabUtils.addTab(i18n['ci_configureItemAdmin'],'../pages/itsm/cim/configureItem.jsp');
		},
		/**
		 * 加载模板
		 */
		selectConfigureItemTemplate:function(){
			$('<option value="">-- '+i18n["common_pleaseChoose"]+' --</option>').appendTo("#configureItemTemplate");
			$.post("template!findByTemplateType.action",{"templateDTO.templateType":"configureItem"},function(data){
				if(data!=null && data.length>0){
					for(var i=0;i<data.length;i++){
						$('<option value="'+data[i].templateId+'">'+data[i].templateName+'</option>').appendTo("#configureItemTemplate");
					}
				}
			});
		},
		/**
		 * 选择模板
		 */
		getTemplateValue:function(templateId){
		    $('#addConfigureItem_uploadedAttachments').html('');
		    $('#addConfigureItem_attachments').val('');
			if(templateId!=null && templateId!=""){
				$('#saveconfigureItemTemplateBtn').hide();
				$('#editconfigureItemTemplateBtn').show();
				$.post("template!findByTemplateId.action",{"templateDTO.templateId":templateId},function(data){
					$('#configureItemTemplate').attr('value',data.templateId);
					$('#configureItemTemplateNameInput').val(data.templateName);
					$('#configureItemTemplateId').val(data.templateId);
					$('#ci_add_companyNo').val(data.dto.companyNo);
					$('#ci_add_companyName').val(data.dto.companyName);
					$('#ci_add_categoryNo').val(data.dto.categoryNo);
					$('#ci_add_categoryName').val(data.dto.categoryName);
					$.post("ciCategory!findCICategoryById.action",{"categoryNo":data.dto.categoryNo},function(res){
						itsm.cim.ciCategoryTree.showAttributetTovalue(res.categoryType,"ci_add_eavAttributet","ciDto",templateId);
						setTimeout(function(){
							if(data.dto.attrVals!=null){
								for(var key in data.dto.attrVals){
									$("#"+key+"ci_add_eavAttributet").val(data.dto.attrVals[key]);
								}
							}else{
								$('#ci_add_eavAttributet').html("");
							}
						},500);
					});
					$('#ci_add_cino').val(data.dto.cino);
					$('#ciname').val(data.dto.ciname);
					$('#model').val(data.dto.model);
					$('#serialNumber').val(data.dto.serialNumber);
					$('#barcode').val(data.dto.barcode);
					$('#ci_statusAdd').val(data.dto.statusId);
					$('#ci_brandNameAdd').val(data.dto.brandId);
					$('#ci_providerAdd').val(data.dto.providerId);
					$('#ci_locid').val(data.dto.locId);
					var locId=data.dto.locId;
					if(locId!=null && locId!=""){
						$.post("event!findByIdCategorys.action",{"categoryId":locId},function(data){
							$('#ci_loc').val(data.eventName);
						});
					}
					$('#configureItem_add_buyDate').val(timeFormatter(data.dto.buyDate));
					$('#configureItem_add_arrivalDate').val(timeFormatter(data.dto.arrivalDate));
					$('#warningDate').val(timeFormatter(data.dto.warningDate));
					$('#poNo').val(data.dto.poNo);
					$('#assetsOriginalValue').val(data.dto.assetsOriginalValue);
					$('#configureItem_add_department').val(data.dto.department);
					$('#workNumber').val(data.dto.workNumber);
					$('#project').val(data.dto.project);
					$('#sourceUnits').val(data.dto.sourceUnits);
					$('#lifeCycle').val(data.dto.lifeCycle);
					$('#warranty').val(data.dto.warranty);
					$('#wasteTime').val(timeFormatter(data.dto.wasteTime));
					$('#borrowedTime').val(timeFormatter(data.dto.borrowedTime));
					$('#expectedRecoverTime').val(timeFormatter(data.dto.expectedRecoverTime));
					$('#recoverTime').val(timeFormatter(data.dto.recoverTime));
					$('#usePermissions').val(data.dto.usePermissions);
					$('#ci_add_originalUser').val(data.dto.originalUser);
					$('#ci_add_useName').val(data.dto.userName);
					$('#ci_add_owner').val(data.dto.owner);
					$('#CDI').val(data.dto.CDI);
					var serviceNos = data.dto.serviceNos;
					if(serviceNos!=null){
						var html="";
						for(var key in serviceNos){
							html+="<tr id=add_ci_"+key+"><td>"+serviceNos[key]+"</td><td><a onclick=common.knowledge.knowledgeTree.serviceRm("+key+")>"+i18n['deletes']+"</a><input type=hidden name=ciDto.serviceDirectoryNos value="+key+"></td></tr>";
					    }
						$("#add_ci_serviceDirectory_tbody").html(html);
					}
					$('#addci_softSetingParam').val(data.dto.softSetingParam);
					$('#addci_softConfigureAm').val(data.dto.softConfigureAm);
					$('#addci_softRemark1').val(data.dto.softRemark1);
					$('#addci_softRemark2').val(data.dto.softRemark2);
					$('#addci_softRemark3').val(data.dto.softRemark3);
					$('#depreciationIsZeroYears').val(data.dto.depreciationIsZeroYears);
					$('#ci_systemPlatform').val(data.dto.systemPlatformId);
					//附件
					$('#addConfigureItem_attachments').val(data.dto.attachmentStr);
					var attrArr=data.dto.attachmentStr.split("-s-");
		    		for(var i=0;i<attrArr.length;i++){
		    			var url=attrArr[i].replace("\\", "/");
						if(url!=""){
		        			var attrArrs=url.split("==");
		        			var name=attrArrs[0];
		        			uploadSuccess(name.substr(name.lastIndexOf(".")),attrArrs[1],name,'#addConfigureItem_attachments','#addConfigureItem_uploadedAttachments',"");
						}
		    		}
				});
			}else{
				$('#saveconfigureItemTemplateBtn').show();
				$('#ci_add_categoryNo').val("");
				$('#ci_add_categoryName').val("");
				$('#ci_add_cino').val("");
				$('#ciname').val("");
				$('#model').val("");
				$('#serialNumber').val("");
				$('#barcode').val("");
				$('#ci_statusAdd').val("");
				$('#ci_brandNameAdd').val("");
				$('#ci_providerAdd').val("");
				$('#ci_locid').val("");
				$('#ci_loc').val("");
				$('#configureItem_add_buyDate').val("");
				$('#configureItem_add_arrivalDate').val("");
				$('#warningDate').val("");
				$('#poNo').val("");
				
				$('#assetsOriginalValue').val("");
				$('#configureItem_add_department').val("");
				$('#workNumber').val("");
				$('#project').val("");
				$('#sourceUnits').val("");
				$('#lifeCycle').val("");
				$('#warranty').val("");
				$('#wasteTime').val("");
				$('#borrowedTime').val("");
				$('#expectedRecoverTime').val("");
				$('#recoverTime').val("");
				$('#usePermissions').val("");
				$('#ci_add_originalUser').val("");
				$('#ci_add_useName').val("");
				$('#ci_add_owner').val("");
				$('#CDI').val("");
				$('#ci_add_eavAttributet table tbody').html("<tr><td colspan=\"2\" style=\"color: red\"><b>"+i18n['label_notExtendedInfo_Ci']+"</b></td></tr>");
				$('#add_ci_serviceDirectory_tbody').html("");
				$('#editconfigureItemTemplateBtn').hide();
				$('#addci_softSetingParam').val("");
				$('#addci_softConfigureAm').val("");
				$('#addci_softRemark1').val("");
				$('#addci_softRemark2').val("");
				$('#addci_softRemark3').val("");
				$('#depreciationIsZeroYears').val("");
				$('#ci_systemPlatform').val("");
			}
		},
		/**
		 * 打开保存模板面板
		 */
		configureItemTemplateNameWin:function(){
			$('#configureItemTemplateNameInput').val("");
			windows('configureItemTemplateName',{close:function(){
				$("#ci_add_categoryName").blur();
			}});
		},
		/**
		 * 保存模板
		 */
		saveConfigureItemTemplate:function(type){
			if(type=="add"){
				$('#configureItemTemplateId').val("");
			}
			if( $('#configureItemTemplateName form').form('validate')&&$('#ciBasicInfoDiv form').form('validate')){
				itsm.cim.ciCategoryTree.getFormAttributesValue("#ci_add_eavAttributet_form");
				var frm = $('#configureItemAdd_panel form,#configureItemTemplateName form').serialize();
				var url = 'ci!saveConfigureItemTemplate.action';
				//调用
				startProcess();
				$.post(url,frm, function(res){
						endProcess();
						$('#configureItemTemplateName').dialog('close');
						if(type=="add"){
							$('#configureItemTemplate').html("");
							itsm.cim.configureItemAdd.selectConfigureItemTemplate();
							$('#templateGrid').trigger('reloadGrid');
							msgShow(i18n['saveSuccess'],'show');
						}else{
							$('#templateGrid').trigger('reloadGrid');
							msgShow(i18n['editSuccess'],'show');
						}
				});
			}
		},
		/**
		 * 初始化数据
		 */
		init:function(){
			//绑定日期控件
//			DateBox(['configureItem_add_buyDate','configureItem_add_arrivalDate','warningDate','wasteTime','borrowedTime','expectedRecoverTime','recoverTime','operatingSystem_installDate_add','operatingSystem_lastBootUpTime_add','bios_releaseDate_add']);
			
			if(pageType=="template"){
				$('#itemAddSave').hide();
			}
			itsm.cim.includes.includes.loadSelectCIIncludesFile();
			common.security.includes.includes.loadSelectCustomerIncludesFile();
			common.config.includes.includes.loadCategoryIncludesFile();
			common.config.includes.includes.loadCustomFilterIncludesFile();
			$("#configureItemAdd_loading").hide();
			$("#configureItemAdd_panel").show();
			$('#ci_add_useName_select').click(function(){common.security.userUtil.selectUser('#ci_add_useName','#ci_add_useNameId','','fullName',$('#ci_add_companyNo').val())});//选择使用者
			$('#ci_add_owner_select').click(function(){common.security.userUtil.selectUser('#ci_add_owner','#ci_add_ownerId','','fullName',$('#ci_add_companyNo').val())});//选择负责人
			$('#ci_add_originalUser_select').click(function(){common.security.userUtil.selectUser('#ci_add_originalUser','#ci_add_originalUserId','','fullName',$('#ci_add_companyNo').val())});//选择原使用者
			itsm.cim.configureItemAdd.selectConfigureItemTemplate();
			$('#add_configureItem_backList').click(function(){
				if(pageType=='template'){
					basics.tab.tabUtils.refreshTab(i18n['title_dashboard_template'],'../pages/common/config/template/templateMain.jsp');
				}else{
					showLeftMenu('../pages/itsm/cim/leftMenu.jsp','leftMenu');		
					basics.tab.tabUtils.refreshTab(i18n['ci_configureItemAdmin'],'../pages/itsm/cim/configureItem.jsp');	
				}
			});
			$('#ci_loc').click(function(){
				common.config.category.eventCategoryTree.selectlocation('#ci_loc','#ci_locid');			
			});
			//新增
			$('#itemAddSave').click(function(){
				itsm.cim.configureItemAdd.saveConfigureItem();
			});
			itsm.cim.ciCategoryTree.defaultShowAttributet($('#ci_add_categoryNo').val(),'ci_add_eavAttributet');
			
			//选择部门
			$('#configureItem_add_department').click(function(){
				common.security.organizationTreeUtil.showAll_2('#index_assignGroup_window','#index_assignGroup_tree','#configureItem_add_department','',$('#ci_add_companyNo').val());
			});
			//公司选择
			$('#search_ci_companyName').click(function(){
				itsm.itsop.selectCompany.openSelectCompanyWin('#ci_add_companyNo','#ci_add_companyName','#configureItem_add_department,#ci_add_useName,#ci_add_owner,#ci_add_originalUser');
			});
			
			//加载默认公司
			common.security.defaultCompany.loadDefaultCompany('#ci_add_companyNo','#ci_add_companyName');
			$('#saveconfigureItemTemplateBtn').click(itsm.cim.configureItemAdd.configureItemTemplateNameWin);
			$('#addTemplateOk').click(function(){
				itsm.cim.configureItemAdd.saveConfigureItemTemplate("add");
			});
			$('#editconfigureItemTemplateBtn').click(function(){
				itsm.cim.configureItemAdd.saveConfigureItemTemplate("edit");
			});
			if(editTemplateId!=""){
				$('#configureItemTemplate').val(editTemplateId);
				itsm.cim.configureItemAdd.getTemplateValue(editTemplateId);
			}
			$('#add_ci_service_add').click(function(){//关联服务目录
				common.config.category.serviceCatalog.selectServiceDir('#add_ci_serviceDirectory_tbody');
    		});
			
			
			$('#add_ci_relatedService_checked').click(function(){checkAll('add_ci_relatedService_checked','add_ci_relatedService','ciDto.serviceDirectoryNos');});
			itsm.app.autocomplete.autocomplete.bindAutoComplete('#ci_add_companyName','com.wstuo.common.security.entity.Organization','orgName','orgName','orgNo','Long','#ci_add_companyNo',userName,'true');//所属客户
			
			//实例化附加上传
			setTimeout(function(){
				getUploader('#addConfigureItem_uploadAttachments','#addConfigureItem_attachments','#addConfigureItem_uploadedAttachments','addConfigureItem_fileQueue');
				getUploader('#addConfigureItem_uploadSoftAttachments','#addConfigureItem_Softattachments','#add_uploadedSoftAttachments','addConfigureItem_SoftfileQueue');
				//initFileUpload("_CiAdd","","","","","#addConfigureItem_attachments","#addConfigureItem_fileQueue");
				//initFileUpload("_CiAddSoft","","","","","#editConfigureItem_Softattachments","#edit_uploadedSoftAttachments");
			},0);
		}
	};
 }(); 
 //加载初始化数据
 $(document).ready(itsm.cim.configureItemAdd.init);