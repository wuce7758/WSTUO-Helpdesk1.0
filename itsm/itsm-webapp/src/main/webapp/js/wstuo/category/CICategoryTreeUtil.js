$package('wstuo.category');
/**  
 * @author QXY  
 * @constructor WSTO
 * @description 配置项分类树帮助类
 * @date 2010-11-17
 * @since version 1.0 
 */ 
wstuo.category.CICategoryTreeUtil=function(){
	return {
		/**
		 * 显示配置项分类树.
		 * @param windowPanel 窗口面板元素
		 * @param treePanel 树面板元素
		 * @param namePut 名称元素
		 * @param idPut Id元素
		 */
		showCITree:function(windowPanel,treePanel,namePut,idPut){
			$(treePanel).jstree({
				"json_data":{
				    ajax: {
				    	url : "ciCategory!getConfigurationCategoryTree.action",
				    	data:function(n){
					    	  return {'parentEventId':n.attr ? n.attr("cno").replace("node_",""):0};//types is in action
					    },
				    	cache:false}
					},
					plugins : ["themes", "json_data", "ui", "crrm"]
				})
				.bind('loaded.jstree', function(e,data){data.inst.open_all(-1);})
				.bind('select_node.jstree',function(e,data){
					var id=data.rslt.obj.attr('cno');
					var name=data.rslt.obj.attr('cname');
					$(namePut).val(name);
					$(idPut).val(id);
					$(windowPanel).dialog('close');
				});
			windows(windowPanel.replace('#',''));
		},
		/**
		 * 选择配置项分类
		 * @param namePut 名称元素
		 * @param idPut Id元素
		 */
		selectCICategory:function(nameId,valueId){
			wstuo.category.CICategoryTreeUtil.showCITree('#change_category_select_window','#change_category_select_tree',nameId,valueId);
			
		}
	}
 }();

