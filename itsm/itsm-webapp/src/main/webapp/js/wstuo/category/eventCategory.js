$package('wstuo.category');
$import('wstuo.category.eventCategoryTree');
$import('wstuo.category.CICategory');
/**  
 * @author QXY  
 * @constructor WSTO
 * @description 分类
 * @date 2010-11-17
 * @since version 1.0 
 */ 
wstuo.category.eventCategory = function(){
	this.loadCategoryFlag="0";
	return {
			/**
			 * 根据模块确定公共元素属于哪个模块
			 * @param selector 公共元素
			 */
			full:function(selector){
				return $(selector); 
			},
			/**
			 * @description 修改分类信息.
			 */
			updateCategory:function(e) {
				var eventId=$('#eventId').val();
				if(eventId==null || eventId==''){
					msgAlert(i18n['msg_msg_pleaseChooseCategory'],'info');
				}else{
					if($("#eventName").val().toLowerCase() == "null"){
						$("#eventName").val("");
						msgAlert(i18n['err_categoryNameNotNull'],'info');
					}
				    var editForm = wstuo.category.eventCategory.full('#editCategoryForm').serialize();
                    var _url = "event!update.action";
                    startProcess();
				    // 判断分类是否存在    
                    $.post('event!isCategoryExistdOnEdit.action', editForm, function(data){
                        if(data) {
                            msgAlert(i18n['err_categoryNameIsExisted'],'info');
                            endProcess();
                        } else {
    						$.post(_url,editForm,
    							function (r) {
        							wstuo.category.eventCategoryTree.showCategoryTree('#dictionary_div_tree',$("#eventCategoryCode").val(),dictionaryGroup.saveCategory);
        							msgShow(i18n['msg_edit_successful'],'show');
        							$('#editEventCategoryTree').dialog('close');
    								endProcess();
    							},
    							'json'
    						);
                        }
                    });
				}
			},
			/**
			 * @description 新增子分类.
			 */
			addCategory:function() {
				var parentEventId=$('#parentEventId').val();
				var rootId = wstuo.category.eventCategory.full('#dictionary_div_tree').find('li').length;
				if( (parentEventId==null || parentEventId=='') && rootId > 0){
					msgAlert(i18n['msg_msg_pleaseChooseParent'],'info');
				}else{
					if($("#addeventName").val().toLowerCase() == "null"){
						$("#addeventName").val("");
						msgAlert(i18n['err_categoryNameNotNull'],'info');
					}
					    var addForm = wstuo.category.eventCategory.full('#addEventCategoryTree form').serialize();
					    // 判断分类是否存在    
					    startProcess();
					    $.post('event!isCategoryExisted.action', addForm, function(data){
		                    if(data) {
		                    	endProcess();
		                        msgAlert(i18n['err_categoryNameIsExisted'],'info');
		                    } else {
		                        if(rootId>0){
		                            $.post('event!save.action', addForm, function(r){
		                            	 wstuo.category.eventCategoryTree.showCategoryTree('#dictionary_div_tree',$("#eventCategoryCode").val(),dictionaryGroup.saveCategory);
		                    			 wstuo.category.eventCategory.full('#addEventCategoryTree [name="eventCategoryDto.eventName"]').val("");
		                                 wstuo.category.eventCategory.full('#addEventCategoryTree [name="eventCategoryDto.eavId"]').val("");
		                                 wstuo.category.eventCategory.full('#addEventCategoryTree [name="eventCategoryDto.scores"]').val("");
		                                 wstuo.category.eventCategory.full('#addEventCategoryTree [name="eventCategoryDto.eventDescription"]').val("");
		                                 wstuo.category.eventCategory.full('#addEventCategoryTree [name="eventCategoryDto.categoryCodeRule"]').val("");
		                                 msgShow(i18n['msg_add_successful'],'show');
		                                 $('#addEventCategoryTree').dialog('close');
		                                 endProcess();
		                                },'json'); 
		                        }else{
		                             $.post('event!save.action', addForm+"&eventCategoryDto.categoryRoot="+$("#eventCategoryCode").val(), function(r){
		                            	 wstuo.category.eventCategoryTree.showCategoryTree('#dictionary_div_tree',$("#eventCategoryCode").val(),dictionaryGroup.saveCategory);
		                     			 wstuo.category.eventCategory.full('#addEventCategoryTree [name="eventCategoryDto.eventName"]').val("");
		                                 wstuo.category.eventCategory.full('#addEventCategoryTree [name="eventCategoryDto.eavId"]').val("");
		                                 wstuo.category.eventCategory.full('#addEventCategoryTree [name="eventCategoryDto.scores"]').val("");
		                                 wstuo.category.eventCategory.full('#addEventCategoryTree [name="eventCategoryDto.eventDescription"]').val("");
		                                 wstuo.category.eventCategory.full('#addEventCategoryTree [name="eventCategoryDto.categoryCodeRule"]').val("");
		                                 msgShow(i18n['msg_add_successful'],'show');
		                                 $('#addEventCategoryTree').dialog('close');
	                                    endProcess();
	                                },'json');
		                        }
		                    }
		                });
				}
			},
			/**
			 * 分类导出 
			 */
			exportEventCategory:function(){
				$("#exportEventCategoryWindow"+" form").submit();
			},
			/**
			 * 选择导入文件框
			 */
			importEventCategory:function(){
				windows('importEventCategoryWindow',{width:400});
			},
			/**
			 * @description 导入数据库
			 */
			importEventCategoryCsv:function(){
				$.ajaxFileUpload({
		            url:'event!importEventCategory.action',
		            secureuri:false,
		            fileElementId:'importEventCategoryFile', 
		            dataType:'json',
		            success: function(data,status){
		            	if(data=="FileNotFound"){
							msgAlert(i18n['msg_dc_fileNotExists'],'error');
						}else if(data=="IOError"){
							msgAlert(i18n['msg_dc_importFailure'],'error');
						}else{
							msgAlert(i18n['msg_dc_importSuccess'].replace('N','<br>['+data+']<br>'),'show');
							wstuo.category.eventCategoryTree.showCategoryTree('#dictionary_div_tree',$("#eventCategoryCode").val(),dictionaryGroup.saveCategory);
							$('#importEventCategoryWindow').dialog('colse');
						}
		            	
		            }
		        });
			},
			/**
			 * @description  加载自定义表单下拉列表.
			 * @param select 自定义表单下拉列表Id
			 */
			loadFormToSelect:function(select){
				var url = "formCustom!findAllRequestFormCustom.action";
				$.post(url, function(res){
					$(select).html('');
					$('<option value="">--'+i18n.pleaseSelect+'--</option>').appendTo(select);
					$.each(res,function(index,formCustom){
						$("<option value='"+formCustom.formCustomId+"'>"+formCustom.formCustomName+"</option>").appendTo(select);
					});
				});
			},
			/**
			 * 初始化
			 * @private
			 */
			init:function(){
				 //wstuo.category.eventCategoryTree.showCategoryTree( wstuo.category.eventCategory.full("#eventCategoryTree"));
				 //wstuo.category.CICategory.loadEavsToSelect("#edit_category");//加载列表
				// wstuo.category.eventCategory.loadFormToSelect("#edit_form");//加载列表
			}
	}
 }();
$(document).ready(wstuo.category.eventCategory.init);