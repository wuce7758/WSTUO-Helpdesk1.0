$package('wstuo.jbpmMge'); 
/**  
 * @fileOverview 我待处理的任务
 * @author Tan
 * @version 1.0  
 */  
 /**  
 * @author Tan  
 * @constructor My Outstanding Task
 * @description 我待处理的任务
 * @date 2012-12-19
 * @since version 1.0 
 */

wstuo.jbpmMge.myOutstandingTask = function(){

	return {
		/**
		 * 操作项格式化
		 */
		viewDetailedForma:function(cellvalue, options, rowObject){
			var executionId=rowObject.executionId;
			var variables = cellvalue;
			var _href='<a href="javascript:{href}">['+i18n['common_view']+']</a>';
			
			var dto = variables.dto;
			if(dto!=undefined && dto!=null){
				if(executionId.indexOf('Changes')>=0 || executionId.indexOf('Change')>=0 || executionId.indexOf('change')>=0){
					return _href.replace('{href}','basics.tab.tabUtils.reOpenTab(\'change!changeInfo.action?queryDTO.eno='+dto.eno+'\',\''+i18n["change_detail"]+'\')');
				}else if(executionId.indexOf('problem')>=0 || executionId.indexOf('Problem')>=0){
					return _href.replace('{href}','basics.tab.tabUtils.reOpenTab(\'problem!findProblemById.action?eno='+dto.eno+'\',\''+i18n["probelm_detail"]+'\')');
				}else if(executionId.indexOf('release')>=0 || executionId.indexOf('Release')>=0){
					return _href.replace('{href}','basics.tab.tabUtils.reOpenTab(\'release!releaseDetails.action?releaseDTO.eno='+dto.eno+'\',\''+i18n["release_details"]+'\')');
				}else if(executionId.indexOf('request')>=0 || executionId.indexOf('Request')>=0){
					return _href.replace('{href}','basics.tab.tabUtils.reOpenTab(\'request!requestDetails.action?eno='+dto.eno+'\',\''+i18n["request_detail"]+'\')');
				}
			}else if(variables.eno!=null){
				if(executionId.indexOf('Changes')>=0 || executionId.indexOf('Change')>=0 || executionId.indexOf('change')>=0){
					return _href.replace('{href}','basics.tab.tabUtils.reOpenTab(\'change!changeInfo.action?queryDTO.eno='+variables.eno+'\',\''+i18n["change_detail"]+'\')');
				}else if(executionId.indexOf('problem')>=0 || executionId.indexOf('Problem')>=0){
					return _href.replace('{href}','basics.tab.tabUtils.reOpenTab(\'problem!findProblemById.action?eno='+variables.eno+'\',\''+i18n["probelm_detail"]+'\')');
				}else if(executionId.indexOf('release')>=0 || executionId.indexOf('Release')>=0){
					return _href.replace('{href}','basics.tab.tabUtils.reOpenTab(\'release!releaseDetails.action?releaseDTO.eno='+variables.eno+'\',\''+i18n["release_details"]+'\')');
				}else if(executionId.indexOf('request')>=0 || executionId.indexOf('Request')>=0){
					return _href.replace('{href}','basics.tab.tabUtils.reOpenTab(\'request!requestDetails.action?eno='+variables.eno+'\',\''+i18n["request_detail"]+'\')');
				}
			}else{
				return '';
			}
			
		},
		/**
		 * 标题格式化
		 */
		etitleForma:function(cellvalue, options, rowObject){
			var variables = cellvalue;
			if(variables!=undefined && variables!=null){
				return variables.etitle;
			}else{
				return '';
			}
		},
		/**
		 * 类型格式化
		 */
		taskTypeForma:function(cellvalue, options, rowObject){
			var executionId=rowObject.executionId;
			if(executionId.indexOf('Changes')>=0 || executionId.indexOf('Change')>=0 || executionId.indexOf('change')>=0){
				return i18n['change']
			}else if(executionId.indexOf('problem')>=0 || executionId.indexOf('Problem')>=0){  
				return i18n['problem']
			}else if(executionId.indexOf('release')>=0 || executionId.indexOf('Release')>=0){
				return i18n['release']
			}else if(executionId.indexOf('request')>=0 || executionId.indexOf('Request')>=0){
				return i18n['request']
			}
		},
		/**
		 * 显示我待处理的任务列表
		 */
		showTaskGrid:function(){
			var _url='jbpm!findPagePersonalTasks.action';
			if(taskTypes=='proxy'){
				_url='jbpm!findPageProxyTasks.action';
			}
			var gridCaption = {
			        'all' : i18n['title_my_outstanding_tasks'],
			        'proxy' : i18n['title_my_proxy_outstanding_tasks']
			};
			var params=$.extend({},jqGridParams,{
				//caption:gridCaption[taskTypes],
				url:_url,
				postData:{'assignee':userName},
				colNames:
						[
						 i18n['label_bpm_task_id']
						,i18n['title']
						,i18n['title_request_assignTo']
						,i18n['label_bpm_activity_name']
						,i18n['label_bpm_task_name']
						,i18n['priority']
						,i18n['common_createTime']
						//,i18n['label_bpm_task_duedate']
						,i18n['label_bpm_task_type']
						,i18n['operateItems']],
				colModel:[
				          {name:'id',sortable:false,align:'center'},
				          {name:'variables',index:'variables',width:80,align:'center',hidden:false,formatter:function(cellvalue, options, rowObject){
								 return rowObject.variables.etitle;
						  }},
						  {name:'assignee',width:120,sortable:false,align:'center'},
						  {name:'activityName',hidden:true,sortable:false,align:'center'},
						  {name:'name',width:100,sortable:false,align:'center'},
						  {name:'priority',width:100,hidden:true,sortable:false,align:'center'},
						  {name:'createTime',formatter:timeFormatter,sortable:false,align:'center'},
						 // {name:'duedate',formatter:timeFormatter,sortable:false,align:'center'},
						  {name:'executionId',sortable:false,formatter:wstuo.jbpmMge.myOutstandingTask.taskTypeForma,align:'center'},
						  {name:'variables',formatter:wstuo.jbpmMge.myOutstandingTask.viewDetailedForma,sortable:false,align:'center'}
					  ],
				toolbar:false,
				multiselect:false,
				ondblClickRow:function(rowid){
					var href=$("#"+rowid+" a").attr("href");
					if(href!=undefined && href!='')
						window.location=href;
				},
				jsonReader: $.extend(jqGridJsonReader,{id:"id"}),
				sortname:'id',
				pager:'#myOutstandingTasksPager'
			});
			$("#myOutstandingTasksGrid").jqGrid(params);
			$("#myOutstandingTasksGrid").navGrid('#myOutstandingTasksPager',navGridParams);
			//自适应大小
			setGridWidth("#myOutstandingTasksGrid","regCenter",20);
		},
		/**
		 * 初始化
		 * @private
		 */
		init:function(){
			wstuo.jbpmMge.myOutstandingTask.showTaskGrid();
		}
	}
}();

$(document).ready(wstuo.jbpmMge.myOutstandingTask.init);

