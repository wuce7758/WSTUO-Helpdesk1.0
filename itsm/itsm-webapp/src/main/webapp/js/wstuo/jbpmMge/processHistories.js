$package('wstuo.jbpmMge') 

 /**  
 * @author WSTUO  
 * @constructor historys
 * @description for 流程图跟踪函数

 */
wstuo.jbpmMge.processHistories = function(){
	return {
		/**
		 * 时间格式化
		 * @param  cellvalue时间值
		 */
		format:function(cellvalue){	
			var str = "";
	        var hour = 0;
	        var minute = 0;
	        var second = 0;
	        second = cellvalue / 1000;
	        if (second > 60) {
	            minute = second / 60;
	            second = second % 60;
	        }
	        if (minute > 60) {
	            hour = minute / 60;
	            minute = minute % 60;
	        }
	        var strHour = parseInt(hour);
	        var strMinute = parseInt(minute);	
	        var strSecond = parseInt(second);
	        return (strHour + i18n['hours'] + strMinute +i18n['minutes']
	                + strSecond + i18n['second']);
		},
		/**
		 * 流程跟踪图
		 */
		urlProcessInstanceId:function(cellvalue){
			return "<a href=javascript:wstuo.jbpmMge.processHistories.showProcessImg('"+cellvalue+"')>"+i18n['title_request_traceInstance']+"</a>"
		},
		/**
		 * @description 查看流程图
		 */
		showProcessImg:function(id){
			 var _url = 'jbpm!getActivityCoordinates.action?pid='+id;
			 basics.tab.tabUtils.reOpenTab(_url,i18n['title_request_traceInstance']);
		},
		/**
		 * 流程实例状态
		 */
		endActivityNameFormat:function(cellName){
			if(cellName=='结束'){
				return i18n['label_end']
			}else if(cellName=='end'){
				return i18n['label_end']
			}else if(cellName=='end1'){
				return i18n['label_end']
			}else if(cellName!=null){
				return cellName;
			}else{
				return '';
			}
		},
		/**
		 * @description 加载流程实例列表
		 * */
		load_processHistories_list:function(){
			var params = $.extend({},jqGridParams, {	
				url:'jbpm!findPageHistoryProcessInstance.action',
				colNames:[i18n['title_jbpm_processId'],i18n['title_jbpm_instanceId'],i18n['status'],i18n['title_startTime'],i18n['title_endTime'],i18n['title_jbpm_duration'],i18n['title_jbpm_endActivityName'],i18n['title_jbpm_currentActivityName'],i18n['title_request_traceInstance']],
				colModel:[
						  {name:'processDefinitionId',index:'processDefinitionId',align:'center',width:125,sortable:false},
						  {name:'processInstanceId',index:'processInstanceId',align:'center',width:125,sortable:false},
						  {name:'state',index:'state',width:50,sortable:false,align:'center'},
						  {name:'startTime',index:'startTime',width:100,align:'center',formatter:timeFormatter,sortable:false},
						  {name:'endTime',index:'endTime',width:100,sortable:false,align:'center',formatter:timeFormatter},
						  {name:'duration',index:'duration',width:100,align:'center',editable:true,formatter:wstuo.jbpmMge.processHistories.format,sortable:false},
						  {name:'endActivityName',index:'endActivityName',width:100,sortable:false,align:'center',formatter:wstuo.jbpmMge.processHistories.endActivityNameFormat},
						  {name:'endActivityName',width:100,sortable:false,align:'center',hidden:true},
						  {name:'processInstanceId',width:150,sortable:false,align:'center',formatter:wstuo.jbpmMge.processHistories.urlProcessInstanceId}
					  ],
				multiselect:false,
				jsonReader: $.extend(jqGridJsonReader, {id: "processInstanceId"}),
				sortname:'processInstanceId',
				pager:'#historyProcessPage'
				});
			
				$("#historyProcessGrid").jqGrid(params);
				$("#historyProcessGrid").navGrid('#historyProcessPage',navGridParams);
				//列表操作项
				$("#t_historyProcessGrid").css(jqGridTopStyles);
				$("#t_historyProcessGrid").append($('#historyProcessGridToolbar').html());
				
				//自适应宽度
				setGridWidth("#historyProcessGrid","regCenter",20);				
			
		},
		/**
		 * @description 打开搜索框
		 * */
		search_openwindow:function(){		
			windows('searchProcessHistorie',{width:350,height:200,modal: false});
		},
	
		/**关闭搜索框*/
		search_closewindow:function(){		
			$('#searchProcessHistorie').dialog('close');
		},
		
		/**
		 * @description 历史流程搜索
		 */
		searchHistoryProcess:function(){
			var sdata = $('#searchProcessHistorie form').getForm();
			var postData = $("#historyProcessGrid").jqGrid("getGridParam", "postData");     
			$.extend(postData, sdata);
			$('#historyProcessGrid').jqGrid('setGridParam',{page:1,url:'jbpm!findPageHistoryProcessInstance.action'}).trigger('reloadGrid');
		},
		/**
		 * 初始化
		 * @private
		 */
		init: function() {
			//加载数据列表
			wstuo.jbpmMge.processHistories.load_processHistories_list();
		}
	}
}();
$(function(){wstuo.jbpmMge.processHistories.init();});