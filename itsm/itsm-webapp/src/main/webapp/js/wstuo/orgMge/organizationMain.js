
$package('wstuo.orgMge');
$import('wstuo.orgMge.organizationGrid');
$import('wstuo.orgMge.organizationTree');
$import('wstuo.orgMge.organizationHoliday');
$import('wstuo.orgMge.organizationServiceTime');
$import('wstuo.user.userUtil');
/**
 * @author WSTUO 
 **/
wstuo.orgMge.organizationMain = function() {
	//加载标识
	this.holidayTabClick="0";
	this.gridTabClick="0";
	this.roleTabClick="0";
	this.initTable=true;
	
	return {
		/**
		 * @description 编辑当前机构
		 **/
		saveCurrentOrg:function (){
			validateCallback('#editOrgForm',function(){
				var fullName=$('#organizationDto_personInChargeName').val();
				validateUserByFullName(fullName,wstuo.orgMge.organizationMain.checkUserEdit);
			});
		},
		
		
		/**
		 * @description 编辑机构方法
		 **/
		checkUserEdit:function(){
			var editorgName=trim($('#edit_orgName').val());
			$('#edit_orgName').val(editorgName);
			var frm = $('#editOrgForm').serialize();
			var edit_orgType=$('#edit_orgType').val();
			if(edit_orgType=="servicePanel" || edit_orgType=="innerPanel"){
				msgAlert(i18n.ERROR_SYSTEM_DATA_CAN_NOT_EDIT);
			}else{
				var url = 'organization!updateOrg.action';
				// 判断分类是否存在    
                $.post('organization!isCategoryExistdOnEdit.action', frm, function(data){
                   if (data) {
                       msgAlert(i18n.err_orgNameIsExisted);
                   }  else {
                       $.post(url,frm, function(){
                           wstuo.orgMge.organizationTree.loadOrganizationTreeView('#orgTreeDIV');//重新载入树结构
                           $('#orgGrid').trigger('reloadGrid');
                           show(i18n.tip,i18n['msg_org_orgEditSuccessful']);
                       });
                   }
                });
			}
		},
		
		/**
		 * @description 保存机构信息
		 **/
		addCurrentOrg:function (){
			validateCallback('#addOrgForm',function(){
				var parentNo=$('#add_parentOrgNo').val();
				if(parentNo!=null &&  parentNo!=""){
					startProcess();
					var orgName=trim($('#addChild_orgName').val());
					$('#addChild_orgName').val(orgName);
					var fullName=$('#organizationDto_personInChargeName_add').val();
					validateUserByFullName(fullName,wstuo.orgMge.organizationMain.addCurrentOrgMethod);
					endProcess();
				}else{
					msgAlert(i18n["msg_chooseOrg"],"info");
				}
			});
		},
		
		/**
		 * @description 保存机构信息
		 **/
		addCurrentOrgMethod:function (){
				var frm = $('#addOrgForm').serialize();
				var url = 'organization!save.action';
				$.post('organization!isCategoryExisted.action', frm, function(data){
	                   if (data) {
	                	   endProcess();
	                       msgAlert(i18n.err_orgNameIsExisted);
	                   }  else {
	                       $.post(url,frm, function(){
	                           //重新载入树结构
	                           wstuo.orgMge.organizationTree.loadOrganizationTreeView('#orgTreeDIV');
	                       
	                           $('#orgGrid').trigger('reloadGrid');
	                           $('#add_actionType').val('inner');
	                           $('#addChild_orgName').val('');
	                           $('#addChild_officePhone').val('');
	                           $('#addChild_officeFax').val('');
	                           $('#addChild_email').val('');
	                           $('#addChild_address').val('');
	                           $('#organizationDto_personInChargeName_add').val('');
	                           $('#organizationDto_personInChargeNo_add').val('');
	                           show(i18n.tip,i18n['msg_org_orgAddSuccessful']);
	                           endProcess();
	                       });
	                   }
	                   
				});
		},
		
		/**
		 * @description 默认显示公司信息
		 **/
		showCompanyInfo:function(){
			var url = 'organization!findCompany.action?companyNo='+companyNo;
			$.post(url,function(res){

				$("#edit_actionType>option[value='services']").remove();
				$("#edit_actionType>option[value='inner']").remove();
				$("<option value='company'>"+i18n['label_org_companyInfo']+"</option>").appendTo("#edit_actionType");
				
				if(res.officeFax!=null && res.officeFax!="")
					$('#edit_officeFax').val(res.officeFax);
				else
					$('#edit_officeFax').val("");
				if(res.officePhone!=null && res.officePhone!="")
					$('#edit_officePhone').val(res.officePhone);
				else
					$('#edit_officePhone').val("");
				if(res.email!=null && res.email!="")
					$('#edit_email').val(res.email);
				else
					$('#edit_email').val("");
				if(res.address!=null && res.address!="")
					$('#edit_address').val(res.address);
				else
					$('#edit_address').val("");
				if(res.orgNo!=null && res.orgNo!="")
					$('#edit_orgNo').val(res.orgNo);
				else
					$('#edit_orgNo').val("");
				if(res.orgName!=null && res.orgName!="")
					$('#edit_orgName').val(res.orgName);
				else
					$('#edit_orgName').val("");
				if(res.personInChargeName!=null && res.personInChargeName!='null'){
					$('#organizationDto_personInChargeName').val(res.personInChargeName);
					//$('#organizationDto_personInChargeNo').val(res.personInChargeNo);
				}else{
					$('#organizationDto_personInChargeName').val('');
					//$('#organizationDto_personInChargeNo').val('');
				}
				
			});
		},
		
		
		/**
		 * @description 为tab点击绑定事件
		 **/
		innerTabClick:function(){

		        $('#organizationMainTab').tabs({
		            onSelect:function(title){
		            	
		        		if(title==i18n['title_org_holiday'] && holidayTabClick=="0"){	
		        			wstuo.orgMge.organizationHoliday.init();
	        				holidayTabClick="1";
		        		}

		        		if(title==i18n['common_gridView'] && gridTabClick=="0"){     
	        				wstuo.orgMge.organizationGrid.init();
	        				gridTabClick="1";
		        		}
		        		if(title==i18n['title_org_role'] && roleTabClick=="0"){
	        			
	        				wstuo.orgMge.organizationRole.showRoleView('','#orgRoleDiv');	        				
	        				roleTabClick="1";
		        		}
		            }  
		        });
		},
	
		
		
		/**
		 * @description 选择机构负责人.
		 * @param 用户编号
		 * @param 用户名
		 **/
		selectPersonIncharge:function(userId,userName){
			wstuo.user.userUtil.selectUser(userName,userId,'','fullName',companyNo);
		},
		initTable:function(){
			if(initTable){
				wstuo.orgMge.organizationGrid.init();
				initTable=false;
			}
			/*ajaxHtml("organization!find.action?companyNo=1",function(data){
				$("#orgMgeTable_div").html(data);
				initUI($("#orgMgeTable_div"));
				initTable=false;
			}); */
		},
		/**
		 * 载入.
		 **/
		init:function(){	
			//显示机构树
			wstuo.orgMge.organizationTree.loadOrganizationTreeView('');
			
			wstuo.orgMge.organizationGrid.init();
			//点击事件
			$('#mergeCurrentOrgBtn').click(wstuo.orgMge.organizationMain.saveCurrentOrg);
//			$('#saveCurrentOrgBtn').click(wstuo.orgMge.organizationMain.addCurrentOrg);
//			
//			//服务时间
			if(serviceTime=='1')
				wstuo.orgMge.organizationServiceTime.init();
			if(holiday=='1')
				wstuo.orgMge.organizationHoliday.init();
//			
			//绑定日期控件
			DatePicker97(['#hoilday_hdate','#hoilday_hedate']);
			$('#organizationDto_personInChargeName_add_choose').click(function(){//选择机构负责人
				wstuo.orgMge.organizationMain.selectPersonIncharge('#organizationDto_personInChargeNo_add','#organizationDto_personInChargeName_add');
				
			});
			$('#organizationDto_personInChargeName_search_choose').click(function(){//选择机构负责人
				wstuo.orgMge.organizationMain.selectPersonIncharge('','#organizationDto_personInChargeName_search');
				
			});
			
			$('#organizationDto_personInChargeName_gridAdd_choose').click(function(){
				wstuo.orgMge.organizationMain.selectPersonIncharge('#organizationDto_personInChargeNo_gridAdd','#organizationDto_personInChargeName_gridAdd');
				
			});
		}
		
	};
	

}();
//载入
$(document).ready(wstuo.orgMge.organizationMain.init());
