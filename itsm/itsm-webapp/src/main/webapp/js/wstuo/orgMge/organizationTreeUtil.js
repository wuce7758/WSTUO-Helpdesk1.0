$package('wstuo.orgMge');
$import('wstuo.includes');
/**  
 * @author WSTUO 
 */
wstuo.orgMge.organizationTreeUtil = function() {
	return {
		/**
		 * 显示所有机构，含面板
		 * @param 树编号
		 * @param 回调函数
		 * @param 机构编号
		 */
		showAll:function(treeId,callback,companyNo){
			$(treeId).jstree({
				json_data:{
					ajax: {
						url : "organization!findAll.action?companyNo="+companyNo,
						data:function(n){
					    	  return {'parentOrgNo':n.attr ? n.attr("orgNo").replace("node_",""):0};//types is in action
						},
						cache: false
					}
				},
				plugins : ["themes", "json_data", "ui", "crrm"]

			})
			//.bind('loaded.jstree', function(e,data){data.inst.open_all(-1);})
			.bind('select_node.jstree',function(e,data){
				callback(e,data);
				
			});
		},
		
		/**
		 * 显示所有机构，含面板
		 * @param  windowPanel 窗体面板
		 * @param  treePanel  树面板
		 * @param  namePut 选中机构（name）的赋值ID
		 * @param  idPut  选中机构（id）的赋值ID
		 * @param  _companyNo  机构编号
		 */
		showAll_2:function(windowPanel,treePanel,namePut,idPut,_companyNo){
			wstuo.includes.loadCategoryIncludesFile();
			windows(windowPanel.replace('#',''),{width:260,height:400});
			$(treePanel).jstree({
				json_data:{
					ajax: {
						url : "organization!findAll.action?companyNo="+_companyNo,
						data:function(n){
					    	  return {'parentOrgNo':n.attr ? n.attr("orgNo").replace("node_",""):0};//types is in action
						},
						cache: false}
				},
				plugins : ["themes", "json_data", "ui", "crrm"]

			})
			//.bind('loaded.jstree', function(e,data){data.inst.open_all(-1);})
			.bind('select_node.jstree',function(e,data){
				var orgType=data.rslt.obj.attr("orgType");
				if(orgType!='innerPanel' && orgType!='servicePanel'){
					$(namePut).val(data.rslt.obj.attr("orgName")).focus();
					$(idPut).val(data.rslt.obj.attr("orgNo"));
					$(windowPanel).dialog('close');//关闭窗口
					$('#search_user_orgType').val('');
					if(orgType=='innerPanel'){		
						$('#search_user_orgType').val('allInner');
						return;
					}
					if(orgType=='ROOT'){
						$('#user_orgName').val('');
						$('#user_search_orgName').val('');
						$('#itsop_user_orgName').val('');
						$('#search_requestorOrgName').val('');
						$('#search_assigneGroupName').val('');
						return;
					}
					if(orgType=='servicePanel'){		
						$('#search_user_orgType').val('allService');
						return;
					}
				}
			});
		},
		
		/**
		 * 显示所有机构，含面板
		 * @param  windowPanel 窗体面板
		 * @param  treePanel  树面板
		 * @param  namePut 选中机构（name）的赋值ID
		 * @param  idPut  选中机构（id）的赋值ID
		 * @param  _companyNo  机构编号
		 */
		showAll_3:function(windowPanel,treePanel,namePut,idPut,_companyNo){
			windows(windowPanel.replace('#',''),{width:260,height:400});
			$(treePanel).jstree({
				json_data:{
					ajax: {
						url : "organization!findAll.action?companyNo="+_companyNo,
						data:function(n){
					    	  return {'parentOrgNo':n.attr ? n.attr("orgNo").replace("node_",""):0};//types is in action
						},
						cache: false}
				},
				plugins : ["themes", "json_data", "ui", "crrm"]

			})
			//.bind('loaded.jstree', function(e,data){data.inst.open_all(-1);})
			.bind('select_node.jstree',function(e,data){
				var orgType=data.rslt.obj.attr("orgType");
				$(namePut).val(data.rslt.obj.attr("orgName")).focus();
				$(windowPanel).dialog('close');//关闭窗口
			});
		},
		
		
		/**
		 * 选择机构
		 * @param windowPanel
		 * @param treePanel
		 * @param namePut 选中机构（name）的赋值id
		 * @param idPut  选中机构（id）的赋值ID
		 * @param _companyNo 机构编号
		 * @param method  回调方法
		 */
		selectOrg:function(windowPanel,treePanel,namePut,idPut,_companyNo,method){
			windows(windowPanel.replace('#',''),{width:260,height:400});
			$(treePanel).jstree({
				json_data:{
					ajax: {
						url : "organization!findAll.action?companyNo="+_companyNo,
						data:function(n){
					    	  return {'parentOrgNo':n.attr ? n.attr("orgNo").replace("node_",""):0};//types is in action
						},
						cache: false}
				},
				plugins : ["themes", "json_data", "ui", "crrm"]

			})
			//.bind('loaded.jstree', function(e,data){data.inst.open_all(-1);})
			.bind('select_node.jstree',function(e,data){
				var orgType=data.rslt.obj.attr("orgType");
				if(orgType!='innerPanel' && orgType!='servicePanel'){
					$(namePut).val(data.rslt.obj.attr("orgName")).focus();
					$(idPut).val(data.rslt.obj.attr("orgNo"));
					$(windowPanel).dialog('close');//关闭窗口
					$('#search_user_orgType').val('');
					if(orgType=='innerPanel'){		
						$('#search_user_orgType').val('allInner');
						return;
					}
					if(orgType=='ROOT'){
						$('#user_orgName').val('');
						$('#user_search_orgName').val('');
						$('#itsop_user_orgName').val('');
						return;
					}
					if(orgType=='servicePanel'){		
						$('#search_user_orgType').val('allService');
						return;
					}
				}
				method(data.rslt.obj.attr("orgNo"),data.rslt.obj.attr("orgName"),orgType);
			});
			
		},
		/**
		 * 公共模块，指派组.
		 * @param namePut 显示编号
		 * @param idPut   选中机构（id）的赋值ID
		 */
		assginGroup:function(namePut,idPut){
			wstuo.orgMge.organizationTreeUtil.showAll_2('#index_assignGroup_window','#index_assignGroup_tree',namePut,idPut);
		},
		
		/**
		 * 仅显示服务机构
		 * @param  windowPanel 窗体面板
		 * @param  treePanel  树面板
		 * @param  namePut 选中机构（name）的赋值ID
		 * @param  idPut   选中机构（id）的赋值ID
		 */
		showService:function(windowPanel,treePanel,namePut,idPut){
			windows(windowPanel.replace('#',''),{width:260,height:400});
			$(treePanel).jstree({
				json_data:{
					ajax: {url : "organization!findServicesTree.action",cache: false}
				},
				plugins : ["themes", "json_data", "ui", "crrm"]
			})
			.bind('loaded.jstree', function(e,data){data.inst.open_all(-1);})//默认展开
			.bind('select_node.jstree',function(e,data){


				$(namePut).val(data.rslt.obj.attr("orgName"));
				$(idPut).val(data.rslt.obj.attr("orgNo"));
	
				$(windowPanel).dialog('close');//关闭窗口
				
			});
			
			
		},
		
		/**
		 * 仅显示内部机构.
		 * @deprecated    
		 */
		showInner:function(treeId,callback){
			
		},
		
		/**
		 * 显示所有机构，不含面板.
		 * @param windowId 窗体编号
		 * @param treeId   树编号
		 * @param p_companyNo 机构编号
		 * @param callback 回调函数
		 */
		showSelectOrgs:function(windowId,treeId,p_companyNo,callback){
			
			windows(windowId.replace('#',''),{width:260,height:400});
			$(treeId).jstree({
				json_data:{
					ajax: {
						url : "organization!findAll.action?companyNo="+p_companyNo,
						data:function(n){
					    	  return {'parentOrgNo':n.attr ? n.attr("orgNo").replace("node_",""):0};//types is in action
						},
						cache: false}
				},
				plugins : ["themes", "json_data", "ui", "crrm"]

			})
			//.bind('loaded.jstree', function(e,data){data.inst.open_all(-1);})
			.bind('select_node.jstree',function(e,data){
				
				callback(e,data);
				
				$(windowId).dialog('close');
			});
			
		},
		/**
		 * 选择机构
		 * namePut 选中机构（name）的赋值ID
		 * idPut 选中机构（id）的赋值ID
		 * companyNo 公司ID
		 */
		selectORG:function(namePut,idPut,companyNo){
			var _companyNo='-1';
			if(companyNo!=null && companyNo!=''){
				_companyNo=companyNo;
			}
			wstuo.orgMge.organizationTreeUtil.showSelectOrgs("#index_selectORG_window","#index_selectORG_tree",_companyNo,function(e,data){
				$(namePut).val(data.rslt.obj.attr("orgName"));
				$(idPut).val(data.rslt.obj.attr("orgNo"));
			});
		},
		/**
		 * 选择所属组
		 * @param showId 显示编号
		 * @param companyNo 公司编号
		 */
		selectBelongsGroup:function(showId,companyNo){
			var _companyNo='-1';
			if(companyNo!=null && companyNo!=''){
				_companyNo=companyNo;
			}
			var _content = '<div id="belongsGroup_[id]">[name]&nbsp;&nbsp;'+
				'<input type="hidden" name="userDto.belongsGroupIds" value="[id1]" />'+
				'<a style="color:red;" onclick="wstuo.orgMge.organizationTreeUtil.belongsGroupRemove([id2])">X</a><div>';
			wstuo.orgMge.organizationTreeUtil.showSelectOrgs("#index_selectORG_window","#index_selectORG_tree",_companyNo,function(e,data){
				var orgName = data.rslt.obj.attr("orgName");
				var orgId = data.rslt.obj.attr("orgNo");
				var orgType = data.rslt.obj.attr("orgType");
				if($('#belongsGroup_'+orgId).html()==null && (orgType!=='innerPanel' && orgType!=='servicePanel' && orgType!=='ROOT')){
					$('#'+showId).append(
							_content.replace('[id]',orgId)
							.replace('[id1]',orgId)
							.replace('[id2]',orgId)
							.replace('[name]',orgName));
				}
			});
		},
		/**
		 * 移除所属组
		 * @param id  编号
		 */
		belongsGroupRemove:function(id){
			$('#belongsGroup_'+id).remove();
		},
		//
		/**
		 * 直接返回选中的数据集
		 * @param companyNo 机构编号
		 * @param callback  回调函数
		 */
		selectGroup:function(companyNo,callback){
			var _companyNo='-1';
			if(companyNo!=null && companyNo!=''){
				_companyNo=companyNo;
			}
			var _content = '<div id="belongsGroup_[id]">[name]&nbsp;&nbsp;'+
				'<input type="hidden" name="userDto.belongsGroupIds" value="[id1]" />'+
				'<a style="color:red;" onclick="wstuo.orgMge.organizationTreeUtil.belongsGroupRemove([id2])">X</a><div>';
			wstuo.orgMge.organizationTreeUtil.showSelectOrgs("#index_selectORG_window","#index_selectORG_tree",_companyNo,function(e,data){
				callback(e,data);
			});
		}
	};
}();