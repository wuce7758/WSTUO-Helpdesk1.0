$package('wstuo.rules');


/**  
 * @author WSTUO 
 * @constructor callBusinessRule
 * @description 规则列表主函数.wstuo/rules/ruleMain.jsp

 */
wstuo.rules.ruleMain = function(){

	this.ruleValueId=null;
	this.ruleNameId=null;

	return {
			/**
			 * @description 系统数据类型格式化
			 * @param cell 当前列值
			 * @param event 事件
			 * @param data 行数据
			 * @private
			 */
			dataFlagFormatter:function(cell,event,data){
				
				if(data.dataFlag==1){
					return "<span style='color:#ff0000'>[System]</span>&nbsp;"+data.ruleName;
				}else{
					return data.ruleName;
				}
			},
			isToFlagName:function(cellvalue, options, rowObject){
				  if(cellvalue=="saveRequest" && rowObject.dataFlag == 1){
					  return "<span style='color:#ff0000'>[System]</span>&nbsp;"+i18n.lable_Added_triggered_request;
				  }else if(cellvalue=="saveRequest" && rowObject.dataFlag == 0){
					  return i18n.lable_Added_triggered_request;
				  }else if(cellvalue=="saveChange" && rowObject.dataFlag == 1){
					  return "<span style='color:#ff0000'>[System]</span>&nbsp;"+i18n.lable_When_the_change_is_triggered_new;
				  }else if(cellvalue=="saveChange" && rowObject.dataFlag == 0){
					  return i18n.lable_When_the_change_is_triggered_new;
				  }else if(cellvalue=="requestProce"){
					  return i18n.lable_Request_process_flow_trigger;
				  }else if(cellvalue=="changeProce"){
					  return i18n.lable_Circulation_trigger_change_process;
				  }else if(cellvalue=="" && rowObject.dataFlag == 1){
					  return "<span style='color:#ff0000'>[System]</span>&nbsp;"+i18n.system_email_to_request;
				  }else if(cellvalue=="" && rowObject.dataFlag == 0){
					  return i18n.custom_email_to_request;
				  }
			},
			/**
			 * @description 加载规则列表.
			 */
			showRulesGrid:function(){

				var _url='callBusinessRule!findRules.action';
				if(_flag=="requestProce" || _flag=="requestFit"){
					_url=_url+'?flagName=requestProce,saveRequest';
				}else if(_flag=="changeApproval" || _flag=="changeProce"){
					_url=_url+'?flagName=saveChange,changeProce';
				}else{
					_url=_url+'?rulePackageNo='+_rulePackageNo
				}
				var params = $.extend({},jqGridParams, {	
					url:_url,
					colNames:[i18n['common_name'],i18n['type'],i18n['rule_salience'],i18n['common_desc'],'','',''],
					colModel:[
						       {name:'ruleNamePanel',index:'ruleName',width:30,formatter:wstuo.rules.ruleMain.dataFlagFormatter},
						       {name:'flagName',index:'dataFlag',align:'center',width:50,formatter:wstuo.rules.ruleMain.isToFlagName},
						       {name:'salience',width:20,align:'center'},
						       {name:'description',width:50,sortable:false},
						       {name:'ruleNo',hidden:true},
						       {name:'dataFlag',hidden:true},
						       {name:'ruleName',hidden:true}
						      
						],
					jsonReader: $.extend(jqGridJsonReader, {id: "ruleNo"}),
					sortname:'ruleNo',
					pager:'#rulesGridPager'
				});
				$("#rulesGrid").jqGrid(params);
				$("#rulesGrid").navGrid('#rulesGridPager',navGridParams);
				//列表操作项
				$("#t_rulesGrid").css(jqGridTopStyles);
				$("#t_rulesGrid").html($('#rulesGridToolbar').html());
			
				setGridWidth("#rulesGrid",2);
				
			},
			
			/**
			 * @description 转到新增规则页面.
			 */
			showAddRule:function(){

				var titilName='';
				var titilNames=new Array();
				if(_flag=="requestFit" || _flag=="requestProce"){
					titilName=i18n.label_rule_addRequestRule;
				}else if(_flag=="mailToRequest"){
					titilName=i18n.label_rule_addEmailToRequestRule;
				}else if(_flag=="changeApproval" || _flag=="changeProce"){
					titilName=i18n.label_rule_addChangeRule;
				}
				
				titilNames[0]=i18n.label_rule_addRequestRule;
				titilNames[1]=i18n.label_rule_addEmailToRequestRule;
				titilNames[2]=i18n.label_rule_addChangeRule;
				titilNames[3]=i18n.label_rule_editRequestRule;
				titilNames[4]=i18n.label_rule_editEmailToRequestRule;
				titilNames[5]=i18n.label_rule_editChangeRule;
				titilNames[6]=i18n.title_sla_slaDetail;
				for(var i=0;i<7;i++){
			
				}
				
				basics.index.initContent("../pages/wstuo/rules/addRule.jsp?rulePackageNo="+_rulePackageNo+"&flag="+_flag);
			
			},
			/**
			 * @description 打开SLA窗口
			 * @param panelDiv 面板ID
			 * @param valueId 选中返回值(ID)赋值ID
			 * @param nameId 选中返回值(Name)赋值ID
			 */
			select_sla_openWin:function(panelDiv,valueId,nameId){
				
				ruleValueId=valueId;
				ruleNameId=nameId;
				var orgNo=new Array();
				var dirNo = new Array();
				var sign = "";
				var constraintsNames = $(panelDiv+' form input[name="rule_condition_constraints_propertyName"]');
				var constraintsValues = $(panelDiv+' form input[name="rule_condition_constraints_propertyValue"]');
				var constraintsSign = $(panelDiv+' form select[name="rule_condition_constraints_orAnd"]');
				var orgIndex=-1;
				var dirIndex=-1;
				for(i =0; i<constraintsNames.length; i++){
					var j;
					if(orgNo.length>0){
						j=orgNo.length-1;
					}else{
						j=0;
					}
					//请求人所属机构
					if(constraintsNames[i].value.indexOf("organizationNo")>=0){
							orgNo[j]=constraintsValues[i].value;
							orgIndex = i;
					}
					//关联的服务
					var h;
					if(dirNo.length>0){
						h=dirNo.length-1;
					}else{
						h=0;
					}
					if(constraintsNames[i].value.indexOf("serviceDirIdsStr")>=0 || constraintsNames[i].value.indexOf("serviceDirIds")>=0){
						dirNo[h]=constraintsValues[i].value;
						dirIndex= i;
					}
				}
				
				if(dirIndex!=-1&&orgIndex!=-1){
					if(dirIndex<orgIndex){
						sign = constraintsSign[dirIndex].value;
					}else{
						sign = constraintsSign[orgIndex].value;
					}
				}
				wstuo.rules.ruleMain.selectSlaGrid(dirNo,orgNo,valueId,nameId,sign);
				windows('selectMatchRule');
			},
			/**
			 * @description 系统数据类型格式化
			 * @param cell 当前列值
			 * @param event 事件
			 * @param data 行数据
			 * @private
			 */
			dataFlagFormatterCon:function(cell,event,data){
				
				if(data.dataFlag==1){
					return "<span style='color:#ff0000'>[System]</span>&nbsp;"+data.contractName;
				}else{
					return data.contractName;
				}
			},
			/**
			 * @description 添加SLA 规则操作
			 * @param id SLA规则ID
			 * @param name SLA规则名称
			 * @private
			 */
			add_sla_rule_opt:function(id,name){
				$(ruleValueId).val(id);
				$(ruleNameId).val(name);
				$('#selectMatchRule').dialog('close');
			},
			/**
			 * @description 选择SLA规则
			 * @param id 选中返回值(ID)赋值ID
			 * @private
			 */
			select_sla_rule_aff_multi:function(id){
				var rowData = $('#selectMatchRuleGrid').getRowData(id);
				$(ruleValueId).val(rowData.contractNo);
				$(ruleNameId).val(rowData.contractName);
				$('#selectMatchRule').dialog('close');
			},
			/**
			 * @description 选择SLA
			 * @param dirNo 服务目录IDS
			 * @param orgNo 机构ID
			 * @param valueId 选中返回值(id)赋值ID
			 * @param nameId 选中返回值(name)赋值ID
			 * @param sign 标识
			 */
			selectSlaGrid:function(dirNo,orgNo,valueId,nameId,sign){
				var _url=''; 
				if(sign == "or"){
					if(orgNo!=null && orgNo.length>0){
						var _orgs = orgNo[0].split(",");
						var param = $.param({'qdto.orgNos':_orgs},true);
						_url='slaContractManage!find.action?'+param;
						
					}else{
						var param = $.param({'qdto.orgNos':0},true);
						_url='slaContractManage!find.action?'+param;
					}
				}else{
					if(orgNo!=null && orgNo.length>0){
						if(dirNo!=null && dirNo.length>0){
							var _orgs = orgNo[0].split(",");
							var _dirs = dirNo[0].split(",");
							var param = $.param({'qdto.orgNos':_orgs,'qdto.dirNos':_dirs},true);
							_url='slaContractManage!find.action?'+param;
						}else{
							var _orgs = orgNo[0].split(",");
							var param = $.param({'qdto.orgNos':_orgs},true);
							_url='slaContractManage!find.action?'+param;
						}
					}else{
						var param = $.param({'qdto.orgNos':0},true);
						_url='slaContractManage!find.action?'+param;
					}
				}
				
					
				if($("#selectMatchRuleGrid").html()==''){
					var params = $.extend({},jqGridParams, {
						url:_url,
						caption:'',
						colNames:[i18n['title_sla_name'],i18n['title_sla_org'],i18n['title_sla_version'],i18n['title_sla_startTime'],i18n['title_sla_endTime'],i18n['common_action'],'','','',''],
						colModel:[{name:'contractNamePanel',index:'contractName',width:140,align:'center',formatter:wstuo.rules.ruleMain.dataFlagFormatterCon},
								  {name:'serviceOrgName',width:100,align:'center',sortable:false,hidden:true},
								  {name:'versionNumber',width:100,align:'center',hidden:true},
								  {name:'beginTime',width:130,align:'center',formatter:timeFormatter},
								  {name:'endTime',width:130,align:'center',formatter:timeFormatter},
								  {name:'act',width:90,align:'center',sortable:false,formatter:function(cell,event,data){
									  return '<div style="padding:0px">'+
										'<a href="javascript:wstuo.rules.ruleMain.add_sla_rule_opt(\''+(data.contractNo)+'\',\''+data.contractName+'\')" title="'+i18n['check']+'">'+
										'<img src="../images/icons/ok.png"/></a>'+
										'</div>';
			                      }},
								  {name:'agreement' ,hidden:true},
								  {name:'rulePackageNo',hidden:true},
								  {name:'contractNo',hidden:true},
								  {name:'contractName',hidden:true}
					   	],		
					   	ondblClickRow:function(rowId){wstuo.rules.ruleMain.select_sla_rule_aff_multi(rowId)},
						jsonReader: $.extend(jqGridJsonReader, {id: "contractNo"}),
						sortname:'contractNo',
						toolbar:[false,"top"],
						multiselect: false,
						pager:'#selectMatchRulePager'
							
					});
					
					$("#selectMatchRuleGrid").jqGrid(params);
					$("#selectMatchRuleGrid").navGrid('#selectMatchRulePager',navGridParams);
					//列表操作项
					$("#t_selectMatchRuleGrid").css(jqGridTopStyles);
				}else{
					$("#t_selectMatchRuleGrid").html('');
					$("#selectMatchRuleGrid").jqGrid('setGridParam',{url:_url}).trigger('reloadGrid');
				}
			},
			/**
			 * @description 转到编辑规则页面.
			 */
			showEditRule:function(){
		
				var titilName='';
				var titilNames=new Array();
				if(_flag=="requestFit" || _flag=="requestProce"){
					titilName=i18n.label_rule_editRequestRule;
				}else if(_flag=="mailToRequest"){
					titilName=i18n.label_rule_editEmailToRequestRule;
				}else if(_flag=="changeApproval" || _flag=="changeProce"){
					titilName=i18n.label_rule_editChangeRule;
				}
				titilNames[0]=i18n.label_rule_addRequestRule;
				titilNames[1]=i18n.label_rule_addEmailToRequestRule;
				titilNames[2]=i18n.label_rule_addChangeRule;
				titilNames[3]=i18n.label_rule_editRequestRule;
				titilNames[4]=i18n.label_rule_editEmailToRequestRule;
				titilNames[5]=i18n.label_rule_editChangeRule;
				titilNames[6]=i18n.title_sla_slaDetail;
				
				for(var i=0;i<7;i++){
					//basics.tab.tabUtils.closeTab(titilNames[i]);
				}
				
				checkBeforeEditGrid("#rulesGrid",function(rowData){
					var ruleName = rowData.ruleNamePanel;
					if(ruleName.indexOf('[System]') != -1){
						msgAlert(i18n['ERROR_SYSTEM_DATA_CAN_NOT_EDIT'],'info');//
					}else{
						
						basics.index.initContent("callBusinessRuleSet!findRule.action?ruleNo="+rowData.ruleNo+"&packageNo="+_rulePackageNo+"&flag="+_flag);
						
					}			
				});
				
			},
			
			
			/**
			 * @description 删除规则.
			 */
			delRules:function(){
				checkBeforeDeleteGrid('#rulesGrid',function(rowIds){
					var arr = $("#rulesGrid").jqGrid("getRowData",rowIds);
					if(arr.dataFlag === "1"){
						msgShow(i18n['msg_canNotDeleteSystemData'],'warning');
						return false;
					}
					var _param = $.param({'ruleNos':rowIds},true);
					$.post("callBusinessRule!delete.action",_param,function(rs){
						if(rs){
							$("#rulesGrid").trigger('reloadGrid');
							
							msgShow(i18n['msg_deleteSuccessful'],'show');
							
						}else{
							msgAlert(i18n['msg_canNotDeleteSystemData'],'info');	
						}	
		
					},"json");
					
				});
			},
			
			/**
			 * @description 打开搜索框
			 * */
			openSearchRules:function(){
				windows('searchRuleDiv',{width:400,modal: true});
				
			},	
			
			/**
			 * @description 搜索
			 * */
			doSearchRules:function(){
				var sdata = $('#searchRuleDiv form').getForm();
				var postData = $("#rulesGrid").jqGrid("getGridParam", "postData");       
				$.extend(postData, sdata);  //将postData中的查询参数覆盖为空值
				
				var _url = 'callBusinessRule!find.action';
				$('#rulesGrid').trigger('reloadGrid');
				
			},
			
			
			
			/**
			 * @description 导出数据.
			 */
			exportRules:function(){
		
				window.location='callBusinessRule!exportRules.action?rulePackageNo='+_rulePackageNo+'&fileName='+_flag;
			},
			
			/**
			 * @description 执行导入.
			 */
			doImport:function(){
		
				$.ajaxFileUpload({
		            url:'callBusinessRule!importRules.action?rulePackageNo='+_rulePackageNo,
		            secureuri:false,
		            fileElementId:'importFile_drl', 
		            dataType:'json',
		            success: function(data,status){
					
						$('#index_import_drl_window').dialog('close');
						$('#rulesGrid').trigger('reloadGrid');
						resetForm('#index_import_drl_window form');
						
						if(data=="success"){
			            	msgShow(i18n['msg_dc_dataImportSuccessful'],'show');
						}else{
							msgShow(i18n['msg_dc_importFailure'],'show');
						}
						
		            }
		      });
			},
			/**
			 * @description 确认选择
			 */
			clickCommon:function(){
				var strs= new Array(); //定义一数组
				var rowIds="";
				var rowValue="";
				rowIds= $("#selectCompanySLA_grid").getGridParam('selarrrow')+"";
				if(rowIds.length>0){
					strs=rowIds.split(",");
					for (i=0;i<strs.length ;i++ ) {
						var rowData=$("#selectCompanySLA_grid").getRowData(strs[i]);
						rowValue+=rowData.orgName+",";
					}
					rowValue=rowValue.substring(0,rowValue.length-1);
					$('#addRule_propertyValue').val(rowIds);
					$('#editRule_propertyValue').val(rowIds);
					$('#addSLARule_propertyValue').val(rowIds);
					
					$('#addRule_namePut').val(rowValue);
					$('#editRule_namePut').val(rowValue);
					$('#slaRule_propertyName').val(rowValue);
				}
				$('#selectCompanyDiv').dialog('close');
			},
			/**
			 * @description 确认选择
			 */
			clickCommonUser:function(){
				$('#addRule_propertyValue').val(companyNo);
				$('#editRule_propertyValue').val(companyNo);
				$('#addSLARule_propertyValue').val(companyNo);
				
				$('#addRule_namePut').val(companyName);
				$('#editRule_namePut').val(companyName);
				$('#slaRule_propertyName').val(companyName);
				
				$('#selectCompanyDiv').dialog('close');
				
			},

			
			/**
			 * 规则
			 */	
			showRulePackage:function(){
			
				$.get("rulePackage!findListRulePackage.action", function(data) {				
					$.each(data,function(){
						if(_flag =="requestFit" && this.packageName == 'com.drools.drl.helpDeskRequest'){
							_rulePackageNo =this.rulePackageNo;
						}else if(_flag=="mailToRequest"){
							_rulePackageNo =this.rulePackageNo;
						}	
					});	

					if(_rulePackageNo =='' || _rulePackageNo ==null){
						
					}else{
						wstuo.rules.ruleMain.showRulesGrid();	
					}
 
				});	
			},
			
			/**
			 * @description 初始化
			 */
			init:function(){
				wstuo.rules.ruleMain.showRulePackage();
			
				//wstuo.rules.ruleMain.showRulesGrid();	
		
				//为添加按钮增加事件
				$("#leftbtn").click(function (){
			
					//获取选择的值
					$("#leftop option:selected").each(function (i){								
						//在右边添加所选值，并且添加之后在左边删除所选值
						alert(this.value);
						$("#rightop").append("<option value='"+this.value+"'>"+this.text+"</option>");  		 
					}).remove();
					
				});		
				
				//为删除按钮增加事件
				$("#rightbtn").click(function (){
		
					//获取所选择的值
					$("#rightop option:selected").each(function (i){		
						//在左边添加所选值，并且添加之后在右边删除所选值
						alert(this.value);
						$("#leftop").append("<option value='"+this.value+"'>"+this.text+"</option>"); 
					}).remove();
				});
			
				//增加删除按钮事件
				$("#del").click(function (){						
					//获取要删除的值
					$("#rightop option:selected,#leftop option:selected").each(function (i){		
						//删除所选值
						$(this).remove();	 
					});	
				});
					
				
				$('#selectCompany_grid_select').unbind().click(wstuo.rules.ruleMain.clickCommon);

				$('#selectCompany_grid_select_User').unbind().click(wstuo.rules.ruleMain.clickCommonUser);
				
				$('#requestRuleExport').click(wstuo.rules.ruleMain.exportRules);//导出
				
				//导入数据
				$('#requestRuleImport').click(function(){
					windows('index_import_drl_window',{width:400});
					$("#index_import_drl_confirm").unbind(); //清空事件
					$('#index_import_drl_confirm').click(wstuo.rules.ruleMain.doImport);
				});			                     
				$('#rulesGrid_doSearch').click(wstuo.rules.ruleMain.doSearchRules);
			}
	
		};
}();

$(document).ready(wstuo.rules.ruleMain.init);

