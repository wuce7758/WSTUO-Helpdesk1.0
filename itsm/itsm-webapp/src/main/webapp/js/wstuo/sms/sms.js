$package('wstuo.sms');
 /**  
 * @author Wstuo  
 * @constructor sms
 * @description 发送短信主函数.
 * @since version 1.0 
 */  
wstuo.sms.sms = function() {

	return {
		
		/**
		 * @description 查询余额.
		 */
		queryMoney:function(){
			
			var url = 'sms!queryMoney.action';
			$.post(url,function(res)
			{
				$('#accountMsg').empty().html(i18n['label_sms_accountName']+':'+res.id+";<br/>"+i18n['label_sms_accountMoney']+':'+res.totalMoney.replace("_yuan",i18n['common_yuan']).replace("_tiao",i18n['common_tiao']));
			});
		},
		
		/**
		 * @description 转到查看短信历史记录
		 */
		showHistoryRecord:function(){
			
			basics.index.initContent("../pages/wstuo/sms/smsRecord.jsp");
			
		},
		
		/**
		 * @description 验证手机号码
		 */
		checkMobile:function(){
			
			var phones=$('#mobarea').val();
			var arrstr=phones.replace(/\n/g,'---'); 
			var arr=arrstr.split("---");
			
			//var ck = /^1[3,5,8]\d{9}\;$/;
			var ck = /^(0|86|17951)?(13[0-9]|15[0-9]|17[0-9]|18[0-9]|14[0-9])[0-9]{8}\;$/;
	    	for(var i=0;i<arr.length;i++){
	    		if(!arr[i].match(ck)){
	    			msgAlert(i18n["msg_msg_hasWrongPhoneNumber"]+"["+arr[i]+"]","info");
	    			return false;
	    		}
	    	}
	    	return true;
		},
		/**
		 * @description 发送短信.
		 */
		sendSms:function (){
			if(wstuo.sms.sms.checkMobile()){
				startProcess();
				$.post("sms!testfindSMSAccount.action",function(res){
					var msg='';
					if(res==true || res=='true'){	
						//赋值
						$('#mobiles').val($('#mobarea').val());
						var url = 'sms!sendSMS.action';
						var frm = $('#SMSForm').serialize();
						
						$.post(url,frm, function(res){
							var msg="";
							
							if(res.stateMsg!=null && res.stateMsg.indexOf("发送成功")>0){
								msg+=i18n["msg_im_imSendSuccessful"];
							}
							
							if(res.sendCount!=null){
								msg+="<br/>"+i18n['msg_sms_sended']+" "+res.sendCount+" "+i18n['common_tiao'];
							}
							
							/*
							if(res.costMoney!=null){
								msg+="<br/>"+i18n['msg_sms_takeMoney']+" "+res.costMoney.replace("_yuan",i18n['common_yuan']).replace("_tiao",i18n['common_tiao']);
							}
							*/
							if(res.totalMoney!=null){
								msg+="<br/>"+i18n['label_sms_accountMoney']+"： "+res.totalMoney.replace("_yuan",i18n['common_yuan']).replace("_tiao",i18n['common_tiao']);
							}
							endProcess();
							//show(i18n['msg_msg'],msg,'show',10000);
							msgShow(msg,'show');
							//清空
							resetForm("#smsForm");
							resetForm("#phoneNumberForm");
							
							//重新计算
							wstuo.sms.sms.queryMoney();
							
						});				
					}else{
						msg=i18n['msg_sms_failure'];
					}
					endProcess();
					msgShow(msg,'show');
				});
			}
		}
	};
}();