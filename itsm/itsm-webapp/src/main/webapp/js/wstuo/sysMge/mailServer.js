$package('wstuo.sysMge')

$import('wstuo.sysMge.base64Util');

/**  
 * @author WSTUO 
 * @constructor mailServer
 * @description this script is for "pages/security/company.jsp"

 */  
wstuo.sysMge.mailServer = function() {
	
	return {
		/**
		 * 格式化值
		 */
		inputSetValue:function(value){
			if(value==null || value=='')
				return '';
			else
				return value;
		},
		/**
		 * 查询发送邮箱邮件
		 */
		findServiceMail:function(){
			var url = 'mailServer!findServereMail.action';
			
			$.post(url,function(res){
				if(res.emailType=='normal'||res.emailType=='exchange'){
				$('#smtp_serverAddress').val($vl(wstuo.sysMge.mailServer.inputSetValue(res.smtpServerAddress)));
				$('#smtp_serverPort').val($vl(wstuo.sysMge.mailServer.inputSetValue(res.smtpServerPort)));
				$('#pop3_serverAddress').val($vl(wstuo.sysMge.mailServer.inputSetValue(res.pop3ServerAddress)));
				$('#pop3_serverPort').val($vl(wstuo.sysMge.mailServer.inputSetValue(res.pop3ServerPort)));
				$('#userName').val($vl(wstuo.sysMge.mailServer.inputSetValue(res.userName)));
				var pwd = $vl(wstuo.sysMge.mailServer.inputSetValue(res.password));
				
				//if(res.emailType=='normal'){
					wstuo.sysMge.base64Util.setingPassword(pwd,'#company_password_hidden','');
					wstuo.sysMge.base64Util.setingPassword(pwd,'#company_password','#company_password_reality');
				//}else{
					//wstuo.sysMge.base64Util.setingPassword(pwd,'#ReceiveExchangePassword_hidden','');
					wstuo.sysMge.base64Util.setingPassword(pwd,'#exchangePassword','#exchangePassword_reality');
					setTimeout(function(){
						$('#exchangePassword_hidden').val($('#exchangePassword').val());
					},500);
					
					
					
				//}
				
				if(res.exchangeHostName!=null && res.exchangeHostName!="null"){
					$('#exchangeHostName').val(wstuo.sysMge.mailServer.inputSetValue(res.exchangeHostName));
				}else{
					$('#exchangeHostName').val(wstuo.sysMge.mailServer.inputSetValue(""));
				}
				if(res.exchangeUserName!=null && res.exchangeUserName!="null"){
					$('#exchangeUserName').val(wstuo.sysMge.mailServer.inputSetValue(res.exchangeUserName));
				}else{
					$('#exchangeUserName').val(wstuo.sysMge.mailServer.inputSetValue(""));
				}
				if(res.exchangePassword!=null && res.exchangePassword!="null"){
					wstuo.sysMge.base64Util.setingPassword(res.exchangePassword,'#exchangePassword','#exchangePassword_reality');
				}else{
					$('#exchangePassword_reality').val(wstuo.sysMge.mailServer.inputSetValue(""));
				}
				if(res.domain!=null && res.domain!="null"){
					$('#exchangeDomain').val(wstuo.sysMge.mailServer.inputSetValue(res.domain));
				}else{
					$('#exchangeDomain').val(wstuo.sysMge.mailServer.inputSetValue(""));
				}
				if(res.exchangeEmailAccount!=null && res.exchangeEmailAccount!="null"){
					$('#exchangeEmailAccount').val(wstuo.sysMge.mailServer.inputSetValue(res.exchangeEmailAccount));
				}else{
					$('#exchangeEmailAccount').val(wstuo.sysMge.mailServer.inputSetValue(""));
				}
				
				if(res.personal!=null && res.personal!=""){
					$('#personal').val(wstuo.sysMge.mailServer.inputSetValue(res.personal));
				}else{
					$('#personal').val("");
				}
				$('#personalEmailAddress').val($vl(wstuo.sysMge.mailServer.inputSetValue(res.personalEmailAddress)));
				if(res.emailServerId!=null){
					$('#emailServerId').val(res.emailServerId);
				}
				if(res.emailType=='exchange'){
					$('#exchangeEmailRadio').attr('checked',true);
					$('#normalEmailSetting').hide();
					$('#exchangeEmailSetting').show();
					$('#emailConnTestLink').unbind();
					$('#emailConnTestLink').click(function(){exchangeEmailConnTest('server')});
					$('#company_mailserver_save').unbind();
					$('#company_mailserver_save').click(function(){mailserver('exchange')});
				}else{
					$('#normalEmailRadio').attr('checked',true);
					$('#normalEmailSetting').show();
					$('#exchangeEmailSetting').hide();
					$('#emailConnTestLink').unbind();
					$('#company_mailserver_save').unbind();
					$('#company_mailserver_save').click(function(){mailserver('normalEmail')});

					$('#emailConnTestLink').click(function(){emailConnTest()});
				}
				if(res.emailVersion!=null && res.emailVersion != "null"){
					$("#exchangeVersionSelect").val(res.emailVersion);
				}
				}else{
					$('#company_mailserver_save').unbind();
					$('#company_mailserver_save').click(function(){mailserver('normalEmail')});
				}
			});
		},
		
		/**
		 *查找接收邮箱
		 */
		findReceiveServiceMail:function(){
			var url = 'mailServer!findReceiveServiceMail.action';
			$.post(url,function(res){
				if(res.emailType=='Receive'||res.emailType=='Receive_exchange'){
				$('#Receive_Personal').val($vl(wstuo.sysMge.mailServer.inputSetValue(res.personal)));
				$('#ReceivePersonalEmailAddress').val($vl(wstuo.sysMge.mailServer.inputSetValue(res.personalEmailAddress)));
				$('#pop3_ReceiveServerAddress').val($vl(wstuo.sysMge.mailServer.inputSetValue(res.pop3ServerAddress)));
				$('#pop3_ReceiveServerPort').val($vl(wstuo.sysMge.mailServer.inputSetValue(res.pop3ServerPort)));
				$('#ReceiveUserName').val($vl(wstuo.sysMge.mailServer.inputSetValue(res.userName)));
				$('#company_receive_password_reality').val('');
				$('#company_receive_password').val('');
				var pwd = $vl(wstuo.sysMge.mailServer.inputSetValue(res.password));
				
			//	if(res.emailType=='Receive'){
					wstuo.sysMge.base64Util.setingPassword(pwd,'#password_hidden','');
					wstuo.sysMge.base64Util.setingPassword(pwd,'#company_receive_password','#company_receive_password_reality');
				//}else{
					wstuo.sysMge.base64Util.setingPassword(pwd,'#ReceiveExchangePassword','#ReceiveExchangePassword_reality');
					setTimeout(function(){
						$('#ReceiveExchangePassword_hidden').val($('#ReceiveExchangePassword').val());
					},500);
		
			//	}
					
				if(res.exchangeHostName!=null && res.exchangeHostName!="null"){
					$('#ReceiveExchangeHostName').val(wstuo.sysMge.mailServer.inputSetValue(res.exchangeHostName));
				}else{
					$('#ReceiveExchangeHostName').val(wstuo.sysMge.mailServer.inputSetValue(""));
				}
				if(res.exchangeUserName!=null && res.exchangeUserName!="null"){
					$('#ReceiveExchangeUserName').val(wstuo.sysMge.mailServer.inputSetValue(res.exchangeUserName));
				}else{
					$('#ReceiveExchangeUserName').val(wstuo.sysMge.mailServer.inputSetValue(""));
				}
				if(res.exchangePassword!=null && res.exchangePassword!="null"){
					wstuo.sysMge.base64Util.setingPassword(res.exchangePassword,'#ReceiveExchangePassword','#ReceiveExchangePassword_reality');
				}else{
					$('#ReceiveExchangePassword').val(wstuo.sysMge.mailServer.inputSetValue(""));
				}
				if(res.domain!=null && res.domain!="null"){
					$('#ReceiveExchangeDomain').val(wstuo.sysMge.mailServer.inputSetValue(res.domain));
				}else{
					$('#ReceiveExchangeDomain').val(wstuo.sysMge.mailServer.inputSetValue(""));
				}
				if(res.exchangeEmailAccount!=null && res.exchangeEmailAccount!="null"){
					$('#ReceiveExchangeEmailAccount').val(wstuo.sysMge.mailServer.inputSetValue(res.exchangeEmailAccount));
				}else{
					$('#ReceiveExchangeEmailAccount').val(wstuo.sysMge.mailServer.inputSetValue(""));
				}
				if(res.personal!=null && res.personal!=""){
					$('#Receive_Personal').val(wstuo.sysMge.mailServer.inputSetValue(res.personal));
				}else{
					$('#Receive_Personal').val("");
				}
				if(res.emailServerId!=null){
					$('#ReceiveEmailServerId').val(res.emailServerId);
				}
				if(res.emailVersion!=null && res.emailVersion != "null"){
					$("#ReceiveExchangeVersionSelect").val(res.emailVersion);
				}
				if(res.emailType=='Receive_exchange'){
					$('#ReceiveExchangeEmailRadio').attr('checked',true);
					$('#ReceiveEmailSetting').hide();
					$('#ReceiveExchangeEmailSetting').show();
					$('#ReceiveEmailConnTestLink').unbind();
					$('#ReceiveEmailConnTestLink').click(function(){exchangeEmailConnTest('receive')});
					$('#company_receive_mailserver_save').unbind();
					$('#company_receive_mailserver_save').click(function(){Receive('Receive_exchange')});
				}else{
					$('#ReceivelEmailRadio').attr('checked',true);
					$('#ReceiveEmailSetting').show();
					$('#ReceiveExchangeEmailSetting').hide();
					$('#ReceiveEmailConnTestLink').unbind();
					$('#ReceiveEmailConnTestLink').click(function(){ReceiveEmailConnTest()});
					$('#company_receive_mailserver_save').unbind();
					$('#company_receive_mailserver_save').click(function(){Receive('Receive')});
				}
				
				
				}else{
					$('#company_receive_mailserver_save').unbind();
					$('#company_receive_mailserver_save').click(function(){Receive('Receive')});
				}

			});
		},
		
		
		
		/**
		 * 保存发送邮箱配置信息
		 */
		saveOrUpdateMailServer:function(){
			var server = $('#radio_value').val();
			
				if(server=='exchange'){
					wstuo.sysMge.base64Util.encodePassword('#exchangePassword','#exchangePassword_reality','#exchangePassword_hidden');
				}else{
					wstuo.sysMge.base64Util.encodePassword('#company_password','#company_password_reality','#company_password_hidden');
				}
				var _params = $('#MailServerDiv form').serialize();
				var url = 'mailServer!saveOrUpdateEmailServer.action';
				//显示进程
				startProcess();
				 $.post(url,_params, function(){
						//隐藏进程
						endProcess();
						//wstuo.sysMge.mailServer.findServiceGuide();
					 	msgShow(i18n['saveSuccess'],'show');
						wstuo.sysMge.mailServer.findServiceMail();						
				});		
			
		},
		/**
		 * 保存接收邮箱配置信息
		 */
		saveOrUpdateReceiveMailServer:function(test){
			var test = $('#ReceivelRadio').val();
			//if($('#ReceiveMailServerForm').form('validate')){
				if(test=='Receive_exchange'){
					wstuo.sysMge.base64Util.encodePassword('#ReceiveExchangePassword','#ReceiveExchangePassword_reality','#ReceiveExchangePassword_hidden');
				}else{
					wstuo.sysMge.base64Util.encodePassword('#company_receive_password','#company_receive_password_reality','#password_hidden');
				}
				if($("#company_receive_password").val()==''){
					$("#company_receive_password_reality").val('')			
				}
				var _params = $('#ReceiveMailServerDiv form').serialize();
				var url = 'mailServer!saveOrUpdateEmailServer.action';
				//显示进程
				startProcess();
				 $.post(url,_params, function(){
						//隐藏进程
						endProcess();
				 	    msgShow(i18n['saveSuccess'],'show');
						wstuo.sysMge.mailServer.findReceiveServiceMail();
						
				});
			//}
			
		},
		/**
		 * 初始化
		 */
		init:function(){
			$('#ReceiveEmailConnTestLink').click(function(){ReceiveEmailConnTest()});
			$('#emailConnTestLink').click(function(){emailConnTest()});
			wstuo.sysMge.mailServer.findServiceMail();
			wstuo.sysMge.mailServer.findReceiveServiceMail();
			//$('#company_receive_mailserver_save').click(wstuo.sysMge.mailServer.saveOrUpdateReceiveMailServer);
			//$('#company_mailserver_save').click(wstuo.sysMge.mailServer.saveOrUpdateMailServer);
			
		}
	};

}();
//载入
$(document).ready(wstuo.sysMge.mailServer.init);
	