<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@ include file="../language.jsp" %>
     

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>配置项选择includes文件</title>
</head>
<body>
<!-- 联系管理员-->
<div id="welcome_tab_contract_admin_window" title="<fmt:message key="label_welcome_page_ContactSystemAdmin" />" class="WSTUO-dialog" style="width:400px;height:150px;">
<form>
   <div class="lineTableBgDiv" >
	   <table  class="lineTable"  width="100%" cellspacing="1"> 
	   		
	   		
	   		 <tr>
	            <td width="100px"><fmt:message key="label.ContactPer" /></td>
	            <td>
	            <span id="contract_fullName"></span>
	            </td>
	        </tr>
	   		
	        <tr>
	            <td width="100px"><fmt:message key="label.UserLandline" /></td>
	            <td>
	            <span id="contract_phone"></span>
	            </td>
	        </tr>
	        
	         <tr>
	            <td width="100px"><fmt:message key="label.user.mobile" /></td>
	            <td>
	            <span id="contract_mobile"></span>
	            </td>
	        </tr>
	        
	         <tr>
	            <td width="100px"><fmt:message key="label.user.email" /></td>
	            <td>
	            <span id="contract_email"></span>
	            </td>
	        </tr>
			
	    </table>
    </div>
   </form>
</div>
<!-- 快速创建请求 -->
<div id="welcome_tab_fastcreate_window" class="WSTUO-dialog" title='<fmt:message key="label_welcome_page_LoginQuickCall" />' style="width:350px;height:auto">

<div class="lineTableBgDiv" id="companyInfoDiv" style="" >
   
<form>
<input type="hidden" name="requestDTO.technicianName" value="${loginUserName}"  />
<input type="hidden" name="requestDTO.createdByName" value="${loginUserName}"  />
<input type="hidden" name="requestDTO.creator" value="${loginUserName}"  />
<input type="hidden" name="requestDTO.companyNo"  id="request_fastCreate_companyNo" value="${companyNo}"/> 
<input type="hidden" name="requestDTO.piv.approvalNo" value="0" />
<input type="hidden" name="requestDTO.piv.eno"  />
<input type="hidden" name="requestDTO.piv.etitle"  />
<input type="hidden" name="requestDTO.piv.eventCode"  />
<input type="hidden" name="requestDTO.actionName" value="<fmt:message key="common.add" />"  />
 <table style="width:100%" class="lineTable" cellspacing="1">

	<tr>
		<td><fmt:message key="common.title" /></td>
		<td>
			<input name="requestDTO.etitle" id="welcome_etitle" class="input easyui-validatebox" required="true"/>
		</td>
	</tr>
	<tr>
		<td><fmt:message key="common.content" /></td>
		<td>
			<textarea  name="requestDTO.edesc" id="welcome_content" class="textarea easyui-validatebox" required="true"/></textarea>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<a class="easyui-linkbutton" id="welcome_tab_fastcreate_window_submit"  icon="icon-save"><fmt:message key="common.save" /></a>
		</td>
	</tr>
</table>
</form>
</div>
</div>


<!-- 搜索知识 -->
<div id="welcome_tab_search_kw_window" class="WSTUO-dialog" title='<fmt:message key="label.compass.searchKnowledge" />' style="width:350px;height:auto">
<div class="lineTableBgDiv" id="companyInfoDiv">
    <table style="width:100%" class="lineTable" cellspacing="1">

	<tr>
		<td><input class="input" id="welcome_rq_knowledge_fullSearchKeyWord" onfocus="javascript:common.compass.fullSearch.setFullSearchId('#welcome_rq_knowledge_fullSearchKeyWord')"></td>
	</tr>
	<tr>
		<td>
		<a class="easyui-linkbutton" id="welcome_rq_knowledge_fullSearch_submit" onclick="common.compass.fullSearch.searchKnowledge('#welcome_rq_knowledge_fullSearchKeyWord')"  icon="icon-search"><fmt:message key="common.search" /></a></td>
	</tr>
	</table>
</div>
</div>


<!-- 自助服务 -->
<div id="welcome_tab_self_service_window" class="WSTUO-dialog" title='<fmt:message key="label_welcome_page_SelfService"/>' style="width:350px;height:auto">
<div class="lineTableBgDiv" id="companyInfoDiv">
    <table style="width:100%" class="lineTable" cellspacing="1">

	<tr>
		<td style="height:80px;line-height:45px">
		<sec:authorize url="KNOWLEGEINFO_VIEWKNOWLEGE">
			<a class="easyui-linkbutton" onclick="open_search_kw_window()">
			<fmt:message key="label_welcome_page_SearchKnowledge" /></a>&nbsp;&nbsp;
		</sec:authorize>
		<sec:authorize url="/pages/request!saveRequest.action">
			<a class="easyui-linkbutton" onclick="welcome_createRequest()">
			<fmt:message key="label_welcome_page_CreateRequest" /></a>&nbsp;&nbsp;
		</sec:authorize>
		<sec:authorize url="/pages/request!findRequestPager.action">
			<a class="easyui-linkbutton" onclick="view_my_request()">
			<fmt:message key="label_welcome_page_ViewSearchRequest" /></a>&nbsp;&nbsp;
		</sec:authorize>
			<a class="easyui-linkbutton" onclick="openHelp()">
			<fmt:message key="label_welcome_page_ViewHelpCrads" /></a>&nbsp;&nbsp;
		</td>
	</tr>
	
</table>

</div>

</div>




<!-- CSV导入数据 -->
<div id="welcome_tab_import_ci_window"  class="WSTUO-dialog" title="<fmt:message key="label.dc.importFromCsv"/>" style="width:400px;height:auto">
<form>
   <div class="lineTableBgDiv" >
	   <table  class="lineTable"  width="100%" cellspacing="1"> 
	   		<!-- 所属客户  -->
				<tr <c:if test="${versionType!='ITSOP'}"><c:out value="style=display:none"></c:out></c:if>>
					<td><fmt:message key="label.belongs.client" />&nbsp;<span style="color:red">*</span></td>
					<td>
						<input type="hidden" id="welcome_tab_csv_import_companyNo" />
						<input id="welcome_tab_csv_import_companyName" class="easyui-validatebox input" required="true" readonly />
					</td>
				</tr>
	        <tr>
	            <td width="100px">
	            	<fmt:message key="label.dc.filePath"/>
	            </td>
	            <td>
	           		<input type="file" class="easyui-validatebox input" required="true" name="importFile" id="welcome_tab_import_ci_file" onchange="checkFileType(this,'csv')"/>
	            </td>
	        </tr>
			<tr>
	            <td colspan="2" align="center">
	            	 <c:choose>  
						   <c:when test="${lang eq 'en_US'}">
						   		<a href="../importFile/en/ConfigureItem.zip" target="_blank">[<fmt:message key="label.csv.template.download"></fmt:message>]</a>
						   </c:when>
						     
						   <c:otherwise>
						   		<a href="../importFile/ConfigureItem.zip" target="_blank">[<fmt:message key="label.csv.template.download"></fmt:message>]</a>
						   </c:otherwise>
					</c:choose>
	           		
	            </td>
	        </tr>
	        <tr>
	            <td colspan="2">
	                <div style="padding-top:8px;">
                	<a id="importCiCategoryBtn" class="easyui-linkbutton" onclick="doImportCI()"  icon="icon-import"><fmt:message key="label.dc.import"/></a>
	                </div>
	       		 </td>
	        </tr>
	    </table>
    </div>
   </form>
</div>
 <form action="ci!exportConfigureItem.action" method="post" id="export_ci_form">
		<div id="export_ci_values"></div>
	</form>
	
<!-- 快速创建请求 -->
<div id="welcome_tab_language_window" class="WSTUO-dialog" title='<fmt:message key="i18n.language" />' style="width:350px;height:auto">
<div class="lineTableBgDiv" >
   <table  class="lineTable"  width="100%" cellspacing="1"> 
   	<tr> <td>
                <fmt:message key="i18n.language"/>
            </td>
            <td>
                <select id="language_Select">
                    <option value="zh_CN">简体中文</option>
                    <option value="zh_TW">繁體中文</option>
                    <option value="en_US">English</option>
                    <option value="ja_JP">日本語</option>
                </select>
            </td>
       </tr>
       <tr>
            <td colspan="2">
                <div style="padding-top:8px;">
               	<a id="selectlanguageBtn" class="easyui-linkbutton"  icon="icon-ok"><fmt:message key="label.determine"/></a>
                </div>
       		 </td>
        </tr>
   </table>
 </div></div> 
</body>
</html>