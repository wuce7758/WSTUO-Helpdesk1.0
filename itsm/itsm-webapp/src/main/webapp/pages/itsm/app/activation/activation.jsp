<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%
response.setHeader("Cache-Control","no-store");
response.setHeader("Pragrma","no-cache");
response.setDateHeader("Expires",0);
%> 
<%@ include file="../../../language.jsp" %>

<c:set var="currentTime" value="<%=new java.util.Date().getTime()%>" />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<style>.tabs li{margin-left:5px}</style>
<link rel="stylesheet" href="${pageContext.request.contextPath}/styles/style.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/styles/comm.css">
<script src="${pageContext.request.contextPath}/scripts/jquery/jquery-1.4.2.min.js"></script>
<script src="${pageContext.request.contextPath}/scripts/jquery/jquery.bgiframe.min.js"></script>
<link rel="stylesheet" href="${pageContext.request.contextPath}/scripts/jquery/easyui/themes/icon.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/scripts/jquery/easyui/themes/default/easyui.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/scripts/jquery/jqgrid/redmond/jquery-ui-1.8.1.custom.css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/scripts/jquery/jqgrid/css/jqgrid.css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/scripts/jquery/fullcalendar-1.5/fullcalendar/fullcalendar.css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/scripts/jquery/uploadify/uploadify.css" /> 
<link rel="stylesheet" href="${pageContext.request.contextPath}/scripts/jquery/jm-calendar/css/jm-calendar.css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/scripts/jquery/contextmenu/jquery.contextmenu.css" />
<script src="${pageContext.request.contextPath}/scripts/i18n/i18n_${lang}.js"></script>
<script src="${pageContext.request.contextPath}/scripts/jquery/jquery.xml2json.js"></script>
<script src="${pageContext.request.contextPath}/scripts/jquery/easyui/plugins/jquery.form.js"></script>
<script src="${pageContext.request.contextPath}/scripts/jquery/easyui/plugins/jquery.layout.js"></script>
<script src="${pageContext.request.contextPath}/scripts/jquery/easyui/plugins/jquery.linkbutton.js"></script>
<script src="${pageContext.request.contextPath}/scripts/jquery/easyui/plugins/jquery.panel.js"></script>
<%-- autocomplete--%>
<script src="${pageContext.request.contextPath}/scripts/jquery/jquery-ui-1.8.2.custom/development-bundle/ui/jquery.ui.core.js"></script>
<script src="${pageContext.request.contextPath}/scripts/jquery/jquery-ui-1.8.2.custom/development-bundle/ui/jquery.ui.widget.js"></script>
<script src="${pageContext.request.contextPath}/scripts/jquery/jquery-ui-1.8.2.custom/development-bundle/ui/jquery.ui.mouse.js"></script>
<script src="${pageContext.request.contextPath}/scripts/jquery/jquery-ui-1.8.2.custom/development-bundle/ui/jquery.ui.position.js"></script>
<script src="${pageContext.request.contextPath}/scripts/jquery/jquery-ui-1.8.2.custom/development-bundle/ui/jquery.ui.autocomplete.js"></script>
<script src="${pageContext.request.contextPath}/scripts/jquery/jquery-ui-1.8.2.custom/development-bundle/ui/jquery.ui.datepicker.js"></script>
<script src="${pageContext.request.contextPath}/scripts/jquery/jquery-ui-1.8.2.custom/development-bundle/ui/jquery.ui.resizable.js"></script>
<script src="${pageContext.request.contextPath}/scripts/jquery/jquery-ui-1.8.2.custom/development-bundle/ui/jquery.ui.dialog.js"></script>
<script src="${pageContext.request.contextPath}/scripts/jquery/jquery-ui-1.8.2.custom/development-bundle/ui/jquery.ui.button.js"></script>
<script src="${pageContext.request.contextPath}/scripts/jquery/easyui/plugins/jquery.resizable.js"></script>
<script src="${pageContext.request.contextPath}/scripts/jquery/easyui/plugins/jquery.validatebox.js"></script>
<script src="${pageContext.request.contextPath}/scripts/jquery/jquery.floatingmessage.js"></script>
<script src="${pageContext.request.contextPath}/scripts/jquery/easyui/locale/easyui-lang-${lang}.js"></script>
<script src="${pageContext.request.contextPath}/scripts/jquery/jstree/js/lib/jquery.cookie.js?random=${currentTime}"></script>
<script src="${pageContext.request.contextPath}/scripts/jquery/jstree/js/lib/jquery.hotkeys.js?random=${currentTime}"></script>
<script src="${pageContext.request.contextPath}/scripts/core/import.js?random=${currentTime}"></script>
<script src="${pageContext.request.contextPath}/scripts/core/package.js?random=${currentTime}"></script>
<script src="${pageContext.request.contextPath}/scripts/basics/jqgrid_common.js"></script>
<script src="${pageContext.request.contextPath}/scripts/basics/jquery_common.js"></script>
<script src="${pageContext.request.contextPath}/scripts/basics/easyui_common.js"></script>
<script src="${pageContext.request.contextPath}/scripts/basics/initUploader.js?random=${currentTime}"></script>
<script src="${pageContext.request.contextPath}/scripts/basics/ie6.js"></script>
<script src="${pageContext.request.contextPath}/scripts/jquery/uploadify/swfobject.js"></script>
<script src="${pageContext.request.contextPath}/scripts/jquery/uploadify/jquery.uploadify.v2.1.4.min.js"></script>
<script src="${pageContext.request.contextPath}/scripts/jquery/jquery-json.js"></script>
<script src="${pageContext.request.contextPath}/scripts/jquery/ajaxfileupload/ajaxfileupload.js"></script>
<script src="${pageContext.request.contextPath}/scripts/itsm/app/activation/activation.js"></script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><fmt:message key="i18n.mainTitle"/></title>
</head>
<body>
<div style="margin: 10px 32%;height: 60%;width: 500px;">
<div style="width: 100%;margin-left: 5px;">
	<font size="4px"><fmt:message key="tip.notActivation.info" /></font>
</div><br>
<div id="frist" style="float: left;border-color:#DAE6FC; border-style:solid;width: 500px;" >
	<div style="height: 30px;width: 500px;background-color: #DAE6FC;line-height: 30px;"><font style="margin-left: 20px;"><b><fmt:message key="button.license.code.import" />:</b></font></div><br>
	<div style="float: left;margin-left: 5px;margin-top: 15%;"><b><fmt:message key="title.dashboard.license.code" /></b></div>
	<form id="codeForm">
		<textarea id="licenseKeyCode" name="licenseKeyCode" style="height:150px;width: 80%;margin-left: 15px;" class="easyui-validatebox input" required="true" ></textarea><br><br>
		<a id="sub_licenseKey" style="margin: 5px 35%;"><fmt:message key="title.dashboard.activate" /></a>
	</form>
</div>
</div>
<div style="height: 0px;overflow: hidden;width: 0px;position:relative">
<div id="alertMSG" title="<fmt:message key="msg.tips" />">
	
</div>
</div>
</body>
</html>