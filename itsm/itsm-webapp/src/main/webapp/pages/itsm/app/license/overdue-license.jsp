<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%
response.setHeader("Cache-Control","no-store");
response.setHeader("Pragrma","no-cache");
response.setDateHeader("Expires",0);
%> 
<%@ include file="../../../language.jsp" %>

<c:set var="currentTime" value="<%=new java.util.Date().getTime()%>" />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<style>.tabs li{margin-left:5px}</style>
<title><fmt:message key="i18n.mainTitle"/></title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/styles/comm.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/styles/style.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/scripts/jquery/easyui/themes/default/easyui.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/scripts/jquery/jqgrid/redmond/jquery-ui-1.8.1.custom.css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/scripts/jquery/fullcalendar-1.5/fullcalendar/fullcalendar.css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/scripts/jquery/uploadify/uploadify.css" /> 
<link rel="stylesheet" href="${pageContext.request.contextPath}/scripts/jquery/jm-calendar/css/jm-calendar.css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/scripts/jquery/contextmenu/jquery.contextmenu.css" />
<script src="${pageContext.request.contextPath}/scripts/jquery/jquery-1.4.2.min.js"></script>
<script src="${pageContext.request.contextPath}/scripts/jquery/jquery.bgiframe.min.js"></script>
<script src="${pageContext.request.contextPath}/scripts/i18n/i18n_${lang}.js"></script>
<script src="${pageContext.request.contextPath}/scripts/jquery/jquery.xml2json.js"></script>
<script src="${pageContext.request.contextPath}/scripts/jquery/easyui/plugins/jquery.form.js"></script>
<script src="${pageContext.request.contextPath}/scripts/jquery/easyui/plugins/jquery.layout.js"></script>
<script src="${pageContext.request.contextPath}/scripts/jquery/easyui/plugins/jquery.linkbutton.js"></script>
<script src="${pageContext.request.contextPath}/scripts/jquery/easyui/plugins/jquery.panel.js"></script>
<%-- autocomplete--%>
<script src="${pageContext.request.contextPath}/scripts/jquery/jquery-ui-1.8.2.custom/development-bundle/ui/jquery.ui.core.js"></script>
<script src="${pageContext.request.contextPath}/scripts/jquery/jquery-ui-1.8.2.custom/development-bundle/ui/jquery.ui.widget.js"></script>
<script src="${pageContext.request.contextPath}/scripts/jquery/jquery-ui-1.8.2.custom/development-bundle/ui/jquery.ui.mouse.js"></script>
<script src="${pageContext.request.contextPath}/scripts/jquery/jquery-ui-1.8.2.custom/development-bundle/ui/jquery.ui.position.js"></script>
<script src="${pageContext.request.contextPath}/scripts/jquery/jquery-ui-1.8.2.custom/development-bundle/ui/jquery.ui.autocomplete.js"></script>
<script src="${pageContext.request.contextPath}/scripts/jquery/jquery-ui-1.8.2.custom/development-bundle/ui/jquery.ui.datepicker.js"></script>
<script src="${pageContext.request.contextPath}/scripts/jquery/jquery-ui-1.8.2.custom/development-bundle/ui/jquery.ui.resizable.js"></script>
<script src="${pageContext.request.contextPath}/scripts/jquery/jquery-ui-1.8.2.custom/development-bundle/ui/jquery.ui.dialog.js"></script>
<script src="${pageContext.request.contextPath}/scripts/jquery/jquery-ui-1.8.2.custom/development-bundle/ui/jquery.ui.button.js"></script>
<script src="${pageContext.request.contextPath}/scripts/jquery/easyui/plugins/jquery.resizable.js"></script>
<script src="${pageContext.request.contextPath}/scripts/jquery/easyui/plugins/jquery.validatebox.js"></script>
<script src="${pageContext.request.contextPath}/scripts/jquery/jquery.floatingmessage.js"></script>
<script src="${pageContext.request.contextPath}/scripts/jquery/easyui/locale/easyui-lang-${lang}.js"></script>
<script src="${pageContext.request.contextPath}/scripts/jquery/jstree/js/lib/jquery.cookie.js?random=${currentTime}"></script>
<script src="${pageContext.request.contextPath}/scripts/jquery/jstree/js/lib/jquery.hotkeys.js?random=${currentTime}"></script>
<script src="${pageContext.request.contextPath}/scripts/core/import.js?random=${currentTime}"></script>
<script src="${pageContext.request.contextPath}/scripts/core/package.js?random=${currentTime}"></script>
<script src="${pageContext.request.contextPath}/scripts/basics/jqgrid_common.js"></script>
<script src="${pageContext.request.contextPath}/scripts/basics/jquery_common.js"></script>
<script src="${pageContext.request.contextPath}/scripts/basics/easyui_common.js"></script>
<script src="${pageContext.request.contextPath}/scripts/basics/initUploader.js?random=${currentTime}"></script>
<script src="${pageContext.request.contextPath}/scripts/basics/ie6.js"></script>
<script src="${pageContext.request.contextPath}/scripts/jquery/uploadify/swfobject.js"></script>
<script src="${pageContext.request.contextPath}/scripts/jquery/uploadify/jquery.uploadify.v2.1.4.min.js"></script>
<script src="${pageContext.request.contextPath}/scripts/jquery/jquery-json.js"></script>
<script src="${pageContext.request.contextPath}/scripts/jquery/ajaxfileupload/ajaxfileupload.js"></script>
<script src="${pageContext.request.contextPath}/scripts/itsm/app/license/overdue_license.js"></script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><fmt:message key="title.dashboard.activate.license"/>
</title>
<style type="text/css">
.progressbar{
	width: 200px;
	height: 20px;
	float: left;
}
#license_table{
	background-color:#DCE7FB;
}
#lineTableBgDiv table tr{
	height: 30px;
}
</style>

</head>
<body>
<div class="lineTableBgDiv">
	<table style="width:99%" class="lineTable" cellspacing="1">
	<tr>
		<td style="line-height:28px;font-size:12px;color:#FF0000">
		<p><fmt:message key="title.dashboard.activate.license"/></p>
		<a class="easyui-linkbutton" id="over_licenseCode_input" plain="true">
		<fmt:message key="button.license.code.import" /></a>
		<a class="easyui-linkbutton" id="over_upload_license_window" plain="true">
		<fmt:message key="button.license.file.import" /></a>
		</td>
	</tr>
	</table>
</div>
<div id="over_license_code" title="<fmt:message key="title.dashboard.activate.license" />" class="WSTUO-dialog" style="width:600px; height:auto;padding:2px">	
	<form id="over_license_code_form">
		<p><fmt:message key="title.dashboard.license.code" />：</p>
		<textarea id="over_licenseCode" name="licenseCoed" style="height:250px;width: 98%;"  class="easyui-validatebox input" required="true" ></textarea><br>
		<a class="easyui-linkbutton" id="over_licenseCode_submit" icon="icon-ok" plain="true" style="margin-left: 48%;" >
		<fmt:message key="title.dashboard.activate" /></a>
	</form>
</div>
<form id="over_uploadFile">
<div id="over_license_file" title="<fmt:message key="button.license.file.import" />" class="WSTUO-dialog" style="width:350px; height:auto;padding:15px">	
		<div style="float: left;"><fmt:message key="label.license.file" />:&nbsp;&nbsp;&nbsp;&nbsp;</div>
		<div style="float: left;">
			<input type="file" class="easyui-validatebox input" required="true" id="over_importLicenseFile" name="fileName" onchange="checkFileType(this,'txt')"/>
		</div><br><br>
		<a class="easyui-linkbutton" id="over_uploadFile_license_window_submit" icon="icon-search">
		<fmt:message key="label.attachment.upload" /></a>
</div>
</form>
<div style="height: 0px;overflow: hidden;width: 0px;position:relative">
<div id="alertMSG">
	
</div>
</div>
</body>
</html>