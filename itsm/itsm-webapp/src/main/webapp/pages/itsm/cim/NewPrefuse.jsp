<%@page import="com.wstuo.itsm.cim.dto.CIUtilDTO"%>
<%@page import="com.wstuo.itsm.cim.action.ConfigureItemAction"%>
<%@page
	import="org.springframework.web.context.support.WebApplicationContextUtils"%>
<%@page import="org.springframework.context.ApplicationContext"%>
<%@page import="com.wstuo.itsm.cim.service.ICIprefuseService"%>
<%@page import="com.wstuo.common.tools.service.IPagesSetSerice"%>
<%@page import="com.wstuo.common.tools.entity.PagesSet"%>
<%@page import="java.util.Set"%>
<%@page import="java.util.HashSet"%>
<%@page import="java.lang.*"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="../../language.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<script src="../../../scripts/jquery/jquery.min.js"></script>
<%
	Long ii = Long.valueOf(request.getParameter("ciId"));
	ApplicationContext ac = WebApplicationContextUtils
			.getWebApplicationContext(getServletContext());
	ICIprefuseService systemPage = (ICIprefuseService) ac
			.getBean("ciprefuseService");
	CIUtilDTO ciutil = new CIUtilDTO();
	Set<Long> ids=new HashSet<Long>();
	ciutil.getCilist().clear();
	ciutil.getReleList().clear();
	String cijson = systemPage.newPrefuseFindCINode(ii, ciutil,ids);

	request.setAttribute("cijson", cijson);
	String url = systemPage.findUrl()
			+ "/organization!getImageUrlforimageStream.action?imgFilePath=";

	request.setAttribute("url", url);
	String winWidth = request.getParameter("winWidth");
	request.setAttribute("winWidth", winWidth);
	String colorjson = systemPage.findColor();
	request.setAttribute("colorjson", colorjson);
	// ApplicationContext ac = WebApplicationContextUtils.getWebApplicationContext(getServletContext());
	IPagesSetSerice pageSet = (IPagesSetSerice) ac
			.getBean("pagesSetSerice");
	PagesSet pagesSet = pageSet.showPagesSet();
	request.setAttribute("pagesSet", pagesSet);
%>

</head>
<c:if test="${empty pagesSet.paName}">
	<title><fmt:message key="i18n.mainTitle" />
	</title>
</c:if>
<c:if test="${!empty pagesSet.paName}">
	<title>${pagesSet.paName}</title>
</c:if>

<body>
	<div
		style="width: 100%; background-color: #e4edfe; font-weight: bold; height: 32px; text-align: center;">
		<p style="margin-top: 0px;">配置项关系图</p>
	</div>

	<div style="width: 100%; height: 600px; border: 1px solid #666666;">
		<APPLET CODE="com.wstuo.itsm.cidependency.view.CIPrefuseView"
			ARCHIVE="ciPrefuse/itsm-cidependency-3.0-SNAPSHOT.jar,ciPrefuse/prefuse.jar,ciPrefuse/json-lib-2.3-jdk15.jar,ciPrefuse/commons-lang-2.4.jar,ciPrefuse/ezmorph-1.0.6.jar,
	ciPrefuse/commons-logging-api-1.1.jar,ciPrefuse/commons-collections-3.2.1.jar,ciPrefuse/commons-beanutils-1.7.0.jar"
			codebase="." 
			WIDTH="100%" 
			HEIGHT="600px">
			<param name='ciutil' value="${cijson}">
			<param name="lang" value="${lang}">
			<param name="url" value="${url}">
			<param name="fullwidth" value="${winWidth}">
			<param name="rightwidth" value="320">
			<param name="colorjson" value="${colorjson}">
		</APPLET>

		<div style="width: 100%; height: 130px; border: 1px solid #666666; border-top: none">
			<div style="padding-left: 60px; padding-top: 7px; font-size: 12px;" id="ColorTip">
				图例：
			</div>
			<div style="padding-left: 60px; padding-top: 7px; font-size: 12px;">
				<fmt:message key="title.prefuse.message" />
			</div>

			<div style="padding-left: 80px; padding-top: 12px; font-size: 12px;">
				<fmt:message key="title.prefuse.plug" />
			</div>
			<div style="padding-left: 80px; padding-top: 6px; font-size: 12px;">
				<fmt:message key="title.prefuse.url" />
				<a href="http://www.java.com/zh_CN/download/manual_java7.jsp">http://www.java.com/zh_CN/download/windows_ie.jsp</a>
				<fmt:message key="title.prefuse.install" />
			</div>
			<div style="padding-left: 80px; padding-top: 6px; font-size: 12px;">
				<fmt:message key="title.prefuse.wait" />
			</div>
		</div>

	</div>
<script type="text/javascript">
var url = 'dataDictionaryItems!findByCode.action?dataDictionaryQueryDto.groupCode=useStatus';
$.post(url,function(res){
	if(res!=null && res.length>0){
		$("#ColorTip").append('&nbsp;&nbsp;<b>CI 状态：</b>');
		for(var i=0;i<res.length;i++){
			if(res[i].color!=null && res[i].color!=""){
				$("#ColorTip").append('<span style="background-color:'+res[i].color+';width:120px;height:17px;padding:3px;">'+res[i].dname+'</span>&nbsp;&nbsp;');
			}
		}
	}	
});
var url = 'dataDictionaryItems!findByCode.action?dataDictionaryQueryDto.groupCode=ciRelationType';
$.post(url,function(res){
	if(res!=null && res.length>0){
		$("#ColorTip").append('&nbsp;&nbsp;<b>CI关系状态：</b>');
		for(var i=0;i<res.length;i++){
			if(res[i].color!=null && res[i].color!=""){
				$("#ColorTip").append('<span style="background-color:'+res[i].color+';width:120px;height:17px;padding:3px;color:white">'+res[i].dname+'</span>&nbsp;&nbsp;');
			}
		}
	}	
});
</script>
</body>
</html>