<%@page import="org.springframework.web.context.support.WebApplicationContextUtils"%>
<%@page import="java.util.Map"%>
<%@page import="com.wstuo.common.security.service.IUserInfoService"%>
<%@page import="org.springframework.web.context.WebApplicationContext"%>
<%@page import="java.text.DecimalFormat" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>   
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ include file="../../language.jsp" %>
<%
    WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(session.getServletContext());
	IUserInfoService userInfoService = (IUserInfoService)ctx.getBean("userInfoService");
	if(session.getAttribute("loginUserName")!=null){
	 	Map<String,Object> resAuth=userInfoService.findResAuthByUserName(session.getAttribute("loginUserName").toString(),new String[]{
	 		  "/pages/ci!cItemUpdate.action",
			  "CofnigureItem_AssetOriginalValue_Res",
			  "CofnigureItem_Barcode_Res",
			  "CofnigureItem_Supplier_Res",
			  "CofnigureItem_PurchaseDate_Res",
			  "CofnigureItem_ArrivalDate_Res",
			  "CofnigureItem_PurchaseNo_Res",
			  "CofnigureItem_Owner_Res",
			  "CofnigureItem_LifeCycle_Res",
			  "CofnigureItem_WarrantyDate_Res",
			  "CofnigureItem_WarningDate_Res",
			  "CofnigureItem_SerialNumber_Res",
			  "CofnigureItem_Department_Res",
			  "CofnigureItem_Fimapping_Res",
			  "CofnigureItem_ExpiryDate_Res",
			  "CofnigureItem_LendTime_Res",
			  "CofnigureItem_OriginalUser_Res",
			  "CofnigureItem_RecycleTime_Res",
			  "CofnigureItem_PlannedRecycleTime_Res",
			  "CofnigureItem_UsePermission_Res",
			  "/pages/ciRelevance!save.action",
			  "/pages/ciRelevance!update.action",
			  "/pages/ciRelevance!delete.action"
			  });
	 	request.setAttribute("resAuth",resAuth);
	}
%>
<script type="text/javascript">
if("${ciDetailDTO.ciId}" ==null || "${ciDetailDTO.ciId}" ==""){
	endProcess();
	msgAlert(i18n.requestIsDelete,'info');
	basics.tab.tabUtils.closeTab(i18n.ci_configureItemInfo);
}
</script>
<input type="hidden" id="configureItem_CiNo" value="${ciDetailDTO.ciId}" />
<input type="hidden" id="configureItem_num" value="${ciDetailDTO.cino}" />
<script src="${pageContext.request.contextPath}/scripts/itsm/cim/ciTree.js?random=<%=new java.util.Date().getTime()%>"></script>
<script src="${pageContext.request.contextPath}/scripts/itsm/cim/configureItemInfo.js?random=<%=new java.util.Date().getTime()%>"></script>

<div  class="loading" id="configureItemInfo_loading"><img src="${pageContext.request.contextPath}/images/icons/loading.gif" /></div>
	<!-- 新的面板 start-->
<div id="configureItemInfo_panel" class="content" style="height: 100%;">
	<div id="ciDetails_layout" class="easyui-layout" fit="true">
		<div region="north" border="false" class="panelTop">
			<a class="easyui-linkbutton" plain="true" icon="icon-undo" id="configureItemInfo_returnToList" title="<fmt:message key="common.returnToList" />"></a>
			<c:if test="${resAuth['/pages/ci!cItemUpdate.action'] }">
				<a class="easyui-linkbutton" plain="true" icon="icon-edit" id="configureItem_edit_but" title="<fmt:message key="common.edit"/>"></a> 
			</c:if>
		</div>
		
		<div region="west" id="configureItemInfo_west" split="true" title="<fmt:message key="label.resourceInformation"/>" style="width:400px;">
		
		<form>
        <input type="hidden" name="ciDto.ciId" id="configureCiInfoId" value="${ciDetailDTO.ciId}" />
        <input type="hidden" name="ciDto.categoryType" id="categoryType" />
        <div id="configureItemInfo_div" class="lineTableBgDiv">
     	   <table style="width:100%" class="lineTable" cellspacing="1">
            
            <!-- 所属客户  -->
			<tr <c:if test="${versionType!='ITSOP'}"><c:out value="style=display:none"></c:out></c:if>>
				<td><fmt:message key="label.belongs.client" /></td>
				<td>
					${ciDetailDTO.companyName}
					<input type="hidden" id="configureItemInfo_companyNo" value="${ciDetailDTO.companyNo}" />
				</td>
			</tr>
			<%-- <tr>
                <td style="width: 160px;"><fmt:message key="label.ci.ciServiceDir"/></td>
				<td>${ciDetailDTO.ciServiceDirName}</td>
			</tr> --%>
            <tr>
                <td style="width: 160px;"><fmt:message key="title.request.CICategory"/></td>
				<td>${ciDetailDTO.categoryName}</td>
			</tr>
			<tr>	
                <td><fmt:message key="lable.ci.assetNo"/></td>
                <td>${ciDetailDTO.cino}</td>
            </tr>
            <tr>
                <td><fmt:message key="title.asset.name"/></td>
                <td><div style="white-space:normal; display:block;width:100%; word-break:break-all;">${ciDetailDTO.ciname}</div>
                	<input type="hidden" id="configureCiInfo_ciname" value="${ciDetailDTO.ciname}"> 
                </td>
            </tr>
            <tr>    
                <td><fmt:message key="label.dc.systemPlatform"/></td>
                <td>${ciDetailDTO.systemPlatform}</td>
            </tr>
			<tr>    
                <td><fmt:message key="ci.productType"/></td>
                <td><div style="white-space:normal; display:block;width:100%; word-break:break-all;">${ciDetailDTO.model}</div></td>        
            </tr>
            <c:if test="${resAuth['CofnigureItem_SerialNumber_Res'] }">
            <tr>
                <td><fmt:message key="ci.serialNumber"/></td>
                <td><div style="white-space:normal; display:block;width:100%; word-break:break-all;">${ciDetailDTO.serialNumber}</div></td>
            </tr>
            </c:if>
            <c:if test="${resAuth['CofnigureItem_Barcode_Res'] }">
			<tr>    
                <td><fmt:message key="ci.barcode"/></td>
                <td><div style="white-space:normal; display:block;width:100%; word-break:break-all;">${ciDetailDTO.barcode}</div></td>
            </tr>
			</c:if>
            <tr>
                <td><fmt:message key="common.status"/></td>
                <td>${ciDetailDTO.status}</td>
            </tr>
			<tr>    
                <td><fmt:message key="ci.brands"/></td>
                <td>${ciDetailDTO.brandName}</td>
            </tr>
            <c:if test="${resAuth['CofnigureItem_Supplier_Res'] }">
            <tr>
                <td><fmt:message key="label.supplier"/></td>
                <td>${ciDetailDTO.providerName}</td>
            </tr>
			</c:if>
			<tr>    
                <td><fmt:message key="ci.location"/></td>
                <td>${ciDetailDTO.loc}</td>
            </tr>
            <!-- 创建时间 -->
            <tr>
            	<td><fmt:message key="common.cerateTime"/></td><td><fmt:formatDate type="date" value="${ciDetailDTO.createTime}" pattern="yyyy-MM-dd"/></td>
            </tr>
            <tr>
            	<td><fmt:message key="common.updateTime" /></td><td><fmt:formatDate type="date" value="${ciDetailDTO.lastUpdateTime}"  pattern="yyyy-MM-dd" /></td>
            </tr>
            <c:if test="${resAuth['CofnigureItem_PurchaseDate_Res'] }">
            <tr>
                <td><fmt:message key="ci.purchaseDate"/></td>
                <td><fmt:formatDate type="date" value="${ciDetailDTO.buyDate}"  pattern="yyyy-MM-dd" /></td>
            </tr>
            </c:if>
            <c:if test="${resAuth['CofnigureItem_ArrivalDate_Res'] }">
			<tr>    
                <td><fmt:message key="ci.arrivalDate"/></td>
                <td><fmt:formatDate type="date" value="${ciDetailDTO.arrivalDate}"  pattern="yyyy-MM-dd" /></td>
            </tr>
            </c:if>
            <c:if test="${resAuth['CofnigureItem_WarningDate_Res'] }">
			<tr>    
                <td><fmt:message key="ci.warningDate"/></td>
                <td><fmt:formatDate type="date" value="${ciDetailDTO.warningDate}"  pattern="yyyy-MM-dd" /></td>
            </tr>
            </c:if>
            <c:if test="${resAuth['CofnigureItem_PurchaseNo_Res'] }">
            <tr>
                <td><fmt:message key="ci.purchaseNo"/></td>
                <td><div style="white-space:normal; display:block;width:100%; word-break:break-all;">${ciDetailDTO.poNo}</div></td>
            </tr>
            </c:if>
            <c:if test="${resAuth['CofnigureItem_AssetOriginalValue_Res'] }">
            <tr>
            	<td><fmt:message key="ci.assetOriginalValue"/></td>
            	<td>
<%--             	<fmt:formatNumber value="${ciDetailDTO.assetsOriginalValue}" pattern="#,#00.0#"/> --%>
            	</td>
            </tr>
            </c:if>
            
            <c:if test="${resAuth['CofnigureItem_Fimapping_Res'] }">
            <tr>
            	<td><fmt:message key="ci.FIMapping"/></td>
            	<td>
            	<c:if test="${ciDetailDTO.financeCorrespond==true}">
				   <fmt:message key="tool.affiche.yes" />
				</c:if>
				<c:if test="${ciDetailDTO.financeCorrespond==false}">
				  <fmt:message key="tool.affiche.no" />
				</c:if>
            	</td>
            </tr>
            </c:if>
            
            <!-- 多少年折旧率为0 -->
	            <tr>
	            	<td><fmt:message key="ci.depreciationIsZeroYears"/></td>
	            	<td>
	            	<c:if test="${ciDetailDTO.depreciationIsZeroYears>0}">
	            	${ciDetailDTO.depreciationIsZeroYears}<fmt:message key="title.report.year"/>
	            	</c:if>
	            	</td>
	            </tr>
	            <c:if test="${ciDetailDTO.depreciationIsZeroYears > 0 && ciDetailDTO.arrivalDate != null && ciDetailDTO.ciDepreciation > 0}">
	            <tr>
	            	<td><fmt:message key="ci.depreciationRate"/></td>
	            	<td>
	            		<c:if test="${ciDetailDTO.ciDepreciation > 100}">100%</c:if>
	            		<c:if test="${ciDetailDTO.ciDepreciation < 100}">${ciDetailDTO.ciDepreciation} %</c:if>
	            	</td>
	            </tr>
	            </c:if>
	            <c:if test="${ciDetailDTO.depreciationIsZeroYears > 0 && ciDetailDTO.arrivalDate != null && ciDetailDTO.ciDepreciation<=0}">
	            <tr>
	            	<td><fmt:message key="ci.depreciationRate"/></td>
	            	<td><font color="red"><fmt:message key="ci.depreciationAlreadyIsZero"/></font></td>
	            </tr>
	            </c:if>
          </table>
          </div>
        </form>
		</div>
		
		
		<div region="center" id="configureItemInfo_center" title='<fmt:message key="common.detailInfo"/>' style="padding-bottom: 15px;">
		
			<div id="configureItemInfoTab" class="easyui-tabs" fit="true" > 
				<div title="<fmt:message key="ci.ciOtherInfo" />">
					<div class="lineTableBgDiv">
     	  				 <table style="width:100%" class="lineTable" cellspacing="1">
     	  				 	<c:if test="${resAuth['CofnigureItem_Department_Res'] }">
     	  				 	<tr>
				            	<td style="width: 160px;"><fmt:message key="ci.department" /></td>
				            	<td>${ciDetailDTO.department}</td>
				            </tr>
				            </c:if>
				            
				            <tr>
				            	<td><fmt:message key="label.ci.businessEntity" /></td>
				            	<td>${ciDetailDTO.workNumber}</td>
				            </tr>
				            <tr>
				            	<td><fmt:message key="label.ci.project" /></td>
				            	<td>${ciDetailDTO.project}</td>
				            </tr>
				            
				            <tr>
				            	<td><fmt:message key="ci.productProvider" /></td>
				            	<td>${ciDetailDTO.sourceUnits}</td>
				            </tr>
				            <c:if test="${resAuth['CofnigureItem_LifeCycle_Res'] }">
     	  				 	<tr>
				                <td><fmt:message key="ci.lifeCycle"/></td>
				                <td>
				                <c:if test="${ciDetailDTO.lifeCycle!='0'}">
				                	${ciDetailDTO.lifeCycle}
				                </c:if>
				                </td>
				           	</tr>
				           	</c:if>
				           	
				           	<c:if test="${resAuth['CofnigureItem_WarrantyDate_Res'] }">
							<tr>     
				                <td><fmt:message key="ci.warrantyDate"/></td>
				                <td>
				                <c:if test="${ciDetailDTO.warranty!='0'}">
				                	${ciDetailDTO.warranty}
				                </c:if>
				                </td>
				            </tr>
				            </c:if>
				            <c:if test="${resAuth['CofnigureItem_ExpiryDate_Res'] }">
				            <tr>
				            	<td><fmt:message key="ci.expiryDate" /></td>
				            	<td><fmt:formatDate type="date" value="${ciDetailDTO.wasteTime}"  pattern="yyyy-MM-dd" /></td>
				            </tr>
				            </c:if>
				            <c:if test="${resAuth['CofnigureItem_LendTime_Res'] }">
				            <tr>
				            	<td><fmt:message key="ci.lendTime" /></td>
				            	<td><fmt:formatDate type="date" value="${ciDetailDTO.borrowedTime}"  pattern="yyyy-MM-dd" /></td>
				            </tr>
				            </c:if>
				            
				            <c:if test="${resAuth['CofnigureItem_PlannedRecycleTime_Res'] }">
				            <tr>
				            	<td><fmt:message key="ci.plannedRecycleTime" /></td>
				            	<td><fmt:formatDate type="date" value="${ciDetailDTO.expectedRecoverTime}"  pattern="yyyy-MM-dd" /></td>
				            </tr>
				            </c:if>
				            
				            <c:if test="${resAuth['CofnigureItem_RecycleTime_Res'] }">
				            <tr>
				            	<td><fmt:message key="ci.recycleTime" /></td>
				            	<td><fmt:formatDate type="date" value="${ciDetailDTO.recoverTime}"  pattern="yyyy-MM-dd" /></td>
				            </tr>
				            </c:if>
				            <c:if test="${resAuth['CofnigureItem_UsePermission_Res'] }">
				            <tr>
				            	<td><fmt:message key="ci.usePermission" /></td>
				            	<td>${ciDetailDTO.usePermissions}</td>
				            </tr>
				            </c:if>
				            <c:if test="${resAuth['CofnigureItem_OriginalUser_Res'] }">
				            <tr>
			            		<td><fmt:message key="ci.originalUser" /></td>
			            		<td>${ciDetailDTO.originalUser}
			            		
			            		</td>
			           		</tr>
			           		</c:if>
				            <tr>
				                <td><fmt:message key="ci.use"/></td>
				                <td>${ciDetailDTO.userName}</td>
				            </tr>
				            <c:if test="${resAuth['CofnigureItem_Owner_Res'] }">
							<tr>    
				                <td><fmt:message key="ci.owner"/></td>
				                <td>${ciDetailDTO.owner}</td>  
				            </tr>
				            </c:if>
				            <tr>
				            	<td><fmt:message key="label.role.roleMark" /></td>
				            	<td>${ciDetailDTO.CDI}</td>
				            </tr>
     	  				 </table>
     	  			</div> 
				</div>
					
                <div title="<fmt:message key="config.extendedInfo"/>" style="padding:3px;">
               	    <div class="hisdiv" id="request_info_eavAttributet">
						<table style="width:100%" class="histable" cellspacing="1" >
						<c:set var="oldCiAddGroupName"></c:set>
							<s:if test="attributeList!=null">
							<s:iterator value="attributeList" status="attrs">
							<c:if test="${attrGroupName != oldCiAddGroupName}">
							<thead>
								<tr>
									<th colspan="2" style="text-align:left;"><s:property value="attrGroupName"/></th>
									<%--<th><fmt:message key="common.attributeName" /></th><th><fmt:message key="common.attributeValue" /></th> --%>
									
									<c:set var="oldCiAddGroupName" value="${attrGroupName}"></c:set>
								</tr>
							</thead>
							</c:if>
							
	              			<tr>
							    <td style="text-align:left;width:25%" ><s:property value="attrAsName"/></td>
							    <td style="text-align:left;word-break:break-all;">
							    	${ciDetailDTO.attrVals[attrName]}
								</td>
						    </tr>
							</s:iterator>
							</s:if>
							<s:if test="attributeList==null">
								<tr><td style="color:red;font-size:16px"><fmt:message key="label.ciNotExtendedInfo" /></td></tr>
							</s:if>
						</table>
					</div>	
                </div>
                <%--硬件 --%>
                <div title="<fmt:message key="label_hardware" />" style="padding:2px; width: 100%;">
                	<div id="ciInfoOne" style="display:none">
                	<fieldset>
		 				<legend><fmt:message key="label_computer_system" /></legend>
		 				<div style="width: 98%">
		 					<div style="display:inline-block;width: 48%"><fmt:message key="config.computerName" />:<span id="computerSystem_name"></span></div>
		 					<div style="display:inline-block;width: 48%"><fmt:message key="config_computerSystem_model" />:<span id="computerSystem_model"></span></div>
		 				</div>
		 				<div style="width: 98%">
		 					<div style="display:inline-block;width: 48%"><fmt:message key="config_computerSystem_domain" />:<span id="computerSystem_domain"></span></div>
		 					<div style="display:inline-block;width: 48%"><fmt:message key="common.Username" />:<span id="computerSystem_userName"></span></div>
		 				</div>
		 			</fieldset>
		 			<fieldset>
		 				<legend><fmt:message key="config.systemName" /></legend>
		 				<div style="width: 98%">
		 					<div style="display:inline-block;width: 48%"><fmt:message key="ci.serialNumber" />:<span id="operatingSystem_serialNumber"></span></div>
		 					<div style="display:inline-block;width: 48%"><fmt:message key="label.snmp.systemName" />:<span id="operatingSystem_caption"></span></div>
		 				</div>
		 				<div style="width: 98%">
		 					<div style="display:inline-block;width: 48%"><fmt:message key="config.systemVersion" />:<span id="operatingSystem_version"></span></div>
		 					<div style="display:inline-block;width: 48%"><fmt:message key="config_operatingSystem_osArchitecture" />:<span id="operatingSystem_osArchitecture"></span></div>
		 				</div>
		 				<div style="width: 98%">
		 					<div style="display:inline-block;width: 48%"><fmt:message key="config_operatingSystem_csdVersion" />:<span id="operatingSystem_csdVersion"></span></div>
		 					<div style="display:inline-block;width: 48%"><fmt:message key="config_operatingSystem_installDate" />:<span id="operatingSystem_installDate"></span></div>
		 				</div>
		 				<div style="width: 98%">
		 					<div style="display:inline-block;width: 48%"><fmt:message key="config_operatingSystem_lastBootUpTime" />:<span id="operatingSystem_lastBootUpTime"></span></div>
		 					<div style="display:inline-block;width: 48%"></div>
		 				</div>
		 			</fieldset>
		 			<fieldset>
			 				<legend><fmt:message key="config_memory" /></legend>
			 				<div style="width: 98%">
			 					<div style="display:inline-block;width: 48%"><fmt:message key="config_operatingSystem_totalVisibleMemorySize" />:<span id="operatingSystem_totalVisibleMemorySize"></span></div>
			 					<div style="display:inline-block;width: 48%"><fmt:message key="config_operatingSystem_totalVirtualMemorySize" />:<span id="operatingSystem_totalVirtualMemorySize"></span></div>
			 				</div>
			 		</fieldset>
			 		<fieldset>
			 				<legend>CPU</legend>
			 				<div style="width: 98%">
			 					<div style="display:inline-block;width: 48%"><fmt:message key="config_processor_name" />:<span id="processor_name"></span></div>
			 					<div style="display:inline-block;width: 48%"><fmt:message key="config_processor_maxClockSpeed" />:<span id="processor_maxClockSpeed"></span></div>
			 				</div>
			 				<div style="width: 98%">
			 					<div style="display:inline-block;width: 48%"><fmt:message key="config_processor_l2CacheSize" />:<span id="processor_l2CacheSize"></span></div>
			 					<div style="display:inline-block;width: 48%"><fmt:message key="config_processor_l3CacheSize" />:<span id="processor_l3CacheSize"></span></div>
			 				</div>
			 				<div style="width: 98%">
			 					<div style="display:inline-block;width: 48%"><fmt:message key="config_processor_level" />:<span id="processor_level"></span></div>
			 					<div style="display:inline-block;width: 48%"></div>
			 				</div>
			 		</fieldset>
			 		<fieldset>
			 				<legend>BIOS</legend>
			 				<div style="width: 98%">
			 					<div style="display:inline-block;width: 48%"><fmt:message key="ci.serialNumber" />:<span id="bios_serialNumber"></span></div>
			 					<div style="display:inline-block;width: 48%"><fmt:message key="label.name" />:<span id="bios_name"></span></div>
			 				</div>
			 				<div style="width: 98%">
			 					<div style="display:inline-block;width: 48%"><fmt:message key="config_manufacturer" />:<span id="bios_manufacturer"></span></div>
			 					<div style="display:inline-block;width: 48%"><fmt:message key="config_bios_version" />:<span id="bios_version"></span></div>
			 				</div>
			 				<div style="width: 98%">
			 					<div style="display:inline-block;width: 48%"><fmt:message key="config_bios_releaseDate" />:<span id="bios_releaseDate"></span></div>
			 					<div style="display:inline-block;width: 48%"></div>
			 				</div>
			 		</fieldset>
			 		<fieldset>
			 				<legend><fmt:message key="config_baseBoard_name" /></legend>
			 				<div style="width: 98%">
			 					<div style="display:inline-block;width: 48%"><fmt:message key="label.name" />:<span id="baseBoard_name"></span></div>
			 					<div style="display:inline-block;width: 48%"><fmt:message key="ci.serialNumber" />:<span id="baseBoard_serialNumber"></span></div>
			 				</div>
			 				<div style="width: 98%">
			 					<div style="display:inline-block;width: 48%"><fmt:message key="config.mainboard" />:<span id="baseBoard_manufacturer"></span></div>
			 					<div style="display:inline-block;width: 48%"></div>
			 				</div>
			 		</fieldset>
			 		<fieldset>
			 				<legend><fmt:message key="config_netWork_info" /></legend>
			 				<div style="width: 98%">
			 					<div style="display:inline-block;width: 48%">IP:<span id="netWork_ip"></span></div>
			 					<div style="display:inline-block;width: 48%">MAC:<span id="netWork_mac"></span></div>
			 				</div>
			 				<div style="width: 98%">
			 					<div style="display:inline-block;width: 48%"><fmt:message key="label.name" />:<span id="netWork_name"></span></div>
			 					<div style="display:inline-block;width: 48%">DHCP:<span id="netWork_dhcp"></span></div>
			 				</div>
			 				<div style="width: 98%">
			 					<div style="display:inline-block;width: 48%"><fmt:message key="config_dhcp_server" />:<span id="netWork_dhcpServer"></span></div>
			 					<div style="display:inline-block;width: 48%"></div>
			 				</div>
			 		</fieldset>
			 		<fieldset>
			 				<legend><fmt:message key="config_desktopMonitor_name" /></legend>
			 				<div style="width: 98%">
			 					<div style="display:inline-block;width: 48%"><fmt:message key="label.name" />:<span id="desktopMonitor_name"></span></div>
			 					<div style="display:inline-block;width: 48%"><fmt:message key="config_desktopMonitor_height" />:<span id="desktopMonitor_screenHeight"></span></div>
			 				</div>
			 				<div style="width: 98%">
			 					<div style="display:inline-block;width: 48%"><fmt:message key="config_desktopMonitor_width" />:<span id="desktopMonitor_screenWidth"></span></div>
			 					<div style="display:inline-block;width: 48%"><fmt:message key="config_manufacturer" />:<span id="desktopMonitor_monitorManufacturer"></span></div>
			 				</div>
			 				<div style="width: 98%">
			 					<div style="display:inline-block;width: 48%"><fmt:message key="ci.serialNumber" />:<span id="desktopMonitor_serialNumber"></span></div>
			 					<div style="display:inline-block;width: 48%"></div>
			 				</div>
			 		</fieldset>
			 		<fieldset>
			 				<legend><fmt:message key="config_pointingDevice" /></legend>
			 				<div style="width: 98%">
			 					<div style="display:inline-block;width: 48%"><fmt:message key="label.name" />:<span id="pointingDevice_Name"></span></div>
			 					<div style="display:inline-block;width: 48%"><fmt:message key="label.user.description" />:<span id="pointingDevice_description"></span></div>
			 				</div>
			 				<div style="width: 98%">
			 					<div style="display:inline-block;width: 48%"><fmt:message key="ci.serialNumber" />:<span id="pointingDevice_serialNumber"></span></div>
			 					<div style="display:inline-block;width: 48%"><fmt:message key="config_manufacturer" />:<span id="pointingDevice_manufacturer"></span></div>
			 				</div>
			 		</fieldset>
			 		<fieldset>
			 				<legend><fmt:message key="config_keyboard" /></legend>
			 				<div style="width: 98%">
			 					<div style="display:inline-block;width: 48%"><fmt:message key="label.name" />:<span id="keyboard_Name"></span></div>
			 					<div style="display:inline-block;width: 48%"><fmt:message key="label.user.description" />:<span id="keyboard_description"></span></div>
			 				</div>
			 				<div style="width: 98%">
			 					<div style="display:inline-block;width: 48%"><fmt:message key="config_keyboard_numberOfFunctionKeys" />:<span id="keyboard_numberOfFunctionKeys"></span></div>
			 					<div style="display:inline-block;width: 48%"><fmt:message key="ci.serialNumber" />:<span id="keyboard_serialNumber"></span></div>
			 				</div>
			 				<div style="width: 98%">
			 					<div style="display:inline-block;width: 48%"><fmt:message key="config_manufacturer" />:<span id="keyboard_manufacturer"></span></div>
			 					<div style="display:inline-block;width: 48%"></div>
			 				</div>
			 		</fieldset>
			 		<fieldset>
			 				<legend><fmt:message key="config_physicalMemory" /></legend>
                			<div class="lineTableBgDiv" id="physicalMemory"><table class="lineTable" style="width:100%" cellspacing="1"></table></div>
                	</fieldset>	
			 		<fieldset>
			 				<legend><fmt:message key="config_diskDrive" /></legend>
                			<div class="lineTableBgDiv" id="diskDrive"><table class="lineTable" style="width:100%" cellspacing="1"></table></div>
                	</fieldset>
                	<fieldset>
			 				<legend><fmt:message key="config_cdRomDrive" /></legend>
                			<div class="lineTableBgDiv" id="cdRomDrive"><table class="lineTable" style="width:100%" cellspacing="1"></table></div>
                	</fieldset>	
			 		<fieldset>
			 				<legend><fmt:message key="config_logicalDisk" /></legend>
			 				<div class="lineTableBgDiv" id="logicalDisk"><table class="lineTable" style="width:100%" cellspacing="1"></table></div>
			 		</fieldset>		
			 		
                	<fieldset>
			 				<legend><fmt:message key="config_ideController" /></legend>
                			<div class="lineTableBgDiv" id="ideController"><table class="lineTable" style="width:100%" cellspacing="1"></table></div>
                	</fieldset>			
                	<fieldset>
			 				<legend><fmt:message key="config_usbController" /></legend>
                			<div class="lineTableBgDiv" id="usbController"><table class="lineTable" style="width:100%" cellspacing="1"></table></div>
                	</fieldset>	
                	<fieldset>
			 				<legend>USB Hub</legend>
                			<div class="lineTableBgDiv" id="usbHub"><table class="lineTable" style="width:100%" cellspacing="1"></table></div>
                	</fieldset>
                	<fieldset>
			 				<legend><fmt:message key="config_serialPort" /></legend>
                			<div class="lineTableBgDiv" id="serialPort"><table class="lineTable" style="width:100%" cellspacing="1"></table></div>
                	</fieldset>	
                	<fieldset>
			 				<legend><fmt:message key="config_parallelPort" /></legend>
                			<div class="lineTableBgDiv" id="parallelPort"><table class="lineTable" style="width:100%" cellspacing="1"></table></div>
                	</fieldset>	
                	</div>
                	<div id="ciInfoTwo" style="display:none">
                	<fieldset>
		 				<legend><fmt:message key="label_hardware" /></legend>

		 				<div style="width: 98%">
		 					<div style="display:inline-block;width: 48%"><fmt:message key="label.role.roleName" />:<span id="computerSystem_name1"></span></div>
		 					<div style="display:inline-block;width: 48%">IP:<span id="netWork_ip1"></span></div>
		 					
		 				</div>
		 				<div style="width: 98%">
		 					<div style="display:inline-block;width: 48%"><fmt:message key="label.snmp.systemOID" />:<span id="operatingSystem_serialNumber1"></span></div>
		 					<div style="display:inline-block;width: 48%"><fmt:message key="ci.brands" />:<span id="computerSystem_userName1"></span></div>
		 				</div>
		 				<div style="width: 98%">
		 					<div style="display:inline-block;width: 48%"><fmt:message key="config_operatingSystem_lastBootUpTime" />:<span id="operatingSystem_lastBootUpTime1"></span></div>
		 					<div style="display:inline-block;width: 48%"><fmt:message key="ci.productType" />:<span id="computerSystem_model1"></span></div>
		 				</div>
		 				<div style="width: 98%">
			 					<%-- <div style="display:inline-block;width: 48%"><fmt:message key="config_operatingSystem_totalVisibleMemorySize" />:<span id="operatingSystem_totalVisibleMemorySize1"></span></div> --%>
			 					<div style="display:inline-block;width: 48%"><fmt:message key="config.systemVersion" />:<span id="operatingSystem_version1"></span></div>
			 				</div>
		 			</fieldset>
                	<fieldset>
			 				<legend>Interface</legend>
                			<div class="lineTableBgDiv" id="interfacesCi"><table class="lineTable" style="width:100%" cellspacing="1"></table></div>
                	</fieldset>	
                	</div>
                </div>
                <%-- 附件 --%>
				<div title='<fmt:message key="common.attachment" />'>
						<div class="lineTableBgDiv" id="configureItemInfo_effect_atta_div">
						<form>
							<input type="hidden" id="configureItemInfo_effect_attachmentStr"/>
							<table width="100%" cellspacing="1" class="lineTable">
								
								<tr>
									<td>
									<div id="configureItemInfo_effect_success_attachment" style="line-height:25px;color:#555;display: none"></div>
									<div class="hisdiv" id="show_configureItemInfo_effectAttachment">
										<table style="width:100%" class="histable" cellspacing="1">
											<thead>
												<tr>
													<th><fmt:message key="common.id" /></th>
													<th><fmt:message key="label.problem.attachmentName" /></th>
													<th><fmt:message key="label.problem.attachmentUrl" /></th>
													<th><fmt:message key="label.sla.operation" /></th>
												</tr>
											</thead>
											<tbody></tbody>
					             			</table>
					               	</div>
									</td>
								</tr>
								
								 <tr>
									<td>
                                        <div class="diyLinkbutton">
                                           <div style="float: left;cursor: pointer;">
										      <input type="file"  name="filedata" id="configureItemInfo_effect_file">
											</div>
										<div style="float: left;margin-left: 15px;">
											<a class="easyui-linkbutton" icon="icon-upload" href="javascript:itsm.cim.configureItemInfo.configureitemInfo_uploadifyUpload()" ><fmt:message key="label.attachment.upload" /></a>
							            </div>
							            <div style="float: left;margin-left: 15px;"><a href="javascript:common.config.attachment.chooseAttachment.showAttachmentGrid('ConfigureItemDetail','edit')" class="easyui-linkbutton" icon="icon-selectOldFile" id="chooseAtt"><fmt:message key="common.attachmentchoose" /></a></div>
						             	 </div>
                                        <div style="clear: both;"></div>
                                         <div id="configureItemInfo_effect_fileQueue"></div>
									</td>
								</tr>
							</table>
						</form>
						<%-- <form id="uploadForm_CiInfo" method="post" enctype="multipart/form-data">
							  <table border="0" cellspacing="1" class="fu_list">
					              <tr height="40px">
						        	<td colspan="2">
						        	<table border="0" cellspacing="0"><tr><td >
							       			<a href="javascript:void(0);" class="files" id="idFile_CiInfo"></a>
							       		</td><td>
									        <a class="easyui-linkbutton" style="float:left" plain="true"  icon="icon-ok" id="idBtnupload_CiInfo"><fmt:message key="label.startUpload" /></a>
									         &nbsp;&nbsp;&nbsp;
											<a class="easyui-linkbutton" plain="true" icon="icon-cancel" onclick="javascript:void(0);" id="idBtndel_CiInfo"><fmt:message key="label.allcancel" /></a>
											 &nbsp;&nbsp;&nbsp;
											<a plain="true" href="javascript:common.config.attachment.chooseAttachment.showAttachmentGrid('ConfigureItemDetail','edit')" class="easyui-linkbutton" icon="icon-ok" id="chooseAtt"><fmt:message key="common.attachmentchoose" /></a>
										</td></tr></table>
									</td>
						      		</tr>
						          <tbody id="idFileList_CiInfo">
						          </tbody>
								</table>
							</form> --%>
					</div>
				</div>
				 <!-- 软件配置参数 start -->
				<div title="<fmt:message key="label.ci.softSettingParam"/>">
					
					<form>
					<div class="hisdiv" id="ci_add_softSetting">
					<table style="width:100%" class="histable" cellspacing="1">
						<tr>
							<td width="30%"><fmt:message key="label.ci.softSetParam"/></td>
							<td width="60%"><pre>${ciDetailDTO.softSetingParam}</pre></td>
						</tr>
						<tr>
							<td width="30%"><fmt:message key="label.ci.softSettingAm"/></td>
							<td width="60%"><pre>${ciDetailDTO.softConfigureAm}</pre></td>
						</tr>
						<tr>
							<td width="30%"><fmt:message key="label.ci.softSettingRemark1"/></td>
							<td width="60%"><pre>${ciDetailDTO.softRemark1}</pre></td>
						</tr>
						<tr>
							<td width="30%"><fmt:message key="label.ci.softSettingRemark2"/></td>
							<td width="60%"><pre>${ciDetailDTO.softRemark2}</pre></td>
						</tr>
						<tr>
							<td width="30%"><fmt:message key="label.ci.softSettingRemark3"/></td>
							<td width="60%"><pre>${ciDetailDTO.softRemark3}</pre></td>
						</tr>
             		</table>
               		</div>
					<div class="hisdiv">
						<table width="100%" cellspacing="1" class="histable">
							<c:if test="${not empty ciDetailDTO.softAttachments}">
							<thead>
							<tr>
								<th><fmt:message key="common.id" /></th>
								<th><fmt:message key="label.problem.attachmentName" /></th>
								<th><fmt:message key="label.problem.attachmentUrl" /></th>
								<th><fmt:message key="ci.operateItems" /></th>
							</tr>
							</thead>
							
							<c:forEach items="${ciDetailDTO.softAttachments}" varStatus="i" var="attachment">
							<tr>
								<td>${i.count}</td>
								<td style="text-align: left;" ><a target="_blank" href="attachment!download.action?downloadAttachmentId=${attachment.aid }">${attachment.attachmentName}</a></td>
								<td style="text-align: left;" >&nbsp;&nbsp;${attachment.url}</td>
								<td><a target="_blank" href="attachment!download.action?downloadAttachmentId=${attachment.aid }">[<fmt:message key="label.problem.attachmentDownload" />]</a></td>
							</tr>
							</c:forEach>
							
							</c:if>

							<c:if test="${empty ciDetailDTO.softAttachments}">
								<tr><td style="color:red;font-size:16px"><fmt:message key="label.notAttachment" /></td></tr>
							</c:if>
						</table>	
                
                	</div>
				</form>	
				</div>
				<!-- 软件配置参数end -->
                <div title='<fmt:message key="label.ci.ciServiceDir" />'>
					
					<div class="hisdiv">
						<table width="100%" cellspacing="1" class="histable">
							<c:if test="${not empty ciDetailDTO.serviceDirDtos}">
							<thead>
								<tr>
									<th style="width:10%"><fmt:message key="lable.serial.number"/> </th>
									<th style="width:90%"><fmt:message key="lable.ci.ciServiceDirName" /></th>
								</tr>
							</thead>
							<tbody>
							<c:forEach items="${ciDetailDTO.serviceDirDtos}" var="sd" varStatus="i">
							<tr>
							<td>${i.count}</td>
							<td>${sd.eventName}</td>
							</tr>
							</c:forEach>
							</tbody>
							</c:if>
							<c:if test="${empty ciDetailDTO.serviceDirDtos}">
								<tr><td style="color:#FF0000;font-size:16px"><fmt:message key="commom.noData" /></td></tr>
							</c:if>
						</table>
					</div>
				</div>
				<sec:authorize url="configItem_detail_queryRelatingconfigItem">
                <div title="<fmt:message key="ci.relatedCI"/>" style="padding:2px; width: 100%;">
                	<div style="display: none;" id="ciRelevancaToolbar">
                	<c:if test="${resAuth['/pages/ciRelevance!save.action'] }">
                    	<a class="easyui-linkbutton" icon="icon-add" plain="true" onclick="javascript:itsm.cim.configureItemInfo.open_ci_relevance_win()" title="<fmt:message key="common.add"/>"></a>
                    </c:if>
                    <c:if test="${resAuth['/pages/ciRelevance!update.action'] }"> 
                        <a class="easyui-linkbutton" icon="icon-edit"  plain="true" id="link_ci_relevance_edit" onclick="itsm.cim.configureItemInfo.ciRelevanceEdit()" title="<fmt:message key="common.edit"/>"></a>
                    </c:if>
                    <c:if test="${resAuth['/pages/ciRelevance!delete.action'] }"> 
                        <a class="easyui-linkbutton" icon="icon-cancel"  plain="true" onclick="javascript:itsm.cim.configureItemInfo.ciRelevancetool_bar_delete_aff()"  title="<fmt:message key="common.delete"/>"></a>
                    </c:if>
                    <sec:authorize url="configItem_detail_configItemRelatingTreePic">
                    	<a class="easyui-linkbutton"  icon="icon-treeView" plain="true" id="ciRelevanceTree_but" onclick="itsm.cim.configureItemUtil.ciRelevanceTree(${ciDetailDTO.ciId},'configureItemInfo_ci_relevance_win','configureItemInfo_ci_relevance_tree')" title="<fmt:message key="ci.ciRelevanceTreeView" />"></a>
                   	</sec:authorize>
                   	<sec:authorize url="configItem_detail_configItemRelatingPic">
                   		<a class="easyui-linkbutton" icon="icon-relation"  plain="true" id="prefuser" onclick="itsm.cim.configureItemInfo.showPrefuse()" title="<fmt:message key="ci.Topological.Graph"/>"></a>
                   </sec:authorize>
                    </div>
                	<table id="configureItemInfoRelevanceCI" ></table>
					<div id="configureItemInfoRelevanceCIPager"></div>
                </div>
                </sec:authorize>
                
              	<sec:authorize url="/pages/ciSoftwareAction!findSoftwarePager.action">
                <div title="<fmt:message key="config.installSort"/>" style="padding:2px; width: 100%;">         
                	<table id="installSoftwareGrid"></table>
					<div id="installSoftwarePager" ></div>
                </div>
                </sec:authorize>
                <c:if test="${requestHave eq true}">
                <sec:authorize url="configItem_detail_relatingRequest">
                <div title="<fmt:message key="title.historyRequest"/>" style="padding:2px; width: 100%;">         
                	<table id="historyCall"></table>
					<div id="historyCallPager" ></div>
                </div>
                </sec:authorize>
                </c:if>
                <c:if test="${changeHave eq true}">
                <sec:authorize url="configItem_detail_relatingChange">
                <div title="<fmt:message key="title.change.historyChange"/>" style="padding:2px; width: 100%;">         
                	<table id="historyChangeGrid"></table>
					<div id="historyChangePager" ></div>
                </div>
                </sec:authorize>
                </c:if>
                <c:if test="${problemHave eq true}">
                <sec:authorize url="configItem_detail_relatingProblem">
                <div title="<fmt:message key="title.problem.historyProblem" />" style="padding:2px; width: 100%;">         
                	<table id="historyProblemGrid"></table>
					<div id="historyProblemPager" ></div>
                </div>
                </sec:authorize>
                </c:if>
                
                <!-- 历史更新 -->
                <sec:authorize url="configItem_detail_historyUpdate">
				<div title='<fmt:message key="label.customReport.history"/>'>
					<form>
                	<div id="cust_hisdiv_id" class="hisdiv">
                	
						<table id="configureItemHistory" class="histable" style="width:100%" cellspacing="1">
							<thead>
							<tr>
								<th width="15%"><fmt:message key="common.id"/></th>
								<th width="30%"><fmt:message key="label.customReport.versions"/></th>
								<th width="15%"><fmt:message key="label.operator"/></th>
								<th width="20%"><fmt:message key="common.updateTime"/></th>
								<th width="20%"><fmt:message key="title.dashboard.noticeDetails"/></th>
							</tr>
							</thead>
							<tbody>
								
							</tbody>
						</table>
                	</div>
                	</form>
				</div>
			</sec:authorize>
			
			
			
			  <!-- 二维码  <a onclick="itsm.cim.configureItemInfo.scanShow(${ciDetailDTO.ciId})" style="margin-left: 30px;" >显示</a> -->
                 <sec:authorize url="CONFIGITEM_DETAIL_TDCODE">
				<div title="<fmt:message key="label.ci.scan.tab"/>">
					
					<div style="background-color:#e4edfe;font-weight:bold;height: 26px;padding-top: 12px; margin-bottom:15px; padding-left: 10px;"><fmt:message key="label.ci.scan.title" /></div>
                	
                	 <a class="easyui-linkbutton l-btn" style="margin-left: 18px;" 
                	icon="icon-ok" onclick="itsm.cim.configureItemInfo.scanCreate('${ciDetailDTO.cino}','${ciDetailDTO.ciId}')"><fmt:message key="label.ci.scan.create"/></a> 
                	
                	<a href="ciScanAction!findImage.action?ciScanDTO.ciId=${ciDetailDTO.ciId}" style="margin-left: 30px;"  target=_blank><fmt:message key="label.problem.attachmentDownload"/></a>
                	<br/>
                	<br/>
                	<div>
	                	<div style="float: left;border: solid 1px; ">
	                		<table width="328px;">
	                			<tr>
	                			<%-- 	<td>
	                					<table>
	                						<tr><td style="font-size: 15px;font-weight: 900"><fmt:message key="lable.ci.Use"/>: ${ciDetailDTO.ciname}</td></tr>
	                						<tr>
				                				<td style="font-size: 15px;font-weight: 900">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IP: <span id="scan_CIip"></span></td>
				                			</tr>
				                			<tr><td style="font-size: 15px;font-weight: 900"><fmt:message key="ci.use"/>: ${ciDetailDTO.userName}</td></tr>
	                					</table>
	                				
	                				<td> --%>
	                					<div id="cust_QRcode_id" style="height: 70px;width: 328px;overflow: hidden;"></div>
	                				
	                			</tr>
	                		</table>
	                	</div>
	                	
	                	
	                		<%-- <img id="input_QRcode_id" src="${pageContext.request.contextPath}/QRCodePath/img_${ciDetailDTO.cino}.png" onerror="itsm.cim.configureItemInfo.imgClose()"/> --%>
	                		<!-- <img  src="./${pageContext.request.contextPath}/QRCodePath/img_hh.png"/> -->
	                	
                	</div>
				</div>
			</sec:authorize>
			
            </div>
		</div>	
	</div>
	<!-- 显示历史更新 -->
	 <div id="Cihistory" class="WSTUO-dialog" title="<fmt:message key="label.customReport.history.Detail"/>" style="width:580px;height:380px;">
			<div class="hisdiv">
			<table id="CihistoryInfo" class="histable_left" style="width:100%;" cellspacing="1">
				<tbody >
					
				</tbody>
			</table>
			</div>
	</div>
	
	<!-- 添加配置项关联 -->
	<div id="add_ci_relevance_win" title="<fmt:message key="common.add"/>/<fmt:message key="common.edit"/>-<fmt:message key="ci.relatedCI"/>" class="WSTUO-dialog" style="width:420px;height:auto;">
	    <form>
	    <input type="hidden" name="ciRelevanceDTO.ciRelevanceId" id="ciRelevanceId" />
	    <input type="hidden" name="ciRelevanceDTO.relevanceId" id="relevanceId" />
	    <div class="lineTableBgDiv" style="width: 98%">
	    <table style="width:100%" class="lineTable" cellspacing="1">
	     	<tr>
	            <td><fmt:message key="ci.relatedCI"/></td>
	            <td>
	            <input type="hidden" name="ciRelevanceDTO.unCiRelevanceId" id="unCiRelevanceId" />	
	            <input name="ciRelevanceDTO.unCiRelevanceName" id="unCiRelevanceName" class="input choose" readonly /></td>
	        </tr>
	        <tr>	
				<td><fmt:message key="label.dc.ciRelationType" /></td>
				<td>
					<select id="relevance_ciRelationType" name="ciRelevanceDTO.ciRelationTypeNo" class="input"></select>
				</td>
			</tr>
	        <tr>
	            <td><fmt:message key="common.remark"/></td>
	            <td>
	            <input name="ciRelevanceDTO.relevanceDesc" id="relevanceDesc" class="input"  /></td>
	        </tr>
	      	<tr>
	            <td colspan="2" align="center"> 
		             <a  class="easyui-linkbutton" plain="true" icon="icon-save" id="link_add_ci_relevance_ok"><fmt:message key="common.save"/></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	            </td> 
	      	</tr>
	      	
	      	
	    </table>
	    </div>
	    </form>
	</div>
	
	
	<!-- 选择配置项 -->
	<div id="ci_select_win" class="WSTUO-dialog" title="<fmt:message key="common.select"/><fmt:message key="ci.ci"/>" style="width:650px;height:500px;padding:5px;">
	    	<div class="easyui-layout" style="width:630px;height:400px;">
	    		<div region="west" split="true" title="<fmt:message key="ci.ci"/><fmt:message key="common.category"/>" style="width:180px;padding:10px;">
	    			<div id="ci_select_tree"></div>
	    		</div>
	    		
	    		<div region="center" title="<fmt:message key="ci.ci"/>" style="width:400px;padding:4px;">
	    			<table id="ci_select_grid"></table>
	    			<div id="ci_select_pager"></div>
	    		</div>
	    	</div>
	</div>
	
	
	
	
	<div id="ci_select_Toolbar" style="display:none">
		<form>
		<input id="ci_select_toolbar_search" name="ciQueryDTO.ciname" />
		<a class="easyui-linkbutton" plain="true" icon="icon-search" onclick="selectQuery('ci_select_Toolbar','ci_select_grid')"><fmt:message key="common.search"/></a>
		</form>
	</div>
	
	
	<!-- 关联配置项树 -->
	<div id="configureItemInfo_ci_relevance_win" title="<fmt:message key="ci.ciRelevanceTreeView" />" class="WSTUO-dialog" style="width:200px;height:auto">
		<div id="configureItemInfo_ci_relevance_tree"></div>
	</div>
	
	<!-- 安装的软件工具栏 -->
	<div id="installSoftwareGridToolbar" style="display: none">
        <%-- <sec:authorize url="/pages/ciSoftwareAction!saveSoftware.action">
		<a class="easyui-linkbutton" plain="true" icon="icon-add" id="installSoftwareGrid_add_but" onclick="javascript:itsm.cim.configureItemInfo.openAddConfigureItemSoftware()" title="<fmt:message key="common.add"/>"></a>
		</sec:authorize> --%>
        <sec:authorize url="/pages/ciSoftwareAction!deleteSoftware.action">
        <a class="easyui-linkbutton" plain="true" icon="icon-cancel" id="installSoftwareGrid_delete_but" onclick="javascript:itsm.cim.configureItemInfo.deleteConfigureItemSoftware()" title="<fmt:message key="common.delete"/>"></a>
	    </sec:authorize>
    </div>
	<!-- 选择关联软件窗口 -->
	<div id="configureItemInfo_installSoftware_win" title="<fmt:message key="common.select" />-<fmt:message key="label_scan_software" />" class="WSTUO-dialog" style="width:650px;height:auto">
		<%-- 数据列表 --%>
		<table id="configureItemInfoSoftwareMainGrid" ></table>
		<div id="configureItemInfoSoftwareMainPager"></div>
		<div id="configureItemInfo_installSoftwareToolbar" style="display:none">
		&nbsp;&nbsp;<fmt:message key="label_software_name" />:<input id="search_software_name" type="text">
		&nbsp;&nbsp;<a class="easyui-linkbutton" plain="true" icon="icon-search" onclick="javascript:itsm.cim.configureItemInfo.searchSoftware()"><fmt:message key="common.search" /></a>
	    &nbsp;&nbsp;<a class="easyui-linkbutton" plain="true" icon="icon-ok" onclick="javascript:itsm.cim.configureItemInfo.addConfigureItemSoftware()"><fmt:message key="label.determine" /></a>
		</div>
	
	</div>
	<div id="ciPrefuse"  class="WSTUO-dialog" title='<fmt:message key="msg.tips"/>' style="width:260px;height:auto">
		
		<img alt="temp" src="${pageContext.request.contextPath}/images/icons/messager_info.gif">
			
	
		<br/>
		<p style="text-align: center;"> <a class="easyui-linkbutton" icon="icon-ok" onclick="javascript:itsm.cim.configureItemInfo.ciPrefuseColse()"><fmt:message key="label.determine"/></a></p>
	</div>
	
	
		<%-- 自定义列设置窗口 --%>
	<div id="columnChooserWin_installSoftwareGrid"  class="WSTUO-dialog" title="<fmt:message key="label.set.column"/>" style="width:250px;height:300px;padding:1px;">
		<div id="columnChooserDiv_installSoftwareGrid" class="hisdiv">
			<table class="histable" style="width:100%" cellspacing="1">
				<thead>
				<tr>
					<td colspan="2">
						<a id="columnChooserTopBut_installSoftwareGrid" plain="true" class="easyui-linkbutton" icon="icon-ok"><fmt:message key="label.determine"/></a>
						<a id="columnChooserDefaultBut_installSoftwareGrid" plain="true" class="easyui-linkbutton" icon="icon-ok"><fmt:message key="label.dashboard.defautl.show"/></a>
						<span id="columnChooserStatus_installSoftwareGrid"></span>
					</td>
				</tr>
				
				<tr>
					<th style="padding-left:11px;"><input type="checkbox" id="colAllUnSelect_installSoftwareGrid"  onclick="colAllUnSelect('installSoftwareGrid')" /></th>
					<th><fmt:message key="label.colName" /></th>
				</tr>
				</thead>
				<tbody>
				</tbody>			
			</table>
		</div>
	</div>
	
</div>
