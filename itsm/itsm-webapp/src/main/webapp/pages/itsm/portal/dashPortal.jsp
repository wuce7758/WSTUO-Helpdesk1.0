<%@page import="java.util.Date"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page import="com.opensymphony.xwork2.ActionContext" %>  
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%> 
<%@ include file="../../language.jsp" %>
<div id="helpdeskPortalTabid" title="<fmt:message key="label.dashboard.my.panel" />" style="width:auto;padding:3px;height:auto;">
<div style="width: 100%;">

<div id="notPortalDiv" style=" background-color:#e4edfe;text-align:center; font-weight:bold;height: 20px;padding-top: 12px;"><fmt:message key="label.notPortal"/></div>

<input type="hidden" id="rowsWid" value="${userCustomDTO.layoutType}">
    
    <c:set var="rowWidth" value="50"></c:set>
    <c:if test="${userCustomDTO.layoutType eq 'oneRow'}">
    <c:set var="portalLayoutType" value="1"></c:set>
    </c:if>
    <c:if test="${userCustomDTO.layoutType eq 'twoRow'}">
    <c:set var="portalLayoutType" value="2"></c:set>
    </c:if>
    <c:if test="${userCustomDTO.layoutType eq 'threeRow'}">
    <c:set var="portalLayoutType" value="3"></c:set>
    </c:if>
    <c:if test="${userCustomDTO.layoutType eq 'fourRow'}">
    <c:set var="portalLayoutType" value="4"></c:set>
    </c:if>
    <c:forEach end="${portalLayoutType}" begin="1" step="1" var="data" varStatus="vs">
        <c:if test="${vs.count%portalLayoutType eq 1 or (portalLayoutType eq 1 and vs.count%portalLayoutType eq 0) }">
            <c:set var="rowWidth" value="${userCustomDTO.rowsWidth.oneRows}"></c:set>
        </c:if>
        <c:if test="${vs.count%portalLayoutType eq 2 or (portalLayoutType eq 2 and vs.count%portalLayoutType eq 0) }">
            <c:set var="rowWidth" value="${userCustomDTO.rowsWidth.twoRows}"></c:set>
        </c:if>
        <c:if test="${vs.count%portalLayoutType eq 3 or (portalLayoutType eq 3 and vs.count%portalLayoutType eq 0) }">
            <c:set var="rowWidth" value="${userCustomDTO.rowsWidth.threeRows}"></c:set>
        </c:if>
        <c:if test="${vs.count%portalLayoutType eq 4 or (portalLayoutType eq 4 and vs.count%portalLayoutType eq 0) }">
            <c:set var="rowWidth" value="${userCustomDTO.rowsWidth.fourRows}"></c:set>
        </c:if>
        
        <div class="column" id="column_${vs.count }" style="width:${rowWidth}%;height:auto;">
        </div>
    </c:forEach>
    
    
    <c:forEach items="${userCustomDTO.viewdtos}" var="data" varStatus="vs">
    
        <input type="hidden" id="divid_${data.viewId}" value="${data.viewName}">
        <script>
            startProcess();
            notPortal=false;
            var viewRowStr="${userCustomDTO.viewRowsStr}";
            var strIds=viewRowStr.split(':');
            $('#column_'+strIds[${(vs.count)-1}]).append('<div class="portlet" id="protlet_${data.viewId}">'+
                    '<div class="portlet-header" style="cursor: move;">${data.viewName}</div>'+
                    '<div class="portlet-content" id="protlet_content_${data.viewId}"><table width="100%" id="TAB_ID_${data.viewId}" class="table_css" cellspacing="1"></table>'+
                    '<div id="${data.viewId}_0">'+
                    '</div></div>'+
                    '</div>');
            endProcess();
        </script>
    </c:forEach>
    <script type="text/javascript">
    //alert(notPortal);
        if(notPortal){
            $('#notPortalDiv').show();
        }
    </script>
</div>

</div>

