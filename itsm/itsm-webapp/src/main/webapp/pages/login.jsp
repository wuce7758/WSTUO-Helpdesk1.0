﻿<%@page import="org.springframework.web.context.support.WebApplicationContextUtils"%>
<%@page import="org.springframework.context.ApplicationContext"%>
<%@page import="com.wstuo.common.tools.entity.PagesSet"%>
<%@page import="com.wstuo.common.tools.service.IPagesSetSerice"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%
response.setHeader("Cache-Control","no-store");
response.setHeader("Pragrma","no-cache");
response.setDateHeader("Expires",0);
String version="1.0";

request.setAttribute("version",version);

ApplicationContext ac = WebApplicationContextUtils.getWebApplicationContext(getServletContext());
IPagesSetSerice systemPage = (IPagesSetSerice)ac.getBean("pagesSetSerice");
PagesSet pagesSet=systemPage.showPagesSet();
request.setAttribute("pagesSet",pagesSet);

%>
<c:set var="browserLanguage" scope="session" value="${param.language}" />
<c:set var="language" value="${param.language}" />
<c:if test="${language ne ''}">
	<fmt:setLocale value="${language}"/>
	<fmt:setBundle basename="i18n.itsmbest"/>
	<c:set var="lang" value="${language}"/>
</c:if>


<c:if test="${language eq '' or empty language}">
	<fmt:setLocale value="${empty cookie['language'].value?'zh_CN':cookie['language'].value}"/>
	<fmt:setBundle basename="i18n.itsmbest"/>
	<c:set var="lang" value="${empty cookie['language'].value?'zh_CN':cookie['language'].value}"/>
</c:if>
<!-- 系统版本  -->
<%-- systemVersion：OEM版本控制,值非WSTUO为OEM版本(提供给我们合作伙伴的); --%>
<c:set var="systemVersion" value="WSTUO" />

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">   
	<script type="text/javascript">
		var browserLanguage = navigator.browserLanguage?navigator.browserLanguage:navigator.language;
	</script>	
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="WSTUO，免费的ITSM，IT服务流程管理软件，强大的自定义能力，让企业用的更顺手">
    <meta name="author" content="WSTUO">

	<link rel="stylesheet" href="../js/jquery/jquery-ui-1.11.4.custom/jquery-ui.min.css" />
    <!-- The styles -->
    <link id="bs-css" href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
	<link href="../css/charisma-app.css" rel="stylesheet">
	<link href='../css/jquery.noty.css' rel='stylesheet'>
<link href='../css/noty_theme_default.css' rel='stylesheet'>
	<link href='../css/jquery.iphone.toggle.css' rel='stylesheet'>
	<link href='../css/css.css' rel='stylesheet'>
	<!-- jQuery -->
	<script src="../bower_components/jquery/jquery.min.js"></script>

    <!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- The fav icon -->
    <link rel="shortcut icon" href="../images/logo2.png">
    
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- notification plugin -->
	<script src="../js/js/jquery.noty.js"></script>
	<script src="../js/js/jquery.cookie.js"></script>
	<script src="../js/i18n/i18n_${lang}.js"></script>
	<script src="../js/jquery/jquery.validate.min.js"></script>
	<script src="../js/jquery/messages_zh.js"></script>
	<script src="../js/jquery/jquery-ui-1.11.4.custom/jquery-ui.min.js"></script>
	
	<script type="text/javascript" src="../js/basics/jquery_common.js"></script>
	<script>var contextPath="${pageContext.request.contextPath}";</script>
	<script src="../js/basics/timeZone.js"></script>
	<script src="../js/basics/login.js"></script>
	
<script language="javascript">
function setLanguage(lang){
	setCookie('language',lang,300);
	// location.reload();
	location.href='login.jsp?loginName='+$('#j_username').val();
}
function show(msg){  
	noty({'text':msg,'layout':'topCenter','type':'error','animateOpen': {'opacity': 'show'}});
}
function browserLanguageFun(){
	var aQuery = window.location.href.split("?");//取得Get参数
	if(aQuery[1]!=undefined && aQuery[1]!=null){
		var aBuf = aQuery[1].split("=")[1];
		if(aBuf=='errorid')
			show(i18n['itsm_login_error']);
	}
	
	var cookieLanguage = getCookie('language');
	var paramLanguage='${param.language}';
	var strHref  =  location.href; 
	var i=strHref.length;
	var st=strHref.substring(i-5,i)
	if(st=="en_US"){
   	     setCookie('language',st,300);
    }
   if(st=="zh_CN"){
   	     setCookie('language',st,300);
    }
   if(st=="ja_JP"){
   	    setCookie('language',st,300);
    }
	if(cookieLanguage==undefined && (paramLanguage==null || paramLanguage=='')){
		if(browserLanguage=='zh-CN' || browserLanguage=='zh-cn')
			browserLanguage='zh_CN';
		else if(browserLanguage=='zh-TW' || browserLanguage=='zh-tw')
			browserLanguage='zh_TW';
		
		else if(browserLanguage=='en-US' || browserLanguage=='en-us')
			browserLanguage='en_US';
		
		else if(browserLanguage=='ja-JP' || browserLanguage=='ja-jp')
			browserLanguage='ja_JP';
		else
			browserLanguage='zh_CN';
		//alert("---"+browserLanguage);
		location.href='login.jsp?language='+browserLanguage
	}
}
</script>

</head>
<c:if test="${empty pagesSet.paName}">
	<title><fmt:message key="i18n.loginTitle"/> - <fmt:message key="i18n.mainTitle"/></title>
</c:if>
<c:if test="${!empty pagesSet.paName}">
	<title><fmt:message key="i18n.loginTitle"/> - ${pagesSet.paName}</title>
</c:if>
<c:if test="${empty pagesSet.paName}">
	<c:if test="${lang=='zh_CN'}">
		<c:set var="paname" value='ITSM管理系统'/>
	</c:if>
	<c:if test="${lang=='en_US'}">
		<c:set var="paname" value='ITSM System'/>
	</c:if>
	<c:if test="${lang=='ja_JP'}">
		<c:set var="paname" value='ITSMサービスマネジメントシステム'/>
	</c:if>
	<c:if test="${lang=='zh_TW'}">
		<c:set var="paname" value='ITSM管理系統  '/>
	</c:if>
</c:if>
<c:if test="${!empty pagesSet.paName}">
<c:set var="paname" value="${pagesSet.paName}"></c:set>
</c:if>

<body onload="browserLanguageFun()" style="margin:0px;background: #e7e9e8;font-size:12px; overflow: hidden;" >
<div class="ch-container">
    <div class="row">
        
    <div class="row">
        <div class="col-md-12 center login-header">
            <c:if test="${!empty pagesSet.imgname}">
            <img style="height:60px;min-width:150px;max-width:320px;" src="logo.jsp?picName=${pagesSet.imgname}" />
            </c:if>
            <c:if test="${empty pagesSet.imgname}">
            <img style="height:60px" src="../images/logo.png" />
            </c:if>
        </div>
        <!--/span-->
    </div><!--/row-->

    <div class="row">
        <div class="well col-md-5 center login-box">
            <div class="alert alert-info">
                <h2>${paname}</h2>
            </div>
           <form id="userLogin"  class="form-horizontal" name="userLogin" method="POST" action="${pageContext.request.contextPath}/j_spring_security_check" style="text-align:center;margin:0px">
             <fieldset>
                	<%-- <div class="input-group input-group-lg">
                        <label><fmt:message key="i18n.language"/></label>
                        <select id="languageSelect">
		                    <option value="zh_CN">简体中文</option>
		                    <option value="zh_TW">繁體中文</option>
		                    <option value="en_US">English</option>
		                    <option value="ja_JP">日本語</option>
		                </select>
                    </div>
                    <div class="clearfix"></div><br> --%>
                
                    <div class="input-group input-group-lg">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-user red"></i></span>
                       <input id="j_username" name="j_username" class="form-control" placeholder="用户名" required="true"/>
                    </div>
                    <div class="clearfix"></div><br>

                    <div class="input-group input-group-lg">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-lock red"></i></span>
                        <input type="password" name="j_password" id="j_password" class="form-control" placeholder="密码"  required="true"/>
                    </div>
                    <div class="clearfix"></div>

                    <div class="input-prepend">
                        <label class="remember" for="_spring_security_remember_me">
                        <input type="checkbox" name="_spring_security_remember_me" id="_spring_security_remember_me"/> <fmt:message key="i18n.cookieAlert"/></label>
                    </div>
                    <div class="clearfix"></div>

                    <p class="center col-md-5">
                     <input type="hidden" name="j_loginModel" id="j_loginModel" value="normal">
                        <button type="submit" class="btn btn-primary"><fmt:message key="i18n.doLogin"/></button>
                    </p>
                    <p class="center col-md-5">
                     <a href="javascript:getPassword()" style="width: 40%;padding: 11px">忘记密码</a><a  href="reg.jsp" style="width: 40%;padding: 11px">注册试用</a>
                    </p>
                    Helpdesk<%=version%> &copy; <a href="http://wstuo.com" target="_blank" >维思拓</a> 2016  &nbsp; WSTUO<sup>TM</sup>
                </fieldset>
            </form>
        </div>
        <!--/span-->
    </div><!--/row-->
</div><!--/fluid-row-->

</div><!--/.fluid-container-->
<div class="WSTUO-dialog" id="my-prompt" title="找回密码" style="width:300px">
  <div class="input-group input-group-lg">
     <input id="phone" class="form-control" placeholder="手机号码" required="true"/>
  </div>
  <div class="clearfix"></div><br>
  <div class="input-group input-group-lg">
     <input id="code" class="form-control" placeholder="验证码" required="true"/>
  </div><span id="code_tip"><a href="javascript:getCode()">获取验证码</a></span><br><br>
   <div class="input-group input-group-lg">
     <button type="button" onclick="resetPassword()" class="btn btn-primary">提交</button>
    </div>
</div>
<script>

var login_error='${param.error}';
var login_overonlinenontermianl='${param.overonlinenontermianl}';
	
if(login_error=='true'){
	setTimeout(function(){show('<fmt:message key="err.userNameOrPassword"/>');},500);  
	setCookie('cookieUserName','',0);
	setCookie('cookieUserPassword','',0);
}
if(login_overonlinenontermianl=='true'){
	setTimeout(function(){show(i18n['more_than_linen_count']);},500);  
}

if(login_error=='account_used'){
	setTimeout(function(){show(i18n['msg_account_used']);},500);  
	setCookie('cookieUserName','',0);
	setCookie('cookieUserPassword','',0);
}

$(function(){
	var lan="${cookie['language'].value}";
	if('${param.language}'==''){
		if(lan!=null && lan!=''){
		   $('#languageSelect').val(lan);
		}else{
		   $('#languageSelect').val(browserLanguage);
		}
	}else{
		$('#languageSelect').val('${param.language}');
	}
	//获取终端用户时区并保存到会话
	$.post('timeZone!loadClientTimeZone.action','clientTimeZone='+clientTimeZone('GMT').replace('+','^'),function(){});
	var _loginName = '${param.loginName}';
	if(_loginName!=null && _loginName!=''){
		$('#j_username').val(_loginName);
	}
})
var num=60;
var myVar;
function getPassword() {
	windows('my-prompt');
}
function resetPassword(){
	var phone=$("#phone").val();if(!phone){show('请输入手机号码！');return false;}
	var data=$("#code").val();if(!data){show('验证码错误！');return false;}
   	 $.post('../wap/validateCode.jsp',function(code){
   		if(code.indexOf(data)>-1){
			$.ajax({
		        type:"post",
		        url: "user!resetPassword.action",
		        data: "editUserPasswordDTO.loginName="+phone+"&editUserPasswordDTO.resetPassword=resetPassword",
		        dataType: "json",
		        success: function(res){
		        	if(res || res=='true'){
		        		show('密码重置成功！密码为：itsmbest');
		        		$('#my-prompt').dialog('close');
		        	}else{
		        		show('密码重置失败！');
			        	location.href="login.jsp";
		        	}
		        	clearInterval(myVar); 
		        	$("#code_tip").html("");
		        }
		    });
		}else
			show('验证码错误！');
   	 });
}
var jqueryDialog = {
		width:'auto',
		height:'auto',
		autoOpen: true,
		modal: true	
	};
function windows(id,option){
	var _p = {};
	option = $.extend({},option,{beforeClose:function(){beforeClose(id);}});
	_p = $.extend({},jqueryDialog,option);
	$('#'+id).dialog($.extend({},jqueryDialog,_p));
};
function checkPhone(value){
	var ck = /^1[3,5,8,9,7]\d{9}$/;
	return ck.test(value);
}
function getCode(){
	var phone=$("#phone").val();
	if(!checkPhone(phone)){
 	   show("手机格式错误！");
	   		return false;
	   	}
	$.ajax({
        type:"post",
        url: "sms!sendsms_reg.action",
        data: {mobiles:phone,content:'validate'},
        dataType: "json",
        success: function(res){
       		if(res.sendCount>0){
       			num=60;
       			$("#code_tip").html("请输入验证码,60秒后重新发送！");
				myVar=setInterval(function(){
					num-=1;
					if(num==0){
						clearInterval(myVar); 
						$("#code_tip").html('<a href="javascript:getCode()">重新发送</a>');
					}else
						$("#code_tip").html("请输入验证码,"+num+"秒后重新发送！");
				},1000); 
			}else{
				$("#code_tip").html('发送失败！<a href="javascript:getCode()">重新发送</a>');
			}
         }
    });
}
</script>
</body>
</html>
