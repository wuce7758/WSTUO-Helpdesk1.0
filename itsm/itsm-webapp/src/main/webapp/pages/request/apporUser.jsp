<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date" %>
<%@ page import="org.apache.struts2.ServletActionContext" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.opensymphony.xwork2.ActionContext" %>
<%@ page import="java.io.File,java.util.List,java.util.ArrayList,java.util.Date,com.wstuo.common.security.utils.AppConfigUtils"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="../../language.jsp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script src="${pageContext.request.contextPath}/scripts/jquery/jquery-1.4.2.min.js"></script>
<link rel="stylesheet" href="${pageContext.request.contextPath}/styles/comm.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/styles/style.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/scripts/jquery/easyui/themes/icon.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/scripts/jquery/jqgrid/redmond/jquery-ui-1.8.1.custom.css" />

<style>.tabs li{margin-left:5px}</style>
<script src="${pageContext.request.contextPath}/scripts/i18n/i18n_${lang}.js"></script>
<script src="${pageContext.request.contextPath}/scripts/jquery/jquery.xml2json.js"></script>
<script src="${pageContext.request.contextPath}/scripts/jquery/easyui/plugins/jquery.form.js"></script>
<%-- autocomplete--%>
<script src="${pageContext.request.contextPath}/scripts/jquery/jquery-ui-1.8.2.custom/development-bundle/ui/jquery.ui.core.js"></script>
<script src="${pageContext.request.contextPath}/scripts/jquery/jquery-ui-1.8.2.custom/development-bundle/ui/jquery.ui.widget.js"></script>
<script src="${pageContext.request.contextPath}/scripts/jquery/jquery-ui-1.8.2.custom/development-bundle/ui/jquery.ui.position.js"></script>
<script src="${pageContext.request.contextPath}/scripts/jquery/jquery-ui-1.8.2.custom/development-bundle/ui/jquery.ui.datepicker.js"></script>
<script src="${pageContext.request.contextPath}/scripts/jquery/jquery-ui-1.8.2.custom/development-bundle/ui/jquery.ui.dialog.js"></script>
<script src="${pageContext.request.contextPath}/scripts/jquery/jquery-ui-1.8.2.custom/development-bundle/ui/jquery.ui.button.js"></script>
<script src="${pageContext.request.contextPath}/scripts/jquery/easyui/plugins/jquery.resizable.js"></script>
<script src="${pageContext.request.contextPath}/scripts/jquery/easyui/plugins/jquery.validatebox.js"></script>
<script src="${pageContext.request.contextPath}/scripts/jquery/jquery.floatingmessage.js"></script>
<script src="${pageContext.request.contextPath}/scripts/jquery/easyui/locale/easyui-lang-${lang}.js"></script>
<script src="${pageContext.request.contextPath}/scripts/jquery/jstree/js/lib/jquery.cookie.js?random=${currentTime}"></script>
<script src="${pageContext.request.contextPath}/scripts/jquery/jstree/js/lib/jquery.hotkeys.js?random=${currentTime}"></script>
<script src="${pageContext.request.contextPath}/scripts/core/import.js?random=${currentTime}"></script>
<script src="${pageContext.request.contextPath}/scripts/core/package.js?random=${currentTime}"></script>
<script src="${pageContext.request.contextPath}/scripts/basics/jqgrid_common.js"></script>
<script src="${pageContext.request.contextPath}/scripts/basics/jquery_common.js"></script>
<script src="${pageContext.request.contextPath}/scripts/basics/easyui_common.js"></script>
<script src="${pageContext.request.contextPath}/scripts/basics/initUploader.js?random=${currentTime}"></script>
<script src="${pageContext.request.contextPath}/scripts/basics/ie6.js"></script>
<script src="${pageContext.request.contextPath}/scripts/jquery/uploadify/swfobject.js"></script>
<script src="${pageContext.request.contextPath}/scripts/jquery/jquery-json.js"></script>
<script src="${pageContext.request.contextPath}/scripts/itsm/request/apporUser.js"></script>

<script>
var mailEno ='${param.eno}';
var ocIndex ='${param.ocIndex}';
var mailPid = '${param.pid}';
var moduleType ='${param.moduleType}';
var random ='${param.random}';
var mailTaskId = '${param.taskId}';
</script> 
<fmt:setLocale value="${lang}"/>
<fmt:setBundle basename="i18n.itsmbest"/>
<c:set var="lang" value="${lang}"/>
<title><fmt:message key="lable.request.appor.user" /></title>
<body>
<%
String path=application.getRealPath("jasper");
File file=new File(path);
String images="logo.jpg";
File [] files=file.listFiles();
if(files!=null && files.length>0){
	for(File f:files){
		if(f.isFile()){
			images=f.getName();
		}
	}
}
%>
<c:set var="images" value="<%=images%>"/>

<center>
<div style="width:794px;">
	<table width="93%">
		<thead>
			<tr style="font-size:12px;" align=left>
				<th width="40%" align="left">
					<img width="200px" height="58px;" src="${pageContext.request.contextPath}/jasper/${images}?ramdon=<%=new Date().getTime() %>" /></th>
				<th align="left"><br/><font size="6px" ><fmt:message key="lable.request.appor.user" /></font></th>
			</tr>
		</thead>
	</table>

			<table border="1" cellspacing="0px" style="border-collapse:collapse;width:93%;border:1px solid black; display: none;" id="ApporTAB">
				<th><fmt:message key="lable.request.appor.user.msg"></fmt:message></th>	
				<tr>
					<td colspan="2"><textarea rows="" cols="" style="width: 730px;height: 300px;" id="apporText"></textarea></td>
				</tr>
				<tr>
					<td>
						<div id="addApporUser" style="padding: 5px;background-color: #dae6fc;" align="center">
							<input type="button" value="<fmt:message key="common.submit" />"/>
						</div>
					</td>
				</tr>
			</table>
</div>
<div id="mailHandingRepeatDiv"  style="display: none;">
	<span id="mailHandingRepeat" style="color:RoyalBlue;height:22px;width:300px;"><fmt:message key="lable.taskAlreadyCompletion" /></span>
</div>		
</center>
<div style="height: 0px;overflow: hidden;width: 0px;position:relative">
<div id="alertMSG" title="<fmt:message key="msg.tips" />">
	
</div>
</div>
<div id="addApporUserDIV" style="padding: 5px;background-color: #dae6fc; display: none;" align="center">
<fmt:message key="label.request.approval" />
</div>
</body>
</html>