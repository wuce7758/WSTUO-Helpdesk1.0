<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../../language.jsp" %>

<script type="text/javascript">
$(document).ready(function(){
	itsm.request.myGroupRequestTask.myGroupRequestTaskGrid();
});
</script>
<script src="../scripts/itsm/request/myGroupRequestTask.js"></script>
<div align="left">
	<div style="overflow:hidden;padding:4px;padding-right:6px">
		<table id="MyGroupRequestTaskGrid" width="100%"></table>
		<div id="MyGroupRequestTaskPager"></div>
	</div>
</div>
