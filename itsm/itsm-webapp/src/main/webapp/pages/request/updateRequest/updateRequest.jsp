<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ include file="../../../language.jsp" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<script src="../scripts/itsm/request/updateRequest/updateRequest.js"></script>


<script>

$(document).ready(function(){


	$('.loading').hide();
	$('.content').show();

	itsm.request.updateRequest.updateRequest.find();//加载数据

	$('#updateRequest_form_save').click(itsm.request.updateRequest.updateRequest.save);

	
});

</script>


<div class="loading" ><img src="../images/icons/loading.gif" /></div>

<div class="content" style="display:none">




<div class="lineTableBgDiv">
<form id="updateRequest_form">
<table style="width: 100%" class="lineTable" cellspacing="1">



	


	<tr>
		<td><fmt:message key="label.sla.ruleDetail"/></td>
		<td><fmt:message key="label.ur.request_overdue"/>
		
		 <input class="easyui-numberbox" style="width:50px" name="updateRequestDTO.day" id="edit_update_request_day" value="0" min="0" max="10000">
	     <fmt:message key="label.sla.day"/> , 
		 <input class="easyui-numberbox" style="width:50px" name="updateRequestDTO.hour" id="edit_update_request_hour" value="0" min="0" max="23">
	     <fmt:message key="label.sla.hour"/> ,  
	     <input class="easyui-numberbox" style="width:50px" name="updateRequestDTO.minute" id="edit_update_request_minute" value="0" min="0" max="59">
	     <fmt:message key="label.sla.minute"/>
		
		&nbsp;<fmt:message key="label.ur.request_upgrade"/>
		
		</td>
	</tr>
	<tr>
		<td><fmt:message key="common.enable"/></td>
		<td><input type="checkbox" name="updateRequestDTO.enable" id="edit_update_request_enable" value="true"/></td>
	</tr>
	
	<tr>
		<td colspan="2" style="padding-top:8px;">
			<input type="hidden" name="updateRequestDTO.id" id="edit_update_request_id"/>
			<a class="easyui-linkbutton" icon="icon-save" id="updateRequest_form_save"><fmt:message key="common.save"/></a>
		</td>
	</tr>
	
</table>
</form>
</div>
</div>
