<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
 <%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<li class="nav-header">系统设置</li>
<li class="accordion">
    <a href="#"><i class="glyphicon glyphicon-plus "></i><span> 基础数据</span></a>
    <ul class="nav nav-pills nav-stacked" style="display: block;">
    	<li class="active"><a href="wstuo/baseData/soft_setting.jsp" class="ajax-link ">软件设置</a></li>
        <sec:authorize url="ORGMG">
        	<li><a href="wstuo/orgMge/orgMge.jsp" class="ajax-link ">机构管理</a></li></sec:authorize>
		<sec:authorize url="USER">
        	<li><a href="wstuo/user/user.jsp" class="ajax-link">用户管理</a></li></sec:authorize>
		<sec:authorize url="ROLE">
        	<li><a href="wstuo/role/role.jsp" class="ajax-link">角色管理</a></li></sec:authorize>
		<sec:authorize url="ITSOP_MAIN">
        	<li><a href="wstuo/customer/customer.jsp" class="ajax-link">客户管理</a></li></sec:authorize>
		<sec:authorize url="module_MGE">
        	<li><a href="wstuo/sysMge/moduleManage.jsp" class="ajax-link">模块管理</a></li></sec:authorize>
		<sec:authorize url="SEC_DICTIONARY">
        	<li><a href="wstuo/dictionary/dataDictionaryGroupManage.jsp" class="ajax-link">数据字典</a></li></sec:authorize>
		
    </ul>
</li>
<sec:authorize url="SEC_RULES">
<li class="accordion" >
    <a href="#"><i class="glyphicon glyphicon-plus"></i><span> 业务规则</span></a>
    <ul class="nav nav-pills nav-stacked" >
    	<sec:authorize url="SLSL"><li><a href="wstuo/slaMge/slaMain.jsp"  class="ajax-link">SLA服务管理</a></li></sec:authorize>
		<sec:authorize url="WORKFLOW_PACKAGE_opt"><li><a href="wstuo/rules/packageMain.jsp"  class="ajax-link">流程规则包</a></li></sec:authorize>		
		<sec:authorize url="operation_rule_requestRule_opt"><li ><a href="wstuo/rules/ruleMain.jsp?flag=requestFit" id="showRequestFit" class="ajax-link">流程规则</a></li></sec:authorize>
		<sec:authorize url="operation_rule_mailTurnRequestRule_opt"><li >
			<a href="wstuo/rules/ruleMain.jsp?flag=mailToRequest" id="showMailToRequest" class="ajax-link">邮件转请求</a>
		</li></sec:authorize>
    </ul>
</li>
</sec:authorize>
<sec:authorize url="SEC_WORKFLOW">
<li class="accordion">
    <a href="#"><i class="glyphicon glyphicon-plus"></i><span> 流程管理</span></a>
    <ul class="nav nav-pills nav-stacked">
		<sec:authorize url="WORKFLOW_MAIN_opt"><li><a href="wstuo/jbpmMge/ProcessDefined.jsp" class="ajax-link">流程列表</a></li></sec:authorize>
		<sec:authorize url="WORKFLOW_DES_opt"><li><a href="${pageContext.request.contextPath}/jbpm" target="_blank">自定义流程</a></li></sec:authorize>
    </ul>
</li>
</sec:authorize>
<sec:authorize url="SEC_CUSTOMFORM">
<li class="accordion">
    <a href="#"><i class="glyphicon glyphicon-plus"></i><span> 表单自定义</span></a>
    <ul class="nav nav-pills nav-stacked">
		<sec:authorize url="Field_MGE_opt"><li><a href="wstuo/customForm/fieldMain.jsp" class="ajax-link">字段管理</a></li></sec:authorize>
		<sec:authorize url="FORM_MGE_opt"><li><a href="wstuo/customForm/customFormMain.jsp" class="ajax-link">表单管理</a></li></sec:authorize>
    </ul>
</li>
</sec:authorize>
<sec:authorize url="TOOLS_MAIN">
<li class="accordion">
    <a href="#"><i class="glyphicon glyphicon-plus"></i><span> 辅助工具</span></a>
	    <ul class="nav nav-pills nav-stacked">
	    <sec:authorize url="AFFICHE"><li><a href="wstuo/affiche/affiche.jsp" class="ajax-link">公告管理</a></li></sec:authorize>
	     <sec:authorize url="SMS"><li><a href="wstuo/sms/sms.jsp" class="ajax-link">手机短信</a></li></sec:authorize>
	     <sec:authorize url="/pages/email!find.action"><li><a href="wstuo/email/email.jsp" class="ajax-link">邮件管理</a></li></sec:authorize>
    	<sec:authorize url="IMSL"><li><a href="wstuo/infoMge/infoMge.jsp" class="ajax-link">消息管理</a></li></sec:authorize>
    </ul>
</li>
</sec:authorize>
<sec:authorize url="SYSTEM_TOOL_RES">
<li class="accordion">
    <a href="#"><i class="glyphicon glyphicon-plus"></i><span> 系统工具</span></a>
	    <ul class="nav nav-pills nav-stacked">
    	<sec:authorize url="SCHEDULEDTASK_RES"><li><a href="wstuo/sysMge/sysMge_scheduledTask.jsp" class="ajax-link">定期任务</a></li></sec:authorize>
		<sec:authorize url="UTILITY_TABUTILITY_DASHBOARD"><li><a href="wstuo/sysMge/sysMge_noticeGrid.jsp" class="ajax-link">通知模板</a></li></sec:authorize>
		<!-- <sec:authorize url="ORGMG"><li><a href="newPage1.html" class="ajax-link">模板管理</a></li></sec:authorize> -->
		<%-- <sec:authorize url="timeZone_set"><li><a href="wstuo/sysMge/sysMge_timeZoneSet.jsp" class="ajax-link">时区设置</a></li></sec:authorize>
		<sec:authorize url="CURRENCYSET_RES"><li><a href="wstuo/sysMge/sysMge_currencySet.jsp" class="ajax-link">货币设置</a></li></sec:authorize> --%>
		<sec:authorize url="wstuo/sysMge/sysMge_Thirdjk.jsp"><li><a href="wstuo/sysMge/sysMge_Thirdjk.jsp" class="ajax-link">第三方接口</a></li></sec:authorize>
    </ul>
</li>
</sec:authorize>
<sec:authorize url="SEC_LOG">
<li class="accordion">
    <a href="#"><i class="glyphicon glyphicon-plus"></i><span> 日志管理</span></a>
    <ul class="nav nav-pills nav-stacked">
		<sec:authorize url="/pages/useronlinelog!findPager.action"><li><a href="wstuo/onlinelog/userOnlineLog.jsp" class="ajax-link">用户在线日志</a></li></sec:authorize>
		<sec:authorize url="/pages/useroptlog!findPager.action"><li><a href="wstuo/onlinelog/userOptLog.jsp" class="ajax-link">用户操作日志</a></li></sec:authorize>
    </ul>
</li>	
</sec:authorize>