<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
    <%@ include file="../../language.jsp" %>
<style type="text/css">
	ul.rightTools {float:right; display:block;}
	ul.rightTools li{float:left; display:block; margin-left:5px}
</style>

<div class="pageContent">
	<div class="tabs">
		<div class="tabsHeader">
			<div class="tabsHeaderContent">
				<ul>
					<li><a href="javascript:;"><span>机构列表</span></a></li>
				</ul>
			</div>
		</div>
		<div class="tabsContent">
			<div>
				<div layoutH="146" id='selectUser_organizationTree' style="float:left; display:block; overflow:auto; width:240px; border:solid 1px #CCC; line-height:21px; background:#fff">
				</div>
				
				<div style="margin-left:246px;" class="unitBox">
					<div id ="selectUser_userSearch" style="border-top: #99bbe8 1px solid; border-bottom: #99bbe8 1px solid; border-left:#99bbe8 1px solid ; border-right:#99bbe8 1px solid ;padding:8px;margin-left: 2px;">
						<form id="userRoleFm" onsubmit="return false" >
							&nbsp;&nbsp;<fmt:message key="title.orgSettings.role" />&nbsp;&nbsp;
							<select id="selectUser_userGrid_Roles" name="userQueryDto.roleNo" style="width:170px"></select>
							<a class="easyui-linkbutton" plain="true" icon="icon-ok" id="index_related_user_grid_select"><fmt:message key="common.select"/></a>
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<fmt:message key="label.loginName" />&nbsp;
							<input type="text" id="selectUser_userGrid_loginName" name="userQueryDto.loginName" />
							&nbsp;&nbsp;
							<input type="hidden" name="userQueryDto.orgNo" id="searchSelectUser_userGrid_orgNo" >
							<div class="buttonActive" style="float:right"><div class="buttonContent"><button type="button" id="selectUser_SearchSelect">检索</button></div></div>
						</form>
					</div>
					<div title="<fmt:message key="label.userList" />" style="padding:10px 3px 3px 3px">
						<table id="selectUser_userGrid"></table>
						<div id="selectUser_userGridPager"></div>
					</div>
				</div>
	
			</div>
		</div>
		<div class="formBar">
			<ul>
				<!--<li><a class="buttonActive" href="javascript:;"><span>保存</span></a></li>-->
				<li><div class="buttonActive"><div class="buttonContent"><button type="button">确认选择</button></div></div></li>
				<li>
					<div class="button"><div class="buttonContent"><button type="button" class="close">取消</button></div></div>
				</li>
			</ul>
		</div>
	</div>
	
</div>

    