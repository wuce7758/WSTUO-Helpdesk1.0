<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@ include file="../../language.jsp" %>     
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>选择已有的附件</title>
</head>
<script type="text/javascript">
$(function(){
	//绑定日期控件
	DatePicker97(['#search_startTime','#search_endTime']);
});
</script>
<body>


<!-- 附件列表 -->
<div id="index_choose_attachment_window"  class="WSTUO-dialog" title="<fmt:message key="attachment.grid"/>" style="width:auto;height:auto">
	<table id="attachmentGrid"></table>
    <div id="attachmentGridPager"></div>
	<div id="attachmentGridToolbar" style="display:none;">
	<table><tr><td>
		<form id="topForm">
		 <input type="hidden"" id="addOredit"/>
		 <input type="hidden" id="pageName"/>
		 <input type="hidden" id="allType" name="dto.type"/>
		<span>&nbsp;&nbsp;<fmt:message key="label.type" /></span>
		|&nbsp;<select id="attachmentType" name="dto.url" onchange="wstuo.tools.chooseAttachment.search()">
				<option value="">--<fmt:message key="common.pleaseSelect" />--</option>
				<option>.doc</option>
				<option>.docx</option>
				<option>.ppt</option>
				<option>.pptx</option>
				<option>.txt</option>
				<option>.csv</option>
				<option>.xls</option>
				<option>.rar</option>
				<option>.zip</option>
				<option>.pdf</option>
				<option>.jpg</option>
				<option>.png</option>
				<option>.gif</option>
				<option>.bmp</option>
				<option>.swf</option>
				<option>.xlsx</option>
				<option>.vsd</option>
		</select></form>
		</td><td>
		&nbsp;&nbsp;<fmt:message key="label.problem.attachmentName" /> 
		|&nbsp;<input name="dto.attachmentName" id="attachmentNameSearch">
		&nbsp;<a class="easyui-linkbutton" href="javascript:wstuo.tools.chooseAttachment.search()" id="attachmentGrid_select" icon="icon-search" plain="true" title="<fmt:message key="common.search" />"></a>
		</td></tr>
	</table>
</div> 
</div>

<div id="searchAttachment" class="WSTUO-dialog" title="<fmt:message key="attachment.search" />" style="width:400px;;height:auto">
<div class="lineTableBgDiv" >
     	 <form id="attachmentForm">
       		<table style="width:100%" class="lineTable" cellspacing="1">
	<tr>
		<td style="width:40%" ><fmt:message key="attachment.Name" /></td>
		<td style="width:60%">
			<input name="dto.attachmentName" id="searchAttachmentName" class="input">
			<input name="dto.type" type="hidden" id="searchAttachmentType">
		</td>
	</tr>
	<tr>
		<td style="width:40%" ><fmt:message key="label.type" /></td>
		<td style="width:60%">
			<select name="dto.url">
				<option value="">--<fmt:message key="common.pleaseSelect" />--</option>
				<option>.doc</option>
				<option>.txt</option>
				<option>.csv</option>
				<option>.xls</option>
				<option>.docx</option>
				<option>.rar</option>
				<option>.zip</option>
				<option>.pdf</option>
				<option>.jpg</option>
				<option>.png</option>
				<option>.gif</option>
				<option>.bmp</option>
				<option>.swf</option>
				<option>.xlsx</option>
				<option>.vsd</option>
			</select>
		</td>
	</tr>
<%-- 	<tr>
		<td style="width:40%" ><fmt:message key="attachment.creator" /></td>
		<td style="width:60%">
			<input name="dto.creator" id="searchCreator" type="text" style="color:#555;cursor:pointer;width: 80%;">
			<a onclick="cleanIdValue('searchCreator','searchCreator')"> <fmt:message key="label.request.clear" /></a>
		</td>
	</tr >--%>
	<tr>
		<td style="width:40%" ><fmt:message key="attachment.date" /></td>
		<td style="width:60%">
		  		<input id="search_startTime" readonly="readonly"  name="dto.startTime" class="input"  style="width:65px"/>&nbsp;<fmt:message key="setting.label.to" />
		         &nbsp;<input id="search_endTime" readonly="readonly" name="dto.endTime" class="input easyui-validatebox"  style="width:65px" validType="DateComparison['search_startTime']"/>
		  	</td>
	</tr>
	<tr>
		<td style="width:40%" ><fmt:message key="attachment.size" /></td>
		<td style="width:60%">
			<input name="dto.startSize" id="startSizeHiedden" type="hidden"/>
			<input name="dto.endSize" id="endSizeHiedden" type="hidden"/>
			<input style="width: 65px;" id="search_startSize" class="easyui-validatebox" validType="decimal">&nbsp;<fmt:message key="setting.label.to" />
			 &nbsp;<input style="width: 65px;" id="search_endSize" class="easyui-validatebox" validType="decimal">&nbsp;KB
		</td>
	</tr>
	
	<tr>
		<td colspan="2">
			<a class="easyui-linkbutton"  id="attachment_common_dosearch" icon="icon-search" style="margin-top: 8px;"><fmt:message key="common.search" /></a>
			
		</td>
	</tr>
</table>

</form>
</div> 
</div>


<div id="imagesAttachment" class="WSTUO-dialog" style="width:auto;height:auto">

</div>
</body>
</html>