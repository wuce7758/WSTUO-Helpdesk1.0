<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@ include file="../../language.jsp" %>     

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>常用分类includes文件</title>
<style>
.filterTitle{
	font-weight: bold; text-align: center; 
}

.row {
    margin-right: 0px;
    margin-left: 0px;
}

</style>
</head>
<body>
<script src="../js/wstuo/customFilter/shareGroup.js"></script>
<!-- 自定义查询过滤器 start -->
<div id="customFilterWindow" class="WSTUO-dialog" title="<fmt:message key="lable.customFilter.self_defined"/>" style="width:580px; height:auto; padding:3px;line-height: 20px;">
	<form>
    	<div class="easyui-panel" style="padding:5px">
			<button type="button" class="btn btn-default btn-xs" id="customFilter_save" style="float: left;margin-left: 10px;"><i class="glyphicon glyphicon-floppy-disk"></i><fmt:message key="common.save"/></button>
			<input type="hidden" id="loadShareGroupGrid_sign" value=""/><!--标记记录共享组 will  -->
			<input type="hidden" id="loadShareGroupGrid"/><!--标记记录共享组 will  -->
			<button type="button" class="btn btn-default btn-xs" id="customFilter_delete" style="float: left;margin-left: 5px;" title="<fmt:message key="common.delete"/>"><i class="glyphicon glyphicon-trash"></i><fmt:message key="common.delete"/></button>
			<span style="float: left;margin-left: 10px;"><select id="filterByUser" style="width: 200px;" class="form-control" onchange="wstuo.customFilter.filterGrid_Operation.getFilterSearchByUser(this.value)"></select></span>
		</div>
		<div class="row">
			<div class="box col-md-12">
				<div class="box-inner">
					<div class="box-header well">
						<h2><fmt:message key="lable.customFilter.basic_info"/></h2>
					</div>
					<div class="box-content " style="padding: 0px;">
						
				  <div class="lineTableBgDiv">
				  
				  <div class="panel panel-default">
						  <div class="panel-body form-horizontal" style="padding: 0px;">
						  <input type="hidden" id="filter_Id">
						    <table style="width:100%" class="lineTable" cellspacing="1">
										<tr>
											<td class="filterTitle" style="width:30%;"><fmt:message key="lable.customFilter.filterName"/></td>
											<td class="filterTitle" style="width:30%;"><fmt:message key="lable.customFilter.filterDesc"/></td>
											<td class="filterTitle" style="width:40%;"><fmt:message key="label.filter.share"/></td>
										</tr>
										<tr>
										    <td style="padding: 5px;">
										      <input id="filter_name" name="customFilterDTO.filterName" class="form-control" required="true" validType="length[1,200]"/>
										    </td>
										    <td style="padding: 5px;">
										       <input id="filter_desc" name="customFilterDTO.filterDesc" class="form-control"/>
										    </td>
										    <td style="padding: 5px;">
												<select id="customFilterDTO_share" onchange="checkShare(this.value)" class="form-control">
													<option value="private" selected="selected"><fmt:message key="label.filter.private"/></option>
													<option value="share"><fmt:message key="label.filter.isshare"/></option>
													<option value="sharegroup"><fmt:message key="label.filter.sharegroup"/></option>
												</select>
												<a href="#" id="selectShareGroup" style="display:none;margin-left:15px"><fmt:message key="label.filter.viewslectGroup"/></a>
												<script type="text/javascript">
												 function checkShare(vl){
													$('#customFilterDTO_share_vl').val(vl);
													if(vl=='sharegroup'){
														$('#selectShareGroup').show();		
													}else{
														$('#selectShareGroup').hide();
													}
												} 
												</script>
												<input type="hidden" id="customFilterDTO_share_groups" name="orgs" >
												<input type="hidden" name="customFilterDTO.share" id="customFilterDTO_share_vl" value="private"/>
										    
										    </td>
										</tr>
						    </table>
						  
		                     
				          </div>
					</div>
					
					</div>
						
						</div>
					</div>
				</div>
			</div>
			
			
			<div class="row">
				<div class="box col-md-12">
					<div class="box-inner">
						<div class="box-header well" data-original-title="">
							<h2>
								<fmt:message key="lable.customFilter.AEConditions"/>
							</h2>
						</div>
						<div class="box-content " style="padding: 0px;">
						
						<div class="lineTableBgDiv">

								<div class="panel panel-default">
									<div class="panel-body form-horizontal" style="padding: 0px;">
									
									<table style="width:100%" class="lineTable" cellspacing="1">
										<tr>
											<td class="filterTitle" style="width:20%;"><fmt:message key="lable.customFilter.field"/></td>
											<td class="filterTitle" style="width:20%;"><fmt:message key="lable.customFilter.method"/></td>
											<td class="filterTitle" style="width:30%;"><fmt:message key="lable.customFilter.vaile"/></td>
											<td class="filterTitle" style="width:15%;"><fmt:message key="lable.customFilter.relation"/></td>
											<td class="filterTitle" style="width:15%;"><fmt:message key="lable.customFilter.operation"/></td>
										</tr>
										<tr>
										    <td style="padding: 5px;">
										       <select id="express_propName" class="form-control"></select>
												<select id="express_attrName" style="width:96%;margin-top:2px;display: none">
												   <option>请选择扩展字段</option>
												</select>
										    </td>
										    <td style="padding: 5px;">
										       <select id="express_operator" class="form-control"></select>
										    </td>
										    <td style="padding: 5px;">
										       <div id="express_propValueDIV">
													<input id="express_propertyValue" size="10" value="" class="form-control"/>
												</div>
												<div style="display:none;" id="propValue_ToDataDIV">
													<input id="express_propertyValue" class="form-control choose" readonly/>
												</div>
										    </td>
										    <td style="padding: 5px;text-align: center;">
										    	
										        <input id="express_or" type="radio" name="express_joinType" value="or" checked/>&nbsp;Or
								               &nbsp;
								                <input id="express_and" type="radio" name="express_joinType" value="and" />&nbsp;And
								              
										    </td>
										    <td align="center">
										        <button style="margin-right: 20px;" class="btn btn-primary btn-sm" type="button" id="filterGrid_addExpressToList"><fmt:message key="label.rule.confirm"/></button>
										    </td>
										</tr>
									</table>
								</div>
						</div>
				</div>
						
						</div>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="box col-md-12">
					<div class="box-inner">
						<div class="box-header well" >
							<h2>
								<fmt:message key="lable.customFilter.conditionList"/>
							</h2>
						</div>
						<div class="box-content " style="padding: 0px;">
						
						<div class="lineTableBgDiv" >
					<table id="customFilter_constraintsTable" style="width:100%" class="lineTable" cellspacing="1">
						<thead>
						<tr style="text-align: center;">
							<td ><fmt:message key="lable.customFilter.field"/></td>
							<td ><fmt:message key="lable.customFilter.method"/></td>
							<td ><fmt:message key="lable.customFilter.vaile"/></td>
							<td ><fmt:message key="lable.customFilter.relation"/></td>
							<td ><fmt:message key="lable.customFilter.operation"/></td>
						</tr>
						</thead>
						<tbody></tbody>
					</table>
				</div>
						</div>
					</div>
				</div>
			</div>
		</form>		
</div>
<!-- 自定义查询过滤器 end -->

<!-- 过滤器选择分享组窗口. -->
<div id="filtershareGroupWindow" class="WSTUO-dialog" title="<fmt:message key="label.filter.viewslectGroup" />" style="width:500px;height:200px;padding:3px">
<table id="filtershareGroupGrid" width="100%"></table>	
<div id="filtershareGroupGridPager"></div> 
<div id="filtershareGroupGridToolbar" style="display:none">
	<a class="btn btn-default btn-xs" id="filtershareGroupGrid_add" ><i class="glyphicon glyphicon-plus"></i> <fmt:message key="label.sla.append"/></a>
	<a class="btn btn-default btn-xs" id="filtershareGroupGrid_remove" title=""><i class="glyphicon glyphicon-trash"></i> <fmt:message key="label.sla.remove"/></a>
    <a class="btn btn-default btn-xs"  id="filtershareGroupGrid_enter" title=""><i class="glyphicon glyphicon-ok"></i> <fmt:message key="label.determine"/></a> 
	<%-- <a class="btn btn-default btn-xs" icon="icon-ok" id="shareGroupGrid_save" title=""><fmt:message key="label.determine"/></a> --%>
</div>

</div>


<!-- 选择被服务机构 -->

<div id="selectShareGroupTree" title='<fmt:message key="label.filter.selectGroup"/>' class="WSTUO-dialog" style="width:250px;height:400px; padding:10px; line-height:20px;" >
	
	<div id="selectShareGroupTreeDiv" ></div>
	<br/>
	<div style="border:#99bbe8 1px solid;padding:5px;text-align:center">
		<a class="btn btn-default btn-xs"  id="shareGroupGrid_getSelectedNodes" ><i class="glyphicon glyphicon-ok"></i> <fmt:message key="label.sla.confirmChoose"/></a> 
	</div>
</div>

<script>

</script>
</body>
</html>
