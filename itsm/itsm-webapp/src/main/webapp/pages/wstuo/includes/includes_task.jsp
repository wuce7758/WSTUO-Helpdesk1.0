<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@ include file="../../language.jsp" %>    

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>事件任务includes文件</title>
</head>
<body>
<script type="text/javascript">
/*function showRealTime(){
	$("#realStartTimeTr").removeAttr("style");
	$("#realEndTimeTr").removeAttr("style");
	$("#realFreeTr").removeAttr("style");
}

function hiddenRealTime(){
	$("#realStartTimeTr").attr("style","display:none;");
	$("#realEndTimeTr").attr("style","display:none;");
	$("#realFreeTr").attr("style","display:none;");
}

function calculationRealFree(){
	var dayTime = $("#index_eventTaskDTO_taskCostDay").val();
	var hourTime = $("#index_eventTaskDTO_taskCostHour").val();
	var minuteTime = $("#index_eventTaskDTO_taskCostMinute").val();
	var totle = dayTime*24*60+hourTime*60+minuteTime*1;
	$("#index_eventTaskDTO_realFree").val(totle);
} */
</script>
<!-- 添加编辑任务 start -->
<div id="index_addEditTask_window" class="WSTUO-dialog" title="<fmt:message key="title.request.addEditTask" />" style="width:450px;height:auto">
<form id="index_addEditTask_form">

	<input type="hidden" name="eventTaskDTO.taskId" id="index_eventTaskDTO_taskId" />
	<input type="hidden" name="eventTaskDTO.eno" id="index_eventTaskDTO_eno"/>
	<input type="hidden" name="eventTaskDTO.eventType" id="index_eventTaskDTO_eventType" />
	<div class="lineTableBgDiv">
      		<table style="width:100%" class="lineTable" cellspacing="1">
		<tr>
			<td style="width:30%"><fmt:message key="common.title"/></td>
			<td style="width:70%">
				<input name="eventTaskDTO.etitle" id="index_eventTaskDTO_taskTitle" validType="nullValueValid"  class="easyui-validatebox input" required="true" />
			</td>
		</tr>
		<tr>
			<td><fmt:message key="lable.task.location"/></td>
			<td>
				<input name="eventTaskDTO.location" id="index_eventTaskDTO_taskLocation" class="easyui-validatebox input" />
			</td>
		</tr>
		<tr>
			<td><fmt:message key="label.user.description" /></td>
			<td>
				<textarea rows="3" cols="50" name="eventTaskDTO.introduction" id="index_eventTaskDTO_introduction" class="easyui-validatebox input" ></textarea>
			</td>
		</tr>
		<tr id="eventTaskStatusTr" style="display:none;">
			<td><fmt:message key="common.state" /></td>
			<td>
				<input type="radio" name="eventTaskDTO.taskStatus" id="index_eventTaskDTO_statusNew" value="0" checked="checked" onclick="hiddenRealTime()"/><fmt:message key="title.newCreate" />
				<input type="radio" name="eventTaskDTO.taskStatus" id="index_eventTaskDTO_statusProcessing" value="1" onclick="hiddenRealTime()"/><fmt:message key="title.processing" />
				<input type="radio" name="eventTaskDTO.taskStatus" id="index_eventTaskDTO_statusDone" value="2" onclick="showRealTime()"/><fmt:message key="titie.complete" />
			</td>
		</tr>
		<tr>
			<td><fmt:message key="lable.scheduledStartTime"/></td>
			<td>
				<input name="eventTaskDTO.startTime" id="index_eventTaskDTO_startTime" class="easyui-validatebox choose" required="true" readonly="readonly"/>
			</td>
		</tr>
		<tr>
			<td><fmt:message key="lable.scheduledEndTime"/></td>
			<td>
				<input name="eventTaskDTO.endTime" id="index_eventTaskDTO_endTime" class="easyui-validatebox input" required="true" validType="DateComparison['index_eventTaskDTO_startTime']" readonly/>
			</td>
		</tr>
		<tr id="realStartTimeTr" style="display:none;">
			<td><fmt:message key="lable.scheduledRealStartTime"/></td>
			<td>
				<input name="eventTaskDTO.realStartTime" id="index_eventTaskDTO_realStartTime" class="choose" required="true"  readonly/>
			</td>
		</tr>
		<tr id="realEndTimeTr" style="display: none;">
			<td><fmt:message key="lable.scheduledRealEndTime"/></td>
			<td>
				<input name="eventTaskDTO.realEndTime" id="index_eventTaskDTO_realEndTime" class="input" required="true" validType="DateComparison['index_eventTaskDTO_realStartTime']" readonly/>
			</td>
		</tr>
		<tr id="realFreeTr" style="display:none;">
			<td><fmt:message key="lable.scheduledRealFree"/></td>
			<td>
				<input style="width: 40px" value="0" id="index_eventTaskDTO_taskCostDay" class="easyui-numberbox"  onkeyup="calculationRealFree()">天
				<input style="width: 40px" value="0" id="index_eventTaskDTO_taskCostHour" class="easyui-numberbox"  onkeyup="calculationRealFree()">时
				<input style="width: 40px" value="0" id="index_eventTaskDTO_taskCostMinute" class="easyui-numberbox"  onkeyup="calculationRealFree()">分
				<input type="hidden" name="eventTaskDTO.realFree" id="index_eventTaskDTO_realFree"/>
			</td>
		</tr> 
		<tr>
			<td><fmt:message key="task.allDay" /></td>
			<td>
				<input type="checkbox" name="eventTaskDTO.allDay" value="true" id="index_eventTaskDTO_allDay" />
			</td>
		</tr>
		<tr>
			<td><fmt:message key="task.ownerName" /></td>
			<td>
				<input style="width:90%" id="index_eventTaskDTO_owner"  name="eventTaskDTO.owner"  class="easyui-validatebox input" type="hidden" />
				<input style="width:90%" id="index_eventTaskDTO_owner_fullName"  class="easyui-validatebox input" required="true" readonly />
			<a class="easyui-linkbutton" plain="true" icon="icon-clean" onclick="cleanIdValue('index_eventTaskDTO_owner')" title="<fmt:message key="label.request.clear" />">
			</td>
		</tr>
		<tr style="display: none;">
			<td><fmt:message key="tool.affiche.creator" /></td>
			<td>
				<input id="index_eventTaskDTO_creator"  name="eventTaskDTO.creator" class="input" value="${loginUserName}"  readonly />
			</td>
		</tr>
		<tr id="eventTaskTreatmentResultsTr" style="display:none;">
			<td><fmt:message key="label.processingResults"/></td>
			<td>
				<textarea rows="3" cols="50" name="eventTaskDTO.treatmentResults" id="index_eventTaskDTO_treatmentResults" class="easyui-validatebox input" ></textarea>
			</td>
		</tr>
		<tr>
			<td colspan="2">
			    <input type="hidden" id="index_eventTaskDTO_operator" name="eventTaskDTO.operator" value="${loginUserName}"/>
				<a  class="easyui-linkbutton" icon="icon-save" id="index_addEditTask_save"><fmt:message key="common.save" /></a>
			</td>
		</tr>
	</table>
	</div>
</form>
</div>
<!-- 添加编辑任务 end -->
</body>
</html>