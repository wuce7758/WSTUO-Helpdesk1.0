<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ include file="../../language.jsp" %>
<script src="../js/wstuo/jbpmMge/processDefined.js?random=<%=new java.util.Date().getTime()%>"></script>

	<!-- 数据表格 -->

<div class="tab-pane active" >
	<div class="box-inner">
		<div class="box-header well" data-original-title="">
	        <h2><i class="glyphicon glyphicon-list"></i>&nbsp;<fmt:message key="title.request.requestGrid" /></h2>
        </div>
		<div class="box-content ">
			<table id="processDefinedGrid" ></table>
			<div id="processDefinedPager"></div>		
			<div id="processToolbar"  style="display: none">
				<div class="panelBar">	
					<sec:authorize url="/pages/jbpm!deployProcess.action">
						<a class="btn btn-default btn-xs" href="../jbpm/index.jsp" target="_blank"><i class="glyphicon glyphicon-plus"></i>&nbsp;<fmt:message key="common.add" /></a>				
					</sec:authorize>
					
					<a class="btn btn-default btn-xs" onClick="wstuo.jbpmMge.processDefined.search_openwindow()"><i class="glyphicon glyphicon-search"></i>&nbsp;<fmt:message key="common.search" /></a>
					<font color="red"><fmt:message key="label.processManageSearch" /></font>
				
				</div>
			</div>
		</div>
	</div>
</div>

<!-- add process -->
<div id="addProcess" icon="icon-add" class="WSTUO-dialog" title="<fmt:message key="setting.deploymentProcess" />" style="width:400px;padding:3px;line-height:20px; height:100px;">
	<form action="jbpm!deployProcessDefinitionFile.action" method="post" enctype="multipart/form-data" id="addProcessForm">
			<table style="width:100%;margin-top:30px;" cellspacing="1">
				<tr>
				<td><input type="file" id="processFile" name="processFile" onchange="checkFileMultiType(this,'zip,jpdl.xml','addProcessForm')"/></td>
				<td><a id="link_jbpm_deploy_ok" class="btn btn-primary btn-sm" icon="icon-ok"><fmt:message key="setting.deploymentProcess" /></a></td>
			</tr>
	
			</table>
	</form>
</div>

<!-- 搜索 -->
<div id="processDefinedSearchWin" class="WSTUO-dialog" title="<fmt:message key="common.search"/>" style="width:400px;height:auto">
	<form id="processDefinedSearchForm">	
   			<table class="table" cellspacing="1"> 
			<tr>
				<td><h5 style="color: black;"><fmt:message key="label.name"/></h5></td>
				<td><input name="processDefinitionDTO.name" class="form-control" /></td>
			</tr>
			<tr>
				<td><h5 style="color: black;"><fmt:message key="label.key"/></h5></td>
				<td><input name="processDefinitionDTO.key" class="form-control" /></td>
			</tr>
			<tr>
				<td colspan="2">
					<br>
					<a class="btn btn-primary btn-sm" plain="false" icon="icon-search" id="processDefinedSearch_opt_but"><fmt:message key="common.search"/></a>
				</td>
			</tr>
		</table>
	</form>
</div>


<!-- 查看流程图 -->
<div id="processTracelnstanceWin" class="WSTUO-dialog" title="查看流程图" style="width:80%;height:auto">

		<c:if test="${
			activityCoordinatesDTO.x eq 0 
			and activityCoordinatesDTO.y eq 0 
			and activityCoordinatesDTO.width eq 0 
			and activityCoordinatesDTO.height eq 0 }">
			<div class="traceInstance" style="width: 99%;">
				<fmt:message key="title.jbppm.processEnd" />
			</div>
		</c:if>
		<c:if test="${! empty activityCoordinatesDTO or ! empty activityHistoryCoordinatesDTO }">
			<center>
				<div style="margin-top:5px">
				<span style="border:1px dashed #FF6600;border-radius: 4px;text-align: center;color: red;padding: 0px 10px 0px 10px;" >
				<fmt:message key="label.flow.chart.hisActivityCoordinatesTip" />
				</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<span style="border:1px solid #FF6600;border-radius: 4px;text-align: center;color: red;padding: 0px 10px 0px 10px;">
				<fmt:message key="label.flow.chart.activityCoordinatesTip" />
				</span></div>
			</center>
		</c:if>
		<div style="boverflow: auto; height:90%;position:relative;top:3px;bottom: 3px" >
			<!-- 当前坐标 -->
			<c:if test="${! empty activityCoordinatesDTO }">
				<div style="position:absolute;border:1px solid #FF6600;z-index:999px;border-radius: 4px;
					left:${activityCoordinatesDTO.x}px;top:${activityCoordinatesDTO.y}px;
					width:${activityCoordinatesDTO.width}px;height:${activityCoordinatesDTO.height}px;">
				</div>
			</c:if>
			
			<!-- 历史坐标 -->
			<c:if test="${! empty activityHistoryCoordinatesDTO }">
			<c:forEach items="${activityHistoryCoordinatesDTO }" var="hisacDTO"> 
			<div style="position:absolute;border:1px dashed #FF6600;z-index:999px;border-radius: 4px;
				left:${hisacDTO.x }px;top:${hisacDTO.y }px;
				width:${hisacDTO.width }px;
				height:${hisacDTO.height}px;"></div>
			</c:forEach>
			</c:if>
			<!-- 历史坐标 -->
			
			<img id="imgTracelnstance" src=""  />
	
		</div>

	<br/>
</div>











	
