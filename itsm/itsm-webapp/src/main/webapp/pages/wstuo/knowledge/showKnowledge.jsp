<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="../../language.jsp"%>

<script type="text/javascript">
if("${knowledgeDto.kid}" ==null || "${knowledgeDto.kid}" ==""){
	endProcess();
	msgAlert(i18n.requestIsDelete,'info');
	basics.tab.tabUtils.closeTab(i18n['knowledge']+i18n['details']);
}
</script>

<script type="text/javascript">
rat('star2','result2',1);
function rat(star,result,m){
	star= '#' + star;
	result= '#' + result;
	$(result).hide();//将结果DIV隐藏

	$(star).raty({
		hints: ['一星','二星', '三星', '四星', '五星'],
		path: "${pageContext.request.contextPath}/images/img",
		starOff: 'star-off.png',
		starOn: 'star-on.png',
		size: 24,
		start: 40,
		showHalf: true,
		target: result,
		targetKeep : true,//targetKeep 属性设置为true，用户的选择值才会被保持在目标DIV中，否则只是鼠标悬停时有值，而鼠标离开后这个值就会消失
		click: function (score, evt) {
			$("#value").val(score*m);
			//第一种方式：直接取值
			//alert('你的评分是'+score*m+'分');
		}
	});
}
	function appKw_openwindow(){
		$("#showAppInfo").show();
	    $('#showKnowledge_app_window form')[0].reset();
	    windows('showKnowledge_app_window',{width: 350});
	}
	function appKw_save(){
		var knowledgeIds=$('#showKnowledge_kid').val();
		
		var _param = $.param({'knowledgeDto.kids': knowledgeIds}, true);

        var _form = $('#showKnowledge_app_window form').serialize();
        if(knowledgeIds=='' || knowledgeIds==null){
        	return false;
        }
        startProcess();
        $.post("knowledgeInfo!passKW.action", _param+'&'+_form, function () {

        	//$('#showKnowledge_app_window').dialog('close');
        	 msgShow(i18n['common_operation_success'], 'show');
        	 if(type!=null && type!=""){
        		 wstuo.knowledge.knowledgeGrid.refresh();
        	 }
        	 var url="knowledgeInfo!showKnowledge.action?kid="+knowledgeIds;
        	 basics.index.initContent(url);
			endProcess();
        });
	}
</script>

<div id="showKnowledge" >
<div class="row">
	<div class="box col-md-12">
		<div class="box-inner">
			<div class="box-header well" data-original-title="">
				<h2>
					<i class="glyphicon glyphicon-list-alt"></i>&nbsp;知识详情
				</h2>
				<div class="box-icon">
				</div>
			</div>
            <input type="hidden" value="${knowledgeDto.kid}" id="knowledge_kid" >
			<div class="box-content ">
			  <div class="page-header">
                    <h1>
                    ${knowledgeDto.title}
                    </h1>
                    <div style="color: #AAA; font-size: 12px; padding: 8px">
								<c:if test="${not empty knowledgeDto.serviceDirectoryItemName}">
									<fmt:message key="label.knowledge.relatedService" />：${knowledgeDto.serviceDirectoryItemName}&nbsp;&nbsp;
					            </c:if>
					
								<fmt:message key="tool.affiche.creator" />
								：${knowledgeDto.creatorFullName}&nbsp;&nbsp;
								<fmt:message key="knowledge.label.loginTime" />
								：
								<fmt:formatDate type="date" value="${knowledgeDto.addTime}" pattern="yyyy-MM-dd HH:mm:ss" />
								<fmt:message key="knowledge.click" />
								：${knowledgeDto.clickRate} 引用数目 : ${knowledgeDto.count}
								<c:if test="${not empty knowledgeDto.keyWords}">
									<fmt:message key="label.keywords" /> ：${knowledgeDto.keyWords}
					            </c:if>
							</div>
                </div>
				<div class="row ">
					<div class="col-md-12">
                        <p>${knowledgeDto.content}</p>
                        <p>
                            <c:if test="${not empty knowledgeDto.attachments}">
								<div style="margin: 10px; border: #FFD2D2 1px dashed; padding: 5px; line-height: 22px">
									<div style="font-size: 14px; font-weight: bold; font-style: italic; color: #FF2D2D">
										<fmt:message key="knowledge.relatedAttachmentOfKnowledgeAs" />
										：
									</div>
									<c:forEach items="${knowledgeDto.attachments}" varStatus="i"
										var="attachment">${i.count}       
										<a href="attachment!download.action?downloadAttachmentId=${attachment.aid}" target="_blank">${attachment.attachmentName}</a>
										<br />
										<c:if test="${not empty attachment.attachmentContentShort}">
											<div style="margin: 8px; padding: 5px; background: #EEE">
												<fmt:message key="label.attachment.preview" />
												：${attachment.attachmentContentShort}
											</div>
										</c:if>
						
						
									</c:forEach>
								</div>
							</c:if>
                        </p>
                        <p>
	                          <c:if test="${knowledgeDto.knowledgeStatus eq '0'}">
								<sec:authorize url="/pages/knowledgeInfo!passKW.action">
									<div>
										<button type="button" class="btn btn-primary btn-sm" onclick="appKw_openwindow()" ><fmt:message key="label_knowledge_approval" /></button> 
										<input type="hidden" id="showKnowledge_kid" value="${knowledgeDto.kid}" />
									</div>
								</sec:authorize>
							 </c:if>
                        </p>
                    </div>
					<c:if test="${knowledgeDto.knowledgeStatus eq '1'}">
					<div class="col-md-12">
					    <div id="knowledgestars">
							<form>
								<input type="hidden" value="${sessionScope.fullName}" name="knowledgeDto.creator" id="creator"> 
								<input type="hidden" value="${knowledgeDto.kid}" id="kid" name="knowledgeDto.kid">
								<table style="width: 100%;">
									<tr>
										<td width="80px" height="40px"><fmt:message key="star" /> ：</td>
										<td>
											<div style="margin-left: 2px;" id="star2"></div>
											<div style="margin-left: 18px;" id="result2"></div>
											<input type="hidden" name="knowledgeDto.stars" id="value">
										</td>
									</tr>
									<tr>
										<td><fmt:message key="content" /> ：</td>
										<td>
											<textarea name="knowledgeDto.remark" style="width: 100%;" id="reply_content" class="form-control" name="Evaluationdto.content"></textarea>
									    </td>
									</tr>
									<tr>
									  <td height="40px"></td>
									  <td>
									     <button type="button" class="btn btn-primary btn-sm" id="saveEvaluation" ><fmt:message key="common.save" /></button>
									  </td>									
									</tr>
								</table>
							</form>
						</div>
					</div>
					<div class="col-md-12">
						<div class="well">
							<table id="tableContent" style=" font-size: 12px; padding: 3px;width:98%">
								
							</table>
						</div>
					</div>
			</c:if>
				</div>
			</div>
		</div>
	</div>
</div>	
</div>


<!-- 审批知识 start -->
<div id="showKnowledge_app_window" class="WSTUO-dialog"
	title='<fmt:message key="label_knowledge_appKw" />'
	style="width: 350px; height: auto">
	<form>
		<div class="lineTableBgDiv">
			<table style="width: 100%" class="lineTable" cellspacing="1">
				<tr id="showAppInfo">
					<td><fmt:message key="label_knowledge_appOpinion" /></td>
					<td><input type="radio" name="knowledgeDto.knowledgeStatus"
						value="1" checked="checked" /> <fmt:message
							key="label_knowledge_approvalPass" /> <input type="radio" name="knowledgeDto.knowledgeStatus" value="-1" /> 
							<fmt:message key="label_knowledge_approvalRefuse" /></td>
				</tr>
				<tr>
					<td><fmt:message key="common.remark" /></td>
					<td><textarea class="form-control" style="height: 100px"
							name="knowledgeDto.statusDesc"></textarea></td>
				</tr>

				<tr>
					<td colspan="2"><input type="hidden" value="app"
						name="knowledgeDto.knowledgeAppDescRole" /> <%-- <a id="knowledge_ap_submit" class="easyui-linkbutton" icon="icon-save"><fmt:message key="common.submit" /></a> --%>
						<button onclick="appKw_save()" type="button" class="btn btn-primary btn-sm"><fmt:message key="common.submit" /></button>
						</td>
				</tr>
			</table>
		</div>
	</form>
</div>
<!-- 审批知识 end -->
<style>

.commont:hover{
	background: url('../images/comm/KWBG1.jpg') repeat;
	background-size:100%;
}

</style>
<script>

$("#showKnowledge").parent().parent().height("auto");
$("#showKnowledge").parent().height("auto");
initCommont();
function initCommont(){
	$.get("knowledgeInfo!findByIdCommentKnowledge.action?kid="+$("#knowledge_kid").val(),function(data){
	    //data 就是你的数据
		$.each(data,function(key,value){
			var stars=value.stars;
			if(stars=='undefined'){
				stars="无"
			}
			/* "+value.creator+" */
			$("#tableContent").append(
					"<tr class='commont'><td style='border-bottom:1px solid #ccc;width:95%'>"+
				    "<p style='color:#AAA;margin-left: 13px;margin-top: 3px;'><font style='color:#000000;'>"+value.creator+"："+value.remark+
				    "</font><p style='color:#AAA;margin-left: 13px;'>"+value.createTime+"&nbsp;&nbsp;"+jsimg(stars)+"</p></td>"+
					"</tr>");
		});
	});
}

function jsimg(count){
	var imgsrc="";
	if(count){
		for(var i=0;i<count;i++){
			imgsrc+="<img src='${pageContext.request.contextPath}/images/img/star-on.png' style='vertical-align:middle;width: 15px;height: 15px;'/>"
		}
	}
	return imgsrc;
}

$("#saveEvaluation").click(function(){
	//var val_payPlatform = $('#radio input[name="knowledgeDto.stars"]:checked ').val();
	var content=$("#reply_content").val();
	if(!content){
		msgShow(i18n.validate_notNull,'show');
		return;
	}
	var stars=$("#value").val();
	if(stars==""){
		msgShow(i18n.msg_stars_notnull,'show');
		return;
	}
	var _param = $('#knowledgestars form').serialize();
	var _url='knowledgeInfo!saveCommentKnowledge.action';
	//进程状态
	startProcess();
	$.post(_url,_param,function(){
		msgShow(i18n['saveSuccess'],'info');
		$("#tableContent").prepend(
				"<tr class='commont'><td style='border-bottom:1px solid #ccc;width:95%'>"+
				//"<p style='color:#AAA;margin-left: 10px;'>&nbsp;<font style='margin-left: 3px;'>"+jsimg(stars)+"</font></p>"+
			    "<p style='color:#AAA;margin-left: 13px;'><font style='color:#000000;'>${sessionScope.fullName}："+content+
			    "</font><p style='color:#AAA;margin-left: 13px;'>"+i18n.msg_just+"&nbsp;&nbsp;"+jsimg(stars)+"</p></td>"+
				"</tr>");
		//进程状态
        endProcess();
        rat('star2','result2',1);
        $("#reply_content").val('');
        $("#value").val('');
	}); 
});
</script>
<style>
.page-header {
    padding-bottom: 0px;
    margin: 5px 0 20px;
    border-bottom: 1px solid #eee;
}
</style>