<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
    <%@ include file="../../language.jsp" %>
<!-- <style type="text/css">
	ul.rightTools {float:right; display:block;}
	ul.rightTools li{float:left; display:block; margin-left:5px}
</style> -->
<script type="text/javascript">
//<!--

var viewOrglist_edit="0";
var viewOrglist_delete="0";
var hiddenOrglist_act=false;
var holiday='0';
var serviceTime='0';
var companyNo = "${companyNo}";
<sec:authorize url="/pages/organization!updateOrg.action">
viewOrglist_edit="1";
</sec:authorize>

<sec:authorize url="/pages/organization!remove.action">
viewOrglist_delete="1";
</sec:authorize>

<sec:authorize url="/pages/serviceTime!saveOrgUpdate.action">
serviceTime="1";
</sec:authorize>
<sec:authorize url="/pages/holiday!findHolidayPage.action">
holiday="1";
</sec:authorize>
if(viewOrglist_edit=="0"&&viewOrglist_delete=="0"){
	hiddenOrglist_act=true;
}
//-->
</script>
<script src="../js/wstuo/orgMge/organizationMain.js?random=<%=new java.util.Date().getTime()%>"></script>
<div class="row">
    <div class="box col-md-2">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-list-alt"></i> 机构树</h2>
            </div>
            <div class="box-content buttons" id="orgTreeDIV">
                
            </div>
        </div>
    </div>
    <!--/span-->

    <div class="box col-md-10">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-list"></i> 机构管理</h2>
            </div>
            <div class="box-content ">
           		 <ul class="nav nav-tabs" id="orgMgeTab">
                   <sec:authorize url="/pages/organization!find.action"> <li class="active"><a href="#list">机构列表</a></li></sec:authorize>
					<sec:authorize url="/pages/serviceTime!saveOrgUpdate.action"><li><a href="#serviceTimeDiv">工作时间</a></li></sec:authorize>
                   <sec:authorize url="/pages/holiday!findHolidayPage.action"> <li><a href="#holiday">节假日</a></li></sec:authorize>
                </ul>

               <div id="myTabContent" class="tab-content">
               
<!-- OrgGrid start -->
<sec:authorize url="/pages/organization!find.action">
                   <div class="tab-pane active" id="list">
                	<div id="orgMgeTable_div" class="">
						<!-- OrgGrid start -->
							<div title="<fmt:message key="title.orgSettings.gridView" />"  style="padding: 3px;">
							<!-- 列表start -->
							<table id="orgGrid"></table> 
							<div id="orgGridPager"></div>
							<div style="display:none" id="orgGridAction"><sec:authorize url="/pages/organization!updateOrg.action"><a href="javascript:wstuo.orgMge.organizationGrid.editOrganizationOpenWindow_aff('{orgNo}')" title="<fmt:message key="title.orgSettings.editOrg" />"><i class="glyphicon glyphicon-edit"></i></a></sec:authorize>&nbsp;&nbsp;&nbsp;&nbsp; <sec:authorize url="/pages/organization!remove.action"><a href="javascript:wstuo.orgMge.organizationGrid.deleteOrgs_aff('{orgNo}')" title="<fmt:message key="common.delete" />"><i class="glyphicon glyphicon-trash"></i></a></sec:authorize></div>
								<div id="orgGridToolbar" style="display: none">
									<div class="panelBar">
										 <sec:authorize url="/pages/organization!save.action">
										 <button class="btn btn-default btn-xs" id="organizationGrid_add"><i class="glyphicon glyphicon-plus"></i> <fmt:message key="common.add" /></button>
										</sec:authorize>
										 <sec:authorize url="/pages/organization!updateOrg.action">
										<button class="btn btn-default btn-xs" onclick="wstuo.orgMge.organizationGrid.editOrganizationOpenWindow();"><i class="glyphicon glyphicon-edit"></i> <fmt:message key="common.edit" /></button>
										</sec:authorize>
										<sec:authorize url="/pages/organization!remove.action">
										<button class="btn btn-default btn-xs" onclick="wstuo.orgMge.organizationGrid.deleteOrgs();"><i class="glyphicon glyphicon-trash"></i> <fmt:message key="common.delete" /></button>
										</sec:authorize>
										<button class="btn btn-default btn-xs"  id="organizationGrid_search" >
										<i class="glyphicon glyphicon-search"></i><fmt:message key="common.search" />
										</button>
										<sec:authorize url="organizationGrid_import">
										<button class="btn btn-default btn-xs" id="organizationImport"><i class="glyphicon glyphicon-import"></i> <fmt:message key="label.dc.import" /></button>
										</sec:authorize>
										<sec:authorize url="organizationGrid_export">
										<button class="btn btn-default btn-xs" id="organizationExport"><i class="glyphicon glyphicon-share"></i> 导出</button>
										</sec:authorize>
									</div>
								</div>
							</div>
					</div>
					</div>
					</sec:authorize>
		<sec:authorize url="/pages/serviceTime!saveOrgUpdate.action">
                    <div class="tab-pane" id="serviceTimeDiv">
                    <form style="padding:0px;margin:0px">
                        <div class="row">
						    <div class="box col-md-6">
						        <div class="box-inner">
						            <div class="box-content buttons" style="min-height:160px">
						            <h3>工作时间</h3>
						               <label for="fullday"> <input type="radio" name="alldaylabel" value="true" id="fullday" checked/><fmt:message key="label.orgSettings.allDay" /></label>
						                <label for="diyday"><input type="radio"  name="alldaylabel" value="false" id="diyday" /><fmt:message key="label.orgSettings.selectWorkTime" /></label>
									<br/>
									<div id="setTime" style="display: none">
										<fmt:message key="lable.service.UpstartWorkTime" />&nbsp;&nbsp;
						            	<select name="serviceTimeDTO.startHour" id="serviceTimeDTO_startHour">
						                    	<option>0</option>
						                        <option>1</option>
						                        <option>2</option>
						                        <option>3</option>
						                        <option>4</option>
						                        <option>5</option>
						                        <option>6</option>
						                        <option>7</option>
						                        <option>8</option>
						                        <option selected="selected">9</option>
						                        <option>10</option>
						                        <option>11</option>
						                        <option>12</option>
						                        <option>13</option>
						                        <option>14</option>
						                        <option>15</option>
						                        <option>16</option>
						                        <option>17</option>
						                        <option>18</option>
						                        <option>19</option>
						                        <option>20</option>
						                        <option>21</option>
						                        <option>22</option>
						                        <option>23</option>
						                    </select>&nbsp;&nbsp;<fmt:message key="label.orgSettings.hour" />&nbsp;&nbsp;
						                    <select name="serviceTimeDTO.startMinute" id="serviceTimeDTO_startMinute">
						                    	<option>0</option>
						                        <option>5</option>
						                        <option>10</option>
						                        <option>15</option>
						                        <option>20</option>
												<option>25</option>
						                        <option>30</option>
						                        <option>35</option>
						                        <option>40</option>
						                        <option>45</option>
												<option>50</option>
						                        <option>55</option>
						                 </select>&nbsp;<fmt:message key="setting.label.to" />
						                 <select name="serviceTimeDTO.startNoonHour" id="serviceTimeDTO_startNoonHour">
						                    	<option>0</option>
						                        <option>1</option>
						                        <option>2</option>
						                        <option>3</option>
						                        <option>4</option>
						                        <option>5</option>
						                        <option>6</option>
						                        <option>7</option>
						                        <option>8</option>
						                        <option>9</option>
						                        <option>10</option>
						                        <option>11</option>
						                        <option selected="selected">12</option>
						                        <option>13</option>
						                        <option>14</option>
						                        <option>15</option>
						                        <option>16</option>
						                        <option>17</option>
						                        <option>18</option>
						                        <option>19</option>
						                        <option>20</option>
						                        <option>21</option>
						                        <option>22</option>
						                        <option>23</option>
						                    </select>&nbsp;&nbsp;<fmt:message key="label.orgSettings.hour" />&nbsp;&nbsp;
						                    <select name="serviceTimeDTO.startNoonMinute" id="serviceTimeDTO_startNoonMinute">
						                    	<option>0</option>
						                        <option>5</option>
						                        <option>10</option>
						                        <option>15</option>
						                        <option>20</option>
												<option>25</option>
						                        <option>30</option>
						                        <option>35</option>
						                        <option>40</option>
						                        <option>45</option>
												<option>50</option>
						                        <option>55</option>
						                 </select>&nbsp;<fmt:message key="label.orgSettings.minute" />
						            	<br>
						            	<fmt:message key="lable.service.DownendWorkTime" />&nbsp;&nbsp;
						            	<select name="serviceTimeDTO.endNoonHour" id="serviceTimeDTO_endNoonHour">
						                    	<option>0</option>
						                        <option>1</option>
						                        <option>2</option>
						                        <option>3</option>
						                        <option>4</option>
						                        <option>5</option>
						                        <option>6</option>
						                        <option>7</option>
						                        <option>8</option>
						                        <option>9</option>
						                        <option>10</option>
						                        <option>11</option>
						                        <option>12</option>
						                        <option selected="selected">13</option>
						                        <option>14</option>
						                        <option>15</option>
						                        <option>16</option>
						                        <option>17</option>
						                        <option>18</option>
						                        <option>19</option>
						                        <option>20</option>
						                        <option>21</option>
						                        <option>22</option>
						                        <option>23</option>
						                    </select>&nbsp;&nbsp;<fmt:message key="label.orgSettings.hour" />&nbsp;&nbsp;
						                    <select name="serviceTimeDTO.endNoonMinute" id="serviceTimeDTO_endNoonMinute">
						                    	<option>0</option>
						                        <option>5</option>
						                        <option>10</option>
						                        <option>15</option>
						                        <option>20</option>
												<option>25</option>
						                        <option selected="selected">30</option>
						                        <option>35</option>
						                        <option>40</option>
						                        <option>45</option>
												<option>50</option>
						                        <option>55</option>
						                 </select>&nbsp;<fmt:message key="setting.label.to" />
						            	<select name="serviceTimeDTO.endHour" id="serviceTimeDTO_endHour">
						                    	<option>0</option>
						                        <option>1</option>
						                        <option>2</option>
						                        <option>3</option>
						                        <option>4</option>
						                        <option>5</option>
						                        <option>6</option>
						                        <option>7</option>
						                        <option>8</option>
						                        <option>9</option>
						                        <option>10</option>
						                        <option>11</option>
						                        <option>12</option>
						                        <option>13</option>
						                        <option>14</option>
						                        <option>15</option>
						                        <option>16</option>
						                        <option selected="selected">17</option>
						                        <option>18</option>
						                        <option>19</option>
						                        <option>20</option>
						                        <option>21</option>
						                        <option>22</option>
						                        <option>23</option>
						                    </select>&nbsp;&nbsp;<fmt:message key="label.orgSettings.hour" />&nbsp;&nbsp;
						                    <select name="serviceTimeDTO.endMinute" id="serviceTimeDTO_endMinute">
						                    	<option selected="selected">0</option>
						                        <option>05</option>
						                        <option>10</option>
						                        <option>15</option>
						                        <option>20</option>
												<option>25</option>
						                        <option>30</option>
						                        <option>35</option>
						                        <option>40</option>
						                        <option>45</option>
												<option>50</option>
						                        <option>55</option>
						                    </select>&nbsp;<fmt:message key="label.orgSettings.minute" />
						                </div>
						              </div>
						            </div>
						        </div>
						    <div class="box col-md-6">
						        <div class="box-inner">
						            <div class="box-content buttons" style="min-height:160px">
						            <h3>工作日</h3>
						                <label><input id="workDay_reverseSelect" type="radio" name="workDayQuickSelect"><fmt:message key="common.selectAll" /></label>
										<label><input id="workDay_normalSelect" type="radio" name="workDayQuickSelect"><fmt:message key="common.usefullTime" /></label>
										<br/>
										<span id="workDay_workTime">
						                <label><input type="checkbox" name="serviceTimeDTO.monday" id="serviceTimeDTO_monday" value="false" checked="checked"/><fmt:message key="label.orgSettings.monday" /></label>
							            <input type="checkbox" name="serviceTimeDTO.tuesday" id="serviceTimeDTO_tuesday" value="false" checked="checked"/><fmt:message key="label.orgSettings.tuesday" />
							            <input type="checkbox" name="serviceTimeDTO.wednesday" id="serviceTimeDTO_wednesday" value="false" checked="checked"/><fmt:message key="label.orgSettings.wednesday" />
							            <input type="checkbox" name="serviceTimeDTO.thursday" id="serviceTimeDTO_thursday" value="false" checked="checked"/><fmt:message key="label.orgSettings.thursday" />
							            <input type="checkbox" name="serviceTimeDTO.friday" id="serviceTimeDTO_friday" value="false" checked="checked"/><fmt:message key="label.orgSettings.friday" />
							        	</span>
							        	<span id="workDay_weekEnd"><input type="checkbox" name="serviceTimeDTO.saturday" id="serviceTimeDTO_saturday" value="false" /><fmt:message key="label.orgSettings.saturday" />
							            <input type="checkbox" name="serviceTimeDTO.sunday" id="serviceTimeDTO_sunday" value="false" /><fmt:message key="label.orgSettings.sunday" /></span>
							        </div>
						        </div>
						    </div>
		    	 		</div>
		        	
		        <input type="hidden" name="serviceTimeDTO.allday" id="serviceTimeDTO_allday"/>
		        <input type="hidden" name="serviceTimeDTO.sid" id="serviceTimeDTO_sid"/>
		        <input type="hidden" name="serviceTimeDTO.orgNo" id="serviceTime_orgNo"/>
		        
		        <div id="saveOrUpdateServiceTimeBtnDiv">
		         <button id="serviceTime_save"  type="button" class="btn btn-primary btn-sm"><fmt:message key="common.save" /></button>
		        </div>
		        
		</form>
		</div>
</sec:authorize> 
	 <sec:authorize url="/pages/holiday!findHolidayPage.action">
       <div class="tab-pane" id="holiday">
		<div style="padding: 3px;">
			<!-- 新增节假日  -->
			<table id="holidayGrid"></table>
			<div id="holidayGridPager"></div>
			<div id="holidayGridToolbar" style="display: none">
			
				<sec:authorize url="/pages/holiday!addHoliday.action">
				<button class="btn btn-default btn-xs" id="holidayGrid_add"><i class="glyphicon glyphicon-plus"></i> <fmt:message key="common.add" /></button>
				</sec:authorize>
				 <sec:authorize url="/pages/holiday!updateHoliday.action">
				<button class="btn btn-default btn-xs" id="holidayGrid_edit"><i class="glyphicon glyphicon-edit"></i> <fmt:message key="common.edit" /></button>
				</sec:authorize>
				<button class="btn btn-default btn-xs" id="holidayGrid_delete"><i class="glyphicon glyphicon-trash"></i> <fmt:message key="common.delete" /></button>
			</div>
		</div>
</div>

</sec:authorize>  
                  </div>
            </div>
        </div>
    </div>
	
</div><!--/row-->
<!-- 机构添加/修改start -->
<div id="organizationWindow" class="WSTUO-dialog" title="<fmt:message key="title.orgSettings.addOrg" />" style="width: 500px; height:auto">
<div class="panel panel-default">
	<div class="panel-body">
		<form id="organizationWindowForm" class="form-horizontal" event="wstuo.orgMge.organizationGrid.saveOrganization">
			<div class="form-group">
				<label class="col-sm-4 control-label" for="orgWindow_parentOrgName"><fmt:message key="label.orgSettings.ownerOrg" /></label>
			<div class="col-sm-5">
				<input type="text" class="choose form-control" id="orgWindow_parentOrgName" required="true" >
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-4 control-label" for="orgWindow_orgType"><fmt:message key="label.orgSettings.orgType" /></label>
			<div class="col-sm-5">
				<select id="orgWindow_orgType" name="actionType" class="form-control">
					<option value="inner"><fmt:message key="label.orgSettings.orgInner" /></option>
					<option value="services"><fmt:message key="label.orgSettings.orgService" /></option>
				 </select>
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-4 control-label" for="username"><fmt:message key="label.orgSettings.orgName" /></label>
			<div class="col-sm-5">
				<input type="text" class="form-control" id="orgWindow_orgName" name="organizationDto.orgName" required="true" >
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-4 control-label" for="email"><fmt:message key="label.orgSettings.orgPhone" /></label>
			<div class="col-sm-5">
				<input type="text" class="form-control" id="orgWindow_officePhone" name="organizationDto.officePhone"  validType="phone">
			</div>
		</div>
		
		<div class="form-group">
			<label class="col-sm-4 control-label" for="email"><fmt:message key="label.orgSettings.orgFax" /></label>
			<div class="col-sm-5">
				<input type="text" class="form-control" id="orgWindow_officeFax" name="organizationDto.officeFax"  validType="phone">
			</div>
		</div>
		
		<div class="form-group">
			<label class="col-sm-4 control-label" for="email"><fmt:message key="label.orgSettings.orgEmail" /></label>
			<div class="col-sm-5">
				 <input type="email" class="form-control" id="orgWindow_email" name="organizationDto.email"  validType="email">
			</div>
		</div>
		
		<div class="form-group">
			<label class="col-sm-4 control-label" for="email"><fmt:message key="label.orgSettings.orgAddress" /></label>
			<div class="col-sm-5">
				 <input type="text" class="form-control" id="orgWindow_address" name="organizationDto.address">
			</div>
		</div>
		
		<div class="form-group">
			<label class="col-sm-4 control-label" for="email"><fmt:message key="label.orgSettings.orgInchargeName" /></label>
			<div class="col-sm-5">
				    <input type="text" class="form-control"  id="organizationDto_personInChargeName_gridAdd" name="organizationDto.personInChargeName">
			        <a href="javascript:wstuo.orgMge.organizationMain.selectPersonIncharge('#organizationDto_personInChargeNo_gridAdd','#organizationDto_personInChargeName_gridAdd');" ><span class="glyphicon glyphicon-user form-control-feedback choose" ></span></a>
			        <input id="organizationDto_personInChargeNo_gridAdd" name="organizationDto.personInChargeNo" type="hidden" /> 
			</div>
		</div>

        <input type="hidden" id="orgWindow_parentOrgNo" name="organizationDto.parentOrgNo" /> 
		<input type="hidden" name="organizationDto.orgNo" id="orgWindow_orgNo">
		
		<div class="form-group">
			<div class="col-sm-9 col-sm-offset-4">
				<button  type="submit" class="btn btn-primary btn-sm"><fmt:message key="common.save"/></button>
				<button type="reset" class="btn btn-default btn-sm"><fmt:message key="common.reset"/></button>
				</div>
			</div>
		</form>
	</div>
</div>  
</div>
<!-- 机构添加/修改end -->
<!-- 选择树 start-->
<div id="selectTree" class="WSTUO-dialog" title="<fmt:message key="title.user.selectOrg" />" style="width: 250px; height: 350px; padding: 10px; line-height: 20px;">
     <div id="selectTreeDIV"></div>
</div>
<!-- 选择树 end-->
 <!-- 导出隐藏信息 -->
    <div style="display: none;">
	   <form action="organization!exportOrganization.action" id="exportOrganizationForm" method="post">
	     <table>
	     <tr>
			<td>						
				<input id="exportOrganization_orgName" name="organizationQueryDto.orgName"/>
				<input id="exportOrganization_officePhone" name="organizationQueryDto.officePhone"/>
				<input id="exportOrganization_address" name="organizationQueryDto.address"/>
				<input id="exportOrganization_sidx" name="sidx"/>
				<input id="exportOrganization_sord" name="sord"/>
				<input id="exportOrganization_page" name="page"/>
				<input id="exportOrganization_rows" name="rows"/>
			</td>
		</tr>
	     </table>
	     
	     <input name="companyNo" id="exportOrganization_companyNo">
	   </form>
    </div>

<div id="addHolidayWindow" class="WSTUO-dialog" title="<fmt:message key="title.orgSettings.addHoliday" />" style="width:350px; height:auto">

<form id="holidayForm" event="wstuo.orgMge.organizationHoliday.saveHoliday">
	<div class="lineTableBgDiv">
			<table style="width:100%" class="lineTable" cellspacing="1">
			<tr id="hoiday_tr">
			<td style="width:20%"><fmt:message key="label.orgSettings.holidayDate" /></td>
			<td style="width:80%">
				<input name="holidayDTO.companyNo" value="${companyNo }" type="hidden" />
				<div class="form-inline">
					<input id="hoilday_hdate" name="holidayDTO.hdate"  class="form-control"   required="true" style="width:120px;cursor:pointer;" readonly onfocus="this.blur()"/>
					<%-- <fmt:message key="label.orgSettings.holidayTo" /> --%>-
					<input id="hoilday_hedate" name="holidayDTO.hedate"   class="form-control"  required="true" validType="DateComparisonHoliday['hoilday_hdate']" style="width:120px;cursor:pointer" readonly onfocus="this.blur()"/>
				</div>			
			</td>
			</tr>
			
			<tr >
			<td><fmt:message key="label.orgSettings.holidayDescription" /></td>
			<td style="padding: 3px">
				<textarea id="hoilday_hdesc"  name="holidayDTO.hdesc"  class="form-control"  required="true" style="height:60px"></textarea>
			</td>
			</tr>
			
			<tr>
			<td></td>
			<td style="padding-top:10px">
			
			<input type="hidden" id="hoilday_hid" name="holidayDTO.hid" />
			<input type="hidden" id="hoilday_orgNo" name="holidayDTO.orgNo" />
			<input type="hidden" id="hoilday_orgType" name="holidayDTO.orgType" />
			<button  type="submit" class="btn btn-primary btn-sm"><fmt:message key="common.save" /></button>
			</td>
			</tr>
			
			</table>
	</div>
</form>
</div>

<!-- 机构列表搜索start -->
<div id="searchOrg_win" class="WSTUO-dialog" title='<fmt:message key="common.search" />' style="width: 350px; height:auto">
          <div class="panel panel-default">
					<div class="panel-body">
						<form id="searchOrganizationForm" class="form-horizontal">
							<div class="form-group">
								<label class="col-sm-4 control-label" for="firstname"><fmt:message key="label.orgSettings.orgName" /></label>
								<div class="col-sm-5">
									 <input id="search_organizationQueryDto_orgName" name="organizationQueryDto.orgName" class="form-control"  />
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-4 control-label" for="lastname"><fmt:message key="label.orgSettings.orgPhone" /></label>
								<div class="col-sm-5">
									<input id="search_organizationQueryDto_officePhone" name="organizationQueryDto.officePhone" class="form-control"  />
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-sm-4 control-label" for="lastname"><fmt:message key="label.orgSettings.orgEmail" /></label>
								<div class="col-sm-5">
									<input id="search_organizationQueryDto_email" name="organizationQueryDto.email" class="form-control" />
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-sm-4 control-label" for="lastname"><fmt:message key="label.orgSettings.orgAddress" /></label>
								<div class="col-sm-5">
									<input id="search_organizationQueryDto_address" name="organizationQueryDto.address" class="form-control" />
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-sm-4 control-label" for="lastname"><fmt:message key="label.orgSettings.orgInchargeName" /></label>
								<div class="col-sm-5">
									<input id="search_organizationDto_personInChargeName_gridAdd" name="organizationQueryDto.personInChargeName" class="form-control" />
					                <a href="javascript:wstuo.orgMge.organizationMain.selectPersonIncharge('#search_organizationDto_personInChargeNo_gridAdd','#search_organizationDto_personInChargeName_gridAdd');" ><span class="glyphicon glyphicon-user form-control-feedback choose" ></span></a>
			                        <input id="search_organizationDto_personInChargeNo_gridAdd" name="organizationDto.personInChargeNo" type="hidden" /> 
								</div>
							</div>
							
							<div class="form-group">
								<div class="col-sm-9 col-sm-offset-4">
		                            <button id="searchOrgBtn_OK"  type="button" class="btn btn-primary btn-sm"><fmt:message key="common.save"/></button>
								</div>
							</div>
						</form>
					</div>
			</div>  
</div>
 <!-- 机构列表搜索end -->