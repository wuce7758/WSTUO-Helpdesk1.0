<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ include file="../../language.jsp" %>
<script src="../bower_components/flot/excanvas.min.js"></script>
<script src="../bower_components/flot/jquery.flot.js"></script>
<script src="../bower_components/flot/jquery.flot.pie.js"></script>
<script src="../bower_components/flot/jquery.flot.stack.js"></script>
<script src="../bower_components/flot/jquery.flot.resize.js"></script>
<script src="../js/basics/init-chart.js" type="text/javascript"></script>
<script type="text/javascript">
$(function(){
	$.post("report!requestStatusReport.action",function(data){
		var html="";
		var attr=[];
		$.each(data,function(i,o){
			if(o[0]==null){
				o[0]="未设置状态";
			}
			attr.push({label:o[0], data:o[1]});
			//console.log(attr);
			html=html+"<tr><td>"+o[0]+"</td><td>"+o[1]+"</td></tr>";
		});
		$("#requestStatusinfo tbody").append(html);
		piechart("requestStatuspiechart",attr);
	});
});

</script>
<div class="row">
		<div class="box col-md-12">
			<div class="box-inner">
				<div class="box-header well" data-original-title="">
					<h2>
						<i class="glyphicon glyphicon-list-alt"></i>&nbsp;请求状态统计
					</h2>
					<div class="box-icon">
					</div>
				</div>
				<div class="box-content buttons" >
				 <table id="requestStatusinfo" class="table table-striped table-bordered responsive">
                        <thead>
                        <tr>
                            <th width="50%">请求状态</th>
                            <th>请求个数</th>
                        </tr>
                        </thead>
                        <tbody>
                       
                        </tbody>
                    </table>
                     <div id="hover"></div>
				    <div id="requestStatuspiechart" style="height:500px"></div>
				   
				</div>
			</div>
		</div>
 </div>