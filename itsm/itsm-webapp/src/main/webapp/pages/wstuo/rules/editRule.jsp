﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="../../language.jsp" %>

<script> 
var ruleNo = "${param.ruleNo}";
var _rulePackageNo = '${param.packageNo}';
var _flag = "${param.flag}";
var approveGrid='<fmt:message key="label.sla.ruleList" />';
if(_flag=='requestFit' || _flag=='requestProce'){
	approveGrid='<fmt:message key="label.rulePackage.requestRule" />';
}else if(_flag=='mailToRequest'){
	approveGrid='<fmt:message key="title.rule.mailToRequest" />';
}else if(_flag=='changeApproval' || _flag=='changeProce'){
	approveGrid='<fmt:message key="title.rule.changeRules" />';
}
</script>
<c:if test="${param.flag =='requestFit' or param.flag =='requestProce'}">
	<c:set var="patternType" value="RequestDTO" />
</c:if>

<c:if test="${param.flag =='mailToRequest' }">
	<c:set var="patternType" value="RequestDTO" />
</c:if>

<c:if test="${param.flag =='changeApproval' or param.flag=='changeProce' }">
	<c:set var="patternType" value="ChangeDTO" />
</c:if>

<script src="../js/wstuo/jbpmMge/processUse.js"></script>
<script src="../js/wstuo/rules/editRule.js?random=<%=new java.util.Date().getTime()%>"></script>

<div class="tab-pane active" >
	<div class="box-inner">
		<div class="box-header well" data-original-title="">
	        <h2><i class="glyphicon glyphicon-list"></i>&nbsp;
	       		 
	        <c:if test="${param.flag =='mailToRequest' }">
				<fmt:message key="common.edit" /><fmt:message key="title.rule.mailToRequest" />
			</c:if>
			<c:if test="${param.flag =='requestFit' }">
				<fmt:message key="common.edit" /><fmt:message key="label.rulePackage.requestRule" />
			</c:if>
			
	        </h2>
	        <div class="box-icon"></div>
        </div>
	<div id="editRule_content" class="content" border="none" fit="true" style="height: 98%;">
		<!-- 新的面板  -->
		<div fit="true" id="editRuleDiv_layout">		
		<!-- 左边规则信息 -->			
			<div class="row">
			<div class="box col-md-4" >
			<div class="box-inner" style="padding-left: 5px;">
		        <div class="box-content" id="flowPropertyMain_west" region="west" split="true">	        
            		<div class="box-header well" data-original-title="">
			       		<h2><fmt:message key="common.basicInfo" /></h2>
			      	  	<div class="box-icon"></div>
		        	</div>				
				<input id="operator" name="operator" value="${param.operator }" type="hidden"/>
				<input id="rulepackageNo" name="rulepackageNo" value="${param.packageNo }" type="hidden"/>
				
				<form id="editRuleForm">
				<s:hidden name="rule.condition.rulePatternNo" />
				<s:hidden type="hidden" name="rule.rulePackage.rulePackageNo" />
	        	<s:hidden name="rule.ruleNo" ></s:hidden>
	       	 	<input type="hidden" name="rule.dialect" value="mvel">
				<input type="hidden" name="rule.condition.patternBinding" value="dto">
				<input type="hidden" id="edit_Proce_ruleFlagName" value="${rule.rulePackage.flagName}">
				<input type="hidden" name="rule.condition.patternType" value="${patternType}">
				<div class="lineTableBgDiv">
					<table style="width:100%;" class="table table-bordered" cellspacing="1">
						<tr>
							<td style="width:20%"><fmt:message key="label.rule.ruleName" /></td>
							<td style="width:80%">
							<input name="rule.ruleName" value="${rule.ruleName}" class="form-control" validType="length[1,200]" required="true" />
							</td>
						</tr>
						<tr>
							<td style="width: 20%"><fmt:message key="label.rule.salience" /></td>
							<td style="width: 80%">
								<select name="rule.salience" id="editRuleSalience" style="width:90%;" class="form-control">
								</select>
									<input value="${rule.salience}" type="hidden" id="edit_rule_salience"/>						
							</td>
						</tr>
						<tr>
							<td><fmt:message key="label.rule.ruleDecription" /></td>
							<td>
							<textarea name="rule.description" class="form-control" validType="length[1,200]" required="true" >${rule.description}</textarea>
							</td>
						</tr>
						<c:if test="${param.flag!='mailToRequest'}">
						<tr>
							<td><fmt:message key="lable.com.drools.rule.pages" /></td>
							<td>${rule.rulePackage.rulePackageRemarks}</td>
						</tr></c:if>
					</table>
				</div>
				</form>
			</div>
			</div></div>
			
			<div class="box col-md-8">
        		<div class="box-inner">	
        		<div class="box-header well"  title="<fmt:message key="common.detailInfo" />">
			        <h2><fmt:message key="common.detailInfo" /></h2>
		        </div>			
				<div class="box-content">
					<ul class="nav nav-tabs" id="myTab">
						<li class="active"><a href="#list1"><span><fmt:message key="title.rule.ruleSet" /></span></a></li>	
						<li><a href="#list2"><span><fmt:message key="title.rule.actionSet" /></span></a></li>	
	             	</ul>
             		<div id="myTabContent" class="tab-content">
						<div region="center" class="tab-pane active" id="list1">			
						
						<form>
						<div id="edit_rule_cs_add" style="max-height:120px;height:auto;width:auto">
							<div style="margin-top: 10px;"><b style="margin-top: 8px;"><fmt:message key="title.rule.addOrEdirRuleSet" /></b></div>
							<hr style="margin-top: 3px;margin-bottom: 5px;">	
							<div>
								<table style="width:100%;" cellspacing="1">
									<tr height="30px">
									    <td style="white-space:nowrap;width:40px">&nbsp;&nbsp;&nbsp;<fmt:message key="label.rule.ruleSetDetail" /></td>
									    <td style="text-align:center;">
									    	<table style="width:100%">
												<tr>
													<td style="width:20%">
														<select id="editRule_team" class="form-control" style="width:90%"></select>
													</td>
													<td style="width:20%">
														<select id="editRule_matical" class="form-control" style="width:90%"></select>
													</td>
													<td style="width:30%">
														<div id="editRule_propertyValueDIV" style="float:left">
															<input id="editRule_propertyValue" style="width:80%" size="30" value="" class="form-control"/> 
														</div>
													</td>
													
													<td style="width:20%">
														<a class="btn btn-primary" plain="true" style="margin-right: 15px" id="editRule_addRuleToList"><fmt:message key="label.rule.confirm" /></a>
													</td>
												</tr>
											</table>
									    </td>
								  	</tr>
								  	<tr height="30px">
										<td style="width:25%">&nbsp;&nbsp;&nbsp;<fmt:message key="lable.customFilter.relation" /></td>
										<td style="width:75%">
											<label class="radio-inline">
											<input id="edit_rule_or" type="radio" name="orand" checked/>or
											</label>
	               							<label class="radio-inline">
											<input id="edit_rule_and" type="radio" name="orand" ${rule.condition.constraints[0].andOr eq 'and'?'checked':''}/>and
											</label>
										</td>
									</tr>
								</table>   	
							</div> 	
						</div>
						<hr/>
						
						<div style="max-height:300px;width:auto;overflow:auto;">
								<table id="editRule_constraintsTable" style="width:100%" class="table table-bordered" cellspacing="1">									
									<tr height="30px" ><th align="left" colspan="4"><fmt:message key="title.rule.ruleSet" /></th></tr>
									<tr >
								    	<td style="width:20%;font-weight:bold;text-align:center;"><fmt:message key="label.rule.condition" /></td>
								   	 	<td style="width:40%;font-weight:bold;text-align:center;"><fmt:message key="label.rule.ruleContent" /></td>
								    	<td style="width:20%;font-weight:bold;text-align:center;"><fmt:message key="lable.customFilter.relation" /></td>
								    	<td style="width:20%;font-weight:bold;text-align:center;"><fmt:message key="label.rule.operation" /></td>
								  	</tr>
								  	
								  	<c:if test="${!empty rule.condition }">
								  	<s:iterator value="rule.condition.constraints" var="cs" status="idx">
								  	<c:set value="<%=new java.util.Random().nextInt(999999)%>" var="constraintId"></c:set>
								  	<tr id="constraint_${constraintId}" style="text-align: center;">
										<td style=";height:28px;text-align:center">
										    <input type="hidden" name="rule_condition_constraints_dataType" value="${cs.dataType }">
										    <input type="hidden" name="rule_condition_constraints_conNo" value="${cs.conNo}"/>
										    <script>
										    var pname="${cs.propertyName}";
										    var id="#${idx.count}_editcpn";
										    var pnameValue="${cs.propertyValue}";
										    
										    var array1=new Array();
										    var dataid=(pnameValue).replace("(","").replace(")","");
										    var dataValue=pname.replace(" in","").replace(" notIn","").replace("..","");
										    var dataZhi="";
										    array1=dataid.split(",");
										    for(var i=0;i<array1.length;i++){
										    	dataZhi+=dataValue+array1[i]+",";
										    }
										    $("#constraint${cs.conNo}").append("<input type='hidden' class='"+dataValue+"' value='"+dataZhi+"' >");
										    
										    //$('#rule_'+dataid).val(dataid);
										    
										    wstuo.rules.editRule.switchRuleNameActionName(pname,id);
										    </script>
										    
										   	<div id="${idx.count}_editcpn"></div>
										   
										    <input type="hidden" name="rule_condition_constraints_propertyName" value="${cs.propertyName}" >
										 </td>
										 <td style="text-align:left">${cs.propertyValueName}
										    <input type="hidden" name="rule_condition_constraints_propertyValueName" value="${cs.propertyValueName}" >
										    <input type="hidden" name="rule_condition_constraints_propertyValue" value="${cs.propertyValue}" >
										 </td>
										 <td style="text-align:center">
										 	<input type="hidden" name="rule_condition_constraints_orAnd" value="${cs.andOr}" >
										    <select name="rule_condition_constraints_orAnd" class="form-control" style="width:50%">
									    		<option value="or" <c:if test="${cs.andOr eq 'or'}">selected="selected"</c:if>>or</option>
									    		<option value="and" <c:if test="${cs.andOr eq 'and'}">selected="selected"</c:if>>and</option>
									    	</select>
										</td>
									    <td style="text-align:center">
									    	
									    	<a href="#" id="constraint_${constraintId}" onclick="wstuo.rules.ruleCM.removeRow('#constraint_${constraintId}')" ><i class=" glyphicon glyphicon-trash"></i></a>&nbsp;
									    	
									    	<a href="#" id="constraint_${constraintId}" onclick="basics.tableOpt.trUp('#constraint_${constraintId}')" ><i class=" glyphicon glyphicon-arrow-up"></i></a>&nbsp;
									    	
									    	<a href="#" id="constraint_${constraintId}" onclick="basics.tableOpt.trDown('#editRule_constraintsTable','#constraint_${constraintId}')" ><i class=" glyphicon glyphicon-arrow-down"></i></a>
									    
									    </td>
									</tr>
								  	</s:iterator>
								  	</c:if>								  	
								</table>   	
						</div>
						</form>
					</div>
					
					<c:if test="${param.tag!='autoUpdate'}">
					
					<div class="tab-pane" id="list2">
						<form>
						<div id="edit_rule_action_add" style="height:130px;;width:auto">
							<div style="margin-top: 10px;"><b><fmt:message key="title.rule.addOrEditActionSet" /></b></div>							
							<hr style="margin-top: 3px;margin-bottom: 5px;">
							<table style="width:100%"  cellspacing="1">
								<tr style="height:30px;">
									<td style="width: 25%">&nbsp;&nbsp;&nbsp;<fmt:message key="label.rule.actionType" /></td>
										<td style="width: 75%;margin-right:10px;">
										    <table width="100%">
												<tr>
													<td>
										   			 	<select style="font-size:12px; width:170px;"  id="editRule_executeAction" class="form-control"></select>
										   			</td>
										    	</tr>
										    </table>
										</td>
									</tr>
									<tr style="height:30px;">
										<td>&nbsp;&nbsp;&nbsp;<fmt:message key="label.rule.actionDetail" /></td>
										<td><br/>
											<table width="100%">
										  		<tr>
													<td style="width:60%">
														<div id="editRule_givenValueDIV">
														<c:if test="${param.flag == 'changeApproval' or param.flag=='changeProce'}">
															<select id="editRule_givenValue" style="width:170px" class="form-control">
																<option value='true'><fmt:message key="common.yes" /></option>
																<option value='false'><fmt:message key="common.not" /></option>
															</select>
														</c:if>
														<c:if test="${param.flag == 'mailToRequest' }">
															<input style='width:170px;' id='editRule_givenValue' class="form-control"/>
														</c:if>
														<c:if test="${param.flag != 'changeApproval' && param.flag != 'mailToRequest' && param.flag != 'changeProce'}">
															<input id="editRule_givenValue_Panel" style='width:170px;cursor:pointer;color:#555555' readonly class="form-control"/>
															<input type="hidden" id="editRule_givenValue" />
														</c:if>
												
											</div>
										</td>
										<td style="width:40%">
											<a class="btn btn-primary" plain="true"  style="margin-right: 15px;min-width:50px;" id="editRule_addActionsToList"><fmt:message key="label.rule.confirm" /></a>
										</td>
											
										</tr>
										
										</table>
										</td>
									</tr>
								</table>
						</div>
					<hr>
						<div style="max-height:300px;width:auto;overflow:auto;">
						
							<table id="editRule_actionTable" style="width:100%" class="table table-bordered" cellspacing="1">
							<tr height="30px" ><th align="left" colspan="3"><fmt:message key="title.rule.actionSet" /></th></tr>
								
									<tr>
										<td style="width: 25%; font-weight: bold; text-align: center;"><fmt:message key="label.rule.actionType" /></td>
										<td style="width: 50%; font-weight: bold; text-align: center;"><fmt:message key="label.rule.actionDetail" /></td>
										<td style="width: 25%; font-weight: bold; text-align: center;"><fmt:message key="label.rule.operation" /></td>
									</tr>
									
									
								  <c:if test="${!empty rule.actions }">
					 
								  <s:iterator value="rule.actions" var="rc" status="idx">
								  
								  <tr id="action${rc.actionNo}">
								    <td style=";height:28px;text-align:center">
								    <input type="hidden" name="rule_actions_dataType" value="${rc.dataType}">
								    <input type="hidden" name="rule_actions_actionNo" value="${rc.actionNo}" />
								    
								    <script>
								    var pname="${rc.propertyName}";
								    var id="#${idx.count}_editacpn";
								    wstuo.rules.editRule.switchRuleNameActionName(pname,id);
								    </script>
								    
								    <div id="${idx.count}_editacpn"></div>
								    
								    <input type="hidden" style="border:none" name="rule_actions_propertyName" value="${rc.propertyName}" readonly/>
								
								    </td>
								    <td style=";text-align:center">${rc.givenName}
								    <input type="hidden" name="rule_actions_givenValue" value="${rc.givenValue}" readonly>
								    <input type="hidden" name="rule_actions_givenName" value="${rc.givenName}" readonly>
					
								    </td>
								    <td style=";text-align:center">
								    	
								   		<a href="#" id="constraint_${constraintId}" onclick="wstuo.rules.ruleCM.removeRowRule('#action${rc.actionNo}')"><i class="glyphicon glyphicon-trash"></i></a>
										    
								    </td>
								  </tr>
								  </s:iterator>
								  </c:if>					
								</table>
							</div>
						</form>
					</div>
					</c:if>
				</div>
			</div>
		</div>
	</div>
	</div>
		</div>
	</div>

<%--选择审批成员 --%>
	<div id="selectRuleCABMember" class="WSTUO-dialog" title="<fmt:message key="title.selectCabMember" />" style="width:520px;padding: 2px;">
	 	<div align="center"><span id="selectRuleCABMember_tip" style="color:red;font-size:14px"></span></div>
	 	<div style="width:500px;">
	 	<table id="selectRuleCABMemberGrid"></table>
		<div id="selectRuleCABMemberPager"></div>
		</div>
	 </div>

	<div id="selectRuleCABMemberToolBar" style="display: none;">&nbsp;&nbsp;CAB:&nbsp;<select id="cabMemberRuleOption" onchange="itsm.change.changeApproval.selectRuleChange(this.value)"></select>
		&nbsp;&nbsp;&nbsp;&nbsp;
		<a class="btn btn-primary" plain="true" icon="icon-ok" onclick="itsm.change.changeApproval.select_approval_rule_aff_multi()"><fmt:message key="common.select"/></a>
	</div>	
		
		<div  border="false" style="overflow: hidden; height: 35px; text-align:center;">
			<c:if test="${rule.dataFlag!=1}">
				<a class="btn btn-primary" plain="true"  style="margin-right:15px" id="editRule_saveRule" title="<fmt:message key="common.save" />"><fmt:message key="common.save" /></a>
			</c:if>
			<a class="btn btn-primary" plain="true" style="margin-right:15px" id="editRule_backToRuleList" title="<fmt:message key="common.returnToList" />"><fmt:message key="common.returnToList" /></a>
		</div>			
		<br/> 
	</div>
</div>