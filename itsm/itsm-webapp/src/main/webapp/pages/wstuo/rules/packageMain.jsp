<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="../../language.jsp" %>


<script src="../js/wstuo/rules/packageMain.js"></script>

<div class="tab-pane active" id="packageMain_content">
	<div class="box-inner">
		<div class="box-header well" data-original-title="">
	        <h2><i class="glyphicon glyphicon-list"></i>&nbsp;<fmt:message key="lable.com.drools.rule.pages.Management" /></h2>
        </div>
		<table id="packagesGrid"></table>
		<div id="packagesGridPager" ></div>
		<div id="packagesGridToolbar" style="display: none">	
			<div class="panelBar">	
				<a class="btn btn-default btn-xs" plain="true" id="packagesGrid_add"  title="<fmt:message key="common.add" />"><i class="glyphicon glyphicon-plus"></i>&nbsp;<fmt:message key="common.add" /></a>
				<a class="btn btn-default btn-xs" plain="true" id="packagesGrid_edit" title="<fmt:message key="common.edit" />"><i class="glyphicon glyphicon-edit"></i>&nbsp;<fmt:message key="common.edit" /></a> 
				<a class="btn btn-default btn-xs" plain="true" id="packagesGrid_delete" title="<fmt:message key="common.delete" />"><i class="glyphicon glyphicon-trash"></i>&nbsp;<fmt:message key="common.delete" /></a> 
				<a class="btn btn-default btn-xs" plain="true" id="packagesGrid_search" title="<fmt:message key="common.search" />"><i class="glyphicon glyphicon-search"></i>&nbsp;<fmt:message key="common.search" /></a>
			</div>
		</div>
	</div>
	<!-- 新增规则包 -->	
	<div id="packageAddWin" title='<fmt:message key="lable.com.drools.rule.pages.add" />' class="WSTUO-dialog" style="margin-top: 5px; height:auto;">
		<form class="form-horizontal" event="wstuo.rules.packageMain.package_save">
			<table style="width:100%" cellspacing="1">
				<tr>
					<td style="width:25%"><fmt:message key="lable.com.drools.rule.pages.name" /></td>
					<td style="width:75%">
						<input name="rulePackageDto.packageName" id="add_packageName"  class="form-control" required="true" onkeyup="value=value.replace(/[^\a-\z\A-\Z\.]/g,'')" style="width:95%" />
					</td>
				</tr>
				<tr>
					<td style="width:25%;color:red;"><fmt:message key="lable.com.drools.rule.pages.Tips" /></td>
					<td style="width:75%;color:red;">
						<fmt:message key="lable.com.drools.rule.pages.Tips.Information" /><br/>(<fmt:message key="label.for.example" />:label.rulePackage.requestRule)
					</td>
				</tr>
				<tr>
					<td><br/><fmt:message key="label.type" /></td>
					<td><br/>
						<select id="ruleTypeAdd" name="rulePackageDto.module" class="form-control" required="true" style="width: 95%;">
							<option value=""><fmt:message key="common.pleaseSelect" /></option>
							<c:if test="${requestHave eq true}">
							<option value="request"><fmt:message key="lable.Request.process.flow.trigger"/></option>
							</c:if>
							<c:if test="${changeHave eq true}">
							<option value="change"><fmt:message key="lable.Circulation.trigger.change.process"/></option>
							</c:if>
					</select>
					</td>
				</tr>
				<tr>
					<td style="width:20%"><br/><fmt:message key="lable_Rules_package_name" /></td>
					<td style="width:80%">
						<br/>
						<input name="rulePackageDto.rulePackageRemarks" class="form-control" required="true" id="add_rulePackageRemarks" style="width:95%" />
					</td>
				</tr>
				<tr>
				<td colspan="2"><br/>
					<input type="submit"  class="btn btn-primary btn-sm" style="margin-right:15px" value="<fmt:message key="common.save" />">
				</td>
				</tr>
			</table>
		</form>
	</div>
	<!-- 编辑规则包 -->	
	<div id="packageEditWin" title='<fmt:message key="lable.com.drools.rule.pages.Tips.edit" />' class="WSTUO-dialog" style="height:auto;margin-top: 5px;">
		<form>	
			<input name="rulePackageDto.rulePackageNo" id="packageMain_rulePackageNo" type="hidden"/>
				<table style="width:100%" cellspacing="1">
					<tr>
						<td style="width:20%"><fmt:message key="lable_Rules_package_name" /></td>
						<td style="width:80%">
						<input name="rulePackageDto.rulePackageRemarks" id="rulePackage_rulePackageRemarks" style="width:96%" class="form-control"/>
						</td>
					</tr>
					<tr>
					<td colspan="2"><br/>
						<input type="button" id="EditpackageBtn" class="btn btn-primary btn-sm" style="margin-right:15px" value="<fmt:message key="common.save" />">
					</td>
					</tr>
				</table>
		</form>
	</div>

	<!-- 搜索规则包 -->	
	<div id="packageSearchWin" title='<fmt:message key="lable.com.drools.rule.pages.Tips.Search" />' class="WSTUO-dialog" style="height:auto;margin-top: 5px;">
		<form>
			<table style="width:100%" cellspacing="1">
				<tr>
					<td style="width:20%"><fmt:message key="lable.com.drools.rule.pages.name" /></td>
					<td style="width:80%">
						<input name="rulePackageDto.packageName" id="SearchpackageName" style="width:96%" class="form-control"/>
					</td>
				</tr>
				<tr>
					<td><br/><fmt:message key="label.type" /></td>
					<td><br/>
						<select id="ruleTypeSearch" name="rulePackageDto.flagName" style="width:96%" class="form-control">
							<option value=""><fmt:message key="common.pleaseSelect" /></option>
							<c:if test="${requestHave eq true}">
							<option value="saveRequest"><fmt:message key="lable.Added.triggered.request"/></option>
							<option value="requestProce"><fmt:message key="lable.Request.process.flow.trigger"/></option>
							</c:if>
							<c:if test="${changeHave eq true}">
							<option value="saveChange"><fmt:message key="lable.When.the.change.is.triggered.new"/></option>
							<option value="changeProce"><fmt:message key="lable.Circulation.trigger.change.process"/></option>
							</c:if>
						</select>
					</td>
			</tr>
			<tr>
				<td style="width:20%"><br/><fmt:message key="lable.com.drools.rule.pages.Tips.Remark" /></td>
				<td style="width:80%"><br/>
				<input name="rulePackageDto.rulePackageRemarks" id="SearchrulePackageRemarks" style="width:96%" class="form-control"/>
				</td>
			</tr>
			<tr>
			<td colspan="2"><br/>
				<input type="hidden" name="requestQueryDTO.keyWord" id="requestQueryDTO_keyWord">
				<input type="button" id="SearchpackageBtn" class="btn btn-primary btn-sm" style="margin-right:15px" value="<fmt:message key="common.search" />"/>
			</td>
			</tr>
		</table>
	</form>
	</div>
</div>