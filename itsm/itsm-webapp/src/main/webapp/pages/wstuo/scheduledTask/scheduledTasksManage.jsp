<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ include file="../../language.jsp" %>
<script src="${pageContext.request.contextPath}/js/common/tools/scheduledTask/scheduledTasksManage.js?random=<%=new java.util.Date().getTime()%>"></script>

<div class="loading" id="scheduledTasksManage_loading"><img src="${pageContext.request.contextPath}/images/icons/loading.gif" /></div>
<div class="content" id="scheduledTasksManage_content">

<!-- 定期任务数据列表 -->
<table id="scheduledTasksGrid"></table>
<div id="scheduledTasksPager"></div>

<!-- 定期任务菜单操作项 -->
<div id="scheduledTaskOptItem" style="display: none">
	

	<sec:authorize url="/pages/user!save.action">
		<a class="easyui-linkbutton" plain="true" icon="icon-add" id="link_scheduledTask_add" title="<fmt:message key="common.add" />"></a> 
	</sec:authorize>
	
	<sec:authorize url="/pages/user!merge.action">
		<a class="easyui-linkbutton" plain="true" icon="icon-edit" id="link_scheduledTask_edit" title="<fmt:message key="common.edit" />"></a>
	</sec:authorize> 
	
	<sec:authorize url="/pages/user!delete.action">
	<a class="easyui-linkbutton" plain="true" icon="icon-cancel" id="link_scheduledTask_delete" title="<fmt:message key="common.delete" />"></a> 
	</sec:authorize>
	
	<sec:authorize url="/pages/user!find.action">
	<a class="easyui-linkbutton" plain="true" icon="icon-search" id="scheduledTask_search" title="<fmt:message key="common.search" />"></a> 
	</sec:authorize>
	<%-- 
	&nbsp;|&nbsp;<a href="javascript:showHelpFilm('scheduledTask.flv')" plain="true" class="easyui-linkbutton" icon="icon-help"></a>
 	--%>
</div>


<!-- 搜索用户 -->
<div id="scheduledTaskSearchDiv" class="WSTUO-dialog" title="<fmt:message key="common.search" />" style="width:400px;height:auto">
	<form>
		<div class="lineTableBgDiv">
			<table style="width:100%" class="lineTable" cellspacing="1">
				<tr>
					<td><fmt:message key="label.type" /></td>
					<td>
						<select name="queryDTO.scheduledTaskType" class="input">
							<option value="">--<fmt:message key="common.pleaseSelect" />--</option>
							<c:if test="${requestHave eq true}">
							<option value="request"><fmt:message key="label.scheduled.request.task" /></option>
							</c:if>
							<option value=sla><fmt:message key="label.scheduled.sla.task" /></option>
							<option value="email"><fmt:message key="label.scheduled.email.task" /></option>
							<c:if test="${requestHave eq true and problemHave eq true}">
							<option value="request2problem"><fmt:message key="label.ur.request_to_problem_rule" /></option>
							</c:if>
							<c:if test="${cimHave eq true}">
							<option value="configureItem"><fmt:message key="label.scheduled_configureItem.task" /></option>
							</c:if>
							<option value="adUpdate"><fmt:message key="label_ad_update_task" /></option>
							<option value="senReport"><fmt:message key="label.customReport.autoSendReport" /></option>
						</select>
					</td>
				</tr>
				<tr>
					<td><fmt:message key="label.scheduled.type" /></td>
					<td>
						<select name="queryDTO.timeType" class="input">
							<option value="">--<fmt:message key="common.pleaseSelect" />--</option>
							<option value="day"><fmt:message key="title.scheduled.day.plan" /></option>
							<option value="weekly"><fmt:message key="title.scheduled.week.plan" /></option>
							<option value="month"><fmt:message key="title.scheduled.month.plan" /></option>
							<option value="cycle"><fmt:message key="title.scheduled.cycle.plan" />(<fmt:message key="label.sla.day" />)</option>
							<option value="cycleMinute"><fmt:message key="title.scheduled.cycle.plan" />(<fmt:message key="label.sla.minute" />)</option>
							<option value="on_off"><fmt:message key="title.scheduled.on-off.plan" /></option>
						</select>
					</td>
				</tr>
				<%-- <tr>
					<td><fmt:message key="common.title" /></td>
					<td><input name="queryDTO.keywork" id="scheduledTask_search_keywork" style="width:95%" /></td>
				</tr> --%>
				<tr>
				<tr>
					<td><fmt:message key="label.dateRange"/></td>
					<td>
						<input id="scheduledTask_search_taskDate" class="easyui-validatebox input"  validType="date" name="queryDTO.taskDate" style="width:100px" readonly="readonly"/>&nbsp;<fmt:message key="setting.label.to"/>&nbsp;
						<input id="scheduledTask_search_taskEndDate" class="easyui-validatebox input"  validType="DateComparison['scheduledTask_search_taskDate']" name="queryDTO.taskEndDate" style="width:100px" readonly="readonly"/>
						<a class="easyui-linkbutton" plain="true" icon="icon-clean" onclick="cleanIdValue('scheduledTask_search_taskDate','scheduledTask_search_taskEndDate')" title="<fmt:message key="label.request.clear"/>"></a>
					</td>
				</tr>
		    	<td><fmt:message key="common.createTime"/>&nbsp;</td>
		    	<td>
			    	<input name="queryDTO.startCreateTime" class="easyui-validatebox input" id="scheduledTask_search_startCreateTime" style="width:100px" readonly="readonly"/>&nbsp;<fmt:message key="setting.label.to"/>&nbsp;
			    	<input name="queryDTO.endCreateTime" class="easyui-validatebox input" id="scheduledTask_search_endCreateTime"  validType="DateComparison['scheduledTask_search_startCreateTime']" style="width:100px" readonly="readonly"/>
		    		<a class="easyui-linkbutton" plain="true" icon="icon-clean" onclick="cleanIdValue('scheduledTask_search_startCreateTime','scheduledTask_search_endCreateTime')" title="<fmt:message key="label.request.clear"/>"></a>
		    	</td>
		    	</tr>
		    	<tr>
		    	<td><fmt:message key="common.updateTime"/>&nbsp;</td>
		    	<td>
		    		<input name="queryDTO.startUpdateTime" class="easyui-validatebox input" id="scheduledTask_search_startUpdateTimes" style="width:100px" readonly="readonly"/>&nbsp;<fmt:message key="setting.label.to"/>&nbsp;
		    		<input name="queryDTO.endUpdateTime" class="easyui-validatebox input" id="scheduledTask_search_startUpdateTime" validType="DateComparison['scheduledTask_search_startUpdateTimes']" style="width:100px" readonly="readonly"/>
		    		<a class="easyui-linkbutton" plain="true" icon="icon-clean" onclick="cleanIdValue('scheduledTask_search_startUpdateTimes','scheduledTask_search_startUpdateTime')" title="<fmt:message key="label.request.clear"/>"></a>
		    	</td>
		    	</tr>
		    	
		    	<tr>
		    		<td colspan="2">
		        		<a class="easyui-linkbutton" icon="icon-search"  id="scheduledTask_search_doSearch"><fmt:message key="common.search"/></a>
		        	</td>
		    	</tr>
			</table>
		</div>
	</form>		
</div>
<!-- 选择创建的类型 -->
<div id="scheduledTaskTypeDiv" class="WSTUO-dialog" title="<fmt:message key="label.type" />" style="width:300px;height:auto;padding: 5px;">
<c:if test="${requestHave eq true}">
<label><input type="radio" name="scheduledTaskType" value="request" checked /><fmt:message key="label.scheduled.request.task" /></label><br>
<label><input type="radio" name="scheduledTaskType" value="sla" /><fmt:message key="label.scheduled.sla.task"/></label><br>
<label><input type="radio" name="scheduledTaskType" value="email" /><fmt:message key="label.scheduled.email.task" /></label><br>
</c:if>
<c:if test="${requestHave eq true and problemHave eq true }">
<label><input type="radio" name="scheduledTaskType" value="request2problem" /><fmt:message key="label.ur.request_to_problem_rule" /></label><br>
</c:if>
<c:if test="${cimHave eq true}">
<label><input type="radio" name="scheduledTaskType" value="configureItem" /><fmt:message key="label.scheduled_configureItem.task" /></label><br>
</c:if>
<label><input type="radio" name="scheduledTaskType" value="adUpdate" /><fmt:message key="label_ad_update_task" /></label><br>
<label><input type="radio" name="scheduledTaskType" value="senReport"  /><span id="scheduledTaskTypeAutoSend"></span></label><br>
<br>
<label><a class="easyui-linkbutton" icon="icon-ok"  id="scheduledTaskType_confirm"><fmt:message key="tool.im.confirmSelectUser"/></a></label>
<br><br>
</div>

<!-- 创建定期任务 -->
<div id="scheduledTask_add_win" class="WSTUO-dialog" title="<fmt:message key="创建定期任务" />" style="width:520px;height:auto;">
<form id="scheduledTask_add_comm_form">


<input type="hidden" id="base_scheduledTask_reportId" name="scheduledTaskDTO.reportId" value="0"  />
<input type="hidden" id="base_scheduledTask_id" name="scheduledTaskDTO.scheduledTaskId"  />
<input type="hidden" id="base_scheduledTaskType" name="scheduledTaskDTO.scheduledTaskType"  />
<input type="hidden" id="base_scheduledTask_beanId" name="scheduledTaskDTO.beanId"  />
<input type="hidden" id="base_scheduledTask_companNo" name="scheduledTaskDTO.requestDTO.companyNo"  />

</form>
<form id="scheduledTask_add_form">
<div class="lineTableBgDiv" style="height: auto">
<table style="width:100%" class="lineTable" cellspacing="1">
	<tr id="base_scheduledTask_title_tr">
		<td><fmt:message key="common.title" /></td>
		<td><input id="base_scheduledTask_etitle" name="scheduledTaskDTO.requestDTO.etitle" class="easyui-validatebox input" validType="length[1,200]" required="true" /></td>
	</tr>
	<tr>
		<td colspan="2">
			<table width="100%">
				<tr>
					<td>
						<label><input type="radio" name="scheduledTaskDTO.timeType" id="base_timeType_day" value="day" onclick="common.tools.scheduledTask.scheduledTask.everyWhatChange(this.value,'base_')" checked="checked">
						<fmt:message key="title.scheduled.day.plan" /></label>
					</td>
					<td>
						<label><input type="radio" name="scheduledTaskDTO.timeType" id="base_timeType_weekly" value="weekly" onclick="common.tools.scheduledTask.scheduledTask.everyWhatChange(this.value,'base_')">
						<fmt:message key="title.scheduled.week.plan" /></label>
					</td>
					<td>
						<label><input type="radio" name="scheduledTaskDTO.timeType" id="base_timeType_month" value="month" onclick="common.tools.scheduledTask.scheduledTask.everyWhatChange(this.value,'base_')">
						<fmt:message key="title.scheduled.month.plan" /></label>
					</td>
				</tr>
				<tr>
					<td>
						<%-- 周期性计划(天) --%>
						<label><input type="radio" name="scheduledTaskDTO.timeType" id="base_timeType_cycle" value="cycle" onclick="common.tools.scheduledTask.scheduledTask.everyWhatChange(this.value,'base_')">
						<fmt:message key="title.scheduled.cycle.plan" />(<fmt:message key="label.sla.day" />)</label>
					</td>
					<td>
						<%-- 周期性计划(分钟) --%>
						<label><input type="radio" name="scheduledTaskDTO.timeType" id="base_timeType_cycleMinute" value="cycleMinute" onclick="common.tools.scheduledTask.scheduledTask.everyWhatChange(this.value,'base_')">
						<fmt:message key="title.scheduled.cycle.plan" />(<fmt:message key="label.sla.minute" />)</label>
					</td>
					<td>
						<label><input type="radio" name="scheduledTaskDTO.timeType" id="base_timeType_on_off" value="on_off" onclick="common.tools.scheduledTask.scheduledTask.everyWhatChange(this.value,'base_')">
						<fmt:message key="title.scheduled.on-off.plan" /></label>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<b>[<span id="base_everyWhat_show"><fmt:message key="label.scheduledTask.day" /></span>]</b><br>
			
			<div id="base_everyWhat_weekly" style="display: none;" class="lineTableBgDiv">
				<!--每周定时维护： -->
				
				
				<table width="100%" class="lineTableBgDiv" class="lineTable" cellspacing="1">
					<tr><td colspan="4"><input type="checkbox" id="base_scheduledTask_weekWeeks_all" value="1" onclick="common.tools.scheduledTask.scheduledTask.checkAll('base_scheduledTask_weekWeeks_all','base_everyWhat_weekly','scheduledTaskDTO.weekWeeks')" />
					<fmt:message key="label.date.weekly" />：</td></tr>
					<tr>
						<td><input type="checkbox" id="checkbox_SUN" value="SUN" name="scheduledTaskDTO.weekWeeks" /><fmt:message key="label.orgSettings.sunday" /></td>
						<td><input type="checkbox" id="checkbox_MON" value="MON" name="scheduledTaskDTO.weekWeeks" /><fmt:message key="label.orgSettings.monday" /></td>
						<td><input type="checkbox" id="checkbox_TUE" value="TUE" name="scheduledTaskDTO.weekWeeks" /><fmt:message key="label.orgSettings.tuesday" /></td>
						<td><input type="checkbox" id="checkbox_WED" value="WED" name="scheduledTaskDTO.weekWeeks" /><fmt:message key="label.orgSettings.wednesday" /></td>
					</tr>
					<tr>
						<td><input type="checkbox" id="checkbox_THU" value="THU" name="scheduledTaskDTO.weekWeeks" /><fmt:message key="label.orgSettings.thursday" /></td>
						<td><input type="checkbox" id="checkbox_FRI" value="FRI" name="scheduledTaskDTO.weekWeeks" /><fmt:message key="label.orgSettings.friday" /></td>
						<td><input type="checkbox" id="checkbox_SAT" value="SAT" name="scheduledTaskDTO.weekWeeks" /><fmt:message key="label.orgSettings.saturday" /></td>
						<td></td>
					</tr>
				</table>
			</div>
			
			<div id="base_everyWhat_monthly" style="display: none;"  class="lineTableBgDiv">
				<!-- 每月定时维护：  -->
				
				<table width="100%" class="lineTableBgDiv" class="lineTable" cellspacing="1">
					<tr><td colspan="4"><input type="checkbox" value="1" id="base_scheduledTask_monthMonths_all" onclick="common.tools.scheduledTask.scheduledTask.checkAll('base_scheduledTask_monthMonths_all','base_everyWhat_monthly','scheduledTaskDTO.monthMonths')" />
					<fmt:message key="label.date.month" />：</td></tr>
					<tr>
						<td><input type="checkbox" id="checkbox_JAN" value="JAN" name="scheduledTaskDTO.monthMonths" /><fmt:message key="label.date.Jan" /></td>
						<td><input type="checkbox" id="checkbox_FEB" value="FEB" name="scheduledTaskDTO.monthMonths" /><fmt:message key="label.date.Feb" /></td>
						<td><input type="checkbox" id="checkbox_MAR" value="MAR" name="scheduledTaskDTO.monthMonths" /><fmt:message key="label.date.Mar" /></td>
						<td><input type="checkbox" id="checkbox_APR" value="APR" name="scheduledTaskDTO.monthMonths" /><fmt:message key="label.date.Apr" /></td>
					</tr>
					<tr>
						<td><input type="checkbox" id="checkbox_MAY" value="MAY" name="scheduledTaskDTO.monthMonths" /><fmt:message key="label.date.May" /></td>
						<td><input type="checkbox" id="checkbox_JUN" value="JUN" name="scheduledTaskDTO.monthMonths" /><fmt:message key="label.date.Jun" /></td>
						<td><input type="checkbox" id="checkbox_JUL" value="JUL" name="scheduledTaskDTO.monthMonths" /><fmt:message key="label.date.Jul" /></td>
						<td><input type="checkbox" id="checkbox_AUG" value="AUG" name="scheduledTaskDTO.monthMonths" /><fmt:message key="label.date.Aug" /></td>
					</tr>
					<tr>
						<td><input type="checkbox" id="checkbox_SEP" value="SEP" name="scheduledTaskDTO.monthMonths" /><fmt:message key="label.date.Sep" /></td>
						<td><input type="checkbox" id="checkbox_OCT" value="OCT" name="scheduledTaskDTO.monthMonths" /><fmt:message key="label.date.Oct" /></td>
						<td><input type="checkbox" id="checkbox_NOV" value="NOV" name="scheduledTaskDTO.monthMonths" /><fmt:message key="label.date.Nov" /></td>
						<td><input type="checkbox" id="checkbox_DEC" value="DEC" name="scheduledTaskDTO.monthMonths" /><fmt:message key="label.date.Dec" /></td>
					</tr>
					<tr><td colspan="4">
						<fmt:message key="common.date"></fmt:message>：
						<select name="scheduledTaskDTO.monthDay" id="base_scheduledTask_monthDay">
						</select>
					</td></tr>
				</table>

				
			</div>
			
			<div id="base_everyWhat_cycle" style="display: none;">
				<!-- 周期性地维护： ：  -->
				<hr>
				<fmt:message key="label.scheduledTask.every"></fmt:message>&nbsp;&nbsp;<input type="text" width="12px" class="easyui-numberbox" value="1" id="base_scheduledTask_cyclicalDay" name="scheduledTaskDTO.cyclicalDay">
				<fmt:message key="label.scheduledTask.day.one"></fmt:message>
			</div>
			
			<%-- 周期性执行(分钟) --%>
			<div id="base_everyWhat_cycleMinute" style="display: none;">
				
				<hr>
				<fmt:message key="label.scheduledTask.every"></fmt:message>&nbsp;&nbsp;
				<input type="text" width="12px" class="easyui-numberbox" value="1" id="base_scheduledTask_cyclicalMinute" name="scheduledTaskDTO.cyclicalMinute">
				<fmt:message key="label.sla.minute" />
				
			</div>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<div id="base_scheduledTask_taskDate" style="width: 100%">
			<span id="base_scheduledTask_startTime"><fmt:message key="label.sla.slaStartTime" /></span>&nbsp;&nbsp;
			<input type="text" id="base_scheduledTask_startDate_input" name="scheduledTaskDTO.taskDate" readonly>
			
			<div id="base_scheduledTask_endTime_div">
			<br>
			<span id="base_scheduledTask_endTime"><fmt:message key="label.sla.slaEndTime" /></span>&nbsp;&nbsp;
			<input type="text" name="scheduledTaskDTO.taskEndDate" id="base_scheduledTask_endDate_input" class="easyui-validatebox" validType="DateComparison['base_scheduledTask_startDate_input']" readonly ></div>
			
			<hr></div>
			
			<div id="base_scheduledTask_taskHour_taskMinute">
				<fmt:message key="label.scheduledTask.specific.time" />：
				<select name="scheduledTaskDTO.taskHour" id="base_scheduledTask_hours" >
					<c:forEach var="i" begin="0" end="23" step="1"> 
						<option value="${i}">${i}</option>
					</c:forEach>
				</select>
				<fmt:message key="label.orgSettings.hour" />
				<!-- 分 -->
				<select name="scheduledTaskDTO.taskMinute" id="base_scheduledTask_minute">
					<c:forEach var="i" begin="0" end="59" step="1"> 
						<option value="${i}">${i}</option>
					</c:forEach>
				</select>
				<fmt:message key="label.orgSettings.minute" />
			</div>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<a class="easyui-linkbutton" icon="icon-save"  id="scheduledTask_save_but"><fmt:message key="common.save"/></a>
			<a class="easyui-linkbutton" icon="icon-save" id="scheduledTask_ok_btn" onclick="javascript:$('#scheduledTask_add_win').window('close');"><fmt:message key="common.save"/></a>
		</td>
	</tr>
</table>
</div>	
</form>
</div>
</div>