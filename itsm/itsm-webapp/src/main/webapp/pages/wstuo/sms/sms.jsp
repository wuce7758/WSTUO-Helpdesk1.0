﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../../language.jsp" %>

<script src="../js/wstuo/sms/sms.js"></script>

<script type="text/javascript">

$(document).ready(function(){
	wstuo.sms.sms.queryMoney();
	//动态创建事件
	//$('#sendMsgBtn').click(wstuo.sms.sms.sendSms);
});

</script>
<form id="SMSForm" event="wstuo.sms.sms.sendSms">
<div class="tab-pane active" >
	<div class="box-inner">
		<div class="box-header well" data-original-title="">
	        <h2><i class="glyphicon glyphicon-list">&nbsp;</i>手机短信</h2>
        </div>
        
	<div class="content" id="sendMsgDiv" border="none" fit="true" style="height: 98%;">

		<!-- 新的面板 start-->
		<div id="sendMsgDiv_layout" >
			<!-- 左边信息start -->			
		<div class="row">
			<div class="box col-md-3" >
			<div class="box-inner" style="padding-left: 5px;">
		        <div class="box-content" region="west" split="true">	 
            		<div class="box-header well" >
			       		<h2><fmt:message key="tool.sms.sendList"/></h2>
		        	</div>	
					<table style="width:100%" class="table table-bordered" cellspacing="1">
						<tr>
							<td style="border-bottom: 0px;">
								<fmt:message key="tool.sms.AphoneNumberPerLine"/>&nbsp;<span style="color:red">(;)*</span>
							</td>
						</tr>
						<tr>
							<td style="border-top: 0px;">
								<textarea id="mobarea" style="overflow:auto; width:90%; height:350px" class="form-control" required="true"></textarea>
							</td>
						</tr>
					</table>
		        </div>		          
		      </div>
		    </div>		
		    <!-- 左边信息 end-->
		    <!-- 右边详细信息 start -->
		    <div class="box col-md-7">
        		<div class="box-inner" id="slaDetailTab">	      		
				<div class="box-content" region="center" >
					<div class="box-header well" >
			       		<h2><fmt:message key="tool.sms.smsContent"/></h2>
		        	</div>	
						<table class="table table-bordered" cellspacing="1" >
							<tr>
								<td style="border-bottom: 0px;"> <fmt:message key="tool.sms.messageContentAlert"/></td>
							</tr>
							<tr>
								<td style="border-bottom: 0px;border-top: 0px;">
									<textarea id="smscontent" name="content" style="width:98%; height:150px" class="form-control" required="true"></textarea>
								</td>									
							</tr>
							<tr>
								<td style="border-top: 0px;">
									<input type="hidden" name="mobiles" id="mobiles"> 
									<button id="sendMsgBtn" class="btn btn-primary btn-sm"><fmt:message key="tool.sms.send"/></button>
								</td>
							</tr>
						</table>
					
				</div>     
          		</div>
			</div>

			<div class="box col-md-2">
        		<div class="box-inner" id="slaDetailTab">	      		
				<div class="box-content" region="east" >
					<div class="box-header well" >
			       		<h2><fmt:message key="tool.sms.accountInfo"/></h2>
		        	</div>					
					<table class="table table-bordered" cellspacing="1" >
						<tr>
							<td style="border-bottom: 0px;"> 
								
								<div id="accountMsg" style="line-height:25px"></div>
					            <sec:authorize url="/pages/smsRecord!findPager.action">
									<div style="padding-top:12px">
										
										<a href="javascript:wstuo.sms.sms.showHistoryRecord()"><fmt:message key="label.sms.historyRecord"/></a>
									</div>
								</sec:authorize> 
							</td>
						</tr>				
					</table>
				</div>     
          		</div>
			</div>

		</div>
		</div>
	</div>

</div>
</div>  </form>        