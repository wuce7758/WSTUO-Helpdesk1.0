<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="../../language.jsp" %>
<%@ page import="java.io.File,java.util.List,java.util.ArrayList,java.util.Date"%>
<%@page import="org.springframework.web.context.support.WebApplicationContextUtils"%>
<%@page import="org.springframework.context.ApplicationContext"%>
<%@page import="com.wstuo.common.tools.service.IPagesSetSerice"%>
<%@page import="com.wstuo.common.tools.entity.PagesSet"%>
<%
String images="logo.jpg";

%>
<c:set var="images" value="<%=images%>"/>

<script>var merge_security='no';</script>


<sec:authorize url="/pages/organization!mergeCompany.action">

<script>merge_security='yes';</script>

</sec:authorize>

<script>
$(document).ready(function(){
	 $("#company_loading").hide();
	 $("#company_content").show();
	 
	 if(merge_security=="no"){
	 	$("#companyInfoDiv .easyui-validatebox,#init_address").attr('disabled','disabled'); 
	 }
}); 
</script>
<script src="${pageContext.request.contextPath}/js/wstuo/sysMge/company.js?random=<%=new java.util.Date().getTime()%>"></script>
<script src="${pageContext.request.contextPath}/js/wstuo/sysMge/mailServer.js?random=<%=new java.util.Date().getTime()%>"></script>
<script src="${pageContext.request.contextPath}/js/wstuo/sms/smsAccount.js?random=<%=new java.util.Date().getTime()%>"></script>

<div  id="companyCenter" style="width:100%;">
<div class="box-inner">
	<div class="box-header well" >
        <h2><i class="glyphicon glyphicon-list"></i>&nbsp;第三方接口</h2>
        <div class="box-icon">

        </div>
    </div>
	<div class="box-content">
		<ul class="nav nav-tabs" >
			<sec:authorize url="/pages/mailServer!saveOrUpdateEmailServer.action">
				<li  class="active"><a href="#list1"><span><fmt:message key="title.basicSettings.sendServiceEmail" /></span></a></li>	
			</sec:authorize>
			<sec:authorize url="/pages/mailServer!saveOrUpdateEmailServer.action">
				<li><a href="#list2"><span><fmt:message key="label.customReport.sendToEmail" /></span></a></li>
			</sec:authorize>
			<sec:authorize url="/pages/sms!saveOrUpdateSMSAccount.action">
				<li><a href="#list3"><span><fmt:message key="title.basicSettings.smsAccount"/></span></a></li>	
			</sec:authorize>		
		</ul>			
	</div>
	<div class="tabsContent" >
	<div id="myTabContent" class="tab-content">
		<sec:authorize url="/pages/mailServer!saveOrUpdateEmailServer.action">
			<!-- 发送邮箱设置 -->
			<div class="tab-pane active" id="list1">
				<div id="MailServerDiv" style="width: 100%;">	
					<form id="MailServerForm" class="form-horizontal" event="wstuo.sysMge.mailServer.saveOrUpdateMailServer"> 
					<input type="hidden" id="radio_value" value="">
					<!-- 设置服务邮箱开始 -->				
					<div class="hisdiv" style="">
						<div>
							<table style="width:100%;" cellspacing="1" >
								<tr style="height: 40px;">
									<td style="width:15%;padding-left: 15px;" >
										<span style="font-weight:bold;"><fmt:message key="label.email.server.type" />：</span>
									</td>
									<td >
										<input type="radio" name="emailServerDTO.emailType" value="normal" id="normalEmailRadio" checked="checked" onclick="emailServerChange(this.value)"  />
										<fmt:message key="label.normal.email" />&nbsp;&nbsp;&nbsp;&nbsp;
										<input type="radio" name="emailServerDTO.emailType" value="exchange" id="exchangeEmailRadio" onclick="emailServerChange(this.value)" />
										<fmt:message key="lable.exchange.email" />
									</td>
								</tr>
								<tr style="height: 40px;">
									<td style="width:15%;padding-left: 15px;" >
										<span style="font-weight:bold;" class="control-label"><fmt:message key="label.your.name" />:</span>
									</td>
									<td>
										<input name="emailServerDTO.personal" id="personal" onblur="toVal();" style="width:20%;float: left;" class="form-control" />
										<span style="float: left;padding-left: 5px;padding-top: 8px;">(<fmt:message key="label.your.name" /> [email@mail.com])</span>
										
									</td>
								</tr>
							</table>
						</div>
						<%-- 普通邮件服务器 --%>
						<div id="normalEmailSetting">
						<table style="width:100%;" cellspacing="1" >					
							<tr style="height: 40px;">
								<td style="width:15%;padding-left: 15px;" ><fmt:message key="title.basicSettings.sendServiceEmail" />：</td>
								<td  >
									<input name="emailServerDTO.personalEmailAddress" id="personalEmailAddress" onblur="toVal();" style="width:20%;" class="form-control" validType="email"  />
								</td>
							</tr>
							<tr style="height: 40px;">
								<td style="width:15%;padding-left: 15px;" ><fmt:message key="title.basicSettings.emailAccount" />：</td>
								<td  >
									<input name="emailServerDTO.userName" id="userName" onblur="toVal();" style="width:20%;" class="form-control"  validType="email"/>
								</td>
							</tr>
							
							<tr style="height: 40px;">
								<td style="width:15%;padding-left: 15px;" ><fmt:message key="i18n.password" />：</td>
								<td  >
									<input type="password" id="company_password" style="width:20%" class="form-control"  />
									<input type="hidden" id="company_password_hidden" >
									<input type="hidden" id="company_password_reality" name="emailServerDTO.password" >
								</td>
							</tr>
							<tr style="height: 40px;">
								<td colspan="2" style="width:15%;padding-left: 15px;" ><span style="font-weight:bold;">[SMTP <fmt:message key="setting.subService"/>]</span></td>
							</tr>
							
							<tr style="height: 40px;">
								<td style="width:15%;padding-left: 15px;" ><fmt:message key="label.basicSettings.serverAddress"/>：</td>
								<td >
									<input name="emailServerDTO.smtpServerAddress" id="smtp_serverAddress" style="width:20%" class="form-control" />
								</td>
							</tr>
							<tr style="height: 40px;">
								<td style="width:15%;padding-left: 15px;" ><fmt:message key="label.basicSettings.serverPort"/>：</td>
								<td  >
									<input name="emailServerDTO.smtpServerPort" id="smtp_serverPort" style="width:50px;float: left;" class="form-control"/>

									<h5 style="float: left;color: black;text-align: center;padding-left: 5px;">(Port:25)</h5>
								</td>
							</tr>
						</table>
						</div>
						<%-- Exchange邮件服务器 --%>
						<div  id="exchangeEmailSetting" style="display: none">
						<table style="width:100%;" cellspacing="1" >
							<tr style="height: 40px;">
								<td style="width:15%;padding-left: 15px;" ><fmt:message key="label.ldap.server" />：</td>
								<td >
									<input name="emailServerDTO.exchangeHostName" id="exchangeHostName" style="width:20%;float: left;" class="form-control" required="true"/>
									<select id="exchangeVersionSelect" name="emailServerDTO.emailVersion" class="form-control" style="width: 15%;float: left;">
										<option value="Exchange2007_SP1">Exchange 2007_SP1</option>
										<option value="Exchange2010">Exchange 2010</option>
										<option value="Exchange2010_SP1">Exchange 2010_SP1</option>
										<option value="Exchange2010_SP2">Exchange 2010_SP2</option>
										
									</select>
									<input id="exchangeVersion" value="" type="hidden">
								</td>
							</tr>
							<tr style="height: 40px;">
								<td style="width:15%;padding-left: 15px;" ><fmt:message key="label_domain" />：</td>
								<td >
									<input name="emailServerDTO.domain" id="exchangeDomain" style="width:20%" class="form-control" required="true"/>
								</td>
							</tr>
							<tr style="height: 40px;">
								<td style="width:15%;padding-left: 15px;" ><fmt:message key="label.user.loginName" />：</td>
								<td >
									<input name="emailServerDTO.exchangeUserName" id="exchangeUserName" style="width:20%" class="form-control" required="true"/>
								</td>
							</tr>
							<tr style="height: 40px;">
								<td style="width:15%;padding-left: 15px;" ><fmt:message key="i18n.password" />：</td>
								<td  >
									<input type="password" id="exchangePassword" style="width:20%" class="form-control"/>
									<input type="hidden" id="exchangePassword_reality" name="emailServerDTO.exchangePassword"  >
									<input type="hidden" id="exchangePassword_hidden" >
								</td>
							</tr>
							
							<tr style="height: 40px;">
								<td style="width:15%;padding-left: 15px;" ><fmt:message key="title.basicSettings.emailAccount" />：</td>
								<td  >
									<input name="emailServerDTO.exchangeEmailAccount" id="exchangeEmailAccount" style="width:20%" class="form-control" validType="email"/>
								</td>
							</tr>
						</table>
						</div>
						
						<div>
							<table style="width:100%;" cellspacing="1" >
								<tr style="height: 60px;">
									<td  style="padding-left: 10%;">
										<input type="hidden" name="emailServerDTO.emailServerId" id="emailServerId"  /> 
										<input type="hidden" name="emailServerDTO.attestation" id="attestation"/>									
								
										<input type="submit" class="btn btn-primary btn-sm"  value="<fmt:message key="common.save"/>"/>&nbsp;&nbsp;&nbsp;&nbsp;
										<input type="button" id="emailConnTestLink" class="btn btn-primary btn-sm"  value="<fmt:message key="label.basicSettings.connectionTest" />"/>	
									
									</td>
									
								</tr>
							</table>
						</div>
					</div>					
					</form>
				</div>
			</div>
			<!-- 设置结束 -->
		</sec:authorize>
		<!-- 接受邮箱设置 -->
		<sec:authorize url="/pages/mailServer!saveOrUpdateEmailServer.action">
		<div class="tab-pane" id="list2">
			<div id="ReceiveMailServerDiv" style="width: 100%;">	
				<form id="ReceiveMailServerForm" class="form-horizontal" event="wstuo.sysMge.mailServer.saveOrUpdateReceiveMailServer"> 
					<input type="hidden" id="ReceivelRadio" value="">
					<!-- 设置服务邮箱开始 -->				
					<div class="hisdiv" style="">
						<div>
							<table style="width:100%;" cellspacing="1" >
								<tr style="height: 40px;">
									<td style="width:15%;padding-left: 15px;"  >
										<span style="font-weight:bold;"><fmt:message key="label.email.server.type" />:</span>
									</td>
									<td  >
										<input type="radio" name="emailServerDTO.emailType" value="Receive" id="ReceivelEmailRadio" checked="checked" onclick="emailReceiveServerChange(this.value)"  />
										<fmt:message key="label.normal.email" />&nbsp;&nbsp;&nbsp;&nbsp;
										<input type="radio" name="emailServerDTO.emailType" value="Receive_exchange" id="ReceiveExchangeEmailRadio" onclick="emailReceiveServerChange(this.value)" />
										<fmt:message key="lable.exchange.email" /><%-- (WebDav) --%>
									</td>
								</tr>
								<tr style="height: 40px;">
									<td style="width:15%;padding-left: 15px;"  >
									<span style="font-weight:bold;"><fmt:message key="label.your.name" />:</span></td>
									<td  >
										<input name="emailServerDTO.personal" id="Receive_Personal"  style="width:20%;float: left;" class="form-control" required="true"/>
										<span style="float: left;padding-left: 5px;padding-top: 8px;">(<fmt:message key="label.your.name" /> [email@mail.com])</span>
										
									</td>
								</tr>
							</table>
						</div>
						<%-- 普通邮件服务器 --%>
						<div id="ReceiveEmailSetting">
							<table style="width:100%;" cellspacing="1" >
								<tr style="height: 40px;">
									<td style="width:15%;padding-left: 15px;"  ><fmt:message key="label.customReport.sendToEmail"  />：</td>
									<td  >
										<input name="emailServerDTO.personalEmailAddress" id="ReceivePersonalEmailAddress"  style="width:20%" class="form-control" validType="email"  />
									</td>
								</tr>
								<tr style="height: 40px;">
									<td style="width:15%;padding-left: 15px;"  ><fmt:message key="title.basicSettings.emailAccount" />：</td>
									<td  >
										<input name="emailServerDTO.userName" id="ReceiveUserName"  style="width:20%;" class="form-control" required="true"/>
									</td>
								</tr>
								
								<tr style="height: 40px;">
									<td style="width:15%;padding-left: 15px;"  ><fmt:message key="i18n.password" />：</td>
									<td  >
										<input type="password" id="company_receive_password" style="width:20%" class="form-control"  />
										<input type="hidden" style="width: 300px;" id="password_hidden" >
										<input type="hidden" style="width: 300px;" id="company_receive_password_reality" name="emailServerDTO.password" >
									</td>
								</tr>
			
							</table>
							<div >
								<table style="width:100%;" cellspacing="1" >
									<tr style="height: 40px;">
										<td colspan="2" style="width:15%;padding-left: 15px;"  >
											
											<span style="font-weight:bold;padding-left: 30px;" >
												<input type="radio" name="emailServerDTO.duankouType" id="Pop3Radio" checked="checked" value="check_POP3" onclick="checkPop3Change(this.value)">
												[POP3 <fmt:message key="setting.subService"/>]</span>
											<span style="font-weight:bold;padding-left: 30px;">
												<input type="radio" name="emailServerDTO.duankouType" id="ImapRadio" value="check_IMAP" onclick="checkPop3Change(this.value)">
												[IMAP <fmt:message key="setting.subService"/>]</span>
										</td>
									</tr>
									<tr style="height: 40px;">
										<td style="width:15%;padding-left: 15px;"  ><fmt:message key="label.basicSettings.serverAddress"/>：</td>
										<td   >
											<input name="emailServerDTO.pop3ServerAddress" id="pop3_ReceiveServerAddress" style="width:20%" class="form-control" required="true"/>
										</td>
									</tr>
									<tr style="height: 40px;">
										<td style="width:15%;padding-left: 15px;"  ><fmt:message key="label.basicSettings.serverPort"/>：</td>
										<td  >
											<input name="emailServerDTO.pop3ServerPort" id="pop3_ReceiveServerPort" style="width:50px;float: left;" class="form-control" />
											<h5 style="float: left;color: black;padding-left: 5px;text-align: center;">(Port:</h5>
											<h5 id='span_port' style="float: left;color: black;text-align: center;">110</h5>
											<h5 style="float: left;color: black;text-align: center;">)</h5>
											
										</td>
									</tr>
								</table>
							</div>
						
						</div>
						<%-- Exchange邮件服务器 --%>
						<div  id="ReceiveExchangeEmailSetting" style="display: none">
							<table style="width:100%;" cellspacing="1" >
								<tr style="height: 40px;">
									<td style="width:15%;padding-left: 15px;"  ><fmt:message key="label.ldap.server" />：</td>
									<td  >
										<input name="emailServerDTO.exchangeHostName" id="ReceiveExchangeHostName" style="width:20%;float: left;" class="form-control" required="true"/>
										<select id="ReceiveExchangeVersionSelect" name="emailServerDTO.emailVersion" class="form-control" style="width: 15%;float: left;">
											<option value="Exchange2007_SP1">Exchange 2007_SP1</option>
											<option value="Exchange2010">Exchange 2010</option>
											<option value="Exchange2010_SP1">Exchange 2010_SP1</option>
											<option value="Exchange2010_SP2">Exchange 2010_SP2</option>									
										</select>
										<input id="ReceiveExchangeVersion" value="" type="hidden">
									</td>
								</tr>
								<tr style="height: 40px;">
									<td style="width:15%;padding-left: 15px;"  ><fmt:message key="label_domain" />：</td>
									<td  >
										<input name="emailServerDTO.domain" id="ReceiveExchangeDomain" style="width:20%" class="form-control" required="true"/>
									</td>
								</tr>
								<tr style="height: 40px;">
									<td style="width:15%;padding-left: 15px;"  ><fmt:message key="label.user.loginName" />：</td>
									<td  >
										<input name="emailServerDTO.exchangeUserName" id="ReceiveExchangeUserName" style="width:20%" class="form-control" required="true"/>
									</td>
								</tr>
								<tr style="height: 40px;">
									<td style="width:15%;padding-left: 15px;"  ><fmt:message key="i18n.password" />：</td>
									<td  >
										<input type="password" id="ReceiveExchangePassword" style="width:20%" class="form-control"/>
										<input type="hidden" id="ReceiveExchangePassword_hidden" />
										<input type="hidden" id="ReceiveExchangePassword_reality" name="emailServerDTO.exchangePassword" >
									</td>
								</tr>
								
								<tr style="height: 40px;">
									<td style="width:15%;padding-left: 15px;"  ><fmt:message key="title.basicSettings.emailAccount" />：</td>
									<td  >
										<input name="emailServerDTO.exchangeEmailAccount" id="ReceiveExchangeEmailAccount" style="width:20%" class="form-control" validType="email"/>
									</td>
								</tr>						
							</table>
						</div>					
						<div>
							<table style="width:100%;" cellspacing="1" >
								
								<tr style="height: 60px;">
									<td  style="padding-left: 10%;">
										<input type="hidden" name="emailServerDTO.emailServerId" id="ReceiveEmailServerId"  /> 
										<input type="hidden" name="emailServerDTO.attestation" id="ReceiveAttestation"/>
										<input type="submit"  class="btn btn-primary btn-sm"  value="<fmt:message key="common.save"/>"/>&nbsp;&nbsp;&nbsp;&nbsp;	
										<input type="button" id="ReceiveEmailConnTestLink" class="btn btn-primary btn-sm"  value="<fmt:message key="label.basicSettings.connectionTest" />"/>	
									</td>
									
								</tr>
							</table>
						</div>
					</div>
					<!-- 设置结束 -->
				</form>
			</div>
		</div>
		</sec:authorize>
		
		<!-- 短信号码设置 -->
		<sec:authorize url="/pages/sms!saveOrUpdateSMSAccount.action">
		<div class="tab-pane" id="list3">
		<div id="SMSAccountDiv" style="padding:3px">
			<form id="SMSAccountForm">
			<!-- 短信账户 -->
				<div class="lineTableBgDiv"  style="width: 100%;">
					<table style="width:100%;" cellspacing="1" >
						<tr style="height: 40px;">
							<td style="width:10%;padding-left: 15px;height:32px"  ><fmt:message key="label.sms.provider"/>：</td>
							<td >
								<script>
									function changeSMSInstance(){
										if($('#smsAccountDTO_smsInstance').val()=='PanZhiSMSHelper'){
											$('#smsAccountDTO_orgId_tr').show();
											$('#smsAccountDTO_orgId,#smsAccountDTO_userName,#smsAccountDTO_pwd').val('');
										}else{
											$('#smsAccountDTO_orgId_tr').hide();
											//清空数据
											$('#smsAccountDTO_orgId,#smsAccountDTO_userName,#smsAccountDTO_pwd').val('');
											wstuo.sms.smsAccount.findSMSAccount();
										}
									}
								</script>
			
								<select id="smsAccountDTO_smsInstance" name="smsAccountDTO.smsInstance" onchange="changeSMSInstance()" class="form-control" style="width: 15%;">
									<option value=""><fmt:message key="common.pleaseSelect"/></option>
									<option value="WanCiSMSHelper"><fmt:message key="label.sms.provider_yidong"/></option>
									<option value="PanZhiSMSHelper"><fmt:message key="label.sms.provider_panzhi"/></option>
									<option value="OtherSMSHelper"><fmt:message key="label.returnVisit.other"/></option>
								</select>					
							</td>
						</tr>
					
						<tr id="smsAccountDTO_orgId_tr" style="display:none;height: 40px;">
							<td style="width:10%;padding-left: 15px;height:32px"  ><fmt:message key="label.sms.orgId"/>：</td>
							<td  >
								<input name="smsAccountDTO.orgId" id="smsAccountDTO_orgId" style="width:20%" class="form-control"/>
							</td>
						</tr>
						
						<tr style="height: 40px;">
							<td style="width:10%;padding-left: 15px;height:32px"  ><fmt:message key="label.basicSettings.smsLoginName"/>：</td>
							<td  >
								<input name="smsAccountDTO.userName" id="smsAccountDTO_userName" style="width:20%" class="form-control"/>
							</td>
						</tr>
					
						<tr style="height: 40px;">
							<td style="width:10%;padding-left: 15px;height:32px"  ><fmt:message key="label.basicSettings.smsLoginPassword"/>：</td>
							<td  >
								<input type="password" id="smsAccountDTO_pwd" style="width:20%" class="form-control"/>
								<input type="hidden" name="smsAccountDTO.pwd" id="smsAccountDTO_pwd_reality" />
							</td>
						</tr>
	
						<tr style="height: 60px;">
							<td  style="padding-left: 10%;" colspan="2">
								<input type="hidden" name="smsAccountDTO.smsLength" id="smsAccountDTO_smsLength" value="70" style="width:100%"/>
								<input type="hidden" name="smsAccountDTO.moblieCount" id="smsAccountDTO_moblieCount" value="99"/>
								<input type="hidden" name="smsAccountDTO.said" id="smsAccountDTO_said">
							
								<input type="button" id="saveSMSAccountBtn" class="btn btn-primary btn-sm"  value="<fmt:message key="common.save"/>"/>&nbsp;&nbsp;&nbsp;&nbsp;	
								<input type="button" id="testSMSAccountBtn" class="btn btn-primary btn-sm"  value="<fmt:message key="label.basicSettings.smsAccountTest" />"/>
							</td>					
						</tr>				
					</table>
				</div>
			
			<!-- 设置结束 -->
			</form>
		</div>
		</div>
	</sec:authorize>


</div>	
</div>	
</div>
</div>


	





