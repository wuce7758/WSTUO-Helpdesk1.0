<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="../../language.jsp" %>

<script src="${pageContext.request.contextPath}/js/wstuo/noticeRule/noticeGrid.js?random=<%=new java.util.Date().getTime()%>"></script>

<%-- <div class="loading" id="noticeGrid_loading"><img src="${pageContext.request.contextPath}/images/icons/loading.gif" /></div>
 --%>

<div class="tab-pane active" >
	<div class="box-inner">
		<div class="box-header well" >
	        <h2><i class="glyphicon glyphicon-list"></i>&nbsp;<fmt:message key="lable.bpm.noticeModule" /></h2>
	        <div class="box-icon"></div>
        </div>
		<div id="noticeGrid_content" style="padding: 3px;">
			<table id="noticeGrid" ></table>
			<div id="noticeGridPager"></div>		
			<div id="noticeGridToolbar"  style="display: none">
				<div class="panelBar">						
					<a class="btn btn-default btn-xs" href="javascript:wstuo.noticeRule.noticeGrid.addNoticeModel()" ><i class="glyphicon glyphicon-plus"></i>&nbsp;<fmt:message key="common.add" /></a>				
					<a class="btn btn-default btn-xs" href="javascript:wstuo.noticeRule.noticeGrid.edit_notices_aff()" ><i class="glyphicon glyphicon-edit"></i>&nbsp;<fmt:message key="common.edit" /></a>
					<a class="btn btn-default btn-xs" href="javascript:wstuo.noticeRule.noticeGrid.delete_notices_aff()" ><i class="glyphicon glyphicon-trash"></i>&nbsp;<fmt:message key="common.delete" /></a>
					<a class="btn btn-default btn-xs" href="javascript:wstuo.noticeRule.noticeGrid.opendoSearch()"><i class="glyphicon glyphicon-search"></i>&nbsp;<fmt:message key="common.search" /></a>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- 搜索框 -->
<div id="noticeSearchWin" class="WSTUO-dialog" title="<fmt:message key="lable.notice.searchNoticeRule" />" style="height: auto;">
	<form id="noticeGrid_fm" >
		<table style="width:100%" class="table" cellspacing="1">
			<tr>
				<td style="width: 20%"><fmt:message key="lable.notice.name" /></td>
				<td><input type="text" id="noticeNameSearch" class="form-control" style="width: 80%"/></td>
			</tr>
			<tr>
				<td class="lineTableTd"><fmt:message key="notice.enableThisRule" /></td><td>
					<select id="useStatusSeach" style="width: 80%" class="form-control">
						<option value="all"><fmt:message key="common.pleaseSelect" /></option>
						<option value="true"><fmt:message key="common.enable" /></option>
						<option value="false"><fmt:message key="common.disable" /></option>
					</select>
				</td>
			</tr>
			<tr>
				<td><fmt:message key="notice.module" /></td>
				<td>
					<select id="moduleSeach" name="noticeRuleDTO.module" class="form-control" style="width: 80%" onchange="javascript:wstuo.noticeRule.noticeGrid.changeNoticeObject(this.value,'Seach')" >
							<option value=""><fmt:message key="common.pleaseSelect" /></option>
							<c:if test="${requestHave eq true}">
							<option value="request"><fmt:message key="itsm.request" /></option>
							</c:if>
							<c:if test="${problemHave eq true}">
							<option value="problem"><fmt:message key="title.mainTab.problem.manage" /></option>
							</c:if>
							<c:if test="${changeHave eq true}">
							<option value="change"><fmt:message key="title.mainTab.change.manage" /></option>
							</c:if>
							<c:if test="${releaseHave eq true}">
							<option value="release"><fmt:message key="title.mainTab.release.manage" /></option>
							</c:if>
							<c:if test="${cimHave eq true}">
							<option value="cim"><fmt:message key="title.mainTab.cim.manage" /></option>
							</c:if>
							<%-- <option value="task"><fmt:message key="lable.task.manage" /></option> --%>
							<option value="other"><fmt:message key="label.returnVisit.other" /></option>
					</select>
				</td>
			</tr>
			<tr id="nrtrSeach" style="display: none;" ><td class="lineTableTd"><fmt:message key="notice.noticeTarget" /></td>
				<td>
					<label class="checkbox-inline">
						<span id="nrRequestSeach" style="display: none;"><input type="checkbox" name="noticeRuleTypeSeach" value="requester"/><fmt:message key="notice.requesterOrReporter" /></span>
					</label>
		            <label class="checkbox-inline">
						<span id="nrTechnicianSeach" style="display: none;"><input type="checkbox" name="noticeRuleTypeSeach" value="technician"/><fmt:message key="notice.assignTechnician" /></span>
					</label>
		            <label class="checkbox-inline">
						<span id="nrOwnerSeach" style="display: none;"><input type="checkbox" name="noticeRuleTypeSeach" value="owner"/><fmt:message key="task.ownerName" /></span>
					</label>
		            <label class="checkbox-inline">
						<span id="nrTechnicalGroupSeach" style="display: none;"><input type="checkbox" name="noticeRuleTypeSeach" value="technicalGroup"/><fmt:message key="notice.assignTechnicalGroup" /></span>
					</label>
		            <label class="checkbox-inline">	
						<span id="nrCiOwnerSeach" style="display: none;"><input type="checkbox" name="noticeRuleTypeSeach" value="ciOwner"/><fmt:message key="ci.owner" /></span>
					</label>
		            <label class="checkbox-inline">	
						<span id="nrCiUseSeach" style="display: none;"><input type="checkbox" name="noticeRuleTypeSeach" value="ciUse"/><fmt:message key="ci.use" /></span>
					</label>
		
				</td>
			</tr>
			<tr>
				<td class="lineTableTd"><fmt:message key="sla.request.NotificationWay" /></td>
				<td>
					<label class="checkbox-inline">
		                    <input type="checkbox" name="mailNotice" value="true" id="mailNoticeSeach"/><fmt:message key="lable.notice.emialNoticeMethod" />
		            </label>
		            <label class="checkbox-inline">
		                   <input type="checkbox" name="smsNotice" value="true" id="smsNoticeSeach"/><fmt:message key="lable.notice.smsNotice" />
		            </label>
		            <label class="checkbox-inline">
		                   <input type="checkbox" name="imNotice" value="true"  id="imNoticeSeach"/><fmt:message key="notice.IMnotice" />
		            </label>
		            <%-- <label class="checkbox-inline">
		                    <input type="checkbox" name="pushNotice" value="true"  id="pushNoticeSeach" /><fmt:message key="notice.pushNotice"/>
		            </label>  --%>    
				</td>
			</tr>	
			<tr>
				<td colspan="2" style="text-align: center;">
					<input  type="button" class="btn btn-primary btn-sm" onclick="javascript:wstuo.noticeRule.noticeGrid.doSearch()" id="attachmentGrid_select" value="<fmt:message key="common.search" />">
				</td>
			</tr>
		</table>
	</form>
</div>
