function loadHTML(source,id){
	
	$.ajax({
		  url: source,
		  cache: false,
		  success: function(html){
		    $(id).html(html);
		  }
		});
	
}

/**
 * 重置表单.
 * @param fromId
 */
//function resetForm(fromId){
//	$(fromId).each(function(){ 
//		this.reset();  
//	}); 
//}

//function resetForm(fromId){
//	
//	$(fromId+ ' :input')
//    .not(':button, :submit, :reset, :hidden')
//    .val('')
//    .removeAttr('checked')
//    .removeAttr('selected');
//}
/**
 *  清除重复表单
 */
function clearRelForm(ids){
	for(var i=0;i<ids.length;i++){
		$('.ui-dialog #'+ids[i]).remove();
	}
	$('.ui-dialog '+ids).remove();
}

/**
 * 清空文本框内容.
 */
function resetInput(){
	  for (var i=0; i<arguments.length; i++) {
		  $(arguments[i]).val('');
		  //$(arguments[i]).html('');
	  }
}

/**
 * 验证文件后缀
 * @param obj
 * @param filter
 */
function checkFileType(obj,filter){
    var file = obj.value.match(/[^\/\\]+$/gi)[0];
    var rx = new RegExp('\\.(' + (filter?filter:'') + ')$','gi');
    if(filter&&file&&!file.match(rx)){
    	msgAlert(i18n['msg_dc_wrongFile'],'info');
    	obj.value='';
    	obj.outerHTML+='';
    }
}

/**
 * 验证文件后缀(多种格式验证)
 * @param obj
 * @param filter(多个格式过滤以逗号隔开)
 * @param formId 要清空的表单
 */
function checkFileMultiType(obj,filter,formId){
	var _filter=filter.split(',');
	var _result=false;
	for(var i=0;i<_filter.length;i++){
		if(obj.value.toLowerCase().lastIndexOf(_filter[i])>-1){
			_result=true;
		}
	}
	if(!_result){
		msgAlert(i18n['msg_dc_wrongFile'],'info');
		obj.value='';
		resetForm('#'+formId);
		return;
	}
}


/**
 * 判断空值并返回.
 * @param value
 * @returns
 */
function $vl(vl) {
	if(vl==null || vl=='' || vl=='null' || vl=='undefined'){
		return '';
	}else{
		return vl;
	}
}

/**
 * 判断空值并返回.(如果值是0的也直接返回空字符)
 * @param value
 * @returns
 */
function $vl0(vl) {
	if(vl==null || vl=='' || vl=='null' || vl=='0' || vl=='undefined'){
		return '';
	}else{
		return vl;
	}
}

/**
 * 获取浏览器大小
 */

function getWindowWidth() {// 函数：获取尺寸

	var winWidth = 0;
	
	
	// 获取窗口宽度
	
	if (window.innerWidth){
		winWidth = window.innerWidth;
	}
	
	else if ((document.body) && (document.body.clientWidth)){
		winWidth = document.body.clientWidth;
	}
	
	if (document.documentElement && document.documentElement.clientHeight && document.documentElement.clientWidth){
		winWidth = document.documentElement.clientWidth;
	}
	
	return winWidth;
}

/**
 * 根据用户名字符串判断用户是否存在.
 * @param userNames
 * @param method
 */
function validateUsers(userNames,method){
	
	
	var userNamesArrs=userNames.split(";");
	//userNames=userNames.replace(/;/g,'').replace(/,/g,'');
	
	var url='user!vaildateUserByIds.action';
	var _param = $.param({'userNames':userNamesArrs},true);

	$.post(url,_param, function(res){
		if(res==true || res=="true"){					
			method(userNamesArrs);
		}else{
			msgAlert(i18n['msg_msg_userNotExist'],'info');
		}
	});

}

/**
 * 根据用户名字符串判断用户是否存在.
 * @param userNames
 * @param method
 */
//function validateUsersByFullName(userNames,method){
	
	
//	var userNamesArrs=userNames.split(";");
	//userNames=userNames.replace(/;/g,'').replace(/,/g,'');
	
//	var url='user!vaildateUserByFullName.action';
//	var _param = $.param({'userNames':userNamesArrs},true);
//
//	$.post(url,_param, function(res){
//		if(res==true || res=="true"){					
//			method(userNamesArrs);
//		}else{
//			msgAlert(i18n['msg_msg_userNotExist'],'info');
//		}
//	});

//}






/**
 * 根据用户名字符串判断用户是否存在.
 * @param userNames
 * @param method
 */
function validateUserByFullName(userName,method){
	
	if(userName!=null && userName!=''){
		var url='user!vaildateUserByFullName.action';
		var _param = $.param({'userNames':userName},true);
		$.post(url,_param, function(res){
			if(res==true || res=="true"){					
				method();
			}else{
				msgAlert(i18n['msg_msg_userNotExist'],'info');
			}
		});
		
	}else{
		method();
	}
	

}
/**
 * 重置表单.(补充清空隐藏域的代码)
 * @param from
 */
function resetFormAll(from){
	clearFormData(from);
	$(from).find("input[type='hidden']").val("");	
}
/**
 * 重置表单.
 * @param from
 */
function clearFormData(from){
	$(from).each(function(){ 
		this.reset();
	});
}
/**
 * 显示帮助视频.
 */
function showHelpFilm(addr){
	  /**
	   * 
	   var fileFolder='http://www.WSTUO.com/attachments/demo/';//网络
	   * 
	   */
	 
	var fileFolder='';
	
	   show(i18n['title_helpvideo'],
			'<embed src="../attachments/demo/flvplayer.swf?file='+fileFolder+addr+'" allowFullScreen="true" quality="high" width="500" height="400" align="middle" allowScriptAccess="always" type="application/x-shockwave-flash" style="margin:0px"></embed>',
			'show',
			500000
		);
}
var jqueryDialog = {
	width:'auto',
	height:'auto',
	autoOpen: true,
	modal: true	
};

var loading = {
		width:'auto',
		height:'auto',
		autoOpen: true,
		modal: true,
		resizable:false
	};

function beforeClose(id){
	$('#'+id+' input,textarea').blur();
};
function windows(id,option){
	var _p = {};
	option = $.extend({},option,{beforeClose:function(){beforeClose(id);}});
	_p = $.extend({},jqueryDialog,option);
	$('#'+id).dialog($.extend({},jqueryDialog,_p));
};
function windowsAuto(id,option,insideGridView,offset){
	var _offset = offset || 0;_offset = parseInt(_offset);
	var _p = {};
	option = $.extend({},option,{
		beforeClose:function(){beforeClose(id);}
		,resizeStop:function(){$(insideGridView).setGridWidth($('#'+id).width() - _offset);}
	});
	_p = $.extend({},jqueryDialog,option);
	$('#'+id).dialog($.extend({},jqueryDialog,_p));
	$(insideGridView).setGridWidth($('#'+id).width() - _offset);
};
function windows_beforeClose(id,option,event){
	var _p = {};
	option = $.extend({},option,{beforeClose:function(){beforeClose(id);event();}});
	_p = $.extend({},jqueryDialog,option);
	$('#'+id).dialog($.extend({},jqueryDialog,_p));
};
function DateBox(ids){
	for(var i=0;i<ids.length;i++){
		$('#'+ids[i]).datepicker({changeMonth:true,changeYear:true,yearRange:"-60:+20"});
	}
};
/**
 * 
 * @param elements ["#id1","#id2"]
 * @param format yyyy-MM-dd HH:mm:ss -> 2008-03-12 19:20:00，MMMM d, yyyy  ->  三月 12, 2008 
 * @returns
 */
function DatePicker97(elements,format){
	if (format == null || format === '') {format = 'yyyy-MM-dd';}
	for(var i=0;i<elements.length;i++){
		$(elements[i]).click(function(){WdatePicker({dateFmt:format});});
	}
};
/**
 * 
 * @param elements "#id1,#id2"
 * @param format yyyy-MM-dd HH:mm:ss -> 2008-03-12 19:20:00，MMMM d, yyyy  ->  三月 12, 2008 
 * @returns
 */
function DateBox97(elements,format){
	if (format == null || format === '') {format = 'yyyy-MM-dd HH:mm:ss';}
	$(elements).click(function(){WdatePicker({dateFmt:format});});
};
function commonWdatePicker(option,element){
	$(element).click(function(){WdatePicker(option);});
};
function jsStringToDate(dateStr){//yyyy-MM-dd
	dateStr = dateStr.replace(/-/g,"/");
	return new Date( dateStr );
};
jQuery.fn.extend({
//	remove: function( selector, keepData ) {
//		for ( var i = 0, elem; (elem = this[i]) != null; i++ ) {
//			if ( !selector || jQuery.filter( selector, [ elem ] ).length ) {
//				if ( !keepData && elem.nodeType === 1 ) {
//					jQuery.cleanData( elem.getElementsByTagName("*") );
//					jQuery.cleanData( [ elem ] );
//				}
//
//                jQuery.event.remove(elem);
//                jQuery.removeData(elem);
//			}
//		}
//		if(jQuery.browser.msie) {
//			  if (this.parentNode) {
//			     this.parentNode.innerHTML="";
//			  }
//			}else if ( elem.parentNode ) {
//				elem.parentNode.removeChild( elem );
//			}
//		return this;
//	}

});
$.fn.serializeObject = function()
{
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};
function calculateFunction(func) {
    if (typeof func === 'string') {
        // support obj.func1.func2
        var fs = func.split('.');
        if (fs.length > 1) {
            func = window;
            $.each(fs, function (i, f) {
                func = func[f];
            });
        } else {
            func = window[func];
        }
    }
    if (typeof func === 'function') {
        return func;
    }
};
/**
 * 格式化input
 * @author will
 */ 
function farmatDouble(event){
	//先把非数字的都替换掉，除了数字和.
	$(event).val($(event).val().replace(/[^\d.]/g,""));
    //必须保证第一个为数字而不是.
	$(event).val($(event).val().replace(/^\./g,""));
    //保证只有出现一个.而没有多个.
	$(event).val($(event).val().replace(/\.{2,}/g,"."));
    //保证.只出现一次，而不能出现两次以上
	$(event).val($(event).val().replace(".","$#$").replace(/\./g,"").replace("$#$","."));
}
function farmatInteger(event){
	$(event).val($(event).val().replace(/\D/g,''));
}
