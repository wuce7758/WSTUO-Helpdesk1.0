$package('windowResize');

windowResize = function(){
    return {
        /**
         * @description 强制改变itsmMainTab尺寸
         */
        resizeItsmMainTab : function() {
            var itsmMainTabWidth = browserProp.browserProp.getWinWidth() - ($('#index_body').layout('panel', 'west').panel('options').width + 12 );
            $('#itsmMainTab').css('min-width', itsmMainTabWidth + 5);
            $('#itsmMainTab').width(itsmMainTabWidth + 5);
            var tabsPanels="#itsmMainTab > .tabs-panels";
            $('#itsmMainTab .tabs-header:first, #itsmMainTab .tabs-wrap:first,'+tabsPanels+',#helpdeskPortalTabid')
    			.css({'width':itsmMainTabWidth});
            $('#myDashboardTab').parent().css({'width':itsmMainTabWidth-20});
            if(browserProp.browserProp.getWinWidth()<1024){
            	$('#myDashboardTab').css({'width':1024 - ($('#index_body').layout('panel', 'west').panel('options').width + 30)});
			}else{
				$('#myDashboardTab').css({'width':itsmMainTabWidth});
			}
            var height=browserProp.browserProp.getWinHeight();
            $(tabsPanels).height(height-100);
            if(page=="main"){
            	$("#addRequest_center").height(height-110);
            	
            	$("#addRequest_tab").width(itsmMainTabWidth);
            	$("#addRequest_tab .tabs-header,#addRequest_tab .tabs-panels").width(itsmMainTabWidth);
            	$("#addRequest_tab> .tabs-panels > .panel,#addRequest_tab > .tabs-panels > .panel > .panel-body").width(itsmMainTabWidth);
            	$("#addRequest_tab > .tabs-header > .tabs-wrap").width(itsmMainTabWidth);
            	
            	$("#requestEditsTab").width(itsmMainTabWidth);
            	$("#requestEditsTab .tabs-header,#requestEditsTab .tabs-panels").width(itsmMainTabWidth);
            	$("#requestEditsTab> .tabs-panels > .panel,#requestEditsTab > .tabs-panels > .panel > .panel-body").width(itsmMainTabWidth);
            	$("#requestEditsTab > .tabs-header > .tabs-wrap").width(itsmMainTabWidth);
            	
            	$("#requestDetailsTab").width(itsmMainTabWidth);
            	$("#requestDetailsTab .tabs-header,#requestDetailsTab .tabs-panels").width(itsmMainTabWidth);
            	$("#requestDetailsTab> .tabs-panels > .panel,#requestDetailsTab > .tabs-panels > .panel > .panel-body").width(itsmMainTabWidth);
            	$("#requestDetailsTab > .tabs-header > .tabs-wrap").width(itsmMainTabWidth);
            	
	            $(tabsPanels+' > .panel,'+tabsPanels+' > .panel > .panel-body').height(height-100).width(itsmMainTabWidth);
	            $(tabsPanels+' > .panel:eq(0),'+tabsPanels+' > .panel > .panel-body:eq(0)').height("auto");
            	$("#requestMain_content,#problemMain_content,#changeManage_panel,#configureItemMain_layout,#releaseGrid_content,#knowledgeMain_layout,#showKnowledge").parent().parent().height("auto");
            	$("#requestMain_content,#problemMain_content,#changeManage_panel,#configureItemMain_layout,#releaseGrid_content,#knowledgeMain_layout,#showKnowledge").parent().height("auto");
            	
            }else{
            	$("#addScheduledTask_tab").width(itsmMainTabWidth);
            	$("#addScheduledTask_tab .tabs-header,#addScheduledTask_tab .tabs-panels").width(itsmMainTabWidth);
            	$("#addScheduledTask_tab> .tabs-panels > .panel,#addScheduledTask_tab > .tabs-panels > .panel > .panel-body").width(itsmMainTabWidth);
            	$("#addScheduledTask_tab > .tabs-header > .tabs-wrap").width(itsmMainTabWidth);
            	
            	$("#editScheduledTask_tab").width(itsmMainTabWidth);
            	$("#editScheduledTask_tab .tabs-header,#editScheduledTask_tab .tabs-panels").width(itsmMainTabWidth);
            	$("#editScheduledTask_tab> .tabs-panels > .panel,#editScheduledTask_tab > .tabs-panels > .panel > .panel-body").width(itsmMainTabWidth);
            	$("#editScheduledTask_tab > .tabs-header > .tabs-wrap").width(itsmMainTabWidth);
            	
            	$(tabsPanels).css("overflow-y","hidden");
            	$(tabsPanels).width(itsmMainTabWidth);
            	$(tabsPanels+' > .panel,'+tabsPanels+' > .panel > .panel-body').height(height-100);
            }
        }
    };
}();