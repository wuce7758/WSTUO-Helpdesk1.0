$package('common.config.category')

common.config.category.serviceOrgUtils=function(){
	return{
		
		/**
		 * @description 集所属客户/技术组点击(规则集)
		 * @param putID 选中的机构ID赋值ID
		 * @param putName 选中的机构名称赋值ID
		 * */
		selectORG:function(putID,putName,title){
			//windows('selectOrgDirDiv',{width:,height:});
			$('#selectOrgDirDiv').dialog({   
		         title: title,   
		         width: 250,   
		         height: 400,   
		         modal: true  
		     });   
			common.config.category.serviceOrgUtils.selectOrgDir();
			$('#selectOrgDir_getOrgdNodes').unbind().click(function(){
				common.config.category.serviceOrgUtils.getSelectedOrgDirNodes(putID,putName);
			});
		},
		/**
		 * @description 所属客户显示机构信息(规则集)
		 * */
		selectOrgDir:function(){
			$('#selectOrgDirTreeDiv').jstree({
				"json_data":{
				    ajax: {
				    	url : "organization!findAll.action?companyNo=-1",
				    	data:function(n){
					    	  return {'parentOrgNo':n.attr ? n.attr("orgNo").replace("node_",""):0};//types is in action
						},
				    	cache:false}
				},
				"plugins" : [ "themes", "json_data", "checkbox" ] 
				});
		},
		/**
		 * @description 所属客户显示机构信息，获取windwos面板上选择的值(规则集)
		 * @param putID 选中机构ID赋值ID
		 * @param putName 选中的机构名称赋值ID
		 * */
		getSelectedOrgDirNodes:function(putID,putName){
			 var namePut1="";
			 var classiftyid="";
			 var categoryIds=new Array();
			 var flag=new Array();
			$("#selectOrgDirTreeDiv").jstree("get_checked",false,true).each(function (i,n) { 
				 var node = jQuery(this); 
				 var ids=node.attr('orgno')
				 if(ids==null){//校正orgNo
					 ids=1;
				 }
				 categoryIds[i]=ids;
				 flag[i]=node.attr('orgtype').replace("Panel","");
			});
			if(categoryIds.length>0){
				var param = $.param({'orgNos':categoryIds,'orgTypes':flag},true);
				var url="organization!findOrganizationsByTypeArray.action";
				$.post(url,param,function(res){	
					if(res!=null){
						  for(var i=0;i<res.length;i++){
							  if(i==0 ){
								  namePut1=res[i].orgName+",";
								  classiftyid=res[i].orgNo+",";
							 }else{
								 if(res[i-1].orgNo!=res[i].orgNo){
									  
									  namePut1+=res[i].orgName+",";
									  classiftyid+=res[i].orgNo+",";
							  	  }
							  }
						  }
				 	 }
					 if(classiftyid.length>1){
						  namePut1=namePut1.substring(0,namePut1.length-1);
						  classiftyid=classiftyid.substring(0,classiftyid.length-1);
						  }
					 $(putName).val(classiftyid);
					 $(putID).val(namePut1);
				});
			}
			
			$('#selectOrgDirDiv').dialog('close');
		}
	}
}();