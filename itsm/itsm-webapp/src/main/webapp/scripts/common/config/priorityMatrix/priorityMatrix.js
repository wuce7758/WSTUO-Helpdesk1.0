$package('common.config.priorityMatrix');
/**  
* @author QXY  
* @constructor tan
* @description 优先级矩阵函数
* @date 2012-01-17
* @since version 1.0 
*/
common.config.priorityMatrix.priorityMatrix=function(){
	return{
		/**
		 * 显示优先级矩阵
		 */
		showPriorityMatrix:function(){
			var urgency;//紧急度
			var affect;//影响
			var priotiry;
			$.post('priorityMatrix!findPriorityMatrixDataShow.action',function(data){
				urgency=data.urgency;
				affect=data.affect;
				priority=data.priority;
				var str="<tr><td align='center' style='width:20%;'>"+i18n['label_sla_effectRange']+"/"+i18n['label_sla_seriousness']+"</td>"
				$.each(urgency,function(i,item){
					str=str+"<td align='center' style='background-color: #DCE7FB;'>"+item.dname+"</td>"
				});
				str=str+"</tr>";
				$('#priorityMatrix_diw table').append(str);
				
				var _rows="";
				$.each(affect,function(i,a){
					_rows="<tr><td align='center' style='background-color: #DCE7FB;'>"+a.dname+"</td>";
					$.each(urgency,function(j,u){
						var divId="affect"+a.dcode+"urgency"+u.dcode;
						_rows_str="<td align='center' style='width:100px'><div id='{divId}'><a href=javascript:common.config.priorityMatrix.priorityMatrix.setMatrix('{priorityId}','{divId_p}','{affectId}','{urgencyId}')>[{priorityName}]</a></div></td>"
						var isexits=false;
						var priorityId='';
						var priorityName='--'+i18n["common_pleaseChoose"]+'--';
						if(priority!=null){
							$.each(priority,function(j,k){
								if(a.dcode==k.affectId && u.dcode==k.urgencyId){
									priorityId=k.priorityId;
									if(k.priorityName!=null && k.priorityName!='null'&& k.priorityName!='')
									priorityName=k.priorityName;
								}
							});
						}
						_rows=_rows+_rows_str.replace('{divId}',divId)
						.replace('{priorityId}',priorityId)
						.replace('{divId_p}',divId)
						.replace('{affectId}',a.dcode)
						.replace('{urgencyId}',u.dcode)
						.replace('{priorityName}',priorityName);
					});
					_rows=_rows+"</tr>";
					$('#priorityMatrix_diw table').append(_rows);
				});
			});
			
		},
		/**
		 * 保存矩阵
		 * @param value 优先级Id
		 * @param text 页面显示文本
		 * @param id 矩阵Html元素
		 * @param affectId 影响范围Id
		 * @param urgencyId 紧急度Id
		 * @param type 类型
		 */
		saveMatrix:function(value,text,id,affectId,urgencyId,type){
			$.post('priorityMatrix!savePriorityMatrix.action','priorityMatrixDTO.urgencyId='+urgencyId+'&priorityMatrixDTO.affectId='+affectId+'&priorityMatrixDTO.priorityId='+value,function(){
				$('#'+id).html("<a href=javascript:common.config.priorityMatrix.priorityMatrix.setMatrix('"+value+"','"+id+"','"+affectId+"','"+urgencyId+"')>["+text+"]</a>");
				msgShow(i18n['common_operation_success'],'show');
			});
		},
		onblurChangeMatrix:function(value,text,id,affectId,urgencyId){
			$('#'+id).html("<a href=javascript:common.config.priorityMatrix.priorityMatrix.setMatrix('"+value+"','"+id+"','"+affectId+"','"+urgencyId+"')>["+text+"]</a>");
		},
		
		/**
		 * 设置矩阵
		 * @param value 优先级Id
		 * @param id 矩阵Html元素
		 * @param affectId 影响范围Id
		 * @param urgencyId 紧急度Id
		 */
		setMatrix:function(value,id,affectId,urgencyId){
			var _select='<select id="select_priority_'+id+'" onchange="common.config.priorityMatrix.priorityMatrix.saveMatrix(this.value,this.options[this.selectedIndex].text,\''+id+'\',\''+affectId+'\','+urgencyId+')" onblur="common.config.priorityMatrix.priorityMatrix.onblurChangeMatrix(this.value,this.options[this.selectedIndex].text,\''+id+'\',\''+affectId+'\','+urgencyId+')">'
				+'<option value="">--'+i18n["common_pleaseChoose"]+'--</option>';
			var url = 'dataDictionaryItems!findByCode.action?dataDictionaryQueryDto.groupCode=priority';
			$.post(url,function(res){
				if(res!=null && res.length>0){
					for(var i=0;i<res.length;i++){
						_select=_select+'<option value="'+res[i].dcode+'">'+res[i].dname+'</option>';
						
						if(i==res.length-1){//循环完成后再赋值
							$('#'+id).html('');
							_select=_select+'</select>'
							$(_select).appendTo('#'+id);
							$('#select_priority_'+id).val(value);
							$("#select_priority_"+id).focus();
						}
					}
				}
				
			});
		},
		/**
		 * 查询优先级矩阵状态
		 */
		findPriorityMatrix:function(){
			$.post('priorityMatrix!findPriorityMatrixStatus.action',function(data){
				if(data!=null){
					if(data.priorityMatrixStatus=='true')
						$('#priorityMatrixStatus_status').attr('checked',true);
				}	
			});
			
		},
		/**
		 * 保存优先级矩阵状态
		 */
		savePriorityMatrix:function(){
			
			var frm = $('#priorityMatrixStatusForm').serialize();
			$.post('priorityMatrix!savePriorityMatrixStatus.action',frm,function(){
				msgShow(i18n['common_operation_success'],'show');
			});
			
		},
		/**
		 * 初始化
		 * @private
		 */
		init:function(){
			$("#priorityMatrix_loading").hide();
			$("#priorityMatrix_panel").show();
			common.config.priorityMatrix.priorityMatrix.showPriorityMatrix();
			common.config.priorityMatrix.priorityMatrix.findPriorityMatrix();
			$('#priorityMatrixStatus_status').click(common.config.priorityMatrix.priorityMatrix.savePriorityMatrix);
		}
	}
}();
$(document).ready(common.config.priorityMatrix.priorityMatrix.init);