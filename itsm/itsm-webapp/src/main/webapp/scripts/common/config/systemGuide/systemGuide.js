$package("common.config.systemGuide"); 
$import('basics.tab.tabUtils');
 /**  
 * @author phil
 * @constructor 
 * @description 用户向导
 * @date 2013-07-25
 * @since version 1.0 
 */
common.config.systemGuide.systemGuide = function() {
	return {
		/**
		  * @description 如果tabs存在则转到对应的TAB，如果不存在则添加tabs
		  * @param title 标题
		  * @param url 对应的链接
		  * @param guideSetId 对应的模块
		  * */
		addTabGuide:function(title, url,guideSetId){
			///var _title=
			basics.tab.tabUtils.closeTab(i18n.caption_ruleGrid);
			basics.tab.tabUtils.closeTab(i18n.default_security_changeRule);
			basics.tab.tabUtils.closeTab(i18n.default_security_mailToRequestRule);
			basics.tab.tabUtils.closeTab(i18n.request_process_rule);
			
			guideShareId = guideSetId;
			guideShare=$('#'+guideSetId).val();
			if(guideSetId=='slaRuleGuide' || guideSetId=='changGuide' || guideSetId=='requestRuleGuide'){
				showTab('006');
			}
			if(guideSetId=='categoryGuide' || guideSetId=='dataGuide'){
				showTab('003');
			}
			if(guideSetId=='userGuide'){
				showTab('002');
			}
			if(guideSetId=='slaGuide'){
				showTab('005');
			}
			if(guideSetId=='requestProcess'){
				showTab('007');
			}
			if(guideSetId=='companyGuide' || guideSetId=='emilGuide' || guideSetId=='orgGuide'){
				showTab('001');
			}
			
			basics.tab.tabUtils.addTab(title, url);
		},
		
		/**
		 * 判断是否设置过，配置过显示打钩
		 */
		findGuide:function(){
			var url = 'systemGuide!findGuide.action';
			$.post(url,function(res){
				var ico = "<img src='../images/icons/gou2.png' style='width: 20px;height: 18px;'/>";
				for(var i=0;i<res.length;i++){
					if(res[i].guideBoolean){
						//guideShare = res[i].guideBoolean;
						$('#img_'+res[i].guideName).html(ico+"<input type='hidden' id='"+res[i].guideName+"' value='"+res[i].guideBoolean+"'>");
					}
					
				}
			});
		},
		/**
		 * 保存系统设置向导
		 * @private gname 模块名称
		 */
		saveSystemGuide:function(gname){
			if(guideShare==undefined || guideShare!=1){
				var url = 'systemGuide!guideSaveUpdate.action';
				 $.post(url, {'systemGuide.guideBoolean':true,'systemGuide.guideName':gname},function(res){
					 if(guideShareId==gname&&guideShareId!=""){
							guideShareId="";
							guideShare="";
							basics.tab.tabUtils.selectTab(i18n.label_systemGuide_title,function(){
								common.config.systemGuide.systemGuide.findGuide();
							});
						}
				 });
			}
		},
		/**
		 * 打开页面时是否系统设置向导
		 */
		checkboxGuide:function(){
			var url = 'systemGuide!findGuideBoo.action';
			$.post(url, function(res){
				if(res){
					$("#checkboxGuide").attr("checked", true);
				}else{
					$("#checkboxGuide").attr("checked", false);
				}
			 });
			 $("#checkboxGuide").click(function() {
	                if ($("#checkboxGuide").attr("checked")) {
	                	var url = 'systemGuide!guideSaveUpdate.action';
	   				 	$.post(url, {'systemGuide.guideBoolean':true,'systemGuide.guideName':'checkboxGuide'},function(res){
	   				 		msgShow(i18n['saveSuccess'],'show');
	   				 	});
	                }else{
	                	var url = 'systemGuide!guideSaveUpdate.action';
	   				 	$.post(url, {'systemGuide.guideBoolean':false,'systemGuide.guideName':'checkboxGuide'},function(res){
	   				 		msgShow(i18n['saveSuccess'],'show');
	   				 	});
	                }
	         });
		},
		/**
		 * 初始化
		 * @private
		 */
		init:function(){
			common.config.systemGuide.systemGuide.findGuide();
			common.config.systemGuide.systemGuide.checkboxGuide();
		}
	}
}();
$(document).ready(common.config.systemGuide.systemGuide.init);