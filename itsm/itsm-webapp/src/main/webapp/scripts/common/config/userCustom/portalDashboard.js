$package("common.config.userCustom");
/**  
 * @author QXY  
 * @constructor portalDashboard
 * @description "portalDashboard"
 * @date 2011-10-09
 * @since version 1.0 
 */ 
common.config.userCustom.portalDashboard=function(){
	var result='';
	var viewShowStr='',viewNoShowStr='';
	var $gallery = $("#gallery"), $trash = $("#trash");
	var recycle_icon = '<a href="#" class="action_delete" style="background-image: url(\'../images/comm/action_delete.png\');background-repeat: no-repeat;display: inline-block;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>';
	var trash_icon = '<br/><a href="#" class="action_check" style="background-image: url(\'../images/comm/action_check.png\');background-repeat: no-repeat;display: inline-block;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>';
	var trash_icons = '<a href="#" class="action_check" style="background-image: url(\'../images/comm/action_check.png\');background-repeat: no-repeat;display: inline-block;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>';
	var  laytou={'fourRow':4,'threeRow':3,'twoRow':2,'oneRow':1};
	var countView="";
	return {
		/**
		 * 计算面板宽度
		 */
		countRowsNum:function(){
			var oneText=$("#rowText1").val();
			var twoText=$("#rowText2").val();
			var threeText=$("#rowText3").val();
			var fourText=$("#rowText4").val();
			var num= 100-(oneText*1+twoText*1+threeText*1+fourText*1);
			if(num<0)
			$("#rowsNum").html("<font color='red'>"+i18n['more_than']+num+"</font>");
			else
			$("#rowsNum").html("<font color='red'>"+i18n['The_remaining']+num+"</font>");
		},
		/**
		 * 保存面板宽度
		 */
		saveRows:function(){
			//保存的时候先判断一下用户填写的宽度
			var rowsSize= $("#selectItem").val();
			var oneText=$("#rowText1").val();
			var twoText=$("#rowText2").val();
			var threeText=$("#rowText3").val();
			var fourText=$("#rowText4").val();
			switch (rowsSize) {
			case "oneRow":
				if((oneText*1) > 79 && (oneText*1) < 101){
					common.config.userCustom.portalDashboard.saveCheck();
				}else{
					msgAlert(i18n.ERROR_VIEW_CANNOT_MORE,'info');
				}
				break;
			case "twoRow":
				if((oneText*1) > 19 && (twoText*1) > 19 &&(oneText*1+twoText*1) > 79 && (oneText*1+twoText*1) < 101){
					common.config.userCustom.portalDashboard.saveCheck();
				}else{
					msgAlert(i18n.ERROR_VIEW_CANNOT_MORE,'info');
				}
				break;
			case "threeRow":
				if((oneText*1) > 19 && (twoText*1) > 19 && (threeText*1) > 19 && (oneText*1+twoText*1+threeText*1) >79 && (oneText*1+twoText*1+threeText*1) <101){
					common.config.userCustom.portalDashboard.saveCheck();
				}else{
					msgAlert(i18n.ERROR_VIEW_CANNOT_MORE,'info');	
				}
				break;
			case "fourRow":
				if((oneText*1) > 19 && (twoText*1) > 19 && (threeText*1) > 19 && (fourText*1) > 19 && (oneText*1+twoText*1+threeText*1+fourText*1) > 79 && (oneText*1+twoText*1+threeText*1+fourText*1) < 101){
					common.config.userCustom.portalDashboard.saveCheck();
				}else{
					msgAlert(i18n.ERROR_VIEW_CANNOT_MORE,'info');
				}
			}
		},
		/**
		 * 检查面板是否存在
		 */
		saveCheck:function(){
			var _parm= $("#portalLayoutForm").serialize();
			$.post("rowsWidth!updateRowsWidth.action",_parm+"&&rowsWidth.loginName="+userName,function(){
				common.config.userCustom.portalDashboard.savaView();
				basics.tab.tabUtils.refreshWindow();
			});
		},
		/**
		 * 显示面板
		 * @param value 面板类型
		 */
		selectViewType:function(value){
			var _value = value.split('_');
			viewNoShowStr = '';
			switch(_value[0]){
			case 'static':
				common.config.userCustom.portalDashboard.loadStaticView();
				break;
			case 'filter':
				common.config.userCustom.portalDashboard.loadFilterView(_value[1]);
				break;
			case 'report':
				common.config.userCustom.portalDashboard.loadReportView(_value[1]);
				break;
			case 'ci':
				common.config.userCustom.portalDashboard.loadCiView();
			}
			setTimeout(function(){
				$('#gallery').html(viewNoShowStr);
				$.parser.parse($('#gallery'));
			},500);
		},
		/**
		 * 显示配置项面板
		 */
		loadCiView:function(){
			viewNoShowStr+='<div class="ui-widget-content ui-helper-clearfix" style="OVERFLOW-Y: hidden; OVERFLOW-X:auto;float:left;width:48%;margin:1px 1px">'+
			'<div class="portlet-header ui-widget-header" style="text-align: center;cursor: move;">'+i18n['lable_findCiByno']+'</div>'+
			'<div class="portlet-content"><table width="100%" class="lineTable" cellspacing="1"></table>'+
			'<input class="inputs" type="hidden" value="CIID"/>'+
			'<input class="viewCodeInput" type="hidden" value="ci_CIID"/>'+
			'<div style="text-align:center;"><span id="staticView" style="text-align: center;">'+i18n['lable_findCiByno']+'</span></div>'+
			'<br/><br/><div style="text-align:right;"><a href="#"  class="action_check" style="background-image: url(\'../images/comm/action_check.png\');background-repeat: no-repeat;display: inline-block;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a></div>'+
			'</div></div>';
			common.config.userCustom.portalDashboard.bindOkLink();
			$('#gallery').append(viewNoShowStr);
			
		},
		/**
		 * @description 删除配置项面板
		 */
		deleteCIView:function(){
			$("#deleteCIViewDiv").remove();
			common.config.userCustom.portalDashboard.savaView();
		},
		/**
		 * @description 设置面板显示
		 * @param value 面板显示类型
		 */
		selectitems:function(value){
			layoutType = value;
			common.config.userCustom.portalDashboard.onChangeSetValue(layoutType);
			common.config.userCustom.portalDashboard.showHideText(layoutType);
		},
		/**
		 * 设置每个面板显示的长宽
		 * @param  rowData 行数据
		 */
		onChangeSetValue:function(rows){
			switch(rows){
			case "oneRow":
				$("#rowText1").val(100);
				$("#rowText2,#rowText3,#rowText4").val(0);
				break;
			case "twoRow":
				$("#rowText1,#rowText2").val(49);
				$("#rowText3,#rowText4").val(0);
				break;
			case "threeRow":
				$("#rowText1,#rowText2,#rowText3").val(33);
				$('#rowText4').val(0);
				break;
			case "fourRow":
				$("#rowText1,#rowText2,#rowText3,#rowText4").val(24);
				break;
			}
		},
		/**
		 * 设置面板每行显示个数
		 * @param layValue 面板显示类型
		 */
		showHideText:function(layValue){
			switch (layValue) {
			case "oneRow":
				layoutType=1;
				$("#row1").show();
				$("#row2,#row3,#row4").hide();
				break;
			case "twoRow":
				layoutType=2;
				$("#row1,#row2").show();
				$("#row3,#row4").hide();
				break;
			case "threeRow":
				$("#row1,#row2,#row3").show();
				$("#row4").hide();
				layoutType=3;
				break;
			case "fourRow":
				layoutType=4;
				$("#row1,#row2,#row3,#row4").show();
				break;
			}
		},
		/**
		 * 加载面板设置的属性
		 */
		loadSetValue:function(data){
			$('#selectItem').val(data.layoutType);
			$('#rowText1').val(data.rowsWidth.oneRows);
			$('#rowText2').val(data.rowsWidth.twoRows);
			$('#rowText3').val(data.rowsWidth.threeRows);
			$('#rowText4').val(data.rowsWidth.fourRows);
		},
		/**
		 * 移除显示的面板
		 * @param viewId 面板Id
		 */
		deleteViewAndSaveStr:function(viewId){
			$("#protlet_Id"+viewId).remove();
			common.config.userCustom.portalDashboard.savaView();
		},
		/**
		 * 加载面板
		 */
		loadData:function(){
			var _parm='queryDTO.userName='+userName+'&queryDTO.customType='+1;
			$.post('view!findallViewAndUserViewTwo.action',_parm,function(data){
				var setRowsValue={1:data.rowsWidth.oneRows,2:data.rowsWidth.twoRows,3:data.rowsWidth.threeRows,4:data.rowsWidth.fourRows};
				if(null !== data){
					for(var k = 1;k<=data.map[data.layoutType];k++){
						$('#trash').append("<div class='column' id='dashboard_column_"+k+"' style='float:left;min-height:300px;width:"+setRowsValue[k]+"%;'></div>");
					}
					var _viewdtos = data.viewdtos;
					var rowsStr=data.viewRowsStr.split(":");
					for(var i=0;i<_viewdtos.length;i++){
						var viewType='';
						if(_viewdtos[i].viewType=='staticView'){ //静态视图
							viewType = i18n.staticView;
						}else if(data.viewdtos[i].viewType=='filterView'){  //过滤器视图
							viewType = i18n.filterView;
						}else if(data.viewdtos[i].viewType=='CIView'){
							viewType=i18n.lable_findCiByno;
						}else{//报表视图
							viewType = i18n.reportView;
						}
						var viewShowStr='<div class="portlet" id="protlet_Id'+data.viewdtos[i].viewId+'" style="OVERFLOW-Y: hidden; OVERFLOW-X:auto;">'+
						'<div class="portlet-header" style="text-align: center;cursor: move;">'+data.viewdtos[i].viewName+'</div>'+
						'<div class="portlet-content"><table width="100%" id="ssdddd" class="lineTable" cellspacing="1"></table>'+
						'<input class="inputs" type="hidden" value="'+data.viewdtos[i].viewId+'"/>'+
						'<input class="viewCodeInput" type="hidden" value="'+data.viewdtos[i].viewCode+'"/>'+
						'<div style="text-align:center;"><span id="dd'+data.viewdtos[i].viewType+'" style="text-align: center;">'+viewType+'</span></div>'+
						'<br/><br/><div style="text-align:right;"><a href="#" onclick="common.config.userCustom.portalDashboard.deleteViewAndSaveStr('+data.viewdtos[i].viewId+');" class="action_delete" style="background-image: url(\'../images/comm/action_delete.png\');background-repeat: no-repeat;display: inline-block;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a></div>'+
						'<div id="dddd_0">'+
						'</div></div>'+
						'</div>';
						$('#dashboard_column_'+_viewdtos[i].viewRow).append(viewShowStr);
						if(_viewdtos.length==i+1){
							$("#trash .column" ).sortable({
								connectWith: ".column",
							});
							common.config.userCustom.portalDashboard.refView();
						}
					}
					common.config.userCustom.portalDashboard.loadStaticView(1);
					common.config.userCustom.portalDashboard.loadSetValue(data);
					common.config.userCustom.portalDashboard.showHideText(data.layoutType);
				}
			});
		},
		/**
		 * 刷新视图
		 */
		refView:function(){
			$('#trash .column').sortable('option', 'dropOnEmpty', true);
			$('#trash .column').sortable({ handle: ".portlet-header" });
			$( "#trash .portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" )
			.find( ".portlet-header" )
			.addClass( "ui-widget-header ui-corner-all" )
			.end()
			.find(".portlet-content" );
			$( ".portlet-header .ui-icon" ).click(function() {
				$( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
				$( this ).parents(".portlet:first" ).find(".portlet-content" ).toggle();
				});
			$( "#trash .column" ).disableSelection();
		},
		/**
		 * 加载常用面板
		 * @param val 面板位置
		 */
		loadStaticView:function(val){ //查找常用试图
			var systemView_url='view!findAllSystemView.action';
			$.post(systemView_url,function(data){//加载所有默认视图
				if(null !== data && data.length>0){
					for(var i=0;i<data.length;i++){
						
						viewNoShowStr+='<div class="ui-widget-content ui-helper-clearfix" style="OVERFLOW-Y: hidden; OVERFLOW-X:auto;float:left;width:48%;margin:1px 1px">'+
						'<div class="portlet-header ui-widget-header" style="text-align: center;cursor: move;">'+data[i].viewName+'</div>'+
						'<div class="portlet-content"><table width="100%" class="lineTable" cellspacing="1"></table>'+
						'<input class="inputs" type="hidden" value="'+data[i].viewId+'"/>'+
						'<input class="viewCodeInput" type="hidden" value="'+data[i].viewCode+'"/>'+
						'<div style="text-align:center;"><span id="staticView" style="text-align: center;">'+i18n.staticView+'</span></div>'+
						'<br/><br/><div style="text-align:right;"><a href="#"  class="action_check" style="background-image: url(\'../images/comm/action_check.png\');background-repeat: no-repeat;display: inline-block;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a></div>'+
						'</div></div>';
						if(data.length===i+1){
							common.config.userCustom.portalDashboard.bindOkLink();
						}
					}
					if(val=="1"){
						$('#gallery').html(viewNoShowStr);
					}
					
				}else{
					viewNoShowStr += '<p>'+i18n.noData+'</p>';
				}
			});
		},
		/**
		 * 加载报表面板
		 * @param reportType 面板类型
		 */
		loadReportView:function(reportType){
			var reportView_url = '';
			var reportView_Type='';
			var reportView_show='';
			if(reportType=='all'){
				reportView_url = 'reportModuleAction!findBaseReport.action';
			}else if(reportType=='singleGroup'){
				reportView_url = 'sgreport!findPager.action';
			}else if(reportType=='mutilGroup'){
				reportView_url = 'mutilreport!findPager.action';
			}else if(reportType=='KPI'){
				reportView_url = 'kpireport!findPager.action';
			}else{
				reportView_url = 'crossreport!findPager.action';
			}
			$.post(reportView_url,function(data){//加载所有自定义报表
				var reportData = "";
				if(reportType!=='all'){
					reportData=data.data;
				}else{
					reportData=data;
				}
				if(null !== reportData && reportData.length>0){
					for(var i=0;i<reportData.length;i++){
						switch(trim(reportData[i].moduleIdentifier)){
						case 'singleGroup' && 'singleGroupReport':
							reportView_Type = 'singleView';
							reportView_show = i18n.reportType_single;
							break;
						case 'crosstab' && 'crosstabReport':
							reportView_Type = 'crossRView';
							reportView_show = i18n.reportType_cross;
							break;
						case 'kpiReport' && 'kpiReport':
							reportView_Type = 'KPIREPView';
							reportView_show = i18n.reportType_KPI;
							break;
						case 'mutilGroupReport' && 'mutilGroupReport':
							reportView_Type = 'mutilsView';
							reportView_show = i18n.reportType_mutil;
							break;
						}
						viewNoShowStr+='<div class="ui-widget-content ui-helper-clearfix" style="OVERFLOW-Y: hidden; OVERFLOW-X:auto;float:left;width:48%;margin:1px 1px">'+
						'<div class="portlet-header ui-widget-header" style="text-align: center;cursor: move;">'+reportData[i].reportName+'</div>'+
						'<div class="portlet-content"><table width="100%" class="lineTable" cellspacing="1"></table>'+
						'<input class="inputs" type="hidden" value="'+reportData[i].reportId+'"/>'+
						'<input class="viewCodeInput" type="hidden" value="'+reportView_Type+'_'+reportData[i].reportId+'"/>'+
						'<div style="text-align:center;"><span id="'+reportView_Type+'" style="text-align: center;">'+reportView_show+'</span></div>'+
						'<br/><br/><div style="text-align:right;"><a href="#"  class="action_check" style="background-image: url(\'../images/comm/action_check.png\');background-repeat: no-repeat;display: inline-block;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a></div>'+
						'</div></div>';
						/*viewNoShowStr += '<li class="ui-widget-content ui-corner-tr ui-draggable" style="display: list-item; width: 40%; height: 90px;overflow: auto;"><h5 class="ui-widget-header">'+reportData[i].reportName+'</h5>'+
						'<input class="inputs" type="hidden" value="'+reportData[i].reportId+'"/>'+
						'<input class="viewCodeInput" type="hidden" value="'+reportView_Type+'_'+reportData[i].reportId+'"/>'+
						'<span id='+reportView_Type+'>'+reportView_show+'</span>'+
						'<br><br><a href="#" class="action_check" style="background-image: url(\'../images/comm/action_check.png\');background-repeat: no-repeat;display: inline-block;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>'+
						'</li>';*/
						if(reportData.length===i+1){
							common.config.userCustom.portalDashboard.bindOkLink();
						}
					}
					
				}else{
					viewNoShowStr += '<p>'+i18n.noData+'</p>';
				}
			});
		},
		/**
		 * 加载过滤器面板
		 * @param  filterType 过滤器类型
		 */
		loadFilterView:function(filterType){
			if(filterType=='all'){
				filterType = '';
            }
			var filterView_url = 'filter!findCustomFilter.action?queryDTO.userName='+userName;
			if(filterType!==null && filterType !== ''){
				filterView_url = 'filter!findCustomFilter.action?queryDTO.userName='+userName+'&queryDTO.filterCategory='+filterType;
			}
			$.post(filterView_url,function(data){//加载所有过滤器
				if(null !== data && data.length>0){
					for(var i=0;i<data.length;i++){
						viewNoShowStr+='<div class="ui-widget-content ui-helper-clearfix" style="OVERFLOW-Y: hidden; OVERFLOW-X:auto;float:left;width:48%;margin:1px 1px">'+
						'<div class="portlet-header ui-widget-header" style="text-align: center;cursor: move;">'+data[i].filterName+'</div>'+
						'<div class="portlet-content"><table width="100%" class="lineTable" cellspacing="1"></table>'+
						'<input class="inputs" type="hidden" value="'+data[i].filterId+'"/>'+
						'<input class="viewCodeInput" type="hidden" value="filterView_'+data[i].filterId+'"/>'+
						'<div style="text-align:center;"><span id="filterView" style="text-align: center;">'+i18n.filterView+'</span></div>'+
						'<br/><br/><div style="text-align:right;"><a href="#"  class="action_check" style="background-image: url(\'../images/comm/action_check.png\');background-repeat: no-repeat;display: inline-block;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a></div>'+
						'</div></div>';
						if(data.length===i+1){
							common.config.userCustom.portalDashboard.bindOkLink();
						}
					}
					
				}else{
					viewNoShowStr += '<p>'+i18n.noData+'</p>';
				}
			});
	},
	/**
	 * 面板拖拽，从右边拉过左边去
	 * @param $item 右区域元素
	 * @Parma trashId 左区域元素
	 */
	deleteImage:function($item,trashId){
			//添加视图
			var html=$item.html();
			var viewId =$item.find(".inputs").val();
			var viewType = $item.find("span").attr("id");
			var viewName = $item.find(".portlet-header").text();
			var viewCode = $item.find(".viewCodeInput").val();
			startProcess();
			$.post('view!saveView.action','viewdto.viewType='+viewType+'&viewdto.viewContent='+viewId+'&viewdto.viewName='+viewName+'&viewdto.viewCode='+viewCode,function(bool){
				$.post('view!findPageView.action','queryDTO.viewName='+viewName+'&queryDTO.userName='+userName+'&queryDTO.viewCode='+viewCode,function(viewdata){
					endProcess();
					var data_viewId='';
					var data_viewCode='';
/*					if(viewType==='staticView'){
						data_viewId=viewId;
						console.log(viewdata.data[0].viewId +"==="+viewId);
					}else{*/
						data_viewId = viewdata.data[0].viewId;
					//}
					data_viewCode = viewdata.data[0].viewCode;	
					var checkUrl='view!checkOutViewIsShow.action?queryDTO.viewId='+data_viewId+
								'&queryDTO.userName='+userName+
								'&queryDTO.customType=1'+
								'&queryDTO.viewType='+viewType+
								'&queryDTO.viewCode='+data_viewCode;
					$.post(checkUrl,function(data){
						if(data){
							msgShow(i18n.ERROR_VIEW_CANNOT_MOVE,'show');
						}else{
							if(viewType=='staticView'){ //静态视图
								viewType = i18n.staticView;
							}else if(viewType=='filterView'){  //过滤器视图
								viewType = i18n.filterView;
							}else if(viewType=='CIView'){
									viewType=i18n.lable_findCiByno;
							}else{//报表视图
								viewType = i18n.reportView;
							}
							var viewShowStr='<div class="portlet" id="protlet_Id'+data_viewId+'" style="OVERFLOW-Y: hidden; OVERFLOW-X:auto;">'+
							'<div class="portlet-header" style="text-align: center;cursor: move;">'+viewName+'</div>'+
							'<div class="portlet-content"><table width="100%" class="lineTable" cellspacing="1"></table>'+
							'<input class="inputs" type="hidden" value="'+data_viewId+'">'+
							'<input class="viewCodeInput" type="hidden" value="'+data_viewCode+'">'+
							'<div style="text-align: center;"><span id="dd'+viewType+'" style="text-align:center;">'+viewType+'</span><div>'+
							'<br><br><div style="text-align: right;"><a class="action_delete"  onclick="common.config.userCustom.portalDashboard.deleteViewAndSaveStr('+data_viewId+');"  style="background-image: url(\'../images/comm/action_delete.png\');background-repeat: no-repeat;display: inline-block;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a></div>'+
							'</div>'+
							'</div>';
							$('#'+trashId).append(viewShowStr);
							$('#'+trashId).sortable({
								connectWith: ".column",
								stop:common.config.userCustom.portalDashboard.savaView()
							});
							common.config.userCustom.portalDashboard.refView();
						}
					});
				});
			});
		},
		/**
		 * 面板拖拽。从左边拉往右边方法
		 * @param $item 右区域元素
		 */
		recycleImage:function($item){
			//$item.remove();
			/*$item.fadeOut(function() {
				
				$item.find('input').remove();
				$item.find("a.action_delete").remove().end().css({"width":"120px","height":"90px"});
				//更新ID序列字符串
				common.config.userCustom.portalDashboard.savaView();
			});*/
		},
		/**
		 * 保存面板
		 */
		savaView:function(){
			var list="";
			var list1="";
			var cou="";
			layoutType = $("#selectItem").val();
			var layoutCount=laytou[layoutType];
				for ( var _int = 0; _int <layoutCount; _int++) {
					$.each($("#dashboard_column_"+(_int+1)+" .viewCodeInput"), function(m) {
						list += $(this).attr('value') + ":";
					});
					$.each($("#dashboard_column_"+(_int+1)+" .inputs"),function(){
						list1+=(_int+1)+":";
					});
				}
				for ( var k =(layoutCount+1); k <5; k++) {
					$.each($("#dashboard_column_"+(k)+" .viewCodeInput"), function(m) {
						list += $(this).attr('value') + ":";
					});
					$.each($("#dashboard_column_"+(k)+" .inputs"),function(){
						list1+=(layoutCount)+":";
					});
				}
			var vUrl = 'userCustom!updateUserCustomView.action?userCustomDTO.loginName='+userName+'&userCustomDTO.layoutType='+layoutType+
					'&userCustomDTO.viewIdStr='+list+'&userCustomDTO.customType='+1+'&userCustomDTO.viewRowsStr='+list1;
			$.post(vUrl,function(){
			});	
		},
		/**
		 * 面板拖拽
		 */
		bindOkLink:function(){
			setTimeout(function(){
				$(".gallery > div").click(function(event) {
					var $item = $(this), $target = $(event.target);
					if ($target.is("a.action_delete")) {
						//点击左边的x
						common.config.userCustom.portalDashboard.recycleImage($item);
					}else if ($target.is("a.action_check")) {
						//点击右边的勾
						common.config.userCustom.portalDashboard.deleteImage($item,"dashboard_column_1");
					}
					return false;
				});
			},1000);
		},
		/**
		 * 初始化
		 * @private
		 */
		init:function(){
			//加载数据
			common.config.userCustom.portalDashboard.loadData();
			
			setTimeout(function(){
				var $gallery = $("#gallery"), $trash = $(".column");
				$trash.droppable({
					accept : "#gallery > div",
					activeClass: "ui-custom-hover",
					hoverClass: "ui-custom-active",
					addClasses:false,
					tolerance:'pointer',
					activate: function(event, ui) {
						ui.draggable.css("width","250px");
					},
					drop : function(event, ui) {
						//$( this ).addClass( "ui-state-highlight" );
						common.config.userCustom.portalDashboard.deleteImage(ui.draggable,$(this).attr("id"));
					}
				});
/*				$gallery.droppable({
					accept : "#trash li",
					activeClass : "custom-state-active",
					drop : function(event, ui) {
						//common.config.userCustom.portalDashboard.recycleImage(ui.draggable);
					}
				});*/

				$('#gallery').sortable();
				$trash.sortable({
					stop:common.config.userCustom.portalDashboard.savaView
				});
			},1000);
		}
	}
}();

//载入
$(document).ready(common.config.userCustom.portalDashboard.init);