$package("common.config.userCustom");
/**  
 * @author QXY  
 * @constructor users
 * @description 门户面板管理
 * @date 2011-10-31
 * @since version 1.0 
 */
common.config.userCustom.viewManage=function(){
	this.optType='';
	return {
		/**
		 * 面板类型显示格式化
		 */
		defaultShowFormt:function(cellvalue){
			if(cellvalue=='staticView'){
				return i18n.staticView;
			}else if(cellvalue=='filterView'){
				return i18n.filterView;
			}else if(cellvalue=='reportView'){
				return i18n.reportView;
			}
		},
		/**
		 * 是否显示
		 */
		isShow:function(cellvalue){
			if(cellvalue==1){
				return i18n.label_basicConfig_deafultCurrencyYes;
			}else{
				return i18n.label_basicConfig_deafultCurrencyNo;
			}
		},
		/**
		 * 面板列表
		 */
		viewGrid:function(){
			var params = $.extend({},jqGridParams, {	
				url:'view!findPageView.action?queryDTO.userName='+userName+'&&queryDTO.isShow='+2,
				colNames:['ID',i18n.label_viewName,i18n.label_viewType,'',i18n.label_dashboard_defautl_show,'',''],
                colModel:[
                          {name:'viewId',align:'center',width:100},
                          {name:'viewName',align:'center',width:200},
                          {name:'viewType',align:'center',width:200,formatter:common.config.userCustom.viewManage.defaultShowFormt},
                          {name:'viewType',align:'center',hidden:true},
                          {name:'isShow',align:'center',width:180,formatter:common.config.userCustom.viewManage.isShow},
                          {name:'isShow',align:'center',width:180,hidden:true},
                          {name:'viewContent',align:'center',width:180,hidden:true}
                ],
				jsonReader: $.extend(jqGridJsonReader, {id:"viewId"}),
				sortname:'viewId',
				sortorder:'asc',
				shrinkToFit:false,
				pager:'#viewPager'
				});
				$("#viewGrid").jqGrid(params);
				$("#viewGrid").navGrid('#viewPager',navGridParams);
				//列表操作项
				$("#t_viewGrid").css(jqGridTopStyles);
				$("#t_viewGrid").append($('#viewOptItem').html());
				
				//自适应宽度
				setGridWidth("#viewGrid","regCenter",20);
		},
		
		/**
		 * 新增面板
		 */
		addView:function(){
			optType="save";
			$('#view_viewId').val('');
			resetForm('#viewaddOrEdit_form');
			windows('viewManage_addorEdit',{width:450});
		},
		/**
		 * 选择面板类型
		 * @param value 类别
		 */
		viewTypeSelectChange:function(value){
          var url='';
			$('#view_viewContent').html('');
			var str = '<option value="">--'+i18n.pleaseSelect+'--</option>';
//			if(value=='staticView'){   //静态视图
//				//查找所有dashboard所有面板提供静态视图选择
//                url = 'dashboard!findDashboardAll.action';
//				$.post(url,function(data){
//					if(data!==null && data.length>0){
//						for(var i=0;i<data.length;i++){
//							str += '<option value="'+data[i].dashboardId+'">'+data[i].dashboardName+'</option>';
//						}
//					}
//					$('#view_viewContent').html(str);
//				});
//			}else 
			if(value=='filterView'){   //过滤器视图
			//查找所有当前用户能查看的所有过滤器提供过滤器视图选择
			url = 'filter!findCustomFilter.action?queryDTO.userName='+userName;
			$.post(url,function(filters){
				if(filters!==null && filters.length>0){
					for(var i=0;i<filters.length;i++){
						str += '<option value="'+filters[i].filterId+'">'+filters[i].filterName+'</option>';
					}
				}
				$('#view_viewContent').html(str);
			});
			}else if(value=='reportView'){ //查找所有当前用户能查看的所有自定义报表提供给过滤器视图选择
				url = 'crossreport!findPager.action';
				$.post(url,function(data){
					if(data.data!==null && data.data.length>0){
						for(var i=0;i<data.data.length;i++){
							str += '<option value="'+data.data[i].reportId+'">'+data.data[i].reportName+'</option>';
						}
					}
					$('#view_viewContent').html(str);
				});
			}else{
				$('#view_viewContent').html(str);
			}
			
		},
		/**
		 * 编辑面板
		 */
		editView:function(){
			optType="edit";
			checkBeforeEditGrid('#viewGrid',common.config.userCustom.viewManage.viewEditValue);
		},
		/**
		 * @description 显示编辑的值
		 * @param  rowData 行数据
		 */
		viewEditValue:function(rowData){
			$('#view_viewId').val(rowData.viewId);
			$('#view_viewName').val(rowData.viewName);
			$('#view_viewType').val(rowData.viewType);
			common.config.userCustom.viewManage.viewTypeSelectChange(rowData.viewType);
			$('#view_proportion').val(rowData.proportion);
			if(rowData.isShow==1){
				$('#isShow_yes').attr("checked","true");
			}else{
				$('#isShow_no').attr("checked","true");
			}
			setTimeout(function(){
				$('#view_viewContent').val(rowData.viewContent);
			},500);
			windows('viewManage_addorEdit',{width:450});
		},
		/**
		 * 保存面板
		 */
		saveView:function(){
			//给view_userName 输入框赋值
			$('#view_userName').val(userName);
			if($('#viewManage_addorEdit form').form('validate')){
				startProcess();
				var _frm=$('#viewManage_addorEdit form').serialize();
				$.post('view!'+optType+'View.action',_frm,function(){
					endProcess();
					$('#viewGrid').trigger('reloadGrid');
					msgShow(i18n.saveSuccess,'show');
					$('#viewManage_addorEdit').dialog('close');
				});
			}
		},
		/**
		 * 删除面板是否选择
		 */
		deleteView:function(){
			checkBeforeDeleteGrid('#viewGrid',common.config.userCustom.viewManage.deleteViewMethod);
		},
		/**
		 * @description 删除面板
		 * @param  rowIds 行编号
		 */
		deleteViewMethod:function(rowsId){
			var url='view!deleteView.action';
			var param = $.param({'ids':rowsId},true);
			$.post(url, param, function()
			{
				//重新统计
				$('#viewGrid').trigger('reloadGrid');
				msgShow(i18n.msg_deleteSuccessful,'show');
			});	
		},
		/**
		 * 搜索面板
		 */
		viewSearch:function(){
			windows('view_viewSearch_div',{width:400});
		},
		/**
		 * 搜索面板
		 */
		viewDoSearch:function(){
			var sdata=$('#view_viewSearch_div form').getForm();
			var postData = $("#viewGrid").jqGrid("getGridParam", "postData");
			$.extend(postData,sdata);  
			var _url = 'view!findPageView.action';		
			$('#viewGrid').jqGrid('setGridParam',{url:_url}).trigger('reloadGrid',[{"page":"1"}]);
		},
		/**
		 * 初始化
		 * @private
		 */
		init:function(){
			$("#viewManage_loading").hide();
			$("#viewManage_content").show();
			common.config.userCustom.viewManage.viewGrid();
			$('#link_view_add').click(common.config.userCustom.viewManage.addView);
			$('#link_view_edit').click(common.config.userCustom.viewManage.editView);
			$('#view_save').click(common.config.userCustom.viewManage.saveView);
			$('#link_view_delete').click(common.config.userCustom.viewManage.deleteView);
			$('#link_view_search').click(common.config.userCustom.viewManage.viewSearch);
			$('#view_dosearch').click(common.config.userCustom.viewManage.viewDoSearch);
		}
	}
}();
$(document).ready(common.config.userCustom.viewManage.init);