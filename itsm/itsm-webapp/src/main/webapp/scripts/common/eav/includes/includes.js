﻿ $package('common.eav.includes');
/**  
 * @fileOverview "Includes"
 * @author QXY
 * @version 1.0  
 */  
/**
 * @author QXY  
 * @constructor Includes
 * @description "用于加载Includes文件JS主函数"
 * @date 2012-7-31
 * @since version 1.0 
 */ 
common.eav.includes.includes=function(){
	//扩展属性分组
	this._loadAttributeGroupFileFlag = false;
	return {
		/**
		 * @description 加载扩展属性分组includes文件
		 */
		loadAttributeGroupIncludesFile:function(){
			if(!_loadAttributeGroupFileFlag){
				$('#attributeGroup_html').load('common/eav/includes/includes_selectAttributeGroup.jsp',function(){
					$.parser.parse($('#attributeGroup_html'));
				});
			}
			_loadAttributeGroupFileFlag=true;
		},
		init:function(){
			
		}
	}
}();
$(function(){common.eav.includes.includes.init});
