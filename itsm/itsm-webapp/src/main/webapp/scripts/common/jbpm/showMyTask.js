 
$package('common.jbpm') 
/**  
 * @fileOverview 我的流程任务主函数
 * @author amy
 * @version 1.0  
 */  
 /**  
 * @author amy  
 * @constructor 我的流程任务主函数
 * @description 我的流程任务主函数
 * @date 2010-11-10
 * @since version 1.0 
 */
common.jbpm.showMyTask=function(){
	
	return {
		/**
		 *@description  操作项格式化
		 *@param cellValue:单元格的值；
		 *@param options:字体样式等；
		 *@param rowObject:一行的数据
		 */
		operateFormatter:function(cellValue, options, rowObject) {
			return '<a href=javascript:common.jbpm.processCommon.showFlowChart("'+rowObject.executionId+'","")>'+i18n['tracking']+'</a>';
		},
		/**
		 * 标题格式化
		 */
		titleUrlFrm:function(cellvalue, options, rowObject){
//			var dto = cellvalue.dto;
//			return '<a href="JavaScript:requestDetails('+dto.eno+','+rowObject.id+')">'+cellvalue+'</a>'
			
			return '';
		},
		
		/**
		 * @description 数据列表行格式化.
		 */
		titleUrlFrm1:function(cellvalue, options, rowObject){
//			var executionId = rowObject.executionId;
//			var eno ;
//			var dto = cellvalue.dto;
//			if(dto!=undefined && dto!=null){
//				if(executionId.indexOf('Changes')>=0){
//					return "<a href=javascript:basics.tab.tabUtils.reOpenTab('change!changeInfo.action?queryDTO.changeId="+dto.changeId+"&queryDTO.taskId="+rowObject.id+"&queryDTO.assignee="+rowObject.assignee+"','"+i18n['change_detail']+"')>"+cellvalue+":"+dto.changeTitle+"</a>"
//				}	
//				else	
//					return '<a href="JavaScript:requestDetails1('+dto.eno+','+rowObject.id+')">'+cellvalue+":"+dto.etitle+'</a>'
//			}else
				return '';
			
		},
		
		/**
		 * @description 转到请求详细页面.
		 * @param eno 编号eno
		 * @param taskId 任务Id
		 */
		requestDetails1:function(eno ,taskId){
			basics.tab.tabUtils.refreshTab(i18n['request_detail'],'request!requestDetails.action?eno='+eno+'&taskId='+taskId);
		},
		/**
		 * 加载我的流程任务
		 */
		load_processInstances_list:function(){
			var params = $.extend({},jqGridParams, {	
				url:'jbpm!findPagePersonalTasks.action?assignee='+userName,
				colNames:['',i18n['common_name'],i18n['common_title'],'','',i18n['common_createTime'],i18n['title_process_taskExpirationTime'],'','','formResourceName','executionId','progress',i18n['description'],i18n['common_action']],
				colModel:[
	  					  {name:'id',hidden:true},
						  {name:'name',index:'name',width:100},
						  {name:'variables',width:100,formatter:common.jbpm.showMyTask.titleUrlFrm1},
						  {name:'activityName',hidden:true},
						  {name:'description',hidden:true},
						  {name:'createTime',width:100,formatter:common.jbpm.showMyTask.timeFormatter},
						  {name:'duedate',width:150,formatter:common.jbpm.showMyTask.timeFormatter},
						  {name:'priority',hidden:true},
						  {name:'assignee',hidden:true},
						  {name:'formResourceName',hidden:true},
						  {name:'executionId',hidden:true},
						  {name:'progress',hidden:true},
						  {name:'remark',index:'remark',width:100},
						  {name:'operate',width:100,align:'center',formatter:common.jbpm.showMyTask.operateFormatter}
					  ],
				toolbar:false,	  
				jsonReader: $.extend(jqGridJsonReader, {id: "id"}),
				sortname:'id',
				pager:'#MyTasksPager'
				});
			
				$("#MyTasksGrid").jqGrid(params);
				$("#MyTasksGrid").navGrid('#MyTasksPager',navGridParams);
				//列表操作项
				$("#t_MyTasksGrid").css(jqGridTopStyles);
				$("#t_MyTasksGrid").append($('#toolbar').html());
				
				//自适应宽度
				setGridWidth("#MyTasksGrid","regCenter",20);	
		},
		/**
		 *@description  显示流程图
		 *@param _id 流程ID
		 */
		trace:function(_id){
			    var _url = 'jbpm!traceInstance.action?instanceId='+_id;
			    basics.tab.tabUtils.addTab(i18n['flow']+i18n['tracking'],_url);
		},
		/**
		 * @description 检查流程
		 * @param _id 流程Id
		 * */
		reviewProcess:function(_id){
		    var _url = 'jbpm!viewApply.action?taskId='+_id;
		    basics.tab.tabUtils.addTab(i18n['look']+i18n['flow'],_url);
		},
		/**
		 * @description 检查流程
		 * @param _id 流程Id
		 * */
		reviewProcess1:function(_id){
		    var _url = 'request!findRequestById.action?taskId='+_id;
		    basics.tab.tabUtils.addTab(i18n['look']+i18n['flow'],_url);
		},
		/**
		 * 初始化
		 * @private
		 */
		init:function(){
			common.jbpm.showMyTask.load_processInstances_list();
		}
	}
}();
//载入
$(document).ready(common.jbpm.showMyTask.init());
