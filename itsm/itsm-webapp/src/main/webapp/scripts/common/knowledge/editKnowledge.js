$package('common.knowledge');
$import('common.knowledge.leftMenu');
$import('common.knowledge.knowledgeTree');
$import('common.config.category.serviceDirectoryUtils');
$import('common.tools.file.attachIcon');
$import('common.config.attachment.chooseAttachment');
$import('common.config.includes.includes');
$import('common.config.category.serviceCatalog');
$import('common.security.xssUtil');
/**  
 * @author Van  
 * @constructor editKnowledge
 * @description "知识库编辑"
 * @date 2010-11-17
 * @since version 1.0 
 */
common.knowledge.editKnowledge = function() {	
	return {
		/**
		 * @description 根据ID查找知识信息
		 */
		findKnowledgeById:function(){
			
			var url = 'knowledgeInfo!findKnowledgeById.action?kid='+id;
			$.post(url,function(res){
				
				$('#editKnowledge_kid').val(res.kid);
				$('#editKnowledge_addTime').val(res.addTime);
				if(res.title=="null" || res.title == null){
					$('#editKnowledge_title').val("null");
				}else{
					$('#editKnowledge_title').val(common.security.xssUtil.html_code(res.title));	
				}
				if(res.categoryNo!=null){
					$('#editKnowledge_categoryNo').val(res.categoryNo);
				}
				if(res.categoryName!=null){
					$('#editKnowledge_Category').val(res.categoryName);
				}
				if(res.keyWords=="null" || res.keyWords==null){
					$('#editKnowledge_keyWords').val("null");
				}else{
					$('#editKnowledge_keyWords').val(common.security.xssUtil.html_code(res.keyWords));
				}
				
				initCkeditor('editKnowledgeCon','full',function(){
					var oEditor = CKEDITOR.instances.editKnowledgeCon;
					oEditor.setData(res.content);
				});
				$('#editKnowledgeCon').val(res.content);
				//$('#editKnowledge_service_dir_items_name').val($vl(res.serviceDirectoryItemName));
				var kno=res.listEvent;
				if(kno!=null){
					for(var j=0;j<kno.length;j++){
						var eventId=kno[j].eventId;
						var eventName=kno[j].eventName;
						$("#editKnowledge_service_name").append("<tr id=edit_knowledge_"+eventId+"><td>"+eventName+"</td><input type=hidden name='knowledgeDto.knowledServiceNo' value="+eventId+"><td><a onclick=common.knowledge.knowledgeTree.serviceRm("+eventId+")>"+i18n['deletes']+"</a><input type=hidden name=serviceDirNos value="+eventId+"></td></tr>");
					}
				}
				$('#editKnowledge_service_dir_items_id').val($vl(res.serviceDirectoryItemNo));
				var arr=res.attachments;
				if(arr!=null){
				for(var i=0;i<arr.length;i++){
					var file=arr[i].url;
					var fileName=file.substring(file.lastIndexOf("/")+1);
					var iconUrl=common.tools.file.attachIcon.getIcon(fileName);
					$('#editKnowledge_oldAttachments').append("<div id='editKnowledge_oldAttachments_ID_"+arr[i].aid+"'>"+iconUrl+"<a href='attachment!download.action?downloadAttachmentId="+arr[i].aid+"' target='_blank'>"+arr[i].attachmentName+"</a>&nbsp;&nbsp;<a href=javascript:common.knowledge.editKnowledge.deleteAttachement('"+id+"','"+arr[i].aid+"')>"+i18n['deletes']+"</a></div>");
  
				}
				
				}

				
			});
			
		},
		/**
		 * 移除关联的服务目录
		 */
		editServiceRm:function(id){
			$("#Service"+id).remove();
		},
		
		/**
		 * @description 删除附件
		 * @param kid 知识库ID
		 * @param aid 附件ID
		 */
		deleteAttachement:function(kid,aid){
			
			 msgConfirm(i18n['msg_msg'],'<br/>'+i18n['msg_confirm_deleteAttachment'],function(){
					var _url = "knowledgeInfo!deleteAttachment.action?kid="+kid+"&aid="+aid;
					$.post(_url,function(){
						//根据ID移除DIV
						$('div#editKnowledge_oldAttachments_ID_'+aid).remove(); //删除匹配元素
					});
				});
			
		},
		
		/**
		 * @description 编辑知识
		 */
		doEditKnowledge:function(){
			var oEditor = CKEDITOR.instances.editKnowledgeCon;
			var title=trim($('#editKnowledge_title').val());
			var content=trim(oEditor.getData());
			var content = content.replace(/\s*<\/p>\s*|\s*<\/p>\s*/, '').replace(/\s*<p>\s*|\s*<\/p>\s*/, '').replace(/&nbsp;/gi, '');
			if(title==''||content=='' ){
				msgAlert(i18n['knowTitleAndContentNotNull'],'info');
			}else{
				$('#editKnowledge_Content').val(content);
				if($('#editKnowledge_knowledgeStatus_check').attr("checked")){
					$('#editKnowledge_knowledgeStatus').val(1);
				}else{
					$('#editKnowledge_knowledgeStatus').val(0);
				}
				$.each($("#editKnowledgePanel :input[attrtype='String']"),function(ind,val){
					$(this).val(common.security.xssUtil.html_encode($(this).val()));
				});
				var _params=$('#editKnowledgePanel form').serialize();
				var _url = "knowledgeInfo!updateKnowledgeInfo.action";
				if(editkw_opt=='myapf'){
					_url = "knowledgeInfo!editKnowledgeInfo.action";
				}
				
				//进程状态
				startProcess();
				$.post(_url,_params,function(){
					basics.tab.tabUtils.closeTab(i18n.title_request_knowledgeGrid);
					basics.tab.tabUtils.closeTab(i18n.knowledge_editKnowledge);
					common.knowledge.leftMenu.showKnowledgeIndex("knowledge");
					$('#knowledgeGrid').trigger('reloadGrid');
					msgShow(i18n['editSuccess'],'show');
					//进程状态
					endProcess();
				});
				
			}
		},
		checkKnowledgeTitle:function(){
			var title=trim($('#editKnowledge_title').val());
			var _url='knowledgeInfo!findKnowledgeByTitle.action';
			var parame = {"knowledgeDto.title":title};
			$.post(_url,parame,function(data){
				if(data != null){
					msgAlert(i18n['knowledge_title_exist'],'info');
				}else{
					common.knowledge.editKnowledge.doEditKnowledge();
				}
			});
		},
		checkKnowledgeValidate:function(){
			var title = $("#editKnowledge_title").val();
			var _url = 'knowledgeInfo!findKnowledgeById.action?kid='+$("#editKnowledge_kid").val();
			$.post(_url,function(obj){
				var objTitle = obj.title;
				if(objTitle=="null" || objTitle == null){
					objTitle = "null";
				}else{
					objTitle = common.security.xssUtil.html_code(objTitle);
				}
				if(objTitle==trim(title)){
					common.knowledge.editKnowledge.doEditKnowledge();
				}else{
					common.knowledge.editKnowledge.checkKnowledgeTitle();
				}
			});
		},
		/**
		 * 初始化加载
		 * @private
		 */
		init: function() {
			$("#editKnowledge_loading").hide();
			$("#editKnowledge_contentPanel").show();
			
			$('#editKnowledge_Category').click(function(){
				common.knowledge.knowledgeTree.selectKnowledgeCategory('#knowledge_category_select_window','#knowledge_category_select_tree','#editKnowledge_Category','#editKnowledge_categoryNo');			
			});
			$('#editKnowledgeBtn').click(common.knowledge.editKnowledge.checkKnowledgeValidate);
			$('#edit_backToKnowledgeIndex').click(function(){
				startLoading();
				common.config.includes.includes.loadServiceDirectoryItemsIncludesFile();
				common.config.includes.includes.loadCustomFilterIncludesFile();
				common.config.includes.includes.loadCategoryIncludesFile();
				common.knowledge.leftMenu.showKnowledgeIndex("knowledge");
				endLoading();
			});			
			
			$('#editKnowledge_service_dir_items_name').click(function(){
				common.config.category.serviceCatalog.selectServiceDir('#editKnowledge_service_name');
				//common.knowledge.knowledgeTree.selectKnowledgeServiceEdit('#knowledge_services_select_window','#knowledge_services_select_tree','#editKnowledge_service_name','');			
			});
			
			$('#search_serviceDirectoryItemName_click').click(function(){
				common.knowledge.knowledgeTree.searchKnowledgeService();
				//common.config.category.serviceDirectoryUtils.showSelect('#editKnowledge_service_dir_items_name','#editKnowledge_service_dir_items_id','edit_knowledge_');
				//common.knowledge.knowledgeTree.selectKnowledgeServiceSearch('#knowledge_services_select_window','#knowledge_services_select_tree','#search_service_name','#addKnowledge_serviceNo');			
			});
			//实例化附加上传
			setTimeout(function(){
				//initFileUpload("_KnowledgeEdit","","",userName,"","editKnowledge_attachments","editKnowledge_oldAttachments");
				getUploader('#editKnowledge_uploadAttachments','#editKnowledge_attachments','#editKnowledge_oldAttachments','editKnowledge_fileQueue');
				common.knowledge.editKnowledge.findKnowledgeById();
			},0);
			
		}
		
	};
	
	
}();

//载入
$(document).ready(common.knowledge.editKnowledge.init);




