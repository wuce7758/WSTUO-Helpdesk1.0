$package('common.report');
$import('basics.tab.tabUtils');
/**  
 * @fileOverview "报表展示参数传递"
 * @author Martin  
 * @constructor Martin
 * @description 报表展示参数传递
 * @date 2014-09-13
 * @version 1.0  
 * @since version 1.0 
 */
common.report.clickEvent=function(){
	var reporti18n=$('#leftreportLang').val();
	this.currentMonth=(new Date().format('yyyy-MM-dd'));
	return{
		/**
		 * 调用上下月,周
		 * @param reportName 报表名称
		 * @param title 选项卡标题
		 * @param showHide 是否隐藏
		 * @param opt  上下参数
		 * @param firstDates 开始时间
		 * @param confirm 是否HQL
		 * */
		openReportUpAndDown:function(reportName,title,showHide,opt,firstDates,confirm){
			common.report.processingTime.openReportUpAndDownTimeFormat(showHide,opt,firstDates); 
			var ifnext=$('#htmlReport_'+reportName+showHide).val();
			if(FIRSTDATE!=""){ 
				if(ifnext=="false" || ifnext==false || (ifnext!=false && ifnext!="false" && opt!="next") ){
					basics.tab.tabUtils.refreshTab(title,"../pages/reports!statReportRead.action?dto.fileName="+reportName+"&dto.firstDate="+FIRSTDATE+"&dto.lastDate="+LASTDATE+'&dto.showHide='+showHide+'&dto.reporti18n='+reporti18n+"&dto.confirm="+confirm);
				}
			}
		},
		/**
		 * 公司,技术员,时间添加查看
		 *  @param tabTitle 选项卡标题
		 *  @param reportName 报表名称
		 */
		companiesAndTechniciansAndTimeSearch:function(tabTitle,reportName){
			$('.WSTUO-dialog').dialog('close');//关闭打开的窗口
			setTimeout(function(){
				windows('customer_dateReg_search_wid',{title:tabTitle,modal: false});
			},200);
			$('#generate_report_but').unbind().click(function(){
				if($('#customer_dateReg_search_wid form').form('validate')){
				   var _param=$('#customer_dateReg_search_wid form').serializeArray();
				   basics.tab.tabUtils.refreshTab(tabTitle,"../pages/reports!statReportRead.action?dto.fileName="+reportName+"&dto.firstDate="+_param[2].value+"&dto.lastDate="+_param[3].value+"&dto.companyNo="+_param[0].value+"&dto.assigneeId="+_param[1].value+"&dto.confirm=false"+"&dto.reporti18n="+reporti18n);		
				   $('.WSTUO-dialog').dialog('close');//关闭打开的窗口
				}
			});
		},
		/**
		 * 搜索预览报表.
		 * @param tabTitle 选项卡标题
		 * @param reportName 报表名称
		 * @param confirm 是否HQL
		 */
		techniciansAndTimeSearch:function(tabTitle,reportName,confirm){
			$('.WSTUO-dialog').dialog('close');//关闭打开的窗口
			setTimeout(function(){
				windows('Technician_Cost_Time_Search_Window',{width:450,modal: false})
			},440);
			$('#Technician_Cost_Time_Search_Btn').unbind().click(function(){
				if($('#Technician_Cost_Time_Search_Window form').form('validate')){
					var _param=$('#Technician_Cost_Time_Search_Window form').serializeArray();
					basics.tab.tabUtils.refreshTab(tabTitle,"../pages/reports!statReportRead.action?dto.fileName="+reportName+"&dto.firstDate="+_param[1].value+"&dto.lastDate="+_param[2].value+"&dto.userId="+_param[0].value+"&dto.confirm="+confirm+"&dto.reporti18n="+reporti18n);
					$('.WSTUO-dialog').dialog('close');//关闭打开的窗口
				}
			});
		},
		/**
		 * 搜索请求分类
		 * @param tabTitle 选项卡标题
		 * @param reportName 报表名称
		 * @param confirm 是否HQL
		 */
		 searchRequestCategory:function(reportName,tabTitle,confirm){
			$('.WSTUO-dialog').dialog('close');//关闭打开的窗口
			setTimeout(function(){
				windows('search_in_report',{title:tabTitle,width:450,modal: false});
			},200);
			$('#for_report_releaseCategory_Search_Btn').unbind().click(function(){
				if($('#for_report_releaseCategory_Search').form('validate')){
					var _param=$('#for_report_releaseCategory_Search').serializeArray();
					basics.tab.tabUtils.refreshTab(tabTitle,"../pages/reports!statReportRead.action?dto.fileName="+reportName+"&dto.firstDate="+_param[1].value+"&dto.lastDate="+_param[2].value+"&dto.confirm="+confirm+"&dto.reporti18n="+reporti18n+"&dto.RequestCategory="+_param[0].value);
					$('.WSTUO-dialog').dialog('close');//关闭打开的窗口
				}
			});
		},
		/**
		 * 自动选择时间
		 * @param tabTitle 选项卡标题
		 * @param reportName 报表名称
		 * @param confirm 是否HQL
		 */
		optionalTime:function(reportName,tabTitle,confirm){
			$('.WSTUO-dialog').dialog('close');//关闭打开的窗口
			setTimeout(function(){
				windows('technician_process_request_time',{title:tabTitle,width:450,modal: true});
			},200);
			$('#technician_process_Search_Btn').unbind().click(function(){
				if($('#technician_process_request_time form').form('validate')){
					var _param=$('#technician_process_request_time form').serializeArray();
					basics.tab.tabUtils.refreshTab(tabTitle,"../pages/reports!statReportRead.action?dto.fileName="+reportName+"&dto.firstDate="+_param[0].value+"&dto.lastDate="+_param[1].value+"&dto.confirm="+confirm+"&dto.reporti18n="+reporti18n);
					$('.WSTUO-dialog').dialog('close');//关闭打开的窗口
				}
			});
		},

		/**
		 * 搜索预览报表.
		 */
		Request_detailed:function(tabTitle,reportName,Confirm){
			$('.WSTUO-dialog').dialog('close');//关闭打开的窗口
			setTimeout(function(){
				windows('Request_detailed_Search_Win',{width:450})
			},440);
			$('#Request_detailed_Search_Btn').unbind().click(function(){
				if($('#Request_detailed_Search_Window form').form('validate')){
					var _param=$('#Request_detailed_Search_Win form').serializeArray();
					basics.tab.tabUtils.refreshTab(tabTitle,
							"../pages/reports!statReportRead.action?dto.fileName="+reportName+
							"&dto.firstDate="+_param[3].value+"&dto.lastDate="+_param[4].value+
							"&dto.companyNo="+_param[0].value+"&dto.Confirm="+Confirm+"&dto.reporti18n="+reporti18n+
							"&dto.userId="+_param[1].value+"&dto.assigneeId="+_param[2].value);
				
					$('.WSTUO-dialog').dialog('close');//关闭打开的窗口
				}
			});
		},
		/**
		 *对上传的报表及时更新到相应报表模块
		 * */
		loadReportModuleLeftMenu:function(){
			$('#report_leftMenuName_request').html('');
			$('#report_leftMenuName_problem').html('');
			$('#report_leftMenuName_change').html('');
			$('#report_leftMenuName_CIReport').html('');
			$('#report_leftMenuName_Retrospect').html('');
			$.post('reportModuleAction!findBaseReport.action',function(data){
				if(data!=null){
					var arrayObj = new Array(data.length);
					for(var i=0;i<data.length;i++){
						if(data[i].entityClass){
							if("com.wstuo.itsm.request.entity.Request"==data[i].entityClass){
								arrayObj[i]="report_leftMenuName_request";
							}else if("com.wstuo.itsm.domain.entity.Problems"==data[i].entityClass){
								arrayObj[i]="report_leftMenuName_problem";
							}else if("com.wstuo.itsm.domain.entity.Changes"==data[i].entityClass){
								arrayObj[i]="report_leftMenuName_change";
							}else if("com.wstuo.itsm.domain.entity.CI"==data[i].entityClass){
								arrayObj[i]="report_leftMenuName_CIReport";
							}
						if(data[i].reportType=='singleGroup'){
							
							var strHtml = '<li id="'+data[i].reportId+'" title="'+data[i].reportName+'" onclick="basics.tab.tabUtils.refreshTab(\'' + data[i].reportName + '\', \'sgreport!show.action?reportId=' + data[i].reportId + '\');" >';
							strHtml += '<span class="leftMenu_thirdMenu_item_icon"></span>';
							strHtml += '<span class="leftMenu_thirdMenu_item_title">' + data[i].reportName + '</span>';
							strHtml += '</li>';
							$('#'+arrayObj[i]).append(strHtml);
							leftMenu.isSecondMenuScroll();
					   		leftMenu.thirdMenuClickAction();
						}
						if(data[i].reportType=='crosstab'){
							
							var strHtml = '<li id="'+data[i].reportId+'" title="'+data[i].reportName+'" onclick="basics.tab.tabUtils.refreshTab(\'' + data[i].reportName + '\', \'crossreport!show.action?reportId=' + data[i].reportId + '\');" >';
							strHtml += '<span class="leftMenu_thirdMenu_item_icon"></span>';
							strHtml += '<span class="leftMenu_thirdMenu_item_title">' + data[i].reportName + '</span>';
							strHtml += '</li>';
							$('#'+arrayObj[i]).append(strHtml);
							leftMenu.isSecondMenuScroll();
					   		leftMenu.thirdMenuClickAction();
						}

						if(data[i].reportType=='kpiReport'){
							
							var strHtml = '<li id="'+data[i].reportId+'" title="'+data[i].reportName+'" onclick="basics.tab.tabUtils.refreshTab(\'' + data[i].reportName + '\', \'kpireport!show.action?reportId=' + data[i].reportId + '\');" >';
							strHtml += '<span class="leftMenu_thirdMenu_item_icon"></span>';
							strHtml += '<span class="leftMenu_thirdMenu_item_title">' + data[i].reportName + '</span>';
							strHtml += '</li>';
							$('#'+arrayObj[i]).append(strHtml);
							leftMenu.isSecondMenuScroll();
					   		leftMenu.thirdMenuClickAction();
						
						}

						if(data[i].reportType=='mutilGroup'){
							var strHtml = '<li id="'+data[i].reportId+'" title="'+data[i].reportName+'" onclick="basics.tab.tabUtils.refreshTab(\'' + data[i].reportName + '\', \'mutilreport!show.action?reportId=' + data[i].reportId + '\');" >';
							strHtml += '<span class="leftMenu_thirdMenu_item_icon"></span>';
							strHtml += '<span class="leftMenu_thirdMenu_item_title">' + data[i].reportName + '</span>';
							strHtml += '</li>';
							$('#'+arrayObj[i]).append(strHtml);
							leftMenu.isSecondMenuScroll();
					   		leftMenu.thirdMenuClickAction();
						}
						}
					} 
				}
			})
		},
		/**
		 * 年,月,周查看
		 * @param reportName 报表名称
		 * @param title 选项卡标题
		 * @param type 类型
		 * @param confirm 是否HQL
		 */
		openReportBean:function(reportName,title,type,confirm){
			var FIRSTDATES="";
			var LASTDATES="";
			if(type=="YEARS"){
				FIRSTDATES=common.report.processingTime.showYearFirstDay(currentMonth);
		    	LASTDATES=common.report.processingTime.showYearLastDay(currentMonth);
	    	}else if(type=="MONTHS"){
		    	FIRSTDATES=common.report.processingTime.showMonthFirstDay(currentMonth);
		    	LASTDATES=common.report.processingTime.showMonthLastDay(currentMonth); 
	    	}else if(type=="WEEKS"){
		    	FIRSTDATES=common.report.processingTime.showWeekFirstDay(currentMonth).format('yyyy-MM-dd');
		    	LASTDATES=common.report.processingTime.showWeekLastDay(currentMonth).format('yyyy-MM-dd');		
	    	}else if(type=="DAYS"){
				FIRSTDATES=common.report.processingTime.showDayFirstDay(currentMonth);
				LASTDATES=common.report.processingTime.showDayFirstDay(currentMonth);
			}
			basics.tab.tabUtils.refreshTab(title,"../pages/reports!statReportRead.action?dto.fileName="+reportName+"&dto.firstDate="+FIRSTDATES+"&dto.lastDate="+LASTDATES+"&dto.showHide="+type+"&dto.reporti18n="+reporti18n+"&dto.confirm="+confirm);
		},
		/**
		 * 无参数查看
		 * @param title 选项卡标题
		 * @param reportName 报表名称
		 * @param confirm 是否HQL
		 */
		openReport:function(reportName,title,confirm){
			basics.tab.tabUtils.refreshTab(title,"../pages/reports!PurestatReport.action?dto.fileName="+reportName+"&dto.reporti18n="+reporti18n+"&dto.confirm="+confirm);
		},
		init:function(){
			
		}
	}
}();
$(document).ready(common.report.clickEvent.init);
