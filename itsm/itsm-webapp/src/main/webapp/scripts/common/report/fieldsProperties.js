$import('common.config.dictionary.dataDictionaryUtil');
$import('common.config.category.eventCategoryTree');
if(isCIHave){
	$import('itsm.cim.ciCategoryTree');
}
$import('common.knowledge.knowledgeTree');
$import('common.security.organizationTreeUtil');

/**  
 * @fileOverview "过滤器类型字段管理"
 * @author Van  
 * @constructor WSTO
 * @description 过滤器类型字段管理
 * @date 2011-06-13
 * @version 1.0  
 * @since version 1.0 
 */
var report_common_fields={
		
		'effectRange.dname':'label_sla_effectRange',
		'seriousness.dname':'label_sla_seriousness',
		'priority.dname':'label_sla_priority',
		'status.dname':'common_state',
		'createdBy.loginName':'requester'
		//'createdBy.orgnization.orgName':'title_user_org'查询出现未知错误
};


var report_comm_fields2={};

//if(isITSOPUser==true){//字段清楚不知道怎么查询
//	report_comm_fields2={'companyNo':'label_dynamicReports_companyNo'};
//}


$.extend(report_common_fields,report_comm_fields2);


var report_mutilGroup_fields={
		
		'com.wstuo.itsm.request.entity.Request':{
	
		'eno':'label_dynamicReports_requestNo',
		'slaState.dname':'title_request_SLAState',
		'effectRange.dname':'label_sla_effectRange',
		'seriousness.dname':'label_sla_seriousness',
		'priority.dname':'label_sla_priority',
		'status.dname':'common_state',
		'requestCategory.eventName':'category',
		'servicesOrg.orgName':'label_dynamicReports_request_serviceProvider',
		'owner.loginName':'common_owner',
		//'requestResolvedUser.loginName':'label_dynamicReports_request_resolveTechnician',
//		'ci.ciname':'CI',
		'technician.loginName':'title_technician'
	},
	
	'com.wstuo.itsm.domain.entity.Problems':{
		'eno':'label_dynamicReports_problemNo',
		'effectRange.dname':'label_sla_effectRange',
		'seriousness.dname':'label_sla_seriousness',
		'priority.dname':'label_sla_priority',
		'status.dname':'common_state',
	    'category.eventName':'category',
	    'problemNo':'problem_code',
	    'assigneeGroup.orgName':'label_dynamicReports_assignGroup'
//	    'knownError_knownError':'label_known_errors'
	},
	
	'com.wstuo.itsm.domain.entity.Changes':{
			'eno':'label_dynamicReports_changeNo',
			'effectRange.dname':'label_sla_effectRange',
			'seriousness.dname':'label_sla_seriousness',
			'priority.dname':'label_sla_priority',
			'status.dname':'common_state',
			'category.eventName':'category',
			'assigneeGroup.orgName':'label_dynamicReports_assignGroup'
	},
	//配置项类报表分组字段及统计字段
	'com.wstuo.itsm.domain.entity.CI':{
			
			'cino':'lable_ci_assetNo',
			'ciname':'label_dynamicReports_change_ciname',
			'model':'label_dynamicReports_model',
//			'poNo':'label_dynamicReports_change_orderNo',
			'status.dname':'label_dynamicReports_status',
			'brand.dname':'label_dynamicReports_brand',
			'loc.eventName':'label_dynamicReports_loc',
			'category.cname':'label_dynamicReports_category',
			'provider.dname':'label_dynamicReports_provider'				
//			'userName':'label_dynamicReports_userName',
//			'owner':'label_dynamicReports_owner'
	},
	
	
	//知识库类报表分组字段及统计字段
	'com.wstuo.itsm.knowledge.entity.KnowledgeInfo':{
			'category.eventName':'label_dynamicReports_category',
//			'serviceDirectoryItem.subServiceName':'label_dynamicReports_relatedService',
			'title':'common_title',
			'keyWords':'label_dynamicReports_knowledge_keyWord'
	}
		
}



var report_stistic_fields={
		
		'com.wstuo.itsm.request.entity.Request':{
			'eno':'label_request_all',
			'isFCR':'label_dynamicReports_request_isFRC',
			'isConvertdToProblem':'label_request_isConvertdToProblem',
			'isConvertdToChange':'label_request_isConvertdToChange',
			'hang':'label_request_hang'

				
				
		},
		
		'com.wstuo.itsm.domain.entity.Problems':{
			'eno':'label_dynamicReports_problemNo',
			'relatedConfigureItems.ciId':'relatedCI',
			'relatedRequests.eno':'report_mg_relatedrequest',
			'knownError':'report_mg_knowError',
			'problemsServiceList.eventId':'report_mg_serviceDirectory'


		},
		
		'com.wstuo.itsm.domain.entity.Changes':{
				'eno':'label_dynamicReports_changeNo',
				'cis.ciId':'relatedCI',
				'chanagesServiceList.eventId':'report_mg_serviceDirectory',
				'relatedRequest.eno':'report_mg_relatedrequest',
				'relatedProblems.eno':'report_mg_relatedproblem',
				'needApprove':'report_mg_needApprove'
		},
		//配置项类报表分组字段及统计字段
		'com.wstuo.itsm.domain.entity.CI':{
				'ciId':'label_dynamicReports_ciNo',
				'ciSoftwares.softwareId':'report_mg_ciSoftware'
				
		},
		
		
		//知识库类报表分组字段及统计字段
		'com.wstuo.itsm.knowledge.entity.KnowledgeInfo':{
				'kid':'label_dynamicReports_knowledgeNo',
				'serviceLists.scores':'report_mg_subServiceId',
				'create.userId':'title_creator'
				
		}
		
		
};


/**
 * 所有分组字段.
 */
var report_group_fields={
		
	//请求类报表分组字段及统计字段
	'com.wstuo.itsm.request.entity.Request':{
	
			'xgroupFields':$.extend({},report_common_fields,{
				'requestCategory.eventName':'category',
				'slaState.dname':'title_request_SLAState',
				'imode.dname':'label_sla_imode',
				'level.dname':'label_sla_level',
				'requestCode':'label_dynamicReports_requestNo',
				'servicesOrg.orgName':'label_dynamicReports_request_serviceProvider',
				'owner.loginName':'common_owner',
				'technician.fullName':'title_technician',
				'ci.ciname':'CI',
				'isFCR':'label_dynamicReports_request_isFRC',
				'assigneeGroup.orgName':'label_dynamicReports_assignGroup',
				'companyNo':'label_belongs_client'
			}),
			'xcountField':{
				'eno':'label_dynamicReports_requestNo'
			},
			'filterType':'request',
			'className':'label_dynamicReports_requestReport'
	},
	
	//问题类报表分组字段及统计字段
	'com.wstuo.itsm.domain.entity.Problems':{

		'xgroupFields':$.extend({},
				report_common_fields,{
			    'category.eventName':'category',
			    'problemNo':'problem_code',
			    'assigneeGroup.orgName':'label_dynamicReports_assignGroup'
			    //'knownError':'label_known_errors'
			    //'holdOn':'label_request_hang',
			    //'approval':'title_approverResult',
			    //'approverNum':'label_dynamicReports_change_approverCount'
			    
			   
			    	
				}),
			'xcountField':{
				'eno':'label_dynamicReports_problemNo'
			},
			'filterType':'problem',
			'className':'label_dynamicReports_problemReport'
	},
	
	//变更类报表分组字段及统计字段
	'com.wstuo.itsm.domain.entity.Changes':{
		
		'xgroupFields':$.extend({},report_comm_fields2,{
			
			
				'createdBy.loginName':'title_creator',
				'effectRange.dname':'label_sla_effectRange',
				'seriousness.dname':'label_sla_seriousness',
				'priority.dname':'label_sla_priority',
				'status.dname':'common_state',
				'technician.fullName':'title_technician',
				'category.eventName':'category',
				'assigneeGroup.orgName':'label_dynamicReports_assignGroup',
				'changeNo':'label_dynamicReports_change_code'
				}),
			'xcountField':{
				'eno':'label_dynamicReports_changeNo'
			},
			'filterType':'change',
			'className':'label_dynamicReports_changeReport'
	},
	
	//配置项类报表分组字段及统计字段
	'com.wstuo.itsm.domain.entity.CI':{

		'xgroupFields':$.extend({},report_comm_fields2,{
			
			'cino':'lable_ci_assetNo',
			'ciname':'label_dynamicReports_change_ciname',
			'model':'label_dynamicReports_model',
			//'poNo':'label_dynamicReports_change_orderNo',//出现未知错误
			'status.dname':'label_dynamicReports_status',
			'brand.dname':'label_dynamicReports_brand',
			'loc.eventName':'label_dynamicReports_loc',
			'category.cname':'label_dynamicReports_category',
			'provider.dname':'label_dynamicReports_provider'			
			//'userName':'label_dynamicReports_userName',//出现未知错误
			//'owner':'label_dynamicReports_owner'//出现未知错误
		}),
		'xcountField':{
			'ciId':'label_dynamicReports_ciNo'
		},
		'filterType':'ci',
		'className':'label_dynamicReports_ciReport'
	},
	
	
	//知识库类报表分组字段及统计字段
	'com.wstuo.itsm.knowledge.entity.KnowledgeInfo':{

		'xgroupFields':$.extend({},{
				'category.eventName':'label_dynamicReports_category',
				//'serviceDirectoryItem.subServiceName':'label_dynamicReports_relatedService',//出现未知错误
				'title':'common_title',
				'keyWords':'label_dynamicReports_knowledge_keyWord'
				}),
			'xcountField':{
				'kid':'label_dynamicReports_knowledgeNo'
			},			
			'filterType':'knowledge',
			'className':'label_dynamicReports_knowledgeReport'
	}
};







var common_options={};
if(isITSOPUser){		
	$.extend(common_options,{'companyNo_companyNo': 'label_belongs_client'});
}

//请求
var request_options=$.extend({},common_options,{
	//变量名_类型:字段名
	'requestCode_String':'number',
	'etitle_String':'common_title',
	'requestCategory.eventId_ReventId' : 'category',
	'requestStatus.dcode_dcode':'common_state',
	'slaState.dcode_dcode':'title_request_SLAState',
	'imode.dcode_dcode':'label_sla_imode',
	'level.dcode_dcode':'label_sla_level',
	'priority.dcode_dcode' : 'priority',
	'seriousness.dcode_dcode':'label_sla_seriousness',
	'effectRange.dcode_dcode':'label_sla_effectRange',
	'createdBy.loginName':'requester',
	'assigneeGroup.orgNo_orgNo' :  'title_request_assignToGroup',
	'technician.loginName_loginName' : 'title_request_assignToTC',
	'owner.loginName_loginName':'common_owner',
	'createdOn_Data' : 'common_createTime'
});

//问题
var problem_options=$.extend({},common_options,{
	//变量名_类型:字段名
	'problemNo_String':'problem_code',
	'etitle_String':'title',
	'category.eventId_PeventId' : 'category',
	'problemStatus.dcode_dcode':'status',
	'priority.dcode_dcode' : 'priority',
	'seriousness.dcode_dcode':'label_sla_seriousness',
	'effectRange.dcode_dcode': 'title_changeEffect',
	'createdBy.loginName_loginName' : 'problem_reporter',
	'assigneeGroup.orgNo_orgNo' : 'label_request_assignGroup',
	'technician.loginName_loginName' : 'title_request_assignTo',
	'createdOn_Data' : 'problem_reportTime',
	'overdueTime_Data':'label_overdue_time',
	'knownError_knownError':'label_known_errors'
});


//变更
var change_options=$.extend({},common_options,{
	//变量名_类型:字段名
	'changeNo_String':'number',
	'etitle_String':'title',
	'category.eventId_CeventId' :'category',
	'changeStatus.dcode_dcode':'status',
	'priority.dcode_dcode' : 'priority',
	'seriousness.dcode_dcode': 'label_sla_seriousness',
	'effectRange.dcode_dcode': 'label_sla_effectRange',
	'creator_loginName': 'title_creator',
	'technician.loginName_loginName' : 'title_technician',
	'assigneeGroup.orgNo_orgNo' : 'label_request_assignGroup',
	'createTime_Data' : 'common_createTime'
});

//配置项
var ci_options=$.extend({},common_options,{
	//变量名_类型:字段名
	'cino_String': 'lable_ci_assetNo',
	'ciname_String':'name',
	'category.cno_CIeventId' :'category',
	'department_orgNo':'ci_department',
	'model_String':'productType',
	'barcode_String':'barcode',
	'serialNumber_String':'serialNumber',
	'poNo_String':'purchaseNo',
	'sourceUnits_String':'ci_productProvider',
	'usePermissions_String':'ci_usePermission',
	'workNumber_String':'WorkNumber',
	'project_String':'Project',
	'CDI_String':'CDI',
	'financeCorrespond_Boolean':'ci_FIMapping',
	'useStatus.dcode_dcode':'status',
	'brand.dcode_dcode' : 'brands',
	'supplier.dcode_dcode':'supplier',
	'location.eventName_eventName':'location',
	'buyDate_Data' : 'purchaseDate',
	'wasteTime_Data' : 'ci_expiryDate',
	'borrowedTime_Data' : 'ci_lendTime',
	'recoverTime_Data' : 'ci_recycleTime',
	'expectedRecoverTime_Data' : 'ci_plannedRecycleTime',
	'arrivalDate_Data' : 'arrivalDate',
	'warningDate_Data' : 'warningDate',
	'assetsOriginalValue_Double' : 'ci_assetOriginalValue',
	'lifeCycle_Int' : 'lifeCycle',
	'warranty_Int' : 'warrantyDate',
	'userName_loginName' : 'use',
	'owner_loginName' : 'owner',
	'originalUser_loginName' : 'ci_originalUser',
	'systemPlatform.dcode_dcode' : 'label_systemPlatform'
});


//知识库
var knowledge_options=$.extend({},common_options,{
	//变量名_类型:字段名
	'serviceDirectoryItem.subServiceId_subServiceId':'label_knowledge_relatedService',
	'title_String':'title',
	'category.eventId_KeventId' : 'category',
	'addTime_Data' :'time',
});



var filter_type_arr={
		"com.wstuo.itsm.request.entity.Request":"request",
		"com.wstuo.itsm.domain.entity.Problems":"problem",
		"com.wstuo.itsm.domain.entity.Changes":"change",
		"com.wstuo.itsm.domain.entity.CI":"ci",
		"com.wstuo.itsm.knowledge.entity.KnowledgeInfo":"knowledge",
			
};

function openReportFilter(entityClass,pageId){
	
	
	
	if(entityClass==null || entityClass==""){
		
		msgAlert(i18n['label_dynamicReports_selectReportType'],'info');
		
	}else{
		
		var options={};
		if(entityClass=="com.wstuo.itsm.request.entity.Request"){
			
			options=request_options;
		}
		if(entityClass=="com.wstuo.itsm.domain.entity.Problems"){
						
			options=problem_options;
		}
		if(entityClass=="com.wstuo.itsm.domain.entity.Changes"){
			options=change_options;
		}
		if(entityClass=="com.wstuo.itsm.domain.entity.CI"){
			options=ci_options;
		}
		if(entityClass=="com.wstuo.itsm.knowledge.entity.KnowledgeInfo"){
			options=knowledge_options;
		}

		common.config.customFilter.filterGrid_Operation.openCustomFilterWinR(options,filter_type_arr[entityClass],entityClass,pageId);
	}

	
}


/**
 * 加载报表过滤器
 * @param reportId
 * @param filterNo
 * @param reportName
 * @param url
 */
function loadReportFilter(reportId,filterNo,filterClass,reportName,url){
	

	$('#report_preView_filterTable tbody').empty();
	windows('report_preView_window',{width:400});
	var _line='<tr><td style="text-align:center"><input name="customFilterDTO.expressions[_K].expId" type="hidden" value="{0}">{1}</td><td style="text-align:center">{2}</td><td style="text-align:center">{3}</td><td style="text-align:center">';
	
	$.post('filter!findFilterById.action?queryDTO.filterId='+filterNo,function(data){
		
		if(data!=null && data.expressions!=null){
			$.each(data.expressions,function(k,v){
				
				
				//replace('{display}','<input name="customFilterDTO.expressions[_K].propValue" value="{4}"/>')
				var _code=_line.replace(/_K/g,k).replace('{0}',v.expId).replace('{1}',i18n[v.propDisplayName]).replace('{2}',v.joinType).replace('{3}',v.operator).replace('{4}',v.propValue);
				_code+=loadReportValueHTML(k,v.expId,filterClass,v.propName,v.propValue,v.propDisplayValue);
				_code+='</td></tr>';
				$('#report_preView_filterTable tbody').append(_code);
			});
		};
	});

	$('#report_preView_window_submit').unbind().click(function(){
		var _param=$('#report_preView_window form').serialize();
		basics.tab.tabUtils.refreshTab(reportName,url+'?reportId='+reportId+'&'+_param);
	});
};









function loadReportValueHTML(k,expId,entityClass,propName,propValue,displayValue){
	
	
	
	var _valueId='report_filter_vl_'+k;
	var _viewId='report_filter_vw_'+k;
	
	//var _line='<tr><td style="text-align:center">{PP}&nbsp;||<input name="customFilterDTO.expressions[_K].expId" type="hidden" value="{0}">{1}</td><td style="text-align:center">{2}</td><td style="text-align:center">{3}</td><td style="text-align:center"><input name="customFilterDTO.expressions[_K].propValue" value="{4}"/></td></tr>';
	
	if(propName.indexOf(".eventId")!=-1){
		
		var _line="<input id='"+_viewId+"' value='"+displayValue+"' style='width:96%;cursor:pointer;color:#555555' onclick=selectCategoryReport('#"+_valueId+"','#"+_viewId+"','"+entityClass+"') readonly/><input name='customFilterDTO.expressions["+k+"].propValue' type='hidden' id='"+_valueId+"' value='"+propValue+"'/>";
		return _line;
	}
	
	if(propName.indexOf(".dcode")!=-1){
		
		var flag='';

		if(propName=="status.dcode" && entityClass=="com.wstuo.itsm.request.entity.Request"){
			flag='requestStatus';
		}
		
		if(propName=="status.dcode" && entityClass=="com.wstuo.itsm.domain.entity.Problems"){
			flag='problemStatus';
		}
		
		if(propName=="status.dcode" && entityClass=="com.wstuo.itsm.domain.entity.Changes"){
			flag='changeStatus';
		}
		
		if(propName=="slaState.dcode"){
			flag='SLAStatus';
		}
		
		if(propName=="imode.dcode"){
			flag='imode';
		}
		if(propName=="level.dcode"){
			flag='level';
		}
		if(propName=="priority.dcode"){
			flag='priority';
		}
		if(propName=="seriousness.dcode"){
			flag='seriousness';
		}
		if(propName=="effectRange.dcode"){
			flag='effectRange';
		}

		var _line="<select id='"+_valueId+"' name='customFilterDTO.expressions["+k+"].propValue' style='width:150px'></select>";
		
		
		common.config.dictionary.dataDictionaryUtil.loadOptionsByCode(flag,'#'+_valueId);
		
		setTimeout(function(){
			
			$('#'+_valueId).val(propValue);
		},1000);
		
		return _line;
	}
	
	
	if(propName=='createdBy.loginName'){
		var _line="<input id='"+_viewId+"' value='"+displayValue+"' style='width:96%;cursor:pointer;color:#555555' onclick=common.security.userUtil.selectUser('#"+_viewId+",#"+_valueId+"','','','loginName','"+companyNo+"') readonly/><input name='customFilterDTO.expressions["+k+"].propValue' type='hidden' id='"+_valueId+"' value='"+propValue+"'/>"
		return _line;
	}
	
	
	
	if(propName.indexOf(".loginName")!=-1 || propName=='creator'){
		var _line="<input id='"+_viewId+"' value='"+displayValue+"' style='width:96%;cursor:pointer;color:#555555' onclick=common.security.userUtil.selectUser('#"+_viewId+"','#"+_valueId+"','','loginName','"+companyNo+"') readonly/><input name='customFilterDTO.expressions["+k+"].propValue' type='hidden' id='"+_valueId+"' value='"+propValue+"'/>"
		return _line;
	}
	
	
	
	if(propName.indexOf(".orgNo")!=-1){
		var _line="<input id='"+_viewId+"' value='"+displayValue+"' style='width:96%;cursor:pointer;color:#555555' onclick=common.security.organizationTreeUtil.selectORG('#"+_viewId+"','#"+_valueId+"','"+companyNo+"') readonly/><input name='customFilterDTO.expressions["+k+"].propValue' type='hidden' id='"+_valueId+"' value='"+propValue+"'/>";
		return _line;
	}
	
	if(propName=='companyNo'){
		var _line="<input id='"+_viewId+"' value='"+displayValue+"' style='width:96%;cursor:pointer;color:#555555' onclick=itsm.itsop.selectCompany.openSelectCompanyWin('#"+_valueId+"','#"+_viewId+"','') readonly/><input name='customFilterDTO.expressions["+k+"].propValue' type='hidden' id='"+_valueId+"' value='"+propValue+"'/>";
		return _line;
	}
	
	
	 
	
	return "<input id='"+_valueId+"' value='"+propValue+"' name='customFilterDTO.expressions["+k+"].propValue' style='width:96%' />";
	
};




/**
 * 打开下拉框。
 * @param putID
 * @param putName
 * @param entityClass
 * @returns {Boolean}
 */
function selectCategoryReport(putID,putName,entityClass){

	if(entityClass=='com.wstuo.itsm.request.entity.Request'){
		common.config.category.eventCategoryTree.requestCategory(putName,putID);
		return false;
	}
	
	if(entityClass=='com.wstuo.itsm.domain.entity.Problems'){
		common.config.category.eventCategoryTree.problemCategory(putName,putID);
		return false;
	}
	if(entityClass=='com.wstuo.itsm.domain.entity.Changes'){
		common.config.category.eventCategoryTree.changeCategory(putName,putID);
		return false;
	}
	if(entityClass=='com.wstuo.itsm.domain.entity.CI'){
		putID=putID.replace('#','');
		putName=putName.replace('#','');
		itsm.cim.ciCategoryTree.configureItemTree('search_ciCategoryTreeDiv','search_ciCategoryTreeTree',putID,putName,'false');
		return false;
	}
	if(entityClass=='com.wstuo.itsm.knowledge.entity.KnowledgeInfo'){
	    common.knowledge.knowledgeTree.selectKnowledgeCategory('#knowledge_category_select_window','#knowledge_category_select_tree',putName,putID);
		return false;
	}

}


function showFilterData(filterSlectId,entityClassId){
	
	var filterId=$(filterSlectId).val();
	var entityClass=$(entityClassId).val();

	
	if(filterId!=null && filterId!='' && filterId!='-1' && entityClass!=null && entityClass!=''){
		
		
		if(entityClass=="com.wstuo.itsm.request.entity.Request"){
			
			//打开或选择对应的页面并显示
			basics.tab.tabUtils.openTabCallback(i18n['title_request_requestGrid'],'../pages/itsm/request/requestMain.jsp?filterId='+filterId,function(){					
				var _url="request!findPagerRequestByCustomFilter.action";
				var postData = $("#requestGrid").jqGrid("getGridParam", "postData");     					
				$.extend(postData,{'requestQueryDTO.filterId':filterId});
				$('#requestGrid').jqGrid('setGridParam',{page:1,url:_url,postData:postData}).trigger('reloadGrid');
			});
			
		}
		if(entityClass=="com.wstuo.itsm.domain.entity.Problems"){
				
			//打开或选择对应的页面并显示
			basics.tab.tabUtils.openTabCallback(i18n['problem_grid'],'../pages/itsm/problem/problemMain.jsp?filterId='+filterId,function(){					
				var _url="problem!findProblemPagerByCustomFilter.action";
				var postData = $("#problemGrid").jqGrid("getGridParam", "postData");     					
				$.extend(postData, {'queryDTO.filterId':filterId});
				$('#problemGrid').jqGrid('setGridParam',{page:1,url:_url,postData:postData}).trigger('reloadGrid');
			});	
			
		}
		if(entityClass=="com.wstuo.itsm.domain.entity.Changes"){
			
			//打开或选择对应的页面并显示
			basics.tab.tabUtils.openTabCallback(i18n['title_changeList'],'../pages/itsm/change/changeMain.jsp?filterId='+filterId,function(){
				var _url="change!findPagerChangeByCustomFilter.action";
				var postData = $("#changeGrid").jqGrid("getGridParam", "postData");  
				$.extend(postData, {'queryDTO.filterId':filterId});			
				$('#changeGrid').jqGrid('setGridParam',{page:1,url:_url,postData:postData}).trigger('reloadGrid');
			});
			
			
		}
		if(entityClass=="com.wstuo.itsm.domain.entity.CI"){
			
			//打开或选择对应的页面并显示
			basics.tab.tabUtils.openTabCallback(i18n['ci_configureItemAdmin'],'../pages/itsm/cim/configureItem.jsp?filterId='+filterId,function(){
				var _url=url='ci!findCItemsByCustomFilter.action?ciQueryDTO.filterId='+filterId;
				var postData = $("#configureGrid").jqGrid("getGridParam", "postData");  
				$('#configureGrid').jqGrid('setGridParam',{page:1,url:_url,postData:postData}).trigger('reloadGrid');
			});
			
		}
		if(entityClass=="com.wstuo.itsm.knowledge.entity.KnowledgeInfo"){
			
			//打开或选择对应的页面并显示
			basics.tab.tabUtils.openTabCallback(i18n['title_request_knowledgeGrid'],'../pages/common/knowledge/knowledgeMain.jsp?filterId='+filterId,function(){
				var _url="knowledgeInfo!findKnowledgesPagerByCustomFilter.action?knowledgeQueryDto.filterId="+filterId;
				var postData = $("#knowledgeGrid").jqGrid("getGridParam", "postData");  
				$('#knowledgeGrid').jqGrid('setGridParam',{page:1,url:_url,postData:postData}).trigger('reloadGrid');
			});
		}
	
	}else{
		
		msgAlert(i18n['rule_report_chooseFilter'],'info');
	}

	
}



