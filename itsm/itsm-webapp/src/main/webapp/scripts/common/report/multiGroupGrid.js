/**  
 * @author Van  
 * @constructor WSTO
 * @description 自定义报表.
 * @date 2010-11-17
 * @since version 1.0 
 */ 
$package('common.report');


$import('common.config.customFilter.filterGrid_Operation');
$import('common.report.multiGroupStisticGrid');
$import('common.report.fieldsProperties');

/**  
 * @fileOverview "多项报表管理"
 * @author Van  
 * @constructor WSTO
 * @description 多项报表管理
 * @date 2011-06-13
 * @version 1.0  
 * @since version 1.0 
 */
common.report.multiGroupGrid=function(){
	
	
	this.loadShareGroupGrid=false;
	this.mutilGroupOperation='save';
	
	this.multiGroupGrid_jsp_loadStisticFlag=false;
	this.module=false;
	
	return {

		
		
		
		/**
		 * 显示列表格式.
		 */
		actionFormatter:function(cell,event,data){
		
			if(data.customFilterName!=null && data.customFilterNo!=null){

				//加载过滤器数据
				//return "<a href=\"javascript:loadReportFilter('"+data.reportId+"','"+data.customFilterNo+"','"+data.entityClass+"','"+data.reportName+"','mutilreport!show.action')\">"+i18n['label_dynamicReports_viewReport']+"</a>";	
				return "<a href=\"javascript:basics.tab.tabUtils.refreshTab('"+data.reportName+"','mutilreport!show.action?reportId="+data.reportId+"')\">"+i18n['label_dynamicReports_request_show']+"</a>"+"&nbsp;&nbsp;<a href=\"javascript:basics.tab.tabUtils.refreshTab('"+data.reportName+"','mutilreport!showdynamic.action?reportId="+data.reportId+"&width=600')\">"+i18n['View_Portal_effect']+"</a>";
			}else{//直接预览
				return "<a href=\"javascript:basics.tab.tabUtils.refreshTab('"+data.reportName+"','mutilreport!show.action?reportId="+data.reportId+"')\">"+i18n['label_dynamicReports_request_show']+"</a>"+"&nbsp;&nbsp;<a href=\"javascript:basics.tab.tabUtils.refreshTab('"+data.reportName+"','mutilreport!showdynamic.action?reportId="+data.reportId+"&width=600')\">"+i18n['View_Portal_effect']+"</a>";
			}
		
	
		},
		
		
		/**
		 * @description 显示报表
		 * @param cell 当前列值
		 * @param event 事件
		 * @param data 行数据
		 */
		calssNameFormatter:function(cell,event,data){
			return i18n[report_group_fields[cell]['className']];
		},
		
		
		/**
		 * 加载列表.
		 */
		showGrid:function(){
		
			var params = $.extend({},jqGridParams, {	
				url:'mutilreport!findPager.action',
				colNames:[i18n['label_dynamicReports_reportName'],i18n['title'],i18n['label_dynamicReports_reportType'],
				          i18n['title_creator'],i18n['title_updator'],i18n['common_createTime'],i18n['common_updateTime'],i18n['custom_report_previewReport'],'','','','','','','',''],
				colModel:[
			   		{name:'reportName',align:'center',width:30},
			   		{name:'title',align:'center',width:30,},
			   		{name:'entityClass',align:'center',width:20,formatter:common.report.multiGroupGrid.calssNameFormatter},
			   		{name:'creator',align:'center',width:30},
			   		{name:'lastUpdater',align:'center',width:30},
			   		{name:'createTime',align:'center',width:25},
			   		{name:'lastUpdateTime',align:'center',width:25},
			   		{name:'act', align:'center',width:30,sortable:false,formatter:common.report.multiGroupGrid.actionFormatter},
			   		{name:'reportId',hidden:true},
			   		{name:'entityClass',hidden:true},
			   		{name:'customFilterName',hidden:true},
			   		{name:'customFilterNo',hidden:true},
			   		{name:'groupField',hidden:true},
			   		{name:'reportModule',hidden:true},
			   		{name:'reportSharing',hidden:true},
			   		{name:'type',hidden:true}
			   	],		
				jsonReader: $.extend(jqGridJsonReader, {id: "reportId"}),
				sortname:'reportId',
				pager:'#multiGroupGridPager'
			});
			$("#multiGroupGrid").jqGrid(params);
			$("#multiGroupGrid").navGrid('#multiGroupGridPager',navGridParams);
			//列表操作项
			$("#t_multiGroupGrid").css(jqGridTopStyles);
			$("#t_multiGroupGrid").append($('#multiGroupGridToolbar').html());

		},
		
		/**
		 * 打开添加窗口.
		 */
		add_open_window:function(){
			//清空
			$('#add_edit_mutilGroupReport_customFilterNo').html('<option value="">-- '+i18n['label_dynamicReports_chooseFilter']+' --</option>');
			$('#add_edit_mutilGroupReport_entityClass_groupField').html('<option value="">-- '+i18n['label_dynamicReports_chooseGroupField']+' --</option>');
			//禁止选择其它
			$('#add_edit_mutilGroupReport_entityClass').unbind('change');
			$('#add_edit_mutilGroupReport_entityClass').change(common.report.multiGroupGrid.reportTypeChange);
			
			$('#multiEntityClass').show();
			$('#add_edit_mutilGroupReport_entityClass').attr("disabled","");
			/*$('#add_edit_mutilGroupReport_creator').val(loginName);	//添加创建者
			$('#add_edit_mutilGroupReport_lastUpdater').val(loginName);	//添加更新者
*/			module=false;
			$('#mutilGroupReport_module').show();
			resetForm('#multiGroupGrid_add_edit_window form');
			$('#add_edit_mutilGroupReport_reportId').val('');
			//$('#add_edit_mutilGroupReport_entityClass').attr("disabled","");
			windows('multiGroupGrid_add_edit_window',{modal: false});
			$("#mutil_shareGroupGrid_sign").val('');//报表共享组标记 will
			$('#selectShareGroupReportMutil').val('');
			mutilGroupOperation='save';
			if(multiGroupGrid_jsp_loadStisticFlag==false){
				multiGroupGrid_jsp_loadStisticFlag=true;
				common.report.multiGroupStisticGrid.loadGrid("-1");
				common.report.multiGroupStisticGrid.refreshGrid();
			}else{
				common.report.multiGroupStisticGrid.refreshGrid();
			}
		},
		
		/**
		 * 添加自定义报表.
		 */
		add:function(){
			startProcess();
			var staticsIds = $("#multiGroupStisticGrid").getDataIDs();
			if($('#multiGroupGrid_add_edit_window form').form('validate')){
				if(mutilGroupOperation=='save')
					$('#mutilGrouplastUpdater').val('');
				else
					$('#mutilGrouplastUpdater').val($('#mutilGroupcreator').val());
				
				//标题
				var groupFieldName=$('#add_edit_mutilGroupReport_entityClass_groupField').find("option:selected").text();
				$('#add_edit_mutilGroupReport_groupFieldName').val(groupFieldName);
				var boo=$("#reportModule_checkbox_multi input[type='checkbox']").is(':checked');
				$('#reportModule_checkbox_multi div').html('');
				if(boo){	
					$('#reportModule_checkbox_multi div').append('<input name="mutilGroupReportDTO.reportType" type="hidden" value="mutilGroup" />');
					$('#reportModule_checkbox_multi div').append('<input name="mutilGroupReportDTO.reportModule" type="hidden" value="true" />');
				}else{
					$('#reportModule_checkbox_multi div').append('<input name="mutilGroupReportDTO.reportType" type="hidden" value="false" />');
					$('#reportModule_checkbox_multi div').append('<input name="mutilGroupReportDTO.reportModule" type="hidden" value="" />');
				}
				if($("#multilGroupReport_Sharing_group").attr("checked")){
					var ids = $("#mutil_shareGroupGrid").getDataIDs();
					if(ids==""){
						msgShow(i18n['report_not_group_msg'],'show');
						return false;
					}
					$('#selectShareGroupReportMutil').val(ids);
				}else{
					$('#selectShareGroupReportMutil').val('');
				}
				var _param = $('#multiGroupGrid_add_edit_window form').serialize();
				var _paramStaticsIds = $.param({'mutilGroupReportDTO.stisticIds':staticsIds},true);
				$.post('mutilreport!'+mutilGroupOperation+'.action',_param+'&'+_paramStaticsIds,function(data){	
					endProcess();
					$('#multiGroupGrid').trigger('reloadGrid');
					$('#multiGroupGrid_add_edit_window').dialog('close');
					common.report.clickEvent.loadReportModuleLeftMenu();
					msgShow(i18n['label_dynamicReports_addEditSuccessful'],'show');
				});				
			}else{
				endProcess();
			}
		},
		
		/**
		 * 编辑.
		 */
		edit_open_window:function(){
			checkBeforeEditGrid('#multiGroupGrid', function(data){
				
				$('#multiEntityClass').show();
				$('#add_edit_mutilGroupReport_entityClass').removeClass('validatebox-invalid');
				$('#add_edit_mutilGroupReport_entityClass').attr("disabled","true");
				resetForm('#multiGroupGrid_add_edit_window form');
				//$('#add_edit_mutilGroupReport_entityClass').attr("disabled","disabled");
				
				$('#add_edit_mutilGroupReport_entityClass').val($vl(data.entityClass));
				if($vl(data.entityClass)=="com.wstuo.itsm.knowledge.entity.KnowledgeInfo"){
					$('#mutilGroupReport_module').hide();
					module=true;
				}
				if(module&&$vl(data.entityClass)!="com.wstuo.itsm.knowledge.entity.KnowledgeInfo"){
					module=false;
					$('#mutilGroupReport_module').show();
				}
				$('#add_edit_mutilGroup_reportModule').val($vl(data.reportModule));
				$('#add_edit_mutilGroupReport_reportName').val($vl(data.reportName));
				$('#add_edit_mutilGroupReport_title').val($vl(data.title));
				$('#add_edit_mutilGroupReport_reportId').val($vl(data.reportId));
				$('#add_edit_mutilGroupReport_lastUpdater').val(loginName);	//添加更新者
				var bo=Boolean($vl(data.reportModule));
				$('#checkbox_multi_add').attr('checked',bo);
				//禁止选择其它
				$('#add_edit_mutilGroupReport_entityClass').change(function (){
					$('#add_edit_mutilGroupReport_entityClass').val($vl(data.entityClass)); 
				});

				var reportSharing=data.reportSharing;
				if(reportSharing=="share")
					$('#multilGroupReport_Sharing').attr("checked",'true');
				else if(reportSharing.substr(0,5)=="name_")
					$('#multilGroupReport_private').attr("checked",'true');
				else
					$('#multilGroupReport_Sharing_group').attr("checked",'true');
				
				common.report.multiGroupGrid.reportTypeChange();
				
				//打开窗口
				mutilGroupOperation='merge';
				windows('multiGroupGrid_add_edit_window',{modal: false});
				$("#mutil_shareGroupGrid_sign").val("");//报表共享组标记 will
				setTimeout(function(){//延迟加载
					if($vl(data.customFilterNo)!=0){
						$('#add_edit_mutilGroupReport_customFilterNo').val($vl(data.customFilterNo));
					}
					$('#add_edit_mutilGroupReport_entityClass_groupField').val($vl(data.groupField));
					common.report.multiGroupStisticGrid.showGrid();

				},800);
				
			});
		},
		
		/**
		 * 删除报表.
		 */
		del:function(){
			
			checkBeforeDeleteGrid('#multiGroupGrid', function(ids){
				
				var _param = $.param({'reportIds':ids},true);
				$.post("mutilreport!delete.action", _param, function(){
					common.report.clickEvent.loadReportModuleLeftMenu();
					$('#multiGroupGrid').trigger('reloadGrid');
					msgShow(i18n['msg_deleteSuccessful'],'show');
					
				}, "json");
				
				
			});
			
		},
		
		/**
		 * 选择过滤器
		 */
		reportTypeChange:function(){
			var entityClass=$('#add_edit_mutilGroupReport_entityClass').val();
			if(entityClass=="com.wstuo.itsm.knowledge.entity.KnowledgeInfo"){
				$('#mutilGroupReport_module').hide();
				module=true;
			}
			if(module&&entityClass!="com.wstuo.itsm.knowledge.entity.KnowledgeInfo"){
				module=false;
				$('#mutilGroupReport_module').show();
			}
			
			//清空
			$('#add_edit_mutilGroupReport_customFilterNo').html('<option value="">-- '+i18n['label_dynamicReports_chooseFilter']+' --</option>');
			if(entityClass!=null && entityClass!=''){
				
				var filterType=report_group_fields[entityClass]['filterType'];
				
				//加载过滤器
				common.config.customFilter.filterGrid_Operation.loadFilterByModule("#add_edit_mutilGroupReport_customFilterNo",filterType);
				//加载统计字段
				common.report.multiGroupStisticGrid.loadStisticField(entityClass);
				//查询对应的数据.
				common.report.multiGroupStisticGrid.queryByEntityClass(entityClass);
				//加载分组字段
				$('#add_edit_mutilGroupReport_entityClass_groupField').html('<option value="">-- '+i18n['label_dynamicReports_chooseGroupField']+' --</option>');
				$.each(report_mutilGroup_fields[entityClass],function(k,v){
					$('<option value="'+k+'">'+i18n[v]+'</option>').appendTo('#add_edit_mutilGroupReport_entityClass_groupField');
				});
			}else{
				common.report.multiGroupStisticGrid.queryByEntityClass('NO');
				//加载分组字段
				$('#add_edit_mutilGroupReport_entityClass_groupField').html('<option value="">-- '+i18n['label_dynamicReports_chooseGroupField']+' --</option>');
			}
			
			
		},
		
		/**
		 * 打开搜索窗口.
		 */
		search_open_window:function(){
			windows('multiGroupGrid_search_window',{width:320,modal: false});
		},
		
		/**
		 * 执行搜索.
		 */
		search:function(){

			var _search_data = $('#multiGroupGrid_search_window form').getForm();
			var _postData = $("#multiGroupGrid").jqGrid("getGridParam", "postData");       
			$.extend(_postData, _search_data);	
			$('#multiGroupGrid').trigger('reloadGrid',[{"page":"1"}]);
			
		},
		
		/**
		 * 快速搜索.
		 */
		searchByEntityClass:function(){
			
			resetForm('#multiGroupGrid_search_window form');//重置搜索表单
			$('#multiGroupGrid_search_entityClass').val($('#mutilGroupGrid_searchByEntityClass').val());
			common.report.multiGroupGrid.search();
		},
		
		

		/**
		 * 显示共享组列表
		 * */
		showShareGroup:function(){
			windows('mutil_shareGroupWindow',{width:500,height:450,close:function(){
				$("#multilGroupReport_Sharing_group").attr("checked","checked");
			}});
			//$('#singleGroupReport_Sharing_group').attr('checked','');
			var groupId=$('#add_edit_mutilGroupReport_reportId').val();
			if(!basics.ie6.htmlIsNull("#mutil_shareGroupGrid")){
				common.report.multiGroupGrid.showGridAction(groupId);
				$("#mutil_shareGroupGrid_sign").val("mutil_shareGroupGrid_sign");
			}else if($("#mutil_shareGroupGrid_sign").val()===''){
				$("#mutil_shareGroupGrid_sign").val("mutil_shareGroupGrid_sign");
				if (groupId != null && groupId != '') {
					var postData = $("#mutil_shareGroupGrid").jqGrid("getGridParam", "postData");       
					$.extend(postData, {"reportId":groupId});  //将postData中的查询参数覆盖为空值
					$('#mutil_shareGroupGrid').trigger('reloadGrid');
				} else {
				    $('#mutil_shareGroupGrid').jqGrid("clearGridData");
				}
			}
			
		},
		
		/**
		 * 查询共享组列表
		 * */
		showGridAction:function(groupId){
			
			
			var _postData={};
			
			if(groupId!=null && groupId!=''){
				_postData={'reportId':groupId}
			}
			
			var params = $.extend({},jqGridParams, {	
				url:'mutilreport!findShareGroups.action',
				//caption:'分享组',
				postData:_postData,
				colNames:['ID',i18n['name']],
				colModel:[{name:'orgNo',width:100,align:'center',sortable:false},
				          {name:'orgName',width:300,align:'center',sortable:false}
						  ],
				jsonReader: $.extend(jqGridJsonReader, {id: "orgNo"}),
				sortname:'orgNo',
				autowidth:true,
				rowNum:10000
			});
			
			$("#mutil_shareGroupGrid").jqGrid(params);
			$("#mutil_shareGroupGrid").navGrid('#mutil_shareGroupGridPager',navGridParams);
			//列表操作项
			$("#t_mutil_shareGroupGrid").css(jqGridTopStyles);
			$("#t_mutil_shareGroupGrid").html($('#mutil_shareGroupGridToolbar').html());
			
			$('#mutil_shareGroupGrid_save').unbind().click(common.config.customFilter.shareGroup.save);
			$('#mutil_shareGroupGrid_add').unbind().click(function(){common.config.customFilter.shareGroup.showSelectWindow("mutil_");});
			$('#mutil_shareGroupGrid_remove').unbind().click(function(){common.config.customFilter.shareGroup.removeShareGroups("mutil_");});
		},
		
		
		
		
		
		
		init:function(){
			$('.loading').hide();
			$('.content').show();
			setTimeout(function(){					
				common.report.multiGroupGrid.showGrid();common.report.multiGroupStisticGrid.showGrid();
				//移除事件
				$('#multiGroupGrid_add,#multiGroupGrid_edit,#multiGroupGrid_delete,#multiGroupGrid_search,#multiGroupGrid_doSearch,#multiGroupGrid_searchByEntityClass,#add_edit_mutilGroupReport_entityClass,#multiGroupGrid_save').unbind();
			
				
				
				$('#multiGroupGrid_add').click(common.report.multiGroupGrid.add_open_window);
				$('#multiGroupGrid_edit').click(common.report.multiGroupGrid.edit_open_window);
				$('#multiGroupGrid_delete').click(common.report.multiGroupGrid.del);
				$('#multiGroupGrid_search').click(common.report.multiGroupGrid.search_open_window);
				$('#multiGroupGrid_doSearch').click(common.report.multiGroupGrid.search);
				$('#mutilGroupGrid_searchByEntityClass').change(common.report.multiGroupGrid.searchByEntityClass);
				$('#add_edit_mutilGroupReport_entityClass').change(common.report.multiGroupGrid.reportTypeChange);
				$('#multiGroupGrid_save').click(common.report.multiGroupGrid.add);//执行保存
				
				$('#add_edit_mutilGroupReport_addCustomFilter').click(function(){
					
					openReportFilter($("#add_edit_mutilGroupReport_entityClass").val(),"add_edit_mutilGroupReport_customFilterNo");
				});
				
				

			},0);
			
			$('#multilGroupReport_Sharing_group').click(common.report.multiGroupGrid.showShareGroup);
		}
		
	};
	
}();

$(document).ready(common.report.multiGroupGrid.init);