$package('common.report');

/**  
 * @fileOverview "报表时间处理"
 * @author Martin  
 * @constructor Martin
 * @description 报表时间处理
 * @date 2014-09-13
 * @version 1.0  
 * @since version 1.0 
 */
common.report.processingTime=function(){
	/** 时间格式化*/ 
	Date.prototype.format = function(format)
	{
	    var o =
	    { 
	        "M+" : this.getMonth()+1,    //month
	        "d+" : this.getDate(),       //day 
	        "h+" : this.getHours(),      //hour
	        "m+" : this.getMinutes(),    //minute
	        "s+" : this.getSeconds(),    //second
	        "q+" : Math.floor((this.getMonth()+3)/3),  //quarter
	        "S" : this.getMilliseconds() //millisecond
	    }
	    if(/(y+)/.test(format))
	    format=format.replace(RegExp.$1,(this.getFullYear()+"").substr(4 - RegExp.$1.length));
	    for(var k in o)
	    if(new RegExp("("+ k +")").test(format))
	    format = format.replace(RegExp.$1,RegExp.$1.length==1 ? o[k] : ("00"+ o[k]).substr((""+ o[k]).length));
	    return format;
	}
	this.currentMonth=(new Date().format('yyyy-MM-dd'));
	this.FIRSTDATE="";
	this.LASTDATE="";
	return{
		/** 处理上下周，上下月*/
		openReportUpAndDownTimeFormat:function(type,opt,firstDates){
			if(type=="MONTHS"&&opt=="next"){ 
				var Nowdate   =   new   Date(Date.parse(firstDates.replace(/-/g,   "/"))); 
				var v=Nowdate.getMonth()+1; 
				Nowdate.setMonth(v);  
				var now=Nowdate.format('yyyy-MM-dd'); 
				FIRSTDATE=common.report.processingTime.showMonthFirstDay(now);
				LASTDATE=common.report.processingTime.showMonthLastDay(now); 
			}else if(type=="MONTHS"&&opt=="m"){ 
				var Nowdate   =   new   Date(Date.parse(firstDates.replace(/-/g,   "/"))); 
				var v=Nowdate.getMonth()-1;  
				Nowdate.setMonth(v);  
				var now=Nowdate.format('yyyy-MM-dd');
				FIRSTDATE=common.report.processingTime.showMonthFirstDay(now); 
				LASTDATE=common.report.processingTime.showMonthLastDay(now);
			}else if(type=="WEEKS"){
				if(type=="WEEKS"&&opt=="next"){
					var Nowdate = new Date(Date.parse(firstDates.replace(/-/g, "/")));  /**根据当前星期一处理上下周*/
					Nowdate.setDate(Nowdate.getDate()+7) ;
					var now=Nowdate.format('yyyy-MM-dd'); 
					FIRSTDATE=common.report.processingTime.showWeekFirstDay(now).format('yyyy-MM-dd');
					LASTDATE=common.report.processingTime.showWeekLastDay(now).format('yyyy-MM-dd'); 
				}else if(type=="WEEKS"&&opt=="w"){   
					var Nowdate = new Date(Date.parse(firstDates.replace(/-/g, "/"))); 
					Nowdate.setDate(Nowdate.getDate()-7) ; 
					var now=Nowdate.format('yyyy-MM-dd');  
					
					FIRSTDATE=common.report.processingTime.showWeekFirstDay(now).format('yyyy-MM-dd');  
					LASTDATE=common.report.processingTime.showWeekLastDay(now).format('yyyy-MM-dd');
				}  
			}else if(type=="YEARS"){
				if(type=="YEARS"&&opt=="next"){
					var Nowdate   =   new   Date(Date.parse(firstDates.replace(/-/g, "/")));  /**根据当前年处理上下年*/
					Nowdate.setFullYear(Nowdate.getFullYear()+1) ;
					var now=Nowdate.format('yyyy-MM-dd'); 
					FIRSTDATE=common.report.processingTime.showYearFirstDay(now);
					LASTDATE=common.report.processingTime.showYearLastDay(now); 
				}else if(type=="YEARS"&&opt=="y"){   
					var Nowdate   =   new   Date(Date.parse(firstDates.replace(/-/g, "/"))); 
					Nowdate.setFullYear(Nowdate.getFullYear()-1) ; 
					var now=Nowdate.format('yyyy-MM-dd'); 
					FIRSTDATE=common.report.processingTime.showYearFirstDay(now);  
					LASTDATE=common.report.processingTime.showYearLastDay(now);
				}  
			}
		},
		/**
		 * 本周第一天
		 */
		showWeekFirstDay:function(time) {   
			var Nowdate = new Date(Date.parse(time.replace(/-/g, "/")));   
			var WeekFirstDay = new Date(Nowdate-(Nowdate.getDay())*86400000); 
			return WeekFirstDay;
		},

		/**
		 * 本周第七天 
		 */
		showWeekLastDay:function(time) {   
			var Nowdate = new Date(Date.parse(time.replace(/-/g, "/"))); 
			var WeekFirstDay=new Date(Nowdate-(Nowdate.getDay())*86400000); 
			var WeekLastDay=new Date((WeekFirstDay/1000+6*86400)*1000);  
			return WeekLastDay; 
		},
		 
		/**
		 * 本月第一天
		 */
		showMonthFirstDay:function(time) {    
		    var myDate = new Date(Date.parse(time.replace(/-/g,"/")));
		    var year = myDate.getFullYear();
		    var month = myDate.getMonth()+1;
		    if (month<10)
		        month = "0"+month;
		    var firstDay =year+"-"+month+"-01";
		    return firstDay;
		},

		/**
		 * 本月最后一天
		 */ 
		showMonthLastDay:function(time) {    
		    var myDate = new Date(Date.parse(time.replace(/-/g, "/")));
		    var year = myDate.getFullYear();
		    var month = myDate.getMonth()+1;
		    if (month<10)
		        month = "0"+month;
		    myDate = new Date(year,month,0);
		    var lastDay = year+"-"+month+"-"+myDate.getDate();
		    return lastDay;
		},
		
		/**
		 * 本年第一天
		 */
		showYearFirstDay:function(time) {    
		    var myDate = new Date(Date.parse(time.replace(/-/g,"/")));
		    var year = myDate.getFullYear();
		    var firstDay =year+"-01-01";
		    return firstDay;
		},
		/**
		 * 本年最后一天
		 */
		showYearLastDay:function(time) {    
		    var myDate = new Date(Date.parse(time.replace(/-/g, "/")));
		    var year = myDate.getFullYear();
		    var lastDay = year+"-12-31";
		    return lastDay;
		},
		/**
		 * 当天
		 */
		showDayFirstDay:function(time) {    
			var myDate = new Date(Date.parse(time.replace(/-/g, "/")));
			//获取本年
		    var year = myDate.getFullYear();
		    //获取本月
		    var month = myDate.getMonth()+1;
		    if (month<10)
		        month = "0"+month;
		    //获取本日
		    var day = myDate.getDate();
		    if (day<10)
		    	day = "0"+day;
		    var firstDay = year+"-"+month+"-"+day;
		    return firstDay;
		},
		/**
		 * 明天
		 */
		showDayLastDay:function(time) {
			
			var myDate = new Date(Date.parse(time.replace(/-/g, "/")));
			//获取本年
		    var year = myDate.getFullYear();
		    //获取本月
		    var month = myDate.getMonth()+1;
		    var dd = new Date();
		    var sum= new Date(dd.getFullYear(), dd.getMonth()+1,0).getDate();
		    if(dd.getDate()==sum)
		    	var month = myDate.getMonth()+2;
		    if (month<10)
		        month = "0"+month;
		    //获取明天
		    var dd = new Date();
		    dd.setDate(dd.getDate()+1);
		    var day = dd.getDate();
		    if (day<10)
		    	day = "0"+day;
		    var lastDay = year+"-"+month+"-"+day;
		    return lastDay;
		},
		init: function(){
			
		}
	}
}();
$(document).ready(common.report.processingTime.init);
