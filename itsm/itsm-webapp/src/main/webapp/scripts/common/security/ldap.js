$package('common.security')
/**  
 * @author QXY  
 * @constructor ldap
 * @description  LDAP管理主函数.
 * @date 2011-02-25
 * @since version 1.0 
 */
common.security.ldap=function(){
	
	this.operator;
	return{
		/**
		 * @description LDAP动作格式化
		 * @param cell 当前列值
		 * @param event 事件
		 * @param data 行数据
		 *
		 */
		ladpActionFormatter:function(cell,opt,data){
//			return '<a href="javascript:common.security.ldap.importNow('+data.ldapId+')" title="'+i18n['label_import_now']+'">'
//			+'<img src="../images/icons/user_green.gif" border="0"/></a>&nbsp;&nbsp;&nbsp;'
			return '<a href="javascript:common.security.ldap.editLDAP_aff('+opt.rowId+')" title="'+i18n['common_edit']+'">'
			+'<img src="../skin/default/images/grid_edit.png" border="0"/></a>&nbsp;&nbsp;&nbsp;'
			+'<a href="javascript:common.security.ldap.deleteLDAP_aff('+opt.rowId+')"  title="'+i18n['deletes']+'">'
			+'<img src="../skin/default/images/grid_delete.png" border="0"/></a>'
			+'</div>';
		},
		/**
		 * 状态格式化
		 * @param callValue 字段的值
		 */
		statuForma:function(callValue){
			if(callValue=='Yes')
				return i18n['label_dynamicReports_yes']
			else
				return i18n['label_dynamicReports_no']
			
		},
		/**
		 * @description 加载LDAP列表.
		 */
		ldapList:function(){
			var params = $.extend({},jqGridParams, {	
				url:'ldap!find.action',
				colNames:['ID',i18n['common_name'],i18n['title_ldap_ladpServerType'],i18n['title_ldap_server'],i18n['title_ldap_port'],
				          i18n['title_ldap_userName'],i18n['title_ldap_pwd'],'BaseDN',i18n['title_ldap_searchFilter'],
				          i18n['status'],i18n['label_allow_auto_update'],'',i18n['common_action']],
			 	colModel:[
			 	          {name:'ldapId',width:30},
			 	          {name:'ldapName',width:120},
			 	          {name:'ldapType',width:200},
			 	          {name:'ldapURL',width:150},
			 	          {name:'prot',width:50},
			 	          {name:'adminName',width:80},
			 	          {name:'adminPassword',hidden:true},
			 	          {name:'searchBase',width:120},
			 	          {name:'searchFilter',width:120},
			 	          {name:'connStatus',align:'center',width:40},
			 	         // {name:'ifImport',align:'center',width:70,formatter:common.security.ldap.statuForma},
			 	          {name:'ifAllowAutoImport',align:'center',width:90,formatter:common.security.ldap.statuForma},
			 	          {name:'ifAllowAutoImport',align:'center',hidden:true},
			 	          {name:'act', width:80,align:'center',sortable:false,formatter:common.security.ldap.ladpActionFormatter}
			 	],
				jsonReader: $.extend(jqGridJsonReader, {id: "ldapId"}),
				sortname:'ldapId',
				shrinkToFit:false,
				pager:'#ldapPager'
			});
			$("#ldapGrid").jqGrid(params);
			$("#ldapGrid").navGrid('#ldapPager',navGridParams);
			//列表操作项
			$("#t_ldapGrid").css(jqGridTopStyles);
			$("#t_ldapGrid").append($('#ldapTopMenu').html());
			
			//自适应宽度
			setGridWidth("#ldapGrid","regCenter",20);
		},
		
		/**
		 * @description 新增LDAP.
		 */
		
		addLDAP:function(){
			
			if($('#ldapForm').form('validate')){
			
				var ldap_form = $('#ldapData form').serialize();
				var url = 'ldap!'+operator+'.action';
				$.post(url, ldap_form, function(){
					
					msgShow(i18n['saveSuccess'],'show');
					$('#addLdap').dialog('close');
					$('#ldapGrid').trigger('reloadGrid');
				});
			
			}
		},
		
		/**
		 * @description 新增窗口.
		 */
		ldap_add_win:function(){
			$('#ldapConfigError').text("");
			$("#ldapData form")[0].reset();
			windows('addLdap',{width:620});
			operator = 'save';
			
		},
		/**
		 * @description 编辑LDAP.
		 */
		editLDAP:function(){
	
			checkBeforeEditGrid('#ldapGrid',function(data){
				resetForm("#ldapForm");
				operator = 'mergeLdap';
				$('#ldapId').val(data.ldapId);
				$('#ldapName').val(data.ldapName);
				$('#ldapType').val(data.ldapType);
				$('#ldapURL').val(data.ldapURL);
				$('#prot').val(data.prot);
				$('#adminName').val(data.adminName);
				$('#adminPassword').val(data.adminPassword);
				$('#searchBase').val(data.searchBase);
				$('#searchFilter').val(data.searchFilter);
				if(data.ifAllowAutoImport=='Yes')
					$('#ifAllowAutoImport').attr('checked',true);
				$('#ldapConfigError').text("");
				windows('addLdap',{width:620});
			});
		},
		
		/**
		 * @description 编辑LDAP.
		 */
		editLDAP_aff:function(rowId){
			var rowData= $("#ldapGrid").jqGrid('getRowData',rowId);  // 行数据 
				resetForm("#ldapForm");
				operator = 'mergeLdap';
				$('#ldapId').val(rowData.ldapId);
				$('#ldapName').val(rowData.ldapName);
				$('#ldapType').val(rowData.ldapType);
				$('#ldapURL').val(rowData.ldapURL);
				$('#prot').val(rowData.prot);
				$('#adminName').val(rowData.adminName);
				$('#adminPassword').val(rowData.adminPassword);
				$('#searchBase').val(rowData.searchBase);
				$('#searchFilter').val(rowData.searchFilter);
				if(rowData.ifAllowAutoImport=='Yes')
					$('#ifAllowAutoImport').attr('checked',true);
				$('#ldapConfigError').text("");
				windows('addLdap',{width:620});
		},
		
		
		/**
		 * @description 删除LDAP.
		 */
		deleteLDAP:function(){
			checkBeforeDeleteGrid('#ldapGrid',function(rowIds){
				var pp = $.param({'ids':rowIds},true);
				$.post("ldap!delete.action", pp, function(){
					$('#ldapGrid').trigger('reloadGrid');
					msgShow(i18n['deleteSuccess'],'show');
				}, "json");
				
			});
		},
		
		/**
		 * @description 删除LDAP.
		 * @param  rowId  行的编号
		 */
		deleteLDAP_aff:function(rowId){
			msgConfirm(i18n['msg_msg'],'<br/>'+i18n['msg_confirmDelete'],function(){
				var pp = $.param({'ids':rowId},true);
				$.post("ldap!delete.action", pp, function(){
					$('#ldapGrid').trigger('reloadGrid');
					msgShow(i18n['deleteSuccess'],'show');
				}, "json");
				
			});
		},
		 
		/**
		 * @description 搜索
		 */
		searchLdap:function(){
			var sdata=$('#searchLdap form').getForm();
			var postData = $("#ldapGrid").jqGrid("getGridParam", "postData");       
			$.extend(postData, sdata); 
			
			var _url = 'ldap!find.action';		
			$('#ldapGrid').jqGrid('setGridParam',{url:_url})
				.trigger('reloadGrid',[{"page":"1"}]).jqGrid('setGridParam',{url:_url});
			return false;
			
		},
		
		connTest:function(){
			if($('#ldapForm').form('validate')){
				var pp=$('#ldapForm').getForm();
				$('#ldapConfigError').text(i18n['label_ldap_connection']);
				$.post('ldap!ldapConfigCheck.action',pp,function(data){
					if(data){
						$('#ldapConfigError').text(i18n['label_ldap_connectionSuccessful']);
					}	
					else{
						$('#ldapConfigError').text(i18n['label_ldap_paramError']);
					}	
				},"json")
			}
		},
		//连接测试
		ldapConnTest:function(){
			checkBeforeEditGrid('#ldapGrid',function(data){
				$('#ldap_conn_test_result').text('['+i18n['label_ldap_connection']+']');
				$.post('ldap!ldapConnTest.action','ldapDto.ldapId='+data.ldapId,function(result){
					if(result){
						$('#ldap_conn_test_result').text('['+i18n['label_ldap_connectionSuccessful']+']');
					}	
					else{
						$('#ldap_conn_test_result').text('['+i18n['label_ldap_paramError']+']');
					}
					$('#ldapGrid').trigger('reloadGrid');
				})
			});
		},
		//立即导入
		importNow:function(ldapId){
			$.post('ldap!ldapUserImport.action','queryDto.ldapId='+ldapId,function(data){
				msgAlert(i18n['opertionTotal']+':'+total+','+i18n['newAdd']+':'+addNumber+','+i18n['update']+':'+upNumber+','+i18n['failure']+':'+filed,'info');
			})
		},
		init:function(){
			$("#ldapMain_loading").hide();
			 $("#ldapMain_content").show();
			
			 common.security.ldap.ldapList();
			
			//添加
			$('#link_ldap_add').click(common.security.ldap.ldap_add_win);
			$('#link_ldap_save').click(common.security.ldap.addLDAP);
			
			//编辑
			$('#link_ldap_edit').click(common.security.ldap.editLDAP)
			
			//删除
			$('#link_ldap_delete').click(common.security.ldap.deleteLDAP)
			
			//打开搜索窗口
			$('#link_ldap_search').click(function(){
				windows('searchLdap',{width:400,modal: false});
			})
			
			//开始搜索
			$('#link_start_search').click(function(){common.security.ldap.searchLdap();});
			
			//连接测试
			$('#link_conn_test').click(common.security.ldap.connTest);
			$('#link_ldap_conn_test').click(common.security.ldap.ldapConnTest);
			
		}
	}
}();

/**载入**/
$(document).ready(common.security.ldap.init);