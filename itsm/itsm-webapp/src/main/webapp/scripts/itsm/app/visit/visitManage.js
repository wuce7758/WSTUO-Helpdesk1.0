$package('itsm.app.visit') 
/**  
 * @fileOverview "visitManage"
 * @author QXY
 * @version 1.0  
 */  
 /**  
 * @author QXY  
 * @constructor visitManage
 * @description "visitManage"
 * @date 2010-11-17
 * @since version 1.0 
 */  
itsm.app.visit.visitManage = function(){
	var visitNo=0;
	return { 
	
		/**
		 * 类型国际化
		 */
		typeForma:function(cellvalue, options){
			if(cellvalue=='text'){
				return i18n['text']
			}
			if(cellvalue=='checkbox'){
				return i18n['checkbox']
			}
			if(cellvalue=='radio'){
				return i18n['radio']
			}
			if(cellvalue=='Lob'){
				return i18n['longText']
			}
			if(cellvalue='Integer'){
				return i18n['integer']
			}
		},
		useStatusForma:function(cellvalue, options){
			if(cellvalue)
				return i18n['enable']
			else
				return i18n['disable']
		},

		/**
		 * @description 回访事项列表
		 **/
		showVisitGrid:function()
		{	
			var params = $.extend({},jqGridParams, {	
				url:'visit!visitFindPager.action',
				caption:i18n['returnItem'],
				colNames:[i18n['number'],i18n['name'],i18n['type'],i18n['sort'],'',i18n['status']],
				colModel:[
						{name:'visitNo',width:80,align:'center'},	
						{name:'visitName',width:100},
						{name:'visitItemType',align:'center',width:100,formatter:itsm.app.visit.visitManage.typeForma},
						{name:'visitOrder',align:'center',width:100},
						{name:'visitItemType',hidden:true},
						{name:'useStatus',align:'center',width:100,formatter:itsm.app.visit.visitManage.useStatusForma}
						
						
				    ],
				jsonReader: $.extend(jqGridJsonReader, {id: "visitNo"}),
				sortname:'visitNo',
				pager:'#visitPager',
				onSelectRow:itsm.app.visit.visitManage.reloadVisitItem
				});
				$("#visitList").jqGrid(params);
				$("#visitList").navGrid('#visitPager',navGridParams);
				//列表操作项
				$("#t_visitList").css(jqGridTopStyles);
				$("#t_visitList").append($('#visitToolbar').html());
				
				//自适应宽度
				setGridWidth("#visitList","regCenter",20);
		},	

		/**
		 * @description 新增回访事项窗口
		 **/
		addvisit_win:function(){
			resetForm('#addvisitForm');
			windows('addvisit_win',{width:350});
		},
		/**
		 * @description 新增回访事项
		 **/
		addvisit_opt:function(){		
			if($('#addvisitForm').form('validate')){
				var _params = $('#addvisit_win form').serialize();
				startProcess();
				$.post('visit!visitSave.action', _params, function(){
					$('#addvisit_win').dialog('close');
					$('#visitList').trigger('reloadGrid');
					msgShow(i18n['addSuccess'],'show');
					endProcess();
				})
			}
		},
		/**
		 * @description 编辑回访事项面板
		 **/
		editvisit_win:function(){
			checkBeforeEditGrid("#visitList",itsm.app.visit.visitManage.editvisitFillData);
		},
		/**
		 * @description 搜索回访事项面板
		 **/
		searchvisit_win:function(){
			windows('searchvisit_win',{width:350,modal: false});
		},
		/**
		 * @description 搜索回访事项
		 **/
		searchvisit_opt:function(){
			var _params = $('#searchvisitForm').getForm();
			var postData = $("#visitList").jqGrid("getGridParam", "postData");       
			$.extend(postData, _params);  //将postData中的查询参数加上查询表单的参数		
			$('#visitList').trigger('reloadGrid',[{"page":"1"}]);
		},
		
		/**
		 * @description 编辑回访事项填充数据
		 **/
		editvisitFillData:function(rowData){
			$('#visitNo').val(rowData.visitNo);
			$('#visitName').val(rowData.visitName);
			$('#visitOrder').val(rowData.visitOrder);
			$('#visitItemType').val(rowData.visitItemType);
			if(rowData.useStatus==i18n['disable']){
				$('#visitState1').attr("checked",'false')
			}else
			{
				$('#visitState').attr("checked",'ture')
			}
			
			windows('editvisit_win',{width:350});
		},
		
		/**
		 * @description 编辑回访事项Do  
		 **/
		editvisit_opt:function(){		
			if($('#editvisitForm').form('validate')){
				var _params = $('#editvisit_win form').serialize();
				startProcess();
				$.post('visit!visitUpdate.action', _params, function(){
					$('#editvisit_win').dialog('close');
					$('#visitList').trigger('reloadGrid');
					msgShow(i18n['editSuccess'],'show');
					endProcess();
				});
			}
		},

		/**
		 * @description 删除回访事项
		 */
		deletevisit_opt:function(){
			checkBeforeDeleteGrid("#visitList",itsm.app.visit.visitManage.deletevisitMethod);
		},

		/**
		 * @description 删除回访事项Method
		 **/
		deletevisitMethod:function(rowIds){
			var pp = $.param({'ids':rowIds},true);
			$.post("visit!visitDelete.action", pp, function(){
				$('#visitList').trigger('reloadGrid');
				msgShow(i18n['deleteSuccess'],'show');
				visitNo=null;
				$('#visitItemList').jqGrid('setGridParam',{url:'visit!visitItemFindPager.action'}).trigger('reloadGrid',[{"page":"1"}]);
			}, "json");
		},

		 /**
		  * @description 回访选项值列表
		  **/
		showVisitItemGrid:function()
		{		
			var params = $.extend({},jqGridParams, {	
				url:'visit!visitItemFindPager.action',
				caption:i18n['returnOptionValue'],
				colNames:[i18n['number'],i18n['visit_value']],
				colModel:[
						{name:'visitItemNo',width:80,sorttype:'Long',align:'center',sortable:false},					
						{name:'visitItemName',width:100,align:'center',sortable:false}
				    ],
				jsonReader: $.extend(jqGridJsonReader, {id: "visitItemNo"}),
				sortname:'visitItemNo',
				pager:'#visitItemPager',
				onSelectRow:itsm.app.visit.visitManage.voidMethod
				});
				$("#visitItemList").jqGrid(params);
				$("#visitItemList").navGrid('#visitItemPager',navGridParams);
				//列表操作项
				$("#t_visitItemList").css(jqGridTopStyles);
				$("#t_visitItemList").html($('#visitItemToolbar').html());
				
				//自适应宽度
				setGridWidth("#visitItemList","regCenter",20);
		},
	    /**
	     * @description 根据ID刷新扩展属性
	     **/
		reloadVisitItem:function(id){
			visitNo=id;
			var _url = 'visit!visitItemFindPager.action?visitNo='+id;		
			$('#visitItemList').jqGrid('setGridParam',{url:_url})
				.trigger('reloadGrid',[{"page":"1"}]).jqGrid('setGridParam',{url:_url});
			return false;
		},
		
		 /**
		  * @description 新增回访选项值窗口
		  **/
		addVisitItem_win:function(){
			if(visitNo==null||visitNo=="0"){
				msgAlert(i18n['msg_chooseVisit'],'info');
			}else{
				var rowData=$("#visitList").getRowData(visitNo);
				if(rowData.visitItemType=='radio'){
					$('#edit_visitItemName').val('');
					windows('addVisitItem_win',{width:300});
				}else{
					msgAlert(i18n['lable_to_addVisitItem_win_show'],'info');
				}
			}
		},
		 /**
		  * @description 新增回访选项值
		  **/
		addVisitItem_opt:function(){	
				if($('#addVisitItemForm').form('validate')){
					var _params = $('#addVisitItem_win form').serialize();
					startProcess();
					$.post('visit!visitItemSave.action', _params+"&visitItemDTO.visitNo="+visitNo, function(){
						$('#addVisitItem_win').dialog('close');
						$('#visitItemList').trigger('reloadGrid');
						msgShow(i18n['addSuccess'],'show');
						endProcess();
					})
				}
		},
		
		
		 /**
		  * @description 编辑回访事项
		  **/
		editVisitItem_win:function(){
			checkBeforeEditGrid("#visitItemList",itsm.app.visit.visitManage.editVisitItemFillData);
		},
		
		
		 /**
		  * @description 编辑回访事项填充数据面板
		  */
		editVisitItemFillData:function(rowData){
			$('#visitItemNo').val(rowData.visitItemNo);
			$('#visitItemName').val(rowData.visitItemName);
			windows('editVisitItem_win',{width:300});
		},
		 /**
		  * @description 编辑回访事项填充数据
		  */
		editVisitItem_opt:function(){	
			if($('#editVisitItemForm').form('validate')){
				var _params = $('#editVisitItem_win form').serialize();
				$.post('visit!visitItemUpdate.action', _params+"&visitItemDTO.visitNo="+visitNo, function(){
					$('#editVisitItem_win').dialog('close');
					$('#visitItemList').trigger('reloadGrid');
					msgShow(i18n['editSuccess'],'show');
				});
			}
		},
		
		 /**
		  * @description 删除回访事项
		  **/
		deleteVisitItem_opt:function(){
			checkBeforeDeleteGrid("#visitItemList",itsm.app.visit.visitManage.deleteVisitItemMethod);
		},
		
		  /**
		   * @description 删除回访事项
		   **/
		deleteVisitItemMethod:function(rowIds){
			var pp = $.param({'ids':rowIds},true);
			$.post("visit!visitItemDelete.action", pp+"&visitNo="+visitNo, function(){
				$('#visitItemList').trigger('reloadGrid');
				msgShow(i18n['deleteSuccess'],'show');
				
			}, "json");
			
		},

	
		init: function() {	
			showAndHidePanel("#visitManage_panel","#visitManage_loading");
		    itsm.app.visit.visitManage.showVisitGrid();
		    itsm.app.visit.visitManage.showVisitItemGrid();
		}
	}
}();
$(document).ready(itsm.app.visit.visitManage.init);

