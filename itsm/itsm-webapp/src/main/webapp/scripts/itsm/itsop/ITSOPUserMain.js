$package('itsm.itsop');
$import("itsm.itsop.ITSOPCustomer_userGrid");
$import('common.security.editUserPassword');
$import("common.config.dictionary.dataDictionaryUtil");
/**
 * @author Van
 * @constructor WSTO
 * @description itsop/ITSOPUserMain.jsp
 * @date 2011-07-20
 * @since version 1.0
 */
itsm.itsop.ITSOPUserMain = function() {
	this.ITSOP_ADD_EDIT_FLAG;
	return {
		/**
		 * @description 动作格式化
		 */
		actionFormatter : function(cell, opt, data) {
			return actionFormat('1', '1').replace(
					'[edit]',
					'itsm.itsop.ITSOPUserMain.openEditWindow_aff(' + opt.rowId
							+ ')').replace(
					'[delete]',
					'itsm.itsop.ITSOPUserMain.deleteITSOPUser_aff(' + opt.rowId
							+ ')');
		},
		/**
		 * 加载外包客户列表
		 */
		showGrid : function() {
			var params = $.extend({},jqGridParams,
							{
								url : 'itsopUser!findITSOPUserPager.action',
								colNames : [ i18n['common_id'],
										i18n['common_name'],
										i18n['common_phone'],
										i18n['common_email'],
										i18n['common_address'],
										i18n['title_itsop_teachnician'], '',
										i18n['category'],
										i18n['common_action'], '', '', '', '',
										'', '', '', '' ],
								colModel : [
										{
											name : 'orgNo',
											width : 100,
											align : 'center'
										},
										{
											name : 'orgName',
											width : 100,
											align : 'center'
										},
										{
											name : 'officePhone',
											width : 100,
											align : 'center'
										},
										{
											name : 'email',
											width : 100,
											align : 'center'
										},
										{
											name : 'address',
											width : 150,
											align : 'left'
										},
										{
											name : 'technicianNameFullName',
											width : 200,
											align : 'center',
											sortable : false
										},
										{
											name : 'technicianNames',
											hidden : true
										},
										{
											name : 'typeName',
											align : 'center',
											width : 150,
											sortable : false
										},
										{
											name : 'act',
											width : 100,
											align : 'center',
											formatter : itsm.itsop.ITSOPUserMain.actionFormatter,
											sortable : false
										}, {
											name : 'officeFax',
											hidden : true
										}, {
											name : 'homePage',
											hidden : true
										}, {
											name : 'logo',
											hidden : true
										}, {
											name : 'corporate',
											hidden : true
										}, {
											name : 'companySize',
											hidden : true
										}, {
											name : 'regNumber',
											hidden : true
										}, {
											name : 'regCapital',
											hidden : true
										}, {
											name : 'typeNo',
											hidden : true
										} ],
								jsonReader : $.extend(jqGridJsonReader, {
									id : "orgNo"
								}),
								sortname : 'orgNo',
								pager : '#ITSOPUserGridPager',
								onSelectRow : function(id) {
								    setTimeout(function(){
								        var rowId = $('#ITSOPUserGrid').getGridParam('selrow');
								        if (rowId)
								            itsm.itsop.ITSOPCustomer_userGrid
	                                            .showUserGridByCustomer(rowId);
								    }, 0);
								}
							});
			$("#ITSOPUserGrid").jqGrid(params);
			$("#ITSOPUserGrid").navGrid('#ITSOPUserGridPager', navGridParams);
			
			// 列表操作项
			$("#t_ITSOPUserGrid").css(jqGridTopStyles);
			$("#t_ITSOPUserGrid").append($('#ITSOPUserGridToolbar').html());
			//自适应宽度
			setGridWidth("#ITSOPUserGrid","regCenter",20);
		},

		// countLicense:function(){
		//			
		// //计算剩余许可
		// setTimeout(function(){
		//				
		// var used=$("#ITSOPUserGrid").getGridParam("records");
		// var
		// license_msg=(i18n['msg_itsop_liscense']).replace('{0}',itsop_licenseNumber).replace('{1}',used).replace('{2}',itsop_licenseNumber-used);
		// $('#itsop_license').html("&nbsp;|&nbsp;&nbsp;"+license_msg);
		// },1000);
		//			
		// },

		/**
		 * 打开添加外包客户窗口.
		 */
		openAddWindow : function() {

			var used = $("#ITSOPUserGrid").getGridParam("records");

			$('#ITSOPUser_orgNo').val('');
			resetForm('#Window_Add_Edit_ITSOPUser form');// 清空表单
			ITSOP_ADD_EDIT_FLAG = 'addITSOPUser';

			windows('Window_Add_Edit_ITSOPUser', {
				width : 450
			});
			// if(itsop_licenseNumber-used<0){
			//				
			// msgAlert(i18n['ERROR_ITSOP_OVER_COUNT'],'info');
			//				
			// }else{
			//				
			//				
			// }

		},

		/**
		 * 打开编辑外包客户窗口.
		 */
		openEditWindow : function() {

			checkBeforeEditGrid("#ITSOPUserGrid", function(rowData) {
				ITSOP_ADD_EDIT_FLAG = 'updateITSOPUser';
				windows('Window_Add_Edit_ITSOPUser', {
					width : 450
				});
				resetForm('#Window_Add_Edit_ITSOPUser form');// 清空表单
				// 填充数据
				if (rowData.technicianNames != null
						&& rowData.technicianNames != '') {
					$('#ITSOPUser_technicianNamesStr').val(rowData.technicianNames.replace(/,/g, ";") + ";");
				}
				$('#ITSOPUser_orgNo').val(rowData.orgNo);
				$('#ITSOPUser_orgName').val(rowData.orgName);
				$('#ITSOPUser_officeFax').val(rowData.officeFax);
				$('#ITSOPUser_officePhone').val(rowData.officePhone);
				$('#ITSOPUser_email').val(rowData.email);
				$('#ITSOPUser_address').val(rowData.address);
				$('#ITSOPUser_corporate').val(rowData.corporate);
				$('#ITSOPUser_companySize').val(rowData.companySize);
				$('#ITSOPUser_regNumber').val(rowData.regNumber);
				$('#ITSOPUser_regCapital').val(rowData.regCapital);
				$('#ITSOPUser_typeNo').val(rowData.typeNo);
			});

		},

		/**
		 * 打开编辑外包客户窗口.
		 */
		openEditWindow_aff : function(rowId) {

			var rowData = $("#ITSOPUserGrid").jqGrid('getRowData', rowId); // 行数据
			ITSOP_ADD_EDIT_FLAG = 'updateITSOPUser';
			windows('Window_Add_Edit_ITSOPUser', {
				width : 450
			});
			resetForm('#Window_Add_Edit_ITSOPUser form');// 清空表单
			// 填充数据
			if (rowData.technicianNames != null
					&& rowData.technicianNames != '') {
				$('#ITSOPUser_technicianNamesStr').val(
						rowData.technicianNames.replace(/,/g, ";") + ";");
			}
			$('#ITSOPUser_orgNo').val(rowData.orgNo);
			$('#ITSOPUser_orgName').val(rowData.orgName);
			$('#ITSOPUser_officeFax').val(rowData.officeFax);
			$('#ITSOPUser_officePhone').val(rowData.officePhone);
			$('#ITSOPUser_email').val(rowData.email);
			$('#ITSOPUser_address').val(rowData.address);
			$('#ITSOPUser_corporate').val(rowData.corporate);
			$('#ITSOPUser_companySize').val(rowData.companySize);
			$('#ITSOPUser_regNumber').val(rowData.regNumber);
			$('#ITSOPUser_regCapital').val(rowData.regCapital);
			$('#ITSOPUser_typeNo').val(rowData.typeNo);
		},

		/**
		 * 保存外包客户.
		 */
		save : function() {
			var userStr=$('#ITSOPUser_technicianNamesStr').val();
			if(trim(userStr)==""){
				msgShow(i18n['itsop_add_msg'],'show');
				return false;
			}
			validateUsersByLoginName(
					$('#ITSOPUser_technicianNamesStr').val(),
					function() {

						var _frm = $('#Window_Add_Edit_ITSOPUser form')
								.serialize();
						var _url = 'itsopUser!' + ITSOP_ADD_EDIT_FLAG
								+ '.action';

						if ($('#Window_Add_Edit_ITSOPUser form').form('validate')) {
							startProcess();
							if (ITSOP_ADD_EDIT_FLAG == "addITSOPUser") {
								var _param = $.param({'itsopUserDTO.companyNo': companyNo}, true);
								_frm=_frm+'&'+_param;
							}
							$.post(_url,_frm,function(data) {
								endProcess();
								if( data === "over_itsop_license_number" ){
									msgAlert(i18n["itsop_license_over"],"error");
									return;
								}
								$('#Window_Add_Edit_ITSOPUser').dialog('close');
								$('#ITSOPUserGrid').trigger('reloadGrid');

								msgShow(i18n['msg_itsop_addEditSuccess'],'show');

								if (ITSOP_ADD_EDIT_FLAG == "addITSOPUser") {
									msgAlert(i18n["msg_itsop_managerPassword"]+ data,
											"info");
								}
							});
						}
					});

		},

		/**
		 * 删除外包客户.
		 */
		deleteITSOPUser : function() {
			checkBeforeDeleteGrid("#ITSOPUserGrid", function(rowIds) {
				var _param = $.param({
					'ids' : rowIds
				}, true);
				var _url = 'itsopUser!deleteITSOPUser.action';

				$.post(_url, _param, function() {
					$('#ITSOPUserGrid').trigger('reloadGrid');
					msgShow(i18n['msg_itsop_deleteSuccess'], 'show');
					// itsm.itsop.ITSOPUserMain.countLicense();
					setTimeout(function() {
						var arr = $("#ITSOPUserGrid").getDataIDs();
						var defaultId = 0;

						if (arr.length > 0) {
							defaultId = arr[0];
						}
						itsm.itsop.ITSOPCustomer_userGrid
								.showUserGridByCustomer(defaultId);
					}, 1000);
				});
			});

		},

		/**
		 * 删除外包客户.
		 */
		deleteITSOPUser_aff : function(orgNo) {
			msgConfirm(i18n['msg_msg'], '<br/>' + i18n['msg_confirmDelete'],
					function() {
						var _param = $.param({
							'ids' : orgNo
						}, true);
						var _url = 'itsopUser!deleteITSOPUser.action';
						$.post(_url, _param, function() {
							$('#ITSOPUserGrid').trigger('reloadGrid');
							msgShow(i18n['msg_itsop_deleteSuccess'], 'show');
							// itsm.itsop.ITSOPUserMain.countLicense();
							setTimeout(function() {
								var arr = $("#ITSOPUserGrid").getDataIDs();
								var defaultId = 0;

								if (arr.length > 0) {
									defaultId = arr[0];
								}
								itsm.itsop.ITSOPCustomer_userGrid
										.showUserGridByCustomer(defaultId);
							}, 1000);
						});
					});

		},

		/**
		 * 打开搜索窗口.
		 */
		searchOpenWindow : function() {
			windows('Window_Search_ITSOPUserGrid', {
				width : 400,
				modal : false
			});
		},

		/**
		 * 搜索外包客户.
		 */
		doSearch : function() {

			var _url = 'itsopUser!findITSOPUserPager.action';
			searchGridWithForm('#ITSOPUserGrid',
					'#Window_Search_ITSOPUserGrid', _url);
		},

		/**
		 * 导出数据到Excel
		 */
		exportView : function() {
			var _postData = $("#ITSOPUserGrid").jqGrid("getGridParam",
					"postData"); // 列表参数
			var params = "?";
			$.each(_postData, function(k, v) {
				params += k + "=" + v + "&";
			});
			params = params.substring(0, params.length - 1);
			location.href = "itsopUser!exportITSOPUser.action"+ params;
		},
		/**
		 * 导入数据.
		 */
		openImportWindow : function() {
			windows('importITSOPUserWindow', {
				width : 400
			});
		},
		/**
		 * 执行导入
		 */
		importExcel : function(fix) {
			startProcess();
			var inputPath=$('#itsopUserImportFile').val();
			if(inputPath!=""){
				$.ajaxFileUpload({
					url : 'itsopUser!importITSOPUser.action?fix='+fix,
					secureuri : false,
					fileElementId : 'itsopUserImportFile',
					dataType : 'json',
					success : function(data, status) {
						if( data === "over_itsop_license_number" ){
							$('#ITSOPUserGrid').trigger('reloadGrid');
							msgAlert(i18n["itsop_license_over"],"error");
						}else if (data == "FileNotFound") {
							msgAlert(i18n['msg_dc_fileNotExists'], 'error');
						} else if (data == "IOError") {
							msgAlert(i18n['msg_dc_importFailure'], 'error');
						} else if (data == "comeBack") {
							msgConfirm(i18n['msg_msg'], '<br/>'+ i18n['msg_import_msg'], function() {
								itsm.itsop.ITSOPUserMain.importExcel(1); //确定则执行覆盖
							});
						}else if (data == "userNUll") {
							msgAlert(i18n['msg_userNUll'], 'info');
						} else {
							$('#importITSOPUserWindow').dialog('close');
							$('#ITSOPUserGrid').trigger('reloadGrid');
							msgAlert(data.replace('Total', i18n['opertionTotal'])
									.replace('Insert', i18n['newAdd']).replace(
											'Update', i18n['update']).replace(
											'Failure', i18n['failure']), 'show');
						}
						endProcess();
					}
				});
			}else{
				msgAlert(i18n['msg_dc_fileNull'],'info');
				endProcess();
			}
		},
		/**
		 * 打开编辑密码窗口
		 */
		edit_ITSOPUser_pwd:function(){
			var rowData=$('#ITSOPCustomer_userGrid').getRowData($('#itsop_user_userId').val());
			$.post('user!findUserDetail.action','userDto.userId='+$('#itsop_user_userId').val(),function(data){
				$('#editITSOPUserPwd_loginName').val(data.loginName);
				$('#editITSOPUserPwd_loginName_span').text(data.loginName);
				$('#editITSOPUserPwd_oldPassword').val(data.password);
				$('#editITSOPUserPwd_newPassword,#editITSOPUserPwd_repeatPassword').val('');
				windows('editITSOPUserPwd_win',{width:380,height:190});
			})
			
		},
		/**
		 * 修改密码(用户管理)
		 */
		editUserPassword_manage:function(){
			if($('#editITSOPUserPwd_win form').form('validate')){
				
				var pass=$('#editITSOPUserPwd_newPassword').val();
				var parent=/^[A-Za-z]+$/;
				/*if(pass.length<6){
					//$('#userOtherInfo').tabs('select',i18n.title_ldap_pwd);
					msgAlert(i18n.add_msg_trial_wrong_password,'info');
				}else
				if(!isNaN(pass)){
					msgAlert(i18n.lable_password_num,'info');
				}
				else if(parent.test(pass)){
					msgAlert(i18n.lable_password_chart,'info');
				}else{*/ 
				/*if($('#editITSOPUserPwd_newPassword').val().length<6){
					msgAlert(i18n['add_msg_trial_wrong_password'],'info');
				}else{*/
					var _param = $('#editITSOPUserPwd_win form').serialize();
					$.post('user!resetPassword.action',_param,function(data){
						if(data){
							$('#editITSOPUserPwd_win').dialog('close');
							msgShow(i18n['editSuccess'],'show');
							
						}else{
							msgAlert(i18n['oldPasswordError'],'info');
						}
					})
				//}
			}	
		},
		/**
		 * 打开编辑密码窗口
		 */
		getitsopAlreadyUsed:function(){
			$.post('itsopUser!getitsopAlreadyUsed.action',function(data){
				$("#itsopAlreadyUsed").text(data);
			});
		},
		/**
		 * 初始化
		 */
		init : function() {
			// 绑定日期控件
			DatePicker97([ '#itsop_user_birthday' ]);

			showAndHidePanel("#ITSOPUserMain_panel","#ITSOPUserMain_loading");
			$('#ITSOPUser_regCapital').numberbox({
				min : 0,
				validType : length[1, 18],
				max : 999999999999999999
			});
			itsm.itsop.ITSOPUserMain.showGrid();
			itsm.itsop.ITSOPUserMain.getitsopAlreadyUsed();
			// 添加
			$('#ITSOPUserGrid_add').click(
					itsm.itsop.ITSOPUserMain.openAddWindow);
			// 编辑
			$('#ITSOPUserGrid_edit').click(
					itsm.itsop.ITSOPUserMain.openEditWindow);
			// 删除
			$('#ITSOPUserGrid_delete').click(
					itsm.itsop.ITSOPUserMain.deleteITSOPUser);
			// 搜索
			$('#ITSOPUserGrid_search').click(
					itsm.itsop.ITSOPUserMain.searchOpenWindow);
			$('#SEARCH_ITSOPUSER_SUBMIT').click(
					itsm.itsop.ITSOPUserMain.doSearch);

			$('#exportITSOPUser').click(itsm.itsop.ITSOPUserMain.exportView);// 导出
			$('#importITSOPUser').click(
					itsm.itsop.ITSOPUserMain.openImportWindow);// 导入
			// 保存
			$('#Add_Edit_ITSOPUser_Submit')
					.click(itsm.itsop.ITSOPUserMain.save);

			$('#add_edit_itsop_technician').click(
					function() {
						common.security.userUtil.selectUserMulti(
								'#ITSOPUser_technicianNamesStr', '',
								'loginName', companyNo);
					});

			$('#itsop_reset_user_password_link').click(
					function() {
						common.security.editUserPassword.userPasswordReset($(
								'#itsop_user_loginName').val())
					});

			// 加载用户列表
			itsm.itsop.ITSOPCustomer_userGrid.init();
			$('#itsop_edit_pwd').click(itsm.itsop.ITSOPUserMain.edit_ITSOPUser_pwd);//修改密码
			$('#edit_ITSOPUser_pwd_but').click(itsm.itsop.ITSOPUserMain.editUserPassword_manage);//确定修改密码
			common.config.dictionary.dataDictionaryUtil.loadOptionsByCode(
					'itsopType', '#ITSOPUser_typeNo,#search_ITSOPUser_typeNo');
			itsm.app.autocomplete.autocomplete.bindManyAutocomplete('#ITSOPUser_technicianNamesStr','com.wstuo.common.security.entity.User','fullName','fullName','userId','Long','#addRequestUserId',companyNo);
		}

	};

}();
$(document).ready(itsm.itsop.ITSOPUserMain.init);