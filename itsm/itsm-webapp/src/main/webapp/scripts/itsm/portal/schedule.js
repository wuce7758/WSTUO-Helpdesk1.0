$package('itsm.portal');
$import('common.config.dictionary.dataDictionaryUtil');

$import('itsm.portal.costCommon');
/**  
 * @fileOverview "日程表中任务管理"
 * @author QXY  
 * @constructor Tan
 * @description 日程表中任务管理
 * @date 2011-12-13
 * @version 1.0  
 * @since version 1.0 
 */
itsm.portal.schedule = function(){
	this.calendarTaskDiv = $('#calendarTaskDiv');
	this.opt="";
	this.obj;
	this._startTime ="";
	this._endTime ="";
	this._taskId ="";
	this._eventTask = "";
	this.eventSource='tasks!findAllTaskShowSchedue.action?taskQueryDto.owner='+userName;
	var showOther="taskQueryDto.owner=";
	var state ="0";
	var dtoState ="";
	var ii=0;
	return {
			
		showSchedule:function(){
			var date = new Date();
			var d = date.getDate();
			var m = date.getMonth();
			var y = date.getFullYear();
			dtoState="";
			$('#schedule_div').fullCalendar({
				//国际化
				monthNames: [
                  i18n.label_calendar_January,
                  i18n.label_calendar_February,
                  i18n.label_calendar_March,
                  i18n.label_calendar_April,
                  i18n.label_calendar_May,
                  i18n.label_calendar_June,
                  i18n.label_calendar_July,
                  i18n.label_calendar_August,
                  i18n.label_calendar_September,
                  i18n.label_calendar_October,
                  i18n.label_calendar_November,
                  i18n.label_calendar_December
				],
				monthNamesShort: [
                  i18n.label_calendar_January_Short,
                  i18n.label_calendar_February_Short,
                  i18n.label_calendar_March_Short,
                  i18n.label_calendar_April_Short,
                  i18n.label_calendar_May_Short,
                  i18n.label_calendar_June_Short,
                  i18n.label_calendar_July_Short,
                  i18n.label_calendar_August_Short,
                  i18n.label_calendar_September_Short,
                  i18n.label_calendar_October_Short,
                  i18n.label_calendar_November_Short,
                  i18n.label_calendar_December_Short
				],
				dayNames: [
                  i18n.label_calendar_Sunday,
                  i18n.label_calendar_Monday,
                  i18n.label_calendar_Tuesday,
                  i18n.label_calendar_Wednesday,
                  i18n.label_calendar_Thursday,
                  i18n.label_calendar_Friday,
                  i18n.label_calendar_Saturday
				],
				dayNamesShort: [
                  i18n.label_calendar_Sunday_Short,
                  i18n.label_calendar_Monday_Short,
                  i18n.label_calendar_Tuesday_Short,
                  i18n.label_calendar_Wednesday_Short,
                  i18n.label_calendar_Thursday_Short,
                  i18n.label_calendar_Friday_Short,
                  i18n.label_calendar_Saturday_Short
				],
				buttonText: {
					prev: '&nbsp;&#9668;&nbsp;',
					next: '&nbsp;&#9658;&nbsp;',
					prevYear: '&nbsp;&lt;&lt;&nbsp;',
					nextYear: '&nbsp;&gt;&gt;&nbsp;',
					today: i18n.label_calendar_Today,
					month: i18n.label_calendar_Month,
					week: i18n.label_calendar_Week,
					day: i18n.label_calendar_Day
				},
				
				allDayText:i18n.label_calendar_AllDay,
				theme: true,
				header: {
					left: 'prev,next today',
					center: 'title',
					right: 'month,agendaWeek,agendaDay'
				},
				editable: false,
				timeFormat : 'TT HH:mm',
				events: 'tasks!findAllTaskShowSchedue.action?taskQueryDto.owner='+userName,
				//点击查看任务详细
				eventClick : function(calEvent, jsEvent, view) {
					dtoState="";
					var startTime = dateFormat(calEvent.start);
					var endTime = dateFormat(calEvent.end);
					_startTime = startTime;_endTime = endTime;_taskId = calEvent.taskId;
					itsm.portal.costCommon.scheduleShow1( calEvent.taskId ,startTime,endTime );
					obj=calEvent;
				},
				dayClick:function(dayDate,allDay, jsEvent, view){
					$('#scheduleAddEditTaskDiv input,#scheduleAddEditTaskDiv textarea').removeAttr('disabled');
					$("#schedule_type_val").hide();
					$('#schedule_type,#schedule_task_owner').removeAttr('disabled');
					
					$('#schedule_link_task_save_edit_ok2,#schedule_link_task_save_edit_ok,'+
							'#schedule_task_delete_but,#schedule_task_edit_but,#schedule_type,'+
							'#schedule_task_owner_link,.schedule_taskCycle_tr').show();
					
					$("#schedule_realStartTimeTr").attr("style","display:none;");
					$("#schedule_realEndTimeTr").attr("style","display:none;");
					$("#schedule_realFreeTr").attr("style","display:none;");
					opt='saveTask';
					$("#schedule_task_owner").val("");
					resetForm('#scheduleTaskForm');
					itsm.portal.schedule.nowDay(dayDate);
					bindControl('#schedule_realEndTime,#schedule_realStartTime');
					$('#schedule_taskAllDay').attr('checked',allDay);
					$('#schedule_link_task_save_edit_ok').hide();
					$('#schedule_task_owner_link').unbind()
						.click(function(){common.security.userUtil.selectUserMultiRule('#schedule_task_owner_fullName','#schedule_task_owner','Tesk',companyNo);});//选择技术员
					$("#schedule_taskCycle_week_tr").attr('style','display:none');
					if (!$('#schedule_taskCycleWeek').is(":checked")) {
						$(".schedule_taskTime").show();
					}
					$("#schedule_taskCycle_tr,#schedule_task_owner_tr,#schedule_type").show();
					$("#schedule_type_val").hide();
					itsm.portal.schedule.userTaskViewByAuthority();
					$('#scheduleAddEditTaskDiv').dialog({width:600,position:['auto',80],modal: true});
				}
			});
			//查看全部
			$('#viewAllTask').click(function(){
				startLoading();
				state="1";
				itsm.portal.schedule.addEventSource('taskQueryDto.owner=');
				$("#searchCalendarTask_owner").val("");
				$("#searchCalendarTask_owner_fullName").val("");
			});
			//我的任务
			$('#viewMyTask').click(function(){
				startLoading();
				state="0";
				itsm.portal.schedule.addEventSource('taskQueryDto.owner='+userName);
				$("#searchCalendarTask_owner").val(userName);
			});
			//查看其他工程师
			$('#viewSpecifiedEngineerTask').click(function(){
				state="2";
				resetFormAll('#searchCalendarTask form');
				var calendarView = $('#schedule_div').fullCalendar( 'getView' );
				itsm.portal.schedule.timePickerInitValue(null,
						"#searchStartTime",null, dateFormat(calendarView.visStart) );
				itsm.portal.schedule.timePickerInitValue(null,
						"#searchEndTime",null, dateFormat(calendarView.visEnd) );
				windows('searchCalendarTask',{width:410});
				$('#searchCalendarTask_owner_fullName').unbind().click(function(){
					common.security.userUtil.selectUser('#searchCalendarTask_owner_fullName','#searchCalendarTask_owner','','EditTesk',companyNo);//选择技术员
				});
				$('#link_calendarTask_search_ok,#searchCalendarTask_owner_link').unbind();
				$('#searchCalendarTask_owner_link').click(function(){common.security.userUtil.selectUser('#searchCalendarTask_owner_fullName','#searchCalendarTask_owner','','EditTesk',companyNo);});//选择技术员
				$('#link_calendarTask_search_ok').click(function(){
					if($('#searchCalendarTask form').form('validate')){
						startLoading();
						showOther = $('#searchCalendarTask form').serialize();
						itsm.portal.schedule.addEventSource(showOther);
					}
				});
			});
			$('#ExportInfoTask').click(function(){
				var calendarView = $('#schedule_div').fullCalendar( 'getView' );
				itsm.portal.schedule.timePickerInitValue(null,
						"#searchStartTime",null, dateFormat(calendarView.visStart) );
				itsm.portal.schedule.timePickerInitValue(null,
						"#searchEndTime",null, dateFormat(calendarView.visEnd) );
				$("#searchCalendarTask form").submit();
			});
			//查看全部和查看其他工程师权限
			if(viewAllTaskRes){
				$('#viewAllTaskRes').show();
			}else{
				$('#viewAllTaskRes').hide();
			}
			$("#searchCalendarTask_owner").val(userName);
			$('input[name="taskDto.taskCycle"]').change(itsm.portal.schedule.scheduleTaskCycle);
		},
		addEditTask_win:function(){
			itsm.portal.schedule.userTaskViewByAuthority();
			$("#schedule_task_owner").val("");
			resetForm('#scheduleTaskForm');
			opt='saveTask';
			$("#schedule_task_owner_tr,#schedule_link_task_save_edit_ok").hide();
			$("schedule_link_task_save_edit_ok2").show();
			$("#schedule_task_owner").val( userName );
			$("#schedule_task_owner_fullName").val( fullName );
			itsm.portal.schedule.nowDay( new Date() );
			//$('#schedule_task_owner_link').unbind().click(function(){common.security.userUtil.selectUserMultiRule('#schedule_task_owner','','loginName',companyNo);});//选择技术员
			$("#schedule_taskCycle_week_tr").attr('style','display:none');
			$('#schedule_taskCycle').attr("checked",'true');
			$('#scheduleAddEditTaskDiv').dialog({width:600,position:['auto',80]});
		},
		//保存任务
		saveTask:function(){
			validateUsersByLoginName(
					$('#schedule_task_owner').val(),
					function(){
					var title = $('#schedule_taskTitle').val();
					$('#schedule_taskTitle').val($.trim(title));
					var title = $('#schedule_taskIntroduction').val();
					$('#schedule_taskIntroduction').val($.trim(title));
					if($('#scheduleTaskForm').form('validate')){
						if ( itsm.portal.schedule.taskCycleWeekCheck_opt()) {
							return;
						}
						startLoading();
						var _param = $('#scheduleTaskForm').serialize();
						var _url='tasks!'+opt+'.action';
						if(opt=='saveTask'){
							$.post('tasks!timeConflict.action',_param,function(data){
								if(data){
									msgConfirm(i18n.msg_msg,'<br/>'+i18n.timeConflictTip,function(){
										itsm.portal.schedule.saveTask_opt(_url,_param);
									});
									endLoading();
								}else{
									itsm.portal.schedule.saveTask_opt(_url,_param);
								}
								
							});
						}else{
							itsm.portal.schedule.saveTask_opt(_url,_param);
						}
                    }
			});
		},
		//保存任务
		saveTask_opt:function(_url,_param){
			$.post(_url,_param,function(ids){
				endLoading();
				var owners = $('#schedule_task_owner').val().split(";");
				for ( var i = 0; i < ids.length; i++) {
					$('#scheduleAddEditTaskDiv').dialog('close');
					var _allDay=false;
					if($("#schedule_taskAllDay").attr('checked')){
						_allDay=true;
					}
					var _end=$('#schedule_taskEndTime').val();
					var _id=ids[i];
					var _introduction=$('#schedule_taskIntroduction').val();
					var _location=$('#schedule_taskLocation').val();
					var _start=$('#schedule_taskStartTime').val();
					var _status=$("#scheduleAddEditTaskDiv [name='taskDto.taskStatus'][checked=true]:radio").val();
					var _title='['+owners[i]+']'+$('#schedule_taskTitle').val();
					var _class="new_fc-event-skin";//样式
					
					if(_status==1){
						_class="processing_fc-event-skin";
					}else if(_status==2){
						_class="complete_fc-event-skin";
					}
					if(opt=='saveTask'){
						$('#schedule_div').fullCalendar('renderEvent',{
							"allDay":_allDay,
							"end":_end,
							"id":_id,
							"introduction":_introduction,
							"location":_location,
							"start":_start,
							"status":_status,
							"title":_title,
							"className":_class
						},false);
					}else{
						var _cellEvent = itsm.portal.schedule.obj;
						$.extend(_cellEvent,{
							"allDay":_allDay,
							"end":_end,
							"introduction":_introduction,
							"location":_location,
							"start":_start,
							"status":_status,
							"title":_title,
							"className":_class
						});
						if (_cellEvent) {
							$('#schedule_div').fullCalendar('updateEvent',_cellEvent);
						}
					}
				}
				
				startLoading();
				if(state=="0"){
					itsm.portal.schedule.addEventSource('taskQueryDto.owner='+userName);
				}else if(state=="1"){
					itsm.portal.schedule.addEventSource('taskQueryDto.owner=');
				}else if(state=="2"){
					itsm.portal.schedule.addEventSource(showOther);
				}
				$("#taskGrid").trigger('reloadGrid');
				msgShow(i18n.saveSuccess,'show');
			});
		},
		//显示任务详细
		showCalendarTask:function(eventObject){
			$("#addcalendarTreatmentResultsTr").hide();
			$("#calendarTreatmentResults_tr").hide();
			$("#addcalendarTreatmentResults").val("");
			startLoading();
			$('#schedule_task_delete_but').show();
			$('#schedule_task_owner_link').show();
			$('#schedule_task_edit_but').show();
			$('#schedule_task_process_but').show();
			$('#schedule_task_remark_but').show();
			$('#schedule_task_closed_but').show();
			$('#calendarTaskId').val(eventObject.taskId);
			$('#calendarTaskCreator').text(eventObject.creator);
			$('#calendarTaskOwner').text(eventObject.owner);
			if(eventObject.taskType=='task_personal'){
				$("#taskType").text(i18n.label_task_personal);
			}
			if(eventObject.taskType=='task_workforce'){
				$("#taskType").text(i18n.label_task_workforce);
			}
			if(eventObject.title!=null && eventObject.title!="null"){
				$('#calendarTaskTitle').text(eventObject.title.substring(eventObject.title.indexOf(']')+1,eventObject.title.length));
			}else{
				$("#calendarTaskTitle").val("null");
			}
			$('#calendarTaskLocation').text(eventObject.location);
			$('#calendarTaskIntroduction').text(eventObject.introduction);
			if(dtoState=="dtoState"){
				$('#calendarTaskStartTime').text(eventObject.startTime);
				if(eventObject.endTime===null){
					$('#calendarTaskEndTime').text(eventObject.startTime);
				}else{
					$('#calendarTaskEndTime').text(eventObject.endTime);
				}
			}else{
				$('#calendarTaskStartTime').text($.fullCalendar.formatDate(eventObject.start,'yyyy-MM-dd HH:mm:ss'));
				if(eventObject.end===null){
					$('#calendarTaskEndTime').text($.fullCalendar.formatDate(eventObject.start,'yyyy-MM-dd HH:mm:ss'));
				}else{
					$('#calendarTaskEndTime').text($.fullCalendar.formatDate(eventObject.end,'yyyy-MM-dd HH:mm:ss'));
				}
			}
			if (eventObject.taskStatus == '0') {
				$('#calendarTaskStatus').text(i18n.title_newCreate);
				$('#schedule_task_closed_but').hide();
			}
			if (eventObject.taskStatus == '1') {
				$('#calendarTaskStatus').text(i18n.task_label_pending);
				$('#schedule_task_process_but').hide();
			}
			if (eventObject.taskStatus == '2') {
				$('#calendarTaskStatus').text(i18n.lable_complete);
				$('#schedule_task_process_but').hide();
				$('#schedule_task_closed_but').hide();
				$("#schedule_task_edit_but").hide();
				$('#calendarResults_tr').show();
				$('#calendarResults').text(eventObject.treatmentResults);
			}
			if (eventObject.allDay == 'true' || eventObject.allDay) {
				$('#calendarTaskAllDay').text(i18n.label_basicConfig_deafultCurrencyYes);
			} else {
				$('#calendarTaskAllDay').text(i18n.label_basicConfig_deafultCurrencyNo);
			}
			$('#calendarRealStartTime').text(eventObject.realStartTime);
			$('#calendarRealEndTime').text(eventObject.realEndTime);
			$('#calendarRealFree').text(Math.floor(eventObject.realFree*1/1440)+i18n.label_slaRule_days+" "+Math.floor(eventObject.realFree*1%1440/60)+i18n.label_slaRule_hours+" "+Math.floor(eventObject.realFree*1%1440%60)+i18n.label_slaRule_minutes);
			//$('#calendarTreatmentResults').text(eventObject.treatmentResults);
			$('#schedule_task_edit_but').unbind().click(function(){itsm.portal.schedule.editCalendarTask(eventObject.taskId);});
			$('#schedule_task_delete_but').unbind().click(function(){itsm.portal.schedule.remoreCalendarTask(eventObject.taskId);});
			$('#schedule_task_process_but').unbind().click(function(){itsm.portal.schedule.processCalendarTask(eventObject);});
			$('#schedule_task_closed_but').unbind().click(function(){itsm.portal.schedule.closedCalendarTask_opt(eventObject.taskId);});
			$('#schedule_task_remark_but').unbind().click(function(){itsm.portal.schedule.remarkCalendarTask_opt(eventObject.taskId);});
			
			if (eventObject.taskCycle === "WEEK") {
				$("#taskType").text( $("#taskType").text() + " ( "+i18n.lable_task_cycle_type+" )");
			}
			itsm.portal.schedule.scheduleShow(eventObject);
		},
		/**
		 * 
		 */
		scheduleShow:function(eventObject){
			var taskId = eventObject.taskId;
			//if(eventObject.ownerLoginName==userName && taskId && eventObject.taskStatus != "0"){
				var startTime = dateFormat(eventObject.start);
				var endTime = dateFormat(eventObject.end);
				_startTime = startTime;_endTime = endTime;_taskId = eventObject.taskId;
				$.post("tasks!taskCostByTaskId.action",
						'taskDto.taskId='+taskId+"&taskDto.startTime="+startTime+"&taskDto.endTime="+endTime+"&taskDto.taskCycle="+eventObject.taskCycle,
						function(res){
					if (res.costDTO && res.costDTO.progressId) {
						var data = res.costDTO;
						$('#calendarRealStartTime').text(timeFormatter(data.startTime,'',''));
						$('#calendarRealStartTime').text(timeFormatter(data.endTime,'',''));
						$('#taskStartToEedTime').text(itsm.portal.costCommon.minute2HM2(data.startToEedTime,'',''));
						$('#calendarRealFree').text(itsm.portal.costCommon.minute2HM2(data.actualTime,'',''));
						$('#calendarResults').text(data.description);
						
						if (res.costDTO.status === 2) {
							$("#schedule_task_closed_but").hide();
							$("#schedule_task_edit_but").hide();
							$('#calendarResults_tr').show();
						}else{
							$("#schedule_task_closed_but").show();
						}
						$("#schedule_task_process_but").hide();
					}else{
						$("#schedule_task_process_but").show();
						$("#schedule_task_closed_but").hide();
						$("#task_cost_show_action").hide();
					}
					if (res.taskStatus === 1) {
						$('#calendarTaskStatus').text(i18n.task_label_pending);
						$("#schedule_task_delete_but").hide();
					}else if(res.taskStatus === 2){
						$("#schedule_task_delete_but").hide();
						$("#schedule_task_edit_but").hide();
						$('#calendarTaskStatus').text(i18n.lable_complete);
					}else{
						$('#calendarTaskStatus').text(i18n.title_newCreate);
						$("#schedule_task_delete_but").show();
					}
					var taskUserName=eventObject.taskCreator;
					var ownerLoginName = eventObject.ownerLoginName;
					if(ownerLoginName!=userName){
						$('#schedule_task_process_but').hide();
						$('#schedule_task_closed_but').hide();
					}
					if(taskUserName==userName || operationTaskRes){
						$('#task_info_win').show();
					}else{
						$('#task_info_win').show();
						$('#schedule_task_delete_but').hide();
						$('#schedule_task_owner_link').hide();
						$('#schedule_task_edit_but').hide();
					}
					
					if (res.dataFlag === 99) {
						$("#schedule_task_edit_but").hide();
						$("#schedule_task_delete_but").hide();
					}
					endLoading();
					windows('calendarTaskDiv',{title:i18n.title_request_task+':'+eventObject.title,width:'600px',modal: true});
				});
			/*}else{
				endLoading();
				windows('calendarTaskDiv',{title:i18n.title_request_task+':'+eventObject.title,width:'600px',modal: true});
			}*/
		},
		//编辑任务
		editCalendarTask:function(id){
			startProcess();
			$('#scheduleAddEditTaskDiv input,#scheduleAddEditTaskDiv textarea').removeAttr('disabled');
			$("#schedule_taskCycle_tr,#schedule_task_owner_tr,#schedule_type,#schedule_task_taskType_tr").show();
			$("#schedule_type_val").hide();
			
			$("#addcalendarTreatmentResultsTr").attr('style','display:none');
			$("#calendarTreatmentResults_tr").attr('style','display:none');
			$(".schedule_taskCycle_tr").show();
			$("#schedule_taskCycle_week_tr").show();
			$.post("tasks!findByTaskId.action",'taskId='+id,function(res){
				bindControl('#schedule_realEndTime,#schedule_realStartTime');
				opt='editTask';
				$('#schedule_link_task_save_edit_ok').show();
				$('#schedule_link_task_save_edit_ok2').show();
				if(res.taskStatus=='0'){
					$('#schedule_new').attr("checked",'true');
				}else if(res.taskStatus=='1'){
					$('#schedule_processing').attr("checked",'true');
				}else{
					$('#schedule_complete').attr("checked",'true');
				}
				if(res.taskType=='task_personal'){
					$("#schedule_type").text(i18n.label_task_personal);
				}
				if(res.taskType=='task_workforce'){
					$("#schedule_type").text(i18n.label_task_workforce);
				}
				$("#schedule_type").val(res.dCode);
				if(res.allDay){
					$('#schedule_taskAllDay').attr("checked",true);
				}else{
					$('#schedule_taskAllDay').attr("checked","");
				}
				$("#schedule_task_taskId").val(res.taskId);
				if( res.title!=null && res.title!="null"){
					$("#schedule_taskTitle").val(res.title.substr(res.title.indexOf(']')+1));
				}else{
					$("#schedule_taskTitle").val("null");
				}
				$("#schedule_taskLocation").val(res.location);
				$("#schedule_taskIntroduction").val(res.introduction);
				$("#schedule_taskStartTime_hid").val(res.startTime);
				$('#schedule_realStartTime').val(res.realStartTime);
				$('#schedule_realEndTime').val(res.realEndTime);
				$("#schedule_realFree").val(res.realFree);
				$('#schedule_taskCostDay').val(Math.floor(res.realFree*1/1440));
				$('#schedule_taskCostHour').val(Math.floor(res.realFree*1%1440/60));
				$("#schedule_taskCostMinute").val(Math.floor(res.realFree*1%1440%60));
				$('#schedule_treatmentResults').val(res.treatmentResults);
				$("#schedule_task_creator").val(res.taskCreator);
				$("#schedule_task_owner").val(res.ownerLoginName);
				$("#schedule_task_owner_fullName").val( res.owner );
				if(res.endTime!==null){
					$("#schedule_taskEndTime_hid").val(res.endTime);}
				else{
					$("#schedule_taskEndTime_hid").val(res.startTime);}
				$('#schedule_task_owner_link').unbind();
				if ( res.taskCycle == null || res.taskCycle === 'NO'
					|| res.taskCycle === '') {//非循环任务
					res.startDate = res.startTime;
					res.endDate = res.endTime;
					$("#schedule_taskCycle").attr('checked',true);
				}else{
					$("#schedule_taskCycleWeek").attr('checked',true);
					$("#schedule_taskCycle_tr_id").hide();
				}
				itsm.portal.schedule.cycleTaskEditInitForWeek(res);
				
				$('#schedule_task_owner_link').show().click(function(){
					common.security.userUtil.selectUser('#schedule_task_owner_fullName','#schedule_task_owner','','EditTesk',companyNo,'');
				});//选择技术员
				$('#schedule_link_task_save_edit_ok').hide();

				itsm.portal.schedule.userTaskViewByAuthority();
				$("#schedule_type").hide();
				$("#schedule_type_val").show().attr('disabled','disabled')
					.val( $("#schedule_type").find("option:selected").text() );
				$('#schedule_type').removeAttr('disabled');
				if (res.dCode == '' || res.dCode == null || res.dCode < 1) {//事件任务
					$(".schedule_taskCycle_tr").hide();
					$("#schedule_taskCycle_week_tr").hide();
					$(".schedule_taskTime").show();
					itsm.portal.schedule.timePickerInitValue("#schedule_taskEndTime_hid",
							"#schedule_taskEndTime","#schedule_taskEndTime_time",res.endTime);
					itsm.portal.schedule.timePickerInitValue("#schedule_taskStartTime_hid",
							"#schedule_taskStartTime","#schedule_taskStartTime_time",res.startTime);
					$("#schedule_type").attr('disabled','disabled');
				}
				$('.WSTUO-dialog').dialog('close');
				endProcess();
				$('#scheduleAddEditTaskDiv').dialog({width:600,position:['auto',80]});
			});
		},
		//删除任务
		remoreCalendarTask:function(id){
			msgConfirm(i18n.msg_msg,'<br/>'+i18n.msg_confirmDelete,function(){
				if ( _startTime && _endTime) {
					$.post("tasks!deleteScheduleTask.action",
							"taskDto.taskId="+id+"&taskDto.startTime="+_startTime+"&taskDto.endTime="+_endTime
							,function(){
						$("#taskGrid").trigger('reloadGrid');
						$('.WSTUO-dialog').dialog('close');
						msgShow(i18n.deleteSuccess,'show');
						if(state=="0"){
							itsm.portal.schedule.addEventSource('taskQueryDto.owner='+userName);
						}else if(state=="1"){
							itsm.portal.schedule.addEventSource('taskQueryDto.owner=');
						}else if(state=="2"){
							itsm.portal.schedule.addEventSource(showOther);
						}
						$('#schedule_div').fullCalendar('removeEvents',id);
						if (_eventTask != null && _eventTask != "" && _eventTask.length > 0) {
							$('#'+_eventTask+'EventTaskGrid').trigger('reloadGrid');
							_eventTask = "";
						}
					});
				}
				/*$.post("tasks!deleteTask.action",'taskIds='+id,function(){
					$("#taskGrid").trigger('reloadGrid');
					calendarTaskDiv.dialog('close');
					msgShow(i18n.deleteSuccess,'show');
					if(state=="0"){
						itsm.portal.schedule.addEventSource('taskQueryDto.owner='+userName);
					}else if(state=="1"){
						itsm.portal.schedule.addEventSource('taskQueryDto.owner=');
					}else if(state=="2"){
						itsm.portal.schedule.addEventSource(showOther);
					}
					$('#schedule_div').fullCalendar('removeEvents',id);
				});*/
			});
			
		},
		cycleTaskEditInitForWeek:function(res){
			$("#taskTypeForCycle input[value='" + res.taskCycle + "']").attr('checked',true);
			
			itsm.portal.schedule.choiseWeek(res.weekWeeks);//初始化星期
			itsm.portal.schedule.scheduleTaskCycle();
			$("#schedule_taskCycle_tr_div_week input[value='" + res.cycleWeekEnd + "']").attr('checked',true);
			//不循环任务时间设置值
			itsm.portal.schedule.timePickerInitValue("#schedule_taskStartTime_hid",
					"#schedule_taskStartTime","#schedule_taskStartTime_time",res.startTime);
			itsm.portal.schedule.timePickerInitValue("#schedule_taskEndTime_hid",
					"#schedule_taskEndTime","#schedule_taskEndTime_time",res.endTime);
			//按周循环的时间设置值初始化
			itsm.portal.schedule.timePickerInitValue(null,'#taskWeekStartDate',null,res.startTime);
			itsm.portal.schedule.timePickerInitValue(null,'#taskWeekEndDate',null,res.endTime);
		},
		choiseWeek:function(weeks){
			$("#schedule_taskCycle_tr_div_week input[name='taskDto.weekWeeks']").attr('checked',false);
			if (weeks == null || weeks.length == 0) {
				$("#schedule_taskCycle_tr_div_week input[name='taskDto.weekWeeks']")[new Date().getDay()].checked=true;
			}else{
				for ( var i in weeks) {
					$("#schedule_taskCycle_tr_div_week input[value='" + weeks[i] + "']").attr('checked',true);
				}
			}
			
		},
		//更新任务日程表
		addEventSource:function(parameter){
			parameter = encodeURI( parameter );
			$("#schedule_div").fullCalendar("removeEventSource",eventSource); //移除一个日程事件源  
			$('#schedule_div').fullCalendar('removeEvents');
			var full=$('#schedule_div').fullCalendar('events');
//			$('#schedule_div').fullCalendar({events:'tasks!findAllTaskShowSchedue.action?'+parameter}); 
//			$('#schedule_div').fullCalendar('refetchEvents');
			$('#schedule_div').fullCalendar('addEventSource','tasks!findAllTaskShowSchedue.action?'+parameter);
			eventSource='tasks!findAllTaskShowSchedue.action?'+parameter;
			setTimeout(function(){
				endLoading();
			},1000);
		},
		processCalendarTask:function(eventObject){
			var taskId = eventObject.taskId;
			var startTime = dateFormat(eventObject.start);
			var endTime = dateFormat(eventObject.end);
			
			var _url="tasks!editTaskDetail.action";
			$.post(_url,'taskDto.taskId='+taskId+"&taskDto.taskStatus="+1+"&taskDto.operator="+userName
					+"&taskDto.startTime="+startTime+"&taskDto.endTime="+endTime+"&taskDto.taskCycle="+eventObject.taskCycle
					,function(){
				calendarTaskDiv.dialog('close');
				$("#taskGrid").trigger('reloadGrid');
				msgShow(i18n.lable_startProcess,"show");
				if(state=="0"){
					itsm.portal.schedule.addEventSource('taskQueryDto.owner='+userName);
				}else if(state=="1"){
					itsm.portal.schedule.addEventSource('taskQueryDto.owner=');
				}else if(state=="2"){
					itsm.portal.schedule.addEventSource(showOther);
				}
				itsm.portal.schedule.reload();
			});
		},
		closedCalendarTask_opt:function(id){
			$.post("tasks!taskRealTimeDefaultValue.action",'taskId='+id,function(taskDto){
				calendarTaskDiv.dialog('close');
				resetForm("#realProcessTaskfm");
				$('#schedule_process_taskId').val(id);
				$('#schedule_process_realStartTime').val(taskDto.realStartTime);
				$('#schedule_process_realEndTime').val(taskDto.realEndTime);
				$('#schedule_process_taskCostDay').val(Math.floor(taskDto.realFree*1/1440));
				$('#schedule_process_taskCostHour').val(Math.floor(taskDto.realFree*1%1440/60));
				$('#schedule_process_taskCostMinute').val(Math.floor(taskDto.realFree*1%1440%60));
				$("#schedule_process_realFree").val(taskDto.realFree);
				$('#schedule_link_task_closedTask').unbind().click(function(){itsm.portal.schedule.closedCalendarTask();});
				windows('realProcessTaskDiv',{width:600});
			});
		},
		closedCalendarTask:function(){
			var _url="tasks!closedTaskDetail.action";
			if ($('#realProcessTaskfm').form('validate')) {
				var _param=$('#realProcessTaskDiv form').serialize();
				var eventObject = obj;
				if (eventObject) {
					var startTime = dateFormat(eventObject.start);
					var endTime = dateFormat(eventObject.end);
					_param += "&taskDto.startTime="+startTime+"&taskDto.endTime="+endTime+"&taskDto.taskCycle="+obj.taskCycle;
				}
				if(($('#schedule_process_realEndTime').val()!=="" && $('#schedule_process_realStartTime').val() !== "" && $('#schedule_process_realFree').val()>=1)){
					startProcess();
					$.post(_url,_param+"&taskDto.taskStatus="+2,function(){
						calendarTaskDiv.dialog('close');
						$("#taskGrid").trigger('reloadGrid');
						msgShow(i18n.lable_process_closedTask,"show");
						if(state=="0"){
							itsm.portal.schedule.addEventSource('taskQueryDto.owner='+userName);
						}else if(state=="1"){
							itsm.portal.schedule.addEventSource('taskQueryDto.owner=');
						}else if(state=="2"){
							itsm.portal.schedule.addEventSource(showOther);
						}
						$('#realProcessTaskDiv').dialog('close');
						endProcess();
					});
				}else{
					msgAlert(i18n.labe_taskClosedMsg,'info');
				}
				itsm.portal.schedule.reload();
			}
		},
		remarkCalendarTask_opt:function(id){
			$("#calendarTreatmentResults_tr").hide();
			
			var styleDisplay = $("#addcalendarTreatmentResultsTr").css("display");
			if (styleDisplay === "none") {
				$("#addcalendarTreatmentResultsTr").show();
				$('#schedule_link_task_remarkTask').unbind().click(function(){
					itsm.portal.schedule.remarkCalendarTask(id);
				});
			}else{
				$("#addcalendarTreatmentResultsTr").hide();
			}
		},
		remarkCalendarTask:function(id){
		    startProcess();
			var _url="tasks!remarkTaskDetail.action";
			var treatmentResults = $("#addcalendarTreatmentResults").val();
			$.post(_url,'taskDto.taskId='+id+"&taskDto.treatmentResults="+treatmentResults+"&taskDto.operator="+userName,function(){
				$("#addcalendarTreatmentResultsTr").attr("style","display:none;");
				$("#addcalendarTreatmentResults").val("");
				calendarTaskDiv.dialog('close');
				if(state=="0"){
					itsm.portal.schedule.addEventSource('taskQueryDto.owner='+userName);
				}else if(state=="1"){
					itsm.portal.schedule.addEventSource('taskQueryDto.owner=');
				}else if(state=="2"){
					itsm.portal.schedule.addEventSource(showOther);
				}
				endProcess();
				itsm.portal.schedule.reload();
			});
		},
		historyShow:function(eno){
			$('#calendarTreatmentResults').text('');
			$.post('historyRecord!findAllHistoryRecord.action','hisotryRecordDto.eno='+eno+'&hisotryRecordDto.eventType=itsm.task',function(data){
				var n=0;
				var optHtml="";
				$.each(data,function(i,v){
					var size = data.length-n;
					var _logDetails=v.logDetails;
					var optLogHtml=_logDetails;
					optLogHtml=optLogHtml
					.replace(/Task Remark/g,i18n.lable_taskRemark)
					.replace(/Task Closed/g,i18n.lable_process_closedTask)
					.replace(/Task Processing/g,i18n.lable_startProcess)
					.replace(/Add Task/g,i18n.lable_addTask)
					.replace(/Edit Task/g,i18n.lable_editTask)
					.replace(/Close Remark/g,i18n.close_remark);
					optHtml += '<span class=wspan><span id=nd'+n+'>'+size+'、</span>'+optLogHtml+'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;('+v.operator+') '+timeFormatter(v.createdTime)+'<br/></span>';
					n++;
				}); 
				$('#calendarTreatmentResults').html(optHtml);
			}); 
		},
		loadResourcesCalendar:function(){
			$('#itsmMainTab').tabs('select', i18n['title_calendar']);
			/*if ($('#itsmMainTab').tabs('exists', i18n['title_calendar'])){
				
			} else {
				$('#itsmMainTab').tabs('add',{
					title:i18n['label_dashboard_my_panel'],
					cache:"true",
					href:'view!findAllViewAndUserView.action?queryDTO.userName='+userName+'&queryDTO.customType='+1,
					closable:false
				});
			}*/
			common.config.includes.includes.loadDashboardDataInfoIncludesFile();
			/*$('#main_menu_tabs li').removeClass("tabs-selected");
			$('#protal_link_li').addClass("tabs-selected");
			$('#helpdeskPortalTab').tabs('select',i18n.title_calendar);*/
		},
		nowDay:function(date){
			var myDate = new Date();
			if (date) {
				myDate.setFullYear(date.getFullYear(),date.getMonth(),date.getDate());
			}
			var _dayDate = dateFormat( myDate );
			var strs = _dayDate.split(" ");
			itsm.portal.schedule.timePickerInitValue("#schedule_taskStartTime_hid",
					"#schedule_taskStartTime","#schedule_taskStartTime_time",_dayDate);

			var c = 2;
			var myDate1 = date;
			var _dayDate1 = _dayDate;
			if (myDate.getHours() != 23) {myDate1.setHours( myDate.getHours() + c );
			}else{myDate1.setDate( myDate1.getDate() + 1 );	myDate1.setHours( c-1 );}
			_dayDate1 = dateFormat( myDate1 );
			
			itsm.portal.schedule.timePickerInitValue("#schedule_taskEndTime_hid",
					"#schedule_taskEndTime","#schedule_taskEndTime_time",_dayDate1);
			
			itsm.portal.schedule.timePickerInitValue(null,'#taskWeekStartDate',null,_dayDate);
			itsm.portal.schedule.timePickerInitValue(null,'#taskWeekEndDate',null,_dayDate1);
			$("#schedule_taskCycle_tr_div_week input[name='taskDto.weekWeeks']")[date.getDay()].checked=true;
		},
		timePickerInitValue:function(hidInput,dateInput,timeInput,dateStr){
			var strs = dateStr.split(" ");
			if (dateInput) {$(dateInput).val(strs[0]);}
			if (timeInput) {$(timeInput).val(strs[1]);}
			if (hidInput) {	$(hidInput).val( dateStr );	}
		},
		allDay:function(){
			if ( $('#schedule_taskAllDay').is(":checked") ) {
			}
		},
		scheduleTaskCycle:function(){
			var val = $('input[name="taskDto.taskCycle"]:checked').val();
			if (val === "WEEK") {
				$("#schedule_taskCycle_week_tr").show();
				$(".schedule_taskTime").hide();
			}else{
				$("#schedule_taskCycle_week_tr").hide();
				$(".schedule_taskTime").show();
			}
		},
		
		showHistoryRecode:function(){
			$("#addcalendarTreatmentResultsTr").attr('style','display:none');
			
			var styleDisplay = $("#calendarTreatmentResults_tr").css("display");
			if (styleDisplay === "none") {
				$("#calendarTreatmentResults_tr").show();
			}else{
				$("#calendarTreatmentResults_tr").hide();
			}
		},
		calcDateTime:function(dateStrA,dateStrB){
			if (dateStrA && dateStrB) {
				dateStrA = dateStrA.replace(/-/g,"/");
				dateStrB = dateStrB.replace(/-/g,"/");
				var dateA = Math.ceil(new Date(dateStrA ).getTime() / 60000);
				var dateB = Math.ceil(new Date(dateStrB ).getTime() / 60000);
				
				var min = dateB - dateA;
				var day = Math.ceil(min / 1440);
				min = min - day * 1440;
				var hour = Math.ceil( min / 60);
				min = min - hour * 60;
				//console.log(day+"天"+hour+"时"+min);
			}
		},
		//itsm.portal.schedule.reload();
		reload:function(){
			$("#calendarTaskDiv").dialog('close');
			/*if (obj) {
				//itsm.portal.schedule.historyShow(obj.taskId);
				//itsm.portal.schedule.showCalendarTask(obj);
			}*/
		},
		taskCycleWeekCheck:function(){
			var start = $("#taskWeekStartDate").val();
			var end = $("#taskWeekEndDate").val();
			var startTime = new Date(Date.parse(start.replace(/-/g,"/")));
			var endTime = new Date(Date.parse(end.replace(/-/g,"/")));
			var day = Math.abs( startTime.getTime() - endTime.getTime() ) / 86400000;
			var weeks = $("#schedule_taskCycle_tr_div_week input[name='taskDto.weekWeeks']");
			if ( day < 7 ) {
				for(var i =0 ;i <= day ;i++){
			        if( $( weeks[startTime.getDay()] ).is(":checked") ){
			            return true;
			        }
		        	startTime.setDate( startTime.getDate() + 1 );  
				}
			}else{
				return true;
			}
			return false;
		},
		taskCycleWeekCheck_opt:function(){
			
			if ( $("#schedule_taskCycleWeek").is(":checked") ) {
				var weeks = $('#schedule_taskCycle_tr_div_week input:checkbox[name="taskDto.weekWeeks"]:checked');
				if ( weeks.size() > 0) {
		            var re = new RegExp(/^[0-9]*[1-9][0-9]*$/);
		            
					if ( $("#taskCycleWeekEndDate").is(":checked") 
							&& !itsm.portal.schedule.taskCycleWeekCheck() ){
						msgAlert(i18n.lable_task_cycle_week_error,'info');
					}else if( $("#taskCycleWeekEndCount").is(":checked")
							&& !($("#taskCycleWeekCount").val().match(re) != null) ){
						msgAlert(i18n.validate_integer,'info');
					}else{
						return false;
					}
				}else{
					msgAlert(i18n.lable_task_cycle_week_error1,'info');
				}
			}else{
				return false;
			}
			return true;
		},
		userTaskViewByAuthority:function(){
			if( choiseTaskType === false || choiseTaskType === "false"){//没有权限
				$("#schedule_task_taskType_tr,#schedule_task_owner_tr").attr("style","display:none;");
				var loginName = userName;
				if($("schedule_task_creator").val() != null){
					loginName = $("schedule_task_creator").val();
				}
				$("#schedule_task_owner").val( loginName );
				$("#schedule_task_owner_fullName").val( fullName );
				$("#schedule_type_no_access").val("-1");
				$("#schedule_type").attr("disabled",true);
			}else{
				$("#schedule_task_taskType_tr").removeAttr("style");
				$('#schedule_type').removeAttr('disabled');
			}
		},
		taskShow1Init:function(eventTask){
			_eventTask = eventTask;
		},
		init:function(){
			
		}
	};
}();