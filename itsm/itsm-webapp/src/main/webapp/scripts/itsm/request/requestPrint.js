 /**  
 * @author Will  
 * @constructor 
 * @description 请求打印工单主函数
 * @date 2013-11-17
 * @since version 1.0 
 * @returns
 * @param select
 */
var requestPrint=function(){
	return {
		html_encode : function(str){  
			  var s = "";  
			  if (str.length == 0) 
				  return "";
			  s = str.replace(/<div /g, "<p "); 
			  s = s.replace(/<\/div>/g, "</p>");  
			  return s;  
		},
		/**
		 * @description 初始化
		 */
		init:function(){
			$.post('request!showRequestInfo.action','eno='+eno,function(data){
				$('#request_printCode').html(data.requestCode);
				$('#request_printCreatedByName').html(data.fullName);
				$('#request_printCreatedByPhone').html(data.createdByPhone);
				if(data.createdByPhone!=null && data.createdByPhone!==''){
					if(data.createByMoblie!=null && data.createByMoblie!==''){
						$('#request_printCreatedByMoblie').html("/"+data.createByMoblie);
					}
				}else{
					$('#request_printCreatedByMoblie').html(data.createByMoblie);
				}
				$('#request_printTitle').html(data.etitle);
				$('#request_printEdesc').html(requestPrint.html_encode(data.edesc));
				$('#request_printRangeName').html(data.effectRangeName);
				$('#request_printStatusName').html(data.statusName);
				$('#request_printCategoryName').html(data.requestCategoryName);
				$('#request_printLevelName').html(data.levelName);
				$('#request_printPriorityName').html(data.priorityName);
				$('#request_printSeriousnessName').html(data.seriousnessName);
				$('#request_printImodeName').html(data.imodeName);
				$('#request_printMaxResponsesTime').html(timeFormatter(data.maxResponsesTime));
				$('#request_printMaxCompletesTime').html(timeFormatter(data.maxCompletesTime));
				$('#request_printResponsesTime').html(timeFormatter(data.responsesTime));
				$('#request_printCloseTime').html(timeFormatter(data.closeTime));
				$('#request_printCreatedOn').html(timeFormatter(data.createdOn));
				$('#request_printSolutions').html(data.solutions);
				$('#request_location').html(data.locationName);
				
				if(data.cigDTO!=null && data.cigDTO.length>0){
					var cigDTO=data.cigDTO;
					var str='';
					for(var i=0;i<cigDTO.length;i++){
						str+=","+cigDTO[i].ciname;			
					}
					$('#request_printCiName').html(str.substring(1));
				}
			});
			$.post('historyRecord!findAllHistoryRecord.action','hisotryRecordDto.eno='+eno+'&hisotryRecordDto.eventType=itsm.request',function(data){
				var n=0;
				$.each(data,function(i,v){
					var _logTitle=v.logTitle;
					var _logDetails=v.logDetails;

					var temp_logTitle=i18n[_logTitle];
					var temp_logDetails=i18n[_logDetails];
					
					if(temp_logTitle!=null && temp_logTitle!==''){
						_logTitle=temp_logTitle;
					}

					if(temp_logDetails!=null && temp_logDetails!==''){
						_logDetails=temp_logDetails;
					}
					var  optLogHtml=_logTitle+': '+_logDetails;
					optLogHtml=optLogHtml
					.replace(/TaskAssignGroup/g,i18n.title_request_assignToGroup)
					.replace(/TaskAssignTechnician/g,i18n.title_request_assignToTC)
					.replace(/AssignGroup/g,i18n.title_request_assignToGroup)
					.replace(/AssignTechnician/g,i18n.title_request_assignToTC)
					.replace(/Save Solution/g,i18n.problem_action_saveSolution)
					.replace(/Task take/g,i18n.label_taskTake)
					.replace(/Assign task/g,i18n.title_bpm_task_assign)
					.replace(/AssignTechnician/g,i18n.title_request_assignToTC)
					.replace(/ApproverResult/g,i18n.title_approverResult)
					.replace(/Disagree/g,i18n.title_refused)
					.replace(/assginTechnicalGroup/g,i18n.label_requestAssginTechnicalGroup)
					.replace(/Agree/g,i18n.title_agree)
					.replace(/TaskTake/g,i18n.label_taskTake)
					.replace(/TaskReAssigne/g,i18n.assingeeTask)
					.replace(/Assign Task/g,i18n.title_bpm_task_assign)
					.replace(/Edit Request/g,i18n.title_request_editRequest)
					.replace(/Remark/g,i18n.remark);
					n++;
					var str='<span class=wspan><span id=nd'+n+'></span>'+optLogHtml+'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;('+v.operator+')'+timeFormatter(v.createdTime)+'<br/></span>';
					$(str).insertAfter('#request_printHistory');
				}); 
				if(n>0){	//序号	1、2、3.....
					var s=1;
					for ( var _int = n; _int > 0; _int--) {
						$("#nd"+_int).text((s++)+"、");
					}
				}
			}); 
		}
	};
}();
$(document).ready(requestPrint.init);