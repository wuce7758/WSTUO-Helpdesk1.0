$package("itsm.request"); 
$import('common.config.includes.includes');
$import('common.security.includes.includes');

 /**  
 * @author Wstuo  
 * @constructor Wstuo
 * @description 绑定一级菜单
 * @date 2013-07-16
 * @since version 1.0 
 */
itsm.request.requestTop = function() {
	
	//载入
	return {
		/**
		 * @description 菜单点击事件处理
		 */
		menuClick:function(){
			showRequestIndex();
		},
		includesLoading:function(){
			endLoading();
			itsm.request.includes.includes.loadRequestActionIncludesFile();
			common.security.includes.includes.loadSelectCustomerIncludesFile();
			common.config.includes.includes.loadCategoryIncludesFile();
			common.config.includes.includes.loadCustomFilterIncludesFile();
		}
	};
	
}();