﻿"use strict";
$.jstree.plugin("contextmenu", {
		__init : function () {
			this.get_container()
				.delegate("a", "contextmenu.jstree", $.proxy(function (e) {
						e.preventDefault();
						this.show_contextmenu(e.currentTarget, e.pageX, e.pageY);
					}, this))
				.bind("destroy.jstree", $.proxy(function () {
						if(this.data.contextmenu) {
							$.vakata.context.hide();
						}
					}, this));
			$(document).bind("context_hide.vakata", $.proxy(function () { this.data.contextmenu = false; }, this));
		},
		defaults : { 
			select_node : false, // requires UI plugin
			show_at_node : true,
			items : { // Could be a function that should return an object like this one
				
				"cut" : {
					"separator_before"	: false,
					"separator_after"	: false,
					"label"				: i18n['label_tree_cut'],
					"action"			: function (obj) { this.cut(obj);msgShow(i18n['msg_operationSuccessful'],'show'); }
				},
				"copy" : {
					"separator_before"	: false,
					"icon"				: false,
					"separator_after"	: false,
					"label"				: i18n['label_tree_copy'],
					"action"			: function (obj) { this.copy(obj); msgShow(i18n['msg_operationSuccessful'],'show');}
				},
				"paste" : {
					"separator_before"	: false,
					"icon"				: false,
					"separator_after"	: false,
					"label"				: i18n['label_tree_paste'],
					"action"			: function (obj) { this.paste(obj); }
				},
				"remove" : {
					"separator_before"	: false,
					"icon"				: false,
					"separator_after"	: false,
					"label"				: i18n['label_tree_delete'],
					"action"			: function (obj) {
					    var $remove = this;
						msgConfirm(i18n['msg_msg'],'<br/>'+i18n['msg_confirmDelete'],function(){
						    $remove.remove(obj);
				        });
					}
				}
			}
		},
		_fn : {
			show_contextmenu : function (obj, x, y) {
				obj = this._get_node(obj);
				var s = this.get_settings().contextmenu,
					a = obj.children("a:visible:eq(0)"),
					o = false;
				if(s.select_node && this.data.ui && !this.is_selected(obj)) {
					this.deselect_all();
					this.select_node(obj, true);
				}
				if(s.show_at_node || typeof x === "undefined" || typeof y === "undefined") {
					o = a.offset();
					x = o.left;
					y = o.top + this.data.core.li_height;
				}
				if($.isFunction(s.items)) { s.items = s.items.call(this, obj); }
				this.data.contextmenu = true;
				$.vakata.context.show(s.items, a, x, y, this, obj);
				if(this.data.themes) { $.vakata.context.cnt.attr("class", "jstree-" + this.data.themes.theme + "-context"); }
			}
		}
	});
