<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%
String url="login.jsp";

 if(session.getAttribute("loginUserName")==null){
	response.sendRedirect(url);
	return;
} 
 %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html class="no-js">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<title>添加知识库-WSTUO</title>

<link rel="icon" type="image/png" href="assets/img/logo2.png">
<link rel="stylesheet" href="assets/css/amazeui.min.css">
<link href="../css/upload/fileinput.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="editor/css/wangEditor-mobile.min.css">
<link rel="stylesheet" href="assets/css/app.css">	
<style type="text/css">
.container {
	width:100%; 
	height:220px; 
	border:1px solid #ccc;
	background-color: #fff;
	text-align: left;
	box-shadow: 0 0 10px #ccc;
	text-shadow: none;
}
</style>
</head>
<body>
<!-- Header -->
<header data-am-widget="header" class="am-header am-header-default">
    <h1 class="am-header-title">
     添加知识库
    </h1>
  </header>
<!-- Menu -->
<nav data-am-widget="menu" class="am-menu am-menu-offcanvas1" data-am-menu-offcanvas>
      <a href="javascript:history.go(-1);" class="am-menu-left">
    <i style="text-align: right;padding: 16px;" class="am-menu-toggle-icon am-icon-chevron-left"></i>
  </a>
  <a href="javascript: void(0)" class="am-menu-toggle">
    <i style="text-align: right;padding: 16px;" class="am-menu-toggle-icon am-icon-bars"></i>
  </a>
  <div class="am-offcanvas">
    <div class="am-offcanvas-bar">

      <ul class="am-menu-nav sm-block-grid-1">      
        <li>
          <a href="user!findUserDetail_wap.action?userName=${sessionScope.loginUserName}">个人信息</a>        
        </li>
        <li >
          <a href="pwdUpdate.jsp">密码修改</a>       
        </li>  
        <li>
          <a href="index.jsp">返回首页</a>       
        </li>   
        <li>
          <a href="logout.jsp">注&nbsp;&nbsp;销</a>       
        </li>
      </ul>
    </div>
  </div>
</nav>
<!-- List -->
<div  class="am-list-news am-list-news-default">
	<form id="addKnowledgeForm" event="checkKnowledgeTitle">
	 	 <input type="hidden" name="knowledgeDto.creator" value="${loginUserName}"  />
		<input type="hidden" value="${sessionScope.companyNo}" id="request_companyNo" name="requestDTO.companyNo" required='true'>
		<input type="hidden" value="${sessionScope.userId}" id="request_userId" name="requestDTO.createdByNo"  required='true'>
          <div class="form-group col-md-6 ui-sortable-handle">
			<label class="form-control-label">分类<span class="required">*</span></label>
			<input type=hidden name="knowledgeDto.categoryNo" id="addKnowledge_categoryNo">
            <input id="addKnowledge_Category"  class="form-control choose" required="true" readonly="readonly"/>
		</div>
		<div class="form-group col-md-6 ui-sortable-handle">
			<label class="form-control-label">标题<span class="required">*</span></label>
			<input name="knowledgeDto.title" id="addKnowledge_title" value=""  required='true' class="form-control" type="text">
		</div>
		<div class="form-group col-md-6 ui-sortable-handle">
			<label class="form-control-label">关键字</label>
			<input attrtype='String' id="addKnowledge_keyWords" name="knowledgeDto.keyWords"  class="form-control" />
		</div>
		<div class="form-group col-md-12 ui-sortable-handle">
			<label class="form-control-label">描述<span class="required">*</span></label>
			<div class="container"><textarea class="form-control" rows="6"  id="addKnowledge_Content" name="knowledgeDto.content" >
			${requestDetailDTO.edesc}
			<hr/>
			${requestDetailDTO.solutions} 
			</textarea></div>
		</div>
		<sec:authorize url="/pages/knowledgeInfo!passKW.action">
		<div class="form-group col-md-12 ui-sortable-handle">
						<label><input type="checkbox" value="1" name="knowledgeDto.knowledgeStatus" id="addKnowledge_knowledgeStatus">&nbsp;&nbsp;已审批</label>
		</div></sec:authorize>
		<div class="form-group" id="addKnowledge_uploadAttachments_div" style="margin-top:10px">
     	 <input type="hidden" name="knowledgeDto.attachmentStr" id="addKnowledge_attachments" value=""/>
         <input id="addKnowledge_uploadAttachments" type="file" name="filedata" multiple>
     	</div>
          	<div style="height:50px"></div>
        </form>
</div>

<div class="am-modal am-modal-no-btn" tabindex="-1" id="knowledge_category_select_window">
  <div class="am-modal-dialog">
    <div class="am-modal-hd">
      <a href="javascript: void(0)" class="am-close am-close-spin" data-am-modal-close>&times;</a>
    </div>
    <div class="am-modal-bd" id="knowledge_category_select_tree">
    </div>
  </div>
</div>
<%@ include file="include_tip.jsp"%>
<!-- List -->
<div class="am-list-news am-list-news-default">

<!-- Navbar -->
<div  class="am-navbar am-cf am-navbar-default "
     id="">
  <ul class="am-navbar-nav am-cf am-avg-sm-4">
    <li>
      <a href="#" id="reset_btn">
        <span ><i class="am-icon-repeat"></i></span>
        <span class="am-navbar-label">重置</span>
      </a>
    </li>
    <li>
      <a href="#" id="save_btn">
        <span class="am-icon-save"></span>
        <span class="am-navbar-label">保存</span>
      </a>
    </li>

  </ul>
</div>
</div>
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/amazeui.min.js"></script>
<script src="../js/i18n/i18n_zh_CN.js"></script>
<script type="text/javascript" src="../js/basics/package.js"></script>
<script type="text/javascript" src="../js/basics/import.js"></script>
<script src="../js/jquery/jstree/js/lib/jquery.cookie.js"></script>
<script src="../js/jquery/jstree/js/lib/jquery.hotkeys.js"></script>
<script src="../js/jquery/jstree/js/jquery.jstree.js"></script>
<script src="../js/jquery/jstree/js/rewritejstree.js"></script>
<script src="../js/My97DatePicker/WdatePicker.js"></script>
<script src="../js/jquery/jquery.validate.min.js"></script>
<script src="../js/jquery/messages_zh.js"></script>

<script src="js/fileinput.js" type="text/javascript"></script>
<script src="../js/basics/initUploader.js" type="text/javascript"></script>
<script type="text/javascript" src="editor/js/lib/zepto.js"></script>
<script type="text/javascript" src="editor/js/lib/zepto.touch.js"></script>
<script type="text/javascript" src="editor/js/wangEditor-mobile.min.js"></script>
<script type="text/javascript">

// 生成编辑器
var editor = new ___E('addKnowledge_Content');
// 初始化
editor.init();
</script>
<script src="js/common.js"></script>
<script src="js/addKnowledge.js"></script>
</body>
</html>