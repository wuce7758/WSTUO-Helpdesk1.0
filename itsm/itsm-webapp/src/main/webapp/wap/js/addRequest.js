$import('wstuo.customForm.formControlImpl');
$import('wstuo.tools.xssUtil');
var addRequest = function() 
{
	this.htmlDivId="addRequest_formField";
	this.defaultFormId;
	this.url='request!updateRequest.action';
	/**
	 * @description 提交保存请求.
	 */
	this.saveRequest=function(){
		var edesc= editor.$txt.html();//$("#request_edesc").val();
		var uid=$('#request_userId').val();
		var title= $('#addRequestForm #request_etitle').val();
		$('#addRequestForm #request_etitle').val(trim(title));
		var request_edesc=(edesc===''|| edesc=="null" || edesc=="NULL")?false:true;
		if(!request_edesc){
			msgAlert(i18n.titleAndContentCannotBeNull,'info');
		}else if(uid===''){
			$('#request_userId').val('');
			msgAlert(i18n.ERROR_CREATE_BY_NULL,'info');
		}else{
			getFormAttributesValue("#addRequestForm");
			$('#addRequestForm #request_edesc').val(edesc);
			/*$.each($("#addRequestForm input[attrtype='String']"),function(ind,val){
				$(this).val(wstuo.tools.xssUtil.html_encode($(this).val()));
			});
			*/
			var frm = $('#addRequestForm').serialize();
			startProcess();
			$.post(url,frm, function(id){
				endProcess();
				msgAlert(i18n.saveSuccess);
				if(eno) id=eno;
				location.href="request!requestDetailsWap.action?eno="+id;
			});
		}
	};
	this.getFormAttributesValue=function(formId){
		$(formId+" :hidden").each(function(i, obj){
			var value='';
			var valuename='';
			var attrType=$(obj).attr("attrType");
			if(attrType==='checkbox'){
				$(formId+" input[name='"+$(obj).attr("id")+"']:checked").each(function(i, o){
				    value=value+","+$(o).val();
				    valuename=valuename+"，"+$(o).attr('title');
				});
				$(obj).val(value.substr(1)+"~"+valuename.substr(1));
			}else if(attrType==='radio'){
				var o=$(formId+" input[name='"+$(obj).attr("id")+"']:checked");
				$(obj).val(o.val()+"~"+o.attr('title'));
			}else if(attrType==='tree'){
				var val=$(formId+" #show_"+$(obj).attr("id")).val();
				$(obj).val($(obj).val()+"~"+val);
			}
		});
	};
	this.selectTree=function (){
		showSelectTree(
			'#request_category_select_window',
            '#request_category_select_tree',
            'Request',
            '#request_categoryName',
            '#request_categoryNo',
            'requestDTO',function(formCustomId){
				if(defaultFormId!=undefined && defaultFormId!='edit'){
					if((!formCustomId || formCustomId==0) && defaultFormId!==0){
						formCustomId=defaultFormId;
					}
					if(formCustomId){
						loadRequestFormHtmlByFormId(formCustomId);
						$("#addRequest_formId").val(formCustomId);
					}
					else{
						$("#addRequest_formId").val(0);
						$("#addRequest_formField").empty();
					}
				}
			});
	};
	this.showSelectTree=function(windowId,treePanel,param,namePut,idPut,dtoName,method){
		 $(treePanel).jstree({
				json_data: {
					ajax: {
					  type:"post",
					  url : "event!getCategoryTree.action?num=30",
					  data:function(n){
				    	  return {'types': param,'parentEventId':n.attr ? n.attr("id").replace("node_",""):0};//types is in action
				      },
	                  dataType: 'json',
	                  cache:false
				 }
				},
				plugins : ["themes", "json_data","cookies", "ui", "crrm","search"]
		})
		.bind("select_node.jstree",function(e,data){
			var obj = data.rslt.obj;
			$(namePut).val(obj.attr("cname")).focus();
			$(idPut).val(obj.attr("id"));
			$(windowId).modal('close');
			if( typeof method === 'function' )
				method(data.rslt.obj.attr('formId'));
		});
		$(windowId).modal({width:'220px'});
	 };
	 this.loadRequestFormHtmlByFormId=function(formId){
		$("#addRequest_formId").val(formId);
		$.post("formCustom!findFormCustomById.action","formCustomId="+formId,function(data){
			if(data.formCustomContents!=""){
				var formCustomContents = wstuo.customForm.formControlImpl.editHtml(data.formCustomContents,htmlDivId);
				$('#'+htmlDivId).html(formCustomContents);
				$('#'+htmlDivId).find('.glyphicon-trash').remove();
				wstuo.customForm.formControlImpl.formCustomInit('#'+htmlDivId);
				wstuo.customForm.formControlImpl.formCustomInitByDataDictionaray('#'+htmlDivId);
			}
		});
	 };
	 this.setRequestFormValues=function(eventdata,formId,callback){
			setRequestParamValueToOldForm("#addRequestForm",eventdata);
			setAttributesValue(eventdata,formId,callback);
		};
	this.setAttributesValue=function(data,formId,callback){
		if(data!==null){
			var eventEavVals=data.attrVals;
			for(var o in eventEavVals){
				var value='';
				var attrName=o;
				var obj=$(formId+" :input[name*='"+attrName+"']");
				if(eventEavVals && eval('eventEavVals.'+attrName)!==undefined && eval('eventEavVals.'+attrName)!==''){
					value = eval('eventEavVals.'+attrName);
				}
				if(value==undefined || value==null)value='';
				
				if(obj.attr("attrType")=="select"){
					$(formId+" select[name*='"+attrName+"']").attr("val",value);
					//wstuo.dictionary.dataDictionaryUtil.setSelectOptionsByCode(obj.attr('attrdataDictionary'),formId+' #'+selectId,value);
				}if(obj.attr("attrType")=="tree"){
					obj.siblings(".form-control").val(value.substr(value.indexOf('~')+1));
					obj.val(value.substr(0,value.indexOf('~')));
				}else{
					obj.val(value);
				}
			}
			callback();
		}
	};
	this.setRequestParamValueToOldForm=function(htmlDivId,data){
		$(htmlDivId+" #request_companyNo").val(data.companyNo);
		$(htmlDivId+" #request_userName").val(data.createdByName);
		$(htmlDivId+" #request_userId").val(data.createdByNo);
		//$(htmlDivId+" #request_edesc").val(data.edesc);
		editor.$txt.append(data.edesc);
		$(htmlDivId+" #request_etitle").val(wstuo.tools.xssUtil.html_code(data.etitle));
		if(data.requestCategoryName!=null && data.requestCategoryName!=""){
			$(htmlDivId+" #request_categoryName").val(data.requestCategoryName);
		}else{
			$(htmlDivId+" #request_categoryName").val(data.ecategoryName);
		}
		if(data.requestCategoryNo!=null && data.requestCategoryNo!=""){
			$(htmlDivId+" #request_categoryNo").val(data.requestCategoryNo);
		}else{
			$(htmlDivId+" #request_categoryNo").val(data.ecategoryNo);
		}
	 };
	 this.findRequestById=function(){
		$('#'+htmlDivId).html("");
		var url = 'request!findRequestById.action?requestQueryDTO.eno='+eno;
		$.post(url, function(res){
			if(res.formId){
				var _param = {"formCustomId" : res.formId};
				$.post('formCustom!findFormCustomById.action',_param,function(data){
					if(data.formCustomContents!=""){
						var formCustomContents = wstuo.customForm.formControlImpl.editHtml(data.formCustomContents,htmlDivId);
						$('#'+htmlDivId).html(formCustomContents);
						$('#'+htmlDivId).find('.glyphicon-trash').remove();
					}
					setRequestFormValues(res,'#'+htmlDivId,function(){
						wstuo.customForm.formControlImpl.formCustomInit('#'+htmlDivId);
						wstuo.customForm.formControlImpl.formCustomInitByDataDictionaray('#'+htmlDivId);
						endProcess();
					});
					setEditRequestParamValue(res);
				});
			}else{
				setRequestParamValueToOldForm('#addRequestForm',res);
				setEditRequestParamValue(res);
				endProcess();
			}
				
		});
	};
	this.setEditRequestParamValue=function(res){
		$('#requestEdit_requestCode').val(res.requestCode);
		$('#requestEdit_pid').val(res.pid);
		if(res.formId!=null){
			$('#addRequest_formId').val(res.formId);
		}else{
			$('#addRequest_formId').val(0);
		}
		$('#requestEdit_eno').val(res.eno);
		$('#requestEdit_status').val(res.statusNo);
		$("#requestDTO_actionName").val('编辑');
		$('.am-header-title').text('编辑请求');
		$("#addRequest_processKey").val(res.processKey);
	};
	 return{
		init: function() {
			$("#request_categoryName").click(selectTree);
			$("#save_btn").click(function(){
				$('#addRequestForm').submit();
			});
			$("#reset_btn").click(function(){
				$('#addRequestForm')[0].reset();
			});
			if(eno){
				defaultFormId='edit';
				findRequestById();
			}else{
				url = 'request!saveRequest.action?requestDTO.isShowBorder=0&requestDTO.isNewForm=true';
				$.post("formCustom!findIsDefault.action?type=request",function(data){
					if(data.formCustomContents!=""){
						var formCustomContents = wstuo.customForm.formControlImpl.editHtml(data.formCustomContents,htmlDivId);
						$('#'+htmlDivId).html(formCustomContents);
						$('#'+htmlDivId).find('.glyphicon-trash').remove();
						wstuo.customForm.formControlImpl.formCustomInit('#'+htmlDivId);
						wstuo.customForm.formControlImpl.formCustomInitByDataDictionaray('#'+htmlDivId);
					}
					defaultFormId=data.formCustomId;
					$("#addRequest_formId").val(data.formCustomId);
				});
				//初始化附件上传控件
				setTimeout(function(){
					getUploader('#add_request_file','#add_request_attachmentStr','','',null,false);
				},0);
			}
			initValidate();
		}
	 };
}();
//载入
$(document).ready(addRequest.init);